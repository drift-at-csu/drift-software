#include <utl/TimeInterval.h>
using namespace drift;

//-------------------------------------------------------------------//
utl::TimeInterval::TimeInterval():fSeconds(0),fNanoSeconds(0){
}
//-------------------------------------------------------------------//
utl::TimeInterval::TimeInterval(const long aSeconds, const long aNanoSec):
	fSeconds(aSeconds),fNanoSeconds(aNanoSec){

  while (fNanoSeconds < -1000000000) {
  	fNanoSeconds += 1000000000;
	fSeconds -= 1;
  }
  while (fNanoSeconds > 1000000000) {
  	fNanoSeconds -= 1000000000;
	fSeconds += 1;
  }
}
//-------------------------------------------------------------------//
utl::TimeInterval::TimeInterval(const TimeInterval& aTimeInterval) :
	fSeconds(aTimeInterval.fSeconds), 
	fNanoSeconds(aTimeInterval.fNanoSeconds) {
}
//-------------------------------------------------------------------//
utl::TimeInterval::~TimeInterval() {
}
//-------------------------------------------------------------------//
long utl::TimeInterval::GetSecond() const{
  return fSeconds;
}
//-------------------------------------------------------------------//
long utl::TimeInterval::GetNanoSecond() const{
  return fNanoSeconds;
}
//-------------------------------------------------------------------//
