#ifndef _DRIFT_utl_Trace_h_
#define _DRIFT_utl_Trace_h_

#include <TROOT.h>

#include <string>
#include <vector>
#include <array> //FGSII add 09/30/2014

#include <utl/TimeRange.h>
#include <utl/TimeStamp.h> //fmod add 07/09/2014

namespace drift {
namespace utl {

class Trace {
  public:
	//FGSII add 09/30/2014 
	typedef std::array<double,3> QuadParams; //quadratic fit parameters
	//note: if we declare "QuadParams params", then,
	//params[0] => constant term
	//params[1] => linear term
	//params[2] => quadratic term

  	typedef std::vector<double> TimeBinsContainer;
	typedef TimeBinsContainer::iterator TimeBinsIterator;
	typedef TimeBinsContainer::const_iterator ConstTimeBinsIterator;

  public:
	Trace();
	~Trace();
	///===> Let's List a useful Math one can do with the trace:
	/// Multiplies the whole trace by a value number
	void MultiplyBy(double value);

	///Subtracts the pedestal from the trace, assuming
	///pedestal is constant for whole trace
	///FGSII add on 08/29/2014
	void SubtractConstPedestal(double value);

	//FSGII add 10/17/2014
	void AddConst(double value);

	//FGSII add 09/30/2014
	//Subtracts quadratic pedestal from trace
	void SubtractQuadraticPedestal(QuadParams params);

	//FGSII add 10/13/2014
	double GetMax();
	double GetMin();

	//FGSII add 10/05/2014
	bool HasSpark();

	//FGSII add 10/15/2014
	void DeconvolveACCoupling(double time_const);
	void DeconvolveCurrent(double time_const);
	void Integrate(double mult_factor);
	//FGSII add 10/31/2014
	void DeconvolveACCouplingFSS(double time_const,unsigned int bin); //FSS = From Signal Star
	void DeconvolveCurrentFSS(double time_const, unsigned int bin);

	//-----------------------------------------------------------------------//

	///===> Setters and Gettersafter this
	//fmod add 07/07/2014
	void SetTraceId(unsigned int trace_id);
	unsigned int GetTraceId();
	//fmod add 07/09/2014
	long GetTraceTimeSec();
	long GetTraceTimeNsec();
	void SetTraceTime(long sec, long nsec);
	void SetTraceOffset(double trace_offset);
	double GetTraceOffset();

	void SetBinDuration(double bin_duration);
	double GetBinDuration() const;

	unsigned int GetNofTimeBins() const;
	double& GetTimeBin(const unsigned int time_bin);
	const double& GetTimeBin(const unsigned int time_bin) const;

	TimeBinsIterator TimeBinsBegin();
	TimeBinsIterator TimeBinsEnd();
	ConstTimeBinsIterator TimeBinsBegin() const;
	ConstTimeBinsIterator TimeBinsEnd() const;

	Trace operator=(const Trace& trc);

	void AddTimeBin(double time_bin_value);
	/// Clears all time bins from Container
	void Clear();

	//void ClearPedestals();//FGSII comment out 09/05/2014
  private:
	double fTimeBinDuration;
	TimeBinsContainer fTimeBins;

	//fmod add 07/07/2014
	unsigned int fTraceID;	
	utl::TimeStamp fTraceTime;
	//unsigned long fTraceNofBins;//shouldn't need
	double fTraceOffset;

}; /* class Trace */

//=============================== Global functions for Minuit
// This is a parabola fit function
// par[0] is an offset over time
// par[1] quadratic term
// par[2] linear term
// par[3] offset
Double_t FitParabolaFunc(double x, Double_t *par);
// This is Chi2 for parabola fit
void FitParabolaChi2(Int_t &npar,
	Double_t *gin, Double_t &f, Double_t *par, Int_t iflag);

} /* namespace utl */
} /* namespace drift */
#endif /* _DRIFT_utl_Trace_h_ */
