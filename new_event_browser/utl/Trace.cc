#include <utl/Trace.h>
using namespace drift;

#include <iostream>
#include <math.h>
#include <algorithm>
using namespace std;

#include <TMinuit.h>
#include <TSpline.h>

namespace drift{
namespace utl{
	unsigned long FitNofPoints;
	double *FitYarray;
	double *FitXarray;
}
}
//-------------------------------------------------------------------//
utl::Trace::Trace():
	fTimeBinDuration(0.0){
}
//-------------------------------------------------------------------//
utl::Trace::~Trace(){
  Clear();
  //ClearPedestals();//FGSII comment out 09/05/2014
}
//-------------------------------------------------------------------//
void utl::Trace::MultiplyBy(double value){
	cout << "...utl::Trace::MultiplyBy" << endl;

  for (TimeBinsIterator iter=TimeBinsBegin(); iter != TimeBinsEnd();
  		++iter){
	(*iter) *= value;
  }
}
//-------------------------------------------------------------------//
//FGSII add 08/29/2014
//this is to be used assuming a constant valued pedestal
//this will change if we fit a quadratic or spline to the pedestal
//instead of assuming a constant value
void utl::Trace::SubtractConstPedestal(double value){
	cout << "...utl::Trace::SubtractConstPedestal" << endl;
	cout << "......pedestal = " << value << endl;
	for (TimeBinsIterator iter = TimeBinsBegin(); iter != TimeBinsEnd(); ++iter) {
	(*iter) -= value;
  }
}
//-------------------------------------------------------------------//
void utl::Trace::SetBinDuration(double bin_duration){
  fTimeBinDuration = bin_duration;
}
//-------------------------------------------------------------------//
double utl::Trace::GetBinDuration() const {
  return fTimeBinDuration;
}
//-------------------------------------------------------------------//
unsigned int utl::Trace::GetNofTimeBins() const{
  return fTimeBins.size();
}
//-------------------------------------------------------------------//
double& utl::Trace::GetTimeBin(const unsigned int time_bin){
  return fTimeBins[time_bin];
}
//-------------------------------------------------------------------//
const double& utl::Trace::GetTimeBin(const unsigned int time_bin) const{
  return fTimeBins[time_bin];
}
//-------------------------------------------------------------------//
utl::Trace::TimeBinsIterator utl::Trace::TimeBinsBegin(){
  return fTimeBins.begin();
}
//-------------------------------------------------------------------//
utl::Trace::TimeBinsIterator utl::Trace::TimeBinsEnd(){
  return fTimeBins.end();
}
//-------------------------------------------------------------------//
utl::Trace::ConstTimeBinsIterator utl::Trace::TimeBinsBegin() const{
  return fTimeBins.begin();
}
//-------------------------------------------------------------------//
utl::Trace::ConstTimeBinsIterator utl::Trace::TimeBinsEnd() const{
  return fTimeBins.end();
}
//-------------------------------------------------------------------//
utl::Trace utl::Trace::operator=(const utl::Trace& trc){
	Clear();
	//ClearPedestals();//FGSII comment out 09/05/2014

	//FGSII add 09/04/2014-------------------------
	fTraceID = trc.fTraceID;
	fTraceOffset = trc.fTraceOffset;
	fTraceTime = trc.fTraceTime;
	//---------------------------------------------

	fTimeBins = trc.fTimeBins;
	fTimeBinDuration = trc.fTimeBinDuration;
	//fPedestals = trc.fPedestals;//FGSII comment out 09/05/2014

	return *this;
}
//-------------------------------------------------------------------//
void utl::Trace::AddTimeBin(double time_bin_value){
  fTimeBins.push_back(time_bin_value);
}
//-------------------------------------------------------------------//
void utl::Trace::Clear(){
  fTimeBins.clear();
}
//===================================================================//
Double_t utl::FitParabolaFunc(double x, Double_t *par){
  Double_t value=(par[1]*(x-par[0])*(x-par[0]))+(par[2]*(x-par[0]))+par[3];
  return value;
}
//-------------------------------------------------------------------//
void utl::FitParabolaChi2(
	Int_t &npar, Double_t *gin, Double_t &f, Double_t *par, Int_t iflag){

  //calculate chisquare
  Double_t chisq = 0;
  Double_t delta;
  for (unsigned long ii = 0; ii < FitNofPoints; ii++) {
	delta  = (FitYarray[ii]-FitParabolaFunc(FitXarray[ii], par));
	chisq += delta*delta;
  }
  f = chisq;
}
//-------------------------------------------------------------------//
void utl::Trace::SetTraceId(unsigned int trace_id){
	fTraceID = trace_id;
}
//-------------------------------------------------------------------//
unsigned int utl::Trace::GetTraceId(){
	return fTraceID;
}
//-------------------------------------------------------------------//
long utl::Trace::GetTraceTimeSec(){
	return fTraceTime.GetGPSSecond();
}
//-------------------------------------------------------------------//
long utl::Trace::GetTraceTimeNsec(){
	return fTraceTime.GetGPSNanoSecond();
}
//-------------------------------------------------------------------//
void utl::Trace::SetTraceTime(long sec, long nsec){
	fTraceTime.SetGPSTime(sec,nsec);
}
//-------------------------------------------------------------------//
void utl::Trace::SetTraceOffset(double trace_offset){
	fTraceOffset = trace_offset;
}
//-------------------------------------------------------------------//
double utl::Trace::GetTraceOffset(){
	return fTraceOffset;
}
//-------------------------------------------------------------------//
//FGSII add 09/30/2014
//NEED to check this function for correctness
void utl::Trace::SubtractQuadraticPedestal(QuadParams params){
	cout << "...utl::Trace::SubtractQuadraticPedestal()" << endl;
	double x,y;
	double ii=0;
	for (TimeBinsIterator iter = TimeBinsBegin(); iter != TimeBinsEnd(); ++iter) {
		x = GetTraceOffset() + (ii+1)*GetBinDuration();
		y = params[0] + params[1]*x + params[2]*x*x;
		(*iter) -= y;
		ii += 1;
  }
}
//-------------------------------------------------------------------//
//FGSII add 10/05/2014
bool utl::Trace::HasSpark(){

	cout << "...utl::Trace::HasSpark()" << endl;	

/*
	double slope_max = uint_max/GetBinDuration();
	//const double slope_max = 100.0/GetBinDuration();
	bool is_first_bin = true;
	double prev_bin, slope;
	bool is_spark = false;
	for(TimeBinsIterator iter = TimeBinsBegin(); iter != TimeBinsEnd(); ++iter){
		if(is_first_bin == true){
			prev_bin = (*iter);
			is_first_bin = false;
		}
		else{
			slope = ((*iter)-prev_bin)/GetBinDuration();
			if(abs(slope) >= slope_max){
				is_spark = true;
			}
			if(is_spark == true){
				return true;
			}
			prev_bin = (*iter);
		}
	}
	return false;
*/
	
	const double max_noise = 50.0;
	const double pi = 2.0*asin(1.0);
	const double angle_max = (pi/2.0)*0.9;
	bool is_first_bin = true;
	double prev_bin, angle;
	bool is_spark = false;
	double delta_y;
	for(TimeBinsIterator iter = TimeBinsBegin(); iter != TimeBinsEnd(); ++iter){
		if(is_first_bin == true){
			prev_bin = (*iter);
			is_first_bin = false;
		}
		else{
			delta_y = (*iter)-prev_bin;
			angle = atan(abs(delta_y)/GetBinDuration());
			if(angle >= angle_max && abs(delta_y) >= max_noise){
				cout << "......TraceId = " << GetTraceId() << endl;
				cout << "......calculated angle (deg.): " << angle*(180.0/pi) << endl;
				is_spark = true;
			}
			if(is_spark == true){
				return true;
			}
			prev_bin = (*iter);
		}
	}
	return false;

}
//-------------------------------------------------------------------//
//FGSII add 10/13/2014
double utl::Trace::GetMax(){
	cout << "...utl::Trace::GetMax() " << endl;
	double max = GetTimeBin(0);
	unsigned int bin = 0;
	unsigned int max_bin;
	for(TimeBinsIterator iter = TimeBinsBegin(); iter != TimeBinsEnd(); ++iter){
		if((*iter) > max){
			max = (*iter);
			max_bin = bin;
		}
		bin += 1;
	}
	
	cout << "......max = " << max << endl;
	cout << "......bin = " << max_bin << endl;
	cout << "......time = " << GetTraceOffset() + GetBinDuration()*max_bin << endl;

	return max;
}
//-------------------------------------------------------------------//
//FGSII add 10/13/2014
double utl::Trace::GetMin(){
	cout << "...utl::Trace::GetMin() " << endl;
	double min = GetTimeBin(0);
	unsigned int bin = 0;
	unsigned int min_bin;
	for(TimeBinsIterator iter = TimeBinsBegin(); iter != TimeBinsEnd(); ++iter){
		if((*iter) < min){
			min = (*iter);
			min_bin = bin;
		}
		bin += 1;
	}
	
	cout << "......min = " << min << endl;
	cout << "......bin = " << min_bin << endl;
	cout << "......time = " << GetTraceOffset() + GetBinDuration()*min_bin << endl;

	return min;
}
//-------------------------------------------------------------------//
//FGSII add 10/15/2014 this will only work on a trace by trace basis:
//it is only valid to use this if a channel only contains one trace
void utl::Trace::DeconvolveACCoupling(double time_const){
  // Let's create an AC coupling template first:
  std::vector <double> ac_template;

  double current_bin = 0.0;
  double result = 0.0;
  double an_integral = 0.0;
  for (unsigned long ii = 0; ii < GetNofTimeBins(); ++ii){
	//for (unsigned int ii = begin_bin; ii < GetNofTimeBins(); ++ii){
  	if (ii == 0 ) {
		result = (1 - exp(-current_bin/time_const));
	} else {
		result = exp((GetBinDuration()-current_bin)/time_const)
			- exp(-current_bin/time_const);
	}
	ac_template.push_back(result);
	current_bin += GetBinDuration();
	an_integral += result;
  }
  
	cout << "...utl::Trace::DeconvolveACCoupling()" << endl;
	cout << "......Integral = "	<< an_integral << " (should be ~1)" << endl;
  

  for (unsigned long ii = 0; ii < GetNofTimeBins(); ++ii){
	//for (unsigned int ii = begin_bin; ii < GetNofTimeBins(); ++ii){
	result = fTimeBins[ii];
	for (unsigned int jj = ii; jj < GetNofTimeBins(); ++jj) {
		fTimeBins[jj] += result*ac_template[jj-ii];
	}
  }
}
//-------------------------------------------------------------------//
//FGSII add 10/31/2014 this will only work on a trace by trace basis:
//it is only valid to use this if a channel only contains one trace
void utl::Trace::DeconvolveACCouplingFSS(double time_const, unsigned int bin){
  // Let's create an AC coupling template first:
  std::vector <double> ac_template;

  double current_bin = 0.0;
  double result = 0.0;
  double an_integral = 0.0;
  for (unsigned long ii = 0; ii < GetNofTimeBins(); ++ii){
	//for (unsigned int ii = begin_bin; ii < GetNofTimeBins(); ++ii){
  	if (ii == 0 ) {
		result = (1 - exp(-current_bin/time_const));
	} else {
		result = exp((GetBinDuration()-current_bin)/time_const)
			- exp(-current_bin/time_const);
	}
	ac_template.push_back(result);
	current_bin += GetBinDuration();
	an_integral += result;
  }
  
	cout << "...utl::Trace::DeconvolveACCouplingFSS()" << endl;
	cout << "......Integral = "	<< an_integral << " (should be ~1)" << endl;
  
	//for now I have "bin-5" because my signal finding algorithm isn't that great
  for (unsigned int ii = (bin-5); ii < GetNofTimeBins(); ++ii){
	//for (unsigned int ii = begin_bin; ii < GetNofTimeBins(); ++ii){
	result = fTimeBins[ii];
	for (unsigned int jj = ii; jj < GetNofTimeBins(); ++jj) {
		fTimeBins[jj] += result*ac_template[jj-ii];
	}
  }
}

//-------------------------------------------------------------------//
//FGSII add 10/15/2014 this will only work on a trace by trace basis:
//it is only valid to use this if a channel only contains one trace
void utl::Trace::DeconvolveCurrent(double time_const){
	cout << "...utl::Trace::DeconvolveCurrent()" << endl;//fmod

  for (unsigned long ii = 0; ii < GetNofTimeBins()-1; ++ii){
	fTimeBins[ii] += time_const/GetBinDuration()*
		(fTimeBins[ii+1] - fTimeBins[ii]);
  }
}
//-------------------------------------------------------------------//
//FGSII add 10/31/2014 this will only work on a trace by trace basis:
//it is only valid to use this if a channel only contains one trace
void utl::Trace::DeconvolveCurrentFSS(double time_const, unsigned int bin){
	cout << "...utl::Trace::DeconvolveCurrent()" << endl;//fmod

  for (unsigned long ii = (bin-5); ii < GetNofTimeBins()-1; ++ii){
	fTimeBins[ii] += time_const/GetBinDuration()*
		(fTimeBins[ii+1] - fTimeBins[ii]);
  }
}
//-------------------------------------------------------------------//
void utl::Trace::Integrate(double mult_factor){
	cout << "...utl::Trace::Integrate" << endl;//fmod

  double result = 0.0;
  for (unsigned long ii = 0; ii < GetNofTimeBins(); ++ii){
  	result += fTimeBins[ii]*GetBinDuration();
	fTimeBins[ii] = result*mult_factor;
  }
}
//-------------------------------------------------------------------//
//FGSII add 10/17/2014
void utl::Trace::AddConst(double value){
	cout << "...utl::Trace::AddConst()" << endl;
	cout << "......const = " << value << endl;
	for (TimeBinsIterator iter = TimeBinsBegin(); iter != TimeBinsEnd(); ++iter) {
		(*iter) += value;
  }

}
//-------------------------------------------------------------------//







