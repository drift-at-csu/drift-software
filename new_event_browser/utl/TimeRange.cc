#include <utl/TimeRange.h>
using namespace drift;

//-------------------------------------------------------------------//
utl::TimeRange::TimeRange() :
	fMiddlePoint(0.0), fWidth(0.0) {
}
//-------------------------------------------------------------------//
utl::TimeRange::TimeRange(double begin, double end){
  SetInterval(begin,end);
}
//-------------------------------------------------------------------//
utl::TimeRange::~TimeRange() {
}
//-------------------------------------------------------------------//
void utl::TimeRange::SetInterval(double begin, double end){
  fMiddlePoint = (begin + end)/2.0;
  fWidth = (end - begin)/2.0;
}
//-------------------------------------------------------------------//
double utl::TimeRange::GetBegin() const{
  return fMiddlePoint - fWidth;
}
//-------------------------------------------------------------------//
double utl::TimeRange::GetEnd() const{
  return fMiddlePoint + fWidth;
}
//-------------------------------------------------------------------//
double utl::TimeRange::GetMiddle() const{
  return fMiddlePoint;
}
//-------------------------------------------------------------------//
bool utl::TimeRange::isWithin(double value){
  return ((value >= fMiddlePoint-fWidth)&&(value <= fMiddlePoint+fWidth));
}
//-------------------------------------------------------------------//
utl::TimeRange utl::TimeRange::operator=(const utl::TimeRange& tmr){
  fMiddlePoint = tmr.fMiddlePoint;
  fWidth = tmr.fWidth;
  return *this;
}
//-------------------------------------------------------------------//
bool utl::TimeRange::operator <(const TimeRange &tmr) const{
  return (fMiddlePoint < tmr.fMiddlePoint);
}
//-------------------------------------------------------------------//
