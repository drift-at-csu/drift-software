#include <utl/TimeStamp.h>
using namespace drift;

// #include <stdio.h>
// #include <stdlib.h>
#include <iostream>
using namespace std;

//-------------------------------------------------------------------//
utl::TimeStamp::TimeStamp():
	fGPSSecond(0), fGPSNanoSecond(0){
}
//-------------------------------------------------------------------//
utl::TimeStamp::TimeStamp(const long aSec, const long aNsec){
   SetNormalized(aSec, aNsec);
}
//-------------------------------------------------------------------//
utl::TimeStamp::~TimeStamp(){
}
//-------------------------------------------------------------------//
void utl::TimeStamp::SetGPSTime(const long aSec, const long aNsec){
  SetNormalized(aSec, aNsec);
}
//-------------------------------------------------------------------//
long utl::TimeStamp::GetGPSSecond() const{
  return fGPSSecond;
}
//-------------------------------------------------------------------//
long utl::TimeStamp::GetGPSNanoSecond() const{
  return fGPSNanoSecond;
}
//-------------------------------------------------------------------//
bool utl::TimeStamp::operator==(const utl::TimeStamp& ts) const{
  return (fGPSSecond == ts.fGPSSecond && fGPSNanoSecond == ts.fGPSNanoSecond);
}
//-------------------------------------------------------------------//
bool utl::TimeStamp::operator!=(const utl::TimeStamp& ts) const{
  return !operator==(ts);
}
//-------------------------------------------------------------------//
bool utl::TimeStamp::operator>(const utl::TimeStamp& ts) const{
  return (fGPSSecond > ts.fGPSSecond ||
	(fGPSSecond == ts.fGPSSecond && fGPSNanoSecond > ts.fGPSNanoSecond));
}
//-------------------------------------------------------------------//
bool utl::TimeStamp::operator>=(const utl::TimeStamp& ts) const{
  return !operator<(ts);
}
//-------------------------------------------------------------------//
bool utl::TimeStamp::operator<(const utl::TimeStamp& ts) const{
  return (fGPSSecond < ts.fGPSSecond ||
	(fGPSSecond == ts.fGPSSecond && fGPSNanoSecond < ts.fGPSNanoSecond));
}
//-------------------------------------------------------------------//
bool utl::TimeStamp::operator<=(const utl::TimeStamp& ts) const{
  return !operator>(ts);
}
//-------------------------------------------------------------------//
utl::TimeStamp utl::TimeStamp::operator=(const utl::TimeStamp& ts){
  SetNormalized( ts.fGPSSecond, ts.fGPSNanoSecond);
  return *this;
}
//-------------------------------------------------------------------//
utl::TimeStamp utl::TimeStamp::operator+(const utl::TimeInterval& ti){
  return TimeStamp(fGPSSecond + ti.GetSecond(),
  	fGPSNanoSecond + ti.GetNanoSecond());
}
//-------------------------------------------------------------------//
utl::TimeStamp& utl::TimeStamp::operator+=(const utl::TimeInterval& ti){
  SetNormalized(fGPSSecond + ti.GetSecond(),
	fGPSNanoSecond + ti.GetNanoSecond());
  return *this;
}
//-------------------------------------------------------------------//
utl::TimeInterval utl::TimeStamp::operator-(const utl::TimeStamp& ts){
  return TimeInterval( fGPSSecond - ts.fGPSSecond,
	fGPSNanoSecond - ts.fGPSNanoSecond);
}
//-------------------------------------------------------------------//
utl::TimeStamp utl::TimeStamp::operator-(const utl::TimeInterval& ti){
  return TimeStamp(fGPSSecond - ti.GetSecond(),
	fGPSNanoSecond - ti.GetNanoSecond());
}
//-------------------------------------------------------------------//
utl::TimeStamp& utl::TimeStamp::operator-=(const utl::TimeInterval& ti){
  SetNormalized(fGPSSecond - ti.GetSecond(),
  	fGPSNanoSecond - ti.GetNanoSecond());
  return *this;
}
//-------------------------------------------------------------------//
void utl::TimeStamp::SetNormalized(long sec, long nsec){
  // normalize
  const long sperns = 1000000000;
  while (nsec < 0){
  	--sec;
	nsec += sperns;
  }

  while (nsec >= sperns){
  	++sec;
	nsec -= sperns;
  }

  if (sec >= 0) {
	fGPSSecond = sec;
        fGPSNanoSecond = nsec;
  } else {
  	cout << "...TimeStamp Error: " << sec << "," << nsec << endl;
	fGPSSecond = fGPSNanoSecond = 0;
  }
}
//-------------------------------------------------------------------//
