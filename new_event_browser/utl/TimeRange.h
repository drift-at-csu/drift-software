#ifndef _DRIFT_utl_TimeRange_h_
#define _DRIFT_utl_TimeRange_h_

namespace drift {
namespace utl {
class TimeRange {
  public:
	TimeRange();
	TimeRange(double begin, double end);
	~TimeRange();

	void SetInterval(double begin, double end);
	double GetBegin() const;
	double GetEnd() const;
	double GetMiddle() const;

	/// Returns true if a value is within the interval
	bool isWithin(double value);

	TimeRange operator=(const TimeRange& tmr);
	bool operator <(const TimeRange &tmr) const;

  private:
  	double fMiddlePoint;
	double fWidth;

}; /* class TimeRange */
} /* namespace utl */
} /* namespace drift */
#endif /* _DRIFT_utl_TimeRange_h_ */
