#ifndef _DRIFT_EB_EventBrowserMenu_h_
#define _DRIFT_EB_EventBrowserMenu_h_

class TGWindow;
class TGCompositeFrame;
class TGMenuBar;
class TGLayoutHints;
class TGPopupMenu;

//FGSII add 09/23/2014
class TGTextButton;
class TGCheckButton;

class  EventBrowserMenu {
  public:
	EventBrowserMenu  (TGCompositeFrame* main);
	~EventBrowserMenu();

	//FGSII add 09/23/2014
	TGTextButton* GetSortButton1();
	TGCheckButton* GetSortButton2();
//	bool ToggleShowMCTraces();

  private:
	TGMenuBar* fMenuBar;

	TGLayoutHints* fMenuBarLayout;
	TGLayoutHints* fMenuBarItemLayout;
	TGLayoutHints* fMenuBarHelpLayout;

	TGPopupMenu* fMenuFile;
	TGPopupMenu* fMenuHelp;

	//FGSII add 09/23/2014
	TGLayoutHints* fSortButton1Layout;
	TGTextButton* fSortButton1;
	TGLayoutHints* fSortButton2Layout;
	TGCheckButton* fSortButton2;

}; /* class  EventBrowserMenu */

#endif /* _DRIFT_EB_EventBrowserMenu_h_ */
