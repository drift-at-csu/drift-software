#include "TGTextMargin.h"

#include <TGNumberEntry.h>
#include <TGLabel.h>

//-------------------------------------------------------------------//
TGTextMargin::TGTextMargin(const TGWindow *p, const char *name) : 
		TGHorizontalFrame(p) {

  fEntry = new TGNumberEntry(this, 0, 6, -1, TGNumberFormat::kNESInteger);
  AddFrame(fEntry, new TGLayoutHints(kLHintsLeft));
  TGLabel *label = new TGLabel(this, name);
  AddFrame(label, new TGLayoutHints(kLHintsLeft, 10));
}
//-------------------------------------------------------------------//
TGNumberEntryField *TGTextMargin::GetEntry() { 
  return fEntry->GetNumberEntry(); 
}
//-------------------------------------------------------------------//
