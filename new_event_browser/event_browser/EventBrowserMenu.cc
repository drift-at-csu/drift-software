#include "EventBrowserMenu.h"
#include "EventBrowserSignals.h"
#include "EventBrowser.h"

#include <TGClient.h>
#include <TGFrame.h>
#include <TGWindow.h>
#include <TGMenu.h>
#include <TGLayout.h>

//FGSII add 09/23/20114
#include <TGButton.h>

#include <iostream>
using namespace std;

//-------------------------------------------------------------------//
EventBrowserMenu::EventBrowserMenu(TGCompositeFrame* main) {
  // menu layouts
  fMenuBarLayout = new TGLayoutHints(kLHintsTop | kLHintsLeft | kLHintsExpandX,
				     0, 0, 1, 1);
  fMenuBarItemLayout = new TGLayoutHints(kLHintsTop | kLHintsLeft, 0, 4, 0, 0);
  fMenuBarHelpLayout = new TGLayoutHints(kLHintsTop | kLHintsRight);

  // for the Menu
  fMenuFile = new TGPopupMenu(gClient->GetRoot());
  fMenuFile->AddEntry("Open file ...", eFileOpen);
  fMenuFile->AddSeparator();
  
  fMenuFile->AddEntry("Open live ...", eFileLive);//fmod added

  fMenuFile->AddSeparator();//fmod added
  
  
  fMenuFile->AddEntry("Quit", eFileQuit);

  // for some help
  fMenuHelp = new TGPopupMenu(gClient->GetRoot());
  fMenuHelp->AddEntry("About", eAbout);

  fMenuFile->Associate(main);
  fMenuHelp->Associate(main);


  fMenuBar = new TGMenuBar(main);
  fMenuBar->AddPopup("File", fMenuFile, fMenuBarItemLayout);
  fMenuBar->AddPopup("Help", fMenuHelp, fMenuBarHelpLayout);
  fMenuBar->DrawBorder();

  main->AddFrame(fMenuBar, fMenuBarLayout);

	//FGSII add 09/23/2014
	//going to add some buttons that will be used to sort events chronologically
	fSortButton1Layout = new TGLayoutHints(kLHintsTop | kLHintsRight, 0, 0, 1, 1);
	fSortButton1 = new TGTextButton(main,"Sort");
	main->AddFrame(fSortButton1,fSortButton1Layout);
	fSortButton2Layout = new TGLayoutHints(kLHintsTop | kLHintsRight, 0, 0, 1, 1);
	fSortButton2 = new TGCheckButton(main, "Apply Sort");
	main->AddFrame(fSortButton2,fSortButton2Layout);
	fSortButton2->SetEnabled(kFALSE);

}
//-------------------------------------------------------------------//
EventBrowserMenu::~EventBrowserMenu() {
  delete fMenuBar;
}
//-------------------------------------------------------------------//
//FGSII add 09/23/2014
TGTextButton* EventBrowserMenu::GetSortButton1(){
	return fSortButton1;
}
//-------------------------------------------------------------------//
//FGSII add 09/23/2014
TGCheckButton* EventBrowserMenu::GetSortButton2(){
	return fSortButton2;
}
//-------------------------------------------------------------------//

