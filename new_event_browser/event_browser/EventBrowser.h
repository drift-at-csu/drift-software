#include <TGFrame.h>

class TGWindow;
class TGTab;
class TGCompositeFrame;

class EventBrowserMenu;
class StatusBar;

class MWPCPlots;
class VPlots;

#include <string>
#include <vector>

//fmod added 06/05/2014
const unsigned int fgMaxNumChans = 48;

namespace drift{
namespace evt{
  class Event;
}
namespace det{
  class Detector;
}
namespace rec{
  class Reconstruct;
}
}
using namespace drift;

class EventBrowser : public TGMainFrame {

  private:
  	EventBrowser();
  public: 
	
		//FGSII add 09/22/2014
		//this type is to be used in a vector where events can be mapped into a chronological
		//order for loading
		//EventChronPair => <event_num_in_file, <sec,nsec> >
		typedef std::pair<unsigned int, std::pair<long,long> > EvtChronType; //a user defined type for mapping
		typedef std::vector<EvtChronType> EvtChronContainer;
		typedef EvtChronContainer::iterator EvtChronIterator;
		typedef EvtChronContainer::const_iterator ConstEvtChronIterator;
		void MakeEvtChron(unsigned int event_in_file, long sec, long nsec);
		//bool CompareEvtChrons(EvtChronType evt1, EvtChronType evt2);
		EvtChronType GetEvtChron(unsigned int evt_num);
		void SortEvtChrons();
		void ClearEvtChrons();
		EvtChronIterator EvtChronBegin();
		EvtChronIterator EvtChronEnd();
		ConstEvtChronIterator EvtChronBegin() const;
		ConstEvtChronIterator EvtChronEnd() const;
		void CreateEventSortingMap();

		//FGSII add 10/05/2014
		bool EventIsSpark();


		EventBrowser(const TGWindow * p, UInt_t w, UInt_t h,
		det::Detector* det_descr, rec::Reconstruct *reconstructor
		);//fmod version of EventBrowser
	
	virtual ~EventBrowser();

	// SYSTEM ENTRY for signal handling
	Bool_t ProcessMessage(Long_t msg, Long_t parm1, Long_t parm2); // +
	Bool_t HandleKey(Event_t *event); // +
	void CloseWindow(); // + do the cleaning up
  private:
	void UpdateBrowser(); // +
	void UpdateTab(long int); // +
	//void ReadEventList();
	void InitKeyStrokes(); // +
	
  public:
	bool ChangeEvent(Long_t);

	//FGSII add 07/31/2014
	void HandleTracesCanvasEvents(Int_t event, Int_t px, Int_t py, TObject* sel);
	
  private:
  void OpenDRTFile(std::string file_name);
  void OpenDRTFileLive(std::string live_file);//fmod added

	//void ChangeTab(Long_t);//fmod commented out, not being used currently
	void UpdateTabsOnOpenFile();
  private:
  	// Event Stuff
	evt::Event*		fEvent;
	int 			fTotEvent; // total no of events in file
	int			fCurrentEventNumber;
	std::string fFileName;//fmod added
	// Detector Stuff
	det::Detector*		fDetector;
	// Reconstruction Stuff
	rec::Reconstruct*	fReconstr;
    
	// GUI stuff
	EventBrowserMenu*	fMenu;
	StatusBar*		fStatusBar;

	// --- the plots inside the tabs
	MWPCPlots*		fMWPCPlots;
	MWPCPlots* fMWPCReconPlots; //FGSII add 08/14/2014
	TGCompositeFrame*	fMWPCFrame;
	unsigned int 		fMWPCTabIndex;
	//FGSII add 07/30/2014
	TGCompositeFrame* fReconFrame;

	//FGSII add 09/22/2014
	EvtChronContainer fEvtChrons;
	
	

	// Tabs
	std::vector<VPlots*> 	fPlotVector;
	TGTab*			fTabs;
  public:
	static std::string GetVersion();
	static std::string GetRevision();
	static std::string GetDate();
	static void Welcome();

  ClassDef (EventBrowser,1);
};

//must be a static function existing outside the EventBrowser Class
bool CompareEvtChrons(EventBrowser::EvtChronType evt1, EventBrowser::EvtChronType evt2);
