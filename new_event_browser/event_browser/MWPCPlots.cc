#include "MWPCPlots.h"
#include "EventBrowserSignals.h"
#include "EventBrowserConst.h"

#include <TFrame.h> //FGSII add 07/24/2014 for ::MakePave()
#include <TGFrame.h>
#include <TGSlider.h>
#include <TRootEmbeddedCanvas.h>
#include <TCanvas.h>
#include <TGTab.h>
#include <TGraphErrors.h>
#include <TGListBox.h>
#include <TGNumberEntry.h>
#include <TLegend.h>
#include <TLatex.h>
#include <TGLabel.h>
#include <TGaxis.h>
#include <TSpline.h>
#include <TH2F.h>
#include <TSystem.h>
#include <TPad.h> //FGSII add 07/16/2014
#include <TQObject.h> //FGSII add on 07/28/2014
#include <TLine.h>//FGSII add on 09/16/2014 to draw signal region lines
#include <TF1.h>//FGSII add 09/30/2014

#include <TGTextMargin.h>

#include <cmath>
#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdlib>
#include <algorithm>
#include <iomanip>
#include <string> //FGSII added 06/02/2014
using namespace std;

using namespace view::Consts;

using namespace drift;
#include <evt/Event.h>
#include <evt/Channel.h>
#include <evt/WirePair.h> //FGSII add 10/20/2014
#include <det/Detector.h>
#include <det/Channel.h>
#include <utl/Trace.h>
#include <rec/Reconstruct.h>

ClassImp (MWPCPlots);
//-------------------------------------------------------------------//
MWPCPlots::MWPCPlots(TGCompositeFrame* main, det::Detector *det_descr):
	fDetector(det_descr),
	fReconstr(NULL), //stays NULL not used since no recon used for this constructor
	fEvent(NULL),
	fVariablesTab(NULL), fChannelsListBox(NULL), fCanvTraces(NULL), 
	fOptVertOffsetNumber(NULL),
	fChannelsDrawingOffset(-20.0), fCurrentSelectedChannel(0),
	fReconstructionLevel(1), //this stays 1 and does not change
	fAxisHisto(NULL),
	fPreviousEventNumber(0),//FGSII added 07/29/2014
	fZeroLine(NULL) //FGSII add 10/30/2014
	{

	const double width  = main->GetWidth();
	const double height = main->GetHeight();

	//cout << "...Creating MWPC Plots in a Tab:" << width << "x" << height << endl; //FGSII comment out 08/27/2014
	cout << "...Creating MWPC Plots in a Tab (MWPC Raw Traces):" << width << "x" << height << endl;//FGSII add 08/27/2014

	//------ Hints -------//
	TGLayoutHints* mainLayout = new TGLayoutHints(
	kLHintsExpandX, 1,1,1,1);
  // TGLayoutHints* centeredLayout= new TGLayoutHints(
  //	kLHintsCenterX|kLHintsCenterY, 1,1,1,1);
  // TGLayoutHints* centeredXLayout= new TGLayoutHints(
  //	kLHintsCenterX,1,1,1,1);
  /* TGLayoutHints* centeredExpandXYLayout = */ new TGLayoutHints(
	kLHintsExpandX|kLHintsExpandY|kLHintsCenterX|kLHintsCenterY,1,1,1,1);
  TGLayoutHints* expandXYLayout = new TGLayoutHints(kLHintsExpandX|
	kLHintsExpandY,1,1,1,1);
  TGLayoutHints* expandYLayout = new TGLayoutHints(
	kLHintsExpandY,1,1,1,1);
  //------ End of layout hints --------//
  //-------- Frames -------------------//
  TGHorizontalFrame* mainHorTopFrame = new TGHorizontalFrame(main,
	UInt_t(width), UInt_t(0.78*height));//
  TGHorizontalFrame* mainHorBotFrame = new TGHorizontalFrame(main,
  	UInt_t(width), UInt_t(0.22*height));
  main->AddFrame(mainHorTopFrame, mainLayout);
  main->AddFrame(mainHorBotFrame, mainLayout);

  cout << "...MWPC: Created 2 Frames:" << mainHorTopFrame->GetWidth() << "x"
  	<< mainHorTopFrame->GetHeight()  << " and " 
	<< mainHorBotFrame->GetWidth() << "x" << mainHorBotFrame->GetHeight() 
	<< endl;

  //--------- Canvas in which traces are drawn --------------//
  TGVerticalFrame* tracesFrame = new TGVerticalFrame(mainHorTopFrame,
	UInt_t(1.1*mainHorTopFrame->GetWidth()), mainHorTopFrame->GetHeight());//FGSII 07/18/2014, width was 0.7*
  mainHorTopFrame->AddFrame(tracesFrame, expandYLayout);
  fCanvTraces = new TRootEmbeddedCanvas(
  		"tracesCanvas", tracesFrame,
		tracesFrame->GetWidth(), tracesFrame->GetHeight());
  tracesFrame->AddFrame(fCanvTraces, expandXYLayout);

	//FGSII add 07/28/2014
	fCanvTraces->GetCanvas()->Connect("ProcessedEvent(Int_t,Int_t,Int_t,TObject*)", "MWPCPlots", this, "HandleCanvEvents(Int_t,Int_t,Int_t,TObject*)");

  //---------- This is a list of channels in the event -------//
  TGVerticalFrame* channelsListFrame = new TGVerticalFrame(mainHorTopFrame,
	UInt_t(0.3*mainHorTopFrame->GetWidth()), mainHorTopFrame->GetHeight());
  mainHorTopFrame->AddFrame(channelsListFrame, expandYLayout);
  TGTab* fChannelsListTab = new TGTab(channelsListFrame,
  	channelsListFrame->GetWidth(), channelsListFrame->GetHeight());
  channelsListFrame->AddFrame(fChannelsListTab, expandYLayout);

  TGCompositeFrame *channelsInfoFrame = fChannelsListTab->AddTab("Channels");

  fChannelsListBox = new TGListBox(channelsInfoFrame, 49);
  fChannelsListBox->Connect("Selected(Int_t)", "MWPCPlots",
  	this, "SelectChannel()");
  channelsInfoFrame->AddFrame(fChannelsListBox, expandXYLayout);

  //------------ Variables Tabs  ----------//
  fVariablesTab = new TGTab(mainHorBotFrame,
	mainHorBotFrame->GetWidth(), mainHorBotFrame->GetHeight());
  mainHorBotFrame->AddFrame(fVariablesTab, 
  	new TGLayoutHints(kLHintsTop|kLHintsExpandX,1,1,1,1));

  TGCompositeFrame *optFrame = fVariablesTab->AddTab("Options");
  optFrame->ChangeOptions(optFrame->GetOptions() | kHorizontalFrame);

  fOptVertOffsetNumber = new TGTextMargin(optFrame, "Vertical Offset");
  optFrame->AddFrame(fOptVertOffsetNumber,
	new TGLayoutHints(kLHintsTop|kLHintsExpandX, 3, 2, 2, 2));
  fOptVertOffsetNumber->GetEntry()->Connect("TextChanged(char*)", "MWPCPlots",
	this, "DoVerticalOffset()");
  fOptVertOffsetNumber->GetEntry()->SetNumber(fChannelsDrawingOffset);

	//FGSII add 10/27/2014
	//adding a check button so we can load from wires instead of channels
	fLoadFromWirePairsButton = new TGCheckButton(optFrame, "Load From Wires");
	optFrame->AddFrame(fLoadFromWirePairsButton, new TGLayoutHints(kLHintsTop|kLHintsExpandX, 3, 2, 2, 2));
	fLoadFromWirePairsButton->SetOn(false);
	fLoadFromWirePairsButton->SetEnabled(kFALSE);


}
//-------------------------------------------------------------------//

//new code
MWPCPlots::~MWPCPlots(){

	for (evt::Event::ConstChannelIterator iter1 = fEvent->ChannelsBegin();iter1 != fEvent->ChannelsEnd(); ++iter1) {
		for(evt::Channel::ConstTraceIterator iter2 = fEvent->GetChannel((*iter1)->GetId())->TracesBegin(); iter2 != fEvent->GetChannel((*iter1)->GetId())->TracesEnd(); iter2++){
			if(fRawHistoVector[IdPair((*iter1)->GetId(),(*iter2)->GetTraceId())] != NULL){
				delete fRawHistoVector[IdPair((*iter1)->GetId(),(*iter2)->GetTraceId())];
				fRawHistoVector[IdPair((*iter1)->GetId(),(*iter2)->GetTraceId())] = NULL;
			}
		}
	}

//FGSII add 10/21/2014 need to deconstruct fRawHistoVector filled from WirePairs
	/*for(evt::Event::ConstWirePairIterator iter1 = fEvent->WirePairsBegin(); iter1 != fEventWirePairsEnd(); ++iter){
		}*/

}
//-------------------------------------------------------------------//
bool MWPCPlots::ChangeEvent(drift::evt::Event* an_event){
	cout << "...MWPCPlots::ChangeEvent" << endl;

	//first let's set fEvent
	fEvent=an_event;

	//Let's clear all reconstruction levels only if fReconstr exists
	if(fReconstr != NULL){
	fReconstr->ClearPedestal(); //FGSII 09/03/2014
	fReconstr->ClearFirstAC(); //FGSII add 10/15/2014
	fReconstr->ClearSecondAC();
	fReconstr->ClearCurrent();
	fReconstr->ClearCharge();
	}

	//Let's assign an Event to the reconstruction chain
	//make sure fReconstr exists, if not this probably means
	//the raw traces object is calling this for which fReconstr is NULL
	if(fReconstr != NULL){
		cout << "......fReconstr exists" << endl;
		fReconstr->SetEvent(an_event);
	}

	if (fReconstructionLevel == 1) {
		if( (fReconstr != NULL) && (fLoadFromWirePairsButton->IsOn()) ){ 
			TracesFillFromRawEventWirePairs();
		}
		else{
			TracesFillFromRawEvent();
		}
	} 
	else if(fReconstructionLevel == 2) { //FGSII add 09/11/2014
		if( (fReconstr != NULL) && (fLoadFromWirePairsButton->IsOn()) ){
			ReconUpdateSignalRegionWirePairs();
			TracesFillFromRawEventWirePairs();
		}
		else{
			ReconUpdateSignalRegion();
			TracesFillFromRawEvent();
		}
	}
	else if (fReconstructionLevel == 3) {
		if( (fReconstr != NULL) && (fLoadFromWirePairsButton->IsOn()) ){
			ReconUpdateSignalRegionWirePairs();
			ReconUpdatePedestalWirePairs();
			TracesFillFromPedestalWirePairs();
		}
		else{
			ReconUpdateSignalRegion();
			ReconUpdatePedestal();
			TracesFillFromPedestal();
		}
	}
	else if (fReconstructionLevel == 4) {
		if( (fReconstr != NULL) && (fLoadFromWirePairsButton->IsOn()) ){
			ReconUpdateFirstACWirePairs();
			TracesFillFromFirstACWirePairs();
		}
		else{
			ReconUpdateFirstAC();//FGSII uncomment 10/15/2014
			TracesFillFromFirstAC();//FGSII uncomment 10/15/2014
		}
	} 
	else if (fReconstructionLevel == 5) {
		if( (fReconstr != NULL) && (fLoadFromWirePairsButton->IsOn()) ){
			ReconUpdateSecondACWirePairs();
			TracesFillFromSecondACWirePairs();
		}
		else{
			ReconUpdateSecondAC();
			TracesFillFromSecondAC();
		}
	} 
	else if (fReconstructionLevel == 6) {
		if( (fReconstr != NULL) && (fLoadFromWirePairsButton->IsOn()) ){
			ReconUpdateCurrentWirePairs();
			TracesFillFromCurrentWirePairs();
		}
		else{
			ReconUpdateCurrent();
			TracesFillFromCurrent();
		}
	} 
	else if (fReconstructionLevel == 7) {
		if( (fReconstr != NULL) && (fLoadFromWirePairsButton->IsOn()) ){
			ReconUpdateChargeWirePairs();
			TracesFillFromChargeWirePairs();
		}
		else{
			ReconUpdateCharge();
			TracesFillFromCharge();
		}
	} 
	else {
		if( (fReconstr != NULL) && (fLoadFromWirePairsButton->IsOn()) ){
			//TracesFillFromRawEventWirePairs();
		}
		else{
			TracesFillFromRawEvent();
		}
	}

	//LoadChanRecValues();
	TracesUpdateOnCanvas(fChannelsDrawingOffset);

	return true;
}
//-------------------------------------------------------------------//
Bool_t MWPCPlots::ProcessMessage (Long_t msg, Long_t parm1, Long_t /*parm2*/) {
	switch (GET_MSG (msg)) {
		case kC_COMMAND:
			switch (GET_SUBMSG (msg)) {
				case kCM_TAB: {
					// if ( parm1 == 0 )
					// fCanvasInfo->Update();
				}
				default:
				break;
			}
			default:
			break;
	}
  
	return kTRUE;
}
//-------------------------------------------------------------------//
void MWPCPlots::SelectChannel(){
	cout << "\n...MWPCPlots::SelectChannel" << endl;
	int channel_number= fChannelsListBox->GetSelected();
	cout << "......Channel number " << channel_number << " selected" << endl;

	fCurrentSelectedChannel = channel_number;
	TracesUpdateOnCanvas(0.0);
	//TracesUpdateOnCanvas(fChannelsDrawingOffset);

	//FGSII 08/26/2014 need to add stuff here so channel is selected if we are in rec tab
	//and this will enable the electronics and rec tabs
}
//-------------------------------------------------------------------//
void MWPCPlots::DoVerticalOffset(){
	cout << "...MWPCPlots::DoVerticalOffset" << endl;
	if ( !fOptVertOffsetNumber ){
		cout << "...No Option: VertOffset" << endl;
		return;
	}
	if ( !fOptVertOffsetNumber->GetEntry()){
		cout << "...No Option: GetEntry" << endl;
		return;
	}

	TracesUpdateOnCanvas(fOptVertOffsetNumber->GetEntry()->GetNumber() - fChannelsDrawingOffset);
	fChannelsDrawingOffset = fOptVertOffsetNumber->GetEntry()->GetNumber();

	cout << "......Changed Vertical Offset to \"" << fChannelsDrawingOffset << "\"" << endl;
	cout << endl;
}
//-------------------------------------------------------------------//
void MWPCPlots::TracesFillFromRawEvent(){
	cout << "...MWPCPlots::TracesFillFromRawEvent" << endl;
	// Let's Clean the prev histos
	//FGSII added next four lines on 07/21/2014 to fix mem leak problem
	for (std::map<IdPair,TH1D*>::iterator iter = fRawHistoVector.begin();
		iter != fRawHistoVector.end(); ++iter) {
		delete iter->second;
	}
	fRawHistoVector.clear();
	if (fEvent == NULL) {
		cout << "......Event is NULL" << endl;
		return;
	}

	// Let's Clean the previous fChannelsListbox FGSII add 07/08/2014
	fChannelsListBox->RemoveAll();
	//fill the listbox
	for (evt::Event::ConstChannelIterator iter = fEvent->ChannelsBegin(); iter != fEvent->ChannelsEnd(); ++iter) {
		//fill fChannelsListBox FGSII add 07/08/2014
		TString chan_name = "Ch "; //FGSII
		chan_name += (*iter)->GetId();
  		fChannelsListBox->AddEntrySort(chan_name, (*iter)->GetId());
		//fChannelsListBox->MapSubwindows();
		fChannelsListBox->Layout();
	}


	for (evt::Event::ConstChannelIterator iter1 = fEvent->ChannelsBegin();iter1 != fEvent->ChannelsEnd(); ++iter1) {
		for(evt::Channel::ConstTraceIterator iter2 = fEvent->GetChannel((*iter1)->GetId())->TracesBegin(); iter2 != fEvent->GetChannel((*iter1)->GetId())->TracesEnd(); iter2++){	

			//set histo name
			TString histo_name = "chan_";
			histo_name += (Int_t) (*iter1)->GetId(); 
			histo_name += "_trace_";
			histo_name += (Int_t) (*iter2)->GetTraceId();

			//FGSII mod on 08/27/2014 
			//these conditions were added to fix potential mem leak created by
			//the raw histos being overwritten by the recon histos
			if(fReconstr == NULL){
				histo_name += "_raw_histo";
			}
			else if(fReconstr != NULL){
				histo_name += "_recon_histo";
			}

			//set histo title
			TString histo_title = " Channel ";
			histo_title += "Raw Trace "; 
			//FGSII added 05/29/2014
			histo_title += "(Event "; histo_title += (Int_t) fEvent->GetEventNumber();
			histo_title += ") ";
			
			//FGSII 07/18/2014 fixed way that the file name is added to histo title
			string temp_string = fEvent->GetFileName();
			string string_key = ("/");
			unsigned int key_position = temp_string.rfind(string_key);
			string file_name = temp_string.substr(key_position+1,std::string::npos);
			histo_title += TString(file_name);

			fRawHistoVector.insert(std::pair<IdPair,TH1D*> (IdPair((*iter1)->GetId(), (*iter2)->GetTraceId()),
new TH1D(histo_name, histo_title,(*iter2)->GetNofTimeBins(), (*iter2)->GetTraceOffset(),(*iter2)->GetBinDuration()*(*iter2)->GetNofTimeBins() + (*iter2)->GetTraceOffset() ) ) );

			for (unsigned int ii = 0; ii < (*iter2)->GetNofTimeBins(); ++ii){
				//original code
				fRawHistoVector[IdPair((*iter1)->GetId(),(*iter2)->GetTraceId())]->SetBinContent( ii+1,(*iter2)->GetTimeBin(ii) ); 
			}
		}
	}
}
//-------------------------------------------------------------------//
void MWPCPlots::TracesUpdateOnCanvas(double vertical_offset){
	cout << "...MWPCPlots::TracesUpdateOnCanvas" << endl;

	if(fEvent != NULL && fPreviousEventNumber == 0){
		fPreviousEventNumber = fEvent->GetEventNumber();	
	}

	//here we add a statement to be able to preserve the x/y scales
	//if we are zoomed in on a plot
	if(fAxisHisto != NULL && fAxisHisto->TestBit(TH1::kIsZoomed) == 1 && fEvent->GetEventNumber()==
		fPreviousEventNumber){
			fAxisHisto->GetYaxis()->SetRangeUser(fCanvTraces->GetCanvas()->GetFrame()->GetY1(),
				fCanvTraces->GetCanvas()->GetFrame()->GetY2());
			fAxisHisto->GetXaxis()->SetRangeUser(fCanvTraces->GetCanvas()->GetFrame()->GetX1(),
				fCanvTraces->GetCanvas()->GetFrame()->GetX2());
			for (std::map<IdPair,TH1D*>::iterator iter = fRawHistoVector.begin();
				iter != fRawHistoVector.end(); ++iter) {
				iter->second->Draw("SAME ][");
				if ( fCurrentSelectedChannel == iter->first.first ) {
					iter->second->SetLineWidth(1.0);
					iter->second->SetLineColor(2);
				} 
				else {
					iter->second->SetLineWidth(1.0);
					iter->second->SetLineColor(4);
				}
			}
			fCanvTraces->GetCanvas()->Update();
	}
	else{//else***

		//clean previous fAxisHisto
		if (fAxisHisto != NULL){
			delete fAxisHisto;
			fAxisHisto = NULL;
		}

		//clear fPaves (a container the holds the labels for each channel)
		for (ConstPaveIterator iter = PavesBegin(); iter != PavesEnd(); ++iter){
			if( (*iter) != NULL){
				delete (*iter);
			}
		}
		ClearPaves();

		//need to clear previous ped region lines
		for(ConstSigLinesIterator iter = SigLinesBegin(); iter != SigLinesEnd(); ++iter){
			if( (*iter) != NULL){
				delete (*iter);
			}
		}
		ClearSigLines();

		//FGSII add 10/30/2014
		//if fZeroLine is not NULL delete it
		if(fZeroLine != NULL){
			delete fZeroLine;
			fZeroLine = NULL;
		}

		//make sure canvas is active and clear
		fCanvTraces->GetCanvas()->cd();
		fCanvTraces->GetCanvas()->Clear();

		double vert_max_value = 0.0;
		double vert_min_value = 0.0;
		double horiz_min_value = 0.0;
		double horiz_max_value = 0.0;

		double current_trace_offset = 0.0;
		bool init_values = true;
		
		//First create histos
		for (std::map<IdPair,TH1D*>::iterator iter = fRawHistoVector.begin();
				iter != fRawHistoVector.end(); ++iter) {
			current_trace_offset = (double) vertical_offset * iter->first.first;
		
			for(unsigned long ii = 0; ii < (unsigned int)iter->second->GetNbinsX(); ++ii){
				iter->second->SetBinContent(ii+1, iter->second->GetBinContent(ii+1) +
					current_trace_offset);
			}
			if (init_values){
				vert_max_value = iter->second->GetBinContent(iter->second->GetMaximumBin());
				vert_min_value = iter->second->GetBinContent(iter->second->GetMinimumBin());
				horiz_max_value = iter->second->GetXaxis()->GetXmax();
				horiz_min_value = iter->second->GetXaxis()->GetXmin();
				init_values = false;
			} 
			else {
				if (vert_max_value < iter->second->GetBinContent(iter->second->GetMaximumBin())){
					vert_max_value = iter->second->GetBinContent(iter->second->GetMaximumBin());
				}
				if (vert_min_value > iter->second->GetBinContent(iter->second->GetMinimumBin())){
					vert_min_value = iter->second->GetBinContent(iter->second->GetMinimumBin());
				}
				if ( horiz_max_value < iter->second->GetXaxis()->GetXmax() ){
					horiz_max_value = iter->second->GetXaxis()->GetXmax();
				}
				if ( horiz_min_value > iter->second->GetXaxis()->GetXmin() ){
					horiz_min_value = iter->second->GetXaxis()->GetXmin();
				}
			}	
		}

		fVertOffset = vertical_offset;

		//Here we set up the frame in which the histos will be drawn
		if (fRawHistoVector.size() > 0){
			TString histo_title = "";
			histo_title += "(Event "; histo_title += (Int_t) fEvent->GetEventNumber();
			histo_title += ") ";
			histo_title += "(";
			histo_title += (Int_t)(fEvent->GetHeader().GetTime().GetGPSSecond());
			histo_title += ",";
			histo_title += (Int_t)(fEvent->GetHeader().GetTime().GetGPSNanoSecond());
			histo_title += ") ";
			string temp_string = fEvent->GetFileName();
			string string_key = ("/");
			unsigned int key_position = temp_string.rfind(string_key);
			string file_name = temp_string.substr(key_position+1,std::string::npos);
			histo_title += TString(file_name);

			fAxisHisto = fCanvTraces->GetCanvas()->DrawFrame(
				horiz_min_value, vert_min_value,
				horiz_max_value, vert_max_value);
			fAxisHisto->SetTitle(histo_title);
			
			fAxisHisto->SetXTitle("time (#mus)");
	
			if(fReconstructionLevel == 1 || fReconstructionLevel == 2){ //FGSII add 09/11/2014
				fAxisHisto->SetYTitle("raw signal (mV)");
			}
			else if(fReconstructionLevel == 3){
				fAxisHisto->SetYTitle("pedestal subtracted signal (mV)");
			}
			else if(fReconstructionLevel == 4){
				fAxisHisto->SetYTitle("signal after 1st AC stage (mV)");
			}
			else if(fReconstructionLevel == 5){
				fAxisHisto->SetYTitle("signal after 2nd AC stage (mV)");
			}
			else if(fReconstructionLevel == 6){
				fAxisHisto->SetYTitle("current at wire (nA)");
			}
			else if(fReconstructionLevel == 7){
				fAxisHisto->SetYTitle("charge (electrons)");
			}
			fAxisHisto->GetYaxis()->SetRangeUser(vert_min_value, vert_max_value);
			fAxisHisto->GetYaxis()->SetRange(vert_min_value, vert_max_value);
		}

		//Here we draw the histos
		for (std::map<IdPair,TH1D*>::iterator iter = fRawHistoVector.begin();
				iter != fRawHistoVector.end(); ++iter) {
			iter->second->Draw("SAME ][");
			if ( fCurrentSelectedChannel == iter->first.first ) {
				iter->second->SetLineWidth(1.0);
				iter->second->SetLineColor(2);
			} 
			else {
				iter->second->SetLineWidth(1.0);
				iter->second->SetLineColor(4);
			}
		}

		//how to fill paves from channels
		if(fEvent != NULL && fLoadFromWirePairsButton->IsOn() == false){
			evt::Event::ConstChannelIterator chan_begin;
			evt::Event::ConstChannelIterator chan_end;
			if(fEvent != NULL){
			if(fReconstructionLevel == 1){
				chan_begin = fEvent->ChannelsBegin();
				chan_end = fEvent->ChannelsEnd();
			}
			else if(fReconstructionLevel == 2){ //FGSII add 09/11/2014
				chan_begin = fEvent->ChannelsBegin();
				chan_end = fEvent->ChannelsEnd();
			}
			else if(fReconstr != NULL && fReconstructionLevel == 3){
				chan_begin = fReconstr->ChanPedestalBegin();
				chan_end = fReconstr->ChanPedestalEnd();
			}
			else if(fReconstructionLevel == 4){
				//FGSII add 10/15/2014
				chan_begin = fReconstr->ChanFirstACBegin();
				chan_end = fReconstr->ChanFirstACEnd();
				//cout << "......Rec Level 4 not yet available" << endl;
			}
			else if(fReconstructionLevel == 5){
				chan_begin = fReconstr->ChanSecondACBegin();
				chan_end = fReconstr->ChanSecondACEnd();
				//cout << "......Rec Level 6 not yet available" << endl;
			}
			else if(fReconstructionLevel == 6){
				chan_begin = fReconstr->ChanCurrentBegin();
				chan_end = fReconstr->ChanCurrentEnd();
				//cout << "......Rec Level 7 not yet available" << endl;
			}
			else if(fReconstructionLevel == 7){
				chan_begin = fReconstr->ChanChargeBegin();
				chan_end = fReconstr->ChanChargeEnd();
				//cout << "......Rec Level 7 not yet available" << endl;
			}
			else{
				cout << "......ERROR: Rec level does not exist" << endl;
			}
		}
		//FGSII: Now we need to create a Pave (label) for each channel,
		//the label will go with the trace with greatest offset
		//problems here if offsets are ever negative
		double temp_offset=0;
		unsigned int pave_trace_id;
		double x1,y1;
		if(fRawHistoVector.size() != 0){
			for(evt::Event::ConstChannelIterator iter1 = chan_begin; iter1 != chan_end; ++iter1){
				temp_offset = 0;	
				for(evt::Channel::ConstTraceIterator iter2 = (*iter1)->TracesBegin();
				iter2 != (*iter1)->TracesEnd(); ++iter2){
					if((*iter2)->GetTraceOffset() >= temp_offset){
						temp_offset = (*iter2)->GetTraceOffset();
						pave_trace_id = (*iter2)->GetTraceId();
					}
				}
				x1 = fRawHistoVector[IdPair((*iter1)->GetId(),pave_trace_id)]->GetXaxis()->GetXmax();
				y1 = fRawHistoVector[IdPair((*iter1)->GetId(),pave_trace_id)]->
				GetBinContent(fRawHistoVector[IdPair((*iter1)->GetId(),pave_trace_id)]->GetNbinsX());
				MakePave((*iter1)->GetId(), vertical_offset,horiz_min_value, horiz_max_value, 
				vert_min_value, vert_max_value, x1, y1);
			}
		}
	}

	//How to fill paves from wires
	if(fEvent != NULL && fLoadFromWirePairsButton->IsOn() == true){
		evt::Event::ConstWirePairIterator wires_begin;
		evt::Event::ConstWirePairIterator wires_end;
		evt::Event::ConstChannelIterator chan_begin;//FGSII add 11/03/2014
		evt::Event::ConstChannelIterator chan_end;//FGSII add 11/03/2014
		if(fEvent != NULL){
			if(fReconstructionLevel == 1){
				wires_begin = fEvent->WirePairsBegin();
				wires_end = fEvent->WirePairsEnd();
			}
			else if(fReconstructionLevel == 2){ //FGSII add 09/11/2014
				wires_begin = fEvent->WirePairsBegin();
				wires_end = fEvent->WirePairsEnd();
			}
			else if(fReconstructionLevel == 3){
				//FGSII add 11/03/2014
				chan_begin = fReconstr->ChanPedestalBegin();
				chan_end = fReconstr->ChanPedestalEnd();
				//cout << "......Rec Level 3 not yet available" << endl;
			}
			else if(fReconstructionLevel == 4){
				//FGSII add 11/03/2014
				chan_begin = fReconstr->ChanFirstACBegin();
				chan_end = fReconstr->ChanFirstACEnd();
				//cout << "......Rec Level 4 not yet available" << endl;
			}
			else if(fReconstructionLevel == 5){
				//FGSII add 11/03/2014
				chan_begin = fReconstr->ChanSecondACBegin();
				chan_end = fReconstr->ChanSecondACEnd();
				//cout << "......Rec Level 6 not yet available" << endl;
			}
			else if(fReconstructionLevel == 6){
				//FGSII add 11/03/2014
				chan_begin = fReconstr->ChanCurrentBegin();
				chan_end = fReconstr->ChanCurrentEnd();				
				//cout << "......Rec Level 7 not yet available" << endl;
			}
			else if(fReconstructionLevel == 7){
				//FGSII add 11/03/2014
				chan_begin = fReconstr->ChanChargeBegin();
				chan_end = fReconstr->ChanChargeEnd();				
				//cout << "......Rec Level 7 not yet available" << endl;
			}
			else{
				cout << "......ERROR: Rec level does not exist" << endl;
			}
		}
		//FGSII: Now we need to create a Pave (label) for each channel,
		//the label will go with the trace with greatest offset
		//problems here if offsets are ever negative
		double temp_offset=0;
		unsigned int pave_trace_id;
		double x1,y1;
		//FGSII add && condition 11/03/2014
		if(fRawHistoVector.size() != 0 && (fReconstructionLevel == 1 || fReconstructionLevel == 2) ){
			for(evt::Event::ConstWirePairIterator iter1 = wires_begin; iter1 != wires_end; ++iter1){
				//make pos wire paves
				temp_offset = 0;	
				for(evt::Channel::ConstTraceIterator iter2 = (*iter1)->GetWirePair().first->TracesBegin();
				iter2 != (*iter1)->GetWirePair().first->TracesEnd(); ++iter2){
					if((*iter2)->GetTraceOffset() >= temp_offset){
						temp_offset = (*iter2)->GetTraceOffset();
						pave_trace_id = (*iter2)->GetTraceId();
					}
				}
				x1 = fRawHistoVector[IdPair((*iter1)->GetPosWireId(),pave_trace_id)]->GetXaxis()->GetXmax();
				y1 = fRawHistoVector[IdPair((*iter1)->GetPosWireId(),pave_trace_id)]->
				GetBinContent(fRawHistoVector[IdPair((*iter1)->GetPosWireId(),pave_trace_id)]->GetNbinsX());
				MakePave((*iter1)->GetPosWireId(), vertical_offset,horiz_min_value, horiz_max_value, 
				vert_min_value, vert_max_value, x1, y1);

				//make neg wire paves
				temp_offset = 0;	
				for(evt::Channel::ConstTraceIterator iter2 = (*iter1)->GetWirePair().second->TracesBegin();
				iter2 != (*iter1)->GetWirePair().second->TracesEnd(); ++iter2){
					if((*iter2)->GetTraceOffset() >= temp_offset){
						temp_offset = (*iter2)->GetTraceOffset();
						pave_trace_id = (*iter2)->GetTraceId();
					}
				}
				x1 = fRawHistoVector[IdPair((*iter1)->GetNegWireId(),pave_trace_id)]->GetXaxis()->GetXmax();
				y1 = fRawHistoVector[IdPair((*iter1)->GetNegWireId(),pave_trace_id)]->
				GetBinContent(fRawHistoVector[IdPair((*iter1)->GetNegWireId(),pave_trace_id)]->GetNbinsX());
				MakePave((*iter1)->GetNegWireId(), vertical_offset,horiz_min_value, horiz_max_value, 
				vert_min_value, vert_max_value, x1, y1);
			}
		}
		else if (fRawHistoVector.size() != 0){ //FGSII add 11/03/2014 case for the rest of the rec levels
			for(evt::Event::ConstChannelIterator iter1 = chan_begin; iter1 != chan_end; ++iter1){
				temp_offset = 0;	
				for(evt::Channel::ConstTraceIterator iter2 = (*iter1)->TracesBegin();
				iter2 != (*iter1)->TracesEnd(); ++iter2){
					if((*iter2)->GetTraceOffset() >= temp_offset){
						temp_offset = (*iter2)->GetTraceOffset();
						pave_trace_id = (*iter2)->GetTraceId();
					}
				}
				x1 = fRawHistoVector[IdPair((*iter1)->GetId(),pave_trace_id)]->GetXaxis()->GetXmax();
				y1 = fRawHistoVector[IdPair((*iter1)->GetId(),pave_trace_id)]->
				GetBinContent(fRawHistoVector[IdPair((*iter1)->GetId(),pave_trace_id)]->GetNbinsX());
				MakePave((*iter1)->GetId(), vertical_offset,horiz_min_value, horiz_max_value, 
				vert_min_value, vert_max_value, x1, y1);
			}
		}
	}


		//Now we plot the paves
		if(fRawHistoVector.size() != 0){
			for (ConstPaveIterator iter = PavesBegin(); iter != PavesEnd(); ++iter){
				(*iter)->Draw();
			}
		}

		//if the rec level is 2 (signal region is being found) let's fill fSigLines and plot
		if(fReconstructionLevel == 2){
			if(fEvent != NULL && fLoadFromWirePairsButton->IsOn() == false){
				//iterate through each channel and add pedestal region lines to the container
				for (evt::Event::ConstChannelIterator iter = fEvent->ChannelsBegin(); 
				iter != fEvent->ChannelsEnd(); ++iter) {
					MakeSigLines((*iter)->GetId());
				}
				//now that the ped region lines are created for each channel let's plot them
				for(ConstSigLinesIterator iter = SigLinesBegin(); iter != SigLinesEnd(); ++iter){
					(*iter)->Draw();
				}
				//Let's also draw the quadratic fit for each channel not working as of 09/30/2014
				//for (evt::Event::ConstChannelIterator iter = fEvent->ChannelsBegin(); 
				//iter != fEvent->ChannelsEnd(); ++iter) {
				//	(*iter)->GetQuadraticFit()->Draw();
				//}
			}
			else if(fEvent != NULL && fLoadFromWirePairsButton->IsOn() == true){
				//cout << "......Sig lines not yet available when filling from wires" << endl;
				for (evt::Event::ConstWirePairIterator iter = fEvent->WirePairsBegin();
				iter != fEvent->WirePairsEnd(); ++iter){
					MakeSigLinesWirePairs((*iter)->GetWirePair().first->GetId());//could also use second
				}
				//now that the ped region lines are created for each channel let's plot them
				for(ConstSigLinesIterator iter = SigLinesBegin(); iter != SigLinesEnd(); ++iter){
					(*iter)->Draw();
				}
			}
		}

		//Let's plot fZeroLine (a line along y=0)
		if(fEvent != NULL && fReconstr != NULL && fZeroLineButton->IsOn() == true){
			unsigned int a = fReconstructionLevel;
			if( a==3 || a==4 || a==5 || a==6 || a==7){
				double x1ZL,x2ZL;//ZL => zero line
				x1ZL = fCanvTraces->GetCanvas()->GetFrame()->GetX1(); 
				x2ZL = fCanvTraces->GetCanvas()->GetFrame()->GetX2();
				fZeroLine = new TLine(x1ZL,0.0,x2ZL,0.0);
				fZeroLine->SetLineColor(1);
				fZeroLine->SetLineStyle(1);
				fZeroLine->SetLineWidth(2);
				fZeroLine->Draw();
			}
		}

		//Let's set the x-axis grid so it is easier to tell where traces begin and end
		fCanvTraces->GetCanvas()->SetGridx();	
	
		//update the canvas
		fCanvTraces->GetCanvas()->Update();

	}//else***

	//before exiting we need to set the previous event number for reference next time
	//TracesUpdateOnCanvas() is called
	if(fEvent != NULL){
		fPreviousEventNumber = fEvent->GetEventNumber();
	}

	cout << endl;

} 
//-------------------------------------------------------------------//
//FGSII add on 07/21/2014
void MWPCPlots::MakePave(unsigned int channelId, double vert_offset, double hmin, double hmax,
	double vmin, double vmax, double x1, double y1){
	stringstream ss;
	ss << channelId;
	string chanId = ss.str();
	const char* chr = chanId.c_str();

	//FGSII comment out 10/29/2014*****************************************
	/*
	//FGSII comment out 10/27/2014
	//when selecting a channel and updating canvas vert_offset is zero
	//if (vert_offset == 0.0){
		//vert_offset = fOptVertOffsetNumber->GetEntry()->GetNumber();
	//}

	//FGSII add 10/27/2014 don't want sig lines to be too small
	double min_offset = 20.0;
	if(vert_offset < min_offset){
		vert_offset = min_offset;
	}

	double w = (hmax-hmin)*(1.0/30.0); 
	double ha = (0.9*abs(vert_offset))*0.5;
	double hb = ((vmax-vmin)*(1.0/20.0))*0.5;

	//FGSII add 07/29/2014
	//this will apply when zooming on the horizontal axis
	if(x1 > fCanvTraces->GetCanvas()->GetFrame()->GetX2()){
		x1 = fCanvTraces->GetCanvas()->GetFrame()->GetX2();
	}

	double x2 = x1 + w;
	double y1_new,y2;
	if(hb > ha){
		y1_new = y1 - ha;
		y2 = y1 + ha;
	}
	else{
		y1_new = y1 - hb;
		y2 = y1 + hb;
	}

	//cout << "hmin = " << hmin << " hmax = " << hmax << " vmin = " << vmin << " vmax = " << vmax << endl;
	//cout << "w = " << w << " ha = " << ha << " hb = " << hb << endl;
	//cout << "x1 = " << x1 << " y1_new = " << y1_new << " x2 = " << x2 << " y2 = " << y2 << endl;
	//cout << endl;
	*/
	//*********************************************************************
	
	//FGSII 10/29/2014 new code to make paves constant height**************
	double vert = ((vmax-vmin)*(1.0/25.0))*0.5;
	double horiz = (hmax-hmin)*(1.0/30.0);
	if(x1 > fCanvTraces->GetCanvas()->GetFrame()->GetX2()){
		x1 = fCanvTraces->GetCanvas()->GetFrame()->GetX2();
	}
	double x2 = x1 + horiz;
	double y1_new = y1 - vert;
	double y2 = y1 + vert;
	//*********************************************************************

	TPaveText* PavePnt = new TPaveText(x1,y1_new,x2,y2);
	PavePnt->SetShadowColor(fCanvTraces->GetCanvas()->GetFrame()->GetFillColor());	
	PavePnt->SetBorderSize(0); //default is 4
	PavePnt->SetFillStyle(0);//0=hollow, 4000=transparent(not working)
	//PavePnt->SetFillColor(fCanvTraces->GetCanvas()->GetFrame()->GetFillColor());
	//PavePnt->TAttFill::SetFillColorAlpha(fCanvTraces->GetCanvas()->GetFrame()->GetFillColor(),0.0);
	PavePnt->AddText(chr);

	fPaves.push_back(PavePnt);
	
}
//-------------------------------------------------------------------//
void MWPCPlots::ClearPaves(){
	fPaves.clear();
}
//-------------------------------------------------------------------//
//FGSII add on 07/21/2014
MWPCPlots::PaveIterator MWPCPlots::PavesBegin(){
	return fPaves.begin();
}
//-------------------------------------------------------------------//
//FGSII add on 07/21/2014
MWPCPlots::PaveIterator MWPCPlots::PavesEnd(){
	return fPaves.end();
}
//-------------------------------------------------------------------//
//FGSII add on 07/21/2014
MWPCPlots::ConstPaveIterator MWPCPlots::PavesBegin() const{
	return fPaves.begin();
}
//-------------------------------------------------------------------//
//FGSII add on 07/21/2014
MWPCPlots::ConstPaveIterator MWPCPlots::PavesEnd() const{
	return fPaves.end();
}
//-------------------------------------------------------------------//
//FGSII add on 07/28/2014
void MWPCPlots::UpdatePaves(){
	cout << "...MWPCPlots::UpdatePaves()" << endl;
	
//NOTE 09/10/2014 since I have 2 tabs, I need to set things such that I draw
//the pave in the correct tab, if not I believe it draws the pave in the last 
//tab opened, so I lose ability to update tabs in the first tab since control
//belongs to the second tab (remove this comment when fixed)

	for (ConstPaveIterator iter = PavesBegin(); iter != PavesEnd(); ++iter){
		if( (*iter) != NULL){
			delete (*iter);
		}
	}
	ClearPaves();//fPaves.clear(); //FGSII mod 09/22/2014

	//cout << fCanvTraces->GetCanvas()->GetFrame()->GetX1() << endl;
	//cout << fCanvTraces->GetCanvas()->GetFrame()->GetX2() << endl;
	//cout << fCanvTraces->GetCanvas()->GetFrame()->GetY1() << endl;
	//cout << fCanvTraces->GetCanvas()->GetFrame()->GetY2() << endl;
	//cout << fAxisHisto->GetXaxis()->GetXmin() << " " << fAxisHisto->GetXaxis()->GetXmax() << " " << fAxisHisto->GetYaxis()->GetXmin() << " " << fAxisHisto->GetYaxis()->GetXmax() << endl;

	evt::Event::ConstChannelIterator chan_begin;
	evt::Event::ConstChannelIterator chan_end;
	if(fEvent != NULL && fReconstr == NULL && fReconstructionLevel == 1){
		chan_begin = fEvent->ChannelsBegin();
		chan_end = fEvent->ChannelsEnd();
	}
	else if(fEvent != NULL && fReconstr != NULL && fReconstructionLevel == 1){
		chan_begin = fEvent->ChannelsBegin();
		chan_end = fEvent->ChannelsEnd();
	}
	else if(fEvent !=NULL && fReconstructionLevel == 2){ //FGSII add 09/11/2014
		chan_begin = fEvent->ChannelsBegin();
		chan_end = fEvent->ChannelsEnd();
	}
	else if(fEvent != NULL && fReconstr != NULL && fReconstructionLevel == 3){
		chan_begin = fReconstr->ChanPedestalBegin();
		chan_end = fReconstr->ChanPedestalEnd();
	}
	else if(fReconstructionLevel == 4){
		//FGSII add 10/15/2014
		chan_begin = fReconstr->ChanFirstACBegin();
		chan_end = fReconstr->ChanFirstACEnd();		
		//cout << "......Rec Level 4 not yet available" << endl;
	}
	else if(fReconstructionLevel == 5){
		chan_begin = fReconstr->ChanSecondACBegin();
		chan_end = fReconstr->ChanSecondACEnd();
		//cout << "......Rec Level 5 not yet available" << endl;
	}
	else if(fReconstructionLevel == 6){
		chan_begin = fReconstr->ChanCurrentBegin();
		chan_end = fReconstr->ChanCurrentEnd();
		//cout << "......Rec Level 6 not yet available" << endl;
	}
	else if(fReconstructionLevel == 7){
		chan_begin = fReconstr->ChanChargeBegin();
		chan_end = fReconstr->ChanChargeEnd();
		//cout << "......Rec Level 7 not yet available" << endl;
	}
	else{
		cout << "......ERROR: Rec level does not exist" << endl;
	}

	double temp_offset=0;
	unsigned int pave_trace_id;
	double x1,y1;//x2,y2;
	if(fRawHistoVector.size() != 0){
		for(evt::Event::ConstChannelIterator iter1 = chan_begin; iter1 != chan_end; ++iter1){
			temp_offset = 0;
			for(evt::Channel::ConstTraceIterator iter2 = (*iter1)->TracesBegin(); iter2 != (*iter1)->TracesEnd(); ++iter2){
				if((*iter2)->GetTraceOffset() >= temp_offset){
					temp_offset = (*iter2)->GetTraceOffset();
					pave_trace_id = (*iter2)->GetTraceId();
				}
			}
			x1 = fRawHistoVector[IdPair((*iter1)->GetId(),pave_trace_id)]->GetXaxis()->GetXmax();
			y1 = fRawHistoVector[IdPair((*iter1)->GetId(),pave_trace_id)]->GetBinContent(fRawHistoVector[IdPair((*iter1)->GetId(),pave_trace_id)]->GetNbinsX());

			//only make Pave if it will exist in the vertical viewing range
			if(y1 <= fCanvTraces->GetCanvas()->GetFrame()->GetY2() &&
				y1 >= fCanvTraces->GetCanvas()->GetFrame()->GetY1()){
			MakePave((*iter1)->GetId(), fVertOffset,
				fCanvTraces->GetCanvas()->GetFrame()->GetX1(), 
				fCanvTraces->GetCanvas()->GetFrame()->GetX2(), 
				fCanvTraces->GetCanvas()->GetFrame()->GetY1(), 
				fCanvTraces->GetCanvas()->GetFrame()->GetY2(), 
				x1, y1);
			//cout << "......x1 = " << x1 << " y1 = " << y1 << " x2 = " << x2 << " y2 = " << y2 << endl;
			}
		}
	}
	
	if(fRawHistoVector.size() != 0){
		for (ConstPaveIterator iter = PavesBegin(); iter != PavesEnd(); ++iter){
			(*iter)->Draw();
		}
	}

	fCanvTraces->GetCanvas()->Update();
	
}
//-------------------------------------------------------------------//
//FGSII add 10/29/2014
void MWPCPlots::UpdatePavesWirePairs(){
	cout << "...MWPCPlots::UpdatePavesWirePairs()" << endl;

	for (ConstPaveIterator iter = PavesBegin(); iter != PavesEnd(); ++iter){
		if( (*iter) != NULL){
			delete (*iter);
		}
	}
	ClearPaves();

	evt::Event::ConstWirePairIterator wires_begin;
	evt::Event::ConstWirePairIterator wires_end;
	if(fEvent != NULL){
		if(fReconstructionLevel == 1){
			wires_begin = fEvent->WirePairsBegin();
			wires_end = fEvent->WirePairsEnd();
		}
		else if(fReconstructionLevel == 2){ //FGSII add 09/11/2014
			wires_begin = fEvent->WirePairsBegin();
			wires_end = fEvent->WirePairsEnd();
		}
		else if(fReconstr != NULL && fReconstructionLevel == 3){
			cout << "......Rec Leve 3 not yet available" << endl;
		}
		else if(fReconstructionLevel == 4){
			cout << "......Rec Level 4 not yet available" << endl;
		}
		else if(fReconstructionLevel == 5){
			cout << "......Rec Level 6 not yet available" << endl;
		}
		else if(fReconstructionLevel == 6){
			cout << "......Rec Level 7 not yet available" << endl;
		}
		else if(fReconstructionLevel == 7){
			cout << "......Rec Level 7 not yet available" << endl;
		}
		else{
			cout << "......ERROR: Rec level does not exist" << endl;
		}
	}

	double temp_offset=0;
	unsigned int pave_trace_id;
	double x1,y1;
	if(fRawHistoVector.size() != 0){
		for(evt::Event::ConstWirePairIterator iter1 = wires_begin; iter1 != wires_end; ++iter1){
			//make pos wire paves
			temp_offset = 0;	
			for(evt::Channel::ConstTraceIterator iter2 = (*iter1)->GetWirePair().first->TracesBegin();		iter2 != (*iter1)->GetWirePair().first->TracesEnd(); ++iter2){
				if((*iter2)->GetTraceOffset() >= temp_offset){
					temp_offset = (*iter2)->GetTraceOffset();
					pave_trace_id = (*iter2)->GetTraceId();
				}
			}
			x1 = fRawHistoVector[IdPair((*iter1)->GetPosWireId(),pave_trace_id)]->GetXaxis()->GetXmax();
			y1 = fRawHistoVector[IdPair((*iter1)->GetPosWireId(),pave_trace_id)]->GetBinContent(fRawHistoVector[IdPair((*iter1)->GetPosWireId(),pave_trace_id)]->GetNbinsX());
			if(y1 <= fCanvTraces->GetCanvas()->GetFrame()->GetY2() &&
				y1 >= fCanvTraces->GetCanvas()->GetFrame()->GetY1()){
			MakePave((*iter1)->GetPosWireId(), fVertOffset,
				fCanvTraces->GetCanvas()->GetFrame()->GetX1(), 
				fCanvTraces->GetCanvas()->GetFrame()->GetX2(), 
				fCanvTraces->GetCanvas()->GetFrame()->GetY1(), 
				fCanvTraces->GetCanvas()->GetFrame()->GetY2(), 
				x1, y1);
			//cout << "......x1 = " << x1 << " y1 = " << y1 << " x2 = " << x2 << " y2 = " << y2 << endl;
			}

			//make neg wire paves
			temp_offset = 0;	
			for(evt::Channel::ConstTraceIterator iter2 = (*iter1)->GetWirePair().second->TracesBegin();		iter2 != (*iter1)->GetWirePair().second->TracesEnd(); ++iter2){
				if((*iter2)->GetTraceOffset() >= temp_offset){
					temp_offset = (*iter2)->GetTraceOffset();
					pave_trace_id = (*iter2)->GetTraceId();
				}
			}
			x1 = fRawHistoVector[IdPair((*iter1)->GetNegWireId(),pave_trace_id)]->GetXaxis()->GetXmax();
			y1 = fRawHistoVector[IdPair((*iter1)->GetNegWireId(),pave_trace_id)]->GetBinContent(fRawHistoVector[IdPair((*iter1)->GetNegWireId(),pave_trace_id)]->GetNbinsX());
			if(y1 <= fCanvTraces->GetCanvas()->GetFrame()->GetY2() &&
				y1 >= fCanvTraces->GetCanvas()->GetFrame()->GetY1()){
			MakePave((*iter1)->GetNegWireId(), fVertOffset,
				fCanvTraces->GetCanvas()->GetFrame()->GetX1(), 
				fCanvTraces->GetCanvas()->GetFrame()->GetX2(), 
				fCanvTraces->GetCanvas()->GetFrame()->GetY1(), 
				fCanvTraces->GetCanvas()->GetFrame()->GetY2(), 
				x1, y1);
			//cout << "......x1 = " << x1 << " y1 = " << y1 << " x2 = " << x2 << " y2 = " << y2 << endl;
			}
		}
	}

	if(fRawHistoVector.size() != 0){
		for (ConstPaveIterator iter = PavesBegin(); iter != PavesEnd(); ++iter){
			(*iter)->Draw();
		}
	}

	fCanvTraces->GetCanvas()->Update();
}
//-------------------------------------------------------------------//
void MWPCPlots::HandleCanvEvents(Int_t event, Int_t px, Int_t py, TObject* sel){
	//note: kButton1Down==1, kButton1Up==11, kButton1Double==61
	
	//update paves upon clicks so paves are positioned correctly
	if ((event == kButton1Up)){
		//maybe add code to only update if kIsZoomed is set, will reduce updates
		//FGSII add if conditions on 10/29/2014
		if(fLoadFromWirePairsButton->IsOn() == false){
			UpdatePaves();
		}
		else if(fLoadFromWirePairsButton->IsOn() == true){
			UpdatePavesWirePairs();
		}
	}
	/*if((event == kButton3Up)){
		if(fAxisHisto != NULL && fAxisHisto->TestBit(TH1::kIsZoomed) == 1){
			cout << "......Here" << endl;
		}
	}*/


}
//-------------------------------------------------------------------//
//FGSII add on 07/31/2014
TRootEmbeddedCanvas* MWPCPlots::GetfCanvTraces(){
	return fCanvTraces;
}
//-------------------------------------------------------------------//




//-------------------------------------------------//
//---beginning of code unique to reconstruction plots------------------------//
//-------------------------------------------------//

MWPCPlots::MWPCPlots(TGCompositeFrame* main, det::Detector *det_descr, drift::rec::Reconstruct* reconstr):
	fDetector(det_descr),
	fReconstr(reconstr),
	fEvent(NULL),
	fVariablesTab(NULL), fChannelsListBox(NULL), fCanvTraces(NULL), 
	fOptVertOffsetNumber(NULL),
	fChannelsDrawingOffset(-20.0), fCurrentSelectedChannel(0),
	fReconstructionLevel(1),
	fAxisHisto(NULL),
	fPreviousEventNumber(0),//FGSII added 07/29/2014
	fZeroLine(NULL) //FGSII add 10/30/2014
	{

	const double width  = main->GetWidth();
	const double height = main->GetHeight();

	//cout << "...Creating MWPC Plots in a Tab:" << width << "x" << height << endl;
	cout << "...Creating MWPC Plots in a Tab (Reconstruction):" << width << "x" << height << endl;//FGSII add 08/27/2014

	//FGSII 08/27/2014 for debug
	//cout << "......" << main->GetParent()->ClassName() << endl;

	//------ Hints -------//
	TGLayoutHints* mainLayout = new TGLayoutHints(
	kLHintsExpandX, 1,1,1,1);
  // TGLayoutHints* centeredLayout= new TGLayoutHints(
  //	kLHintsCenterX|kLHintsCenterY, 1,1,1,1);
  // TGLayoutHints* centeredXLayout= new TGLayoutHints(
  //	kLHintsCenterX,1,1,1,1);
  /* TGLayoutHints* centeredExpandXYLayout = */ new TGLayoutHints(
	kLHintsExpandX|kLHintsExpandY|kLHintsCenterX|kLHintsCenterY,1,1,1,1);
  TGLayoutHints* expandXYLayout = new TGLayoutHints(kLHintsExpandX|
	kLHintsExpandY,1,1,1,1);
  TGLayoutHints* expandYLayout = new TGLayoutHints(
	kLHintsExpandY,1,1,1,1);
  //------ End of layout hints --------//
  //-------- Frames -------------------//
  TGHorizontalFrame* mainHorTopFrame = new TGHorizontalFrame(main,
	UInt_t(width), UInt_t(0.78*height));//
  TGHorizontalFrame* mainHorBotFrame = new TGHorizontalFrame(main,
  	UInt_t(width), UInt_t(0.22*height));
  main->AddFrame(mainHorTopFrame, mainLayout);
  main->AddFrame(mainHorBotFrame, mainLayout);

  cout << "...MWPC: Created 2 Frames:" << mainHorTopFrame->GetWidth() << "x"
  	<< mainHorTopFrame->GetHeight()  << " and " 
	<< mainHorBotFrame->GetWidth() << "x" << mainHorBotFrame->GetHeight() 
	<< endl;

  //--------- Canvas in which traces are drawn --------------//
  TGVerticalFrame* tracesFrame = new TGVerticalFrame(mainHorTopFrame,
	UInt_t(1.1*mainHorTopFrame->GetWidth()), mainHorTopFrame->GetHeight());//FGSII 07/18/2014, width was 0.7*
  mainHorTopFrame->AddFrame(tracesFrame, expandYLayout);
  fCanvTraces = new TRootEmbeddedCanvas(
  		"tracesCanvas", tracesFrame,
		tracesFrame->GetWidth(), tracesFrame->GetHeight());
  tracesFrame->AddFrame(fCanvTraces, expandXYLayout);

	//FGSII add 07/28/2014
	fCanvTraces->GetCanvas()->Connect("ProcessedEvent(Int_t,Int_t,Int_t,TObject*)", "MWPCPlots", this, "HandleCanvEvents(Int_t,Int_t,Int_t,TObject*)");

  //---------- This is a list of channels in the event -------//
  TGVerticalFrame* channelsListFrame = new TGVerticalFrame(mainHorTopFrame,
	UInt_t(0.3*mainHorTopFrame->GetWidth()), mainHorTopFrame->GetHeight());
  mainHorTopFrame->AddFrame(channelsListFrame, expandYLayout);
  TGTab* fChannelsListTab = new TGTab(channelsListFrame,
  	channelsListFrame->GetWidth(), channelsListFrame->GetHeight());
  channelsListFrame->AddFrame(fChannelsListTab, expandYLayout);

  TGCompositeFrame *channelsInfoFrame = fChannelsListTab->AddTab("Channels");

  fChannelsListBox = new TGListBox(channelsInfoFrame, 96);//FGSII change 10/28/2014 was 49
  fChannelsListBox->Connect("Selected(Int_t)", "MWPCPlots",
  	this, "SelectChannel()");
  channelsInfoFrame->AddFrame(fChannelsListBox, expandXYLayout);

  //------------ Variables Tabs  ----------//
  fVariablesTab = new TGTab(mainHorBotFrame,
	mainHorBotFrame->GetWidth(), mainHorBotFrame->GetHeight());
  mainHorBotFrame->AddFrame(fVariablesTab, 
  	new TGLayoutHints(kLHintsTop|kLHintsExpandX,1,1,1,1));

  TGCompositeFrame *optFrame = fVariablesTab->AddTab("Options");
  optFrame->ChangeOptions(optFrame->GetOptions() | kHorizontalFrame);

  fOptVertOffsetNumber = new TGTextMargin(optFrame, "Vertical Offset");
  optFrame->AddFrame(fOptVertOffsetNumber,
	new TGLayoutHints(kLHintsTop|kLHintsExpandX, 3, 2, 2, 2));
  fOptVertOffsetNumber->GetEntry()->Connect("TextChanged(char*)", "MWPCPlots",
	this, "DoVerticalOffset()");
  fOptVertOffsetNumber->GetEntry()->SetNumber(fChannelsDrawingOffset);

	//FGSII add 10/27/2014
	//adding a check button so we can load from wires instead of channels
	fLoadFromWirePairsButton = new TGCheckButton(optFrame, "Load From Wires");
	optFrame->AddFrame(fLoadFromWirePairsButton, new TGLayoutHints(kLHintsTop|kLHintsExpandX, 3, 2, 2, 2));
	//fLoadFromWirePairsButton->SetEnabled(kFALSE);

	//FGSII add 10/30/2014
	fZeroLineButton = new TGCheckButton(optFrame, "Show Zero Line");
	optFrame->AddFrame(fZeroLineButton, new TGLayoutHints(kLHintsTop|kLHintsExpandX, 3, 2, 2, 2));


//-------------------------------------------------//
//---beginning of rec stuff------------------------//
//-------------------------------------------------//

	TGCompositeFrame *elecFrame = fVariablesTab->AddTab("Channel Electronics");
	elecFrame->ChangeOptions(elecFrame->GetOptions() | kHorizontalFrame);
	TGCompositeFrame *recFrame = fVariablesTab->AddTab("Channel Reconstruction");
	recFrame->ChangeOptions(recFrame->GetOptions() | kHorizontalFrame);
	
	//-------- Reconstruction Level Buttons -----//
	TGVButtonGroup *recLevelButtons = new TGVButtonGroup(optFrame,"Rec Level");
	optFrame->AddFrame(recLevelButtons, 
	new TGLayoutHints(kLHintsTop,1,1,1,1));
	TGRadioButton *fRecLevelButton1 = new TGRadioButton(recLevelButtons, "Raw", 1);
													new TGRadioButton(recLevelButtons, "Find Signal Region" , 2); //add 09/11/2014
														 new TGRadioButton(recLevelButtons, "Remove Pedestal" , 3);
	/* TGRadioButton *fRecLevelButton2 = */ new TGRadioButton(recLevelButtons, "First AC", 4);
	/* TGRadioButton *fRecLevelButton3 = */ new TGRadioButton(recLevelButtons, "Second AC", 5);
	/* TGRadioButton *fRecLevelButton4 = */ new TGRadioButton(recLevelButtons, "Current", 6);
	/* TGRadioButton *fRecLevelButton5 = */ new TGRadioButton(recLevelButtons, "Charge", 7);
	recLevelButtons->SetRadioButtonExclusive();
	fRecLevelButton1->SetState(kButtonDown);
	recLevelButtons->Connect("Pressed(Int_t)", "MWPCPlots", this, "DoRecLevel(Int_t)");

	//-------- Electronics Parameters: TransImpedance -----//
	TGGroupFrame *elecParamsTransimpFrame = new TGGroupFrame(elecFrame, "Transimpedance");
	elecFrame->AddFrame(elecParamsTransimpFrame, new TGLayoutHints(kLHintsTop|kLHintsExpandY,0,0,2,2));

	fElecTransRfNumber = new TGTextMargin(elecParamsTransimpFrame, "Rf [MOhms]");
	elecParamsTransimpFrame->AddFrame(fElecTransRfNumber, 
		new TGLayoutHints(kLHintsTop|kLHintsExpandX, 3, 2, 2, 2));
	fElecTransRfNumber->GetEntry()->SetFormat(TGNumberFormat::kNESRealOne);
	//fElecTransRfNumber->GetEntry()->Connect("TextChanged(char*)", "MWPCPlots", this, "DoTransimpRf()");

	fElecTransCfNumber = new TGTextMargin(elecParamsTransimpFrame, "Cf [pF]");
	elecParamsTransimpFrame->AddFrame(fElecTransCfNumber, 
		new TGLayoutHints(kLHintsTop|kLHintsExpandX, 3, 2, 2, 2));
	fElecTransCfNumber->GetEntry()->SetFormat(TGNumberFormat::kNESRealOne);
	//fElecTransCfNumber->GetEntry()->Connect("TextChanged(char*)", "MWPCPlots", this, "DoTransimpCf()");

	//-------- Electronics Parameters: AC First -------//
	TGGroupFrame *elecParamsAC1Frame = new TGGroupFrame(elecFrame, "First AC Coupling");
	elecFrame->AddFrame(elecParamsAC1Frame,
		new TGLayoutHints(kLHintsTop|kLHintsExpandY,0,0,2,2));
  
	fElecAC1RfNumber = new TGTextMargin(elecParamsAC1Frame, "Rf [Ohms]");
	elecParamsAC1Frame->AddFrame(fElecAC1RfNumber, new TGLayoutHints(kLHintsExpandX, 0, 0, 2, 2));
	fElecAC1RfNumber->GetEntry()->SetFormat(TGNumberFormat::kNESRealOne);
	//fElecAC1RfNumber->GetEntry()->Connect("TextChanged(char*)", "MWPCPlots", this, "DoFirstACRf()");

	fElecAC1RgNumber = new TGTextMargin(elecParamsAC1Frame, "Rg [Ohms]");
	elecParamsAC1Frame->AddFrame(fElecAC1RgNumber, new TGLayoutHints(kLHintsExpandX, 0, 0, 2, 2));
	fElecAC1RgNumber->GetEntry()->SetFormat(TGNumberFormat::kNESRealOne);
	//fElecAC1RgNumber->GetEntry()->Connect("TextChanged(char*)", "MWPCPlots", this, "DoFirstACRg()");

	fElecAC1CgNumber = new TGTextMargin(elecParamsAC1Frame, "Cg [uF]");
	elecParamsAC1Frame->AddFrame(fElecAC1CgNumber, new TGLayoutHints(kLHintsExpandX, 0, 0, 2, 2));
	fElecAC1CgNumber->GetEntry()->SetFormat(TGNumberFormat::kNESRealOne);
	//fElecAC1CgNumber->GetEntry()->Connect("TextChanged(char*)", "MWPCPlots", this, "DoFirstACCg()");

 
	//-------- Electronics Parameters: AC Second -------//
	TGGroupFrame *elecParamsAC2Frame = new TGGroupFrame(elecFrame, "Second AC Coupling");
	elecFrame->AddFrame(elecParamsAC2Frame, new TGLayoutHints(kLHintsTop|kLHintsExpandY,0,0,2,2));

	fElecAC2RfNumber = new TGTextMargin(elecParamsAC2Frame, "Rf [Ohms]");
	elecParamsAC2Frame->AddFrame(fElecAC2RfNumber, new TGLayoutHints(kLHintsExpandX, 0, 0, 2, 2));
	fElecAC2RfNumber->GetEntry()->SetFormat(TGNumberFormat::kNESRealOne);
	//fElecAC2RfNumber->GetEntry()->Connect("TextChanged(char*)", "MWPCPlots", this, "DoSecondACRf()");

	fElecAC2RgNumber = new TGTextMargin(elecParamsAC2Frame, "Rg [Ohms]");
	elecParamsAC2Frame->AddFrame(fElecAC2RgNumber, new TGLayoutHints(kLHintsExpandX, 0, 0, 2, 2));
	fElecAC2RgNumber->GetEntry()->SetFormat(TGNumberFormat::kNESRealOne);
	//fElecAC2RgNumber->GetEntry()->Connect("TextChanged(char*)", "MWPCPlots", this, "DoSecondACRg()");

	fElecAC2CgNumber = new TGTextMargin(elecParamsAC2Frame, "Cg [uF]");
	elecParamsAC2Frame->AddFrame(fElecAC2CgNumber, new TGLayoutHints(kLHintsExpandX, 0, 0, 2, 2));
	fElecAC2CgNumber->GetEntry()->SetFormat(TGNumberFormat::kNESRealOne);
	//fElecAC2CgNumber->GetEntry()->Connect("TextChanged(char*)", "MWPCPlots", this, "DoSecondACCg()");


	//-------- Rec Parameters: Time Interval -----//
	TGGroupFrame *recTimeIntervalFrame = new TGGroupFrame(recFrame, "TimeInterval");
	recFrame->AddFrame(recTimeIntervalFrame, new TGLayoutHints(kLHintsTop|kLHintsExpandY,0,0,2,2));

	fRecSignalBegTimeNumber = new TGTextMargin(recTimeIntervalFrame, "Signal Begins [usec]");
	recTimeIntervalFrame->AddFrame(fRecSignalBegTimeNumber, new TGLayoutHints(kLHintsExpandX, 0, 0, 2, 2));
	fRecSignalBegTimeNumber->GetEntry()->SetFormat(TGNumberFormat::kNESReal);
	//fRecSignalBegTimeNumber->GetEntry()->Connect("TextChanged(char*)", "MWPCPlots", this, "DoTimeRangeBeg()");

	fRecSignalEndTimeNumber = new TGTextMargin(recTimeIntervalFrame, "Signal Ends [usec]");
	recTimeIntervalFrame->AddFrame(fRecSignalEndTimeNumber, new TGLayoutHints(kLHintsExpandX, 0, 0, 2, 2));
	fRecSignalEndTimeNumber->GetEntry()->SetFormat(TGNumberFormat::kNESReal);
	//fRecSignalEndTimeNumber->GetEntry()->Connect("TextChanged(char*)", "MWPCPlots", this, "DoTimeRangeEnd()");

	fVariablesTab->SetEnabled(1, kFALSE); // 1 is channel electronics
	fVariablesTab->SetEnabled(2, kFALSE); // 2 is channel rec parameters


}
//-------------------------------------------------------------------//
void MWPCPlots::DoRecLevel(Int_t rec_level){
	cout << "\n...MWPCPlots::DoRecLevel" << endl;
	cout << "......Rec Level chosen " << rec_level << endl;

	if (rec_level == 1){
		fReconstructionLevel = 1;
		if( (fReconstr != NULL) && (fLoadFromWirePairsButton->IsOn()) ){ 
			TracesFillFromRawEventWirePairs();
			TracesUpdateOnCanvas(fChannelsDrawingOffset);
		}
		else{
			TracesFillFromRawEvent();
			TracesUpdateOnCanvas(fChannelsDrawingOffset);
		}
 	}
	else if (rec_level == 2){ //FGSII add 09/11/2014
		fReconstructionLevel = 2;
		if( (fReconstr != NULL) && (fLoadFromWirePairsButton->IsOn()) ){ 
			//add function to check for time continuity
			ReconUpdateSignalRegionWirePairs();
			TracesFillFromRawEventWirePairs();
			TracesUpdateOnCanvas(fChannelsDrawingOffset);
		}
		else{
			//add functiion to check for time continuity
			ReconUpdateSignalRegion();
			TracesFillFromRawEvent();
			TracesUpdateOnCanvas(fChannelsDrawingOffset);
		}
	}
	else if (rec_level == 3){
		fReconstructionLevel = 3;
		if( (fReconstr != NULL) && (fLoadFromWirePairsButton->IsOn()) ){ 
			//FGSII add 10/31/2014
			ReconUpdatePedestalWirePairs();
			TracesFillFromPedestalWirePairs();
			TracesUpdateOnCanvas(fChannelsDrawingOffset);
		}
		else{
			ReconUpdatePedestal();
			TracesFillFromPedestal();
			TracesUpdateOnCanvas(fChannelsDrawingOffset);
		}
	} 
	else if (rec_level == 4){ //FGSII add 10/15/2014
		fReconstructionLevel = 4;
		if( (fReconstr != NULL) && (fLoadFromWirePairsButton->IsOn()) ){ 
			ReconUpdateFirstACWirePairs();
			TracesFillFromFirstACWirePairs();
			TracesUpdateOnCanvas(fChannelsDrawingOffset);
		}
		else{
			ReconUpdateFirstAC();
			TracesFillFromFirstAC();
			TracesUpdateOnCanvas(fChannelsDrawingOffset);
		}
	}
	else if (rec_level == 5){
		fReconstructionLevel = 5;
		if( (fReconstr != NULL) && (fLoadFromWirePairsButton->IsOn()) ){
			ReconUpdateSecondACWirePairs();
			TracesFillFromSecondACWirePairs();
			TracesUpdateOnCanvas(fChannelsDrawingOffset); 
			//cout << "......Feature not yet available for wire pairs" << endl;		
		}
		else{
			ReconUpdateSecondAC();
			TracesFillFromSecondAC();
			TracesUpdateOnCanvas(fChannelsDrawingOffset);
		}
	}
	else if (rec_level == 6){
		fReconstructionLevel = 6;
		if( (fReconstr != NULL) && (fLoadFromWirePairsButton->IsOn()) ){
			ReconUpdateCurrentWirePairs();
			TracesFillFromCurrentWirePairs();
			TracesUpdateOnCanvas(fChannelsDrawingOffset); 
			//cout << "......Feature not yet available for wire pairs" << endl;		
		}
		else{
			ReconUpdateCurrent();
			TracesFillFromCurrent();
			TracesUpdateOnCanvas(fChannelsDrawingOffset);
		}
	}
	else if (rec_level == 7){
		fReconstructionLevel = 7;
		if( (fReconstr != NULL) && (fLoadFromWirePairsButton->IsOn()) ){
			ReconUpdateChargeWirePairs();
			TracesFillFromChargeWirePairs();
			TracesUpdateOnCanvas(fChannelsDrawingOffset);
			//cout << "......Feature not yet available for wire pairs" << endl;		
		}
		else{
			ReconUpdateCharge();
			TracesFillFromCharge();
			TracesUpdateOnCanvas(fChannelsDrawingOffset);
		}
	} 
	else {
		cout << "......ERROR: rec level does not exist" << endl;
	}

}
//-------------------------------------------------------------------//
//FGSII add 09/11/2014
void MWPCPlots::ReconUpdateSignalRegion(){ 
	cout << "...MWPCPlots::ReconUpdateSignalRegion" << endl;
	if(fEvent == NULL){
		cout << "......Event is NULL" << endl;
		return;
	}

	for (evt::Event::ConstChannelIterator iter = fEvent->ChannelsBegin();iter != fEvent->ChannelsEnd(); ++iter) {
		//sort traces in ascending chron order
		(*iter)->SortTraces(); //seems to be working 09/24/2014
		//(*iter)->DetermineSigContinuity(); //not yet working
		(*iter)->FindSignalBegin_3_2((*iter)->GetTrace(0));

		//FGSII 10/31/2014 comment these out for now
		//(*iter)->FindSignalEnd1((*iter)->GetTrace((*iter)->GetNofTracesInChan()-1));
		//(*iter)->CalcQuadraticFitParams();
	}

	//FGSII 11/03/2014
	//UpdatefReconstr();
}
//-------------------------------------------------------------------//
void MWPCPlots::ReconUpdatePedestal(){
	cout << "...MWPCPlots::ReconUpdatePedestal()" << endl;
	if(fEvent == NULL){
		cout << "......Event is NULL" << endl;
		return;
	}
	if(fReconstr == NULL){
		cout << "......Reconstruction is NULL" << endl;
		return;
	}

	for (evt::Event::ConstChannelIterator iter = fEvent->ChannelsBegin();iter != fEvent->ChannelsEnd(); ++iter) {
		
		//first check that the first ped region is set, if not set it
		//add 09/25/2014
		if((*iter)-> GetNofPedestals() == 0){
			cout << "......Pedestal regions have not yet been set" << endl;
			cout << "......Going to find pedestal regions" << endl;
			ReconUpdateSignalRegion();
		}
		
		//create ped channel if it does not exist
		if(!fReconstr->HasPedestalChannel((*iter)->GetId()) ){
			fReconstr->RecPedestal((*iter)->GetId());
		}		
	}

}
//-------------------------------------------------------------------//
//FGSII add 10/15/2014
void MWPCPlots::ReconUpdateFirstAC(){
	cout << "...MWPCPlots::ReconUpdateFirstAC()" << endl;
	if(fEvent == NULL){
		cout << "......Event is NULL" << endl;
	}
	if(fReconstr == NULL){
		cout << "......Reconstruction is NULL" << endl;
		return;
	}
	
	for (evt::Event::ConstChannelIterator iter = fEvent->ChannelsBegin();iter != fEvent->ChannelsEnd(); ++iter) {

		//first check if ped region(s) are set, if not set them
		if((*iter)-> GetNofPedestals() == 0){
			cout << "......Pedestal regions have not yet been set" << endl;
			cout << "......Going to find pedestal regions" << endl;
			ReconUpdateSignalRegion();
		}

		//next, check if the pedestal channels have been made in the reconstruction
		//if not let's make them
		if(!fReconstr->HasPedestalChannel((*iter)->GetId()) ){
			fReconstr->RecPedestal((*iter)->GetId());
		}

		//next, check if the first ac channels have been made in the reconstruction
		//if not let's make them
		if(!fReconstr->HasFirstACChannel((*iter)->GetId()) ){
			fReconstr->RecFirstAC((*iter)->GetId());
		}
	}
}
//-------------------------------------------------------------------//
void MWPCPlots::ReconUpdateSecondAC(){
	cout << "...MWPCPlots::ReconUpdateSecondAC()" << endl;
	if(fEvent == NULL){
		cout << "......Event is NULL" << endl;
	}
	if(fReconstr == NULL){
		cout << "......Reconstruction is NULL" << endl;
		return;
	}
	
	for (evt::Event::ConstChannelIterator iter = fEvent->ChannelsBegin();iter != fEvent->ChannelsEnd(); ++iter) {

		//first check if ped region(s) are set, if not set them
		if((*iter)-> GetNofPedestals() == 0){
			cout << "......Pedestal regions have not yet been set" << endl;
			cout << "......Going to find pedestal regions" << endl;
			ReconUpdateSignalRegion();
		}

		//next, check if the pedestal channels have been made in the reconstruction
		//if not let's make them
		if(!fReconstr->HasPedestalChannel((*iter)->GetId()) ){
			fReconstr->RecPedestal((*iter)->GetId());
		}

		//next, check if the first ac channels have been made in the reconstruction
		//if not let's make them
		if(!fReconstr->HasFirstACChannel((*iter)->GetId()) ){
			fReconstr->RecFirstAC((*iter)->GetId());
		}

		//next, check if the second ac channels have been made in the reconstruction
		//if not let's make them
		if(!fReconstr->HasSecondACChannel((*iter)->GetId()) ){
			fReconstr->RecSecondAC((*iter)->GetId());
		}
	}
}
//-------------------------------------------------------------------//
void MWPCPlots::ReconUpdateCurrent(){
	cout << "...MWPCPlots::ReconUpdateCurrent()" << endl;
	if(fEvent == NULL){
		cout << "......Event is NULL" << endl;
	}
	if(fReconstr == NULL){
		cout << "......Reconstruction is NULL" << endl;
		return;
	}
	
	for (evt::Event::ConstChannelIterator iter = fEvent->ChannelsBegin();iter != fEvent->ChannelsEnd(); ++iter) {

		//first check if ped region(s) are set, if not set them
		if((*iter)-> GetNofPedestals() == 0){
			cout << "......Pedestal regions have not yet been set" << endl;
			cout << "......Going to find pedestal regions" << endl;
			ReconUpdateSignalRegion();
		}

		//next, check if the pedestal channels have been made in the reconstruction
		//if not let's make them
		if(!fReconstr->HasPedestalChannel((*iter)->GetId()) ){
			fReconstr->RecPedestal((*iter)->GetId());
		}

		//next, check if the first ac channels have been made in the reconstruction
		//if not let's make them
		if(!fReconstr->HasFirstACChannel((*iter)->GetId()) ){
			fReconstr->RecFirstAC((*iter)->GetId());
		}

		//next, check if the second ac channels have been made in the reconstruction
		//if not let's make them
		if(!fReconstr->HasSecondACChannel((*iter)->GetId()) ){
			fReconstr->RecSecondAC((*iter)->GetId());
		}

		//next, check if the current channels have been made in the reconstruction
		//if not let's make them
		if(!fReconstr->HasCurrentChannel((*iter)->GetId()) ){
			fReconstr->RecCurrent((*iter)->GetId());
		}	
	}
}
//-------------------------------------------------------------------//
void MWPCPlots::ReconUpdateCharge(){
	cout << "...MWPCPlots::ReconUpdateCharge()" << endl;
	if(fEvent == NULL){
		cout << "......Event is NULL" << endl;
	}
	if(fReconstr == NULL){
		cout << "......Reconstruction is NULL" << endl;
		return;
	}
	
	for (evt::Event::ConstChannelIterator iter = fEvent->ChannelsBegin();iter != fEvent->ChannelsEnd(); ++iter) {

		//first check if ped region(s) are set, if not set them
		if((*iter)-> GetNofPedestals() == 0){
			cout << "......Pedestal regions have not yet been set" << endl;
			cout << "......Going to find pedestal regions" << endl;
			ReconUpdateSignalRegion();
		}

		//next, check if the pedestal channels have been made in the reconstruction
		//if not let's make them
		if(!fReconstr->HasPedestalChannel((*iter)->GetId()) ){
			fReconstr->RecPedestal((*iter)->GetId());
		}

		//next, check if the first ac channels have been made in the reconstruction
		//if not let's make them
		if(!fReconstr->HasFirstACChannel((*iter)->GetId()) ){
			fReconstr->RecFirstAC((*iter)->GetId());
		}

		//next, check if the second ac channels have been made in the reconstruction
		//if not let's make them
		if(!fReconstr->HasSecondACChannel((*iter)->GetId()) ){
			fReconstr->RecSecondAC((*iter)->GetId());
		}

		//next, check if the current channels have been made in the reconstruction
		//if not let's make them
		if(!fReconstr->HasCurrentChannel((*iter)->GetId()) ){
			fReconstr->RecCurrent((*iter)->GetId());
		}

		//next, check if the charge channels have been made in the reconstruction
		//if not let's make them
		if(!fReconstr->HasChargeChannel((*iter)->GetId()) ){
			fReconstr->RecCharge((*iter)->GetId());
		}	
	}
}
//-------------------------------------------------------------------//
//FGSII add on 09/03/2014
void MWPCPlots::TracesFillFromPedestal(){
	cout << "...MWPCPlots::TracesFillFromPedestal" << endl;
	// Let's Clean the prev histos
	//FGSII added next four lines on 07/21/2014 to fix mem leak problem
	for (std::map<IdPair,TH1D*>::iterator iter = fRawHistoVector.begin();
		iter != fRawHistoVector.end(); ++iter) {
		delete iter->second;
	}
	fRawHistoVector.clear();
	if (fReconstr == NULL) {
		cout << "......Reconstr is NULL" << endl;
		return;
	}

	// Let's Clean the previous fChannelsListbox FGSII add 07/08/2014
	fChannelsListBox->RemoveAll();
	//fill the listbox
	for (rec::Reconstruct::ConstChannelIterator iter = fReconstr->ChanPedestalBegin();iter != fReconstr->ChanPedestalEnd(); ++iter) {
		//fill fChannelsListBox FGSII add 07/08/2014
		TString chan_name = "Ch "; //FGSII
		chan_name += (*iter)->GetId();
  		//fChannelsListBox->AddEntry(chan_name, (*iter)->GetId());
  		fChannelsListBox->AddEntrySort(chan_name, (*iter)->GetId());
		//fChannelsListBox->MapSubwindows();
		fChannelsListBox->Layout();
	}


	for (rec::Reconstruct::ConstChannelIterator iter1 = fReconstr->ChanPedestalBegin();iter1 != fReconstr->ChanPedestalEnd(); ++iter1) {
		//for(evt::Channel::ConstTraceIterator iter2 = (*iter1)->TracesBegin(); iter2 != (*iter1)->TracesEnd(); iter2++){	
		for(evt::Channel::ConstTraceIterator iter2 = fReconstr->GetPedestalChannel((*iter1)->GetId())->TracesBegin(); iter2 != fReconstr->GetPedestalChannel((*iter1)->GetId())->TracesEnd(); iter2++){

			//set histo name
			TString histo_name = "chan_";
			histo_name += (Int_t) (*iter1)->GetId(); 
			histo_name += "_trace_";
			histo_name += (Int_t) (*iter2)->GetTraceId();

			//FGSII mod on 08/27/2014 
			//these conditions were added to fix potential mem leak created by
			//the raw histos being overwritten by the recon histos
			if(fReconstr == NULL){
				histo_name += "_raw_histo"; //FGSII: shouldn't need this
			}
			else if(fReconstr != NULL){
				histo_name += "_ped_histo";
			}

			//set histo title
			TString histo_title = " Channel ";
			histo_title += "Ped Trace "; 
			//FGSII added 05/29/2014
			histo_title += "(Event "; histo_title += (Int_t) fEvent->GetEventNumber();
			histo_title += ") ";
			
			//FGSII 07/18/2014 fixed way that the file name is added to histo title
			string temp_string = fEvent->GetFileName();
			string string_key = ("/");
			unsigned int key_position = temp_string.rfind(string_key);
			string file_name = temp_string.substr(key_position+1,std::string::npos);
			histo_title += TString(file_name);

			fRawHistoVector.insert(std::pair<IdPair,TH1D*> (IdPair((*iter1)->GetId(), (*iter2)->GetTraceId()),
new TH1D(histo_name, histo_title,(*iter2)->GetNofTimeBins(), (*iter2)->GetTraceOffset(),(*iter2)->GetBinDuration()*(*iter2)->GetNofTimeBins() + (*iter2)->GetTraceOffset() ) ) );

			//set length of histo x-axis
			//TAxis* horiz_axis = fRawHistoVector[IdPair((*iter1)->GetId(),(*iter2)->GetTraceId())]->GetXaxis();
         //horiz_axis->SetRangeUser(0.0, fEvent->GetHeader().GetEventTimeLength());

			for (unsigned int ii = 0; ii < (*iter2)->GetNofTimeBins(); ++ii){
				//original code
				fRawHistoVector[IdPair((*iter1)->GetId(),(*iter2)->GetTraceId())]->SetBinContent( ii+1,(*iter2)->GetTimeBin(ii) ); 

			}
		}
	}
}
//-------------------------------------------------------------------//
//FGSII add on 10/15/2014
void MWPCPlots::TracesFillFromFirstAC(){
	cout << "...MWPCPlots::TracesFillFromFirstAC()" << endl;
	// Let's Clean the prev histos
	for (std::map<IdPair,TH1D*>::iterator iter = fRawHistoVector.begin();
		iter != fRawHistoVector.end(); ++iter) {
		delete iter->second;
	}
	fRawHistoVector.clear();
	if (fReconstr == NULL) {
		cout << "......Reconstr is NULL" << endl;
		return;
	}

	// Let's Clean the previous fChannelsListbox FGSII add 07/08/2014
	fChannelsListBox->RemoveAll();
	//fill the listbox
	for (rec::Reconstruct::ConstChannelIterator iter = fReconstr->ChanFirstACBegin();iter != fReconstr->ChanFirstACEnd(); ++iter) {
		//fill fChannelsListBox FGSII add 07/08/2014
		TString chan_name = "Ch "; //FGSII
		chan_name += (*iter)->GetId();
  		//fChannelsListBox->AddEntry(chan_name, (*iter)->GetId());
  		fChannelsListBox->AddEntrySort(chan_name, (*iter)->GetId());
		//fChannelsListBox->MapSubwindows();
		fChannelsListBox->Layout();
	}


	for (rec::Reconstruct::ConstChannelIterator iter1 = fReconstr->ChanFirstACBegin();iter1 != fReconstr->ChanFirstACEnd(); ++iter1) {
		for(evt::Channel::ConstTraceIterator iter2 = fReconstr->GetFirstACChannel((*iter1)->GetId())->TracesBegin(); iter2 != fReconstr->GetFirstACChannel((*iter1)->GetId())->TracesEnd(); iter2++){

			//set histo name
			TString histo_name = "chan_";
			histo_name += (Int_t) (*iter1)->GetId(); 
			histo_name += "_trace_";
			histo_name += (Int_t) (*iter2)->GetTraceId();

			//these conditions were added to fix potential mem leak created by
			//the raw histos being overwritten by the recon histos
			//FGSII comment out 10/30/2014
			/*if(fReconstr == NULL){
				histo_name += "_raw_histo"; //FGSII: shouldn't need this
			}
			else if(fReconstr != NULL){
				histo_name += "_ped_histo";
			}*/
			histo_name += "_firstAC_histo";

			//set histo title
			TString histo_title = " Channel ";
			histo_title += "First AC Trace "; 
			histo_title += "(Event "; histo_title += (Int_t) fEvent->GetEventNumber();
			histo_title += ") ";
			
			//add file name to histo title
			string temp_string = fEvent->GetFileName();
			string string_key = ("/");
			unsigned int key_position = temp_string.rfind(string_key);
			string file_name = temp_string.substr(key_position+1,std::string::npos);
			histo_title += TString(file_name);

			fRawHistoVector.insert(std::pair<IdPair,TH1D*> (IdPair((*iter1)->GetId(), (*iter2)->GetTraceId()),
new TH1D(histo_name, histo_title,(*iter2)->GetNofTimeBins(), (*iter2)->GetTraceOffset(),(*iter2)->GetBinDuration()*(*iter2)->GetNofTimeBins() + (*iter2)->GetTraceOffset() ) ) );

			for (unsigned int ii = 0; ii < (*iter2)->GetNofTimeBins(); ++ii){
				//original code
				fRawHistoVector[IdPair((*iter1)->GetId(),(*iter2)->GetTraceId())]->SetBinContent( ii+1,(*iter2)->GetTimeBin(ii) ); 

			}
		}
	}
}
//-------------------------------------------------------------------//
void MWPCPlots::TracesFillFromSecondAC(){
	cout << "...MWPCPlots::TracesFillFromSecondAC()" << endl;
	// Let's Clean the prev histos
	for (std::map<IdPair,TH1D*>::iterator iter = fRawHistoVector.begin();
		iter != fRawHistoVector.end(); ++iter) {
		delete iter->second;
	}
	fRawHistoVector.clear();
	if (fReconstr == NULL) {
		cout << "......Reconstr is NULL" << endl;
		return;
	}

	// Let's Clean the previous fChannelsListbox FGSII add 07/08/2014
	fChannelsListBox->RemoveAll();
	//fill the listbox
	for (rec::Reconstruct::ConstChannelIterator iter = fReconstr->ChanSecondACBegin();iter != fReconstr->ChanSecondACEnd(); ++iter) {
		//fill fChannelsListBox FGSII add 07/08/2014
		TString chan_name = "Ch "; //FGSII
		chan_name += (*iter)->GetId();
  		//fChannelsListBox->AddEntry(chan_name, (*iter)->GetId());
  		fChannelsListBox->AddEntrySort(chan_name, (*iter)->GetId());
		//fChannelsListBox->MapSubwindows();
		fChannelsListBox->Layout();
	}


	for (rec::Reconstruct::ConstChannelIterator iter1 = fReconstr->ChanSecondACBegin();iter1 != fReconstr->ChanSecondACEnd(); ++iter1) {
		for(evt::Channel::ConstTraceIterator iter2 = fReconstr->GetSecondACChannel((*iter1)->GetId())->TracesBegin(); iter2 != fReconstr->GetSecondACChannel((*iter1)->GetId())->TracesEnd(); iter2++){

			//set histo name
			TString histo_name = "chan_";
			histo_name += (Int_t) (*iter1)->GetId(); 
			histo_name += "_trace_";
			histo_name += (Int_t) (*iter2)->GetTraceId();

			histo_name += "_secondAC_histo";

			//set histo title
			TString histo_title = " Channel ";
			histo_title += "Second AC Trace "; 
			histo_title += "(Event "; histo_title += (Int_t) fEvent->GetEventNumber();
			histo_title += ") ";
			
			//add file name to histo title
			string temp_string = fEvent->GetFileName();
			string string_key = ("/");
			unsigned int key_position = temp_string.rfind(string_key);
			string file_name = temp_string.substr(key_position+1,std::string::npos);
			histo_title += TString(file_name);

			fRawHistoVector.insert(std::pair<IdPair,TH1D*> (IdPair((*iter1)->GetId(), (*iter2)->GetTraceId()),
new TH1D(histo_name, histo_title,(*iter2)->GetNofTimeBins(), (*iter2)->GetTraceOffset(),(*iter2)->GetBinDuration()*(*iter2)->GetNofTimeBins() + (*iter2)->GetTraceOffset() ) ) );

			for (unsigned int ii = 0; ii < (*iter2)->GetNofTimeBins(); ++ii){
				//original code
				fRawHistoVector[IdPair((*iter1)->GetId(),(*iter2)->GetTraceId())]->SetBinContent( ii+1,(*iter2)->GetTimeBin(ii) ); 

			}
		}
	}
}
//-------------------------------------------------------------------//
void MWPCPlots::TracesFillFromCurrent(){
	cout << "...MWPCPlots::TracesFillFromCurrent()" << endl;
	// Let's Clean the prev histos
	for (std::map<IdPair,TH1D*>::iterator iter = fRawHistoVector.begin();
		iter != fRawHistoVector.end(); ++iter) {
		delete iter->second;
	}
	fRawHistoVector.clear();
	if (fReconstr == NULL) {
		cout << "......Reconstr is NULL" << endl;
		return;
	}

	// Let's Clean the previous fChannelsListbox FGSII add 07/08/2014
	fChannelsListBox->RemoveAll();
	//fill the listbox
	for (rec::Reconstruct::ConstChannelIterator iter = fReconstr->ChanCurrentBegin();iter != fReconstr->ChanCurrentEnd(); ++iter) {
		//fill fChannelsListBox FGSII add 07/08/2014
		TString chan_name = "Ch "; //FGSII
		chan_name += (*iter)->GetId();
  		//fChannelsListBox->AddEntry(chan_name, (*iter)->GetId());
  		fChannelsListBox->AddEntrySort(chan_name, (*iter)->GetId());
		//fChannelsListBox->MapSubwindows();
		fChannelsListBox->Layout();
	}


	for (rec::Reconstruct::ConstChannelIterator iter1 = fReconstr->ChanCurrentBegin();iter1 != fReconstr->ChanCurrentEnd(); ++iter1) {
		for(evt::Channel::ConstTraceIterator iter2 = fReconstr->GetCurrentChannel((*iter1)->GetId())->TracesBegin(); iter2 != fReconstr->GetCurrentChannel((*iter1)->GetId())->TracesEnd(); iter2++){

			//set histo name
			TString histo_name = "chan_";
			histo_name += (Int_t) (*iter1)->GetId(); 
			histo_name += "_trace_";
			histo_name += (Int_t) (*iter2)->GetTraceId();

			histo_name += "_current_histo";

			//set histo title
			TString histo_title = " Channel ";
			histo_title += "Current Trace "; 
			histo_title += "(Event "; histo_title += (Int_t) fEvent->GetEventNumber();
			histo_title += ") ";
			
			//add file name to histo title
			string temp_string = fEvent->GetFileName();
			string string_key = ("/");
			unsigned int key_position = temp_string.rfind(string_key);
			string file_name = temp_string.substr(key_position+1,std::string::npos);
			histo_title += TString(file_name);

			fRawHistoVector.insert(std::pair<IdPair,TH1D*> (IdPair((*iter1)->GetId(), (*iter2)->GetTraceId()),
new TH1D(histo_name, histo_title,(*iter2)->GetNofTimeBins(), (*iter2)->GetTraceOffset(),(*iter2)->GetBinDuration()*(*iter2)->GetNofTimeBins() + (*iter2)->GetTraceOffset() ) ) );

			for (unsigned int ii = 0; ii < (*iter2)->GetNofTimeBins(); ++ii){
				//original code
				fRawHistoVector[IdPair((*iter1)->GetId(),(*iter2)->GetTraceId())]->SetBinContent( ii+1,(*iter2)->GetTimeBin(ii) ); 

			}
		}
	}
}
//-------------------------------------------------------------------//
void MWPCPlots::TracesFillFromCharge(){
	cout << "...MWPCPlots::TracesFillFromCharge()" << endl;
	// Let's Clean the prev histos
	for (std::map<IdPair,TH1D*>::iterator iter = fRawHistoVector.begin();
		iter != fRawHistoVector.end(); ++iter) {
		delete iter->second;
	}
	fRawHistoVector.clear();
	if (fReconstr == NULL) {
		cout << "......Reconstr is NULL" << endl;
		return;
	}

	// Let's Clean the previous fChannelsListbox FGSII add 07/08/2014
	fChannelsListBox->RemoveAll();
	//fill the listbox
	for (rec::Reconstruct::ConstChannelIterator iter = fReconstr->ChanChargeBegin();iter != fReconstr->ChanChargeEnd(); ++iter) {
		//fill fChannelsListBox FGSII add 07/08/2014
		TString chan_name = "Ch "; //FGSII
		chan_name += (*iter)->GetId();
  		//fChannelsListBox->AddEntry(chan_name, (*iter)->GetId());
  		fChannelsListBox->AddEntrySort(chan_name, (*iter)->GetId());
		//fChannelsListBox->MapSubwindows();
		fChannelsListBox->Layout();
	}


	for (rec::Reconstruct::ConstChannelIterator iter1 = fReconstr->ChanChargeBegin();iter1 != fReconstr->ChanChargeEnd(); ++iter1) {
		for(evt::Channel::ConstTraceIterator iter2 = fReconstr->GetChargeChannel((*iter1)->GetId())->TracesBegin(); iter2 != fReconstr->GetChargeChannel((*iter1)->GetId())->TracesEnd(); iter2++){

			//set histo name
			TString histo_name = "chan_";
			histo_name += (Int_t) (*iter1)->GetId(); 
			histo_name += "_trace_";
			histo_name += (Int_t) (*iter2)->GetTraceId();

			histo_name += "_charge_histo";

			//set histo title
			TString histo_title = " Channel ";
			histo_title += "Charge Trace "; 
			histo_title += "(Event "; histo_title += (Int_t) fEvent->GetEventNumber();
			histo_title += ") ";
			
			//add file name to histo title
			string temp_string = fEvent->GetFileName();
			string string_key = ("/");
			unsigned int key_position = temp_string.rfind(string_key);
			string file_name = temp_string.substr(key_position+1,std::string::npos);
			histo_title += TString(file_name);

			fRawHistoVector.insert(std::pair<IdPair,TH1D*> (IdPair((*iter1)->GetId(), (*iter2)->GetTraceId()),
new TH1D(histo_name, histo_title,(*iter2)->GetNofTimeBins(), (*iter2)->GetTraceOffset(),(*iter2)->GetBinDuration()*(*iter2)->GetNofTimeBins() + (*iter2)->GetTraceOffset() ) ) );

			for (unsigned int ii = 0; ii < (*iter2)->GetNofTimeBins(); ++ii){
				//original code
				fRawHistoVector[IdPair((*iter1)->GetId(),(*iter2)->GetTraceId())]->SetBinContent( ii+1,(*iter2)->GetTimeBin(ii) ); 

			}
		}
	}
}
//-------------------------------------------------------------------//
void MWPCPlots::MakeSigLines(unsigned int chan_number){
	cout << "......MWPCPlots::MakeSigLines()" << endl;

	if(fEvent == NULL){
		cout << "......Event is NULL" << endl;
		return;
	}

	//Need to iteratre through each item in container for the channel
	double x1_a,y1_a,x2_a,y2_a;
	double x1_b,y1_b,x2_b,y2_b;
	double vert_offset = fOptVertOffsetNumber->GetEntry()->GetNumber();

	//FGSII add 10/27/2014 don't want sig lines to be too small
	double min_offset = 20.0;
	if(vert_offset < min_offset){
		vert_offset = min_offset;
	}

	for(evt::Channel::ConstPedsIterator iter = fEvent->GetChannel(chan_number)->PedsBegin();
		iter != fEvent->GetChannel(chan_number)->PedsEnd(); iter++){
		;
		//x1 = offset + bin_dur*bin_num
		//y1 = bin_value + 0.5*vert_offset
		//coordinates of first line
		x1_a = (*iter).first.second*(fEvent->GetChannel(chan_number)->GetTrace((*iter).first.first)->GetBinDuration())
			+(fEvent->GetChannel(chan_number)->GetTrace((*iter).first.first)->GetTraceOffset());
		x2_a = x1_a;
		//note that the "+1" is added to both of these to account for the underflow bin
		y1_a = fRawHistoVector[IdPair(chan_number,(*iter).first.first)]->GetBinContent((*iter).first.second+1) 
		+ 0.5*vert_offset;
		y2_a = fRawHistoVector[IdPair(chan_number,(*iter).first.first)]->GetBinContent((*iter).first.second+1) 
		- 0.5*vert_offset;
		
		TLine* LinePnt_a = new TLine(x1_a,y1_a,x2_a,y2_a);
		LinePnt_a->SetLineColor(6);
		LinePnt_a->SetLineStyle(1);
		LinePnt_a->SetLineWidth(2);
		fSigLines.push_back(LinePnt_a);	

		//coordinates of second line
		x1_b = (*iter).second.second*(fEvent->GetChannel(chan_number)->GetTrace((*iter).second.first)->GetBinDuration()) +(fEvent->GetChannel(chan_number)->GetTrace((*iter).second.first)->GetTraceOffset());
		x2_b = x1_b;
		//note that the "+1" is added to both of these to account for the underflow bin
		y1_b = fRawHistoVector[IdPair(chan_number,(*iter).second.first)]->GetBinContent((*iter).second.second+1) + 0.5*vert_offset;
		y2_b = fRawHistoVector[IdPair(chan_number,(*iter).second.first)]->GetBinContent((*iter).second.second+1) - 0.5*vert_offset;

		
		TLine* LinePnt_b = new TLine(x1_b,y1_b,x2_b,y2_b);
		LinePnt_b->SetLineColor(6);
		LinePnt_b->SetLineStyle(1);
		LinePnt_b->SetLineWidth(2);
		fSigLines.push_back(LinePnt_b);
				
	}

}
//-------------------------------------------------------------------//
void MWPCPlots::MakeSigLinesWirePairs(unsigned int chan_number){
	cout << "......MWPCPlots::MakeSigLinesWirePairs()" << endl;

	if(fEvent == NULL){
		cout << "......Event is NULL" << endl;
		return;
	}

	//Need to iteratre through each item in container for the channel
	double x1_a,y1_a,x2_a,y2_a;
	double x1_b,y1_b,x2_b,y2_b;
	double vert_offset = fOptVertOffsetNumber->GetEntry()->GetNumber();

	//FGSII add 10/27/2014 don't want sig lines to be too small
	double min_offset = 20.0;
	if(vert_offset < min_offset){
		vert_offset = min_offset;
	}

	for(evt::Channel::ConstPedsIterator iter = fEvent->GetWirePair(chan_number)->
	GetWirePair().first->PedsBegin();iter != fEvent->GetWirePair(chan_number)->
	GetWirePair().first->PedsEnd(); iter++){
		
		//for pos wire****************************************************
		//coordinates of first line
		x1_a = (*iter).first.second*(fEvent->GetWirePair(chan_number)->GetWirePair().first->
			GetTrace((*iter).first.first)->GetBinDuration())
			+(fEvent->GetWirePair(chan_number)->GetWirePair().first->
			GetTrace((*iter).first.first)->GetTraceOffset());
		x2_a = x1_a;
		//note that the "+1" is added to both of these to account for the underflow bin
		y1_a = fRawHistoVector[IdPair(fEvent->GetWirePair(chan_number)->GetPosWireId(),
			(*iter).first.first)]->GetBinContent((*iter).first.second+1) + 0.5*vert_offset;
		y2_a = fRawHistoVector[IdPair(fEvent->GetWirePair(chan_number)->GetPosWireId(),
			(*iter).first.first)]->GetBinContent((*iter).first.second+1) - 0.5*vert_offset;
		
		TLine* LinePntPos_a = new TLine(x1_a,y1_a,x2_a,y2_a);
		LinePntPos_a->SetLineColor(6);
		LinePntPos_a->SetLineStyle(1);
		LinePntPos_a->SetLineWidth(2);
		fSigLines.push_back(LinePntPos_a);	

		//coordinates of second line
		x1_b = (*iter).second.second*(fEvent->GetWirePair(chan_number)->GetWirePair().first->
			GetTrace((*iter).second.first)->GetBinDuration()) 
			+(fEvent->GetWirePair(chan_number)->GetWirePair().first->
			GetTrace((*iter).second.first)->GetTraceOffset());
		x2_b = x1_b;
		//note that the "+1" is added to both of these to account for the underflow bin
		y1_b = fRawHistoVector[IdPair(fEvent->GetWirePair(chan_number)->GetPosWireId(),
			(*iter).second.first)]->GetBinContent((*iter).second.second+1) + 0.5*vert_offset;
		y2_b = fRawHistoVector[IdPair(fEvent->GetWirePair(chan_number)->GetPosWireId(),
			(*iter).second.first)]->GetBinContent((*iter).second.second+1) - 0.5*vert_offset;

		TLine* LinePntPos_b = new TLine(x1_b,y1_b,x2_b,y2_b);
		LinePntPos_b->SetLineColor(6);
		LinePntPos_b->SetLineStyle(1);
		LinePntPos_b->SetLineWidth(2);
		fSigLines.push_back(LinePntPos_b);
		//****************************************************************

		//for neg wire****************************************************
		x1_a = (*iter).first.second*(fEvent->GetWirePair(chan_number)->GetWirePair().second->
			GetTrace((*iter).first.first)->GetBinDuration())
			+(fEvent->GetWirePair(chan_number)->GetWirePair().second->
			GetTrace((*iter).first.first)->GetTraceOffset());
		x2_a = x1_a;
		//note that the "+1" is added to both of these to account for the underflow bin
		y1_a = fRawHistoVector[IdPair(fEvent->GetWirePair(chan_number)->GetNegWireId(),
			(*iter).first.first)]->GetBinContent((*iter).first.second+1) + 0.5*vert_offset;
		y2_a = fRawHistoVector[IdPair(fEvent->GetWirePair(chan_number)->GetNegWireId(),
			(*iter).first.first)]->GetBinContent((*iter).first.second+1) - 0.5*vert_offset;
		
		TLine* LinePntNeg_a = new TLine(x1_a,y1_a,x2_a,y2_a);
		LinePntNeg_a->SetLineColor(6);
		LinePntNeg_a->SetLineStyle(1);
		LinePntNeg_a->SetLineWidth(2);
		fSigLines.push_back(LinePntNeg_a);	

		//coordinates of second line
		x1_b = (*iter).second.second*(fEvent->GetWirePair(chan_number)->GetWirePair().second->
			GetTrace((*iter).second.first)->GetBinDuration()) 
			+(fEvent->GetWirePair(chan_number)->GetWirePair().second->
			GetTrace((*iter).second.first)->GetTraceOffset());
		x2_b = x1_b;
		//note that the "+1" is added to both of these to account for the underflow bin
		y1_b = fRawHistoVector[IdPair(fEvent->GetWirePair(chan_number)->GetNegWireId(),
			(*iter).second.first)]->GetBinContent((*iter).second.second+1) + 0.5*vert_offset;
		y2_b = fRawHistoVector[IdPair(fEvent->GetWirePair(chan_number)->GetNegWireId(),
			(*iter).second.first)]->GetBinContent((*iter).second.second+1) - 0.5*vert_offset;

		TLine* LinePntNeg_b = new TLine(x1_b,y1_b,x2_b,y2_b);
		LinePntNeg_b->SetLineColor(6);
		LinePntNeg_b->SetLineStyle(1);
		LinePntNeg_b->SetLineWidth(2);
		fSigLines.push_back(LinePntNeg_b);
		//****************************************************************
	}
}
//-------------------------------------------------------------------//
void MWPCPlots::ClearSigLines(){
	fSigLines.clear();
}
//-------------------------------------------------------------------//
MWPCPlots::SigLinesIterator MWPCPlots::SigLinesBegin(){
	return fSigLines.begin();
}	
//-------------------------------------------------------------------//
MWPCPlots::SigLinesIterator MWPCPlots::SigLinesEnd(){
	return fSigLines.end();
}
//-------------------------------------------------------------------//
MWPCPlots::ConstSigLinesIterator MWPCPlots::SigLinesBegin() const{
	return fSigLines.begin();
}
//-------------------------------------------------------------------//
MWPCPlots::ConstSigLinesIterator MWPCPlots::SigLinesEnd() const{
	return fSigLines.end();
}



//FGSII add 10/27/2014
//--------------------------------------------------------------------//
//----------BEGINNING OF STUFF TO TO FILL FROM WIRES------------------//
//--------------------------------------------------------------------//
//FGSII add 11/03/2014
//Don't believe I need this
void MWPCPlots::UpdatefReconstr(){
	cout << "...MWPCPlots::UpdatefReconstr()" << endl;

	/*evt::Event* update_event = new evt::Event();
	update_event->MakeClone(fEvent);
	if(fReconstr != NULL){
		fReconstr->SetEvent(update_event);
	}
	//delete update_event;*/

	if(fReconstr != NULL){
		fReconstr->GetEvent()->Clear();
		cout << "......UpdatefReconstr debug1" << endl;
		cout << "......fEvent->GetNofChannels() = " << fEvent->GetNofChannels() << endl;
		fReconstr->GetEvent()->MakeClone(fEvent);
		cout << "......UpdatefReconstr debug2" << endl;
	}

	//----------------------------------
	//first update header

	//update event number

	//update file name

	//update
	
}
//--------------------------------------------------------------------//
void MWPCPlots::ReconUpdateSignalRegionWirePairs(){
	cout << "...MWPCPlots::ReconUpdateSignalRegionWirePairs" << endl;
	if(fEvent == NULL){
		cout << "......Event is NULL" << endl;
		return;
	}

	for (evt::Event::ConstWirePairIterator iter = fEvent->WirePairsBegin();
		iter != fEvent->WirePairsEnd(); ++iter){
		cout << "......Positive Wire........................................" << endl;
		//first update pos wires
		(*iter)->GetWirePair().first->SortTraces();
		//(*iter)->DetermineSigContinuity(); //not yet working
		(*iter)->GetWirePair().first->FindSignalBegin_3_2((*iter)->GetWirePair().first->GetTrace(0));
		//(*iter)->GetWirePair().first->FindSignalEnd1((*iter)->GetWirePair().first->
			//GetTrace((*iter)->GetWirePair().first->GetNofTracesInChan()-1));
		//(*iter)->GetWirePair().first->CalcQuadraticFitParams();

		cout << "......Negative Wire........................................" << endl;
		//now update neg wires
		(*iter)->GetWirePair().second->SortTraces();
		//(*iter)->DetermineSigContinuity(); //not yet working
		(*iter)->GetWirePair().second->FindSignalBegin_3_2((*iter)->GetWirePair().second->GetTrace(0));
		//(*iter)->GetWirePair().second->FindSignalEnd1((*iter)->GetWirePair().second->
			//GetTrace((*iter)->GetWirePair().second->GetNofTracesInChan()-1));
		//(*iter)->GetWirePair().second->CalcQuadraticFitParams();
	}

	//FGSII 11/03/2014
	//UpdatefReconstr();
}
//-------------------------------------------------------------------//
//FGSII add 10/31/2014
void MWPCPlots::ReconUpdatePedestalWirePairs(){
	cout << "...MWPCPlots::ReconUpdatePedestalWirePairs()" << endl;
	if(fEvent == NULL){
		cout << "......Event is NULL" << endl;
		return;
	}
	if(fReconstr == NULL){
		cout << "......Reconstruction is NULL" << endl;
		return;
	}

	for (evt::Event::ConstWirePairIterator iter = fEvent->WirePairsBegin();
		iter != fEvent->WirePairsEnd(); ++iter){

		//first check if ped region is set
		if((*iter)->GetWirePair().first->GetNofPedestals() == 0 ||
			(*iter)->GetWirePair().second->GetNofPedestals() == 0 ){
			ReconUpdateSignalRegionWirePairs();
		}

		//next, check if ped channels have been made
		if(fReconstr->HasPedestalChannel((*iter)->GetPosWireId()) == false ||
			fReconstr->HasPedestalChannel((*iter)->GetNegWireId()) == false ){
			fReconstr->RecPedestalWirePairs((*iter)->GetWirePairId());
		}
	}
}
//-------------------------------------------------------------------//
//FGSII add 11/03/2014
void MWPCPlots::ReconUpdateFirstACWirePairs(){
	cout << "...MWPCPlots::ReconUpdateFirstACWirePairs()" << endl;
	if(fEvent == NULL){
		cout << "......Event is NULL" << endl;
		return;
	}
	if(fReconstr == NULL){
		cout << "......Reconstruction is NULL" << endl;
		return;
	}

	for (evt::Event::ConstWirePairIterator iter = fEvent->WirePairsBegin();
		iter != fEvent->WirePairsEnd(); ++iter){

		//first check if ped region is set
		if((*iter)->GetWirePair().first->GetNofPedestals() == 0 ||
			(*iter)->GetWirePair().second->GetNofPedestals() == 0 ){
			ReconUpdateSignalRegionWirePairs();
		}

		//next, check if ped channels have been made
		if(fReconstr->HasPedestalChannel((*iter)->GetPosWireId()) == false ||
			fReconstr->HasPedestalChannel((*iter)->GetNegWireId()) == false ){
			fReconstr->RecPedestalWirePairs((*iter)->GetWirePairId());
		}

		//next, check if first ac channels have been made
		if(fReconstr->HasFirstACChannel((*iter)->GetPosWireId()) == false ||
			fReconstr->HasFirstACChannel((*iter)->GetNegWireId()) == false ){
			fReconstr->RecFirstACWirePairs((*iter)->GetWirePairId());
		}	
	}
}
//-------------------------------------------------------------------//
//FGSII add 11/03/2014
void MWPCPlots::ReconUpdateSecondACWirePairs(){
	cout << "...MWPCPlots::ReconUpdateSecondACWirePairs()" << endl;
	if(fEvent == NULL){
		cout << "......Event is NULL" << endl;
		return;
	}
	if(fReconstr == NULL){
		cout << "......Reconstruction is NULL" << endl;
		return;
	}

	for (evt::Event::ConstWirePairIterator iter = fEvent->WirePairsBegin();
		iter != fEvent->WirePairsEnd(); ++iter){

		//first check if ped region is set
		if((*iter)->GetWirePair().first->GetNofPedestals() == 0 ||
			(*iter)->GetWirePair().second->GetNofPedestals() == 0 ){
			ReconUpdateSignalRegionWirePairs();
		}

		//next, check if ped channels have been made
		if(fReconstr->HasPedestalChannel((*iter)->GetPosWireId()) == false ||
			fReconstr->HasPedestalChannel((*iter)->GetNegWireId()) == false ){
			fReconstr->RecPedestalWirePairs((*iter)->GetWirePairId());
		}

		//next, check if first ac channels have been made
		if(fReconstr->HasFirstACChannel((*iter)->GetPosWireId()) == false ||
			fReconstr->HasFirstACChannel((*iter)->GetNegWireId()) == false ){
			fReconstr->RecFirstACWirePairs((*iter)->GetWirePairId());
		}

		//next, check if the second ac channels have been made
		if(fReconstr->HasSecondACChannel((*iter)->GetPosWireId()) == false ||
			fReconstr->HasSecondACChannel((*iter)->GetNegWireId()) == false ){
			fReconstr->RecSecondACWirePairs((*iter)->GetWirePairId());
		}		
	}
}
//-------------------------------------------------------------------//
//FGSII add 11/03/2014
void MWPCPlots::ReconUpdateCurrentWirePairs(){
	cout << "...MWPCPlots::ReconUpdateCurrentWirePairs()" << endl;
	if(fEvent == NULL){
		cout << "......Event is NULL" << endl;
		return;
	}
	if(fReconstr == NULL){
		cout << "......Reconstruction is NULL" << endl;
		return;
	}

	for (evt::Event::ConstWirePairIterator iter = fEvent->WirePairsBegin();
		iter != fEvent->WirePairsEnd(); ++iter){

		//first check if ped region is set
		if((*iter)->GetWirePair().first->GetNofPedestals() == 0 ||
			(*iter)->GetWirePair().second->GetNofPedestals() == 0 ){
			ReconUpdateSignalRegionWirePairs();
		}

		//next, check if ped channels have been made
		if(fReconstr->HasPedestalChannel((*iter)->GetPosWireId()) == false ||
			fReconstr->HasPedestalChannel((*iter)->GetNegWireId()) == false ){
			fReconstr->RecPedestalWirePairs((*iter)->GetWirePairId());
		}

		//next, check if first ac channels have been made
		if(fReconstr->HasFirstACChannel((*iter)->GetPosWireId()) == false ||
			fReconstr->HasFirstACChannel((*iter)->GetNegWireId()) == false ){
			fReconstr->RecFirstACWirePairs((*iter)->GetWirePairId());
		}

		//next, check if the second ac channels have been made
		if(fReconstr->HasSecondACChannel((*iter)->GetPosWireId()) == false ||
			fReconstr->HasSecondACChannel((*iter)->GetNegWireId()) == false ){
			fReconstr->RecSecondACWirePairs((*iter)->GetWirePairId());
		}		

		//next, check if the current channels have been made
		if(fReconstr->HasCurrentChannel((*iter)->GetPosWireId()) == false ||
			fReconstr->HasCurrentChannel((*iter)->GetNegWireId()) == false ){
			fReconstr->RecCurrentWirePairs((*iter)->GetWirePairId());
		}			
	}
}
//-------------------------------------------------------------------//
//FGSII add 11/03/2014
void MWPCPlots::ReconUpdateChargeWirePairs(){
	cout << "...MWPCPlots::ReconUpdateChargeWirePairs()" << endl;
	if(fEvent == NULL){
		cout << "......Event is NULL" << endl;
		return;
	}
	if(fReconstr == NULL){
		cout << "......Reconstruction is NULL" << endl;
		return;
	}

	for (evt::Event::ConstWirePairIterator iter = fEvent->WirePairsBegin();
		iter != fEvent->WirePairsEnd(); ++iter){

		//first check if ped region is set
		if((*iter)->GetWirePair().first->GetNofPedestals() == 0 ||
			(*iter)->GetWirePair().second->GetNofPedestals() == 0 ){
			ReconUpdateSignalRegionWirePairs();
		}

		//next, check if ped channels have been made
		if(fReconstr->HasPedestalChannel((*iter)->GetPosWireId()) == false ||
			fReconstr->HasPedestalChannel((*iter)->GetNegWireId()) == false ){
			fReconstr->RecPedestalWirePairs((*iter)->GetWirePairId());
		}

		//next, check if first ac channels have been made
		if(fReconstr->HasFirstACChannel((*iter)->GetPosWireId()) == false ||
			fReconstr->HasFirstACChannel((*iter)->GetNegWireId()) == false ){
			fReconstr->RecFirstACWirePairs((*iter)->GetWirePairId());
		}

		//next, check if the second ac channels have been made
		if(fReconstr->HasSecondACChannel((*iter)->GetPosWireId()) == false ||
			fReconstr->HasSecondACChannel((*iter)->GetNegWireId()) == false ){
			fReconstr->RecSecondACWirePairs((*iter)->GetWirePairId());
		}		

		//next, check if the current channels have been made
		if(fReconstr->HasCurrentChannel((*iter)->GetPosWireId()) == false ||
			fReconstr->HasCurrentChannel((*iter)->GetNegWireId()) == false ){
			fReconstr->RecCurrentWirePairs((*iter)->GetWirePairId());
		}

		//next, check if the charge channels have been made
		if(fReconstr->HasChargeChannel((*iter)->GetPosWireId()) == false ||
			fReconstr->HasChargeChannel((*iter)->GetNegWireId()) == false ){
			fReconstr->RecChargeWirePairs((*iter)->GetWirePairId());
		}			
	}
}
//-------------------------------------------------------------------//
void MWPCPlots::TracesFillFromRawEventWirePairs(){
	cout << "...MWPCPlots::TracesFillFromRawEventWirePairs()" << endl;
	// Let's Clean the prev histos
	//FGSII added next four lines on 07/21/2014 to fix mem leak problem
	for (std::map<IdPair,TH1D*>::iterator iter = fRawHistoVector.begin();
		iter != fRawHistoVector.end(); ++iter) {
		delete iter->second;
	}
	fRawHistoVector.clear();
	if (fEvent == NULL) {
		cout << "......Event is NULL" << endl;
		return;
	}

	// Let's Clean the previous fChannelsListbox FGSII add 07/08/2014
	fChannelsListBox->RemoveAll();
	//fill the listbox
	for (evt::Event::ConstWirePairIterator iter = fEvent->WirePairsBegin();
		iter != fEvent->WirePairsEnd(); ++iter){
		//add positive wire to list box
		TString pos_name = "W ";//W => Wire
		pos_name += (*iter)->GetPosWireId();
		fChannelsListBox->AddEntrySort(pos_name, (*iter)->GetPosWireId());
		//add negative wire to list box
		TString neg_name = "W ";
		neg_name += (Int_t) (*iter)->GetNegWireId();
		fChannelsListBox->AddEntrySort(neg_name, (*iter)->GetNegWireId());

		fChannelsListBox->Layout();
	}	

	for (evt::Event::ConstWirePairIterator iter1 = fEvent->WirePairsBegin();
		iter1 != fEvent->WirePairsEnd(); ++iter1){
		//FIRST FILL FROM POS WIRE-------------------------------------------//
		for (evt::Channel::ConstTraceIterator iter2 = (*iter1)->GetWirePair().first->TracesBegin();
			iter2 != (*iter1)->GetWirePair().first->TracesEnd(); ++iter2){
			
			TString histo_name = "chan_" + (Int_t) (*iter1)->GetWirePair().first->GetId();
			histo_name += "_wire_" + (Int_t) (*iter1)->GetPosWireId();
			histo_name += "_trace_" + (Int_t) (*iter2)->GetTraceId();

			if(fReconstr == NULL){
				histo_name += "_raw_histo";
			}
			else if(fReconstr != NULL){
				histo_name += "_recon_histo";
			}

			TString histo_title = " Wire Raw Trace (Event ";
			histo_title += (Int_t) fEvent->GetEventNumber();
			histo_title += ") ";		
			string temp_string = fEvent->GetFileName();
			string string_key = ("/");
			unsigned int key_position = temp_string.rfind(string_key);
			string file_name = temp_string.substr(key_position+1,std::string::npos);
			histo_title += TString(file_name);

			fRawHistoVector.insert(std::pair<IdPair,TH1D*> (IdPair((*iter1)->GetPosWireId(), 
				(*iter2)->GetTraceId()),new TH1D(histo_name, histo_title,(*iter2)->GetNofTimeBins(),
				(*iter2)->GetTraceOffset(),(*iter2)->GetBinDuration()*(*iter2)->GetNofTimeBins() + 
				(*iter2)->GetTraceOffset() ) ) );

			for (unsigned int ii = 0; ii < (*iter2)->GetNofTimeBins(); ++ii){
				//original code
				fRawHistoVector[IdPair((*iter1)->GetPosWireId(),(*iter2)->GetTraceId())]->SetBinContent( ii+1,(*iter2)->GetTimeBin(ii) ); 
			}
		}
		//NEXT FILL FROM NEG WIRE--------------------------------------------//
		for (evt::Channel::ConstTraceIterator iter2 = (*iter1)->GetWirePair().second->TracesBegin();
			iter2 != (*iter1)->GetWirePair().second->TracesEnd(); ++iter2){
			
			TString histo_name = "chan_" + (Int_t) (*iter1)->GetWirePair().second->GetId();
			histo_name += "_wire_" + (Int_t) (*iter1)->GetNegWireId();
			histo_name += "_trace" + (Int_t) (*iter2)->GetTraceId();

			if(fReconstr == NULL){
				histo_name += "_raw_histo";
			}
			else if(fReconstr != NULL){
				histo_name += "_recon_histo";
			}

			TString histo_title = " Wire Raw Trace (Event ";
			histo_title += fEvent->GetEventNumber();
			histo_title += ") ";		
			string temp_string = fEvent->GetFileName();
			string string_key = ("/");
			unsigned int key_position = temp_string.rfind(string_key);
			string file_name = temp_string.substr(key_position+1,std::string::npos);
			histo_title += TString(file_name);

			fRawHistoVector.insert(std::pair<IdPair,TH1D*> (IdPair((*iter1)->GetNegWireId(), 
				(*iter2)->GetTraceId()),new TH1D(histo_name, histo_title,(*iter2)->GetNofTimeBins(),
				(*iter2)->GetTraceOffset(),(*iter2)->GetBinDuration()*(*iter2)->GetNofTimeBins() + 
				(*iter2)->GetTraceOffset() ) ) );

			for (unsigned int ii = 0; ii < (*iter2)->GetNofTimeBins(); ++ii){
				//original code
				fRawHistoVector[IdPair((*iter1)->GetNegWireId(),(*iter2)->GetTraceId())]->SetBinContent( ii+1,(*iter2)->GetTimeBin(ii) ); 
			}
		}

	}
}
//-------------------------------------------------------------------//
//FGSII add 10/31/2014
void MWPCPlots::TracesFillFromPedestalWirePairs(){
	TracesFillFromPedestal();
}
//-------------------------------------------------------------------//
void MWPCPlots::TracesFillFromFirstACWirePairs(){
	TracesFillFromFirstAC();
} 
//-------------------------------------------------------------------//
void MWPCPlots::TracesFillFromSecondACWirePairs(){
	TracesFillFromSecondAC();
} 
//-------------------------------------------------------------------//
void MWPCPlots::TracesFillFromCurrentWirePairs(){
	TracesFillFromCurrent();
} 
//-------------------------------------------------------------------//
void MWPCPlots::TracesFillFromChargeWirePairs(){
	TracesFillFromCharge();
} 
//-------------------------------------------------------------------//









//-------------------------------------------------------------------//
//-------------------OLD CODE----------------------------------------//
//-------------------------------------------------------------------//
/*void MWPCPlots::TracesUpdateOnCanvas(double vertical_offset){
	cout << "...MWPCPlots::TracesUpdateOnCanvas" << endl;

	//FGSII 09/09/2014, need to have this function depend on rec level
	evt::Event* canvas_event;
	if(fReconstructionLevel == 1){
		if(fEvent != NULL){
			canvas_event = fEvent;
		}
	}
	else if(fReconstructionLevel == 2){ //FGSII add 09/11/2014
		if(fEvent !=NULL){
			canvas_event = fEvent;
		}
	}
	else if(fReconstructionLevel == 3){
		if(fReconstr != NULL){
			canvas_event = fReconstr->GetEvent();
		}
	}
	else if(fReconstructionLevel == 4){
		if(fReconstr != NULL){ //FGSII add 10/15/2014
			canvas_event = fReconstr->GetEvent();
		}
	}
	else if(fReconstructionLevel == 5){
		cout << "......Rec Level 5 not yet available" << endl;
	}
	else if(fReconstructionLevel == 6){
		cout << "......Rec Level 6 not yet available" << endl;
	}
	else if(fReconstructionLevel == 7){
		cout << "......Rec Level 7 not yet available" << endl;
	}
	else{
		cout << "......ERROR: Rec level does not exist" << endl;
	}

	//FGSII 10/15/2014 note very strange seg fault if I remove this line.
	//need to figure this out
	//cout << "............HERE" << endl; //debug
cout << "......Debug1" << endl;

	//FGSII 10/29/2014 was canvas_event, need to figure out why it cause seg fault
	if(canvas_event != NULL && fPreviousEventNumber == 0){
		fPreviousEventNumber = canvas_event->GetEventNumber();	
	}
cout << "......Debug2" << endl;

	//here we add a statement to be able to preserve the x/y scales
	//if we are zoomed in on a plot
	if(fAxisHisto != NULL && fAxisHisto->TestBit(TH1::kIsZoomed) == 1 && canvas_event->GetEventNumber()==
		fPreviousEventNumber){
			fAxisHisto->GetYaxis()->SetRangeUser(fCanvTraces->GetCanvas()->GetFrame()->GetY1(),
				fCanvTraces->GetCanvas()->GetFrame()->GetY2());
			fAxisHisto->GetXaxis()->SetRangeUser(fCanvTraces->GetCanvas()->GetFrame()->GetX1(),
				fCanvTraces->GetCanvas()->GetFrame()->GetX2());
			for (std::map<IdPair,TH1D*>::iterator iter = fRawHistoVector.begin();
				iter != fRawHistoVector.end(); ++iter) {
				iter->second->Draw("SAME ][");
				if ( fCurrentSelectedChannel == iter->first.first ) {
					iter->second->SetLineWidth(1.0);
					iter->second->SetLineColor(2);
				} 
				else {
					iter->second->SetLineWidth(1.0);
					iter->second->SetLineColor(4);
				}
			}
			fCanvTraces->GetCanvas()->Update();
	}
	else{//aaa

	//clean previous fAxisHisto
	if (fAxisHisto != NULL){
		delete fAxisHisto;
		fAxisHisto = NULL;
	}

	//clear fPaves (a container the holds the labels for each channel)
	for (ConstPaveIterator iter = PavesBegin(); iter != PavesEnd(); ++iter){
		if( (*iter) != NULL){
			delete (*iter);
		}
	}
	ClearPaves();//fPaves.clear(); //FGSII mod on 09/22/2014

	//FGSII add 09/22/2014
	//need to clear previous ped region lines
	for(ConstSigLinesIterator iter = SigLinesBegin(); iter != SigLinesEnd(); ++iter){
		if( (*iter) != NULL){
			delete (*iter);
		}
	}
	ClearSigLines();

	fCanvTraces->GetCanvas()->cd();
	fCanvTraces->GetCanvas()->Clear();

	double vert_max_value = 0.0;
	double vert_min_value = 0.0;
	double horiz_min_value = 0.0;
	double horiz_max_value = 0.0;

	double current_trace_offset = 0.0;
	bool init_values = true;

	// AVD: Let's split the cycles: First is creating histograms and
	// calculating min and max values, the second is drawing everything
	for (std::map<IdPair,TH1D*>::iterator iter = fRawHistoVector.begin();
			iter != fRawHistoVector.end(); ++iter) {
		current_trace_offset = (double) vertical_offset * iter->first.first;
		
		for(unsigned long ii = 0; ii < (unsigned int)iter->second->GetNbinsX(); ++ii){
			iter->second->SetBinContent(ii+1, iter->second->GetBinContent(ii+1) +
				current_trace_offset);
		}
		if (init_values){
			vert_max_value = iter->second->GetBinContent(
					iter->second->GetMaximumBin());
			vert_min_value = iter->second->GetBinContent(
					iter->second->GetMinimumBin());
			horiz_max_value = iter->second->GetXaxis()->GetXmax();
			horiz_min_value = iter->second->GetXaxis()->GetXmin();
			init_values = false;
		} 
		else {
			if (vert_max_value < iter->second->GetBinContent(iter->second->GetMaximumBin())){
				vert_max_value = iter->second->GetBinContent(iter->second->GetMaximumBin());
			}
			if (vert_min_value > iter->second->GetBinContent(iter->second->GetMinimumBin())){
				vert_min_value = iter->second->GetBinContent(iter->second->GetMinimumBin());
			}
			if ( horiz_max_value < iter->second->GetXaxis()->GetXmax() ){
				horiz_max_value = iter->second->GetXaxis()->GetXmax();
			}
			if ( horiz_min_value > iter->second->GetXaxis()->GetXmin() ){
				horiz_min_value = iter->second->GetXaxis()->GetXmin();
			}
		}
	}

	//FGSII add 07/28/2014
	fVertOffset = vertical_offset;

	//Here we set up the frame in which the histos will be drawn
	if (fRawHistoVector.size() > 0){
		TString histo_title = "";
		histo_title += "(Event "; histo_title += (Int_t) canvas_event->GetEventNumber();
		histo_title += ") ";
		histo_title += "(";
		histo_title += (Int_t)(canvas_event->GetHeader().GetTime().GetGPSSecond());
		histo_title += ",";
		histo_title += (Int_t)(canvas_event->GetHeader().GetTime().GetGPSNanoSecond());
		histo_title += ") ";
		string temp_string = canvas_event->GetFileName();
		string string_key = ("/");
		unsigned int key_position = temp_string.rfind(string_key);
		string file_name = temp_string.substr(key_position+1,std::string::npos);
		histo_title += TString(file_name);

		// The smarter way is just to draw a frame:
		fAxisHisto = fCanvTraces->GetCanvas()->DrawFrame(
			horiz_min_value, vert_min_value,
			horiz_max_value, vert_max_value);
		fAxisHisto->SetTitle(histo_title);
		
		fAxisHisto->SetXTitle("time (#mus)");

		if(fReconstructionLevel == 1 || fReconstructionLevel == 2){ //FGSII add 09/11/2014
			fAxisHisto->SetYTitle("raw signal (mV)");
		}
		else if(fReconstructionLevel == 3){
			fAxisHisto->SetYTitle("pedestal subtracted signal (mV)");
		}
		else if(fReconstructionLevel == 4){
			fAxisHisto->SetYTitle("signal after 1st AC stage (mV)");
		}
		else if(fReconstructionLevel == 5){
			fAxisHisto->SetYTitle("signal after 2nd AC stage (mV)");
		}
		else if(fReconstructionLevel == 6){
			fAxisHisto->SetYTitle("current at wire (nA)");
		}
		else if(fReconstructionLevel == 7){
			fAxisHisto->SetYTitle("charge (electrons)");
		}
		fAxisHisto->GetYaxis()->SetRangeUser(vert_min_value, vert_max_value);
		fAxisHisto->GetYaxis()->SetRange(vert_min_value, vert_max_value);

		// fAxisHisto->Draw();
	}

	//Here we draw the histos
	for (std::map<IdPair,TH1D*>::iterator iter = fRawHistoVector.begin();
			iter != fRawHistoVector.end(); ++iter) {
		iter->second->Draw("SAME ][");
		if ( fCurrentSelectedChannel == iter->first.first ) {
			iter->second->SetLineWidth(1.0);
			iter->second->SetLineColor(2);
		} 
		else {
			iter->second->SetLineWidth(1.0);
			iter->second->SetLineColor(4);
			
		}
	}

//how to fill paves from channels
if(fEvent != NULL && fLoadFromWirePairsButton->IsOn() == false){
	evt::Event::ConstChannelIterator chan_begin;
	evt::Event::ConstChannelIterator chan_end;
	if(fEvent != NULL){
		if(fReconstructionLevel == 1){
			chan_begin = fEvent->ChannelsBegin();
			chan_end = fEvent->ChannelsEnd();
		}
		else if(fReconstructionLevel == 2){ //FGSII add 09/11/2014
			chan_begin = fEvent->ChannelsBegin();
			chan_end = fEvent->ChannelsEnd();
		}
		else if(fReconstr != NULL && fReconstructionLevel == 3){
			chan_begin = fReconstr->ChanPedestalBegin();
			chan_end = fReconstr->ChanPedestalEnd();
		}
		else if(fReconstructionLevel == 4){
			//FGSII add 10/15/2014
			chan_begin = fReconstr->ChanFirstACBegin();
			chan_end = fReconstr->ChanFirstACEnd();
			//cout << "......Rec Level 4 not yet available" << endl;
		}
		else if(fReconstructionLevel == 5){
			cout << "......Rec Level 6 not yet available" << endl;
		}
		else if(fReconstructionLevel == 6){
			cout << "......Rec Level 7 not yet available" << endl;
		}
		else if(fReconstructionLevel == 7){
			cout << "......Rec Level 7 not yet available" << endl;
		}
		else{
			cout << "......ERROR: Rec level does not exist" << endl;
		}
	}
	//FGSII: Now we need to create a Pave (label) for each channel,
	//the label will go with the trace with greatest offset
	//problems here if offsets are ever negative
	double temp_offset=0;
	unsigned int pave_trace_id;
	double x1,y1;
	if(fRawHistoVector.size() != 0){
		for(evt::Event::ConstChannelIterator iter1 = chan_begin; iter1 != chan_end; ++iter1){
			temp_offset = 0;	
			for(evt::Channel::ConstTraceIterator iter2 = (*iter1)->TracesBegin(); iter2 != (*iter1)->TracesEnd(); ++iter2){
				if((*iter2)->GetTraceOffset() >= temp_offset){
					temp_offset = (*iter2)->GetTraceOffset();
					pave_trace_id = (*iter2)->GetTraceId();
				}
			}
			x1 = fRawHistoVector[IdPair((*iter1)->GetId(),pave_trace_id)]->GetXaxis()->GetXmax();
			y1 = fRawHistoVector[IdPair((*iter1)->GetId(),pave_trace_id)]->GetBinContent(fRawHistoVector[IdPair((*iter1)->GetId(),pave_trace_id)]->GetNbinsX());
			MakePave((*iter1)->GetId(), vertical_offset,horiz_min_value, horiz_max_value, 
				vert_min_value, vert_max_value, x1, y1);
		}
	}
}

//How to fill paves from wires
if(fEvent != NULL && fLoadFromWirePairsButton->IsOn() == true){
	evt::Event::ConstWirePairIterator wires_begin;
	evt::Event::ConstWirePairIterator wires_end;
	if(fEvent != NULL){
		if(fReconstructionLevel == 1){
			wires_begin = fEvent->WirePairsBegin();
			wires_end = fEvent->WirePairsEnd();
		}
		else if(fReconstructionLevel == 2){ //FGSII add 09/11/2014
			wires_begin = fEvent->WirePairsBegin();
			wires_end = fEvent->WirePairsEnd();
		}
		else if(fReconstr != NULL && fReconstructionLevel == 3){
			cout << "......Rec Leve 3 not yet available" << endl;
		}
		else if(fReconstructionLevel == 4){
			cout << "......Rec Level 4 not yet available" << endl;
		}
		else if(fReconstructionLevel == 5){
			cout << "......Rec Level 6 not yet available" << endl;
		}
		else if(fReconstructionLevel == 6){
			cout << "......Rec Level 7 not yet available" << endl;
		}
		else if(fReconstructionLevel == 7){
			cout << "......Rec Level 7 not yet available" << endl;
		}
		else{
			cout << "......ERROR: Rec level does not exist" << endl;
		}
	}
	//FGSII: Now we need to create a Pave (label) for each channel,
	//the label will go with the trace with greatest offset
	//problems here if offsets are ever negative
	double temp_offset=0;
	unsigned int pave_trace_id;
	double x1,y1;
	if(fRawHistoVector.size() != 0){
		for(evt::Event::ConstWirePairIterator iter1 = wires_begin; iter1 != wires_end; ++iter1){
			//make pos wire paves
			temp_offset = 0;	
			for(evt::Channel::ConstTraceIterator iter2 = (*iter1)->GetWirePair().first->TracesBegin();		iter2 != (*iter1)->GetWirePair().first->TracesEnd(); ++iter2){
				if((*iter2)->GetTraceOffset() >= temp_offset){
					temp_offset = (*iter2)->GetTraceOffset();
					pave_trace_id = (*iter2)->GetTraceId();
				}
			}
			x1 = fRawHistoVector[IdPair((*iter1)->GetPosWireId(),pave_trace_id)]->GetXaxis()->GetXmax();
			y1 = fRawHistoVector[IdPair((*iter1)->GetPosWireId(),pave_trace_id)]->GetBinContent(fRawHistoVector[IdPair((*iter1)->GetPosWireId(),pave_trace_id)]->GetNbinsX());
			MakePave((*iter1)->GetPosWireId(), vertical_offset,horiz_min_value, horiz_max_value, 
				vert_min_value, vert_max_value, x1, y1);

			//make neg wire paves
			temp_offset = 0;	
			for(evt::Channel::ConstTraceIterator iter2 = (*iter1)->GetWirePair().second->TracesBegin();		iter2 != (*iter1)->GetWirePair().second->TracesEnd(); ++iter2){
				if((*iter2)->GetTraceOffset() >= temp_offset){
					temp_offset = (*iter2)->GetTraceOffset();
					pave_trace_id = (*iter2)->GetTraceId();
				}
			}
			x1 = fRawHistoVector[IdPair((*iter1)->GetNegWireId(),pave_trace_id)]->GetXaxis()->GetXmax();
			y1 = fRawHistoVector[IdPair((*iter1)->GetNegWireId(),pave_trace_id)]->GetBinContent(fRawHistoVector[IdPair((*iter1)->GetNegWireId(),pave_trace_id)]->GetNbinsX());
			MakePave((*iter1)->GetNegWireId(), vertical_offset,horiz_min_value, horiz_max_value, 
				vert_min_value, vert_max_value, x1, y1);
		}
	}
}


	//FGSII: Now we plot the paves
	if(fRawHistoVector.size() != 0){
		for (ConstPaveIterator iter = PavesBegin(); iter != PavesEnd(); ++iter){
			(*iter)->Draw();
		}
	}

	//FGSII 09/22/2014: if the rec level is 2 (signal region is being found)
	//let's fill fSigLines
	if(fReconstructionLevel == 2){
		if(fEvent != NULL && fLoadFromWirePairsButton->IsOn() == false){
			//iterate through each channel and add pedestal region lines to the container
			for (evt::Event::ConstChannelIterator iter = fEvent->ChannelsBegin(); 
			iter != fEvent->ChannelsEnd(); ++iter) {
				MakeSigLines((*iter)->GetId());
			}
			//now that the ped region lines are created for each channel let's plot them
			for(ConstSigLinesIterator iter = SigLinesBegin(); iter != SigLinesEnd(); ++iter){
				(*iter)->Draw();
			}
			//Let's also draw the quadratic fit for each channel not working as of 09/30/2014
			//for (evt::Event::ConstChannelIterator iter = fEvent->ChannelsBegin(); 
			//iter != fEvent->ChannelsEnd(); ++iter) {
			//	(*iter)->GetQuadraticFit()->Draw();
			//}
		}
		else if(fEvent != NULL && fLoadFromWirePairsButton->IsOn() == true){
			cout << "......Sig lines not yet available when filling from wires" << endl;
			for (evt::Event::ConstWirePairIterator iter = fEvent->WirePairsBegin();
			iter != fEvent->WirePairsEnd(); ++iter){
				MakeSigLinesWirePairs((*iter)->GetWirePair().first->GetId());//could also use second
			}
			//now that the ped region lines are created for each channel let's plot them
			for(ConstSigLinesIterator iter = SigLinesBegin(); iter != SigLinesEnd(); ++iter){
				(*iter)->Draw();
			}
		}

	}

	//FGSII: Let's set the x-axis grid so it is easier to tell where traces begin and end
	fCanvTraces->GetCanvas()->SetGridx();	
	
	//At this point all the raw histograms should be filled with traces
	fCanvTraces->GetCanvas()->Update();

}//aaa
	if(canvas_event != NULL){
		fPreviousEventNumber = canvas_event->GetEventNumber();
	}

	cout << endl;

} */
//-------------------------------------------------------------------//





