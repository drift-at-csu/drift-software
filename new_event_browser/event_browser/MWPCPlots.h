#ifndef _DRIFT_EB_MWPCPlots_h_
#define _DRIFT_EB_MWPCPlots_h_

#include <TGFrame.h>
#include <TString.h>
#include <TMarker.h>
#include <TGButtonGroup.h>
#include <TGButton.h>
#include <TPaveText.h> //FGSII add 07/21/2014
#include <TLine.h> //FGSII add 09/19/2014
//#include <TGraph.h>//FGSII add 09/30/2014

#include <RQ_OBJECT.h> //FGSII add on 07/28/2014

#include "VPlots.h"
#include "EventBrowserSignals.h"

#include <list> //FGSII add on 07/21/2014
#include <vector>
#include <map>

class TCanvas;
class TGWindow;
class TGTab;
class TGHSlider;
class TGCompositeFrame;
class TObjArray;
class TGListBox;
class TRootEmbeddedCanvas;
class TH1D;
class TH1F;
class TGTextMargin;
class TPaveText;//FGSII add 07/21/2014


namespace drift{
namespace evt{
  class Event;
}
namespace det{
  class Detector;
}
namespace rec{
  class Reconstruct;
}
}


class MWPCPlots : public TGFrame, public VPlots {
	//FGSII add 07/28/2014
	RQ_OBJECT("MWPCPlots");	

	//private:
	protected: //FGSII changed to protected on 08/14/2014
		MWPCPlots();
	//FGSII add 07/11/2014
	public:
		//FGSII add 09/19/2014, let's create container to hold lines to plot for signal region
		typedef std::list<TLine*> SigLinesContainer; //container to hold signal region lines
		typedef SigLinesContainer::iterator SigLinesIterator;
		typedef SigLinesContainer::const_iterator ConstSigLinesIterator;

		typedef std::pair<unsigned long, unsigned int> IdPair;
		//FGSII add 07/21/2014, need to add list of paves to display channel number with each trace
		typedef std::list<TPaveText*> PaveContainer;
		typedef PaveContainer::iterator PaveIterator;
		typedef PaveContainer::const_iterator ConstPaveIterator;
	public:
		//constructor for raw data
		MWPCPlots(TGCompositeFrame* main, 
			drift::det::Detector *det_descr
		);	
		//constructor for reconstructed data
		MWPCPlots(TGCompositeFrame* main, 
			drift::det::Detector *det_descr, 
			drift::rec::Reconstruct* reconstr
		);

		~MWPCPlots();

		void SelectChannel();

		//FGSII add 09/19/2014
		void MakeSigLines(unsigned int chan_number);
		void MakeSigLinesWirePairs(unsigned int chan_number);//FGSII add 10/21/2014
		void ClearSigLines();
		SigLinesIterator SigLinesBegin();
		SigLinesIterator SigLinesEnd();
		ConstSigLinesIterator SigLinesBegin() const;
		ConstSigLinesIterator SigLinesEnd() const;
		//void UpdateSigLines();//FGSII add 10/21/2014
		//void UpdateSigLinesWirePairs();//FGSII add 10/21/2014

		//FGSII add on 07/21/2014
		void MakePave(unsigned int channelId, double vert_offset, double hmin, 
			double hmax, double vmin, double vmax, double x1, double y1);
		void ClearPaves();
		PaveIterator PavesBegin();
		PaveIterator PavesEnd();
		ConstPaveIterator PavesBegin() const;
		ConstPaveIterator PavesEnd() const;
		//FGSII add on 07/28/2014
		void UpdatePaves();
		void UpdatePavesWirePairs();//FGSII add 10/21/2014
		void HandleCanvEvents(Int_t event, Int_t px, Int_t py, TObject* sel);
		TRootEmbeddedCanvas* GetfCanvTraces();//FGSII add on 07/31/2014
	
		// Upper level interfaces
		bool ChangeEvent(drift::evt::Event* an_event);

		//private:
	protected: //FGSII changed to protected on 08/14/2014
		void TracesUpdateOnCanvas(double vertical_offset);

		void ReconUpdateSignalRegion(); ///FGSII add 09/11/2014
		void ReconUpdatePedestal(); //FGSII add 08/25/2014
		void ReconUpdateFirstAC(); //FGSII add 10/15/2014
		void ReconUpdateSecondAC();
		void ReconUpdateCurrent();
		void ReconUpdateCharge();
		void TracesFillFromRawEvent();
		void TracesFillFromPedestal(); //FGSII add 08/25/2014
		void TracesFillFromFirstAC(); //FGSII add 10/15/2014
		void TracesFillFromSecondAC();
		void TracesFillFromCurrent();
		void TracesFillFromCharge();

		//FGSII add 10/27/2014
		void ReconUpdateSignalRegionWirePairs();
		void ReconUpdatePedestalWirePairs();
		void ReconUpdateFirstACWirePairs();
		void ReconUpdateSecondACWirePairs();
		void ReconUpdateCurrentWirePairs();
		void ReconUpdateChargeWirePairs();
		void TracesFillFromRawEventWirePairs();
		void TracesFillFromPedestalWirePairs();
		void TracesFillFromFirstACWirePairs();
		void TracesFillFromSecondACWirePairs();
		void TracesFillFromCurrentWirePairs();
		void TracesFillFromChargeWirePairs();

		//FGSII add 11/03/2014
		void UpdatefReconstr();

		// SYSTEM ENTRY for signal handling
		Bool_t ProcessMessage (Long_t msg, Long_t parm1, Long_t parm2);

	public:
		void DoRecLevel(Int_t rec_level);
		void DoVerticalOffset();

	//private: //======== Variables =========//
	protected: //FGSII changed to protected on 08/14/2014
		drift::det::Detector* fDetector;
		drift::rec::Reconstruct* fReconstr;//for reconstruction constructor functions
		drift::evt::Event* fEvent;

		TGTab* fVariablesTab;
		TGListBox* fChannelsListBox;
		TRootEmbeddedCanvas* fCanvTraces;

		//----- Values For "Options tab"
		TGTextMargin *fOptVertOffsetNumber;	
		//----- Values For "Channel Electronics tab"
		TGTextMargin *fElecTransRfNumber;
		TGTextMargin *fElecTransCfNumber;

		TGTextMargin *fElecAC1RfNumber;
		TGTextMargin *fElecAC1RgNumber;
		TGTextMargin *fElecAC1CgNumber;

		TGTextMargin *fElecAC2RfNumber;
		TGTextMargin *fElecAC2RgNumber;
		TGTextMargin *fElecAC2CgNumber;
		//----- Values For "Channel Rec tab"
		TGTextMargin *fRecSignalBegTimeNumber;
		TGTextMargin *fRecSignalEndTimeNumber;

		//FGSII add 10/27/2014, check button so we can load from wires
		TGCheckButton *fLoadFromWirePairsButton;

		//FGSII add 10/30/2014, check button to show zero line on histos
		TGCheckButton *fZeroLineButton;

		//----- Values For Histograms
		//FGSII changed form of fRawHistoVector 07/11/2014
		std::map<IdPair,TH1D*> fRawHistoVector;
		double fChannelsDrawingOffset;
		unsigned long fCurrentSelectedChannel;
		unsigned short fReconstructionLevel;
		TH1F* fAxisHisto;//this is soley for the purpose of setting the axes correctly for plotting
		PaveContainer fPaves; //FGSII add on 07/21/2014
		SigLinesContainer fSigLines; //FGSII add 09/19/2014
	
		//FGSII add 07/28/2014
		double fVertOffset;
		int fPreviousEventNumber;//keep track of as way to update plots

		//FGSII add 10/30/2014
		TLine* fZeroLine;
	
	ClassDef (MWPCPlots, 1);
}; /* class MWPCPlots */
#endif /* _DRIFT_EB_MWPCPlots_h_ */
