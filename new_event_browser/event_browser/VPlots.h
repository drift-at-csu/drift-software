#ifndef _DRIFT_EB_VPlots_h_
#define _DRIFT_EB_VPlots_h_

#include <string>

namespace drift{
namespace evt{
  class Event;
}
}

class VPlots {
  public:
	VPlots():fPlotsUpTodate(false) {}
	virtual ~VPlots() {}

	virtual bool IsUpToDate() { return fPlotsUpTodate; }
	virtual void SetUpToDate(bool b) { fPlotsUpTodate = b; }

	virtual bool ChangeEvent(drift::evt::Event* an_event) {return false; }

  private:
	bool fPlotsUpTodate;
}; // class VPlots
#endif /* _DRIFT_EB_VPlots_h_ */
