#ifndef _DRIFT_EB_EventBrowserConst_h_
#define _DRIFT_EB_EventBrowserConst_h_

namespace view {
  namespace Consts {

    // some frequently used constants and unit conversions
    const double kSpeedOfLight = 0.299792458; // in units if m/ns
    const double kFDSpeedOfLight = kSpeedOfLight*100.; // in units if m/100 ns
    const double degree = 1.74532925199432955e-02;
    const double km = 1000;

    // alpha from Astroparticle Physics 24 (2006) 421
    const double kdEdXPerElectron = 2.55827e-9; // at s=1, units: PeV/(g/cm^2)

    // Initial offset between GPS and UTC times
    // (1 January 1970 to 6 January 1980 = 10 years, 5 days, 2 leap-days)
    const int    kGPS_UTC_Offset =  315957587;
    //(10*365 + 7)*24*3600 -13- 2*3600;
    //13 seconds missing and 2 hours

    // Gaisser-Hillas TF1 string
    const std::string gHFunctionString("[0]*pow((x-[1])/([2]-[1]),([2]-[1])/[3])"
                                       "*exp(([2]-x)/[3])");

    // FD time-fit function TF1 string
    const std::string gTimeFitFunctionString("[0]/[3]*tan(([1]-"
                                             "TMath::Pi()*x/180.)/2.)+[2]");
  }
}

#endif	/* _DRIFT_EB_EventBrowserConst_h_  */
