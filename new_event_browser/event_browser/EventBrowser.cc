#include "EventBrowser.h"
#include "EventBrowserMenu.h"
#include "EventBrowserSignals.h"
#include "VPlots.h"
#include "MWPCPlots.h"
#include "StatusBar.h"

#include <TROOT.h>
#include <TStyle.h>
#include <TSystem.h>
#include <TCanvas.h>
#include <TRootEmbeddedCanvas.h>//FGSII add on 07/31/2014
#include <TChain.h>
#include <TApplication.h>
#include <TLatex.h>
#include <TString.h>
#include <KeySymbols.h>

#include <TGFrame.h>
#include <TGTab.h>
#include <TGFileDialog.h>
#include <TGComboBox.h>
#include <TGNumberEntry.h>
#include <TGMsgBox.h>

#include <iostream>
#include <vector>
#include <sstream>
#include <cstdlib>
#include <algorithm>

using namespace std;


#include <evt/Event.h>
#include <det/Detector.h>
#include <rec/Reconstruct.h>
using namespace drift;

ClassImp (EventBrowser);
//-------------------------------------------------------------------//
string EventBrowser::GetVersion() {
  return string("trunk");
}
//-------------------------------------------------------------------//
string EventBrowser::GetRevision() {
  return string("0.1");
}
//-------------------------------------------------------------------//
string EventBrowser::GetDate() {
  return string("2014-07");
}
//-------------------------------------------------------------------//
void EventBrowser::Welcome() {
cout << "__/\\\\\\\\\\\\\\\\\\\\\\\\______/\\\\\\\\\\\\\\\\\\____/\\\\\\\\\\";
cout << "\\\\\\\\\\\\_/\\\\\\\\\\\\\\\\\\\\\\\\\\/\\\\\\\\\\\\\\\\\\\\\\\\\\\\_______" << endl;
cout << "__\\/\\\\\\////////\\\\\\__/\\\\\\///////\\\\\\_\\/////\\\\\\///_\\/";
cout << "\\\\\\/////////\\//////\\\\\\/////_______" << endl;
cout << "___\\/\\\\\\______\\//\\\\\\\\/\\\\\\_____\\/\\\\\\_____\\/\\\\\\____";
cout << "\\/\\\\\\______________\\/\\\\\\___________" << endl;
cout << "____\\/\\\\\\_______\\/\\\\\\\\/\\\\\\\\\\\\\\\\\\\\\\/______\\/\\\\\\____\\/";
cout << "\\\\\\\\\\\\\\\\\\\\\\______\\/\\\\\\__________"<< endl;
cout << "_____\\/\\\\\\_______\\/\\\\\\\\/\\\\\\//////\\\\\\______\\/\\\\\\____\\/";
cout << "\\\\\\///////_______\\/\\\\\\_________" << endl;
cout << "______\\/\\\\\\_______\\/\\\\\\\\/\\\\\\____\\//\\\\\\_____";
cout << "\\/\\\\\\____\\/\\\\\\______________\\/\\\\\\________" << endl;
cout << "_______\\/\\\\\\_______/\\\\\\_\\/\\\\\\_____\\//\\\\\\____";
cout << "\\/\\\\\\____\\/\\\\\\______________\\/\\\\\\_______" << endl;
cout << "________\\/\\\\\\\\\\\\\\\\\\\\\\\\/__\\/\\\\\\______\\//\\\\\\/";
cout << "\\\\\\\\\\\\\\\\\\\\\\\\/\\\\\\______________\\/\\\\\\______" << endl;
cout << "_________\\////////////____\\///________\\///\\";
cout << "///////////_\\///_______________\\///______" << endl;
cout << "__________                  _____  ________" ;
cout << "   Version: " << GetVersion() << " rev" << GetRevision() << endl;
cout << "___  ____/__   _______________  /_ ___  __ )";
cout << "______________      ____________________" << endl;
cout << "__  __/  __ | / /  _ \\_  __ \\  __/ __  __  ";
cout << "|_  __/  __ \\_ | /| / /_  ___/  _ \\_  __/" << endl;
cout << "_  /___  __ |/ //  __/  / / / /_   _  /_/ /";
cout << "_  /  / /_/ /_ |/ |/ /_(__  )/  __/  /" << endl;
cout << "/_____/  _____/ \\___//_/ /_/\\__/   /_____/";
cout << " /_/   \\____/____/|__/ /____/ \\___//_/" << endl;
}
//-------------------------------------------------------------------//
EventBrowser::EventBrowser(const TGWindow * p,
                           UInt_t width, UInt_t height,
			   det::Detector* det_descr
			   ,rec::Reconstruct *reconstructor
				) :
		TGMainFrame(p, width, height),
		fEvent(NULL), fTotEvent(0), fCurrentEventNumber(0),
		fDetector(det_descr),
		fReconstr(reconstructor),
		fMenu(NULL),
		fStatusBar(NULL),
		fMWPCPlots(NULL), fMWPCFrame(NULL), fMWPCTabIndex(0),
		fTabs(NULL){
  Welcome();

  // menu, auger tab, status bar
  const UInt_t menuHeight = 25;
  const UInt_t statusBarHeight = 25;
  const int tmpHeight = (int)height - (int)menuHeight - (int)statusBarHeight;

  fMenu = new EventBrowserMenu(this);
  fTabs = new TGTab(this, width, tmpHeight);

	//FGSII add 09/24/2014
	fMenu->GetSortButton1()->Connect("Pressed()", "EventBrowser", this, "CreateEventSortingMap()");

  TGLayoutHints* tabLayout =
    new TGLayoutHints(kLHintsExpandX|kLHintsExpandY|kLHintsCenterX|kLHintsCenterY, 3, 3, 3, 3);
  AddFrame(fTabs, tabLayout);

  fMWPCTabIndex = fTabs->GetNumberOfTabs();
  fMWPCFrame  = fTabs->AddTab("MWPC Raw Traces");//was "MWPC Plane" FGSII 07/18/2014
  fReconFrame = fTabs->AddTab("Reconstruction"); //FGSII add 07/30/2014
  //fMWPCPlots  = new MWPCPlots(fMWPCFrame, fDetector, fReconstr);
  fMWPCPlots = new MWPCPlots(fMWPCFrame, fDetector);
  fMWPCReconPlots = new MWPCPlots(fReconFrame, fDetector, fReconstr); //FGSII add 08/14/2014

  fPlotVector.push_back(fMWPCPlots);
  fPlotVector.push_back(fMWPCReconPlots); //FGSII add 08/14/2014


  fStatusBar = new StatusBar(this, width, statusBarHeight);

	//FGSII add 07/31/2014, connect traces canvas to a function to update coords in status bar
	fMWPCPlots->GetfCanvTraces()->GetCanvas()->Connect("ProcessedEvent(Int_t,Int_t,Int_t,TObject*)", "EventBrowser", this, "HandleTracesCanvasEvents(Int_t,Int_t,Int_t,TObject*)");
	fMWPCReconPlots->GetfCanvTraces()->GetCanvas()->Connect("ProcessedEvent(Int_t,Int_t,Int_t,TObject*)", "EventBrowser", this, "HandleTracesCanvasEvents(Int_t,Int_t,Int_t,TObject*)"); //FGSII add 08/14/2014


  MapSubwindows();
  Resize (GetDefaultSize());
  MapWindow();

  // lets go ...
  SetWindowName("EventBrowser");
  InitKeyStrokes();
}
//-------------------------------------------------------------------//
EventBrowser::~EventBrowser() {
  delete fTabs;
  delete fMenu;
//  delete fStatusBar;
}
//-------------------------------------------------------------------//
bool EventBrowser::HandleKey(Event_t *event) {
	unsigned int keysym;
	char str[2];

	gVirtualX->LookupString(event, str, sizeof(str), keysym);

	if (event->fType == kGKeyPress) {
 		EKeySym theKeyPressed = (EKeySym) keysym;
		if (theKeyPressed == kKey_q || theKeyPressed == kKey_Q)
			CloseWindow();
		else if (theKeyPressed == kKey_Escape)
      CloseWindow();
		else if (theKeyPressed == kKey_Down || theKeyPressed == kKey_PageDown)
			ChangeEvent(eNextEvent);
		else if (theKeyPressed == kKey_Up || theKeyPressed == kKey_PageUp)
			ChangeEvent(ePreviousEvent);
		else if (theKeyPressed == kKey_Home)
			ChangeEvent(eFirstEvent);
		else if (theKeyPressed == kKey_End)
			ChangeEvent(eLastEvent);
	}
	return kTRUE;
}
//-------------------------------------------------------------------//
Bool_t EventBrowser::ProcessMessage(Long_t msg, Long_t parm1, Long_t parm2) {
  switch (GET_MSG (msg)) {
  case kC_COMMAND:
	switch (GET_SUBMSG (msg)) {
	case kCM_TAB: {
		// if (!fEventInfo.empty()) UpdateTab(parm1);
		if (parm1 == 0){
			cout << endl;
			cout << "...Changed tab: MWPC Raw Traces" << endl;
		}
		else if( parm1 == 1){
			cout << endl;
			cout << "...Changed tab: Reconstruction" << endl;
		}
		
		//cout << "......tab event occurred" << endl; //FGSII 08/26/2014 for debug
		//cout << msg << " " << parm1 << " " << parm2 << endl; //FGSII 08/26/2014 for debug
		
	} break;
	case kCM_BUTTON: {
		//cout << "......button pressed, parm1 = " << parm1 << " parm2 = " << parm2 << endl; //debug
		if (parm1 == eNextEvent){
			ChangeEvent(eNextEvent);
		}
		else if (parm1 == ePreviousEvent)
			ChangeEvent(ePreviousEvent);
		else if (parm1 == eFirstEvent)
			ChangeEvent(eFirstEvent);
		else if (parm1 == eLastEvent)
			ChangeEvent(eLastEvent);
		else if (parm1 == eTypedEvent){
			if(parm2 == 0){
				ChangeEvent(eNextEvent);
			}
			else if(parm2 == 10000){
				ChangeEvent(ePreviousEvent);
			}
		}
	} break;
	case kCM_MENU:
	switch (parm1) {
		// ------------------- THE FILE MENU ----------------
		case eFileOpen: {
			TString directory(".");
			TGFileInfo fi;
			fi.fIniDir  = StrDup(directory.Data());
			const char * rootFiles[] = { "DRIFT Raw Traces files",   "*.drt" ,
				"All files",    "*",
				0,              0 };
			fi.fFileTypes   = rootFiles;
			new TGFileDialog(gClient->GetRoot(), this, kFDOpen, &fi);
			if(fi.fFilename){
				OpenDRTFile(string(fi.fFilename));
			}
		} break;
		case eFileLive: {//FGSII added this case
			//FGSII
			//copy of code above for non-live files
			//this works, but I should probably look at it more carefully
			TString directory(".");
			TGFileInfo fi;
			fi.fIniDir = StrDup(directory.Data());
			const char * rootFiles[] = { "DRIFT Raw Traces files", "*.drt", "All files", "*", 0, 0};
			fi.fFileTypes = rootFiles;
			new TGFileDialog(gClient->GetRoot(), this, kFDOpen, &fi);
			while(1){
				OpenDRTFileLive(string(fi.fFilename));
				sleep(5);
			}
		} break;

		case eFileQuit:
			CloseWindow();
		break;
		// ------------------- THE HELP MENU ----------------
		case eAbout: {
			EMsgBoxIcon icontype = kMBIconAsterisk;
			EMsgBoxButton buttonType = kMBOk;
			Int_t retval;

			ostringstream info;
			info << "DRIFT EventBrowser " << endl
				<< "Version: rev " << GetRevision() << " from "
				<< GetDate() << endl
				<< "Compiled with ROOT v" << gROOT->GetVersion() << endl
				<< "Authors: Alexei Dorofeev" << endl
				<< "         Fred Schuckman II" << endl;
			
			new TGMsgBox(gClient->GetRoot(), gClient->GetRoot(),
				"About",info.str().c_str(),
				icontype, buttonType, &retval);
		} break;
		}//switch (parm1)
	}//second switch
	
	//FGSII added 06/04/2014 
	case kC_TEXTENTRY:
		switch (GET_SUBMSG (msg)) {
			case kTE_ENTER: {
				if(parm1 == eTypedEvent){
					//fStatusBar->DoEventChange();
					fCurrentEventNumber = fStatusBar->GetEventNumber();
					cout << "\n...EventBrowser::ProcessMessage Event Number " << fCurrentEventNumber << " was entered" << endl;
					ChangeEvent(eTypedEvent);
					fStatusBar->ResetEventNumber();//after pressing return Event Number
					//is reset to 0 in the text box
				}
			}
		}
	
	}//first switch
  return kTRUE;
  
}
//-------------------------------------------------------------------//
void EventBrowser::CloseWindow() {
  gSystem->ExitLoop();
}
//-------------------------------------------------------------------//
void EventBrowser::UpdateBrowser() {
  const int currentTab = fTabs->GetCurrent();

  // reset everything
  for (unsigned int i=0; i<fPlotVector.size(); ++i) {
     //fPlotVector[i]->Clear();
     //fPlotVector[i]->SetUpToDate(false);
  }
  
  // ... stay on current tab if active ...
  if (fTabs->IsEnabled(currentTab)) {
    UpdateTab(currentTab);
    return;
  }

}
//-------------------------------------------------------------------//
void EventBrowser::UpdateTab(Long_t iTab) {
  if (iTab < (Long_t) fPlotVector.size()) {
	if (!fPlotVector[iTab]->IsUpToDate()) {
		// fPlotVector[iTab]->Update();
		// fPlotVector[iTab]->SetUpToDate(true);
	}
  }
}
//-------------------------------------------------------------------//
void EventBrowser::UpdateTabsOnOpenFile() {
	cout << "...EventBrowser::UpdateTabsOnOpenFile" << endl;
	//cout << "......fTotEvent = " << fTotEvent << endl;//FGSII
  //cout << "......eCurrentEvent = " << eCurrentEvent << endl;//FGSII
  ChangeEvent(eCurrentEvent);
}
//-------------------------------------------------------------------//
/*void EventBrowser::ReadEventList() {
}*/ //FGSII got rid of this for now
//-------------------------------------------------------------------//
/*
//pre-09/22/2014 version of function
void EventBrowser::OpenDRTFile(std::string file_name){

	cout <<"...EventBrowser::OpenDRTFile" << endl;
	
	std::string magic_name = "use_initial_file";//FGSII
	
	if(file_name != magic_name){//FGSII added if condition to this stuff
 	fFileName = file_name;
 	
 	//FGSII just added 04/23/2014 at 15:13 to get fTotEvent
	fEvent = NULL;
	fEvent = new evt::Event();
 	fTotEvent = fEvent->GetNofEvents(fFileName);//FGSII

	cout << "...EventBrowser::OpenDRTFile" << endl;
 	cout << "......fTotEvent = " << fTotEvent << endl;
 	
 	cout << "......Opening file \"" << file_name  << "\"" << endl;
  fCurrentEventNumber = 1;
  // Delete the old Event
  if (fEvent != NULL) delete fEvent; 
  fEvent = NULL;
  fEvent = new evt::Event();
  if ( !fEvent->LoadFromDRTFile(file_name, fCurrentEventNumber) ) {
  	// This means we couldn't load the file
	fCurrentEventNumber = 0;
	delete fEvent; fEvent = NULL;
	return;
  }
  
  cout << "...EventBrowser::OpenDRTFile" << endl;
  cout << "......Loaded event " << fCurrentEventNumber << " from file" << endl;
  
  //FGSII added 06/05/2014, trying to display chan nums present in event
 	for(unsigned int j=0; j<=fgMaxNumChans; j++){
 		if(fEvent->HasChannel(j) == true){
 			cout << "......------------------------------Channel " << j << " is present in Event" << endl;
 		}
 		//cout << "......fEvent->HasChannel() " << fEvent->HasChannel(j) << endl;
 	}
 	
  UpdateTabsOnOpenFile(); //FGSII
  }
  
	if(file_name == magic_name){//FGSII all of if statement
		fEvent = NULL;
		fEvent = new evt::Event();
		fTotEvent = fEvent->GetNofEvents(fFileName);//FGSII add 07/18/2014 to update total number of events
		fEvent->LoadFromDRTFile(fFileName,fCurrentEventNumber);
		cout << "...EventBrowser::OpenDRTFile" << endl;
		cout << "......Loaded event # " << fCurrentEventNumber << " from file" << endl;
		
	//FGSII added 06/05/2014, trying to display chan nums present in event
 	for(unsigned int j=1; j<=fgMaxNumChans; j++){
 		if(fEvent->HasChannel(j) == true){
 			cout << "......------------------------------Channel " << j << " is present in Event" << endl;
 		}
 		//cout << "......fEvent->HasChannel() " << fEvent->HasChannel(j) << endl;
 	}
		
		//UpdateTabsOnOpenFile();
		return;
	}
  
}
*/
//-------------------------------------------------------------------//
//new version of function 09/22/2014
void EventBrowser::OpenDRTFile(std::string file_name){

	cout <<"...EventBrowser::OpenDRTFile" << endl;

	std::string magic_name = "use_initial_file";//FGSII

	unsigned int event_num_to_use;
	
	if(file_name != magic_name){//if1_begin
		fFileName = file_name;
 	
		
		//FGSII added 10/05/2014
		fCurrentEventNumber = 1;
		fEvent = NULL;
		fEvent = new evt::Event();
		fEvent->LoadFromDRTFile(fFileName,fCurrentEventNumber);
		if(EventIsSpark() == true){
			//ChangeEvent(eNextEvent);
			cout << "......Event is Spark" << endl;
		}
		else{
			cout << "......Event is not Spark" << endl;
		}
		

		//FGSII just added 04/23/2014 at 15:13 to get fTotEvent
		fEvent = NULL;
		fEvent = new evt::Event();
		fTotEvent = fEvent->GetNofEvents(fFileName);//FGSII

		cout << "...EventBrowser::OpenDRTFile" << endl;
		cout << "......fTotEvent = " << fTotEvent << endl;
 	
		cout << "......Opening file \"" << file_name  << "\"" << endl;
		fCurrentEventNumber = 1;
		// Delete the old Event
		if (fEvent != NULL) delete fEvent; 
		fEvent = NULL;
		fEvent = new evt::Event();
		//if ( !fEvent->LoadFromDRTFile(file_name, fCurrentEventNumber) ) { //FGSII comment out 09/23/2014
		if(fMenu->GetSortButton2()->IsOn() ){
			event_num_to_use = GetEvtChron(fCurrentEventNumber-1).first;
		}
		else{
			event_num_to_use = fCurrentEventNumber;
		}
		//if( !fEvent->LoadFromDRTFile(file_name,GetEvtChron(fCurrentEventNumber-1).first) ){
		if( !fEvent->LoadFromDRTFile(file_name, event_num_to_use) ){
			// This means we couldn't load the file
			fCurrentEventNumber = 0;
			delete fEvent; fEvent = NULL;
			return;
		}
  
		cout << "...EventBrowser::OpenDRTFile" << endl;
		cout << "......Loaded event " << fCurrentEventNumber << " from file" << endl;
  
		//FGSII added 06/05/2014, trying to display chan nums present in event
		//FGSII comment out 10/06/2014
		/*
		for(unsigned int j=0; j<=fgMaxNumChans; j++){
			if(fEvent->HasChannel(j) == true){
				cout << "......------------------------------Channel " << j << " is present in Event" << endl;
			}
		}
		*/
 	
		UpdateTabsOnOpenFile(); //FGSII
	}//if1_end
  
	if(file_name == magic_name){//if2_begin

		//FGSII added 10/05/2014
		fEvent = NULL;
		fEvent = new evt::Event();
		fEvent->LoadFromDRTFile(fFileName,fCurrentEventNumber);
		if(EventIsSpark() == true){
			//ChangeEvent(eNextEvent);
			cout << "......Event is Spark" << endl;
		}
		else{
			cout << "......Event is not Spark" << endl;
		}

		fEvent = NULL;
		fEvent = new evt::Event();
		fTotEvent = fEvent->GetNofEvents(fFileName);//FGSII add 07/18/2014 to update total number of events
		//fEvent->LoadFromDRTFile(fFileName,fCurrentEventNumber); //FGSII comment out 09/23/2014
		if(fMenu->GetSortButton2()->IsOn()){
			event_num_to_use = GetEvtChron(fCurrentEventNumber-1).first;
		}
		else{
			event_num_to_use = fCurrentEventNumber;
		}
		//fEvent->LoadFromDRTFile(fFileName,GetEvtChron(fCurrentEventNumber-1).first);
		fEvent->LoadFromDRTFile(fFileName, event_num_to_use);
		cout << "...EventBrowser::OpenDRTFile" << endl;
		cout << "......Loaded event # " << fCurrentEventNumber << " from file" << endl;
		
		//FGSII added 06/05/2014, trying to display chan nums present in event
		//FGSII comment out 10/06/2014
		/*for(unsigned int j=1; j<=fgMaxNumChans; j++){
			if(fEvent->HasChannel(j) == true){
				cout << "......------------------------------Channel " << j << " is present in Event" << endl;
			}
		}*/
		
		//UpdateTabsOnOpenFile();
		return;
	}//if2_end
  
}
//-------------------------------------------------------------------//
void EventBrowser::OpenDRTFileLive(std::string live_file){//FGSII added

	cout << "...EventBroswer::OpenDRTFileLive" << endl;	
	
	//FGSII added 06/03/2014
	//set fFileName so it can be used in title on histos
	fFileName = live_file;

	//first get total number of events in the file
	fEvent = NULL;
	fEvent = new evt::Event();
 	fTotEvent = fEvent->GetNofEvents(live_file);
 	cout << "......fTotEvent = " << fTotEvent << endl;
 	
 	//next open last event in file
 	cout << "......Opening file \"" << live_file  << "\"" << endl;
  fCurrentEventNumber = fTotEvent;
  // Delete the old Event
  if (fEvent != NULL) delete fEvent; 
  fEvent = NULL;
  fEvent = new evt::Event();
  if ( !fEvent->LoadFromDRTFile(live_file, fCurrentEventNumber) ) {
  	// This means we couldn't load the file
	fCurrentEventNumber = 0;
	delete fEvent; fEvent = NULL;
	return;
  }
  cout << "......Loaded event # " << fCurrentEventNumber << " from file" << endl;
  
  //FGSII added 06/05/2014, trying to display chan nums present in event
	//FGSII comment out 10/06/2014
	/*
 	for(unsigned int j=1; j<=fgMaxNumChans; j++){
 		if(fEvent->HasChannel(j) == true){
 			cout << "......------------------------------Channel " << j << " is present in Event" << endl;
 		}
 		//cout << "......fEvent->HasChannel() " << fEvent->HasChannel(j) << endl;
 	}
	*/
  
  
  UpdateTabsOnOpenFile(); 
  
	return;
}

//-------------------------------------------------------------------//
/*void EventBrowser::ChangeTab(Long_t where) {
}*/ //FGSII got rid of this for now
//-------------------------------------------------------------------//
void EventBrowser::InitKeyStrokes() {
  BindKey((const TGWindow*)this, gVirtualX->KeysymToKeycode(kKey_Q), kAnyModifier);
  BindKey((const TGWindow*)this, gVirtualX->KeysymToKeycode(kKey_Escape), kAnyModifier);
  BindKey((const TGWindow*)this, gVirtualX->KeysymToKeycode(kKey_Down),kAnyModifier);
  BindKey((const TGWindow*)this, gVirtualX->KeysymToKeycode(kKey_Up), kAnyModifier);
  BindKey((const TGWindow*)this, gVirtualX->KeysymToKeycode(kKey_PageDown),kAnyModifier);
  BindKey((const TGWindow*)this, gVirtualX->KeysymToKeycode(kKey_PageUp), kAnyModifier);
  BindKey((const TGWindow*)this, gVirtualX->KeysymToKeycode(kKey_Home),kAnyModifier);
  BindKey((const TGWindow*)this, gVirtualX->KeysymToKeycode(kKey_End), kAnyModifier);

  //FGSII: added 07/25/2014
  //BindKey((const TGWindow*)this, gVirtualX->KeysymToKeycode(kKey_Left), kAnyModifier);
  //BindKey((const TGWindow*)this, gVirtualX->KeysymToKeycode(kKey_Right), kAnyModifier);
	
}
//-------------------------------------------------------------------//
bool EventBrowser::ChangeEvent(Long_t enum_val) {
  cout << "...EventBrowser::ChangeEvent " << endl;
  cout << "......enum_val = " << enum_val << endl;//FGSII
  
  //set current event number in fEvent
  //used simply to pass to MWPCPlots to plot event number on histogram
  //use: fEvent->SetEventNumber(fCurrentEventNumber);//FGSII, added 05/29/2014
  
  //FGSII added 05/30/2014 to set file name in fEvent so it can be
 	//printed in histogram title
 	//use: fEvent->SetFileName(fFileName);
  
  if(enum_val == eCurrentEvent){//FGSII
  	for (unsigned long ii = 0; ii < fPlotVector.size();++ii){
  	//cout << "......fPlotVector.size() = " << fPlotVector.size() << endl; //FGSII
  		if (fPlotVector[ii] != NULL) {
  			 fEvent->SetEventNumber(fCurrentEventNumber);//FGSII, added 05/29/2014
  			 fEvent->SetFileName(fFileName);//FGSII added 05/30/2014
  			fPlotVector[ii]->ChangeEvent(fEvent);
  			//cout << "......fPlotVector[" << ii << "] != NULL" << endl; //FGSII
			}
  	}
	}
	
	if(enum_val == eNextEvent){ //FGSII all of if statement
		
		//FGSII add 07/18/2014
		//first we update fTotEvent;
		evt::Event* temp_Event = NULL;
		temp_Event = new evt::Event();
		int temp_TotEvent = temp_Event->GetNofEvents(fFileName);
		delete temp_Event;
		fTotEvent = temp_TotEvent;

		if(fCurrentEventNumber == fTotEvent){
			cout << "......This is the last event in file" << endl;
			return true;
		}
		
		std::string magic_name = "use_initial_file";
		fCurrentEventNumber = fCurrentEventNumber + 1;//FGSII
		OpenDRTFile(magic_name);
		
		for (unsigned long ii = 0; ii < fPlotVector.size();++ii){
  		//cout << "......fPlotVector.size() = " << fPlotVector.size() << endl; //FGSII
  		if (fPlotVector[ii] != NULL) {
  		  fEvent->SetEventNumber(fCurrentEventNumber);//FGSII, added 05/29/2014
  			fEvent->SetFileName(fFileName);//FGSII added 05/30/2014
  			fPlotVector[ii]->ChangeEvent(fEvent);
  			//cout << "......fPlotVector[" << ii << "] != NULL" << endl; //FGSII
			}
  	}
	}
	
	if(enum_val == ePreviousEvent){ //FGSII all of if statement
		std::string magic_name = "use_initial_file";
		
		if(fCurrentEventNumber > 1){
			fCurrentEventNumber = fCurrentEventNumber - 1;//FGSII
			OpenDRTFile(magic_name);
		
			for (unsigned long ii = 0; ii < fPlotVector.size();++ii){
  			//cout << "......fPlotVector.size() = " << fPlotVector.size() << endl; //FGSII
  			if (fPlotVector[ii] != NULL) {
  			  fEvent->SetEventNumber(fCurrentEventNumber);//FGSII, added 05/29/2014
  			 	fEvent->SetFileName(fFileName);//FGSII added 05/30/2014
  				fPlotVector[ii]->ChangeEvent(fEvent);
  				//cout << "......fPlotVector[" << ii << "] != NULL" << endl; //FGSII
				}
  		}
  	}
  	
  	if(fCurrentEventNumber == 1){
  		cout << "......This is the first event in file" << endl;
  	}
	}
	
	if(enum_val == eLastEvent){

		//FGSII add 07/18/2014
		//first we update fTotEvent;
		evt::Event* temp_Event = NULL;
		temp_Event = new evt::Event();
		int temp_TotEvent = temp_Event->GetNofEvents(fFileName);
		delete temp_Event;
		fTotEvent = temp_TotEvent;
		if(fCurrentEventNumber == fTotEvent){
			cout << "......This is the last event in file" << endl;
			return true;//not sure about this
		}

		fCurrentEventNumber = fTotEvent;
		std::string magic_name = "use_initial_file";
		OpenDRTFile(magic_name);
		
		for (unsigned long ii = 0; ii < fPlotVector.size();++ii){
  		//cout << "......fPlotVector.size() = " << fPlotVector.size() << endl; //FGSII
  		if (fPlotVector[ii] != NULL) {
  		  fEvent->SetEventNumber(fCurrentEventNumber);//FGSII, added 05/29/2014
  			fEvent->SetFileName(fFileName);//FGSII added 05/30/2014
  			fPlotVector[ii]->ChangeEvent(fEvent);
  			//cout << "......fPlotVector[" << ii << "] != NULL" << endl; //FGSII
			}
  	}
	}
	
	if(enum_val == eFirstEvent){
		fCurrentEventNumber = 1;
		std::string magic_name = "use_initial_file";
		OpenDRTFile(magic_name);
		
		for (unsigned long ii = 0; ii < fPlotVector.size();++ii){
  		//cout << "......fPlotVector.size() = " << fPlotVector.size() << endl; //FGSII
  		if (fPlotVector[ii] != NULL) {
  		  fEvent->SetEventNumber(fCurrentEventNumber);//FGSII, added 05/29/2014
  			fEvent->SetFileName(fFileName);//FGSII added 05/30/2014
  			fPlotVector[ii]->ChangeEvent(fEvent);
  			//cout << "......fPlotVector[" << ii << "] != NULL" << endl; //FGSII
			}
  	}
	 
	}
	
	//FGSII added 06/04/2014 for 0603
	if(enum_val == eTypedEvent){
		std::string magic_name = "use_initial_file";
		OpenDRTFile(magic_name);
		
		//don't try to plot an event number greater than the
		//toal number of events in file
		if(fCurrentEventNumber > fTotEvent){
			cout << "......This is an invalid event number" << endl;
			cout << "......There are not this many events in file" << endl;
			return true;
		}
		
		for (unsigned long ii = 0; ii < fPlotVector.size();++ii){
  		//cout << "......fPlotVector.size() = " << fPlotVector.size() << endl; //FGSII
  		if (fPlotVector[ii] != NULL) {
  		  fEvent->SetEventNumber(fCurrentEventNumber);//FGSII, added 05/29/2014
  			fEvent->SetFileName(fFileName);//FGSII added 05/30/2014
  			fPlotVector[ii]->ChangeEvent(fEvent);
  			//cout << "......fPlotVector[" << ii << "] != NULL" << endl; //FGSII
			}
  	}
		
	}
	
	return true;
}
//-------------------------------------------------------------------//
void printHelp() {
	cout << "\n...EventBrowser::printHelp" << endl;

  cout << "......  Usage: ./drift_event_browser [-option] \n\n"
  		 << "......    --------------options-----------------------\n"
  		 << "......     (use -h to print help screen) \n"
       << "......     (use -s <scale> to scale browser size) \n"
       << "......     (use -v to get version) \n"
       << endl;
}
//-------------------------------------------------------------------//
//FGSII add 07/31/2014
void EventBrowser::HandleTracesCanvasEvents(Int_t event,Int_t px,Int_t py,TObject* sel){
	if ((event == kMouseMotion)){
		double x_user = fMWPCPlots->GetfCanvTraces()->GetCanvas()->PixeltoX(px);
		double y_user = fMWPCPlots->GetfCanvTraces()->GetCanvas()->PixeltoY(py - 
			fMWPCPlots->GetfCanvTraces()->GetCanvas()->GetWh());
		stringstream ss;
		ss << "(" << x_user << "," << y_user << ")" << endl;
		string user_coords = ss.str();
		const char* user_coords_chr = user_coords.c_str();

		fStatusBar->GetfUserCoords()->SetText(user_coords_chr);
	}
}
//-------------------------------------------------------------------//
//FGSII add 09/22/2014
void EventBrowser::MakeEvtChron(unsigned int event_in_file, long sec, long nsec){

	//cout << "...EventBrowser::MakeEvtChron()" << endl;	

	EvtChronType new_evt_chron;
	new_evt_chron.first = event_in_file;
	new_evt_chron.second.first = sec;
	new_evt_chron.second.second = nsec;
	
	cout << "...EventBrowser::MakeEvtChron(): event_in_file = " << new_evt_chron.first << "/" << fTotEvent << endl;
	//cout << "......sec = " << new_evt_chron.second.first << endl;FGSII comment out 10/06/2014


	fEvtChrons.push_back(new_evt_chron);
}		
//-------------------------------------------------------------------//
//FGSII add 09/22/2014
EventBrowser::EvtChronType EventBrowser::GetEvtChron(unsigned int evt_num){
	return fEvtChrons[evt_num];
}
//-------------------------------------------------------------------//
//FGSII add 09/22/2014 NOTE: this is not a member of "EventBrowser" class
bool CompareEvtChrons(EventBrowser::EvtChronType evt1, EventBrowser::EvtChronType evt2){
	return (evt1.second.first < evt2.second.first ||
	(evt1.second.first == evt2.second.first && evt1.second.second < evt2.second.second) );
}		
//-------------------------------------------------------------------//
//FGSII add 09/22/2014
void EventBrowser::SortEvtChrons(){
	std::sort(EvtChronBegin(),EvtChronEnd(),CompareEvtChrons);
}
//-------------------------------------------------------------------//
//FGSII add 09/22/2014
void EventBrowser::ClearEvtChrons(){	
	//clear container
	fEvtChrons.clear();
}	
//-------------------------------------------------------------------//
//FGSII add 09/22/2014
EventBrowser::EvtChronIterator EventBrowser::EvtChronBegin(){
	return fEvtChrons.begin();
}	
//-------------------------------------------------------------------//
//FGSII add 09/22/2014
EventBrowser::EvtChronIterator EventBrowser::EvtChronEnd(){
	return fEvtChrons.end();
}		
//-------------------------------------------------------------------//
//FGSII add 09/22/2014
EventBrowser::ConstEvtChronIterator EventBrowser::EvtChronBegin() const{
	return fEvtChrons.begin();
}		
//-------------------------------------------------------------------//
//FGSII add 09/22/2014
EventBrowser::ConstEvtChronIterator EventBrowser::EvtChronEnd() const{
	return fEvtChrons.end();
}
//-------------------------------------------------------------------//
//FGSII add 09/24/2014
void EventBrowser::CreateEventSortingMap(){

	cout << "...EventBrowser::CreateEventSortingMap()" << endl;

	//first we want to clear fEvtChrons
	ClearEvtChrons();

	evt::Event* temp_event = new evt::Event();
	fTotEvent = temp_event->GetNofEvents(fFileName);	

	//now we iterate through each event
	unsigned int ii=1;
	while(ii <= fTotEvent){
		//create temp event and load from file
		evt::Event* temp_event = new evt::Event();
		//temp_event->LoadFromDRTFile(fFileName,ii);//FGSII comment out 10/06/2014
		temp_event->LoadDRTFileForSort(fFileName,ii);//FGSII add 10/06/2014

      //add to fEvtChrons container
		MakeEvtChron(ii,temp_event->GetHeader().GetTime().GetGPSSecond(),temp_event->GetHeader().
		GetTime().GetGPSNanoSecond()); 

		//increment and delete event
		ii += 1;
		delete temp_event;
	}
	//now, we sort the container
	SortEvtChrons();

	//now, let's enable the ability to use the sort map
	fMenu->GetSortButton2()->SetEnabled(kTRUE);

	cout << "......the sort map has been updated" << endl;
}
//-------------------------------------------------------------------//
//FGSII add 10/05/2014

bool EventBrowser::EventIsSpark(){
		cout << "...EventBrowser::EventIsSpark()" << endl;
		for(evt::Event::ConstChannelIterator iter1 = fEvent->ChannelsBegin(); iter1 != fEvent->ChannelsEnd(); ++ iter1){
			for(evt::Channel::ConstTraceIterator iter2 = (*iter1)->TracesBegin(); iter2 != (*iter1)->TracesEnd(); ++iter2){
				if((*iter2)->HasSpark() == true){
					cout << "......for Channel" << (*iter1)->GetId() << endl;
					//we need to increment the current event number and try to load the next event
					//cout << "......Event #" << fCurrentEventNumber << " is a spark. Event is being skipped." << endl;
					return true;
				}
			}
		}

		return false;
}

//-------------------------------------------------------------------//
//FGSII add 10/07/2014
/*
bool EventBrowser::EventIsSpark(){
		cout << "...EventBrowser::EventIsSpark()" << endl;
		unsigned int non_spark_count = 0;
		//unsigned int spark_count = 0;
		for(evt::Event::ConstChannelIterator iter1 = fEvent->ChannelsBegin(); iter1 != fEvent->ChannelsEnd(); ++ iter1){
			for(evt::Channel::ConstTraceIterator iter2 = (*iter1)->TracesBegin(); iter2 != (*iter1)->TracesEnd(); ++iter2){
				if((*iter2)->HasSpark() == false){
					non_spark_count += 1;
				}				
			}
		}

	//if atleast one trace in the event does not have a spark => event_is_spark = false
	bool event_is_spark = false;
	if(non_spark_count > 0){
		event_is_spark = false;
	}
	else{
		event_is_spark = true;
	}
		
	return event_is_spark;
}*/
//-------------------------------------------------------------------//
int main(int argc, char **argv) {


  // get command line options
	int j = 1;
	double scaleFactor = 1.;
	// double scaleFactor = 0.8;
  int c;

  while ((c = getopt (argc, argv, "hs:v")) != -1) {//FGSII changed this up
    switch (c) {
    case 's':
      scaleFactor = atof(optarg);
      j += 2;
      break;
    case 'v':
      EventBrowser::Welcome();
      return 0;
      break;
    case 'h':
    	printHelp();
    	return 0;
    	break;
    default:
      printHelp();
      return 0;
    }
  }

  // open a root application
  int nArg = 1;
  TApplication theApp("App", &nArg, argv);

  // Set global variables
  gROOT->Reset();
  gStyle->SetOptStat(kFALSE);
  gStyle->SetFrameBorderMode(0);
  gROOT->ForceStyle();

  // Get the screen dimensions
  Int_t  x, y;
  UInt_t w, h;
  gVirtualX->GetWindowSize(gClient->GetRoot()->GetId(),x,y,w,h);

  // The window is slightly smaller than the screen
  UInt_t height = UInt_t(h*0.9*scaleFactor);
  UInt_t width = UInt_t(height*4.5/3.*scaleFactor);

	//FGSII 09/10/2014, note that channel labels run from 0-47
	//so it should be MakeChannel(ii) not (ii+1)
	//FGSII 10/13/2014, instead of channel labels, I will label by wire number 0-95
  det::Detector* det_descr = new det::Detector();
  for (unsigned long ii = 0; ii < 96; ++ii) { //FGSII changed from 48 to 96 10/13/2014
	det_descr->MakeChannel(ii);
  }
  cout << "...Initialized Detector with " << det_descr->GetNofChannels() 
  	<< " Channels " << endl;

  rec::Reconstruct *reconstructor = new rec::Reconstruct();
  reconstructor->SetDetector(det_descr);

  cout << "...Starting Main Window " << width << "x" << height << endl;
    EventBrowser* mainWindow =
    new EventBrowser(gClient->GetRoot(), 
    width, height, det_descr, reconstructor
		);
    	
  cout << "...Main Window launched \n" << endl;

  if (!mainWindow) {
    cerr << " Could not start up!" << endl;
    exit (1);
  }

  theApp.Run(); 
  
  	
  delete det_descr; det_descr=NULL;
  delete reconstructor; reconstructor = NULL;

  return 0;
}
