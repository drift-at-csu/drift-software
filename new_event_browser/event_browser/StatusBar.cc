#include "StatusBar.h"
#include "EventBrowserSignals.h"

#include <TGFrame.h>
#include <TGTextBuffer.h>
#include <TGTextEntry.h>
#include <TGButton.h>
#include <TGString.h>
#include <TGLabel.h>
#include <TGComboBox.h>
#include <iostream>

//fmod added 06/03/2014
#include <TGNumberEntry.h>
#include <TGTextMargin.h>

using namespace std;

ClassImp (StatusBar);

//-------------------------------------------------------------------//
StatusBar::StatusBar(TGCompositeFrame *main, UInt_t width, UInt_t height) :
		fMain (main) {
    
  // Horizontal frame
  TGHorizontalFrame *Horizontal = new TGHorizontalFrame (fMain, width, height);
  fMain->AddFrame (Horizontal, new TGLayoutHints (kLHintsExpandX, 3, 3, 3, 3));

  // navigation buttons 
  TGPictureButton* first = new TGPictureButton(Horizontal, 
	gClient->GetPicture("first_t.xpm"), eFirstEvent);
  TGPictureButton* next = new TGPictureButton(Horizontal, 
	gClient->GetPicture("next_t.xpm"), eNextEvent);
  TGPictureButton* previous = new TGPictureButton(Horizontal, 
	gClient->GetPicture("previous_t.xpm"), ePreviousEvent);
  TGPictureButton* last = new TGPictureButton(Horizontal, 
	gClient->GetPicture("last_t.xpm"), eLastEvent);
	
	//fmod added 06/03/2014  	
  	//name this 0603a
  	fEventNumber = new TGNumberEntry(Horizontal,0,4,eTypedEvent,
  	TGNumberFormat::kNESInteger,TGNumberFormat::kNEAPositive,
  	TGNumberFormat::kNELLimitMinMax,1,100000);//FGSII 07/23/214 last number was 1000 previously
  	fEventNumber->SetButtonToNum(false);//send message to parent
  	//fEventNumber->GetNumberEntry()->Connect("ReturnPressed()","StatusBar",this,"DoEventChange()");
  	fEventNumber->Associate(main);
  	//Horizontal->AddFrame(fEventNumber, new TGLayoutHints(
  	//kLHintsTop|kLHintsLeft|kLHintsExpandX,1, 1, 1, 1));

	//FGSII add on 07/30/2014
	fUserCoords = new TGStatusBar(Horizontal,0,0,kHorizontalFrame);
	//Horizontal->AddFrame(fUserCoords,new TGLayoutHints(
  	//kLHintsTop|kLHintsLeft|kLHintsExpandX,1, 1, 1, 1));

	
  
  last->SetHeight(22);
  first->SetHeight(22);
  previous->SetHeight(22);
  next->SetHeight(22);

  first->Associate(main);
  previous->Associate(main);
  next->Associate(main);
  last->Associate(main);

  first->SetToolTipText("First event ");
  previous->SetToolTipText("Previous event ");
  next->SetToolTipText("Next event ");
  last->SetToolTipText("Last event ");


	Horizontal->AddFrame(fUserCoords,new TGLayoutHints(
		kLHintsTop|kLHintsLeft|kLHintsExpandX,1, 1, 1, 1));
	Horizontal->AddFrame(fEventNumber, new TGLayoutHints(
  		kLHintsTop|kLHintsLeft|kLHintsExpandX,1, 1, 1, 1));
	Horizontal->AddFrame(first, new TGLayoutHints(
		kLHintsTop|kLHintsLeft|kLHintsExpandX,1, 1, 1, 1));
	Horizontal->AddFrame(previous, new TGLayoutHints(
		kLHintsTop|kLHintsLeft|kLHintsExpandX,1, 1, 1, 1));
	Horizontal->AddFrame(next, new TGLayoutHints(
		kLHintsTop|kLHintsLeft|kLHintsExpandX,1, 1, 1, 1));
	Horizontal->AddFrame(last, new TGLayoutHints(
		kLHintsTop|kLHintsLeft|kLHintsExpandX,1, 1, 1, 1));
  	
  	
}
//-------------------------------------------------------------------//
StatusBar::~StatusBar() {
  fMain->Cleanup();
}
//-------------------------------------------------------------------//
//fmod added 06/04/2014 for 0603b
/*void StatusBar::DoEventChange(){
	//int EventNumber = (int)(fEventNumber->GetEntry()->GetNumber());//for 0603b
	int EventNumber = (int)(fEventNumber->GetNumberEntry()->GetNumber());
	if(EventNumber < 1 || EventNumber > fgMaxEventNumber){
		cout << "...StatusBar::DoEventChange()" << endl;
		cout << "...... Invalid Entry " << endl;
		cout << "...... 1 <= Event Number <= " << fgMaxEventNumber << endl;
		return;
	}
		
	cout << "...StatusBar::DoEventChange()" << endl;
	cout << "......" << EventNumber << endl;
	return;
}*/
//-------------------------------------------------------------------//
//fmod added 06/04/2014 for 0603b
	int StatusBar::GetEventNumber(){
	int EventNumber = (int)(fEventNumber->GetNumberEntry()->GetNumber());
	
	return EventNumber;
}
//-------------------------------------------------------------------//
//fmod added 06/04/2014 for 0603b
 void StatusBar::ResetEventNumber(){
		fEventNumber->SetIntNumber(0);
 	
 	return;
 }
//-------------------------------------------------------------------//
TGStatusBar* StatusBar::GetfUserCoords(){
	return fUserCoords;
}
//-------------------------------------------------------------------//


