#ifndef _DRIFT_EB_EventBrowserSignals_h_
#define _DRIFT_EB_EventBrowserSignals_h_

// list of signals
enum EButtonSignals {
	// file menu
	eFileOpen,
	eFileLive,//fmod
	eFileQuit,

	// signals for tab navigation control
	//eNextTab, //fmod commented out currently not being used
	//ePreviousTab, //fmod commented out currently not being used

	// signals for event navigation control
	eFirstEvent,
	eCurrentEvent,
	eNextEvent,
	ePreviousEvent,
	eLastEvent,
	
	//fmod added 06/03/2014 for changes in StatusBar.cc and .h
	eTypedEvent,

	// help menu
	eAbout,

	//eStatusBarEventList //fmod commented out currently not being used
}; /* enum EButtonSignals */
#endif /* _DRIFT_EB_EventBrowserSignals_h_ */
