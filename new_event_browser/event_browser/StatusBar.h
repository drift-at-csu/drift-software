#ifndef _DRIFT_EB_StatusBar_h_
#define _DRIFT_EB_StatusBar_h_

#include <TQObject.h>
#include <TGStatusBar.h>//FGSII add 07/30/2014

class TGCompositeFrame;
class TGComboBox;

//fmod added 06/04/2014
const int fgMaxEventNumber = 1000;//use with 0603b

//fmod added 06/03/2014
//class TGTextEntry;
class TGNumberEntry; //use with 0603a in statusBar.cc
class TGTextMargin; //use with 0603b in statusBar.cc

class StatusBar : public TQObject {
  public:
	StatusBar(TGCompositeFrame *main, UInt_t width, UInt_t height);
	~StatusBar();
	
	//fmod added 06/04/2014 for 0603b
	//void DoEventChange();
	int GetEventNumber();
	void ResetEventNumber();

	//FGSII add on 07/30/2014
	TGStatusBar* GetfUserCoords();
	
  private:
	TGCompositeFrame* fMain;
	
	//fmod added 06/03/2014
	TGNumberEntry* fEventNumber;//use with 0603a in statusBar.cc
	//TGTextMargin* fEventNumber;//use with 0603b in statusBar.cc

	//FGSII add on 07/30/2014
	TGStatusBar* fUserCoords;//FGSII add 07/30/2014

  ClassDef (StatusBar, 1);
}; /* class StatusBar */
#endif /* _DRIFT_EB_StatusBar_h_ */
