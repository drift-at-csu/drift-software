#ifndef _DRIFT_EB_TGTextMargin_h_
#define _DRIFT_EB_TGTextMargin_h_

#include <TGFrame.h>
class TGNumberEntry;
class TGNumberEntryField;

class TGTextMargin : public TGHorizontalFrame {
  public:
	TGTextMargin(const TGWindow *p, const char *name);
	TGNumberEntryField *GetEntry();
  protected:
	TGNumberEntry *fEntry;

  ClassDef(TGTextMargin, 0)
}; /* class TReadProgress */
#endif /* _DRIFT_EB_TGTextMargin_h_ */
