#include <evt/Event.h>
#include <evt/Channel.h>
#include <det/Detector.h>
#include <det/Channel.h>
#include <utl/Trace.h>
#include <rec/Reconstruct.h>
using namespace drift;

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string.h>
using namespace std;


//-------------------------------------------------------------------//
rec::Reconstruct::Reconstruct():
	fEvent(NULL), 
	fDetector(NULL)
	{
}
//-------------------------------------------------------------------//
rec::Reconstruct::~Reconstruct(){
	ClearPedestal(); //FGSII add on 08/26/2014
	ClearFirstAC();
	ClearSecondAC();
	ClearCurrent();
	ClearCharge();
}
//-------------------------------------------------------------------//
//FGSII add on 08/26/2014
bool rec::Reconstruct::RecPedestal(const unsigned int channelId){
	cout << "...rec::Reconstruct::RecPedestal()" << endl;

	if(fEvent == NULL){
		return false;
	}
	if(fDetector == NULL){
		return false;
	}
	evt::Channel *event_channel = fEvent->GetChannel(channelId); //comment out 09/25/2014
	if(event_channel == NULL){
		cout << "WARNING: Channel " << channelId << " does not exist" << endl;//for debug
		return false;
	}
	det::Channel *det_channel = fDetector->GetChannel(channelId);
	if(det_channel == NULL){
		cout << "WARNING: Channel " << channelId << " does not exist" << endl;//for debug
		return false;
	}
	cout << "......RecPedestal for Channel " << channelId << endl;
	//After this channel description of the detector as well as 
	//the channel in the event do exist

	//Let's make the target channel
	MakePedestalChannel(channelId);
	//And get a pointer to it
	//evt::Channel *target_channel = GetPedestalChannel(channelId); //comment out 09/25/2014

	
	//This works, but it is easier the next line below this block of code
	//Need to copy the source traces of the source channel to the target channel
	/*for(evt::Channel::ConstTraceIterator iter = event_channel->TracesBegin(); iter != event_channel->TracesEnd(); iter++){
		//need to get trace id and make each trace in the target channel
		target_channel->MakeTrace((*iter)->GetTraceId());
		*(target_channel->GetTrace((*iter)->GetTraceId())) = *(event_channel->GetTrace((*iter)->GetTraceId()));
	}*/

	/*for(evt::Channel::ConstTraceIterator iter = event_channel->TracesBegin(); iter != event_channel->TracesEnd(); iter++){
		//need to get trace id and make each trace in the target channel
		GetPedestalChannel(channelId)->MakeTrace((*iter)->GetTraceId());
		*(GetPedestalChannel(channelId)->GetTrace((*iter)->GetTraceId())) = *(event_channel->GetTrace((*iter)->GetTraceId()));
	}*/

	//*target_channel = *event_channel; //comment out 09/25/2014
	//*GetPedestalChannel(channelId) = *event_channel;
	GetPedestalChannel(channelId)->MakeClone(event_channel);

	//FGSII 10/31/2014 debug
	/*cout << "......bin num (fEvent) = " << fEvent->GetChannel(channelId)->
		GetPedestal(0).second.second << endl;
	cout << "......bin num (clone) = " << GetPedestalChannel(channelId)->
		GetPedestal(0).second.second << endl;*/

	//calculate the pedestal for the channel
	//target_channel->CalcConstPedestal_2(0); //comment out 09/25/2014
	GetPedestalChannel(channelId) -> CalcConstPedestal_2(0);

	//now subtract pedestal from each trace
	/*for(evt::Channel::ConstTraceIterator iter = target_channel->TracesBegin(); iter != target_channel->TracesEnd(); iter++){
		(*iter)->SubtractConstPedestal(target_channel->GetConstPedestal());
	}*/ //comment out 09/25/2014

for(evt::Channel::ConstTraceIterator iter = GetPedestalChannel(channelId)->TracesBegin(); iter != GetPedestalChannel(channelId)->TracesEnd(); iter++){
		(*iter)->SubtractConstPedestal(GetPedestalChannel(channelId)->GetConstPedestal());
		//(*iter)->SubtractQuadraticPedestal(GetPedestalChannel(channelId)->GetQuadraticFitParams());
	}
	
	return true;
}
//-------------------------------------------------------------------//
//FGSII add on 10/31/2014
bool rec::Reconstruct::RecPedestalWirePairs(const unsigned int channelId){
	cout << "...rec::Reconstruct::RecPedestalWirePairs()" << endl;

	if(fEvent == NULL){
		return false;
	}
	if(fDetector == NULL){
		return false;
	}
	
	evt::WirePair *event_pair = fEvent->GetWirePair(channelId);
	if(event_pair == NULL){
		cout << "WARNING: WirePair " << channelId << " does not exist " << endl;
		return false;
	}
	det::Channel *det_channel = fDetector->GetChannel(channelId);
	if(det_channel == NULL){
		cout << "WARNING: Channel " << channelId << " does not exist" << endl;//for debug
		return false;
	}	
	
	//There is a problem here, when cloning the wire id is reset to the channel id
	/*MakePedestalChannel(event_pair->GetPosWireId());
	MakePedestalChannel(event_pair->GetNegWireId());
	GetPedestalChannel(event_pair->GetPosWireId())->MakeClone(event_pair->GetWirePair().first);
	GetPedestalChannel(event_pair->GetNegWireId())->MakeClone(event_pair->GetWirePair().second);*/
	//FGSII 11/03/2014 new code
	MakePedestalChannel(channelId);
	GetPedestalChannel(channelId)->MakeClone(event_pair->GetWirePair().first);
	GetPedestalChannel(channelId)->SetId(event_pair->GetPosWireId());
	MakePedestalChannel(channelId);
	GetPedestalChannel(channelId)->MakeClone(event_pair->GetWirePair().second);
	GetPedestalChannel(channelId)->SetId(event_pair->GetNegWireId());
	ClearPedestal(channelId);
	

	//SOME ERROR HERE 10/31/2014 15:52
	cout << "......debug: nof peds in fEvent chan = " << fEvent->GetChannel(channelId)->GetNofPedestals() << endl;
	cout << "......debug: nof peds in pos = " << fEvent->GetWirePair(channelId)->GetWirePair().first->GetNofPedestals() << endl;
	cout << "......debug: nof peds in neg = " << fEvent->GetWirePair(channelId)->GetWirePair().second->GetNofPedestals() << endl;
	/*cout << "......... [(" << fEvent->GetChannel(channelId)->GetPedestal(0).first.first << ","
		 << fEvent->GetChannel(channelId)->GetPedestal(0).first.second << "),("
		 << fEvent->GetChannel(channelId)->GetPedestal(0).second.first << ","
		 << fEvent->GetChannel(channelId)->GetPedestal(0).second.second << ")] " << endl;*/
	cout << "......debug: nof peds in ped chan " << GetPedestalChannel(event_pair->GetPosWireId())->GetNofPedestals() << endl;
	GetPedestalChannel(event_pair->GetPosWireId())->CalcConstPedestal_2(0);
	GetPedestalChannel(event_pair->GetNegWireId())->CalcConstPedestal_2(0);	

	for(evt::Channel::ConstTraceIterator iter = GetPedestalChannel(event_pair->
		GetPosWireId())->TracesBegin(); iter != GetPedestalChannel(event_pair->GetPosWireId())
		->TracesEnd(); iter++){
		(*iter)->SubtractConstPedestal(GetPedestalChannel(event_pair->GetPosWireId())->GetConstPedestal());
	}

	for(evt::Channel::ConstTraceIterator iter = GetPedestalChannel(event_pair->
		GetNegWireId())->TracesBegin(); iter != GetPedestalChannel(event_pair->GetNegWireId())
		->TracesEnd(); iter++){
		(*iter)->SubtractConstPedestal(GetPedestalChannel(event_pair->GetNegWireId())->GetConstPedestal());
	}
	
	return true;
}
//-------------------------------------------------------------------//
//new code FGSII add 10/15/2014
bool rec::Reconstruct::RecFirstAC(const unsigned int channelId){
	cout << "...rec::Reconstruct::RecFirstAC()" << endl;

	if(fEvent == NULL){
		return false;
	}
	if(GetNofChanPedestal() == 0){
		return false;
	}
	if(fDetector == NULL){
		return false;
	}
	evt::Channel *pedestal_channel = GetPedestalChannel(channelId);
	if(pedestal_channel == NULL){
		cout << "WARNING: Channel " << channelId << " does not exist" << endl;//for debug
		return false;
	}
	det::Channel *det_channel = fDetector->GetChannel(channelId);
	if(det_channel == NULL){
		cout << "WARNING: Channel " << channelId << " does not exist" << endl;//for debug
		return false;
	}
	cout << "......RecFirstAC for Channel " << channelId << endl;

	//Let's make the target channel
	MakeFirstACChannel(channelId);

	//Now clone the pedestal channel into the target channel
	GetFirstACChannel(channelId)->MakeClone(pedestal_channel);

	//FGSII 10/31/2014 debug
	/*cout << "......bin num (fEvent) = " << fEvent->GetChannel(channelId)->
		GetPedestal(0).second.second << endl;
	cout << "......bin num (clone) = " << GetFirstACChannel(channelId)->
		GetPedestal(0).second.second << endl;*/

	//Let's multiply each trace in channel by 1/(gain)
	double chan_gain = det_channel->GetFirstACGain();
	cout << "......gain = " << chan_gain << endl;
	if (chan_gain != 0.0){ 
  		chan_gain = 1.0/chan_gain;
	}
	for(evt::Channel::ConstTraceIterator iter = GetFirstACChannel(channelId)->TracesBegin(); iter != GetFirstACChannel(channelId)->TracesEnd(); iter++){
		(*iter)->MultiplyBy(chan_gain);
	}

	//finally deconvolve the AC coupling (this is temporary...it is not correct currently)
	for(evt::Channel::ConstTraceIterator iter = GetFirstACChannel(channelId)->TracesBegin(); iter != GetFirstACChannel(channelId)->TracesEnd(); iter++){
		(*iter)->DeconvolveACCoupling(det_channel->GetFirstACTau());

		//FGSII add 10/31/2014, will only work for first trace in container
		//(*iter)->DeconvolveACCouplingFSS(det_channel->GetFirstACTau(),
			//GetFirstACChannel(channelId)->GetPedestal(0).second.second);
	}

	/*
	//let's add other stuff right away to rec all the way to current at wire
	//------------------------------------------------------------
	chan_gain = det_channel->GetSecondACGain();
	cout << "......gain2 = " << chan_gain << endl;
	if (chan_gain != 0.0){ 
  		chan_gain = 1.0/chan_gain;
	}
	for(evt::Channel::ConstTraceIterator iter = GetFirstACChannel(channelId)->TracesBegin(); iter != GetFirstACChannel(channelId)->TracesEnd(); iter++){
		(*iter)->MultiplyBy(chan_gain);
	}

	for(evt::Channel::ConstTraceIterator iter = GetFirstACChannel(channelId)->TracesBegin(); iter != GetFirstACChannel(channelId)->TracesEnd(); iter++){
		(*iter)->DeconvolveACCoupling(det_channel->GetSecondACTau());
	}

	//---------

	chan_gain = det_channel->GetTransimpGain();
	cout << "......gain_transimp = " << chan_gain << endl;
	if (chan_gain != 0.0){ 
  		chan_gain = 1.0/chan_gain;
	}
	for(evt::Channel::ConstTraceIterator iter = GetFirstACChannel(channelId)->TracesBegin(); iter != GetFirstACChannel(channelId)->TracesEnd(); iter++){
		(*iter)->MultiplyBy(chan_gain);
	}

	for(evt::Channel::ConstTraceIterator iter = GetFirstACChannel(channelId)->TracesBegin(); iter != GetFirstACChannel(channelId)->TracesEnd(); iter++){
		(*iter)->DeconvolveCurrent(det_channel->GetTransimpTau());
	}

	//------------------------------------------------------------
	*/  	

	return true;
}
//-------------------------------------------------------------------//
//FGSII add on 11/03/2014
bool rec::Reconstruct::RecFirstACWirePairs(const unsigned int channelId){
	cout << "...rec::Reconstruct::RecFirstACWirePairs()" << endl;

	if(fEvent == NULL){
		return false;
	}
	if(fDetector == NULL){
		return false;
	}

	unsigned int pos_id = fEvent->GetWirePair(channelId)->GetPosWireId();
	unsigned int neg_id = fEvent->GetWirePair(channelId)->GetNegWireId();
	evt::Channel *pos_chan = GetPedestalChannel(pos_id);
	if(pos_chan == NULL){
		cout << "WARNING: Wire " << pos_id << " does not exist" << endl;
		return false;
	}
	evt::Channel *neg_chan = GetPedestalChannel(neg_id);
	if(neg_chan == NULL){
		cout << "WARNING: Wire " << pos_id << " does not exist" << endl;
		return false;
	}
	det::Channel *det_channel = fDetector->GetChannel(channelId);
	if(det_channel == NULL){
		cout << "WARNING: Channel " << channelId << " does not exist" << endl;//for debug
		return false;
	}	
	
	MakeFirstACChannel(pos_id);
	GetFirstACChannel(pos_id)->MakeClone(pos_chan);
	MakeFirstACChannel(neg_id);
	GetFirstACChannel(neg_id)->MakeClone(neg_chan);

	//Get the gain for the channel
	double chan_gain = det_channel->GetFirstACGain();
	cout << "......gain = " << chan_gain << endl;
	if (chan_gain != 0.0){ 
  		chan_gain = 1.0/chan_gain;
	}

	//Positve Wire
	for(evt::Channel::ConstTraceIterator iter = GetFirstACChannel(pos_id)->TracesBegin();
		iter != GetFirstACChannel(pos_id)->TracesEnd(); iter++){
		(*iter)->MultiplyBy(chan_gain);
	}
	for(evt::Channel::ConstTraceIterator iter = GetFirstACChannel(pos_id)->TracesBegin();
		iter != GetFirstACChannel(pos_id)->TracesEnd(); iter++){
		(*iter)->DeconvolveACCoupling(det_channel->GetFirstACTau());
	}

	//Negative Wire
	for(evt::Channel::ConstTraceIterator iter = GetFirstACChannel(neg_id)->TracesBegin();
		iter != GetFirstACChannel(neg_id)->TracesEnd(); iter++){
		(*iter)->MultiplyBy(chan_gain);
	}
	for(evt::Channel::ConstTraceIterator iter = GetFirstACChannel(neg_id)->TracesBegin();
		iter != GetFirstACChannel(neg_id)->TracesEnd(); iter++){
		(*iter)->DeconvolveACCoupling(det_channel->GetFirstACTau());
	}
	
	return true;
}
//-------------------------------------------------------------------//
bool rec::Reconstruct::RecSecondAC(const unsigned int channelId){
	cout << "...rec::Reconstruct::RecSecondAC()" << endl;

	if(fEvent == NULL){
		return false;
	}
	if(GetNofChanFirstAC() == 0){
		return false;
	}
	if(fDetector == NULL){
		return false;
	}
	evt::Channel *firstac_channel = GetFirstACChannel(channelId);
	if(firstac_channel == NULL){
		cout << "WARNING: Channel " << channelId << " does not exist" << endl;//for debug
		return false;
	}
	det::Channel *det_channel = fDetector->GetChannel(channelId);
	if(det_channel == NULL){
		cout << "WARNING: Channel " << channelId << " does not exist" << endl;//for debug
		return false;
	}
	cout << "......RecSecondAC for Channel " << channelId << endl;

	//Let's make the target channel
	MakeSecondACChannel(channelId);

	//Now clone the firstac channel into the target channel
	GetSecondACChannel(channelId)->MakeClone(firstac_channel);

	//Let's multiply each trace in channel by 1/(gain)
	double chan_gain = det_channel->GetSecondACGain();
	cout << "......gain = " << chan_gain << endl;
	if (chan_gain != 0.0){ 
  		chan_gain = 1.0/chan_gain;
	}
	for(evt::Channel::ConstTraceIterator iter = GetSecondACChannel(channelId)->TracesBegin(); iter != GetSecondACChannel(channelId)->TracesEnd(); iter++){
		(*iter)->MultiplyBy(chan_gain);
	}

	//finally deconvolve the AC coupling (this is temporary...it is not correct currently)
	for(evt::Channel::ConstTraceIterator iter = GetSecondACChannel(channelId)->TracesBegin(); iter != GetSecondACChannel(channelId)->TracesEnd(); iter++){
		(*iter)->DeconvolveACCoupling(det_channel->GetSecondACTau());

		//FGSII add 10/31/2014, will only work for first trace in container
		//(*iter)->DeconvolveACCouplingFSS(det_channel->GetSecondACTau(),
			//GetSecondACChannel(channelId)->GetPedestal(0).second.second);
	}

	return true;
}
//-------------------------------------------------------------------//
//FGSII add on 11/03/2014
bool rec::Reconstruct::RecSecondACWirePairs(const unsigned int channelId){
	cout << "...rec::Reconstruct::RecSecondACWirePairs()" << endl;

	if(fEvent == NULL){
		return false;
	}
	if(fDetector == NULL){
		return false;
	}

	unsigned int pos_id = fEvent->GetWirePair(channelId)->GetPosWireId();
	unsigned int neg_id = fEvent->GetWirePair(channelId)->GetNegWireId();
	evt::Channel *pos_chan = GetFirstACChannel(pos_id);
	if(pos_chan == NULL){
		cout << "WARNING: Wire " << pos_id << " does not exist" << endl;
		return false;
	}
	evt::Channel *neg_chan = GetFirstACChannel(neg_id);
	if(neg_chan == NULL){
		cout << "WARNING: Wire " << pos_id << " does not exist" << endl;
		return false;
	}
	det::Channel *det_channel = fDetector->GetChannel(channelId);
	if(det_channel == NULL){
		cout << "WARNING: Channel " << channelId << " does not exist" << endl;//for debug
		return false;
	}	
	
	MakeSecondACChannel(pos_id);
	GetSecondACChannel(pos_id)->MakeClone(pos_chan);
	MakeSecondACChannel(neg_id);
	GetSecondACChannel(neg_id)->MakeClone(neg_chan);

	//Get the gain for the channel
	double chan_gain = det_channel->GetSecondACGain();
	cout << "......gain = " << chan_gain << endl;
	if (chan_gain != 0.0){ 
  		chan_gain = 1.0/chan_gain;
	}

	//Positve Wire
	for(evt::Channel::ConstTraceIterator iter = GetSecondACChannel(pos_id)->TracesBegin();
		iter != GetSecondACChannel(pos_id)->TracesEnd(); iter++){
		(*iter)->MultiplyBy(chan_gain);
	}
	for(evt::Channel::ConstTraceIterator iter = GetSecondACChannel(pos_id)->TracesBegin();
		iter != GetSecondACChannel(pos_id)->TracesEnd(); iter++){
		(*iter)->DeconvolveACCoupling(det_channel->GetSecondACTau());
	}

	//Negative Wire
	for(evt::Channel::ConstTraceIterator iter = GetSecondACChannel(neg_id)->TracesBegin();
		iter != GetSecondACChannel(neg_id)->TracesEnd(); iter++){
		(*iter)->MultiplyBy(chan_gain);
	}
	for(evt::Channel::ConstTraceIterator iter = GetSecondACChannel(neg_id)->TracesBegin();
		iter != GetSecondACChannel(neg_id)->TracesEnd(); iter++){
		(*iter)->DeconvolveACCoupling(det_channel->GetSecondACTau());
	}
	
	return true;
}
//-------------------------------------------------------------------//
bool rec::Reconstruct::RecCurrent(const unsigned int channelId){
	cout << "...rec::Reconstruct::RecCurrent()" << endl;

	if(fEvent == NULL){
		return false;
	}
	if(GetNofChanSecondAC() == 0){
		return false;
	}
	if(fDetector == NULL){
		return false;
	}
	evt::Channel *secondac_channel = GetSecondACChannel(channelId);
	if(secondac_channel == NULL){
		cout << "WARNING: Channel " << channelId << " does not exist" << endl;//for debug
		return false;
	}
	det::Channel *det_channel = fDetector->GetChannel(channelId);
	if(det_channel == NULL){
		cout << "WARNING: Channel " << channelId << " does not exist" << endl;//for debug
		return false;
	}
	cout << "......RecCurrent for Channel " << channelId << endl;

	//Let's make the target channel
	MakeCurrentChannel(channelId);

	//Now clone the secondac channel into the target channel
	GetCurrentChannel(channelId)->MakeClone(secondac_channel);

	//Let's multiply each trace in channel by 1/(gain)
	double chan_gain = det_channel->GetTransimpGain();
	cout << "......gain = " << chan_gain << endl;
	if (chan_gain != 0.0){ 
  		chan_gain = 1.0/chan_gain;
	}
	for(evt::Channel::ConstTraceIterator iter = GetCurrentChannel(channelId)->TracesBegin(); iter != GetCurrentChannel(channelId)->TracesEnd(); iter++){
		(*iter)->MultiplyBy(chan_gain);
	}

	//finally deconvolve the current (this is temporary...it is not correct currently)
	for(evt::Channel::ConstTraceIterator iter = GetCurrentChannel(channelId)->TracesBegin(); iter != GetCurrentChannel(channelId)->TracesEnd(); iter++){
		(*iter)->DeconvolveCurrent(det_channel->GetTransimpTau());

		//FGSII add 10/31/2014, will only work for first trace in container
		//(*iter)->DeconvolveCurrentFSS(det_channel->GetTransimpTau(),
			//GetCurrentChannel(channelId)->GetPedestal(0).second.second);
	}

	return true;
}
//-------------------------------------------------------------------//
//FGSII add on 11/03/2014
bool rec::Reconstruct::RecCurrentWirePairs(const unsigned int channelId){
	cout << "...rec::Reconstruct::RecCurrentWirePairs()" << endl;

	if(fEvent == NULL){
		return false;
	}
	if(fDetector == NULL){
		return false;
	}

	unsigned int pos_id = fEvent->GetWirePair(channelId)->GetPosWireId();
	unsigned int neg_id = fEvent->GetWirePair(channelId)->GetNegWireId();
	evt::Channel *pos_chan = GetSecondACChannel(pos_id);
	if(pos_chan == NULL){
		cout << "WARNING: Wire " << pos_id << " does not exist" << endl;
		return false;
	}
	evt::Channel *neg_chan = GetSecondACChannel(neg_id);
	if(neg_chan == NULL){
		cout << "WARNING: Wire " << pos_id << " does not exist" << endl;
		return false;
	}
	det::Channel *det_channel = fDetector->GetChannel(channelId);
	if(det_channel == NULL){
		cout << "WARNING: Channel " << channelId << " does not exist" << endl;//for debug
		return false;
	}	
	
	MakeCurrentChannel(pos_id);
	GetCurrentChannel(pos_id)->MakeClone(pos_chan);
	MakeCurrentChannel(neg_id);
	GetCurrentChannel(neg_id)->MakeClone(neg_chan);

	//Get the gain for the channel
	double chan_gain = det_channel->GetTransimpGain();
	cout << "......gain = " << chan_gain << endl;
	if (chan_gain != 0.0){ 
  		chan_gain = 1.0/chan_gain;
	}

	//Positve Wire
	for(evt::Channel::ConstTraceIterator iter = GetCurrentChannel(pos_id)->TracesBegin();
		iter != GetCurrentChannel(pos_id)->TracesEnd(); iter++){
		(*iter)->MultiplyBy(chan_gain);
	}
	for(evt::Channel::ConstTraceIterator iter = GetCurrentChannel(pos_id)->TracesBegin();
		iter != GetCurrentChannel(pos_id)->TracesEnd(); iter++){
		(*iter)->DeconvolveCurrent(det_channel->GetTransimpTau());
	}

	//Negative Wire
	for(evt::Channel::ConstTraceIterator iter = GetCurrentChannel(neg_id)->TracesBegin();
		iter != GetCurrentChannel(neg_id)->TracesEnd(); iter++){
		(*iter)->MultiplyBy(chan_gain);
	}
	for(evt::Channel::ConstTraceIterator iter = GetCurrentChannel(neg_id)->TracesBegin();
		iter != GetCurrentChannel(neg_id)->TracesEnd(); iter++){
		(*iter)->DeconvolveCurrent(det_channel->GetTransimpTau());
	}
	
	return true;
}
//-------------------------------------------------------------------//
bool rec::Reconstruct::RecCharge(const unsigned int channelId){
	cout << "...rec::Reconstruct::RecCharge()" << endl;

	if(fEvent == NULL){
		return false;
	}
	if(GetNofChanCurrent() == 0){
		return false;
	}
	if(fDetector == NULL){
		return false;
	}
	evt::Channel *current_channel = GetCurrentChannel(channelId);
	if(current_channel == NULL){
		cout << "WARNING: Channel " << channelId << " does not exist" << endl;//for debug
		return false;
	}
	det::Channel *det_channel = fDetector->GetChannel(channelId);
	if(det_channel == NULL){
		cout << "WARNING: Channel " << channelId << " does not exist" << endl;//for debug
		return false;
	}
	cout << "......RecCharge for Channel " << channelId << endl;

	//Let's make the target channel
	MakeChargeChannel(channelId);

	//Now clone the pedestal channel into the target channel
	GetChargeChannel(channelId)->MakeClone(current_channel);

	//finally integrate to get charge (this is temporary...it is not correct currently)
	for(evt::Channel::ConstTraceIterator iter = GetChargeChannel(channelId)->TracesBegin(); iter != GetChargeChannel(channelId)->TracesEnd(); iter++){
		(*iter)->Integrate(6241.51); // 1fC = 6241.51 electrons
	}

	return true;
}
//-------------------------------------------------------------------//
//FGSII add on 11/03/2014
bool rec::Reconstruct::RecChargeWirePairs(const unsigned int channelId){
	cout << "...rec::Reconstruct::RecChargeWirePairs()" << endl;

	if(fEvent == NULL){
		return false;
	}
	if(fDetector == NULL){
		return false;
	}

	unsigned int pos_id = fEvent->GetWirePair(channelId)->GetPosWireId();
	unsigned int neg_id = fEvent->GetWirePair(channelId)->GetNegWireId();
	evt::Channel *pos_chan = GetCurrentChannel(pos_id);
	if(pos_chan == NULL){
		cout << "WARNING: Wire " << pos_id << " does not exist" << endl;
		return false;
	}
	evt::Channel *neg_chan = GetCurrentChannel(neg_id);
	if(neg_chan == NULL){
		cout << "WARNING: Wire " << pos_id << " does not exist" << endl;
		return false;
	}
	det::Channel *det_channel = fDetector->GetChannel(channelId);
	if(det_channel == NULL){
		cout << "WARNING: Channel " << channelId << " does not exist" << endl;//for debug
		return false;
	}	
	
	MakeChargeChannel(pos_id);
	GetChargeChannel(pos_id)->MakeClone(pos_chan);
	MakeChargeChannel(neg_id);
	GetChargeChannel(neg_id)->MakeClone(neg_chan);

	//Positve Wire
	for(evt::Channel::ConstTraceIterator iter = GetChargeChannel(pos_id)->TracesBegin();
		iter != GetChargeChannel(pos_id)->TracesEnd(); iter++){
		(*iter)->Integrate(6241.51); // 1fC = 6241.51 electrons
	}

	//Negative Wire
	for(evt::Channel::ConstTraceIterator iter = GetChargeChannel(neg_id)->TracesBegin();
		iter != GetChargeChannel(neg_id)->TracesEnd(); iter++){
		(*iter)->Integrate(6241.51); // 1fC = 6241.51 electrons
	}
	
	return true;
}
//-------------------------------------------------------------------//
/* old code
bool rec::Reconstruct::RecFirstAC(const unsigned int channelId){
  if (fEvent == NULL)
	return false;
  if (fDetector == NULL)
	return false;
  evt::Channel *event_channel = fEvent->GetChannel(channelId);
  if (event_channel == NULL)
  	return false;
  det::Channel *det_channel = fDetector->GetChannel(channelId);
  if (det_channel == NULL)
  	return false;
  cout << "...rec::Reconstruct::RecFirstAC for Channel " << channelId << endl;
  //After this  channel description of the detector as well as 
  //the channel in the event do exist.
  
  // Let's Make the target channel
  MakeFirstACChannel(channelId);
  // And Get a pointer to it
  evt::Channel *target_channel = GetFirstACChannel(channelId);
  // We need to copy the source trace to the target trace
  target_channel->GetTrace() = event_channel->GetTrace();

  //Let's put a range for pedestals here if it doesn't exist
  double chan_gain = det_channel->GetFirstACGain();
//	cout << "...returned to rec::Reconstruct::RecFirstAC" << endl; //fmod add 06/25/2014
  if (chan_gain != 0.0) 
  	chan_gain = 1.0/chan_gain;
  target_channel->GetTrace().MultiplyBy(chan_gain);

  // We want to subtract the pedestal
  //target_channel->GetTrace().ShiftPedestalToZero(0.0, 1000.0);//original
 // target_channel->GetTrace().ShiftPedestalToZero(0.0, 800.0);
	//fmod test code added 06/25/2014----------------------------------
	double signal_begin;
	double trace_begin;
	cout << "Enter start time of trace:";
	cin >> trace_begin;
	cout << "Enter start time of signal:";
	cin >> signal_begin;
	target_channel->GetTrace().ShiftPedestalToZero(trace_begin, signal_begin); 
	//-----------------------------------------------------------------

  // And deconvolve the trace
  target_channel->GetTrace().DeconvolveACCoupling( det_channel->GetFirstACTau() );

  return true;
}*/
//-------------------------------------------------------------------//
/*
bool rec::Reconstruct::RecSecondAC(const unsigned int channelId,
	bool correct_one_over_f){
  if (fDetector == NULL)
	return false;
  det::Channel *det_channel = fDetector->GetChannel(channelId);
  if (det_channel == NULL)
	return false;

  if (!HasFirstACChannel(channelId)) {
	cout << "...rec::Reconstruct::RecSecondAC: Channel " << channelId
		<< " doesn't have FirstAC Rec" << endl;
	return false;
  }
  cout << "...rec::Reconstruct::RecSecondAC for Channel " << channelId << endl;
  // Let's Make the target channel
  MakeSecondACChannel(channelId);
  // And Get a pointer to it
  evt::Channel *target_channel = GetSecondACChannel(channelId);

  // We need to copy the source trace to the target trace
  target_channel->GetTrace() = GetFirstACChannel(channelId)->GetTrace();

  // Let's correct 1/f noise
  
  if (correct_one_over_f) {
	target_channel->GetTrace().CorrectOneOverF();
  } else {
  	target_channel->GetTrace().ShiftPedestalToZero(0.0, 1000.0); //original

  	//target_channel->GetTrace().ShiftPedestalToZero(0.0, 800.0); //fmod 06/25/2014
	//fmod test code added 06/25/2014----------------------------------
	//double signal_begin;
	//cout << "Enter start time of signal:";
	//cin >> signal_begin;
	//target_channel->GetTrace().ShiftPedestalToZero(0.0, signal_begin); 
	//-----------------------------------------------------------------
  }
  

  double chan_gain = det_channel->GetSecondACGain();
  if (chan_gain != 0.0)
	chan_gain = 1.0/chan_gain;
  target_channel->GetTrace().MultiplyBy(chan_gain);

  // And deconvolve the trace
  target_channel->GetTrace().DeconvolveACCoupling( det_channel->GetSecondACTau());

  // If we were told to correct for 1/f noise coupling?
  if (correct_one_over_f) {
  	target_channel->GetTrace().CorrectOneOverF();
  } else {
	target_channel->GetTrace().ShiftPedestalToZero(0.0, 1000.0); //original
  	//target_channel->GetTrace().ShiftPedestalToZero(0.0, 800.0); //fmod 06/25/2014
	//fmod test code added 06/25/2014----------------------------------
	//double signal_begin;
	//cout << "Enter start time of signal:";
	//cin >> signal_begin;
	//target_channel->GetTrace().ShiftPedestalToZero(0.0, signal_begin); 
	//-----------------------------------------------------------------
  }
  
  return true;
} */
//-------------------------------------------------------------------//
/*
bool rec::Reconstruct::RecCurrent(const unsigned int channelId){
  if (fDetector == NULL)
	return false;
  det::Channel *det_channel = fDetector->GetChannel(channelId);
  if (det_channel == NULL)
	return false;
  if (!HasSecondACChannel(channelId)) {
	cout << "...rec::Reconstruct::RecCurrent: Channel " << channelId
		<< " doesn't have SecondAC Rec" << endl;
	return false;
  }
  cout << "...rec::Reconstruct::RecCurrentAC for Channel " << channelId << endl;
  // Let's Make the target channel
  MakeCurrentChannel(channelId);
  // And Get a pointer to it
  evt::Channel *target_channel = GetCurrentChannel(channelId);

  // We need to copy the source trace to the target trace
  target_channel->GetTrace() = GetSecondACChannel(channelId)->GetTrace();

  // target_channel->GetTrace().CorrectOneOverF(500.0, 500.0);

  double chan_gain = det_channel->GetTransimpGain();
  if (chan_gain != 0.0)
	chan_gain = 1.0/chan_gain;
  target_channel->GetTrace().MultiplyBy(chan_gain);

  // We want to subtract the pedestal
  // target_channel->GetTrace().ShiftPedestalToZero(0.0, 1000.0);

  // And deconvolve the trace
  target_channel->GetTrace().DeconvolveCurrent(det_channel->GetTransimpTau());

  return true;
} */
//-------------------------------------------------------------------//
/*
bool rec::Reconstruct::RecCharge(const unsigned int channelId){
  if (fDetector == NULL)
	return false;
  det::Channel *det_channel = fDetector->GetChannel(channelId);
  if (det_channel == NULL)
	return false;
  if (!HasCurrentChannel(channelId)) {
	cout << "...rec::Reconstruct::RecCharge: Channel " << channelId
		<< " doesn't have Current Rec" << endl;
	return false;
  }
  cout << "...rec::Reconstruct::RecCharge for Channel " << channelId << endl;
  // Let's Make the target channel
  MakeChargeChannel(channelId);
  // And Get a pointer to it
  evt::Channel *target_channel = GetChargeChannel(channelId);
  // We need to copy the source trace to the target trace
  target_channel->GetTrace() = GetCurrentChannel(channelId)->GetTrace();

  // target_channel->GetTrace().CorrectOneOverF(500.0, 500.0);
  // And integrate it
  target_channel->GetTrace().Integrate(6241.51); // 1fC = 6241.51 electrons

  return true;
} */
//-------------------------------------------------------------------//
void rec::Reconstruct::SetDetector(det::Detector*  detector){
  fDetector = detector;
} 
//-------------------------------------------------------------------//
void rec::Reconstruct::SetEvent(evt::Event* event){
	//FGSII 10/31/2014 this one line was the original code
	fEvent = event;

	//FGSII 10/31/2014 new code need this to update fEvent
	/*if(fEvent != NULL){
		delete fEvent;
		fEvent = NULL;
	}
	fEvent = event;*/

	//FGSII 11/03/2014 new code
	//fEvent->MakeClone(event);
}
//-------------------------------------------------------------------//
//FGSII add 09/09/2014
evt::Event* rec::Reconstruct::GetEvent(){
	return fEvent;
}
//-------------------------------------------------------------------//
//FGSII add 08/26/2014
unsigned int rec::Reconstruct::GetNofChanPedestal() const{
	return fPedestal.size();
}
//-------------------------------------------------------------------//

unsigned int rec::Reconstruct::GetNofChanFirstAC() const{
  return fFirstAC.size();
}
//-------------------------------------------------------------------//
unsigned int rec::Reconstruct::GetNofChanSecondAC() const{
  return fSecondAC.size();
} 
//-------------------------------------------------------------------//
unsigned int rec::Reconstruct::GetNofChanCurrent() const{
  return fCurrent.size();
}
//-------------------------------------------------------------------//
unsigned int rec::Reconstruct::GetNofChanCharge() const{
  return fCharge.size();
}
//-------------------------------------------------------------------//
//FGSII add on 08/26/2014
void rec::Reconstruct::MakePedestalChannel(const unsigned int channelId){
	if(HasPedestalChannel(channelId)){
		return;
	}
	evt::Channel *ChannelPnt = new evt::Channel();
	ChannelPnt->SetId(channelId);
	fPedestal.push_back(ChannelPnt);
}
//-------------------------------------------------------------------//
//FGSII add on 08/26/2014
bool rec::Reconstruct::HasPedestalChannel(const unsigned int channelId){
	for (ConstChannelIterator iter = ChanPedestalBegin(); iter != ChanPedestalEnd(); ++iter) {
		if ((*iter)->GetId() == channelId) {
			return true;
		}
	}
	return false;
}
//-------------------------------------------------------------------//
//FGSII add on 08/26/2014
evt::Channel *rec::Reconstruct::GetPedestalChannel(const unsigned int channelId){
  for (ConstChannelIterator iter = ChanPedestalBegin(); iter != ChanPedestalEnd(); ++iter) {
  	if ((*iter)->GetId() == channelId) {
		return (*iter);
	}
  }
  return NULL;
}
//-------------------------------------------------------------------//
//FGSII add on 08/26/2014
void rec::Reconstruct::ClearPedestal(){
cout << "......rec::Reconstruct::ClearPedestal()" << endl;

  for (ChannelIterator iter = ChanPedestalBegin(); iter != ChanPedestalEnd(); ++iter) {
	if ((*iter)!=NULL) {
		delete (*iter);
		(*iter) = NULL;
	}
  }
  fPedestal.clear();

}
//-------------------------------------------------------------------//
//FGSII add on 08/26/2014
void rec::Reconstruct::ClearPedestal(const unsigned int channelId){
  for (ChannelIterator iter = ChanPedestalBegin(); iter != ChanPedestalEnd(); ++iter) {
	//cout << "HERE" << endl;
	if ((*iter)->GetId() == channelId) {
		delete (*iter);
		fPedestal.erase(iter);
		break;
	}
  }
}
//-------------------------------------------------------------------//
void rec::Reconstruct::MakeFirstACChannel(const unsigned int channelId){
  if (HasFirstACChannel(channelId)) 
	return;
   evt::Channel *ChannelPnt = new evt::Channel();
   ChannelPnt->SetId(channelId);
   fFirstAC.push_back(ChannelPnt);
} 
//-------------------------------------------------------------------//
bool rec::Reconstruct::HasFirstACChannel(const unsigned int channelId){
  for (ConstChannelIterator iter = ChanFirstACBegin(); iter != ChanFirstACEnd(); ++iter) {
	if ((*iter)->GetId() == channelId) {
		return true;
	}
  }
  return false;
}
//-------------------------------------------------------------------//
evt::Channel *rec::Reconstruct::GetFirstACChannel(const unsigned int channelId){
  for (ConstChannelIterator iter = ChanFirstACBegin(); iter != ChanFirstACEnd(); ++iter) {
  	if ((*iter)->GetId() == channelId) {
		return (*iter);
	}
  }
  return NULL;
}
//-------------------------------------------------------------------//
void rec::Reconstruct::ClearFirstAC(){
  for (ChannelIterator iter = ChanFirstACBegin(); iter != ChanFirstACEnd(); ++iter) {
	if ((*iter)!=NULL) {
		delete (*iter);
		(*iter) = NULL;
	}
  }
  fFirstAC.clear();
}
//-------------------------------------------------------------------//
void rec::Reconstruct::ClearFirstAC(const unsigned int channelId){
  for (ChannelIterator iter = ChanFirstACBegin(); iter != ChanFirstACEnd(); ++iter) {
	if ((*iter)->GetId() == channelId) {
		delete (*iter);
		fFirstAC.erase(iter);
		break;
	}
  }
}
//-------------------------------------------------------------------//
void rec::Reconstruct::MakeSecondACChannel(const unsigned int channelId){
  if (HasSecondACChannel(channelId))
	return;
  evt::Channel* ChannelPnt = new evt::Channel();
  ChannelPnt->SetId(channelId);
  fSecondAC.push_back(ChannelPnt);
}
//-------------------------------------------------------------------//
bool rec::Reconstruct::HasSecondACChannel(const unsigned int channelId){
  for (ConstChannelIterator iter = ChanSecondACBegin(); iter != ChanSecondACEnd(); ++iter) {
	if ((*iter)->GetId() == channelId) {
		return true;
	}
  }
  return false;
}
//-------------------------------------------------------------------//
evt::Channel *rec::Reconstruct::GetSecondACChannel(const unsigned int channelId){
  for (ConstChannelIterator iter = ChanSecondACBegin(); iter != ChanSecondACEnd(); ++iter) {
	if ((*iter)->GetId() == channelId) {
		return (*iter);
	}
  }
  return NULL;
}
//-------------------------------------------------------------------//
void rec::Reconstruct::ClearSecondAC(){
  for (ChannelIterator iter = ChanSecondACBegin(); iter != ChanSecondACEnd(); ++iter) {
	if ((*iter)!=NULL) {
		delete (*iter);
		(*iter) = NULL;
	}
  }
  fSecondAC.clear();
} 
//-------------------------------------------------------------------//
void rec::Reconstruct::ClearSecondAC(const unsigned int channelId){
  for (ChannelIterator iter = ChanSecondACBegin(); iter != ChanSecondACEnd(); ++iter) {
	if ((*iter)->GetId() == channelId) {
		delete (*iter);
		fSecondAC.erase(iter);
		break;
	}
  }
}
//-------------------------------------------------------------------//
void rec::Reconstruct::MakeCurrentChannel(const unsigned int channelId){
  if (HasCurrentChannel(channelId))
	return;
  evt::Channel* ChannelPnt = new evt::Channel();
  ChannelPnt->SetId(channelId);
  fCurrent.push_back(ChannelPnt);
}
//-------------------------------------------------------------------//
bool rec::Reconstruct::HasCurrentChannel(const unsigned int channelId){
  for (ConstChannelIterator iter = ChanCurrentBegin(); iter != ChanCurrentEnd(); ++iter) {
	if ((*iter)->GetId() == channelId) {
		return true;
	}
  }
  return false;
}
//-------------------------------------------------------------------//
evt::Channel *rec::Reconstruct::GetCurrentChannel(const unsigned int channelId){
  for (ConstChannelIterator iter = ChanCurrentBegin(); iter != ChanCurrentEnd(); ++iter) {
	if ((*iter)->GetId() == channelId) {
		return (*iter);
	}
  }
  return NULL;
}
//-------------------------------------------------------------------//
void rec::Reconstruct::ClearCurrent(){
  for (ChannelIterator iter = ChanCurrentBegin(); iter != ChanCurrentEnd(); ++iter) {
	if ((*iter)!=NULL) {
		delete (*iter);
		(*iter) = NULL;
	}
  }
  fCurrent.clear();
} 
//-------------------------------------------------------------------//

void rec::Reconstruct::ClearCurrent(const unsigned int channelId){
  for (ChannelIterator iter = ChanCurrentBegin(); iter != ChanCurrentEnd(); ++iter) {
	if ((*iter)->GetId() == channelId) {
		delete (*iter);
		fCurrent.erase(iter);
		break;                                    
	}
  }
}
//-------------------------------------------------------------------//
void rec::Reconstruct::MakeChargeChannel(const unsigned int channelId){
  if (HasChargeChannel(channelId))
	return;
  evt::Channel* ChannelPnt = new evt::Channel();
  ChannelPnt->SetId(channelId);
  fCharge.push_back(ChannelPnt);
} 
//-------------------------------------------------------------------//
bool rec::Reconstruct::HasChargeChannel(const unsigned int channelId){
  for (ConstChannelIterator iter = ChanChargeBegin(); iter != ChanChargeEnd(); ++iter) {
	if ((*iter)->GetId() == channelId) {
		return true;
	}
  }
  return false;
}
//-------------------------------------------------------------------//
evt::Channel *rec::Reconstruct::GetChargeChannel(const unsigned int channelId){
  for (ConstChannelIterator iter = ChanChargeBegin(); iter != ChanChargeEnd(); ++iter) {
	if ((*iter)->GetId() == channelId) {
		return (*iter);
	}
  }
  return NULL;
} 
//-------------------------------------------------------------------//
void rec::Reconstruct::ClearCharge(){
  for (ChannelIterator iter = ChanChargeBegin(); iter != ChanChargeEnd(); ++iter) {
	if ((*iter)!=NULL) {
		delete (*iter);
		(*iter) = NULL;
	}
  }
  fCharge.clear();
} 
//-------------------------------------------------------------------//
void rec::Reconstruct::ClearCharge(const unsigned int channelId){
  for (ChannelIterator iter = ChanChargeBegin(); iter != ChanChargeEnd(); ++iter) {
	if ((*iter)->GetId() == channelId) {
		delete (*iter);
		fCharge.erase(iter);
		break;
	}
  }
}
//-------------------------------------------------------------------//
rec::Reconstruct::ChannelIterator rec::Reconstruct::ChanPedestalBegin(){
  return fPedestal.begin();
} 
//-------------------------------------------------------------------//

rec::Reconstruct::ChannelIterator rec::Reconstruct::ChanPedestalEnd(){
  return fPedestal.end();
} 
//-------------------------------------------------------------------//
rec::Reconstruct::ConstChannelIterator rec::Reconstruct::ChanPedestalBegin() const{
  return fPedestal.begin();
}
//-------------------------------------------------------------------//
rec::Reconstruct::ConstChannelIterator rec::Reconstruct::ChanPedestalEnd() const{
  return fPedestal.end();
}                                    
//-------------------------------------------------------------------//
rec::Reconstruct::ChannelIterator rec::Reconstruct::ChanFirstACBegin(){
  return fFirstAC.begin();
} 
//-------------------------------------------------------------------//
rec::Reconstruct::ChannelIterator rec::Reconstruct::ChanFirstACEnd(){
  return fFirstAC.end();
} 
//-------------------------------------------------------------------//
rec::Reconstruct::ConstChannelIterator rec::Reconstruct::ChanFirstACBegin() const{
  return fFirstAC.begin();
}
//-------------------------------------------------------------------//
rec::Reconstruct::ConstChannelIterator rec::Reconstruct::ChanFirstACEnd() const{
  return fFirstAC.end();
}
//-------------------------------------------------------------------//
rec::Reconstruct::ChannelIterator rec::Reconstruct::ChanSecondACBegin(){
  return fSecondAC.begin();
}
//-------------------------------------------------------------------//
rec::Reconstruct::ChannelIterator rec::Reconstruct::ChanSecondACEnd(){
  return fSecondAC.end();
}
//-------------------------------------------------------------------//
rec::Reconstruct::ConstChannelIterator rec::Reconstruct::ChanSecondACBegin() const{
  return fSecondAC.begin();
}
//-------------------------------------------------------------------//
rec::Reconstruct::ConstChannelIterator rec::Reconstruct::ChanSecondACEnd() const{
  return fSecondAC.end();
}
//-------------------------------------------------------------------//
rec::Reconstruct::ChannelIterator rec::Reconstruct::ChanCurrentBegin(){
  return fCurrent.begin();
}
//-------------------------------------------------------------------//
rec::Reconstruct::ChannelIterator rec::Reconstruct::ChanCurrentEnd(){
  return fCurrent.end();
}
//-------------------------------------------------------------------//
rec::Reconstruct::ConstChannelIterator rec::Reconstruct::ChanCurrentBegin() const{
  return fCurrent.begin();
}
//-------------------------------------------------------------------//
rec::Reconstruct::ConstChannelIterator rec::Reconstruct::ChanCurrentEnd() const{
  return fCurrent.end();
}
//-------------------------------------------------------------------//
rec::Reconstruct::ChannelIterator rec::Reconstruct::ChanChargeBegin(){
  return fCharge.begin();
}
//-------------------------------------------------------------------//
rec::Reconstruct::ChannelIterator rec::Reconstruct::ChanChargeEnd(){
  return fCharge.end();
}
//-------------------------------------------------------------------//
rec::Reconstruct::ConstChannelIterator rec::Reconstruct::ChanChargeBegin() const{
  return fCharge.begin();
}
//-------------------------------------------------------------------//
rec::Reconstruct::ConstChannelIterator rec::Reconstruct::ChanChargeEnd() const{
  return fCharge.end();
}       
//-------------------------------------------------------------------//
