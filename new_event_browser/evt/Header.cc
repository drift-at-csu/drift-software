#include <evt/Header.h>
using namespace drift;

//-------------------------------------------------------------------//
evt::Header::Header(){
}
//-------------------------------------------------------------------//
evt::Header::~Header(){
}
//-------------------------------------------------------------------//
const std::string& evt::Header::GetId() const{
  return fId;
}
//-------------------------------------------------------------------//
void evt::Header::SetId(const std::string& id){
  fId = id;
}
//-------------------------------------------------------------------//
const utl::TimeStamp& evt::Header::GetTime() const{
  return fTime;
}
//-------------------------------------------------------------------//
utl::TimeStamp& evt::Header::GetTime() {
  return fTime;
}
//-------------------------------------------------------------------//
void evt::Header::SetTime(const utl::TimeStamp& t){
  fTime = t;
}
//-------------------------------------------------------------------//
void evt::Header::SetEventTimeLength(double time_length){
	fEventTimeLength = time_length;
}
//-------------------------------------------------------------------//
double evt::Header::GetEventTimeLength(){
	return fEventTimeLength;
}
//-------------------------------------------------------------------//
//FGSII add 10/31/2014 not yet tested
void evt::Header::MakeClone(evt::Header& head){
	fTime = head.fTime;
	fId = head.fId;
	fEventTimeLength = head.fEventTimeLength;
}
//-------------------------------------------------------------------//

