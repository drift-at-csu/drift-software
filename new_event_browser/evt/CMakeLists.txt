set (DRIFT_Event_SOURCES)

include_directories (${DAS_DRIFT_SOURCE_DIR})
include_directories (${Root_INCLUDE_DIRS})

list ( APPEND DRIFT_Event_SOURCES
	Event.cc
	Header.cc
	Channel.cc
	WirePair.cc
)

add_library (DRIFT_Event STATIC ${DRIFT_Event_SOURCES})
