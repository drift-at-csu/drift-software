// #include <evt/Event.h>
#include <evt/Channel.h>
#include <iostream>
#include <algorithm> //FGSII add 09/24/2014 (sort)

#include <TGraph.h> //FGSII add on 09/29/2014, note this is not consistent
//with keeping ROOT out of these classes, but this is what I will do for now
#include <TF1.h> //FGSII add 09/29/2014

using namespace drift;
using namespace std;

//-------------------------------------------------------------------//
evt::Channel::Channel():
		fChanId(0), 
		fConstPedestal(0.0), //FGSII add 08/29/2014
		fSigRegionIsSet(false) //FGSII add 09/11/2014
		
		{
}
//-------------------------------------------------------------------//
evt::Channel::~Channel(){
}
//-------------------------------------------------------------------//
unsigned int evt::Channel::GetId() const{
  return fChanId;
}
//-------------------------------------------------------------------//
void evt::Channel::SetId(unsigned int chan_id){
  fChanId = chan_id;
}
//-------------------------------------------------------------------//
utl::Trace *evt::Channel::GetTrace(unsigned int trace_id){
	for (ConstTraceIterator iter = TracesBegin(); iter != TracesEnd(); ++iter) {
		if ((*iter)->GetTraceId() == trace_id) {
			return (*iter);
		}
	}
  return NULL;
}
//-------------------------------------------------------------------//
void evt::Channel::MakeTrace(unsigned int trace_id){
	utl::Trace *TracePnt = new utl::Trace();
	TracePnt->SetTraceId(trace_id);
   fTraces.push_back(TracePnt);
}
//-------------------------------------------------------------------//
evt::Channel::TraceIterator evt::Channel::TracesBegin(){
  return fTraces.begin();
}
//-------------------------------------------------------------------//
evt::Channel::TraceIterator evt::Channel::TracesEnd(){
  return fTraces.end();
}
//-------------------------------------------------------------------//
evt::Channel::ConstTraceIterator evt::Channel::TracesBegin() const{
  return fTraces.begin();
}
//-------------------------------------------------------------------//
evt::Channel::ConstTraceIterator evt::Channel::TracesEnd() const{
  return fTraces.end();
}
//-------------------------------------------------------------------//
unsigned int evt::Channel::GetNofTracesInChan() const{
  return fTraces.size();
}
//-------------------------------------------------------------------//
//FGSII add 09/18/2014
void evt::Channel::CalcConstPedestal_2(unsigned int ped_number){
	cout << "...evt::Channel::CalcConstPedestal_2()" << endl;

	cout << "......size of ped container = " << GetNofPedestals() << endl; //for debug

	//if there are no ped regions we exit
	if(GetNofPedestals() == 0){
		cout << "......ERROR: can't calculate pedestal, no pedestals in container" << endl;
		return;
	}

	//should also write in a check to make sure that GetPedestal(ped_number) exists

	//first we need to see if the pedestal region is all in the same trace
	//if so, this simplifies things
	bool is_one_trace;
	if(GetPedestal(ped_number).first.first == GetPedestal(ped_number).second.first){
		is_one_trace = true;
	}
	else{
		is_one_trace = false;
	}

	double ped_avg = 0.0;
	double sum = 0.0;
	//iterate through trace
	if(is_one_trace == true){
		for(unsigned int ii=GetPedestal(ped_number).first.second; ii<=GetPedestal(ped_number).second.second;
		ii++){
			sum += GetTrace(GetPedestal(ped_number).first.first)->GetTimeBin(ii);
			ped_avg = sum/(ii+1);
		}
	}
	else{
		//cout << "......code for a multiple trace pedestal does net yet exist" << endl;
		unsigned int counter = 0;
		for(unsigned int ii=GetPedestal(ped_number).first.first; ii<=GetPedestal(ped_number).second.first; ii++){
			if(ii != GetPedestal(ped_number).second.first){
				for(unsigned int jj=0; jj<GetTrace(ii)->GetNofTimeBins(); jj++){
					counter += 1;
					sum += GetTrace(ii)->GetTimeBin(jj);
					ped_avg = sum/(counter);
				}
			}
			else if(ii == GetPedestal(ped_number).second.first){
				for(unsigned int jj=0; jj<=GetPedestal(ped_number).second.second; jj++){
					counter += 1;
					sum += GetTrace(ii)->GetTimeBin(jj);
					ped_avg = sum/(counter);
				}
			}
		}
		cout << "......counter = " << counter << endl;
	}

	cout << "......avg = " << ped_avg << endl;

	SetConstPedestal(ped_avg);
}
//-------------------------------------------------------------------//
//FGSII add on 08/29/2014
void evt::Channel::SetConstPedestal(double ped_value){
	fConstPedestal = ped_value;
}
//-------------------------------------------------------------------//
//FGSII add on 08/29/2014
double evt::Channel::GetConstPedestal(){
	return fConstPedestal;
}
//-------------------------------------------------------------------//
//FGSII add 09/04/2014
evt::Channel evt::Channel::operator=(const evt::Channel& chan){
	fChanId = chan.fChanId;
	fTraces = chan.fTraces;//09/25/2014, not sure if this is correct
	
	fConstPedestal = chan.fConstPedestal;
	fSigBeginBinNum = chan.fSigBeginBinNum;
	fSigRegionIsSet = chan.fSigRegionIsSet;
	fPeds = chan.fPeds;
	fQuadParams = chan.fQuadParams;//FGSII add 09/29/2014
	fQuadFit = chan.fQuadFit;//FGSII add 09/30/2014

	return *this;
}
//-------------------------------------------------------------------//
//need to come back and check this, make sure everything copies correctly for all cases
void evt::Channel::MakeClone(evt::Channel *chan){
	cout << "...evt::Channel::MakeClone()" << endl;
	SetId(chan->GetId());
	SetConstPedestal(chan->GetConstPedestal());

	//fPeds = chan->fPeds;
	//FGSII 10/31/2014, below is a second more complicated way to copy peds
	for(evt::Channel::ConstPedsIterator iter = chan->PedsBegin(); iter != chan->PedsEnd(); ++iter){
		AddPedestal((*iter).first.first,(*iter).first.second,(*iter).second.first,(*iter).second.second);
	}

	fQuadParams = chan->fQuadParams;//FGSII add 09/29/2014
	fQuadFit = chan->fQuadFit;//FGSII add 09/30/2014
	fSigBeginBinNum = chan->fSigBeginBinNum;//FGSII add 10/15/2014, want to remove eventually

	for(evt::Channel::ConstTraceIterator iter = chan->TracesBegin(); iter != chan->TracesEnd(); iter++){
		MakeTrace((*iter)->GetTraceId());
		*(GetTrace((*iter)->GetTraceId())) = *(chan->GetTrace((*iter)->GetTraceId()));
	}
	
	
}
//-------------------------------------------------------------------//
//need to rewrite this to make use of the pedestals
void evt::Channel::FindSignalBegin(utl::Trace* trc){

	cout << "...evt::Channel::FindSigBegin()" << endl;
	cout << "......channel" << GetId() << endl;

	const unsigned int kSigNofBins=5; //number of bins we watch the rms for (whether inc. or dec.)

	unsigned int flag_counter = 0;
	double sum_of_squares = 0.0;
	double rms = 0.0;
	double previous_rms;
	double delta_rms = 0.0;
	unsigned int nof_bins = 0;
	unsigned int bin_num = 0;
	int current_flag; //let 0=neg and 1=pos
	int previous_flag;

	for (utl::Trace::TimeBinsIterator iter = trc->TimeBinsBegin(); iter != trc->TimeBinsEnd();
  		++iter){
		nof_bins += 1;
		bin_num = nof_bins - 1;
		sum_of_squares += (*iter)*(*iter);
		rms = sqrt(sum_of_squares/nof_bins);

		if(bin_num ==0){
			previous_rms = rms;
		}
		else if(bin_num == 1){
			delta_rms = rms - previous_rms;
			if(delta_rms < 0){
				current_flag = 0;
				flag_counter += 1;
			}
			else if(delta_rms > 0){
				current_flag = 1;
				flag_counter += 1;
			}
			//update previous rms and current flag
			previous_rms = rms;
			previous_flag = current_flag;
		}
		else{
			delta_rms = rms - previous_rms;
			//get state on current flag
			if(delta_rms < 0){
				current_flag = 0;
			}
			else if(delta_rms > 0){
				current_flag = 1;
			}
			
			//now compare flags and update counter
			if(previous_flag == current_flag){
				flag_counter += 1;
			}
			else{
				flag_counter = 0; //we reset the counter
			}

			if(flag_counter == kSigNofBins){
				SetSignalBegin(bin_num-kSigNofBins);
				cout << "......signal in trace " << trc->GetTraceId() << " begins in bin number " << GetSignalBegin() << endl;
				cout << "......signal begins at " << trc->GetTraceOffset() + GetSignalBegin()*(trc->GetBinDuration()) << " us" << endl;
				return;
			}
			

			//update previous rms and current flag
			previous_rms = rms;
			previous_flag = current_flag;
		}
	}
	
	
	cout << "......beginning of signal region was not found" << endl;
}
//-------------------------------------------------------------------//
//this can be deleted once I have finished writing the new algorithm
void evt::Channel::FindSignalBegin_3(utl::Trace* trc){

	cout << endl;
	cout << "...evt::Channel::FindSigBegin_3()" << endl;
	cout << "......channel" << GetId() << endl;

	unsigned int nof_bins=0;
	double sum=0;
	double avg=0;
	double sigma=0;
	double sum_of_diff_squared=0;
	
	unsigned int mult = 4;//value to multiply sigma by

	for (utl::Trace::TimeBinsIterator iter = trc->TimeBinsBegin(); iter != trc->TimeBinsEnd();
  		++iter){
		nof_bins += 1;
		sum_of_diff_squared = 0;//need to reset this to zero, must be recalculated each time

		if(nof_bins <= 10){
			sum += (*iter);
			avg = sum/nof_bins;
			//to properly calc this we need to iterate through all bins each time
			for(unsigned int ii=0; ii<nof_bins; ii++){
				sum_of_diff_squared += (trc->GetTimeBin(ii)-avg)*(trc->GetTimeBin(ii)-avg);
				sigma = sqrt(sum_of_diff_squared/nof_bins);
			}
		}
		else{
			//first see if signal is out of background
			if((*iter) <= (avg-mult*sigma) || (*iter) >= (avg+mult*sigma)){
				SetSignalBegin(nof_bins-1);
				SetConstPedestal(avg);
				cout << "......signal in trace " << trc->GetTraceId() << " begins in bin number " << GetSignalBegin() << endl;
				cout << "......signal begins at " << trc->GetTraceOffset() + GetSignalBegin()*(trc->GetBinDuration()) << " us" << endl;
				cout << "......bin value = " << (*iter) << endl;
				cout << "......avg = " << avg << ", sigma = " << sigma << endl;
				cout << "......avg - " << mult << "*sigma = " << avg-mult*sigma << endl;
				cout << "......avg + " << mult << "*sigma = " << avg+mult*sigma << endl;
				return;
			}

			//if signal is not we continue iterating
			sum += (*iter);
			avg = sum/nof_bins;
			for(unsigned int jj=0; jj<nof_bins; jj++){
				sum_of_diff_squared += (trc->GetTimeBin(jj)-avg)*(trc->GetTimeBin(jj)-avg);
				sigma = sqrt(sum_of_diff_squared/nof_bins);
			}
		}	
	}
	
	SetConstPedestal(avg);
	cout << "......beginning of signal region was not found" << endl;
}
//-------------------------------------------------------------------//
void evt::Channel::FindSignalBegin_3_2(utl::Trace* trc){

	//cout << endl;
	cout << "...evt::Channel::FindSigBegin_3_2()" << endl;
	cout << "......channel" << GetId() << endl;

	unsigned int nof_bins=0;
	double sum=0;
	double avg=0;
	double rms=0;//FGSIIadd 10/13/2014
	double sum_of_squares=0;//FGSII add 10/13/2014
	double sigma=0;
	double sum_of_diff_squared=0;
	
	unsigned int mult = 4;//value to multiply sigma by

	for (utl::Trace::TimeBinsIterator iter = trc->TimeBinsBegin(); iter != trc->TimeBinsEnd();
  		++iter){
		nof_bins += 1;
		sum_of_diff_squared = 0;//need to reset this to zero, must be recalculated each time

		if(nof_bins <= 10){
			sum += (*iter);
			avg = sum/nof_bins;
			sum_of_squares += (*iter)*(*iter);//FGSII add 10/13/2014
			rms = sqrt(sum_of_squares/nof_bins);//FGSII add 10/13/2014
			//to properly calc this we need to iterate through all bins each time
			for(unsigned int ii=0; ii<nof_bins; ii++){
				sum_of_diff_squared += (trc->GetTimeBin(ii)-avg)*(trc->GetTimeBin(ii)-avg);
				sigma = sqrt(sum_of_diff_squared/nof_bins);
			}
		}
		else{
			//first see if signal is out of background
			if((*iter) <= (avg-mult*sigma) || (*iter) >= (avg+mult*sigma)){
				//if else is for recursion purposes
				if(trc->GetTraceId() == 0){
					AddPedestal(trc->GetTraceId(),0,trc->GetTraceId(),nof_bins-1);
				}
				else{
					AddPedestal(0,0,trc->GetTraceId(),nof_bins-1);
				}
				SetSignalBegin(nof_bins-1); //replace with setting first ped //FGSII 10/13/2014 need to keep for now so cout is correct
				//SetConstPedestal(avg);
				cout << "......signal in trace " << trc->GetTraceId() << " begins in bin number " << GetSignalBegin() << endl;
				cout << "......signal begins at " << trc->GetTraceOffset() + GetSignalBegin()*(trc->GetBinDuration()) << " us" << endl;
				cout << "......bin value = " << (*iter) << endl;
				cout << "......avg = " << avg << ", sigma = " << sigma << endl;
				//cout << "......rms = " << rms << endl;
				//cout << "......sqrt(avg^2 + sigma^2) = " << sqrt(avg*avg+sigma*sigma) << endl;
				//cout << "......avg - " << mult << "*sigma = " << avg-mult*sigma << endl;
				//cout << "......avg + " << mult << "*sigma = " << avg+mult*sigma << endl;
				/*double max = trc->GetMax();
				double min = trc->GetMin();
				double max_minus_avg = max-avg;
				double min_minus_avg = min-avg;
				double sig_to_noise_max = abs(max_minus_avg)/sigma;
				double sig_to_noise_min = abs(min_minus_avg)/sigma;

				cout << "......max = " << max << endl;
				cout << "......min = " << min << endl;
				cout << "......max_minus_avg = " << max_minus_avg << endl;
				cout << "......min_minus_avg = " << min_minus_avg << endl;
				cout << "......sig_to_noise_max = " << sig_to_noise_max << endl;
				cout << "......sig_to_noise_min = " << sig_to_noise_min << endl;*/
				return;
			}

			//if signal is not we continue iterating
			sum += (*iter);
			avg = sum/nof_bins;
			for(unsigned int jj=0; jj<nof_bins; jj++){
				sum_of_diff_squared += (trc->GetTimeBin(jj)-avg)*(trc->GetTimeBin(jj)-avg);
				sigma = sqrt(sum_of_diff_squared/nof_bins);
			}
		}	
	}
	
	//if no signal region is found, then there is only one pedestal, and it spans the 
	//entire trace
	//AddPedestal(trc->GetTraceId(),0,trc->GetTraceId(),trc->GetNofTimeBins()-1);
	//cout << "......avg = " << avg << endl;
	//cout << "......beginning of signal region was not found" << endl;
	//cout << "......one ped was created spanning entire trace" << endl;

	//instead,
	//let's recursively call function to keep searching for signal if none is found in
	//first trace
	//this isn't the best way to do things, I should keep track of the previous avg and sigma
	//this improvement needs to be made
	if(trc->GetTraceId() < (GetNofTracesInChan()-1)){
		FindSignalBegin_3_2(GetTrace(trc->GetTraceId()+1));
	}
	else{
		AddPedestal(0,0,trc->GetTraceId(),trc->GetNofTimeBins()-1);
	}
	
}
//-------------------------------------------------------------------//
void evt::Channel::SetSignalBegin(unsigned int begin_bin){
		fSigBeginBinNum = begin_bin; 
		fSigRegionIsSet = true; //add 09/11/2014
}
//-------------------------------------------------------------------//
unsigned int evt::Channel::GetSignalBegin(){
	return fSigBeginBinNum;
}
//-------------------------------------------------------------------//
//FGSII add 09/17/2014
//iterate through each trace in channel and determine if signal is 
//continuous temporally
//currently not finished, just outputting values to see what constraints I
//need to apply
/*
void evt::Channel::DetermineSigContinuity(){
		cout << "\nevt::Channel::DetermineSigContinuity()" << endl;

		long trc_sec, trc_nsec;
		double real_offset, calc_offset;
		for (ConstTraceIterator iter = TracesBegin(); iter != TracesEnd(); ++iter) {
			trc_sec = (*iter)->GetTraceTimeSec();
			trc_nsec = (*iter)->GetTraceTimeNsec();
			real_offset = (*iter)->GetTraceOffset();
			//calc_offset = (GetNofTracesInChan()-(*iter)->GetTraceId()-1)*((*iter)->GetNofTimeBins())*((*iter)->GetBinDuration())+(GetTrace(GetNofTracesInChan()-1)->GetTraceOffset()); //works if last trace is first chronologically
			calc_offset = ((*iter)->GetTraceId())*((*iter)->GetNofTimeBins())*((*iter)->GetBinDuration())+GetTrace(0)->GetTraceOffset(); //works if trace 0 is first chronologically
			cout << "trace " << (*iter)->GetTraceId() << " time is: (" << trc_sec << "," << trc_nsec << ")" << endl;
			cout << "nof_bins = " << (*iter)->GetNofTimeBins() << endl;
			cout << "real_offset = " << real_offset << endl;
			cout << "calc_offset = " << calc_offset << endl;
			cout << "real-calc = " << real_offset - calc_offset << endl;
		}


}*/
//-------------------------------------------------------------------//
//FGSII add 09/18/2014
void evt::Channel::AddPedestal(unsigned int trc1Id, unsigned int trc1Bin, unsigned int trc2Id, unsigned int trc2Bin){
	
	cout << "...evt::Channel::AddPedestal()" << endl;
	
	PedType new_ped;
	new_ped.first.first = trc1Id;
	new_ped.first.second = trc1Bin;
	new_ped.second.first = trc2Id;
	new_ped.second.second = trc2Bin;

	fPeds.push_back(new_ped);
	cout <<"......size of ped container = " << GetNofPedestals() << endl;
}
//-------------------------------------------------------------------//
//FGSII add 09/18/2014
evt::Channel::PedType evt::Channel::GetPedestal(const unsigned int ped_number){
	//FGSII 11/03/2014 tried to include some logic to return error if it doesn't exist
	if(GetNofPedestals() == (ped_number+1) ){
		return fPeds[ped_number];
	}
	else{
		cout << "......ERROR: fPeds[" << ped_number << "] does not exist" << endl;
	}
}
//-------------------------------------------------------------------//
//FGSII add 09/18/2014
void evt::Channel::ClearPedestals(){
	fPeds.clear();
}
//-------------------------------------------------------------------//
//FGSII add 09/18/2014
unsigned int evt::Channel::GetNofPedestals() const{
  return fPeds.size();
}
//-------------------------------------------------------------------//
//FGSII add 09/18/2014
evt::Channel::PedsIterator evt::Channel::PedsBegin(){
  return fPeds.begin();
}
//-------------------------------------------------------------------//
//FGSII add 09/18/2014
evt::Channel::PedsIterator evt::Channel::PedsEnd(){
  return fPeds.end();
}
//-------------------------------------------------------------------//
//FGSII add 09/18/2014
evt::Channel::ConstPedsIterator evt::Channel::PedsBegin() const{
  return fPeds.begin();
}
//-------------------------------------------------------------------//
//FGSII add 09/18/2014
evt::Channel::ConstPedsIterator evt::Channel::PedsEnd() const{
  return fPeds.end();
}
//-------------------------------------------------------------------//
//FGSII add 09/18/2014
//will sort traces in ascending chron order
void evt::Channel::SortTraces(){
	cout << "...evt::Channel::SortTraces()" << endl;
	//fTraces.sort();//need to overload "<" operator to use this
	fTraces.sort(CompareTracesByTimeStamp);//using comparator function instead

	//re-Id each trace to reflect the sort we just did
	unsigned int ii=0;
	for (ConstTraceIterator iter = TracesBegin(); iter != TracesEnd(); ++iter) {
		(*iter)->SetTraceId(ii);
		ii += 1;
	}

	//for debug
	for (ConstTraceIterator iter = TracesBegin(); iter != TracesEnd(); ++iter) {
		cout << "......Trace " << (*iter)->GetTraceId() << " time = (" << (*iter)->GetTraceTimeSec()
		<< "," << (*iter)->GetTraceTimeNsec() << ")" << endl;
	}

	
}
//-------------------------------------------------------------------//
//FGSII add 09/18/2014
//will reverse the order of traces in the container
void evt::Channel::ReverseTraces(){
	cout << "...evt::Channel::ReverseTraces()" << endl;
	//first reverse elements in container
	fTraces.reverse();
	//re-Id each trace to reflect the reversal that we just did
	unsigned int ii=0;
	for (ConstTraceIterator iter = TracesBegin(); iter != TracesEnd(); ++iter) {
		(*iter)->SetTraceId(ii);
		ii += 1;
	}

	//for debug
	for (ConstTraceIterator iter = TracesBegin(); iter != TracesEnd(); ++iter) {
		cout << "......Trace " << (*iter)->GetTraceId() << " time = (" << (*iter)->GetTraceTimeSec()
		<< "," << (*iter)->GetTraceTimeNsec() << ")" << endl;
	}
}
//-------------------------------------------------------------------//
//FGSII add 09/24/2014
//NOTE: this is not a member of class "Channel"
bool CompareTracesByTimeStamp(utl::Trace *trc1, utl::Trace *trc2){
	/*return(trc1->GetTraceTimeSec() < trc2->GetTraceTimeSec() ||
	(trc1->GetTraceTimeSec() == trc2->GetTraceTimeSec() &&
	trc1->GetTraceTimeNsec() < trc2->GetTraceTimeNsec() ) );*/
	return(trc1->GetTraceOffset() < trc2->GetTraceOffset());
}
//-------------------------------------------------------------------//
void evt::Channel::FindSignalEnd1(utl::Trace* trc){

	cout << endl;
	cout << "...evt::Channel::FindSignalEnd1(utl::Trace*)" << endl;
	cout << "......channel" << GetId() << endl;

	unsigned int nof_bins=0;
	double sum=0;
	double avg=0;
	double sigma=0;
	double sum_of_diff_squared=0;
	
	unsigned int mult = 4;//value to multiply sigma by

	for (utl::Trace::TimeBinsIterator iter = trc->TimeBinsEnd(); iter != trc->TimeBinsBegin();
  		--iter){
		nof_bins += 1;
		sum_of_diff_squared = 0;//need to reset this to zero, must be recalculated each time

		if(nof_bins <= 10){
			sum += (*iter);
			avg = sum/nof_bins;
			//to properly calc this we need to iterate through all bins each time
			for(unsigned int ii=0; ii<nof_bins; ii++){
				sum_of_diff_squared += (trc->GetTimeBin(ii)-avg)*(trc->GetTimeBin(ii)-avg);
				sigma = sqrt(sum_of_diff_squared/nof_bins);
			}
		}
		else{
			//first see if signal is out of background
			if((*iter) <= (avg-mult*sigma) || (*iter) >= (avg+mult*sigma)){

			AddPedestal(trc->GetTraceId(),trc->GetNofTimeBins()-nof_bins, GetNofTracesInChan()-1, GetTrace(GetNofTracesInChan()-1)->GetNofTimeBins()-1 );

			//shouldn't need if else, firt case is not unique
			/*if(trc->GetTraceId() == (GetNofTracesInChan()-1)){
					AddPedestal(trc->GetTraceId(),trc->GetNofTimeBins()-nof_bins, trc->GetTraceId(), trc->GetNofTimeBins());
				}
			else{
					AddPedestal(trc->GetTraceId(),trc->GetNofTimeBins()-nof_bins, GetNofTracesInChan()-1, GetTrace(GetNofTracesInChan()-1)->GetNofTimeBins());
				} */				

				//SetSignalBegin(nof_bins-1); //replace with setting first ped
				//SetConstPedestal(avg);
				cout << "......signal in trace " << trc->GetTraceId() << " begins in bin number " << GetSignalBegin() << endl;
				cout << "......signal begins at " << trc->GetTraceOffset() + GetSignalBegin()*(trc->GetBinDuration()) << " us" << endl;
				cout << "......bin value = " << (*iter) << endl;
				cout << "......avg = " << avg << ", sigma = " << sigma << endl;
				cout << "......avg - " << mult << "*sigma = " << avg-mult*sigma << endl;
				cout << "......avg + " << mult << "*sigma = " << avg+mult*sigma << endl;
				return;
			}

			//if signal is not we continue iterating
			sum += (*iter);
			avg = sum/nof_bins;
			for(unsigned int jj=0; jj<nof_bins; jj++){
				sum_of_diff_squared += (trc->GetTimeBin(jj)-avg)*(trc->GetTimeBin(jj)-avg);
				sigma = sqrt(sum_of_diff_squared/nof_bins);
			}
		}	
	}
	
	//let's recursively call function to keep searching for signal if none is found in
	//first trace
	//this isn't the best way to do things, I should keep track of the previous avg and sigma
	//this improvement needs to be made
	if(trc->GetTraceId() < GetNofTracesInChan() && trc->GetTraceId() > 0){
		FindSignalEnd1(GetTrace(trc->GetTraceId()-1));
	}
	else{
		//no need to add a pedestal, this would just be the same as the pedestal added
		//from FindSignalBegin
		//AddPedestal(0,0,trc->GetTraceId(),trc->GetNofTimeBins()-1);
	}
	
}
//-------------------------------------------------------------------//
//FGSII add 09/29/2014
void evt::Channel::CalcQuadraticFitParams(){
	int n=0;//nof points in graph
	
	unsigned int jj_min, jj_max;
	//first we need to set n
	for(evt::Channel::ConstPedsIterator iter = PedsBegin(); iter != PedsEnd(); ++iter){
		for(unsigned int ii = (*iter).first.first; ii <= (*iter).second.first; ++ii){
			if(ii == (*iter).first.first && ii == (*iter).second.first){
				jj_min = (*iter).first.second;
				jj_max = (*iter).second.second;
			}
			else if(ii == (*iter).first.first && ii != (*iter).second.first){
				jj_min = (*iter).first.second;
				jj_max = GetTrace(ii)->GetNofTimeBins();
			}
			else if (ii == (*iter).second.first && ii != (*iter).first.first){
				jj_min = 0;
				jj_max = (*iter).second.second;
			}
			else{
				jj_min = 0;
				jj_max = GetTrace(ii)->GetNofTimeBins();
			}

			for(unsigned int jj = jj_min; jj < jj_max; ++jj){
				n += 1;	
			}
		}
	}

	//now we need to fill two arrays of size n, the x,y coords of each point in graph
	double x[n],y[n];
	unsigned int i = 0;
	/*for(evt::Channel::ConstPedsIterator iter = PedsBegin(); iter != PedsEnd(); ++iter){
		for(unsigned int ii = (*iter).first.first; ii <= (*iter).second.first; ++ii){
			for(unsigned int jj = 0; jj < GetTrace(ii)->GetNofTimeBins(); ++jj){
				//offset and bin_dur should both be in usec
				x[i] = GetTrace(ii)->GetTraceOffset()+(GetTrace(ii)->GetBinDuration())*(jj+1);
				y[i] = GetTrace(ii)->GetTimeBin(jj);
				i += 1;	
			}
		}
	}*/
	for(evt::Channel::ConstPedsIterator iter = PedsBegin(); iter != PedsEnd(); ++iter){
		for(unsigned int ii = (*iter).first.first; ii <= (*iter).second.first; ++ii){
			if(ii == (*iter).first.first && ii == (*iter).second.first){
				jj_min = (*iter).first.second;
				jj_max = (*iter).second.second;
			}
			else if(ii == (*iter).first.first && ii != (*iter).second.first){
				jj_min = (*iter).first.second;
				jj_max = GetTrace(ii)->GetNofTimeBins();
			}
			else if (ii == (*iter).second.first && ii != (*iter).first.first){
				jj_min = 0;
				jj_max = (*iter).second.second;
			}
			else{
				jj_min = 0;
				jj_max = GetTrace(ii)->GetNofTimeBins();
			}

			for(unsigned int jj = jj_min; jj < jj_max; ++jj){
				x[i] = GetTrace(ii)->GetTraceOffset()+(GetTrace(ii)->GetBinDuration())*(jj+1);
				y[i] = GetTrace(ii)->GetTimeBin(jj);
				i += 1;	
			}
		}
	}
	
	//now we create a TGraph on the heap as well as the fitting function
	TGraph *gr = new TGraph(n,x,y);
	TF1 *quad_fit = new TF1("quad_fit","pol2",x[0],x[n-1]);
	
	//fit the graph and get the fit params
	gr->Fit(quad_fit,"R");

	//Set the params
	SetQuadraticFitParams(quad_fit->GetParameter(0),quad_fit->GetParameter(1),quad_fit->GetParameter(2));
	SetQuadraticFit(quad_fit);

	//delete TGraph from heap as well as the fit
	delete gr;
	//delete quad_fit;//don't do this, it clears fQuadFit

}
//-------------------------------------------------------------------//
//FGSII add 09/29/2014
void evt::Channel::SetQuadraticFitParams(double param0, double param1, double param2){
	fQuadParams[0] = param0;
	fQuadParams[1] = param1;
	fQuadParams[2] = param2;
	
	cout << "...evt::Channel::SetQuadraticFitParams(double,double,double)" << endl;
	cout << "......fit params are: p0 = " << param0 << " p1 = " << param1 << " p2 = " << param2 << endl;
}
//-------------------------------------------------------------------//
//FGSII add 09/29/2014
evt::Channel::QuadParams evt::Channel::GetQuadraticFitParams(){
	return fQuadParams;
}
//-------------------------------------------------------------------//
//FGSII add 09/29/2014
void evt::Channel::SetQuadraticFit(TF1* fit){
	fQuadFit = fit;
}
//-------------------------------------------------------------------//
//FGSII add 09/29/2014
TF1* evt::Channel::GetQuadraticFit(){
	return fQuadFit;
}
//-------------------------------------------------------------------//
//FGSII add 10/31/2014
bool evt::Channel::TracesAreTimeContigous(utl::Trace* trc1, utl::Trace* trc2){
	unsigned int N = 1;//N is the allowed tolerance

	double trc1_end_time, trc2_begin_time;
	trc1_end_time = trc1->GetTraceOffset() + (trc1->GetNofTimeBins())*(trc1->GetBinDuration());
	trc2_begin_time = trc2->GetTraceOffset();

	if( (trc2_begin_time-trc1_end_time) <= (N*trc2->GetBinDuration()) ){
		return true;
	}
	else{
		return false;
	}

}
//-------------------------------------------------------------------//

















