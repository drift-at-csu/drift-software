#ifndef _DRIFT_evt_Channel_h_
#define _DRIFT_evt_Channel_h_

//note anything marked with "NTD" implies "Need To Delete"
//note anything marked with "NTM" implies "Need To Modify"

#include <utl/Trace.h>

//FGSII add 09/05/2014
#include <utl/TimeStamp.h>

#include <list>
#include <cmath> //FGSII add on 08/29/2014 to calc pedestal
#include <array> //FGSII add 09/29/2014 note:need to compile with c++11 use flag "-std=c++11"

#include <TF1.h>//FGSII add 09/30/2014

//add a function to compare traces chronologically
//FGSII add 09/24/2014
bool CompareTracesByTimeStamp(drift::utl::Trace *trc1, drift::utl::Trace *trc2);

namespace drift {
namespace evt {
  class Event;

class Channel {
  public:
  	Channel();
	~Channel();

	//FGSII add 09/18/2014
	//The below are used to characterize the pedestal regions.
	//There may exist several pedestal regions in a give channel, a pedestal can be completely specified
	//with a pair of pairs. The first pair stores the trace id and bin number in that trace for which the
	// pedestal begins. The Second pair stores the trace id and bin number in that trace for which the
	//pedestal ends;
	//As of right now I will only find two ped regions, the first will be that before the signal region,
	//the second will be that after the signal region.
	//PedType => < <trace_id_begin,begin_bin_in_trace> , <trace_id_end,end_bin_in_trace> >
	typedef std::pair< std::pair<unsigned int, unsigned int>, std::pair<unsigned int, unsigned> > PedType;
	typedef std::vector<PedType> PedsContainer;
	typedef PedsContainer::iterator PedsIterator;
	typedef PedsContainer::const_iterator ConstPedsIterator;

	//FGSII add 09/29/2014 
	typedef std::array<double,3> QuadParams; //quadratic fit parameters
	//note: if we declare "QuadParams params", then,
	//params[0] => constant term
	//params[1] => linear term
	//params[2] => quadratic term

	//FGSII add 07/07/2014
	typedef std::list<utl::Trace*> TraceContainer;
	typedef TraceContainer::iterator TraceIterator;
	typedef TraceContainer::const_iterator ConstTraceIterator;
	void MakeTrace(unsigned int trace_id);
	TraceIterator TracesBegin();
	TraceIterator TracesEnd();
	ConstTraceIterator TracesBegin() const;
	ConstTraceIterator TracesEnd() const;
	unsigned int GetNofTracesInChan() const;

	void MakeClone(Channel*); //FGSII add 09/25/2014


	unsigned int GetId() const;
	void SetId(unsigned int chan_id);

	//FGSII add on 08/29/2014
	void CalcConstPedestal_2(unsigned int ped_number); //add 09/18/2014
	void SetConstPedestal(double ped_value);
	double GetConstPedestal();

	//FGSII add 07/09/2014
	utl::Trace *GetTrace(unsigned int trace_id);
	//FGSII add 09/18/2014
	void SortTraces(); //sort the container chronologically
	void ReverseTraces(); //reverse positions of traces in container
	//FGSII add 10/31/2014
	bool TracesAreTimeContigous(utl::Trace* trc1, utl::Trace* trc2);

	//FGSII add 09/05/2014-------------------------------------------
	//functions used to identify signal region in channel
	void DetermineSigContinuity(); //use with fSigIsContinous
	//void SetSigContinuity(bool bool_val); //use with fSigIsContinous
	//bool GetSigContinuity(); //use with fSigIsContinous

	void FindSignalBegin(utl::Trace* trc); //use with fSigBegin , NTM
	void FindSignalBegin_3(utl::Trace* trc); //NTD
	void FindSignalBegin_3_2(utl::Trace* trc);
	void SetSignalBegin(unsigned int begin_bin); //use with fSigBeginBinNum , NTD
	unsigned int GetSignalBegin(); //use with fSigBeginBinNum , NTD

	void FindSignalEnd1(utl::Trace* trc); //use with fSigEnd
	//void SetSignalEnd1(unsigned int end_bin); //use with fSigEndTime
	//unsigned int GetSignalEnd1(); //use with fSigEndTime
	//----------------------------------------------------------------

	//FGSII add 09/29/2014--------------------------------------------
	//===> fitting funcitons
	void CalcQuadraticFitParams();
	void SetQuadraticFitParams(double param0, double param1, double param2);
	QuadParams GetQuadraticFitParams();

	void SetQuadraticFit(TF1* fit);
	TF1* GetQuadraticFit();
	//----------------------------------------------------------------

	//FGSII add 09/18/2014--------------------------------------------
	///===> Setters and getters for Pedestals
	unsigned int GetNofPedestals() const;//FGSII comment out 09/05/2014
	PedType GetPedestal(const unsigned int ped_number);
	// //const PedType GetPedestal(const unsigned int ped_number);
	PedsIterator PedsBegin();
	PedsIterator PedsEnd();
	ConstPedsIterator PedsBegin() const;
	ConstPedsIterator PedsEnd() const;
	void AddPedestal(unsigned int trc1Id, unsigned int trc1Bin, unsigned int trc2Id, unsigned int trc2Bin);
	void ClearPedestals();
	//----------------------------------------------------------------

	//FGSII add 09/04/2014
	Channel operator=(const Channel& chan);

  //private:
	protected: //FGSII mod 10/13/2014, want Wire to inherit from Channel
	unsigned int fChanId;
	double fConstPedestal;
	 
	//FGSII add 07/07/2014
	TraceContainer fTraces;

	//FGSII add 09/05/2014-------------------------------------------
	//these are fields used to identify signal region in channel
	unsigned int fSigBeginBinNum;
	//unsigned int fSigEndBinNum;
	//utl::TimeStamp fSigBeginTime; //begin time of signal
	//utl::TimeStamp fSigEnd; //end time of signal
	bool fSigRegionIsSet; //tells your if signal region has been set for the channel add 09/11/2014 , NTM

	//bool fSigIsContinous; //a flag that tells you if the signal is continuous temporally
	PedsContainer fPeds;

	//FGSII add 09/29/2014, this is the field that holds the quadratic fit parameters
	QuadParams fQuadParams;

	//FGSII add 09/30/2014, may not need to store fit parameters, just store the whole fit
	TF1* fQuadFit;
	//----------------------------------------------------------------



}; /* class Channel */
} /* namespace evt */
} /* namespace drift */

#endif /* _DRIFT_evt_Channel_h_ */
