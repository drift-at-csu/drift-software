#include <iostream>
#include <evt/WirePair.h>

using namespace drift;
using namespace std;

//-------------------------------------------------------------------//
//evt::WirePair::WirePair(evt::Channel* chan):
evt::WirePair::WirePair()//:
//fWirePair.first(NULL),
//fWirePair.second(NULL),
//fWirePair(NULL)
		{
	SetWireMap();
	//MakeWirePair(chan);

}
//-------------------------------------------------------------------//
evt::WirePair::~WirePair(){
}
//-------------------------------------------------------------------//
void evt::WirePair::MakeWirePair(evt::Channel* chan){
	cout << "...evt::WirePair::MakeWirePair()" << endl;
	
	//FGSII add 10/31/2014
	SetWirePairId(chan->GetId());

	//create pointers to pos and neg wires
	evt::Channel* pos_wire = new evt::Channel();
	evt::Channel* neg_wire = new evt::Channel();

	cout << "......WirePair::MakeWirePair1" << endl;

	//clone channel into both wires
	pos_wire->MakeClone(chan);
	neg_wire->MakeClone(chan);

	cout << "......WirePair::MakeWirePair2" << endl;

	//fWirePair.first = pos_wire;
	//fWirePair.second = neg_wire;

	fWirePair = WirePairType(pos_wire,neg_wire);

	InvertNegWire();

}
//-------------------------------------------------------------------//
evt::WirePair::WirePairType evt::WirePair::GetWirePair(){
	//if(fWirePair->first != NULL && fWirePair->second != NULL){
		return fWirePair;
	//}
}
//-------------------------------------------------------------------//
unsigned int evt::WirePair::GetPosWireId(){
	WireMapKeyType key;
	key.first = GetWirePair().first->GetId();
	key.second = "p";

	return GetWireMap(key);
}
//-------------------------------------------------------------------//
unsigned int evt::WirePair::GetNegWireId(){
	WireMapKeyType key;
	key.first = GetWirePair().second->GetId();
	key.second = "n";
	
	return GetWireMap(key);
}
//-------------------------------------------------------------------//
unsigned int evt::WirePair::GetWireMap(WireMapKeyType key){
	return fWireMap[key];
}
//-------------------------------------------------------------------//
void evt::WirePair::InvertNegWire(){
	evt::Channel* temp_chan = new evt::Channel;
	temp_chan->MakeClone(GetWirePair().second);
	temp_chan->SortTraces();
	temp_chan->FindSignalBegin_3_2(temp_chan->GetTrace(0));
	//temp_chan->FindSignalEnd1(temp_chan->GetTrace(temp_chan->GetNofTracesInChan()-1));
	temp_chan->CalcConstPedestal_2(0);
	double ped = temp_chan->GetConstPedestal();
	//temp_chan->CalcQuadraticFitParams();

	//temp_chan is no longer needed
	delete temp_chan;

	//we have the ped, now we traces in the channel
	for(evt::Channel::ConstTraceIterator iter = GetWirePair().second->TracesBegin(); iter != GetWirePair().second->TracesEnd(); iter++){
		(*iter)->SubtractConstPedestal(ped);//sub
		(*iter)->MultiplyBy(-1.0);//invert
		(*iter)->AddConst(ped);//add the ped back on
		
		//could write in stuff here to do the quadratic version of this also
	}

}
//-------------------------------------------------------------------//
	//typedef std::pair <unsigned int ChId, std::string sign> WireMapKeyType
	//typedef std::map <WireMapKeyType,unsigned int WireId> WireMap
void evt::WirePair::SetWireMap(){
	//evt::Wire::WireMapKeyType temp_key;
	//unsigned int temp_wire;
	const unsigned int nof_chans = 48;

	unsigned int pos_array[nof_chans];
	unsigned int neg_array[nof_chans];

	//index in array gives channel num and the element value gives the wire num
	//these have to be set manually-------------------------
	pos_array[0] = 1;
	pos_array[1] = 3;
	pos_array[2] = 8;
	pos_array[3] = 6;
	pos_array[4] = 10;
	pos_array[5] = 12;
	pos_array[6] = 25;
	pos_array[7] = 23;
	pos_array[8] = 27;
	pos_array[9] = 21;
	pos_array[10] = 40;
	pos_array[11] = 38;
	pos_array[12] = 42;
	pos_array[13] = 44;
	pos_array[14] = 56;
	pos_array[15] = 54;
	pos_array[16] = 58;
	pos_array[17] = 60;
	pos_array[18] = 72;
	pos_array[19] = 70;
	pos_array[20] = 74;
	pos_array[21] = 76;
	pos_array[22] = 96; //THIS MIGHT BE 22n instead
	pos_array[23] = 86;
	pos_array[24] = 2;
	pos_array[25] = 4;
	pos_array[26] = 13;
	pos_array[27] = 15;
	pos_array[28] = 19;
	pos_array[29] = 17;
	pos_array[30] = 30;
	pos_array[31] = 32;
	pos_array[32] = 45;
	pos_array[33] = 47;
	pos_array[34] = 36;
	pos_array[35] = 34;
	pos_array[36] = 61;
	pos_array[37] = 63;
	pos_array[38] = 51;
	pos_array[39] = 49;
	pos_array[40] = 77;
	pos_array[41] = 79;
	pos_array[42] = 67;
	pos_array[43] = 65;
	pos_array[44] = 93;
	pos_array[45] = 95;
	pos_array[46] = 83;
	pos_array[47] = 81;

	neg_array[0] = 89; //wasn't used
	neg_array[1] = 90; //wasn't used
	neg_array[2] = 16;
	neg_array[3] =	14;
	neg_array[4] = 18;
	neg_array[5] = 20;
	neg_array[6] = 33;
	neg_array[7] = 31;
	neg_array[8] = 35;
	neg_array[9] = 29;
	neg_array[10] = 48;
	neg_array[11] = 46;
	neg_array[12] = 50;
	neg_array[13] = 52;
	neg_array[14] = 64;
	neg_array[15] = 62;
	neg_array[16] = 66;
	neg_array[17] = 68;
	neg_array[18] = 80;
	neg_array[19] = 78;
	neg_array[20] = 82;
	neg_array[21] = 84;
	neg_array[22] = 88; //THIS MIGHT BE 22p instead
	neg_array[23] = 94;
	neg_array[24] = 91; //wasn't used
	neg_array[25] = 92; //wasn't used
	neg_array[26] = 5;
	neg_array[27] = 7;
	neg_array[28] = 11;
	neg_array[29] = 9;
	neg_array[30] = 22;
	neg_array[31] = 24;
	neg_array[32] = 37;
	neg_array[33] = 39;
	neg_array[34] = 28;
	neg_array[35] = 26;
	neg_array[36] = 53;
	neg_array[37] = 55;
	neg_array[38] = 43;
	neg_array[39] = 41;
	neg_array[40] = 69;
	neg_array[41] = 71;
	neg_array[42] = 59;
	neg_array[43] = 57;
	neg_array[44] = 85;
	neg_array[45] = 87;
	neg_array[46] = 75;
	neg_array[47] = 73;
 	//------------------------------------------------------
	

//add positive channels to map
	for(unsigned int ii=0; ii<nof_chans; ++ii){
		fWireMap.insert(std::pair<evt::WirePair::WireMapKeyType,unsigned int> (evt::WirePair::WireMapKeyType(ii,"p"), pos_array[ii]));	
	}

	//add negative channels to map
	for(unsigned int ii=0; ii<nof_chans; ++ii){
		fWireMap.insert(std::pair<evt::WirePair::WireMapKeyType,unsigned int> (evt::WirePair::WireMapKeyType(ii,"n"), neg_array[ii]));
	}
		
	
}
//-------------------------------------------------------------------//
//FGSII add 10/31/2014 note this is not yet tested
void evt::WirePair::MakeClone(evt::WirePair* pair){
	//create pointers to pos and neg wires
	evt::Channel* pos_wire = new evt::Channel();
	evt::Channel* neg_wire = new evt::Channel();

	//initialize fWirePair
	fWirePair = WirePairType(pos_wire,neg_wire);

	fWirePair.first->MakeClone(pair->GetWirePair().first);
	fWirePair.second->MakeClone(pair->GetWirePair().second);
}
//-------------------------------------------------------------------//
//FGSII add 10/31/2014
void evt::WirePair::SetWirePairId(unsigned int Id){
	fWirePairId = Id;
}
//-------------------------------------------------------------------//
unsigned int evt::WirePair::GetWirePairId() const{
	return fWirePairId;
}
//-------------------------------------------------------------------//



















