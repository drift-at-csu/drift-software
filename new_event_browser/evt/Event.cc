#include <evt/Event.h>
#include <evt/Channel.h>
#include <evt/WirePair.h> //FGSII add 10/20/2014
using namespace drift;

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string.h>
using namespace std;

//-------------------------------------------------------------------//
evt::Event::Event():
	fCurrentTraceId(0)
	{
}
//-------------------------------------------------------------------//
evt::Event::~Event(){
  for (ChannelIterator iter = ChannelsBegin(); iter != ChannelsEnd(); ++iter) {
	if ((*iter)!=NULL) {
		delete (*iter);
		(*iter) = NULL;
	}
  }
	fChannels.clear();//FGSII add 11/03/2014

	//FGSII add 11/03/2014
	for (WirePairIterator iter = WirePairsBegin(); iter != WirePairsEnd(); ++ iter){
		if((*iter) != NULL) {
			delete (*iter);
			(*iter) = NULL;
		}
	}
	fWirePairs.clear();
	
}
//-------------------------------------------------------------------//
evt::Header& evt::Event::GetHeader(){
  return fHeader;
}
//-------------------------------------------------------------------//
const evt::Header& evt::Event::GetHeader() const{
  return fHeader;
}
//-------------------------------------------------------------------//
void evt::Event::SetHeader(const evt::Header& header){
  fHeader = header;
}
//-------------------------------------------------------------------//
unsigned int evt::Event::GetNofChannels() const{
  return fChannels.size();
}
//-------------------------------------------------------------------//
unsigned int evt::Event::GetNofEvents(std::string file_name) const{
	cout << "...evt::Event::GetNofEvents" << endl;
	ifstream drt_file(file_name.c_str(), ios::binary);
 	if (!drt_file.is_open() ) {
	cout << "......Can't open \"" << file_name << "\"" << endl;
	return 0;
 	}
 	
  unsigned  total_nof_events = 0;
  while (drt_file.good()) {
	char tmp_char[7];
	drt_file.read(reinterpret_cast< char*> (tmp_char), 6);
	if ( strcmp(tmp_char,"DRIFTE")!=0 ) {
		cout << ".....Magic string is incorrect" << endl;
		drt_file.close();
		return 0;
	}
	if ( strcmp(tmp_char,"DRIFTE") == 0){
	total_nof_events = total_nof_events + 1;
	}
	
	unsigned short file_version = 0;
	drt_file.read(reinterpret_cast< char*> (&file_version),sizeof(unsigned short));
	// cout << "......file_version = " << file_version << endl;
	unsigned long evnt_gps_sec; 
	unsigned long evnt_gps_nsec;
	drt_file.read(reinterpret_cast< char*> (&evnt_gps_sec),sizeof(unsigned long));
	// cout << "......evnt_gps_sec = " << evnt_gps_sec << endl;
	drt_file.read(reinterpret_cast< char*> (&evnt_gps_nsec),sizeof(unsigned long));
	// cout << "......evnt_gps_nsec = " << evnt_gps_nsec << endl;
	
	unsigned long evnt_nof_trcs = 0;
	drt_file.read(reinterpret_cast< char*> (&evnt_nof_trcs),sizeof(unsigned long));
	// cout << "......evnt_nof_trcs = " << evnt_nof_trcs << endl;

	for (unsigned long ii=0; ii < evnt_nof_trcs; ++ii){
		unsigned long trace_size;
		drt_file.read(reinterpret_cast< char*> (&trace_size),sizeof(unsigned long));
		// cout << "......trace_size = " << trace_size << endl;

		// AVD: Fred, I'm going to use seekg here to go to the next trace
		drt_file.seekg(trace_size, ios_base::cur);
	}

  } // while loop
  drt_file.close();

  return total_nof_events-1;//returns 1 more event than it should,
  //so for now I subtract 1
}
//-------------------------------------------------------------------//
void evt::Event::MakeChannel(const unsigned int channelId){
  if (HasChannel(channelId)) 
	return;
   Channel *ChannelPnt = new Channel();
   ChannelPnt->SetId(channelId);
   fChannels.push_back(ChannelPnt);
}
//-------------------------------------------------------------------//
bool evt::Event::HasChannel(const unsigned int channelId){
  for (ConstChannelIterator iter = ChannelsBegin(); iter != ChannelsEnd(); ++iter) {
	if ((*iter)->GetId() == channelId) {
		return true;
	}
  }
  return false;
}
//-------------------------------------------------------------------//
evt::Channel *evt::Event::GetChannel(const unsigned int channelId){
  for (ConstChannelIterator iter = ChannelsBegin(); iter != ChannelsEnd(); ++iter) {
  	if ((*iter)->GetId() == channelId) {
		return (*iter);
	}
  }
  return NULL;
}
//-------------------------------------------------------------------//
evt::Event::ChannelIterator evt::Event::ChannelsBegin(){
  return fChannels.begin();
}
//-------------------------------------------------------------------//
evt::Event::ChannelIterator evt::Event::ChannelsEnd(){
  return fChannels.end();
}
//-------------------------------------------------------------------//
evt::Event::ConstChannelIterator evt::Event::ChannelsBegin() const{
  return fChannels.begin();
}
//-------------------------------------------------------------------//
evt::Event::ConstChannelIterator evt::Event::ChannelsEnd() const{
  return fChannels.end();
}
//-------------------------------------------------------------------//
//FGSII added 05/29/2014
void evt::Event::SetEventNumber(int EventNumber){
	fEventNumber = EventNumber;
  return;
}
//-------------------------------------------------------------------//
//FGSII added 05/29/2014
int evt::Event::GetEventNumber() const{
  return fEventNumber;
}
//-------------------------------------------------------------------//
//FGSII added 05/30/2014
void evt::Event::SetFileName(string file_name){
	fFileName = file_name;
  return;
}
//-------------------------------------------------------------------//
//FGSII added 05/30/2014
string evt::Event::GetFileName() const{
  return fFileName;
}
//-------------------------------------------------------------------//
//FGSII add 07/09/2014
void evt::Event::SetCurrentTraceId(unsigned int current_trace_id){
	fCurrentTraceId = current_trace_id;
}
//-------------------------------------------------------------------//
//FGSII add 07/09/2014
unsigned int evt::Event::GetCurrentTraceId(){
	return fCurrentTraceId;
}
//-------------------------------------------------------------------//
// new code
bool evt::Event::LoadFromDRTFile(std::string file_name,unsigned long event_number) {
	cout << "...evt::Event::LoadFromDRTFile" << endl;	
	
	ifstream drt_file(file_name.c_str(), ios::binary);
 //	ofstream out_file("/home/fred/Research/Event_Browser_2/gui11/test_IO/debug_drt_output.txt"); //FGSII for debug
	
 	if (!drt_file.is_open() ) {
		cout << "......Can't open \"" << file_name << "\"" << endl;
		return false;
 	}
	//cout << "......event_number = " << event_number << endl;//FGSII comment out 10/06/2014

	unsigned long current_event_number = 1;
	while (1) {
		char tmp_char[7];
		drt_file.read(reinterpret_cast< char*> (tmp_char), 6);
		// out_file << "tmp_char = " << tmp_char << endl;//FGSII
		if ( strcmp(tmp_char,"DRIFTE")!=0 ) {
			cout << "......Magic string is incorrect" << endl;
			drt_file.close();
			return false;
		}
		unsigned short file_version = 0;
		drt_file.read(reinterpret_cast< char*> (&file_version),sizeof(unsigned short));
		// out_file << "file_version = " << file_version << endl;//FGSII
		unsigned long evnt_gps_sec; 
		unsigned long evnt_gps_nsec;
		drt_file.read(reinterpret_cast< char*> (&evnt_gps_sec),sizeof(unsigned long));
		// out_file << "evnt_gps_sec = "<< evnt_gps_sec << endl;//FGSII
		drt_file.read(reinterpret_cast< char*> (&evnt_gps_nsec),sizeof(unsigned long));
		// out_file << "evnt_gps_nsec = "<< evnt_gps_nsec << endl;//FGSII

		//should do this later to ensure event time is the earliest time
		//update:09/18/2014 as of right now this seems to store earliest trace time
		GetHeader().GetTime().SetGPSTime(evnt_gps_sec, evnt_gps_nsec);
		
		unsigned long evnt_nof_trcs = 0;
		drt_file.read(reinterpret_cast< char*> (&evnt_nof_trcs),sizeof(unsigned long));
	
		//only print out for the event number we are interested in
		//FGSII comment out 10/06/2014
		/*if(current_event_number == event_number){
			cout << "......Time of last bin filled of first trace: " << GetHeader().GetTime().GetGPSSecond() <<
			"[" << GetHeader().GetTime().GetGPSNanoSecond() << "]" << endl;
			cout << "......There are " << evnt_nof_trcs << " traces in this event" << endl; //FGSII
		}*/
			
		//out_file << "evnt_nof_trcs = " << evnt_nof_trcs << endl;//FGSII

		// Let's clear the previous Channels if any
		fChannels.clear();

		for (unsigned long ii=0; ii < evnt_nof_trcs; ++ii){
			unsigned long trace_size;
			drt_file.read(reinterpret_cast< char*> (&trace_size),sizeof(unsigned long));
		
			//only print out for the event number we are interested in
			//FGSII comment out 10/06/2014
			/*if(current_event_number == event_number){
				cout << "......Trace " << ii+1 << " size=" << trace_size << " bytes" << endl; //FGSII
			}*/
		
		//	out_file << "trace_num = " << ii+1 << ",trace_size = " << trace_size << endl; //FGSII
			if (current_event_number != event_number) {
				// This means we just want to skip to next event
				drt_file.seekg(trace_size, ios_base::cur);
			} 
			else {
				// We're reading this event
				unsigned long trace_chan_id;
				drt_file.read(reinterpret_cast< char*> (&trace_chan_id),sizeof(unsigned long));
				//cout << "......Trace " << ii+1 << " Making Channel " << trace_chan_id << endl;//FGSII comment out 10/06/2014
			//	out_file << "trace_num = " << ii+1 << ",trace_chan_id = " << trace_chan_id << endl; //FGSII 
			
		
				if(HasChannel(trace_chan_id) == true){
					fCurrentTraceId = GetChannel(trace_chan_id)->GetNofTracesInChan();
					GetChannel(trace_chan_id)->MakeTrace(fCurrentTraceId);
				}
				else{
					MakeChannel(trace_chan_id);
					fCurrentTraceId = 0;
					GetChannel(trace_chan_id)->MakeTrace(fCurrentTraceId);
				}
			
				//FGSII added 06/04/2014
				//cout << "......GetNofChannels() = " << GetNofChannels() << endl;//FGSII comment out 10/06/2014

			
				// unsigned long trace_gps_sec;
				// unsigned long trace_gps_nsec;
				long trace_offset;
				// AVD: Currently in the file there is a trace offset
				// which is recalculated with respect to event sec/nsec
				// and it contains number of nsec
				
				// drt_file.read(reinterpret_cast< char*> (&trace_gps_sec),
				// 		sizeof(unsigned long));
				//	out_file << "evnt_gps_sec = "<< trace_gps_sec << endl;//FGSII
				// drt_file.read(reinterpret_cast< char*> (&trace_gps_nsec),
				// sizeof(unsigned long));
				//	out_file << "evnt_gps_sec = "<< trace_gps_nsec << endl;//FGSII
				//save time stamp of trace
				drt_file.read(reinterpret_cast< char*> (&trace_offset),sizeof(long));

				GetChannel(trace_chan_id)->GetTrace(fCurrentTraceId)->SetTraceTime(
					evnt_gps_sec, evnt_gps_nsec + trace_offset);

				double  trace_vert_scale;
				drt_file.read(reinterpret_cast< char*> (&trace_vert_scale),
					sizeof(double));
				//out_file << "trace_vert_scale = " << trace_vert_scale << endl;//FGSII
				double  trace_horiz_scale;
				drt_file.read(reinterpret_cast< char*> (&trace_horiz_scale),
					sizeof(double));
				//out_file << "trace_horiz_scale = " << trace_horiz_scale << endl;//FGSII
				unsigned long trace_nof_bins;
				drt_file.read(reinterpret_cast< char*> (&trace_nof_bins),
					sizeof(unsigned long));
				// out_file << "trace_nof_bins = " << trace_nof_bins << endl;//FGSII
				//save number of bins in trace (should be 512 always), 
			
				// save bin duration
				GetChannel(trace_chan_id)->GetTrace(fCurrentTraceId)->SetBinDuration(
					trace_horiz_scale/1.0E+3); 
				// Bin duration expects value in usecs.
			
				//need to calculate offset from first trace
				double trace_offset_calc = (double)trace_offset/1.0E+3;

				// double bin_duration = GetChannel(trace_chan_id)->GetTrace(
				//	fCurrentTraceId)->GetBinDuration();	
				// trace_offset_calc = CalcTraceOffset(
				//	evnt_gps_sec, evnt_gps_nsec,
				//	evnt_gps_sec, evnt_gps_nzsec + ,bin_duration);
				//
				//now that offset is calculated we save it with the trace
				GetChannel(trace_chan_id)->GetTrace(
					fCurrentTraceId)->SetTraceOffset(trace_offset_calc);
				//cout << "...returned to evt::Event::LoadFromDRTFile" << endl;//FGSII comment out 10/06/2014
				/*cout <<"......trace " << GetChannel(trace_chan_id)->GetTrace(
					fCurrentTraceId)->GetTraceId() 
					<< " trace_offset = " << GetChannel(
						trace_chan_id)->GetTrace(
						fCurrentTraceId)->GetTraceOffset() << endl;*///FGSII comment out 10/06/2014
				//cout << "......Trace " << ii+1 << " trace_offset = " 
				//	<< trace_offset << endl;//FGSII for debug
				//short trace_current_bin_value;
				uint32_t trace_current_bin_value; //FGSII was short 06/11/2014
				for (unsigned long jj=0; jj < trace_nof_bins; ++jj){
					drt_file.read (reinterpret_cast< char*> 
						(&trace_current_bin_value), 
						sizeof(uint32_t));//FGSII was short 06/11/2014
					// out_file << "bin_num = " << jj+1 
					// 	<< ",trace_current_bin_vale = " 
					// 	<< trace_current_bin_value << endl;
					// Trace Vertical Scale is supposed to be in
					// mV/ADC count.
					GetChannel(trace_chan_id)->GetTrace(
						fCurrentTraceId)->AddTimeBin(
						(double) trace_vert_scale*trace_current_bin_value);
				}
			}//else
		}//evnt_nof_traces loop
	
		//loop through channel, and then loop through each trace in that channel
		//and store the min and max time values, I will then calculate the total Event time length
		//which will give the horizontal axis range for plotting, we need to save this to the Header
		if (current_event_number == event_number) { 
			SetEventTimeLength();
	
			//FGSII add 10/20/2014, before breaking loop through each channel
			//and fill fWirePairs
			for(ConstChannelIterator iter = ChannelsBegin(); iter != ChannelsEnd(); ++iter){
				MakeWirePair(*iter);
			}

			break;
		}
		current_event_number ++;
	} // while(1)loop on the events

	drt_file.close();
	//cout << "......drt file was closed" << endl;//FGSII comment out 10/06/2014
//	out_file.close();//FGSII
	return true;
}
//-------------------------------------------------------------------//
//trace offset is already stored with file, no need to calc
/*
double evt::Event::CalcTraceOffset(unsigned long time_sec1, unsigned long time_nsec1, unsigned long time_sec2, unsigned long time_nsec2, double bin_duration){
		
  
	//cout << "--------------------------------------------------------------------------------" << endl;
	cout << "...evt::Event::CalcTraceOffset " << endl;
	cout << "......" << time_sec1 << " " << time_nsec1 << " " << time_sec2 << " " << time_nsec2 << endl;

	double trace_offset;
	double trace_offset_time;
	
	long sec_diff = time_sec2-time_sec1;
	cout << "......sec_diff = " << sec_diff << endl;
	long nsec_diff = time_nsec2-time_nsec1;
	cout << "......nsec_diff = " << nsec_diff << endl;
	trace_offset = (double)((sec_diff*1000000000+nsec_diff)/bin_duration);
	trace_offset_time = (double)(sec_diff*1000000000+nsec_diff);

	cout << "......trace_offset = " << trace_offset << " bins" << endl;
	cout << "......trace_offset = " << trace_offset_time << " ns" << endl;

	//by default I am assuming we want the offset in time bins that have units of ns, we
	//can change this by changing changing change_units below
	double change_units = 1000.0; //this will give give microseconds

	return (trace_offset_time/change_units); //returns offset in us intervals
	//return trace_offset_out;
}*/
//-------------------------------------------------------------------//
//new code
/*void evt::Event::SetSignalRange(const unsigned int channelId,
		double left_ped, double right_ped){
	cout << "...evt::Event::SetSignalRange" << endl;

	cout << "......Feature Not Yet Available" << endl;
}*/
//-------------------------------------------------------------------//
//function is to be used when plotting, to know how long of a time range to set up
//horizontal plot axis for
void evt::Event::SetEventTimeLength(){
	cout << "...evt::Event::SetEventTimeLength " << endl;

	long min_time_sec;
	long temp_min_time_sec;
	long min_time_nsec;
	long temp_min_time_nsec;
	long max_time_sec=0;
	long temp_max_time_sec;
	long max_time_nsec=0;	
	long temp_max_time_nsec;

	unsigned int min_chan_id;
	unsigned int min_trace_id;
	unsigned int max_chan_id;
	unsigned int max_trace_id;

	//initialize each min value with times from first trace of first channel in event
	//max values are already initialized to zero, and temp values don't need initialized
	min_time_sec = fChannels.front()->GetTrace(0)->GetTraceTimeSec();
	//cout << "......initial min_time_sec = " << min_time_sec << endl;//FGSII comment out 10/06/2014
	min_time_nsec = fChannels.front()->GetTrace(0)->GetTraceTimeNsec();
	//cout << "......initial min_time_nsec = " << min_time_nsec << endl;//FGSII comment out 10/06/2014
	//need to initialize the channel and trace ids
	min_chan_id = fChannels.front()->GetId();
	min_trace_id = fChannels.front()->GetTrace(0)->GetTraceId();
	max_chan_id = fChannels.front()->GetId();
	max_trace_id = fChannels.front()->GetTrace(0)->GetTraceId();
	

	//first we find the minimum trace time in lookings at seconds
	for (ConstChannelIterator iter1 = ChannelsBegin(); iter1 != ChannelsEnd(); ++iter1){
		for(evt::Channel::ConstTraceIterator iter2 = GetChannel((*iter1)->GetId())->TracesBegin(); iter2 != GetChannel((*iter1)->GetId())->TracesEnd(); ++iter2){
			temp_min_time_sec = (*iter2)->GetTraceTimeSec();
			if(temp_min_time_sec < min_time_sec){
				min_time_sec = temp_min_time_sec;
			}
		}
	}
	//cout << "......min_time_sec = " << min_time_sec << endl;//FGSII comment out 10/06/2014

	//next we find the minimum trace time time in ns, by comparing all times which have the lowest value
	//in seconds
	for (ConstChannelIterator iter1 = ChannelsBegin(); iter1 != ChannelsEnd(); ++iter1){
		for(evt::Channel::ConstTraceIterator iter2 = GetChannel((*iter1)->GetId())->TracesBegin(); iter2 != GetChannel((*iter1)->GetId())->TracesEnd(); ++iter2){
			temp_min_time_sec = (*iter2)->GetTraceTimeSec();
			if(temp_min_time_sec == min_time_sec){
				temp_min_time_nsec = (*iter2)->GetTraceTimeNsec();
				if(temp_min_time_nsec < min_time_nsec){
					min_time_nsec = temp_min_time_nsec;
					//set ids of chan and trace where min occurs
					min_chan_id = (*iter1)->GetId();
					min_trace_id = (*iter2)->GetTraceId();
				}
			}
		}
	}
	//cout << "......min_chan_id = " << min_chan_id << endl;//FGSII comment out 10/06/2014
	//cout << "......min_trace_id = " << min_trace_id << endl;//FGSII comment out 10/06/2014
	//cout << "......min_time_nsec = " << min_time_nsec << endl;//FGSII comment out 10/06/2014

	//Now we find the maximum trace time in lookings at seconds
	for (ConstChannelIterator iter1 = ChannelsBegin(); iter1 != ChannelsEnd(); ++iter1){
		for(evt::Channel::ConstTraceIterator iter2 = GetChannel((*iter1)->GetId())->TracesBegin(); iter2 != GetChannel((*iter1)->GetId())->TracesEnd(); ++iter2){
			temp_max_time_sec = (*iter2)->GetTraceTimeSec();
			if(temp_max_time_sec > max_time_sec){
				max_time_sec = temp_max_time_sec;
			}
		}
	}
	//cout << "......max_time_sec = " << max_time_sec << endl;//FGSII comment out 10/06/2014

	//next we find the maximum trace time time in ns, by comparing all times which have the highest value
	//in seconds
	for (ConstChannelIterator iter1 = ChannelsBegin(); iter1 != ChannelsEnd(); ++iter1){
		for(evt::Channel::ConstTraceIterator iter2 = GetChannel((*iter1)->GetId())->TracesBegin(); iter2 != GetChannel((*iter1)->GetId())->TracesEnd(); ++iter2){
			temp_max_time_sec = (*iter2)->GetTraceTimeSec();
			if(temp_max_time_sec == max_time_sec){
				temp_max_time_nsec = (*iter2)->GetTraceTimeNsec();
				if(temp_max_time_nsec > max_time_nsec){
					max_time_nsec = temp_max_time_nsec;
					//set ids of chan and trace where max occurs
					max_chan_id = (*iter1)->GetId();
					max_trace_id = (*iter2)->GetTraceId();
				}
			}
		}
	}
	//cout << "......max_chan_id = " << max_chan_id << endl;//FGSII comment out 10/06/2014
	//cout << "......max_trace_id = " << max_trace_id << endl;//FGSII comment out 10/06/2014
	//cout << "......max_time_nsec = " << max_time_nsec << endl;//FGSII comment out 10/06/2014

	double change_units = 1000.0;
	//now we calculate how long the event is in time, right now time length will be in ns
	//but I will output it in us
	//also note, this calculation assumes the time stamp of a trace belongs to its last time bin filled
	double time_diff;
	time_diff = (max_time_sec - min_time_sec)*1000000000 + (max_time_nsec - min_time_nsec);
	//cout << "......time_diff = " << time_diff << endl;//FGSII comment out 10/06/2014
	//need to find out the length of the buffers and bin duration
	unsigned int min_trace_nof_bins = GetChannel(min_chan_id)->GetTrace(min_trace_id)->GetNofTimeBins(); 
	//cout << "......min_trace_nof_bins = " << min_trace_nof_bins << endl;//FGSII comment out 10/06/2014
	//unsigned int max_trace_nof_bins = GetChannel(max_chan_id)->GetTrace(max_trace_id)->GetNofTimeBins();
	double min_trace_bin_dur = GetChannel(min_chan_id)->GetTrace(min_trace_id)->GetBinDuration();
	//cout << "......min_trace_bin_dur = " << min_trace_bin_dur << endl;//FGSII comment out 10/06/2014
	//double max_trace_bin_dur = GetChannel(max_chan_id)->GetTrace(max_trace_id)->GetBinDuration();
	double min_trace_length = min_trace_nof_bins*min_trace_bin_dur;
	//cout << "......min_trace_length = " << min_trace_length << endl;//FGSII comment out 10/06/2014
	double event_length = min_trace_length + time_diff/change_units;
	//cout << "......event_length = " << event_length << endl;//FGSII comment out 10/06/2014

	//double change_units = 1000.0; //convert to us
	GetHeader().SetEventTimeLength(event_length);
	//cout << "......Saved event length = " << GetHeader().GetEventTimeLength() << " us " << endl;//FGSII comment out 10/06/2014

}
//-------------------------------------------------------------------//
double evt::Event::GetEventTimeLength(){
	return GetHeader().GetEventTimeLength();
}
//-------------------------------------------------------------------//
//FGSII add 10/06/2014
bool evt::Event::LoadDRTFileForSort(std::string file_name,unsigned long event_number) {
	
	ifstream drt_file(file_name.c_str(), ios::binary);
	
 	if (!drt_file.is_open() ) {
		cout << "......Can't open \"" << file_name << "\"" << endl;
		return false;
 	}

	unsigned long current_event_number = 1;
	while (1) {
		char tmp_char[7];
		drt_file.read(reinterpret_cast< char*> (tmp_char), 6);
		if ( strcmp(tmp_char,"DRIFTE")!=0 ) {
			cout << "......Magic string is incorrect" << endl;
			drt_file.close();
			return false;
		}
		unsigned short file_version = 0;
		drt_file.read(reinterpret_cast< char*> (&file_version),sizeof(unsigned short));
		unsigned long evnt_gps_sec; 
		unsigned long evnt_gps_nsec;
		drt_file.read(reinterpret_cast< char*> (&evnt_gps_sec),sizeof(unsigned long));
		drt_file.read(reinterpret_cast< char*> (&evnt_gps_nsec),sizeof(unsigned long));

		GetHeader().GetTime().SetGPSTime(evnt_gps_sec, evnt_gps_nsec);
		
		unsigned long evnt_nof_trcs = 0;
		drt_file.read(reinterpret_cast< char*> (&evnt_nof_trcs),sizeof(unsigned long));

		for (unsigned long ii=0; ii < evnt_nof_trcs; ++ii){
			unsigned long trace_size;
			drt_file.read(reinterpret_cast< char*> (&trace_size),sizeof(unsigned long));
			drt_file.seekg(trace_size, ios_base::cur);
		}

		if (current_event_number == event_number) { 
			//This means that we read out the right event
			break;
		}
		current_event_number ++;
	} // while(1)loop on the events

	drt_file.close();

	return true;
}
//-------------------------------------------------------------------//
//FGSII add 10/20/2014
unsigned int evt::Event::GetNofWirePairs() const{
	return fWirePairs.size();
}
//-------------------------------------------------------------------//
//FGSII add 10/20/2014
void evt::Event::MakeWirePair(evt::Channel* chan){
	cout << "...evt::Event::MakeWirePair(evt::Channel*)" << endl;
	WirePair* WirePairPnt = new WirePair();
	WirePairPnt->MakeWirePair(chan);
   fWirePairs.push_back(WirePairPnt);
}
//-------------------------------------------------------------------//
//FGSII add 10/31/2014 use this when cloning and Event
void evt::Event::MakeWirePair(const unsigned int channelId){
	cout << "...evt::Event::MakeWirePair(const unsigned int)" << endl;
	WirePair* WirePairPnt = new WirePair();
	WirePairPnt->SetWirePairId(channelId);
	fWirePairs.push_back(WirePairPnt);
}
//-------------------------------------------------------------------//
//FGSII add 10/20/2014
bool evt::Event::HasWirePair(const unsigned int channelId){
  for (ConstWirePairIterator iter = WirePairsBegin(); iter != WirePairsEnd(); ++iter) {
	if ( ((*iter)->GetWirePair().first->GetId() == channelId) && 
		((*iter)->GetWirePair().second->GetId() == channelId) ) {
		return true;
	}
  }
  return false;
}
//-------------------------------------------------------------------//
//FGSII add 10/20/2014
evt::WirePair* evt::Event::GetWirePair(const unsigned int channelId){
	for (ConstWirePairIterator iter = WirePairsBegin(); iter != WirePairsEnd(); ++iter) {
	/*if ( ((*iter)->GetWirePair().first->GetId() == channelId) && 
		((*iter)->GetWirePair().second->GetId() == channelId) ) {*/ //FGSII change 10/31/2014
		if( (*iter)->GetWirePairId() == channelId ){
			return (*iter);
		}
	}
	return NULL;
}
//-------------------------------------------------------------------//
//FGSII add 10/20/2014
evt::Event::WirePairIterator evt::Event::WirePairsBegin(){
	return fWirePairs.begin();
}
//-------------------------------------------------------------------//
//FGSII add 10/20/2014
evt::Event::WirePairIterator evt::Event::WirePairsEnd(){
	return fWirePairs.end();
}
//-------------------------------------------------------------------//
//FGSII add 10/20/2014
evt::Event::ConstWirePairIterator evt::Event::WirePairsBegin() const{
	return fWirePairs.begin();
}
//-------------------------------------------------------------------//
//FGSII add 10/20/2014
evt::Event::ConstWirePairIterator evt::Event::WirePairsEnd() const{
	return fWirePairs.end();
}
//-------------------------------------------------------------------//
//FGSII add 10/31/2014 be able to clone Event (note not yet tested)
void evt::Event::MakeClone(Event* event){
	cout << "...evt::Event::MakeClone()" << endl;

	fHeader.MakeClone(event->GetHeader());
	cout << "......debug1" << endl;
	fEventNumber = event->GetEventNumber();
	cout << "......debug2" << endl;
	fFileName = event->GetFileName();
	cout << "......debug3" << endl;
	
	//clone channels and hence traces
	for(evt::Event::ConstChannelIterator iter = event->ChannelsBegin();
		iter != event->ChannelsEnd(); ++iter){
		MakeChannel((*iter)->GetId());
		GetChannel((*iter)->GetId())->MakeClone(*iter);
	}
	cout << "......debug4" << endl;

	//clone WirePairs
	for(evt::Event::ConstWirePairIterator iter = event->WirePairsBegin();
		iter != event->WirePairsEnd(); ++iter){
		//MakeWirePair((*iter)->GetWirePair().first->GetId());//could also use second
		//GetWirePair((*iter)->GetWirePair().first->GetId())->MakeClone(*iter);//could also use second
		MakeWirePair((*iter)->GetWirePairId());
		GetWirePair((*iter)->GetWirePairId())->MakeClone(*iter);
	}
	cout << "......debug5" << endl;
}
//-------------------------------------------------------------------//
//FGSII add 11/03/2014
void evt::Event::Clear(){

	//Clear Channels
  for (ChannelIterator iter = ChannelsBegin(); iter != ChannelsEnd(); ++iter) {
	if ((*iter)!=NULL) {
		delete (*iter);
		(*iter) = NULL;
	}
  }
	fChannels.clear();

	//Clear WirePairs
	for (WirePairIterator iter = WirePairsBegin(); iter != WirePairsEnd(); ++ iter){
		if((*iter) != NULL) {
			delete (*iter);
			(*iter) = NULL;
		}
	}
	fWirePairs.clear();

	//I don't think I need to clear the other fields, they simply can be overwritten

}
//-------------------------------------------------------------------//








