#ifndef _DRIFT_evt_Wire_h_
#define _DRIFT_evt_Wire_h_

#include <evt/Channel.h>
#include <map>

namespace drift {
namespace evt {
	class Event;
	class Channel;

//class Wire: public Channel {
class WirePair {

	//-------------------public-----------------------------------//
	public:
		
	//typedefs
	//typedef std::pair < std::pair<unsigned int ChId, std::string sign>, unsigned int WireId > MapType
	//typedef std::pair <unsigned int ChId, std::string sign> WireMapKeyType
	//typedef std::map <WireMapKeyType,unsigned int WireId> WireMap
	typedef std::pair <unsigned int, std::string> WireMapKeyType;
	typedef std::map <WireMapKeyType, unsigned int> WireMap;
	typedef std::pair <evt::Channel*, evt::Channel*> WirePairType;

	//constructor/destructor
	//the idea is to take a channel, and create two new channels stored in fWirePair
	//WirePair(evt::Channel* chan);
	WirePair();
	~WirePair();
	
	//FGSII add 10/31/2014
	void SetWirePairId(unsigned int Id);
	unsigned int GetWirePairId() const;

	//the wire pair
	void MakeWirePair(evt::Channel* chan);//set fWirePair
	WirePairType GetWirePair(); //was WirePairType*
	unsigned int GetPosWireId();
	unsigned int GetNegWireId();
	void InvertNegWire();

	//the wire map
	void SetWireMap();//sets fWireMap
	unsigned int GetWireMap(WireMapKeyType key);

	//FGSII add 10/31/2014
	//be able to clone WirePair
	void MakeClone(WirePair* pair);
	

	//------------------------------------------------------------//

	//-------------------protected--------------------------------//
	private:
	WirePairType fWirePair; //was WirePairType*
	WireMap fWireMap;
	unsigned int fWirePairId;//FGSII add 10/31/2014 same as channelId that made wires
	//------------------------------------------------------------//


}; /* class WirePair */
} /* namespace evt */
} /* namespace drift */

#endif /* _DRIFT_evt_Wire_h_ */
