#ifndef _DRIFT_evt_Event_h_
#define _DRIFT_evt_Event_h_

#include <list>
#include <string>

#include <evt/Header.h>
#include <evt/Channel.h> //fmod include 07/10/2014
#include <evt/WirePair.h> //fmod add 10/20/2014

namespace drift {
namespace evt {
	class Channel;
	class WirePair; //FGSII add 10/20/2014	

class Event {
  public:
  	typedef std::list<Channel*> ChannelContainer;
	typedef ChannelContainer::iterator ChannelIterator;
	typedef ChannelContainer::const_iterator ConstChannelIterator;

	//FGSII add 10/20/2014
	typedef std::list<WirePair*> WirePairContainer;
	typedef WirePairContainer::iterator WirePairIterator;
	typedef WirePairContainer::const_iterator ConstWirePairIterator;

  public:
  	Event();
	~Event();

	//FGSII add 11/03/2014
	void Clear();

	/// Loading a given event from file
//	bool LoadFromDRTFile(std::string file_name, unsigned long event_number = 1);//FGSII comment out 09/22/2014
	bool LoadFromDRTFile(std::string file_name, unsigned long event_number);//FGSII add 09/22/2014
	bool LoadDRTFileForSort(std::string file_name, unsigned long event_number);//FGSII add 10/06/2014

	/// Sets the signal range for a channel channelId. Left_ped
	/// is the time (usecs) from the begining of the trace. right_ped
	/// is the time from the begining of the trace where right
	/// pedestal begins
	//void SetSignalRange(const unsigned int channelId, double left_ped, double right_ped);

	drift::evt::Header& GetHeader();
	const drift::evt::Header& GetHeader() const;
	void SetHeader(const Header& header);
	
	unsigned int GetNofEvents(std::string file_name) const;//fmod add this to get nof events in file

	void SetEventNumber(int EventNumber);//fmod added 05/29/2014
	int GetEventNumber() const;//fmod added 05/29/2014
	
	void SetFileName(std::string file_name);//fmod added 05/30/2014
	std::string GetFileName() const;//fmod added 05/30/2014

	//fmod add 07/09/2014
	void SetCurrentTraceId(unsigned int current_trace_id);
	unsigned int GetCurrentTraceId();

	//fmod add 07/10/2014
	void SetEventTimeLength();
	double GetEventTimeLength();
	
	//trace offset is already stored with file, don't need to calc
	//	double CalcTraceOffset(unsigned long time_sec1, unsigned long time_nsec1, unsigned long time_sec2, unsigned long time_nsec2, double bin_duration);
	
	unsigned int GetNofChannels() const;
	void MakeChannel(const unsigned int channelId);
	bool HasChannel(const unsigned int channelId);
	Channel *GetChannel(const unsigned int channelId);
	ChannelIterator ChannelsBegin();
	ChannelIterator ChannelsEnd();
	ConstChannelIterator ChannelsBegin() const;
	ConstChannelIterator ChannelsEnd() const;

	//FGSII add 10/20/2014
	unsigned int GetNofWirePairs() const;
	void MakeWirePair(evt::Channel* chan);
	void MakeWirePair(const unsigned int channelId);//FGSII add 10/31/2014
	bool HasWirePair(const unsigned int channelId);
	WirePair* GetWirePair(const unsigned int channelId);
	WirePairIterator WirePairsBegin();
	WirePairIterator WirePairsEnd();
	ConstWirePairIterator WirePairsBegin() const;
	ConstWirePairIterator WirePairsEnd() const;

	//FGSII add 10/31/2014
	//be able to clone Event
	void MakeClone(Event* event);

  private:
	Header fHeader;
	ChannelContainer fChannels;
	int fEventNumber;//fmod added, stores current event number 05/29/2014
	std::string fFileName;//fmod added 05/30/2014
	unsigned int fCurrentTraceId;
	WirePairContainer fWirePairs;//FGSII add 10/20/2014
}; /* class Event */
} /* namespace evt */
} /* namespace drift */
#endif /* _DRIFT_evt_Event_h_ */
