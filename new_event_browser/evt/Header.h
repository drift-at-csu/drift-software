#ifndef _DRIFT_evt_Header_h_
#define _DRIFT_evt_Header_h_

#include <utl/TimeStamp.h>

namespace drift {
namespace evt {

class Header {
  public:
  	Header();
	~Header();

  	/// Get the event identifier
	const std::string& GetId() const;
	/// Set the Id of the event
	void SetId(const std::string& id);

	/// Get the time stamp of the event
	const utl::TimeStamp& GetTime() const;
	utl::TimeStamp &GetTime();
	void SetTime(const utl::TimeStamp& t);

	//FGSII add 07/10/2014
	void SetEventTimeLength(double time_length);
	double GetEventTimeLength();

	//FGSII add 10/31/2014
	void MakeClone(Header& head);
	
  private:
	utl::TimeStamp fTime;
	std::string fId;
	double fEventTimeLength;//will be set by Event.cc
	

}; // class Header
} // namespace evt
} // namespace drift
#endif /* _das_drift_evt_Header_h_ */
