#ifndef IOPARPORT_H
#define IOPARPORT_H

#include "iobase.h"

class IOParport : public IOBase {
  protected:
	int fd, total, cabletype, debug;
	unsigned char def_byte, tdi_value, tms_value, tck_value, tdo_mask, tdo_inv;
	int write_data(int fd, unsigned char data);
	int write_control(int fd, unsigned char control);
	int read_control(int fd, unsigned char *control);
	int read_status(int fd, unsigned char *status);
  public:
	IOParport();
	int Init(struct cable_t *cable, const char *dev, unsigned int freq);
	~IOParport();
	void tx(bool tms, bool tdi);
	bool txrx(bool tms, bool tdi);
	void tx_tdi_byte(unsigned char tdi_byte);
	void tx_tms(unsigned char *pat, int length, int force);
  public:
	void txrx_block(const unsigned char *tdi, unsigned char *tdo, int length, bool last);

  private:
	void delay(int del);
	int detectcable(void);
};
#endif // IOPARPORT_H
