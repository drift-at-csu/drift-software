#ifndef IOBASE_H
#define IOBASE_H

#include <stdint.h>

#define BLOCK_SIZE 65536
#define CHUNK_SIZE 128
#define TICK_COUNT 2048
#include "cabledb.h"

class IOBase {

 protected:
  uint32_t	      verbose;
  unsigned char ones[CHUNK_SIZE], zeros[CHUNK_SIZE];
  unsigned char tms_buf[CHUNK_SIZE];
  unsigned int tms_len; /* in Bits*/

 protected:
  IOBase();
 public:
  virtual ~IOBase() {}

 public:
  virtual int Init(struct cable_t *cable, const char *devopt, unsigned int freq);
  virtual void flush() {}
  virtual void Usleep(unsigned int usec);

 public:
  void setVerbose(uint32_t v) { verbose = v; }
  void shiftTDITDO(const unsigned char *tdi, unsigned char *tdo, int length, bool last=true);
  void shiftTDI(const unsigned char *tdi, int length, bool last=true);
  void shiftTDO(unsigned char *tdo, int length, bool last=true);
  void shift(bool tdi, int length, bool last=true);
  void set_tms(bool value);
  void flush_tms(int force);

 protected:
  virtual void txrx_block(const unsigned char *tdi, unsigned char *tdo, int length, bool last)=0;
  virtual void tx_tms(unsigned char *pat, int length, int force)=0;
  virtual void settype(int subtype) {}

private:
  void nextTapState(bool tms);
};
#endif // IOBASE_H
