#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <iostream>
#include <string.h>
#include <sstream>
#include <cstdio>
#include <list>
#include <memory>
#include <errno.h>
#include <assert.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <signal.h>
#include <fstream>

#include "drift_jtag_bridge.h"

#include "devicedb.h"
#include "cabledb.h"
#include "ioparport.h"
#include "debug.h"

#include "utilities.h"

using namespace std;
//-------------------------------------------------------------------//
int init_chain(Jtag &jtag, DeviceDB &db) {
  cerr << "...Inside init_chain" << endl;
  int num = jtag.getChain();
  if (num == 0) {
	fprintf(stderr,"No JTAG Chain found\n");
	return 0;
  } else {
  	cerr << "...Found " << num << " devices in chain" << endl;
  }
  // Synchronise database with chain of devices.
  for (int i=0; i<num; i++){
	unsigned long id = jtag.getDeviceID(i);
	int length=db.loadDevice(id);
	if (length>0)
		jtag.setDeviceIRLength(i,length);
	else {
		fprintf(stderr,"Cannot find device having IDCODE=%07lx Revision %c\n",
			id & 0x0fffffff,  (int)(id  >>28) + 'A');
		return 0;
	}
  }
  return num;
}
//-------------------------------------------------------------------//
static int last_pos = -1;

unsigned long get_id(Jtag &jtag, DeviceDB &db, int chainpos) {
  cerr << "...Inside get_id" << endl;
  bool verbose = jtag.getVerbose();
  int num = jtag.getChain();
  cerr << "...Device chain position = " << chainpos << endl;
  if (jtag.selectDevice(chainpos)<0) {
	cerr <<  "Invalid chain position " << chainpos << ", must be >= 0 and < "
		<< num << endl;
	return 0;
  }
  const char *dd=db.getDeviceDescription(chainpos);
  unsigned long id = jtag.getDeviceID(chainpos);
  if (verbose && (last_pos != chainpos)) {
  	cerr <<  "JTAG chainpos: " << chainpos << " Device IDCODE = 0x" << (hex) << id 
		<< " Desc: " << dd << endl;
	last_pos = chainpos;
  }
  return id;
}
//-------------------------------------------------------------------//
void printHex(uint32_t value, int width) {
	std::string hexStr;
	std::stringstream num;
	num.width(width);
	num.fill('0');
	num << std::fixed << std::hex << (int) value;
	hexStr = /* "0x"  + */ num.str();
	cerr << hexStr << flush;
}
//-------------------------------------------------------------------//
void cpu_write_bus( Jtag &jtag,  uint32_t addr,
		uint32_t *data, unsigned long length ){
  uint8_t din[9];
  uint8_t dout[9];

  // 7bit: read(0) or write (1) operation                               0x80
  // 6bit: Bus Operation indicator ( if 1 then reading or writing )     0x40
  // 5bit: Stall indicator                                              0x20
  // 4bit: Ack (ack_i) (BUS Mode) or BPI (Status Mode)                  0x10
  din[8] = 0xC0; // Header
  for (unsigned long ii = 0; ii < length; ++ii) {
	din[4]=((addr+ii)      ) & 0xFF; // Address: LSByte
	din[5]=((addr+ii) >> 8 ) & 0xFF;
	din[6]=((addr+ii) >> 16) & 0xFF;
	din[7]=((addr+ii) >> 24) & 0xFF; // Address: MSByte

	din[0]=((data[ii])      ) & 0xFF; // Data: LSByte
	din[1]=((data[ii]) >> 8 ) & 0xFF;
	din[2]=((data[ii]) >> 16) & 0xFF;
	din[3]=((data[ii]) >> 24) & 0xFF; // Data: MSByte

	jtag.shiftDR(din, dout, 72);
  }
}
//-------------------------------------------------------------------//
void cpu_read_bus( Jtag &jtag, uint32_t addr,
                uint32_t *data, unsigned long length){

  uint8_t din[9];
  uint8_t dout[9];

  din[4]=((addr)      ) & 0xFF; // Address: LSByte
  din[5]=((addr) >> 8 ) & 0xFF;
  din[6]=((addr) >> 16) & 0xFF;
  din[7]=((addr) >> 24) & 0xFF; // Address: MSByte

  din[0]= 0xFF; // Data: LSByte
  din[1]= 0xFF;
  din[2]= 0xFF;
  din[3]= 0xFF; // Data: MSByte

  // 7bit: read(0) or write (1) operation				0x80
  // 6bit: Bus Operation indicator ( if 1 then reading or writing )	0x40
  // 5bit: Stall indicator						0x20
  // 4bit: Ack (ack_i) (BUS Mode) or BPI (Status Mode)			0x10

  din[8] = 0x40; // Read
  // First Read, to get in 0th word of data
  jtag.shiftDR(din, dout, 72);

  for (unsigned long ii = 0; ii < length; ++ii) {
	din[4]=((addr+(ii+1))      ) & 0xFF; // Address: LSByte
	din[5]=((addr+(ii+1)) >> 8 ) & 0xFF;
	din[6]=((addr+(ii+1)) >> 16) & 0xFF;
	din[7]=((addr+(ii+1)) >> 24) & 0xFF; // Address: MSByte

	jtag.shiftDR(din, dout, 72);
	data[ii]  = dout[0]; // Data: LSByte
	data[ii] |= dout[1] << 8;
	data[ii] |= dout[2] << 16;
	data[ii] |= dout[3] << 24;
  }
}
//-------------------------------------------------------------------//
void cpu_stall( Jtag &jtag) {
  uint8_t din[9];
  uint8_t dout[9];

  // Addr:
  din[4] = 0x11;
  din[5] = 0x22;
  din[6] = 0x44;
  din[7] = 0x88;

  // Data:
  din[0] = 0x11;
  din[1] = 0x22;
  din[2] = 0x44;
  din[3] = 0x88;
  
  // Header ( 8 bits )
  // 7bit: read(0) or write (1) operation 				=> 0
  // 6bit: Bus Operation indicator ( if 1 then reading or writing )	=> 0
  // 5bit: Stall indicator						=> 1
  // 4bit: Ack (ack_i) (BUS Mode) or BPI (Status Mode)			=> 0
  // 3bit: 
  // 2-0bit: Reserved

  din[8] = 0x20;

  jtag.shiftDR(din, dout, 72);
}
//-------------------------------------------------------------------//
void cpu_unstall( Jtag &jtag) {
  uint8_t din[9];
  uint8_t dout[9];

  // Addr:
  din[4] = 0x11;
  din[5] = 0x22;
  din[6] = 0x44;
  din[7] = 0x88;
  // Data:
  din[0] = 0x11;
  din[1] = 0x22;
  din[2] = 0x44;
  din[3] = 0x88;

  din[8] = 0x00;

  jtag.shiftDR(din, dout, 72);
}
//-------------------------------------------------------------------//
// length is expressed in words (4 bytes)
void wbm_write_bus( Jtag &jtag,  uint32_t addr, 
		uint32_t *data, unsigned long length, uint8_t mask){
  uint8_t din[9];
  uint8_t dout[9];

  din[8] = 0x80 | (mask & 0x0F); // Write
  for (unsigned long ii = 0; ii < length; ++ii) {
  	din[4]=((addr+ii*4)      ) & 0xFF; // Address: LSByte
	din[5]=((addr+ii*4) >> 8 ) & 0xFF;
	din[6]=((addr+ii*4) >> 16) & 0xFF;
	din[7]=((addr+ii*4) >> 24) & 0xFF; // Address: MSByte
	
	din[0]=((data[ii])      ) & 0xFF; // Data: LSByte
	din[1]=((data[ii]) >> 8 ) & 0xFF;
	din[2]=((data[ii]) >> 16) & 0xFF;
	din[3]=((data[ii]) >> 24) & 0xFF; // Data: MSByte

        jtag.shiftDR(din, dout, 72);
  }
}
//-------------------------------------------------------------------//
void wbm_read_bus( Jtag &jtag, uint32_t addr, 
		uint32_t *data, unsigned long length, uint8_t mask){
  uint8_t din[9];
  uint8_t dout[9];

  din[4]=((addr)      ) & 0xFF; // Address: LSByte
  din[5]=((addr) >> 8 ) & 0xFF;
  din[6]=((addr) >> 16) & 0xFF;
  din[7]=((addr) >> 24) & 0xFF; // Address: MSByte

  din[0]= 0xFF; // Data: LSByte
  din[1]= 0xFF;
  din[2]= 0xFF;
  din[3]= 0xFF; // Data: MSByte

  din[8] = 0x70 | (mask & 0x0F);; // Read

  // First Read, to get in 0th word of data
  jtag.shiftDR(din, dout, 72);

  for (unsigned long ii = 0; ii < length; ++ii) {
	din[4]=((addr+(ii+1)*4)      ) & 0xFF; // Address: LSByte
	din[5]=((addr+(ii+1)*4) >> 8 ) & 0xFF;
	din[6]=((addr+(ii+1)*4) >> 16) & 0xFF;
	din[7]=((addr+(ii+1)*4) >> 24) & 0xFF; // Address: MSByte

	jtag.shiftDR(din, dout, 72);

	data[ii]  = dout[0]; // Data: LSByte
	data[ii] |= dout[1] << 8;
	data[ii] |= dout[2] << 16;
	data[ii] |= dout[3] << 24;
  }

}
//-------------------------------------------------------------------//
int main (int argc, char **argv) {
  int c;
  int number_of_files = 0;
  uint32_t start_addr = 0x100;

  while ((c = getopt(argc, argv, "S:")) != EOF) {
    switch (c) {
	case 'S':
		start_addr = atol(optarg);
		continue;
    };
  }

  number_of_files = argc - optind;
  if (number_of_files > 1) {
  	cerr << "==> Error: Provide a binary file to load" << endl;
	exit(0);
  }

  cerr << "...Start Address = 0x"; printHex(start_addr, 8); cerr << endl;
  cerr << "...Number of BIN files provided " << number_of_files << endl;


  int res;
  struct cable_t cable;
  CableDB cabledb(NULL);
  res = cabledb.getCable("pp", &cable);
  if(res) {
	cerr << "Can't find description for a cable named " << endl;
	cerr << "   Known Cables: " << endl;
	cabledb.dumpCables(stderr);
	exit ( 1 );
  } else {
  	cerr << "...Found PP Cable" << endl;
  }

  std::auto_ptr<IOBase> io;
  res = getIO( &io, &cable, 
  	NULL, // dev
	NULL, // serial
	true, // verbose
	false, // use_ftd2xx
	0 ); // jtag_freq

  if (res) {
	cerr << "...Can't get IO" << endl;
	exit (1);
  } else {
  	// cerr << "...Got IO" << endl;
  }
  // cerr << "...IO Pntr = " << io.get() << endl;
  
  Jtag jtag = Jtag(io.get());

  // cerr << "...Jtag Created" << endl;
  jtag.setVerbose(0);
  io.get()->setVerbose(0);
  // HW_FUNCTIONS|HW_DETAILS
  // PP_FUNCTIONS PP_DETAILS UP_FUNCTIONS UP_DETAILS CP_FUNCTIONS CP_DETAILS
  // UL_FUNCTIONS UL_DETAILS

  unsigned long id;
  DeviceDB db(NULL);
  // cerr << "...DB Created" << endl;
  if (init_chain(jtag, db))
	id = get_id (jtag, db, 0);
  else
	id = 0;

  if (id != 0x364c093) {
	cerr << "... The id " << id << " is not supported" << endl;
	exit ( 1 );
  } else {
  	// cerr << "...Found Right Device (Kintex 7) " << endl;
  }
//----------> After this line we can talk to the Data registers ----->
  uint8_t ir_in[256];
  uint8_t ir_out[256];

  uint32_t dwrite[256];
  uint32_t dread [256];

  jtag.setTapState(Jtag::TEST_LOGIC_RESET);
  jtag.selectDevice(0);

  //--------> Stall the CPU first -------//
  if (number_of_files != 0) {
  	ir_in[0] = 0x22;
  	jtag.shiftIR(ir_in, ir_out);
  	cerr << "===Shifted USER3 (CPU) IN=0x" << (hex) << (int) ir_in[0]
  		<<" OUT=0x" << (int) ir_out[0] << (dec) << (flush) << endl;

  	cerr << "...Stall CPU0" << endl;
  	cpu_stall(jtag);
  }

  //------------> Loading the input file ---------------//
  if (number_of_files != 0) {
  	ir_in[0] = 0x03;
	jtag.shiftIR(ir_in, ir_out);
	cerr << "...Shifted USER2 (Wishbone) IN=0x" << (hex) << (int) ir_in[0]
  		<<" OUT=0x" << (int) ir_out[0] << (dec) << (flush) << endl;

	stringstream binary_filename_string;
	binary_filename_string << argv[optind];
	ifstream binary_file(binary_filename_string.str().c_str(), ios::binary);

	uint32_t current_addr = 0x0;
	uint32_t write_start_addr = 0x0;
	uint32_t tmp_data = 0x0;
	while ( binary_file.good() ) {
		for (int ii = 0; ii < 256; ++ii) {
			binary_file.read(reinterpret_cast< char*> (&(tmp_data)),
				sizeof(uint32_t));
			// Let's switch the bytes order
			dwrite[ii] =  ((tmp_data & 0xFF000000) >> 24);
			dwrite[ii] |= ((tmp_data & 0x00FF0000) >> 16) << 8;
			dwrite[ii] |= ((tmp_data & 0x0000FF00) >> 8) << 16;
			dwrite[ii] |= ((tmp_data & 0x000000FF) ) << 24;
			current_addr += 4;
		}
		// Let's Print Fhat we are writing
		for (int ii = 0; ii < 64; ++ii) {
			cerr << "...Write to Addr=0x"; 
			printHex(write_start_addr+(ii*4*4), 8);
			cerr << " | "; printHex(dwrite[ii*4+0], 8);
			cerr << " "; printHex(dwrite[ii*4+1], 8);
			cerr << " "; printHex(dwrite[ii*4+2], 8);
			cerr << " "; printHex(dwrite[ii*4+3], 8);
			cerr << endl;
		}
		wbm_write_bus(jtag, write_start_addr, dwrite, 256, 0xF);
		write_start_addr = current_addr;
  	}
	// Let's Check the written memory
	cerr << "==> Checking the memory" << endl;
	current_addr = 0x0;
	write_start_addr = current_addr;
	binary_file.clear();
	binary_file.seekg (0, binary_file.beg);
	while ( binary_file.good() ) {
		wbm_read_bus (jtag, write_start_addr , dread, 256, 0xF);
		for (int ii = 0; ii < 256; ++ii) {
			binary_file.read(reinterpret_cast< char*> (&(tmp_data)),
				sizeof(uint32_t));
			// Let's switch the bytes order
			dwrite[ii] =  ((tmp_data & 0xFF000000) >> 24);
			dwrite[ii] |= ((tmp_data & 0x00FF0000) >> 16) << 8;
			dwrite[ii] |= ((tmp_data & 0x0000FF00) >> 8) << 16;
			dwrite[ii] |= ((tmp_data & 0x000000FF) ) << 24;
			current_addr += 4;
		}
		// Let's Print What we are reading
		for (int ii = 0; ii < 64; ++ii) {
			if (    (dwrite[ii*4+0] != dread[ii*4+0])||
				(dwrite[ii*4+1] != dread[ii*4+1])||
				(dwrite[ii*4+2] != dread[ii*4+2])||
				(dwrite[ii*4+3] != dread[ii*4+3])) {
				cerr << "...Error Read from File   Addr=0x";
				printHex(write_start_addr+(ii*4*4), 8);
				cerr << " | "; printHex(dwrite[ii*4+0], 8);
				cerr << " "; printHex(dwrite[ii*4+1], 8);
				cerr << " "; printHex(dwrite[ii*4+2], 8);
				cerr << " "; printHex(dwrite[ii*4+3], 8);
				cerr << endl;

				cerr << "...Error Read from Memory Addr=0x";
				printHex(write_start_addr+(ii*4*4), 8);
				cerr << " | "; printHex(dread[ii*4+0], 8);
				cerr << " ";   printHex(dread[ii*4+1], 8);
				cerr << " ";   printHex(dread[ii*4+2], 8);
				cerr << " ";   printHex(dread[ii*4+3], 8);
				cerr << endl;
				}

		}
		write_start_addr = current_addr;
	}
  	binary_file.close();
	cerr << "==> Checking memory is done" << endl;
  }

  ir_in[0] = 0x22;
  jtag.shiftIR(ir_in, ir_out);
  cerr << "===Shifted USER3 (CPU) IN=0x" << (hex) << (int) ir_in[0]
	<<" OUT=0x" << (int) ir_out[0] << (dec) << (flush) << endl;
  
  cpu_read_bus(jtag, 0x00000010, dread, 1);
  cerr << "... NPC=0x"; printHex(dread[0], 8);
  cpu_read_bus(jtag, 0x00000012, dread, 1);
  cerr << " PPC=0x"; printHex(dread[0], 8);
  cerr << endl;

  cpu_read_bus(jtag, 0x00000000, dread, 8);
  cerr << "...VR      (RO)=0x"; printHex(dread[0], 8); cerr << endl;
  cerr << "...UPR     (RO)=0x"; printHex(dread[1], 8); cerr << endl;
  cerr << "...CPUCFGR (RO)=0x"; printHex(dread[2], 8); cerr << endl;
  cerr << "...DMMUCFGR(RO)=0x"; printHex(dread[3], 8); cerr << endl;
  cerr << "...IMMUCFGR(RO)=0x"; printHex(dread[4], 8); cerr << endl;
  cerr << "...DCCFGR  (RO)=0x"; printHex(dread[5], 8); cerr << endl;
  cerr << "...ICCFGR  (RO)=0x"; printHex(dread[6], 8); cerr << endl;
  cerr << "...DCFGR   (RO)=0x"; printHex(dread[7], 8); cerr << endl << endl;
 
  cpu_read_bus(jtag, 0x00000010, dread, 2);
  cerr << "...PC (RW)=0x"; printHex(dread[0], 8); cerr << endl;
  cerr << "...SR (RW)=0x"; printHex(dread[1], 8); cerr << endl << endl;;

  cpu_read_bus(jtag, 0x00000014, dread, 1);
  cerr << "...FPCSR (RW)=0x"; printHex(dread[0], 8); cerr << endl;
  cpu_read_bus(jtag, 0x00000020, dread, 1);
  cerr << "...EPCR0 (RW)=0x"; printHex(dread[0], 8); cerr << endl;
  cpu_read_bus(jtag, 0x00000030, dread, 1);
  cerr << "...EEAR0 (RW)=0x"; printHex(dread[0], 8); cerr << endl;
  cpu_read_bus(jtag, 0x00000040, dread, 1);
  cerr << "...ESR0  (RW)=0x"; printHex(dread[0], 8); cerr << endl;
  cerr << endl;

  cpu_read_bus(jtag, 0x00000400, dread, 16);
  cerr << endl;
  for (int ii = 0; ii < 16; ++ii) {
  	cerr << "...R" << ii << "=0x"; printHex(dread[ii], 8); cerr << endl;
  }

  //---> Let's Reset SR register to sim value:
  dwrite[ 0] = 0x00008001; cpu_write_bus(jtag, 0x00000011, dwrite, 1); // SR Register
  cpu_read_bus(jtag, 0x00000011, dread, 1);
  cerr << "...Changing SR (RW)=0x"; printHex(dread[0], 8); cerr << endl;

  //---> Let's set PC to 0x100
  dwrite[ 0] = 0x00000100; cpu_write_bus(jtag, 0x00000010, dwrite, 1); // SR Register
  cpu_read_bus(jtag, 0x00000010, dread, 1);
  cerr << "...Changing PC (RW)=0x"; printHex(dread[0], 8); cerr << endl;

  if (number_of_files != 0) {
  	// cerr << "...Unstall CPU0" << endl;
	// cpu_unstall(jtag);
  }

  return 0;
}

