#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <iostream>
#include <string.h>
#include <sstream>
#include <cstdio>
#include <list>
#include <memory>
#include <errno.h>
#include <assert.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <signal.h>
#include <fstream>

#include "drift_jtag_bridge.h"

#include "devicedb.h"
#include "cabledb.h"
#include "ioparport.h"
#include "debug.h"

#include "utilities.h"

using namespace std;
//-------------------------------------------------------------------//
int init_chain(Jtag &jtag, DeviceDB &db) {
  cerr << "...Inside init_chain" << endl;
  int num = jtag.getChain();
  if (num == 0) {
	fprintf(stderr,"No JTAG Chain found\n");
	return 0;
  } else {
  	cerr << "...Found " << num << " devices in chain" << endl;
  }
  // Synchronise database with chain of devices.
  for (int i=0; i<num; i++){
	unsigned long id = jtag.getDeviceID(i);
	int length=db.loadDevice(id);
	if (length>0)
		jtag.setDeviceIRLength(i,length);
	else {
		fprintf(stderr,"Cannot find device having IDCODE=%07lx Revision %c\n",
			id & 0x0fffffff,  (int)(id  >>28) + 'A');
		return 0;
	}
  }
  return num;
}
//-------------------------------------------------------------------//
static int last_pos = -1;

unsigned long get_id(Jtag &jtag, DeviceDB &db, int chainpos) {
  cerr << "...Inside get_id" << endl;
  bool verbose = jtag.getVerbose();
  int num = jtag.getChain();
  cerr << "...Device chain position = " << chainpos << endl;
  if (jtag.selectDevice(chainpos)<0) {
	cerr <<  "Invalid chain position " << chainpos << ", must be >= 0 and < "
		<< num << endl;
	return 0;
  }
  const char *dd=db.getDeviceDescription(chainpos);
  unsigned long id = jtag.getDeviceID(chainpos);
  if (verbose && (last_pos != chainpos)) {
  	cerr <<  "JTAG chainpos: " << chainpos << " Device IDCODE = 0x" << (hex) << id 
		<< " Desc: " << dd << endl;
	last_pos = chainpos;
  }
  return id;
}
//-------------------------------------------------------------------//
void printHex(uint32_t value, int width) {
	std::string hexStr;
	std::stringstream num;
	num.width(width);
	num.fill('0');
	num << std::fixed << std::hex << (int) value;
	hexStr = /* "0x"  + */ num.str();
	cerr << hexStr << flush;
}
//-------------------------------------------------------------------//
void cpu_write_bus( Jtag &jtag,  uint32_t addr,
		uint32_t *data, unsigned long length ){
  uint8_t din[9];
  uint8_t dout[9];

  // 7bit: read(0) or write (1) operation                               0x80
  // 6bit: Bus Operation indicator ( if 1 then reading or writing )     0x40
  // 5bit: Stall indicator                                              0x20
  // 4bit: Ack (ack_i) (BUS Mode) or BPI (Status Mode)                  0x10
  din[8] = 0xC0; // Header
  for (unsigned long ii = 0; ii < length; ++ii) {
	din[4]=((addr+ii*4)      ) & 0xFF; // Address: LSByte
	din[5]=((addr+ii*4) >> 8 ) & 0xFF;
	din[6]=((addr+ii*4) >> 16) & 0xFF;
	din[7]=((addr+ii*4) >> 24) & 0xFF; // Address: MSByte

	din[0]=((data[ii])      ) & 0xFF; // Data: LSByte
	din[1]=((data[ii]) >> 8 ) & 0xFF;
	din[2]=((data[ii]) >> 16) & 0xFF;
	din[3]=((data[ii]) >> 24) & 0xFF; // Data: MSByte

	jtag.shiftDR(din, dout, 72);
  }
}
//-------------------------------------------------------------------//
void cpu_read_bus( Jtag &jtag, uint32_t addr,
                uint32_t *data, unsigned long length){

  uint8_t din[9];
  uint8_t dout[9];

  din[4]=((addr)      ) & 0xFF; // Address: LSByte
  din[5]=((addr) >> 8 ) & 0xFF;
  din[6]=((addr) >> 16) & 0xFF;
  din[7]=((addr) >> 24) & 0xFF; // Address: MSByte

  din[0]= 0xFF; // Data: LSByte
  din[1]= 0xFF;
  din[2]= 0xFF;
  din[3]= 0xFF; // Data: MSByte

  // 7bit: read(0) or write (1) operation				0x80
  // 6bit: Bus Operation indicator ( if 1 then reading or writing )	0x40
  // 5bit: Stall indicator						0x20
  // 4bit: Ack (ack_i) (BUS Mode) or BPI (Status Mode)			0x10

  din[8] = 0x40; // Read
  // First Read, to get in 0th word of data
  jtag.shiftDR(din, dout, 72);

  for (unsigned long ii = 0; ii < length; ++ii) {
	din[4]=((addr+(ii+1)*4)      ) & 0xFF; // Address: LSByte
	din[5]=((addr+(ii+1)*4) >> 8 ) & 0xFF;
	din[6]=((addr+(ii+1)*4) >> 16) & 0xFF;
	din[7]=((addr+(ii+1)*4) >> 24) & 0xFF; // Address: MSByte

	jtag.shiftDR(din, dout, 72);
	data[ii]  = dout[0]; // Data: LSByte
	data[ii] |= dout[1] << 8;
	data[ii] |= dout[2] << 16;
	data[ii] |= dout[3] << 24;
  }
}
//-------------------------------------------------------------------//
void cpu_stall( Jtag &jtag) {
  uint8_t din[9];
  uint8_t dout[9];

  // Addr:
  din[4] = 0x11;
  din[5] = 0x22;
  din[6] = 0x44;
  din[7] = 0x88;

  // Data:
  din[0] = 0x11;
  din[1] = 0x22;
  din[2] = 0x44;
  din[3] = 0x88;
  
  // Header ( 8 bits )
  // 7bit: read(0) or write (1) operation 				=> 0
  // 6bit: Bus Operation indicator ( if 1 then reading or writing )	=> 0
  // 5bit: Stall indicator						=> 1
  // 4bit: Ack (ack_i) (BUS Mode) or BPI (Status Mode)			=> 0
  // 3bit: 
  // 2-0bit: Reserved

  din[8] = 0x20;

  jtag.shiftDR(din, dout, 72);
}
//-------------------------------------------------------------------//
void cpu_unstall( Jtag &jtag) {
  uint8_t din[9];
  uint8_t dout[9];

  // Addr:
  din[4] = 0x11;
  din[5] = 0x22;
  din[6] = 0x44;
  din[7] = 0x88;
  // Data:
  din[0] = 0x11;
  din[1] = 0x22;
  din[2] = 0x44;
  din[3] = 0x88;

  din[8] = 0x00;

  jtag.shiftDR(din, dout, 72);
}


//-------------------------------------------------------------------//
// length is expressed in words (4 bytes)
void wbm_write_bus( Jtag &jtag,  uint32_t addr, 
		uint32_t *data, unsigned long length, uint8_t mask){
  uint8_t din[9];
  uint8_t dout[9];

  din[8] = 0x80 | (mask & 0x0F); // Write
  for (unsigned long ii = 0; ii < length; ++ii) {
  	din[4]=((addr+ii*4)      ) & 0xFF; // Address: LSByte
	din[5]=((addr+ii*4) >> 8 ) & 0xFF;
	din[6]=((addr+ii*4) >> 16) & 0xFF;
	din[7]=((addr+ii*4) >> 24) & 0xFF; // Address: MSByte
	
	din[0]=((data[ii])      ) & 0xFF; // Data: LSByte
	din[1]=((data[ii]) >> 8 ) & 0xFF;
	din[2]=((data[ii]) >> 16) & 0xFF;
	din[3]=((data[ii]) >> 24) & 0xFF; // Data: MSByte

        jtag.shiftDR(din, dout, 72);
  }
}
//-------------------------------------------------------------------//
void wbm_read_bus( Jtag &jtag, uint32_t addr, 
		uint32_t *data, unsigned long length, uint8_t mask){
  uint8_t din[9];
  uint8_t dout[9];

  din[4]=((addr)      ) & 0xFF; // Address: LSByte
  din[5]=((addr) >> 8 ) & 0xFF;
  din[6]=((addr) >> 16) & 0xFF;
  din[7]=((addr) >> 24) & 0xFF; // Address: MSByte

  din[0]= 0xFF; // Data: LSByte
  din[1]= 0xFF;
  din[2]= 0xFF;
  din[3]= 0xFF; // Data: MSByte

  din[8] = 0x70 | (mask & 0x0F);; // Read

  // First Read, to get in 0th word of data
  jtag.shiftDR(din, dout, 72);

  for (unsigned long ii = 0; ii < length; ++ii) {
	din[4]=((addr+(ii+1)*4)      ) & 0xFF; // Address: LSByte
	din[5]=((addr+(ii+1)*4) >> 8 ) & 0xFF;
	din[6]=((addr+(ii+1)*4) >> 16) & 0xFF;
	din[7]=((addr+(ii+1)*4) >> 24) & 0xFF; // Address: MSByte

	jtag.shiftDR(din, dout, 72);

	data[ii]  = dout[0]; // Data: LSByte
	data[ii] |= dout[1] << 8;
	data[ii] |= dout[2] << 16;
	data[ii] |= dout[3] << 24;
  }

}
//-------------------------------------------------------------------//
int main (int argc, char **argv) {
  int c;
  uint32_t start_addr = 0x100;

  while ((c = getopt(argc, argv, "S:")) != EOF) {
    switch (c) {
	case 'S':
		start_addr = atol(optarg);
		continue;
    };
  }

  cerr << "...Start Address = 0x"; printHex(start_addr, 8); cerr << endl;

  int res;
  struct cable_t cable;
  CableDB cabledb(NULL);
  res = cabledb.getCable("pp", &cable);
  if(res) {
	cerr << "Can't find description for a cable named " << endl;
	cerr << "   Known Cables: " << endl;
	cabledb.dumpCables(stderr);
	exit ( 1 );
  } else {
  	cerr << "...Found PP Cable" << endl;
  }

  std::auto_ptr<IOBase> io;
  res = getIO( &io, &cable, 
  	NULL, // dev
	NULL, // serial
	true, // verbose
	false, // use_ftd2xx
	0 ); // jtag_freq

  if (res) {
	cerr << "...Can't get IO" << endl;
	exit (1);
  } else {
  	// cerr << "...Got IO" << endl;
  }
  // cerr << "...IO Pntr = " << io.get() << endl;
  
  Jtag jtag = Jtag(io.get());

  // cerr << "...Jtag Created" << endl;
  jtag.setVerbose(0);
  io.get()->setVerbose(0);
  // HW_FUNCTIONS|HW_DETAILS
  // PP_FUNCTIONS PP_DETAILS UP_FUNCTIONS UP_DETAILS CP_FUNCTIONS CP_DETAILS
  // UL_FUNCTIONS UL_DETAILS

  unsigned long id;
  DeviceDB db(NULL);
  // cerr << "...DB Created" << endl;
  if (init_chain(jtag, db))
	id = get_id (jtag, db, 0);
  else
	id = 0;

  if (id != 0x364c093) {
	cerr << "... The id " << id << " is not supported" << endl;
	exit ( 1 );
  } else {
  	// cerr << "...Found Right Device (Kintex 7) " << endl;
  }
//----------> After this line we can talk to the Data registers ----->
  uint8_t ir_in[256];
  uint8_t ir_out[256];

  uint32_t dwrite[256];
  uint32_t dread [256];

  jtag.setTapState(Jtag::TEST_LOGIC_RESET);
  jtag.selectDevice(0);
  // ir_out[0] = 0x03; // ( USER2)
  ir_in[0] = 0x03;
  jtag.shiftIR(ir_in, ir_out);
  cerr << "...Shifted USER2 IN=0x" << (hex) << (int) ir_in[0] 
	<<" OUT=0x" << (int) ir_out[0] << (dec) << (flush) << endl;
  //------> Wishbone interface is ready to recieve a data
  wbm_read_bus (jtag, 0x00000000, dread, 256, 0xF);
  for (int ii = 0; ii < 256; ++ii) {
  	cerr << "...Memory at 0x"; printHex(0x00000000+ii*4, 8); cerr<< ": 0x";
  	printHex(dread[ii], 8); cerr << endl;
  }
  wbm_read_bus (jtag, 0x00000000+256*4, dread, 256, 0xF);
  for (int ii = 0; ii < 256; ++ii) {
	cerr << "...Memory at 0x"; printHex(0x00000000+256*4+ii*4, 8); cerr<< ": 0x";
	printHex(dread[ii], 8); cerr << endl;
  }
  wbm_read_bus (jtag, 0x7c0000, dread, 256, 0xF);
  for (int ii = 0; ii < 256; ++ii) {
  	cerr << "...Stack at 0x"; printHex(0x7c0000+ii*4, 8); cerr<< ": 0x";
	printHex(dread[ii], 8); cerr << endl;
  }
  wbm_read_bus (jtag, 0x7c0000+256*4, dread, 256, 0xF);
  for (int ii = 0; ii < 256; ++ii) {
	cerr << "...Stack at 0x"; printHex(0x7c0000+256*4+ii*4, 8); cerr<< ": 0x";
	printHex(dread[ii], 8); cerr << endl;
  }
  wbm_read_bus (jtag, 0x7c0000+2*256*4, dread, 256, 0xF);
  for (int ii = 0; ii < 256; ++ii) {
  	cerr << "...Stack at 0x"; printHex(0x7c0000+2*256*4+ii*4, 8); cerr<< ": 0x";
	printHex(dread[ii], 8); cerr << endl;
  }
  wbm_read_bus (jtag, 0x7c0000+3*256*4, dread, 256, 0xF);
  for (int ii = 0; ii < 256; ++ii) {
  	cerr << "...Stack at 0x"; printHex(0x7c0000+3*256*4+ii*4, 8); cerr<< ": 0x";
	printHex(dread[ii], 8); cerr << endl;
  }
  //--------> Stall the CPU first -------//
  ir_in[0] = 0x22;
  jtag.shiftIR(ir_in, ir_out);
  cerr << "===Shifted USER3 (CPU) IN=0x" << (hex) << (int) ir_in[0]
  	<<" OUT=0x" << (int) ir_out[0] << (dec) << (flush) << endl;

  cerr << "...Stall CPU0" << endl;
  cpu_stall(jtag);
  

  //------------> Configuring the CPU ---------------//
  ir_in[0] = 0x03; 
  jtag.shiftIR(ir_in, ir_out);
  cerr << "===Shifted USER2 (Wishbone) IN=0x" << (hex) << (int) ir_in[0]
	<<" OUT=0x" << (int) ir_out[0] << (dec) << (flush) << endl;
  //
  // Uart
  //
  cerr << "...Reading UART0 Status Registers: " << endl << flush;
  wbm_read_bus (jtag, 0x90000000, dread, 1, 0x8);
  cerr << "... Read RB   (Recieve Buffer): 0x"; printHex(dread[0], 8); cerr << endl;
  wbm_read_bus (jtag, 0x90000001, dread, 1, 0x4);
  cerr << "... Read IE (Interrupt Enable): 0x"; printHex(dread[0], 8); cerr << endl;
  wbm_read_bus (jtag, 0x90000002, dread, 1, 0x2);
  cerr << "... Read II (Interrupt ID):     0x"; printHex(dread[0], 8); cerr << endl;
  wbm_read_bus (jtag, 0x90000003, dread, 1, 0x1);
  cerr << "... Read LC (Line Control):     0x"; printHex(dread[0], 8); cerr << endl;
  wbm_read_bus (jtag, 0x90000004, dread, 1, 0x8);
  cerr << "... Read MC (Modem Control):    0x"; printHex(dread[0], 8); cerr << endl;
  wbm_read_bus (jtag, 0x90000005, dread, 1, 0x4);
  cerr << "... Read LS (Line Status):      0x"; printHex(dread[0], 8); cerr << endl;
  wbm_read_bus (jtag, 0x90000006, dread, 1, 0x2);
  cerr << "... Read MS (Modem Status):     0x"; printHex(dread[0], 8); cerr << endl;
  wbm_read_bus (jtag, 0x90000007, dread, 1, 0x1);
  cerr << "... Read SR (Scratch Register): 0x"; printHex(dread[0], 8); cerr << endl;

  // We want to select line control register first
  dwrite[0] = 0x00000083; wbm_write_bus(jtag, 0x90000003, dwrite, 1, 0x1); // Select DLC
  dwrite[0] = 0x00000000; wbm_write_bus(jtag, 0x90000001, dwrite, 1, 0x4); // Reg IE then contains High
  dwrite[0] = 0x1B000000; wbm_write_bus(jtag, 0x90000000, dwrite, 1, 0x8); // TR is low
  dwrite[0] = 0x00000003; wbm_write_bus(jtag, 0x90000003, dwrite, 1, 0x1); // Unselect Select DLC

  // UART_REG_TR:
  dwrite[0] = 0x44000000; wbm_write_bus(jtag, 0x90000000, dwrite, 1, 0x8); // D
  dwrite[0] = 0x52000000; wbm_write_bus(jtag, 0x90000000, dwrite, 1, 0x8); // R
  dwrite[0] = 0x49000000; wbm_write_bus(jtag, 0x90000000, dwrite, 1, 0x8); // I
  dwrite[0] = 0x46000000; wbm_write_bus(jtag, 0x90000000, dwrite, 1, 0x8); // F
  dwrite[0] = 0x54000000; wbm_write_bus(jtag, 0x90000000, dwrite, 1, 0x8); // T
  dwrite[0] = 0x0A000000; wbm_write_bus(jtag, 0x90000000, dwrite, 1, 0x8); // LF
  dwrite[0] = 0x0D000000; wbm_write_bus(jtag, 0x90000000, dwrite, 1, 0x8); // CR
  dwrite[0] = 0x0D000000; wbm_write_bus(jtag, 0x90000000, dwrite, 1, 0x8); // CR


  
  
  return 0;
}

