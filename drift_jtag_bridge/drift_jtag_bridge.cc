#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <iostream>
#include <string.h>
#include <sstream>
#include <cstdio>
#include <list>
#include <memory>
#include <errno.h>
#include <assert.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <signal.h>
#include <fstream>

#include "drift_jtag_bridge.h"

#include "devicedb.h"
#include "cabledb.h"
#include "ioparport.h"
#include "debug.h"

#include "utilities.h"

using namespace std;
//-------------------------------------------------------------------//
int init_chain(Jtag &jtag, DeviceDB &db) {
  cerr << "...Inside init_chain" << endl;
  int num = jtag.getChain();
  if (num == 0) {
	fprintf(stderr,"No JTAG Chain found\n");
	return 0;
  } else {
  	cerr << "...Found " << num << " devices in chain" << endl;
  }
  // Synchronise database with chain of devices.
  for (int i=0; i<num; i++){
	unsigned long id = jtag.getDeviceID(i);
	int length=db.loadDevice(id);
	if (length>0)
		jtag.setDeviceIRLength(i,length);
	else {
		fprintf(stderr,"Cannot find device having IDCODE=%07lx Revision %c\n",
			id & 0x0fffffff,  (int)(id  >>28) + 'A');
		return 0;
	}
  }
  return num;
}
//-------------------------------------------------------------------//
static int last_pos = -1;

unsigned long get_id(Jtag &jtag, DeviceDB &db, int chainpos) {
  cerr << "...Inside get_id" << endl;
  bool verbose = jtag.getVerbose();
  int num = jtag.getChain();
  cerr << "...Device chain position = " << chainpos << endl;
  if (jtag.selectDevice(chainpos)<0) {
	cerr <<  "Invalid chain position " << chainpos << ", must be >= 0 and < "
		<< num << endl;
	return 0;
  }
  const char *dd=db.getDeviceDescription(chainpos);
  unsigned long id = jtag.getDeviceID(chainpos);
  if (verbose && (last_pos != chainpos)) {
  	cerr <<  "JTAG chainpos: " << chainpos << " Device IDCODE = 0x" << (hex) << id 
		<< " Desc: " << dd << endl;
	last_pos = chainpos;
  }
  return id;
}
//-------------------------------------------------------------------//
void printHex(uint32_t value, int width) {
	std::string hexStr;
	std::stringstream num;
	num.width(width);
	num.fill('0');
	num << std::fixed << std::hex << (int) value;
	hexStr = /* "0x"  + */ num.str();
	cerr << hexStr << flush;
}
//-------------------------------------------------------------------//
void cpu_write_bus( Jtag &jtag,  uint32_t addr,
		uint32_t *data, unsigned long length ){
  uint8_t din[9];
  uint8_t dout[9];

  // 7bit: read(0) or write (1) operation                               0x80
  // 6bit: Bus Operation indicator ( if 1 then reading or writing )     0x40
  // 5bit: Stall indicator                                              0x20
  // 4bit: Ack (ack_i) (BUS Mode) or BPI (Status Mode)                  0x10
  din[8] = 0xC0; // Header
  for (unsigned long ii = 0; ii < length; ++ii) {
	din[4]=((addr+ii)      ) & 0xFF; // Address: LSByte
	din[5]=((addr+ii) >> 8 ) & 0xFF;
	din[6]=((addr+ii) >> 16) & 0xFF;
	din[7]=((addr+ii) >> 24) & 0xFF; // Address: MSByte

	din[0]=((data[ii])      ) & 0xFF; // Data: LSByte
	din[1]=((data[ii]) >> 8 ) & 0xFF;
	din[2]=((data[ii]) >> 16) & 0xFF;
	din[3]=((data[ii]) >> 24) & 0xFF; // Data: MSByte

	jtag.shiftDR(din, dout, 72);
  }
}
//-------------------------------------------------------------------//
void cpu_read_bus( Jtag &jtag, uint32_t addr,
                uint32_t *data, unsigned long length){

  uint8_t din[9];
  uint8_t dout[9];

  din[4]=((addr)      ) & 0xFF; // Address: LSByte
  din[5]=((addr) >> 8 ) & 0xFF;
  din[6]=((addr) >> 16) & 0xFF;
  din[7]=((addr) >> 24) & 0xFF; // Address: MSByte

  din[0]= 0xFF; // Data: LSByte
  din[1]= 0xFF;
  din[2]= 0xFF;
  din[3]= 0xFF; // Data: MSByte

  // 7bit: read(0) or write (1) operation				0x80
  // 6bit: Bus Operation indicator ( if 1 then reading or writing )	0x40
  // 5bit: Stall indicator						0x20
  // 4bit: Ack (ack_i) (BUS Mode) or BPI (Status Mode)			0x10

  din[8] = 0x40; // Read
  // First Read, to get in 0th word of data
  jtag.shiftDR(din, dout, 72);

  for (unsigned long ii = 0; ii < length; ++ii) {
	din[4]=((addr+(ii+1))      ) & 0xFF; // Address: LSByte
	din[5]=((addr+(ii+1)) >> 8 ) & 0xFF;
	din[6]=((addr+(ii+1)) >> 16) & 0xFF;
	din[7]=((addr+(ii+1)) >> 24) & 0xFF; // Address: MSByte

	jtag.shiftDR(din, dout, 72);
	data[ii]  = dout[0]; // Data: LSByte
	data[ii] |= dout[1] << 8;
	data[ii] |= dout[2] << 16;
	data[ii] |= dout[3] << 24;
  }
}
//-------------------------------------------------------------------//
void cpu_stall( Jtag &jtag) {
  uint8_t din[9];
  uint8_t dout[9];

  // Addr:
  din[4] = 0x11;
  din[5] = 0x22;
  din[6] = 0x44;
  din[7] = 0x88;

  // Data:
  din[0] = 0x11;
  din[1] = 0x22;
  din[2] = 0x44;
  din[3] = 0x88;
  
  // Header ( 8 bits )
  // 7bit: read(0) or write (1) operation 				=> 0
  // 6bit: Bus Operation indicator ( if 1 then reading or writing )	=> 0
  // 5bit: Stall indicator						=> 1
  // 4bit: Ack (ack_i) (BUS Mode) or BPI (Status Mode)			=> 0
  // 3bit: 
  // 2-0bit: Reserved

  din[8] = 0x20;

  jtag.shiftDR(din, dout, 72);
}
//-------------------------------------------------------------------//
void cpu_unstall( Jtag &jtag) {
  uint8_t din[9];
  uint8_t dout[9];

  // Addr:
  din[4] = 0x11;
  din[5] = 0x22;
  din[6] = 0x44;
  din[7] = 0x88;
  // Data:
  din[0] = 0x11;
  din[1] = 0x22;
  din[2] = 0x44;
  din[3] = 0x88;

  din[8] = 0x00;

  jtag.shiftDR(din, dout, 72);
}


//-------------------------------------------------------------------//
// length is expressed in words (4 bytes)
void wbm_write_bus( Jtag &jtag,  uint32_t addr, 
		uint32_t *data, unsigned long length, uint8_t mask){
  uint8_t din[9];
  uint8_t dout[9];

  din[8] = 0x80 | (mask & 0x0F); // Write
  for (unsigned long ii = 0; ii < length; ++ii) {
  	din[4]=((addr+ii*4)      ) & 0xFF; // Address: LSByte
	din[5]=((addr+ii*4) >> 8 ) & 0xFF;
	din[6]=((addr+ii*4) >> 16) & 0xFF;
	din[7]=((addr+ii*4) >> 24) & 0xFF; // Address: MSByte
	
	din[0]=((data[ii])      ) & 0xFF; // Data: LSByte
	din[1]=((data[ii]) >> 8 ) & 0xFF;
	din[2]=((data[ii]) >> 16) & 0xFF;
	din[3]=((data[ii]) >> 24) & 0xFF; // Data: MSByte

        jtag.shiftDR(din, dout, 72);
  }
}
//-------------------------------------------------------------------//
void wbm_read_bus( Jtag &jtag, uint32_t addr, 
		uint32_t *data, unsigned long length, uint8_t mask){
  uint8_t din[9];
  uint8_t dout[9];

  din[4]=((addr)      ) & 0xFF; // Address: LSByte
  din[5]=((addr) >> 8 ) & 0xFF;
  din[6]=((addr) >> 16) & 0xFF;
  din[7]=((addr) >> 24) & 0xFF; // Address: MSByte

  din[0]= 0xFF; // Data: LSByte
  din[1]= 0xFF;
  din[2]= 0xFF;
  din[3]= 0xFF; // Data: MSByte

  din[8] = 0x70 | (mask & 0x0F);; // Read

  // First Read, to get in 0th word of data
  jtag.shiftDR(din, dout, 72);

  for (unsigned long ii = 0; ii < length; ++ii) {
	din[4]=((addr+(ii+1)*4)      ) & 0xFF; // Address: LSByte
	din[5]=((addr+(ii+1)*4) >> 8 ) & 0xFF;
	din[6]=((addr+(ii+1)*4) >> 16) & 0xFF;
	din[7]=((addr+(ii+1)*4) >> 24) & 0xFF; // Address: MSByte

	jtag.shiftDR(din, dout, 72);

	data[ii]  = dout[0]; // Data: LSByte
	data[ii] |= dout[1] << 8;
	data[ii] |= dout[2] << 16;
	data[ii] |= dout[3] << 24;
  }

}
//-------------------------------------------------------------------//
int main (int argc, char **argv) {
  int c;
  uint32_t start_addr = 0xF0000100;

  while ((c = getopt(argc, argv, "S:")) != EOF) {
    switch (c) {
	case 'S':
		start_addr = atol(optarg);
		continue;
    };
  }

  cerr << "...Start Address = 0x"; printHex(start_addr, 8); cerr << endl;

  int res;
  struct cable_t cable;
  CableDB cabledb(NULL);
  res = cabledb.getCable("pp", &cable);
  if(res) {
	cerr << "Can't find description for a cable named " << endl;
	cerr << "   Known Cables: " << endl;
	cabledb.dumpCables(stderr);
	exit ( 1 );
  } else {
  	cerr << "...Found PP Cable" << endl;
  }

  std::auto_ptr<IOBase> io;
  res = getIO( &io, &cable, 
  	NULL, // dev
	NULL, // serial
	true, // verbose
	false, // use_ftd2xx
	0 ); // jtag_freq

  if (res) {
	cerr << "...Can't get IO" << endl;
	exit (1);
  } else {
  	// cerr << "...Got IO" << endl;
  }
  // cerr << "...IO Pntr = " << io.get() << endl;
  
  Jtag jtag = Jtag(io.get());

  // cerr << "...Jtag Created" << endl;
  jtag.setVerbose(0);
  io.get()->setVerbose(0);
  // HW_FUNCTIONS|HW_DETAILS
  // PP_FUNCTIONS PP_DETAILS UP_FUNCTIONS UP_DETAILS CP_FUNCTIONS CP_DETAILS
  // UL_FUNCTIONS UL_DETAILS

  unsigned long id;
  DeviceDB db(NULL);
  // cerr << "...DB Created" << endl;
  if (init_chain(jtag, db))
	id = get_id (jtag, db, 0);
  else
	id = 0;

  if (id != 0x364c093) {
	cerr << "... The id " << id << " is not supported" << endl;
	exit ( 1 );
  } else {
  	// cerr << "...Found Right Device (Kintex 7) " << endl;
  }
//----------> After this line we can talk to the Data registers ----->
  uint8_t ir_in[256];
  uint8_t ir_out[256];

  uint32_t dwrite[256];
  uint32_t dread [256];

  jtag.setTapState(Jtag::TEST_LOGIC_RESET);
  jtag.selectDevice(0);
  // ir_out[0] = 0x03; // ( USER2)
  ir_in[0] = 0x03;
  jtag.shiftIR(ir_in, ir_out);
  cerr << "...Shifted USER2 IN=0x" << (hex) << (int) ir_in[0] 
	<<" OUT=0x" << (int) ir_out[0] << (dec) << (flush) << endl;
  //------> Wishbone interface is ready to recieve a data
  wbm_read_bus (jtag, 0x00000000, dread, 256, 0xF);
  for (int ii = 0; ii < 256; ++ii) {
  	cerr << "...Memory at 0x"; printHex(0x00000000+ii*4, 8); cerr<< ": 0x";
	printHex(dread[ii], 8); cerr << endl;
  }
  wbm_read_bus (jtag, 0x007C0000, dread, 256, 0xF);
  for (int ii = 0; ii < 256; ++ii) {
  	cerr << "...Stack at 0x"; printHex(0x007C0000+ii*4, 8); cerr<< ": 0x";
  	printHex(dread[ii], 8); cerr << endl;
  }
  wbm_read_bus (jtag, 0x007C0000+256, dread, 256, 0xF);
  for (int ii = 0; ii < 256; ++ii) {
	cerr << "...Stack at 0x"; printHex(0x007C0000+256+ii*4, 8); cerr<< ": 0x";
	printHex(dread[ii], 8); cerr << endl;
  }
  //--------> Stall the CPU first -------//
  /*
  ir_in[0] = 0x22;
  jtag.shiftIR(ir_in, ir_out);
  cerr << "===Shifted USER3 (CPU) IN=0x" << (hex) << (int) ir_in[0]
  	<<" OUT=0x" << (int) ir_out[0] << (dec) << (flush) << endl;

  cerr << "...Stall CPU0" << endl;
  cpu_stall(jtag);
  */

  //------------> Configuring the CPU ---------------//
  ir_in[0] = 0x22;
  jtag.shiftIR(ir_in, ir_out);
  cerr << "===Shifted USER3 (CPU) IN=0x" << (hex) << (int) ir_in[0]
	<<" OUT=0x" << (int) ir_out[0] << (dec) << (flush) << endl;
  cpu_stall(jtag);

  cerr << "...Writing Setup" << endl;
  // dwrite[ 0] = 0x00000000; cpu_write_bus(jtag, 0x00000011, dwrite, 1); // Enable exceptions 
  // dwrite[ 0] = 0x00000000; cpu_write_bus(jtag, 0x00003014, dwrite, 1); // Trap causes stall
  // dwrite[ 0] = start_addr; cpu_write_bus(jtag, 0x00000010, dwrite, 1); // Set PC to start_addr
  // dwrite[ 0] = start_addr+4; cpu_write_bus(jtag, 0x00000012, dwrite, 1); // Set NPC to start_addr
  /*
  dwrite[ 0] = 0x00000000; cpu_write_bus(jtag, 0x00000400, dwrite, 1); // Set R0 to 0
  dwrite[ 0] = 0x00000000; cpu_write_bus(jtag, 0x00000401, dwrite, 1); // Set R1 to 0
  dwrite[ 0] = 0x00000000; cpu_write_bus(jtag, 0x00000402, dwrite, 1); // Set R2 to 0
  dwrite[ 0] = 0x00000000; cpu_write_bus(jtag, 0x00000403, dwrite, 1); // Set R3 to 0
  dwrite[ 0] = 0x00000000; cpu_write_bus(jtag, 0x00000404, dwrite, 1); // Set R4 to 0
  dwrite[ 0] = 0x00000000; cpu_write_bus(jtag, 0x00000405, dwrite, 1); // Set R5 to 0
  dwrite[ 0] = 0x00000000; cpu_write_bus(jtag, 0x00000406, dwrite, 1); // Set R6 to 0
  dwrite[ 0] = 0x00000000; cpu_write_bus(jtag, 0x00000407, dwrite, 1); // Set R7 to 0
  dwrite[ 0] = 0x00000000; cpu_write_bus(jtag, 0x00000408, dwrite, 1); // Set R8 to 0
  dwrite[ 0] = 0x00000000; cpu_write_bus(jtag, 0x00000409, dwrite, 1); // Set R9 to 0
  dwrite[ 0] = 0x00000000; cpu_write_bus(jtag, 0x0000040A, dwrite, 1); // Set R10 to 0
  dwrite[ 0] = 0x00000000; cpu_write_bus(jtag, 0x0000040B, dwrite, 1); // Set R11 to 0
  dwrite[ 0] = 0x00000000; cpu_write_bus(jtag, 0x0000040C, dwrite, 1); // Set R12 to 0
  dwrite[ 0] = 0x00000000; cpu_write_bus(jtag, 0x0000040D, dwrite, 1); // Set R13 to 0
  dwrite[ 0] = 0x00000000; cpu_write_bus(jtag, 0x0000040E, dwrite, 1); // Set R14 to 0
  dwrite[ 0] = 0x00000000; cpu_write_bus(jtag, 0x0000040F, dwrite, 1); // Set R15 to 0
  dwrite[ 0] = 0x00000000; cpu_write_bus(jtag, 0x00000410, dwrite, 1); // Set R16 to 0
  dwrite[ 0] = 0x00000000; cpu_write_bus(jtag, 0x00000411, dwrite, 1); // Set R17 to 0
  dwrite[ 0] = 0x00000000; cpu_write_bus(jtag, 0x00000412, dwrite, 1); // Set R18 to 0
  dwrite[ 0] = 0x00000000; cpu_write_bus(jtag, 0x00000413, dwrite, 1); // Set R19 to 0
  dwrite[ 0] = 0x00000000; cpu_write_bus(jtag, 0x00000414, dwrite, 1); // Set R20 to 0
  dwrite[ 0] = 0x00000000; cpu_write_bus(jtag, 0x00000415, dwrite, 1); // Set R21 to 0
  dwrite[ 0] = 0x00000000; cpu_write_bus(jtag, 0x00000416, dwrite, 1); // Set R22 to 0
  dwrite[ 0] = 0x00000000; cpu_write_bus(jtag, 0x00000417, dwrite, 1); // Set R23 to 0
  dwrite[ 0] = 0x00000000; cpu_write_bus(jtag, 0x00000418, dwrite, 1); // Set R24 to 0
  dwrite[ 0] = 0x00000000; cpu_write_bus(jtag, 0x00000419, dwrite, 1); // Set R25 to 0
  dwrite[ 0] = 0x00000000; cpu_write_bus(jtag, 0x0000041A, dwrite, 1); // Set R26 to 0
  dwrite[ 0] = 0x00000000; cpu_write_bus(jtag, 0x0000041B, dwrite, 1); // Set R27 to 0
  dwrite[ 0] = 0x00000000; cpu_write_bus(jtag, 0x0000041C, dwrite, 1); // Set R28 to 0
  dwrite[ 0] = 0x00000000; cpu_write_bus(jtag, 0x0000041D, dwrite, 1); // Set R29 to 0
  dwrite[ 0] = 0x00000000; cpu_write_bus(jtag, 0x0000041E, dwrite, 1); // Set R30 to 0
  dwrite[ 0] = 0x00000000; cpu_write_bus(jtag, 0x0000041F, dwrite, 1); // Set R31 to 0
  // dwrite[ 0] = 0x00400000; cpu_write_bus(jtag, 0x00003010, dwrite, 1); // Set step bit
  cpu_read_bus(jtag, 0x00000000, dread, 1);
  */

  cerr << "...Unstall CPU0" << endl;
  cpu_unstall(jtag);

  for (int ii = 0; ii < 300; ++ii) {
  	cpu_stall(jtag);

	cpu_read_bus(jtag, 0x00000014, dread, 1);
	cerr << "...FPCSR (RW)=0x"; printHex(dread[0], 8); cerr << endl;
	cpu_read_bus(jtag, 0x00000020, dread, 1);
	cerr << "...EPCR0 (RW)=0x"; printHex(dread[0], 8); cerr << endl;
	cpu_read_bus(jtag, 0x00000030, dread, 1);
	cerr << "...EEAR0 (RW)=0x"; printHex(dread[0], 8); cerr << endl;
	cpu_read_bus(jtag, 0x00000040, dread, 1);
	cerr << "...ESR0  (RW)=0x"; printHex(dread[0], 8); cerr << endl;

	cerr << "--------"<< endl;
	cpu_read_bus(jtag, 0x00000012, dread, 1);
	cerr << " PPC=0x"; printHex(dread[0], 8); cerr << endl;
	cpu_read_bus(jtag, 0x00000010, dread, 1);
	cerr << " NPC=0x"; printHex(dread[0], 8); cerr << endl;

	cpu_read_bus(jtag, 0x00000400, dread, 32);
	cerr << " GPR00: "; printHex(dread[0], 8); cerr << " GPR01: "; printHex(dread[1], 8);
	cerr << " GPR02: "; printHex(dread[2], 8); cerr << " GPR03: "; printHex(dread[3], 8);
	cerr << endl;
	cerr << " GPR04: "; printHex(dread[4], 8); cerr << " GPR05: "; printHex(dread[5], 8);
	cerr << " GPR06: "; printHex(dread[6], 8); cerr << " GPR07: "; printHex(dread[7], 8);
	cerr << endl;
	cerr << " GPR08: "; printHex(dread[8], 8); cerr << " GPR09: "; printHex(dread[9], 8);
	cerr << " GPR10: "; printHex(dread[10], 8); cerr << " GPR11: "; printHex(dread[11], 8);
	cerr << endl;
	cerr << " GPR12: "; printHex(dread[12], 8); cerr << " GPR13: "; printHex(dread[13], 8);
	cerr << " GPR14: "; printHex(dread[14], 8); cerr << " GPR15: "; printHex(dread[15], 8);
	cerr << endl;
	cerr << " GPR16: "; printHex(dread[16], 8); cerr << " GPR17: "; printHex(dread[17], 8);
	cerr << " GPR18: "; printHex(dread[18], 8); cerr << " GPR19: "; printHex(dread[19], 8);
	cerr << endl;
	cerr << " GPR20: "; printHex(dread[20], 8); cerr << " GPR21: "; printHex(dread[21], 8);
	cerr << " GPR22: "; printHex(dread[22], 8); cerr << " GPR23: "; printHex(dread[23], 8);
	cerr << endl;
	cerr << " GPR24: "; printHex(dread[24], 8); cerr << " GPR25: "; printHex(dread[25], 8);
	cerr << " GPR26: "; printHex(dread[26], 8); cerr << " GPR27: "; printHex(dread[27], 8);
	cerr << endl;
	cerr << " GPR28: "; printHex(dread[28], 8); cerr << " GPR29: "; printHex(dread[29], 8);
	cerr << " GPR30: "; printHex(dread[30], 8); cerr << " GPR31: "; printHex(dread[31], 8);
	cerr << endl;
	cerr << "=============" << ii << endl;
	cpu_read_bus(jtag, 0x00000000, dread, 1);

	cpu_unstall(jtag);
	// usleep(100);
  }

  ir_in[0] = 0x03; 
  jtag.shiftIR(ir_in, ir_out);
  cerr << "===Shifted USER2 (Wishbone) IN=0x" << (hex) << (int) ir_in[0]
	<<" OUT=0x" << (int) ir_out[0] << (dec) << (flush) << endl;
  return 0;
}

