#include "iobase.h"
#include "utilities.h"

#include <unistd.h>
#include <stdio.h>
#include <string.h>
 
using namespace std;
//-------------------------------------------------------------------//
IOBase::IOBase()
{
    verbose = 0x00000000;
    memset( ones,0xff,CHUNK_SIZE);
    memset(zeros,   0,CHUNK_SIZE);
    memset(tms_buf,   0,CHUNK_SIZE);
    tms_len = 0;
}    
//-------------------------------------------------------------------//
int IOBase::Init(struct cable_t *cable, const char *devopt, unsigned int freq) {
    return 0;
}    
//-------------------------------------------------------------------//
void IOBase::flush_tms(int force) {
  if (tms_len)
    tx_tms(tms_buf, tms_len, force);
  memset(tms_buf,   0,CHUNK_SIZE);
  tms_len = 0;
}
//-------------------------------------------------------------------//
void IOBase::set_tms(bool val) {
  if( tms_len + 1 > CHUNK_SIZE*8)
    flush_tms(false);
  if(val)
    tms_buf[tms_len/8] |= (1 <<(tms_len &0x7));
  tms_len++;
}
//-------------------------------------------------------------------//
void IOBase::shiftTDITDO(const unsigned char *tdi, unsigned char *tdo,
			 int length, bool last) {
  if(length==0) return;
  flush_tms(false);
  txrx_block(tdi, tdo, length,last);
  return;
}
//-------------------------------------------------------------------//
void IOBase::shiftTDI(const unsigned char *tdi, int length, bool last) {
  shiftTDITDO(tdi, NULL, length,last);
}
//-------------------------------------------------------------------//
// TDI gets a load of zeros, we just record TDO.
void IOBase::shiftTDO(unsigned char *tdo, int length, bool last) {
    shiftTDITDO(NULL, tdo, length,last);
}
//-------------------------------------------------------------------//
// TDI gets a load of zeros or ones, and we ignore TDO
void IOBase::shift(bool tdi, int length, bool last) {
    int len = length;
    unsigned char *block = (tdi)?ones:zeros;
    flush_tms(false);
    while (len > CHUNK_SIZE*8)
    {
	txrx_block(block, NULL, CHUNK_SIZE*8, false);
	len -= (CHUNK_SIZE*8);
    }
    shiftTDITDO(block, NULL, len, last);
}
//-------------------------------------------------------------------//
void IOBase::Usleep(unsigned int usec) {
  flush_tms(false);
  flush();
  xc3sprog_Usleep(usec);
}
//-------------------------------------------------------------------//
