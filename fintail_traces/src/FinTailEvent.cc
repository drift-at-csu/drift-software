#include "FinTailEvent.h"
using namespace FinTail;

#include <stdio.h>
#include <iostream>
using namespace std;

#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/fstream.hpp>
// using namespace boost::filesystem;


//-------------------------------------------------------------------//
FinTailEvent::FinTailEvent():
	fMinSignal(0.0), fMaxSignal(0.0),
	fSource(eEventSrcUnknown),
	fMinSignalPos(0.0), fMinSignalNeg(0.0),
	fMaxSignalPos(0.0), fMaxSignalNeg(0.0) {

}
//-------------------------------------------------------------------//
FinTailEvent::~FinTailEvent() {
  fTraces.clear();
}
//-------------------------------------------------------------------//
int FinTailEvent::Load(char *aPath, TraceSource aSource) {

  boost::filesystem::path dir_path = aPath;

  if( !boost::filesystem::exists(dir_path) ) {
    cout << " No such directory " << endl;
    return -1;
  }

  boost::filesystem::recursive_directory_iterator end_itr;

  for( boost::filesystem::recursive_directory_iterator iter(dir_path) ; 
  		iter != end_itr ; ++iter ) {
    if ( !boost::filesystem::is_directory( *iter ) ) {
	// cout << "..." << iter->filename() << endl;
	// Now one need to try to load all of the bin files
	size_t found;
#if BOOST_FILESYSTEM_VERSION >= 3
	found=iter->path().string().find(".bin");
#else
	found=iter->filename().find(".bin");
#endif
	if (found!=string::npos) {
		// cout << "==>" << iter->filename() << endl;
		FinTailTrace * thisTrace = NULL;
		thisTrace = new FinTailTrace();
		if (thisTrace->Load(	
			(char *) (/* dir_path.string() + "/" + */
#if BOOST_FILESYSTEM_VERSION >= 3
				
				iter->path().string()
#else
				iter->filename()
#endif
				).c_str() )) {
			// cout << "Trace loaded without a problem" << endl;
			//Let's check the quality
			if (thisTrace->GetQuality() == eBad) {
				cout << "...Trace " << thisTrace->GetFileSource()
					<< " is bad" << endl;
				delete (thisTrace);
				continue;
			}

			if (thisTrace->GetSource() == aSource ) {
				// This is a trace from the source that we need,
				// let's process it and add the results
				FinTailTraceProcessing aProcTrace(thisTrace);

				// Standard procedure for calculating our signal
				/*
				aProcTrace.FindMinMax((thisTrace->GetNofBins()/4),
					thisTrace->GetNofBins() - 
						(thisTrace->GetNofBins()/4) );
				*/

				aProcTrace.FindLongestPositiveJumpPosition(
					(thisTrace->GetNofBins()/4),
					thisTrace->GetNofBins() -
						(thisTrace->GetNofBins()/4), 
					100 ); // integrate over 1.0 usec
				aProcTrace.FindBeforePedestal(500); // 5 usec before
				aProcTrace.FindAfterPedestal(500);  // 5 usec after
				aProcTrace.FindSignal();


				fTraces.push_back(aProcTrace);

				// Let's assign minimum and maximum signals here too
				if (GetNumberOfTraces() == 1) {
					// This was a first trace
					fMinSignal = aProcTrace.GetSignal();
					fMaxSignal = aProcTrace.GetSignal();

					fMinSignalPos = aProcTrace.GetSignalBefore();
					fMaxSignalPos = aProcTrace.GetSignalBefore();

					fMinSignalNeg = aProcTrace.GetSignalAfter();
					fMaxSignalNeg = aProcTrace.GetSignalAfter();
				} else {
					if (aProcTrace.GetSignal() > fMaxSignal)
						fMaxSignal = aProcTrace.GetSignal();
					if (aProcTrace.GetSignal() < fMinSignal)
						fMinSignal = aProcTrace.GetSignal();

					if (aProcTrace.GetSignalBefore() > fMaxSignalPos)
						fMaxSignalPos = aProcTrace.GetSignalBefore();
					if (aProcTrace.GetSignalBefore() < fMinSignalPos)
						fMinSignalPos = aProcTrace.GetSignalBefore();

					if (aProcTrace.GetSignalAfter() > fMaxSignalNeg)
						fMaxSignalNeg = aProcTrace.GetSignalAfter();
					if (aProcTrace.GetSignalAfter() < fMinSignalNeg)
						fMinSignalNeg = aProcTrace.GetSignalAfter();
				}

				if (    (aProcTrace.GetSignal() < -40000.0)||
					(aProcTrace.GetSignal() >  40000.0) ){
					
					cout << "Signal = " << aProcTrace.GetSignal()
						<< " for " << thisTrace->GetFileSource()
						<< endl;
				}
				
			}
			delete (thisTrace);
		}
	}
    }
  }
  cout << "# Loaded " << GetNumberOfTraces() << " Traces " << endl;
  return 1;
}
//-------------------------------------------------------------------//
unsigned long FinTailEvent::GetNumberOfTraces(){
  return fTraces.size();
}
//-------------------------------------------------------------------//
FinTailTraceProcessing* FinTailEvent::GetTrace(long aTraceNumber){
  if ((aTraceNumber < 0) || (aTraceNumber >= GetNumberOfTraces()))
	return NULL;

  return &(fTraces[aTraceNumber]);
}
//-------------------------------------------------------------------//
double FinTailEvent::GetMaxSignal(){
  return fMaxSignal;
}
//-------------------------------------------------------------------//
double FinTailEvent::GetMaxSignalPos(){
  return fMaxSignalPos;
}
//-------------------------------------------------------------------//
double FinTailEvent::GetMaxSignalNeg(){
  return fMaxSignalNeg;
}
//-------------------------------------------------------------------//
double FinTailEvent::GetMinSignal(){
  return fMinSignal;
}
//-------------------------------------------------------------------//
double FinTailEvent::GetMinSignalPos(){
  return fMinSignalPos;
}
//-------------------------------------------------------------------//
double FinTailEvent::GetMinSignalNeg(){
  return fMinSignalNeg;
}
//-------------------------------------------------------------------//
