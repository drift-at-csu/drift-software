#ifndef _FinTailFEResponse_h_
#define _FinTailFEResponse_h_

#include <map>
#include <string>

namespace FinTail {

class FinTailFEResponse {
  public:
	FinTailFEResponse();
	~FinTailFEResponse();

/* These will load data from file */
	int Load(char *aFileName);

  private:
	std::map<double, double>  fTrace;

};

}
#endif /* _FinTailFEResponse_h_ */
