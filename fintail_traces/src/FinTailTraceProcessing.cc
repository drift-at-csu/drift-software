#include "FinTailTraceProcessing.h"
#include "FinTailTrace.h"
using namespace FinTail;

#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <iostream> // for cout
#include <sstream> // for string streams
using namespace std;

#include <TH1D.h>
#include <TMath.h>

//-------------------------------------------------------------------//
FinTailTraceProcessing::FinTailTraceProcessing(FinTailTrace *aTrace):
			fTrace(aTrace), fTraceSample(NULL), 
			fMaximum(0.0), fMaximumBin(-1),
			fMinimum(0.0), fMinimumBin(-1),
			fSignalStartBin(-1), fSignalIntegrationBins(-1),
			fSignalBefore(0.0), fSignalBeforeRMS(0.0),
			fSignalBeforeBins(-1), fSignalAfter(0.0),
			fSignalAfterRMS(0.0), fSignalAfterBins(-1),
			fJumpAmplitude(0.0) {
}
//-------------------------------------------------------------------//
FinTailTraceProcessing::~FinTailTraceProcessing() {
  if (fTraceSample) delete (fTraceSample);
}
//-------------------------------------------------------------------//
int FinTailTraceProcessing::FindMinMax(long start_bin, long stop_bin) {
  if (fTrace == NULL) return 0;

  if (start_bin < 0)
	start_bin = 0;
  if (stop_bin > fTrace->GetNofBins())
	stop_bin = fTrace->GetNofBins();

  long nofbins = stop_bin - start_bin;
  if (nofbins <= 0) return -1;

  fMaximum = fTrace->Get(start_bin);
  fMaximumBin = start_bin;
  fMinimum = fTrace->Get(start_bin);
  fMinimumBin = start_bin;

  for(int ii=start_bin;ii < stop_bin;ii++){
	if (fTrace->Get(ii) > fMaximum ) {
		fMaximum = fTrace->Get(ii);
		fMaximumBin = ii;
	}
	if (fTrace->Get(ii) < fMinimum ) {
		fMinimum = fTrace->Get(ii);
		fMinimumBin = ii;
	}
  }
  return 1;
}
//-------------------------------------------------------------------//
int FinTailTraceProcessing::FindLongestPositiveJumpPosition(
	long start_bin, long stop_bin, long average_over_bins){

  if (fTrace == NULL) return 0;

  if (start_bin < 0)
	start_bin = 0;
  if (stop_bin > fTrace->GetNofBins())
	stop_bin = fTrace->GetNofBins();

  long nofbins = (stop_bin - start_bin)/average_over_bins;
  if (nofbins < 1) return -1;

  fSignalStartBin = -1;
  fJumpAmplitude = 0.0;
  double accumul_signal = 0.0;
  double prev_accumul_signal = 0.0;
  for(long ii=0;ii < nofbins; ++ii){
	accumul_signal = 0.0;
	for (long jj = 0; jj < average_over_bins; ++jj) {
		accumul_signal += fTrace->Get(start_bin + ii*average_over_bins + jj);
	}
	accumul_signal = (double) accumul_signal/average_over_bins;
	if (ii == 0) {
		// First pass, just get the accumulated signal
		// prev_accumul_signal = accumul_signal;
	} else {
		if ((accumul_signal - prev_accumul_signal) > fJumpAmplitude) {
			fJumpAmplitude = accumul_signal - prev_accumul_signal;
			fSignalStartBin = start_bin + ii*average_over_bins;
		}
	}
	prev_accumul_signal = accumul_signal;

  }
  fSignalIntegrationBins = 500; // 6usec
  // fSignalStartBin -= 50; // Electonics integration time + risetime 1usec

  // cout << "...Longest Jump=" << fJumpAmplitude << " at bin " << fSignalStartBin << endl;

  return 1;
}
//-------------------------------------------------------------------//
int FinTailTraceProcessing::FindBeforePedestal(long number_of_bins){
  if (fTrace == NULL) return 0;
  long start_bin = fSignalStartBin - number_of_bins;
  long stop_bin = fSignalStartBin;


  if (start_bin < 0)
  	start_bin = 0;
  if (stop_bin > fTrace->GetNofBins())
	stop_bin = fTrace->GetNofBins();

  long nofbins = (stop_bin - start_bin);
  if (nofbins < 1) return -1;

  fSignalBefore = 0.0;
  for(long ii=0;ii < nofbins; ++ii){
	fSignalBefore += fTrace->Get(ii+start_bin);
  }
  fSignalBefore = fSignalBefore / nofbins;
  return 1;
}
//-------------------------------------------------------------------//
int FinTailTraceProcessing::FindAfterPedestal(long number_of_bins){
  if (fTrace == NULL) return 0;
  long start_bin = fSignalStartBin + fSignalIntegrationBins;
  long stop_bin = fSignalStartBin + fSignalIntegrationBins + number_of_bins;


  if (start_bin < 0)
	start_bin = 0;
  if (stop_bin > fTrace->GetNofBins())
	stop_bin = fTrace->GetNofBins();

  long nofbins = (stop_bin - start_bin);
  if (nofbins < 1) return -1;

  fSignalAfter = 0.0;
  for(long ii=0;ii < nofbins; ++ii){
	fSignalAfter += fTrace->Get(ii+start_bin);
  }
  fSignalAfter = fSignalAfter / nofbins;
  return 1;
}
//-------------------------------------------------------------------//
int FinTailTraceProcessing::FindSignal(){
  if (fTrace == NULL) return 0;
  long start_bin = fSignalStartBin;
  long stop_bin = fSignalStartBin + fSignalIntegrationBins;

  if (start_bin < 0)
	start_bin = 0;
  if (stop_bin > fTrace->GetNofBins())
	stop_bin = fTrace->GetNofBins();

  long nofbins = (stop_bin - start_bin);
  if (nofbins < 1) return -1;

  double signal_correction_per_bin = (fSignalAfter - fSignalBefore)/((double)nofbins);
  // cout << "...Signal Correction = " << signal_correction_per_bin << endl;
  fSignal = 0.0;

  for(long ii=0; ii < nofbins; ++ii){
          fSignal += fTrace->Get(ii+start_bin);
	  fSignal -= fSignalBefore;
	  fSignal -= signal_correction_per_bin;
  }

  return 1;
}
//-------------------------------------------------------------------//
TH1D* FinTailTraceProcessing::GetTraceSample() {

  
  if (fTraceSample != NULL) {
	return fTraceSample;
  }
  if (fTrace == NULL) {
	return NULL;
  }

  ostringstream histo_name;
  histo_name.clear();
  histo_name << "hsamp_" << fTrace->GetTraceId();

  ostringstream histo_title;
  histo_title.clear();
  histo_title << " Trace " << fTrace->GetTraceId();

  fTraceSample = new TH1D(
  		histo_name.str().c_str(), 
  		histo_title.str().c_str(),
		fTrace->GetNofBins(),
		( ( fTrace->GetTraceOffset() )* fTrace->GetBinWidth() ) - 0.5, 
		( ( fTrace->GetTraceOffset() + fTrace->GetNofBins() )* 
			fTrace->GetBinWidth() ) - 0.5);

  if (fTraceSample == NULL) {
	return NULL;
  }

  fTraceSample->SetBinContent(0, 0.0);
  for(Int_t ii=0;ii < fTrace->GetNofBins(); ii++){
	fTraceSample->SetBinContent( ii+1,  fTrace->Get(ii) - fTrace->GetPedestal() );
  }
  // fTraceSample->SetBinContent(fTrace->GetNofBins(), 0.0);

  return fTraceSample;
}
//-------------------------------------------------------------------//
double FinTailTraceProcessing::GetMaximum() {
  return fMaximum;
}
//-------------------------------------------------------------------//
long FinTailTraceProcessing::GetMaximumBin() {
  return fMaximumBin;
}
//-------------------------------------------------------------------//
double FinTailTraceProcessing::GetMinimum() {
  return fMinimum;
}
//-------------------------------------------------------------------//
long FinTailTraceProcessing::GetMinimumBin() {
  return fMinimumBin;
}
//-------------------------------------------------------------------//
long FinTailTraceProcessing::GetSignalStartBin(){
  return fSignalStartBin;
}
//-------------------------------------------------------------------//
double FinTailTraceProcessing::GetAmplitude(){
  return fJumpAmplitude;
}
//-------------------------------------------------------------------//
double FinTailTraceProcessing::GetSignal(){
  return fSignal;
}
//-------------------------------------------------------------------//
double FinTailTraceProcessing::GetSignalBefore(){
  return fSignalBefore;
}
//-------------------------------------------------------------------//
double FinTailTraceProcessing::GetSignalAfter(){
  return fSignalAfter;
}
//-------------------------------------------------------------------//
