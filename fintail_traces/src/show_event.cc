#include <TROOT.h>
#include <TStyle.h>
#include <TApplication.h>
#include <TCanvas.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TColor.h>

#include <stdio.h>
#include <iostream>
using namespace std;

#include "FinTailEvent.h"
#include "FinTailEventProcessing.h"
using namespace FinTail;

int main (int argc, char *argv[]) {

  TApplication theApp ("App", NULL, 0);

  if ( argc != 2 ) { // need to provide a path
	cerr << "Usage: " << argv[0] << " <path_to_event>" << endl;
	return 1;
  }
  gROOT->Reset();
  gStyle->SetOptStat(kFALSE);
  gStyle->SetFrameBorderMode(0);
  gROOT->ForceStyle();
  gStyle->SetPalette(1);

  Double_t Pal_Red[5]    = { 0.00, 0.00, 0.87, 1.00, 0.51 };
  Double_t Pal_Green[5]  = { 0.00, 0.81, 1.00, 0.20, 0.00 };
  Double_t Pal_Blue[5]   = { 0.51, 1.00, 0.12, 0.00, 0.00 };
  Double_t Pal_Length[5] = { 0.00, 0.34, 0.61, 0.84, 1.00 };
  Int_t Pal_nb = 100;
  TColor::CreateGradientColorTable(5,Pal_Length,
		Pal_Red,Pal_Green,Pal_Blue,Pal_nb);
  gStyle->SetNumberContours(Pal_nb);

  TCanvas myc("myc", "FinTail Display", 800, 600);
  myc.SetFillColor(kWhite);
  myc.cd();
  myc.Draw();
  myc.SetBorderMode(0);

  TPad canv1("canv1", "Trace Pad",0.01,0.01,0.49,0.99);
  canv1.Draw();
  canv1.SetFillColor(kWhite);
  canv1.SetFrameFillColor(kWhite);
  canv1.SetBorderMode(0);
  // canv1.SetLogx();
  canv1.SetLogy();
  canv1.SetGridx();
  canv1.SetGridy();

  TPad canv2("canv2", "Trace Pad",0.51,0.01,0.99,0.99);
  canv2.Draw();
  canv2.SetFillColor(kWhite);
  canv2.SetFrameFillColor(kWhite);
  canv2.SetBorderMode(0);
  // canv2.SetLogy();
  // canv2.SetLogz();
  canv2.SetGridx();
  canv2.SetGridy();
  
  FinTailEvent event;
  if ( event.Load(argv[1], eADC2) <= 0) {
	cout << "error reading directory" << endl;
	return 0;
  }

  FinTailEventProcessing evnt_proc(&event);
  evnt_proc.CreateSignalDistro(500);
  evnt_proc.CreateSignal2DDistro(500,500);
  

  canv1.cd();
  if (evnt_proc.isThereSignalDistro()) {
	evnt_proc.GetSignalDistro()->DrawCopy();
	// let's print out bins containts here
	for (unsigned long ii = 0; ii < evnt_proc.GetSignalDistro()->GetNbinsX(); ++ii){
		cout << evnt_proc.GetSignalDistro()->GetBinCenter(ii+1) << "\t" 
			<<  evnt_proc.GetSignalDistro()->GetBinContent(ii+1) << "\t";

		if (evnt_proc.GetSignalDistro()->GetBinContent(ii+1) > 0 ){
			cout << evnt_proc.GetAftershootDistro()->GetBinContent(ii+1);
		} else {
			cout << "?";
		}
		cout << endl;
	}
  }


  canv2.cd();
  /*
  if (evnt_proc.isThereSignal2DDistro()) {
  	evnt_proc.GetSignal2DDistro()->DrawCopy("COLZ");
  }
  */
  if (evnt_proc.isThereSignalDistro()) {
  	evnt_proc.GetAftershootDistro()->DrawCopy();
  }

  theApp.Run ();
  return 0;
}
