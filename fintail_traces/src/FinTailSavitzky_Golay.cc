#include <math.h>
#include <stdio.h>
#include <iostream>
using namespace std;

#include <FinTailSavitzky_Golay.h>
using namespace FinTail;

//-------------------------------------------------------------------//
Savitzky_Golay::Savitzky_Golay():
		fSmoothPolyOrder(6), fMatrix(NULL), fPermutationVector(NULL), fLEVector(NULL),
		fDerivativeOrder(0), fNofPointsLeft(5), fNofPointsRight(5),
		fSeekShiftIndex(NULL), fFilterCoefficients(NULL), fNumberOfFilterCoeff(0),
		fOutputTrace(NULL), fNofTracePoints(0){

}
//-------------------------------------------------------------------//
Savitzky_Golay::~Savitzky_Golay(){
  cout << "SG: Destructor called" << endl;
  CleanMemoryForTraces();
  if (fFilterCoefficients != NULL) {
	delete [] fFilterCoefficients;
	fFilterCoefficients = NULL;
  }
  if (fSeekShiftIndex != NULL){
	delete [] fSeekShiftIndex;
	fSeekShiftIndex = NULL;
  }
  fNumberOfFilterCoeff = 0;
}
//-------------------------------------------------------------------//
double *Savitzky_Golay::GetOutputTraceArray(){
  return fOutputTrace;
}
//-------------------------------------------------------------------//
unsigned long Savitzky_Golay::GetNofTracePoints(){
  return (unsigned long) fNofTracePoints;
}
//-------------------------------------------------------------------//
int Savitzky_Golay::Filter(double *anInputTrace, unsigned long aNofPoints){
  int result;

  // let's check if the coefficients was calculated
  if ((fFilterCoefficients == NULL)||(fNumberOfFilterCoeff == 0)||fSeekShiftIndex == NULL) {
	cout << "...SG:Filter: Did you forget to calculate coeff first?" << endl;
	return 0;
  }
  if (aNofPoints < (unsigned long) fNofPointsRight + 1) {
	cout << "...SG:Filter: Can't do anything with this number of points:"
		<< aNofPoints << endl;
	return 0;
  }

  // Let's check if we do have the array allocated already
  result = 1;
  if (fOutputTrace != NULL) {
	if (fNofTracePoints != (aNofPoints - fNofPointsRight)) {
		CleanMemoryForTraces();
		result = AllocateMemoryForTraces(aNofPoints - fNofPointsRight);
	}
  } else {
	result = AllocateMemoryForTraces(aNofPoints - fNofPointsRight);
  }
  if (!result) {
	cout << "...SG:Filter: Can't Allocate memory for the trace" << endl;
	return 0;
  }


  // Apply filter to input data.
  for (long ii = 0; ii < fNofTracePoints; ++ii) {
	fOutputTrace[ii]=0.0;
	for (unsigned long jj = 0; jj < (unsigned long) fNofPointsLeft + fNofPointsRight + 1; ++jj)
		if ( ii + fSeekShiftIndex[jj] >= 0)   //skip left points that do not exist.
			fOutputTrace[ii] += fFilterCoefficients[jj]*
				anInputTrace[ii+fSeekShiftIndex[jj]];
  }

  return 1;
}
//-------------------------------------------------------------------//
int Savitzky_Golay::CalculateCoefficients( unsigned long nl, unsigned long nr, 
		unsigned short ld, unsigned short m){
  long mm;

  if ( fNumberOfFilterCoeff != (nl + nr + 1) ) {
	// This means we want to clear prev coefficients first
	if (fFilterCoefficients != NULL) {
		delete [] fFilterCoefficients; fFilterCoefficients = NULL;
	}
	if (fSeekShiftIndex != NULL){
		delete [] fSeekShiftIndex; fSeekShiftIndex = NULL;
	}
	fNumberOfFilterCoeff = nl + nr + 1;

	fFilterCoefficients = new (nothrow) double [fNumberOfFilterCoeff];
	if (fFilterCoefficients == NULL) {
		cout << "...Can't allocate memory for fFilterCoefficients" << endl;
		fNumberOfFilterCoeff = 0;
		return 0;
	}
	fSeekShiftIndex = new (nothrow) short [fNumberOfFilterCoeff];
	if (fSeekShiftIndex == NULL) {
		cout << "...Can't allocate memory for fSeekShiftIndex" << endl;
		delete [] fFilterCoefficients; fFilterCoefficients = NULL;
		fNumberOfFilterCoeff = 0;
		return 0;
	}
  } else {
	// We have the coefficients already... so let's do nothing
	cout << "...We do have memory for coefficients already" << endl;
  }
  // Let's allocate Matrix and B vector Memory, which we
  // will need to free at the end of this function
  fSmoothPolyOrder = m + 1;
  fMatrix = new (nothrow) double [fSmoothPolyOrder*fSmoothPolyOrder];
  if ( fMatrix == NULL) {
	cout << "...Can't allocate memory for fMatrix" << endl;
	return 0;
  }
  fPermutationVector = new (nothrow) unsigned short [fSmoothPolyOrder];
  if (fPermutationVector == NULL) {
  	delete [] fMatrix; fMatrix = NULL;
	cout << "...Can't allocate memory for fPermutationVector" << endl;
	return 0;
  }
  fLEVector = new (nothrow) double [fSmoothPolyOrder];
  if (fLEVector == NULL) {
	delete [] fMatrix; fMatrix = NULL;
	delete [] fPermutationVector; fPermutationVector = NULL;
	cout << "...Can't allocate memory for fLEVector" << endl;
	return 0;
  }


  // Let's zero everything
  // cout << "...SG: Zero Matrix, resulting vector and permutation vectors" << endl;
  for (unsigned short ii = 0; ii < fSmoothPolyOrder; ++ii) {
	for (unsigned short jj = 0; jj < fSmoothPolyOrder; jj++) 
		fMatrix[ii*fSmoothPolyOrder + jj]=0.0;
	fLEVector[ii]=0.0;
	fPermutationVector[ii] = 0;
  }
  fNofPointsRight = nr;
  fNofPointsLeft = nl;


  for (unsigned long ipj = 0; ipj <= 2*(fSmoothPolyOrder-1); ipj++) { 
  	//Set up the normal equations of the desired leastsquares fit.
	double sum=0.0;
	if (ipj==0) 
		sum=1.0;
	for (long k=1; k<= (long)fNofPointsRight; k++) 
		sum += (double) pow( k, ipj);
	for (long k=1; k<= (long)fNofPointsLeft; k++) 
		sum += (double) pow(-k, ipj);

	if ( ipj <= (fSmoothPolyOrder-1) ) 
		mm = ipj;
	else
		mm = 2*(fSmoothPolyOrder-1) - ipj;
	
	long imj=-mm;
	do {
		// cout << "SG:Filling Matrix at [" << (ipj + imj)/2 << "," << (ipj-imj)/2 
		//	<< "] with " << sum << endl;
		fMatrix[((ipj + imj)/2 *fSmoothPolyOrder) + ((ipj-imj)/2)] = sum;
		imj += 2;	
	} while (imj<=mm); 
  }

  //Solve them: LU decomposition
  // cout << "SG: Calling LUDecomposition" << endl;
  LUDecomposition();

  // cout << "SG: Zeroing out fLEVector" << endl;
  for (unsigned short jj = 0; jj < fSmoothPolyOrder; jj++) 
	fLEVector[jj]=0.0;
  //Right-hand side vector is unit vector, depending on which derivative we want.
  fLEVector[fDerivativeOrder]=1.0;

  //Backsubstitute, giving one row of the inverse matrix.
  // cout << "SG: Calling LUBackSubstitute" << endl;
  LUBackSubstitute();

  // Zero the output array (it may be bigger than the number
  // of coefficients.
  // cout << "SG: Zeroing " << fNumberOfFilterCoeff << " Filter Coefficients" << endl;
  for (unsigned long kk = 0; kk < (unsigned long) fNumberOfFilterCoeff; kk++)
	fFilterCoefficients[kk]=0.0;

  // Each Savitzky-Golay coefficient is the dot product
  // of powers of an integer with the inverse matrix row.
  // cout << "SG: Trying to calculate coefficients" << endl;
  for (long kk=(long)-fNofPointsLeft; kk <= fNofPointsRight; kk++) {
	double sum = fLEVector[0];
	double fac=1.0;
  	for (unsigned short mm = 0; mm < fSmoothPolyOrder - 1; mm++) {
		fac *= kk;
		sum += fLEVector[mm + 1]*fac;
	} 
	//Store in wrap-around order
	long k_index = (((long) fNumberOfFilterCoeff - kk) % fNumberOfFilterCoeff);
	// cout << "SG: Setting filter coefficient [" << k_index << "] to " << sum << endl << flush;
	fFilterCoefficients[k_index]=sum;
  }
  // cout << "SG: Done with coefficients calculations" << endl;

  // As promised let's not forget to free all the memory that we don't need
  delete [] fMatrix; fMatrix = NULL;
  delete [] fPermutationVector; fPermutationVector = NULL;
  delete [] fLEVector; fLEVector = NULL;

  // Finally let's setup SeekShift Vector
  
  fSeekShiftIndex[0]=0;
  // example: case nl=nr=5
  // index(2)=-1; index(3)=-2; index(4)=-3; index(5)=-4; index(6)=-5
  long jj = 2;
  for (long ii = 1; ii < fNofPointsLeft + 1; ++ii) {
	fSeekShiftIndex[ii] = ii-jj;
	// cout << "...SG: fSeekShiftIndex[" << ii << "]=" << fSeekShiftIndex[ii] << endl;
	jj += 2;
  }
  // index(7)= 5; index(8)= 4; index(9)= 3; index(10)=2; index(11)=1
  jj = 1;
  for (long ii = fNofPointsLeft + 1; ii < fNofPointsLeft+fNofPointsRight+1; ++ii) {
	fSeekShiftIndex[ii] = ii-jj;
	// cout << "...SG: fSeekShiftIndex[" << ii << "]=" << fSeekShiftIndex[ii] << endl;
	jj += 2;
  }

  return 1; 
}
//-------------------------------------------------------------------//
int Savitzky_Golay::LUDecomposition(){

  if ((fSmoothPolyOrder == 0) || (fMatrix == NULL)){
		cout << "...SG::LUDecomposition: Forgot to define something?" << endl;
		return 0;
  }

  double matrix_max,DUM,SUM;
  double VV[fSmoothPolyOrder];
  unsigned short ii_max;
  char D=1;
  double TINY= (double) 1e-12;

  // cycle on rows
  for (unsigned short ii = 0; ii < fSmoothPolyOrder; ++ii) {
	matrix_max=0.0;
	for (unsigned short jj = 0; jj < fSmoothPolyOrder; ++jj)
		if (fabs(fMatrix[ii*fSmoothPolyOrder + jj]) > matrix_max) 
			matrix_max = (double) fabs(fMatrix[ii*fSmoothPolyOrder + jj]);
	if (matrix_max < TINY) {
		// Matrix maximum is singular, so do nothing
		return 0;
	}
	VV[ii] = (double) 1.0 / matrix_max;
  }

  for (unsigned short jj = 0; jj < fSmoothPolyOrder; ++jj) {
	for (unsigned short ii = 0; ii < jj; ++ii) {
		SUM = fMatrix[ii*fSmoothPolyOrder + jj];
		for (unsigned short kk = 0; kk < ii; ++kk)
			SUM -= fMatrix[ii*fSmoothPolyOrder + kk]*fMatrix[kk*fSmoothPolyOrder + jj];
		fMatrix[ii*fSmoothPolyOrder + jj] = SUM;
	}
	matrix_max = 0.0;
	for (unsigned short ii = jj; ii < fSmoothPolyOrder; ++ii) {
		SUM = fMatrix[ii*fSmoothPolyOrder + jj];
		for (unsigned short kk = 0; kk < jj; ++kk)
			SUM -= fMatrix[ii*fSmoothPolyOrder + kk]*fMatrix[kk*fSmoothPolyOrder + jj];
		fMatrix[ii*fSmoothPolyOrder + jj] = SUM;
		DUM = VV[ii]*(double)fabs(SUM);
		// Recalculating the matrix maximum (along with max bin position)
		if (DUM >= matrix_max) {
			ii_max = ii;
			matrix_max = DUM;
		}
	}

	if (jj != ii_max) {
		for (unsigned short kk =0; kk < fSmoothPolyOrder; ++kk) {
			DUM = fMatrix[ii_max*fSmoothPolyOrder + kk];
			fMatrix[ii_max*fSmoothPolyOrder + kk] = fMatrix[jj*fSmoothPolyOrder + kk];
			fMatrix[jj*fSmoothPolyOrder + kk] = DUM;

		}
		D = -D;
		VV[ii_max] = VV[jj];
	}
	fPermutationVector[jj] = ii_max;
	if ((double)fabs(fMatrix[jj*fSmoothPolyOrder + jj]) < TINY)  
		fMatrix[jj*fSmoothPolyOrder + jj] = TINY;
	if ( jj != (fSmoothPolyOrder -1) ) {
		DUM = (double)1.0 / fMatrix[jj*fSmoothPolyOrder + jj];
		for (unsigned short ii = jj + 1; ii < fSmoothPolyOrder; ++ii)  
			fMatrix[ii*fSmoothPolyOrder + jj] *= DUM;
	} 
  }
  
  return 1;
}
//-------------------------------------------------------------------//
void Savitzky_Golay::LUBackSubstitute(){
  // Let's check if everything is O.K with vectors
  if ((fSmoothPolyOrder == 0)||(fPermutationVector == NULL)||
  		(fMatrix == NULL) || (fLEVector == NULL)){
	cout << "...SG::LUBackSubstitute: Forgot to define Vectors?" << endl;
  	return;
  }

  double SUM;
  unsigned short kk;
  unsigned short next_ii = 0;

  for (unsigned short ii = 0; ii < fSmoothPolyOrder; ++ii) {
	kk = fPermutationVector[ii];
	SUM = fLEVector[kk];
	fLEVector[kk] = fLEVector[ii];
	if (next_ii != 0)
		for (unsigned short jj = next_ii - 1; jj < ii; ++jj)
			SUM -= fMatrix[ii*fSmoothPolyOrder + jj]*fLEVector[jj];
	else if (SUM != 0.0)
		next_ii = ii + 1;
		fLEVector[ii] = SUM;
  }
  for (unsigned short ii = fSmoothPolyOrder; ii > 0; ii--) {
	SUM = fLEVector[ii-1];
	if (ii < fSmoothPolyOrder)
		for (unsigned short jj=ii; jj < fSmoothPolyOrder; ++jj)
			SUM -= fMatrix[(ii-1)*fSmoothPolyOrder + jj]*fLEVector[jj];
	fLEVector[ii-1] = SUM / fMatrix[(ii-1)*fSmoothPolyOrder + ii-1];
  }
}
//-------------------------------------------------------------------//
int Savitzky_Golay::AllocateMemoryForTraces(unsigned long aNumberOfPoints){
  if ((fOutputTrace !=NULL)) {
	cout << "...Cleanup memory first before allocation" << endl;
	return 0;
  }
  fNofTracePoints = 0;
  fOutputTrace = new (nothrow) double [aNumberOfPoints];
  if (fOutputTrace == NULL) {
	cout << "...Can't allocate memory for OutputTrace" << endl;
	return 0;
  }
  fNofTracePoints = aNumberOfPoints;
  return 1;
}
//-------------------------------------------------------------------//
void Savitzky_Golay::CleanMemoryForTraces(){
  if (fOutputTrace != NULL){
	delete [] fOutputTrace;
	fOutputTrace = NULL;
  }
  fNofTracePoints = 0;
}
//-------------------------------------------------------------------//
