#ifndef _FinTailTraceProcessing_h_
#define _FinTailTraceProcessing_h_

class TH1D;

namespace FinTail {

class FinTailTrace;

class FinTailTraceProcessing {
  public:
	FinTailTraceProcessing(FinTailTrace *aTrace);
	~FinTailTraceProcessing();

	/* Calculates trace maximum and corresponding trace bin 
	 * also Calculates trace minimum and corresponding trace bin 
	 * within start_bin and stop_bin */
	int FindMinMax(long start_bin, long stop_bin);

	/* Calculates the longest positive jump position , while
	 * doing it averages over the timebins, the result is then
	 * put to fSignalStartBin */
	int FindLongestPositiveJumpPosition(long start_bin, long stop_bin,
		long average_over_bins);
	
	int FindBeforePedestal(long number_of_bins);
	int FindAfterPedestal(long number_of_bins);

	int FindSignal();

	TH1D* GetTraceSample();

	double GetMaximum();
	long   GetMaximumBin();
	double GetMinimum();
	long   GetMinimumBin();
	long   GetSignalStartBin();
	double GetAmplitude();
	double GetSignal();
	double GetSignalBefore();
	double GetSignalAfter();
  private:

	FinTailTrace *fTrace;
	TH1D *fTraceSample;

	double fMaximum;
	long   fMaximumBin;

	double fMinimum;
	long   fMinimumBin;

	double  fSignal;
	long	fSignalStartBin;
	long	fSignalIntegrationBins;

	// Added the new jump paradigm here

	double fSignalBefore;
	double fSignalBeforeRMS;
	long fSignalBeforeBins;

	double fSignalAfter;
	double fSignalAfterRMS;
	long fSignalAfterBins;

	double fJumpAmplitude;

  friend class FinTailEvent;
};

}
#endif /* _FinTailTraceProcessing_h_ */
