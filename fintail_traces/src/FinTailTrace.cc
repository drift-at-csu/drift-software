#include <stdio.h>
#include <stdlib.h>
#include <iostream> // for cout
#include <fstream>
using namespace std;

#include <TClass.h>
#include <TStreamerInfo.h>

#include <my_global.h>
#include <mysql.h>

#include "FinTailTrace.h"
using namespace FinTail;

//-------------------------------------------------------------------//
FinTailTrace::FinTailTrace() :
		fTraceStart(0.0), fBinWidth(0.0),
		fTraceId(-1), fTraceSource(eUnknown),
		fTraceOffset(0), 
		fTraceQuality(eBad),
		fTracePedestal(0.0) {
  fTrace.clear();
  fFileSource.clear();
}
//-------------------------------------------------------------------//
FinTailTrace::~FinTailTrace() {
  fTrace.clear();
  fFileSource.clear();
}
//-------------------------------------------------------------------//
int FinTailTrace::Load(char *aFileName) {

  FILE * pFile;
  unsigned long lSize;
  unsigned short * buffer;
  size_t read_result;
  long ii;
  unsigned short adc_source;

  long trace_start_bin;
  long trace_stop_bin;
  unsigned short trace_header[6];
  bool id_mod = false; // This is indicator if the Id of the
  			// event should be modyfied due to the broken header

  pFile = fopen ( aFileName, "rb" );
  if (pFile==NULL) {
  	return 0;
  }
  // obtain file size:
  fseek (pFile , 0 , SEEK_END);
  lSize = ftell (pFile);
  rewind (pFile);

  // allocate memory to contain the whole file:
  const int buffer_size = lSize/sizeof(unsigned short);
  buffer = (unsigned short*) malloc (lSize);
  if (buffer == NULL) {
	// No memory
	fclose(pFile);
	return -1;
  }

  read_result = fread (buffer, 1, lSize, pFile);
  if (read_result != lSize) {
  	// Read Error
	free (buffer);
	fclose(pFile);
	return -2;
  }
  
  // If we got here than the file should be in buffer
  fclose(pFile);


  // cout << "...File Size = " << lSize << endl;

  fTraceStart = 0.0;
  fBinWidth = 10.0; /* ns */

  fTraceQuality = eGood;

  trace_start_bin = CalculateTraceBreak(buffer, 0, buffer_size);

  if (trace_start_bin < (buffer_size - (buffer_size /2)) ) {
	// cout << "...Scenario # 1 " << endl;

  	// This scenario is when the trace begining
	// alone with the header is to the RIGHT of
	// breaking line
	fTraceOffset = 0;

	for (ii = 0; ii < 5; ++ii)
		trace_header[ii] = buffer[trace_start_bin + ii];

	trace_stop_bin = buffer_size;
	trace_start_bin = trace_start_bin + ii;
  } else {
	// This scenario is when the trace we're interested
	// in is to the LEFT of breaking line,
	// but the header is either to the right, or splited

	// cout << "...Scenario # 2 " << endl;
	
	for (ii = 0; ii < 5; ++ii)
		trace_header[ii] = buffer[(trace_start_bin + ii)%buffer_size];

	trace_stop_bin = trace_start_bin;
	if ( trace_start_bin >  buffer_size - ii ) {
		// The trace is still the whole thing,
		// The Header is broken
		trace_start_bin = trace_start_bin - buffer_size;
		fTraceOffset = 0;
	} else {
		// The header is completely lying to the right
		trace_start_bin = 0;
		if (trace_stop_bin < buffer_size )
			fTraceQuality = eBroken;
		fTraceOffset = buffer_size - trace_stop_bin ;
	}
  }
  adc_source = buffer[trace_start_bin] >> 15;

  if (adc_source == 0)
	fTraceSource = eADC1;
  else
	fTraceSource = eADC2;

  fTraceId =
	  (trace_header[2] & 0x7FFF)
	+ (trace_header[3] & 0x7FFF)
	+ (trace_header[4] & 0x7FFF)
	+ (trace_header[5] & 0x7FFF);
//	cout << fTraceId << endl;

  fTrace.clear();
  short is_trace_bad = 0;
  for(ii=trace_start_bin; ii < trace_stop_bin; ii++){
  	if (adc_source != (buffer[ii] >> 15))
		is_trace_bad = 1;
	fTrace.push_back( (double) ((unsigned short) buffer[ii] & 0xFFF) );
  }
 
  if (is_trace_bad)
	fTraceQuality = eBad;

  fFileSource = aFileName;
  fTracePedestal = 0.0;

  // let's free allocated memory
  free (buffer);
  return 1;
}
//-------------------------------------------------------------------//
long   FinTailTrace::GetNofBins() {
  return (long) fTrace.size();
}
//-------------------------------------------------------------------//
long   FinTailTrace::GetTraceId() {
  return fTraceId;
}
//-------------------------------------------------------------------//
double FinTailTrace::Get(long aBinNumber) {
  return fTrace[aBinNumber];
}
//-------------------------------------------------------------------//
TraceSource FinTailTrace::GetSource(){
  return fTraceSource;
}
//-------------------------------------------------------------------//
TraceQuality FinTailTrace::GetQuality(){
  return fTraceQuality;
}
//-------------------------------------------------------------------//
long FinTailTrace::GetTraceOffset() {
  return fTraceOffset;
}
//-------------------------------------------------------------------//
double FinTailTrace::GetTraceStart(){
  return fTraceStart;
}
//-------------------------------------------------------------------//
char *FinTailTrace::GetFileSource() {
  return (char *) fFileSource.c_str();
}
//-------------------------------------------------------------------//
double FinTailTrace::GetBinWidth() {
  return fBinWidth;
}
//-------------------------------------------------------------------//
double FinTailTrace::GetPedestal() {
  return fTracePedestal;
}
//-------------------------------------------------------------------//
void FinTailTrace::PrintInfo() {
  cout << "...Trace ID " << GetTraceId() << " with " << GetNofBins() 
  	<< " bins and offset = " << GetTraceOffset() << " Bin width is "
	<< GetBinWidth() << " ns " << " Starting at " <<
	GetTraceStart() <<" ns "<< endl;
  cout << "   Taken from \"" << GetFileSource() << "\"" << endl;
  cout << "   Pedestal =" << GetPedestal();
  cout << ", Source is " ;
  switch (fTraceSource) {
    case eADC1 :
  	cout << " ADC 1 ";
  	break;
    case eADC2 :
    	cout << " ADC 2 ";
	break;
    case eUnknown:
    	cout << " Unknown ";
	break;
    default:
    	cout << " NEVERMIND ";
  	break;
  }
  cout << " and ";
  switch (fTraceQuality) {
    case eGood:
    	cout << " Good ";
	break;
    case eBad:
    	cout << " Bad ";
	break;
    case eNoisy:
    	cout << " Noisy ";
	break;
    case eBroken:
    	cout << " Broken ";
	break;
    default:
    	cout << " NEVERMIND ";
	break;
  }
  cout << " Quality " << endl;
}
//-------------------------------------------------------------------//
long FinTailTrace::CalculateTraceBreak(unsigned short *buffer,
		long start_bin, long stop_bin) {

  long result = 0;
  long ii;
  unsigned char adc_source;

  adc_source = buffer[start_bin] >> 15;
  for(ii=start_bin;ii < stop_bin;ii++){
	if ( adc_source != (buffer[ii] >> 15) ) {
		result = ii;
		break;
	}
  }
  return result;
}
//-------------------------------------------------------------------//
