#ifndef _FinTailTrace_h_
#define _FinTailTrace_h_

#include <vector>
#include <string>

namespace FinTail {

enum TraceSource {
	eADC1,
	eADC2,
	eUnknown
};

enum TraceQuality {
	eGood,
	eBad,
	eNoisy,
	eBroken, // Can be from badly damaged DMA transfer
};

class FinTailTrace {
  public:
	FinTailTrace();
	~FinTailTrace();

/* These will load data from files */
	int Load(char *aFileName);

/* Get Operators */
	long   GetNofBins();
	long   GetTraceId();
	double Get(long aBinNumber); // Returns the value of Bin
		// aBinNumber starts from 0
	TraceSource GetSource();
	TraceQuality GetQuality();

	long GetTraceOffset();
	double GetTraceStart();
	char *GetFileSource();
	double GetBinWidth();
	double GetPedestal();

	void PrintInfo();


  private:
  	/* returns bin number where the first brake happens
	 * if there were no break within the bins range
	 * returns 0
	 */
	long CalculateTraceBreak(unsigned short *buffer, 
		long start_bin, long stop_bin);

  	double fTraceStart; /* Time stamp of the begining of the trace */
	double fBinWidth; /* Width of bin in ns */
	long fTraceId; /* This is a unique Id of the trace */
	TraceSource fTraceSource; /* Where this trace came from */
	std::vector <double> fTrace; /* Trace itself */
	long fTraceOffset; /* Number of bins that trace is offset */
	TraceQuality fTraceQuality; /* Quality of the trace */
	std::string fFileSource; /* Full path to the Source file */
	double fTracePedestal; /* This is an offset of the trace */

  friend class FinTailTraceProcessing;
};

}
#endif /* _FinTailTrace_h_ */
