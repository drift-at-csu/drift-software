#include <TROOT.h>
#include <TStyle.h>
#include <TApplication.h>
#include <TCanvas.h>
#include <TH1D.h>

#include <stdio.h>
#include <iostream>
using namespace std;

#include "FinTailTraceProcessing.h"
#include "FinTailTrace.h"
#include "FinTailACcoupling.h"
#include "FinTailSavitzky_Golay.h"
using namespace FinTail;

int main (int argc, char *argv[]) {

  TApplication theApp ("App", NULL, 0);

  if ( argc != 2 ) { // need to provide a filename
	cerr << "Usage: " << argv[0] << " <filename>" << endl;
	return 1;
  }
  gROOT->Reset();
  gStyle->SetOptStat(kFALSE);
  gStyle->SetFrameBorderMode(0);
  gROOT->ForceStyle();

  TCanvas myc("myc", "FinTail Display", 800, 600);
  myc.SetFillColor(kWhite);
  myc.cd();
  myc.SetBorderMode(0);
  myc.Draw();

  TPad canv1("canv1", "Trace Pad",0.01,0.01,0.99,0.99);
  canv1.SetBorderMode(0);
  canv1.Draw();
  canv1.SetFillColor(kWhite);
  canv1.SetFrameFillColor(kWhite);
  canv1.SetGridx();
  canv1.SetGridy();
  
  FinTailTrace adc_trace;
  if ( adc_trace.Load(argv[1]) <= 0) {
	cout << "error reading file" << endl;
	return 0;
  }


  FinTailTraceProcessing *trace_processor = new FinTailTraceProcessing(&adc_trace);


  // let's limit ourself to ~ 500 bins within our time window
  trace_processor->FindMinMax((adc_trace.GetNofBins()/4), 
  	adc_trace.GetNofBins() - (adc_trace.GetNofBins()/4) );

  // let's take certain amount of bins from the left (and right of minimum)
  /*
  trace_processor->FindPedestal( 
  	0,
	trace_processor->GetMaximumBin() - 500);

  trace_processor->FindPosSignalRegion(
	trace_processor->GetMaximumBin() - 500,
	adc_trace.GetNofBins() - (adc_trace.GetNofBins()/4) );

  trace_processor->FindSignal(
	trace_processor->GetSignalStartBin(),
 	trace_processor->GetSignalEndBin());
  */
  trace_processor->FindLongestPositiveJumpPosition(
  	(adc_trace.GetNofBins()/4), adc_trace.GetNofBins() - (adc_trace.GetNofBins()/4),
	100);
  trace_processor->FindBeforePedestal(500);
  trace_processor->FindAfterPedestal(500);
  trace_processor->FindSignal();

  canv1.cd();
  adc_trace.PrintInfo();

  TH1D *trace;
  trace = trace_processor->GetTraceSample();
  ACcoupling coupling;
  double cur_signal;
  double accum_signal;

  /*
  cout << "...Correcting for AC" << trace->GetNbinsX() << endl;
  for (unsigned long kk = 0; kk < 2; ++kk){
    if (kk == 0) {
    	coupling.GenerateTemplate(22.0, 0.010, 4096);
    } else {
    	coupling.GenerateTemplate(88.0, 0.010, 4096);
    }
    for (unsigned long ii = 0; ii < trace->GetNbinsX(); ++ii){
  	cur_signal = trace->GetBinContent(ii+1);
	for (unsigned long jj = ii; jj < trace->GetNbinsX(); ++jj) {
		accum_signal = trace->GetBinContent(jj+1);
		trace->SetBinContent(jj+1,accum_signal+
			coupling.GetTemplate( jj - ii)*cur_signal);
	}
    }
  }
  */
  


  trace->DrawCopy();
  // trace_processor->GetPositiveSignal()->DrawCopy("SAME E2");
  // trace_processor->GetNegativeSignal()->DrawCopy("SAME E2");

  // let's find out Savitzky-Golay filtered trace
  Savitzky_Golay sg_filter;
  sg_filter.CalculateCoefficients(
  	250,	// number of points to the left
	250,	// number of points to the right
	0,	// smoothness order
	4); 	//polynomial order

  sg_filter.Filter(trace->GetArray() + 1, trace->GetNbinsX());
  std::copy(sg_filter.GetOutputTraceArray(),
  	sg_filter.GetOutputTraceArray()+sg_filter.GetNofTracePoints(),
	trace->GetArray() + 1 );

  trace->SetLineColor(kRed-2);
  trace->SetLineWidth(3);
  trace->DrawCopy("SAME");

  cout << "...Processed Trace: " << endl;
  cout << "   Signal=" << trace_processor->GetSignal() 
		<< " at bin " << trace_processor->GetSignalStartBin() << endl;
  cout << "   Pedestal: Before=" << trace_processor->GetSignalBefore()
  		<< " After = " << trace_processor->GetSignalAfter() << endl;
  cout << "   Minimum = " << trace_processor->GetMinimum() 
  		<< " at bin " << trace_processor->GetMinimumBin() 
	<< ", Maximum = " << trace_processor->GetMaximum()
		<< " at bin " << trace_processor->GetMaximumBin() << endl;

  delete trace_processor;
  trace_processor = NULL;
  theApp.Run ();
  return 0;
}
