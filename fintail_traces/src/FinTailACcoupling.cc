#include <stdio.h>
#include <iostream>
#include <math.h>
using namespace std;

#include <FinTailACcoupling.h>
using namespace FinTail;

//-------------------------------------------------------------------//
ACcoupling::ACcoupling(){
}
//-------------------------------------------------------------------//
ACcoupling::~ACcoupling(){
  fTemplate.clear();
}
//-------------------------------------------------------------------//
void ACcoupling::GenerateTemplate(double anACcoupl, 
	double aBinWidth, unsigned long aNofBins){

  double result = 0;
  double current_bin = 0.0;
  double an_integral = 0;

  fTemplate.clear();
  cout << "...Creating AC=" << anACcoupl << " with Binwidth = " << aBinWidth << endl;
  for (unsigned long ii = 0; ii < aNofBins; ++ii){ 
	if (ii == 0 ) {
		result = (1 - exp(-current_bin/anACcoupl) );
	} else {
		result = (exp((aBinWidth-current_bin)/anACcoupl)
			- exp(-current_bin/anACcoupl) );
	}
	fTemplate.push_back(result);
	// cout << "...ACcoupling : bin[" << ii << "]=" << result << endl;
	current_bin += aBinWidth;
	an_integral += result;
  }
  cout << "...Resulting integral = " << an_integral << " (should be ~1)" << endl;
}
//-------------------------------------------------------------------//
double ACcoupling::GetTemplate(unsigned long aBinNumber){
  if (aBinNumber < fTemplate.size())
  	return fTemplate[aBinNumber];
  else
	return 0.0;
}
//-------------------------------------------------------------------//
