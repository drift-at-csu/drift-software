#ifndef _FinTailEvent_h_
#define _FinTailEvent_h_

#include <vector>
#include <string>

#include <FinTailTrace.h>
#include <FinTailTraceProcessing.h>

namespace FinTail {

enum EventSource {
	eEventSrcNeutronRun,
	eEventSrcBackground,
	eEventSrcUnknown
};

typedef std::vector <FinTailTraceProcessing>::iterator FinTailTraceIter;

class FinTailEvent {
  public:
	FinTailEvent();
	~FinTailEvent();

	int Load(char *aPath, TraceSource aSource);

	unsigned long GetNumberOfTraces();
	FinTailTraceProcessing *GetTrace(long aTraceNumber);
	/* Returns Maximum Signal in the processed distribution */
	double GetMaxSignal();
	double GetMaxSignalPos();
	double GetMaxSignalNeg();
	/* Returns Minimum Signal in the processed distribution */
	double GetMinSignal();
	double GetMinSignalPos();
	double GetMinSignalNeg();

  private:
	// Key: numbering from 0 to N-1
	std::vector <FinTailTraceProcessing> fTraces;
	double fMinSignal;
	double fMaxSignal;
	EventSource fSource;
	double fMinSignalPos;
	double fMinSignalNeg;
	double fMaxSignalPos;
	double fMaxSignalNeg;
};

}
#endif /* _FinTailEvent_h_ */
