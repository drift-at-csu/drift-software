#ifndef _FinTailEventProcessing_h_
#define _FinTailEventProcessing_h_

class TH1D;
class TH2D;

#include "FinTailTraceProcessing.h"

namespace FinTail {

class FinTailEvent;

class FinTailEventProcessing {
  public:
	FinTailEventProcessing(FinTailEvent *anEvent);
	~FinTailEventProcessing();

	/* 
	 * Builds Histogram of distributions of Signals
	 */
	int CreateSignalDistro( long number_of_bins);
	int CreateSignal2DDistro(long number_of_bins_neg, long number_of_bins_pos);

	bool isThereSignalDistro();
	bool isThereSignal2DDistro();

	/* Getters */
	TH1D *GetSignalDistro();
	TH1D *GetAftershootDistro();
	TH2D *GetSignal2DDistro();

  private:
	FinTailEvent *fEvent;
	TH1D *fSignalDistro;
	TH1D *fSignal2AftershootDistro;
	TH2D *fSignal2DDistro;

};

}
#endif /* _FinTailEventProcessing_h_ */
