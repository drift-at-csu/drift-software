#ifndef _FinTailSavitzky_Golay_h_
#define _FinTailSavitzky_Golay_h_

#include <string>
#include <vector>

namespace  FinTail {
class Savitzky_Golay {
  public:
	Savitzky_Golay();
	~Savitzky_Golay();

	double * GetOutputTraceArray();
	unsigned long GetNofTracePoints();



	/*
	 * Process the input trace anInputTrace with aNofPoints points
	 * and saves result into the internal fOutputTrace
	 */
	int Filter(double *anInputTrace, unsigned long aNofPoints);

	/************************************************************************
	 * Returns in c(np), in wrap-around order (see reference) consistent with 
	 * the argument respns in routine convlv, a set of Savitzky-Golay filter 
	 * coefficients. nl is the number of leftward (past) data points used, while 
	 * nr is the number of rightward (future) data points, making 
	 * the total number of data points used nl+nr+1. ld is the order of 
	 * the derivative desired  (e.g., ld = 0 for smoothed function). 
	 * m is the order of the smoothing polynomial, also 
	 * equal to the highest conserved moment; usual values are m = 2 or m = 4. 
	 ************************************************************************/
	int CalculateCoefficients( unsigned long nl, unsigned long nr,
		unsigned short ld, unsigned short m);

  private:
  	/**************************************************************
	 * Given an N x N matrix A, this routine replaces it by the LU *
	 * decomposition of a rowwise permutation of itself. A and N   *
	 * are input. INDX is an output vector which records the row   *
	 * permutation effected by the partial pivoting; D is output   *
	 * as -1 or 1, depending on whether the number of row inter-   *
	 * changes was even or odd, respectively. This routine is used *
	 * in combination with LUBKSB to solve linear equations or to  *
	 * invert a matrix. Return code is 1, if matrix is singular.   *
	 *
	 * A[ii][jj]->aMatrixA[ii*aMatrixOrder + jj]
	 * N->aMatrixOrder
	 * typedef  float MAT[MMAX+2][MMAX+2]; -> * double[N*N]
	 * Matrix indexing is now starts from 0instead of 1 as it was
	 * CODE-> function result
	 * **************************************************************/
  	int LUDecomposition();
	/*****************************************************************
	 * Solves the set of N linear equations A . X = B.  Here A is    *
	 * input, not as the matrix A but rather as its LU decomposition, *
	 * determined by the routine LUDecomposition. 
	 * INDX is input as the permutation vector returned by LUDCMP. 
	 * B is input as the right-hand   *
	 * side vector B, and returns with the solution vector X. A, N and*
	 * INDX are not modified by this routine and can be used for suc- *
	 * cessive calls with different right-hand sides. This routine is *
	 * also efficient for plain matrix inversion.                     *
	 *
	 * A[ii][jj]->aMatrixA[ii*aMatrixOrder + jj]
	 * N->aMatrixOrder
	 * LUBKSB -> LUSolve
	 * *****************************************************************/
	void LUBackSubstitute();

	int AllocateMemoryForTraces(unsigned long aNumberOfPoints);
	void CleanMemoryForTraces();

	// These are for dealing with calculating coefficients
	unsigned short fSmoothPolyOrder; // MMAX     6    //Maximum order of smoothing polynomial
	double	*fMatrix; 		// = A, size is MMAX x MMAX
	unsigned short *fPermutationVector;   // = INDX ,  size is MMAX
	double  *fLEVector;    //= B Vector (i/o) in the linear equation solution, size is MMAX

	unsigned short fDerivativeOrder; // ld       0    for smoothest
	unsigned short fNofPointsLeft;   // nl 5 -> Number of points to the left of the trace
	unsigned short fNofPointsRight;  // nr 5 -> Number of points to the right

	// These arrays that are used for storing plynomial coefficients
	short          *fSeekShiftIndex;     // index   Size is  NP+1
	double         *fFilterCoefficients; // c       Size is  NP+1
	unsigned short fNumberOfFilterCoeff; // NP   50


	// These are for dealing with output vectors
	double *fOutputTrace; // Output trace dynamic array
	long fNofTracePoints;  // NMAX

}; /* class Savitzky_Golay */
} /* namespace FinTail */
#endif /* _FinTailSavitzky_Golay_h_ */
