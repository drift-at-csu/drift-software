#ifndef _FinTailACcoupling_h_
#define _FinTailACcoupling_h_

#include <string>
#include <vector>

namespace  FinTail {

class ACcoupling {
  public:
	ACcoupling();
	~ACcoupling();

	void GenerateTemplate(double anACcoupl, double aBinWidth, unsigned long aNofBins);
	double GetTemplate(unsigned long aBinNumber);


  private:
  	std::vector <double> fTemplate;
}; /* class ACcoupling */
} /* namespace das_drift */
#endif /* _FinTailACcoupling_h_ */
