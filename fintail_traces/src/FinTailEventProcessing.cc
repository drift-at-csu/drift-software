#include "FinTailTraceProcessing.h"
#include "FinTailEvent.h"
#include "FinTailEventProcessing.h"
using namespace FinTail;

#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <sstream> // for stringstream
#include <iostream> // for cout
using namespace std;

#include <TDirectory.h>
#include <TMath.h>
#include <TH1D.h>
#include <TH2D.h>

//-------------------------------------------------------------------//
FinTailEventProcessing::FinTailEventProcessing(
	FinTailEvent *anEvent):
		fEvent(anEvent), 
		fSignalDistro(NULL),
		fSignal2AftershootDistro(NULL),
		fSignal2DDistro(NULL)
		{
}
//-------------------------------------------------------------------//
FinTailEventProcessing::~FinTailEventProcessing() {
  if (fSignalDistro) { delete (fSignalDistro); fSignalDistro = NULL;}
  if (fSignal2AftershootDistro) { 
  	delete (fSignal2AftershootDistro); 
	fSignal2AftershootDistro = NULL;}
  if (fSignal2DDistro) { delete (fSignal2DDistro); fSignal2DDistro = NULL;}
}
//-------------------------------------------------------------------//
int FinTailEventProcessing::CreateSignalDistro( long number_of_bins) {
  if (fEvent == NULL)
	return -1;

  stringstream title_string;
  title_string << "sig_dist1d-" << 1;

  
  // Delete Prev Histo if it was created...
  if (fSignalDistro != NULL) {

  	TObject* old = gDirectory->GetList()->FindObject(title_string.str().c_str());
	gDirectory->GetList()->Remove(old);

	delete fSignalDistro;
	fSignalDistro = NULL;
  }
  if (fSignal2AftershootDistro != NULL) {
  	TObject* old = gDirectory->GetList()->FindObject(title_string.str().c_str());
	gDirectory->GetList()->Remove(old);

	delete fSignal2AftershootDistro;
	fSignal2AftershootDistro = NULL;
  }

  // Create new histo
  /*
  fSignalDistro = new TH1D(title_string.str().c_str(), title_string.str().c_str(),
	number_of_bins,
	fEvent->GetMinSignal(), fEvent->GetMaxSignal());
  */
  fSignalDistro = new TH1D(
  	(title_string.str()+"_1").c_str(), 
	title_string.str().c_str(),
	number_of_bins,
	-100000, 900000);
  fSignal2AftershootDistro = new TH1D(
  	(title_string.str()+"_2").c_str(),
	title_string.str().c_str(),
	number_of_bins,
	-100000, 900000);

  
  /*
  if (fSignalDistro == NULL)  
  	return -4;
  */
 
  for ( long ii = 0; ii < (long)fEvent->GetNumberOfTraces(); ++ii ) {
  	fSignalDistro->Fill(fEvent->GetTrace(ii)->GetSignal(),1.0);
	fSignal2AftershootDistro->Fill(
		fEvent->GetTrace(ii)->GetSignal(),
		fEvent->GetTrace(ii)->GetSignalBefore() - 
			fEvent->GetTrace(ii)->GetSignalAfter()
		);
  }

  // Let's normalize Aftershoot histo
  fSignal2AftershootDistro->Divide(fSignalDistro);
  

  fSignalDistro->GetXaxis()->SetTitle("Total Signal, ADC");
  fSignalDistro->GetXaxis()->SetLabelSize(0.03);
  
  fSignal2AftershootDistro->GetXaxis()->SetTitle("Total Signal, ADC");
  fSignal2AftershootDistro->GetXaxis()->SetLabelSize(0.03);


  fSignalDistro->GetYaxis()->SetTitle("Number of Entries per Linear Bin");
  fSignalDistro->GetYaxis()->SetLabelSize(0.03);

  fSignal2AftershootDistro->GetYaxis()->SetTitle("Pedestal Difference, ADC");
  fSignal2AftershootDistro->GetYaxis()->SetLabelSize(0.03);
 
  return 1;
}
//-------------------------------------------------------------------//
int FinTailEventProcessing::CreateSignal2DDistro(
		long number_of_bins_neg, long number_of_bins_pos){
  if (fEvent == NULL)
	return -1;

  stringstream title_string;
  title_string << "sig_dist2d-" << 1;

  // Delete Prev Histo if it was created...
  if (fSignal2DDistro != NULL) {
	TObject* old = gDirectory->GetList()->FindObject(title_string.str().c_str());
	gDirectory->GetList()->Remove(old);

	delete fSignal2DDistro;
	fSignal2DDistro = NULL;
  }

  fSignal2DDistro = new TH2D(title_string.str().c_str(), title_string.str().c_str(),
	number_of_bins_neg, fEvent->GetMinSignalNeg(), fEvent->GetMaxSignalNeg(),
	number_of_bins_pos, fEvent->GetMinSignalPos(), fEvent->GetMaxSignalPos());

  if (fSignal2DDistro == NULL)
	return -4;

  for ( long ii = 0; ii < (long) fEvent->GetNumberOfTraces(); ++ii ) {
	fSignal2DDistro->Fill(
		fEvent->GetTrace(ii)->GetSignalBefore(),
		fEvent->GetTrace(ii)->GetSignalAfter(),
		1.0);
  }

  fSignal2DDistro->GetXaxis()->SetTitle("Negative Signal, ADC");
  fSignal2DDistro->GetXaxis()->SetLabelSize(0.03);
  fSignal2DDistro->GetYaxis()->SetTitle("Positive Signal, ADC");
  fSignal2DDistro->GetYaxis()->SetLabelSize(0.03);

  return 1;
}
//-------------------------------------------------------------------//
bool FinTailEventProcessing::isThereSignalDistro() {
  if (fSignalDistro != NULL)
  	return true;
  else
	return false;
}
//-------------------------------------------------------------------//
bool FinTailEventProcessing::isThereSignal2DDistro() {
  if (fSignal2DDistro != NULL)
	return true;
  else
	return false;
}
//-------------------------------------------------------------------//
TH1D *FinTailEventProcessing::GetSignalDistro() {
  return fSignalDistro;
}
//-------------------------------------------------------------------//
TH2D *FinTailEventProcessing::GetSignal2DDistro() {
  return fSignal2DDistro;
}
//-------------------------------------------------------------------//
TH1D *FinTailEventProcessing::GetAftershootDistro() {
  return fSignal2AftershootDistro;
}
//-------------------------------------------------------------------//
