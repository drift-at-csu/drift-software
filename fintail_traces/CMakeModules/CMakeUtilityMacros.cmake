# Macros defined here are
#  MARK_AS_INTERNAL
#  AUGER_CREATE_LINKS
#  AUGER_INCLUDE
#  FIRST
#  REMOVE_FIRST
#  LIST_CONTAINS
#  LIST_REGEX_REPLACE
#  PARSE_ARGUMENTS


include(CMakeCompareVersionStrings)


# MARK_AS_INTERNAL macro
# - Macro to hide a variable from the GUI
#

macro (MARK_AS_INTERNAL _var)
  set (${_var} ${${_var}} CACHE INTERNAL "hide this!" FORCE)
endmacro (MARK_AS_INTERNAL _var)


# LIST_REGEX_PLACE macro
# - This macro does a regex replacement in a list of variables
#
# Usage:
#  LIST_REGEX_REPLACE(${regex_1} ${regex_2} ${string_list})
# will replace regex_1 by regex_2 in all elements in the list

MACRO (LIST_REGEX_PLACE value_1 value_2 destination)
  SET (${destination})
  FOREACH (_item ${ARGN})
    STRING (REGEX REPLACE ${value_1} ${value_2} _item ${_item})
    LIST (APPEND ${destination} ${_item})
  ENDFOREACH (_item)
ENDMACRO (LIST_REGEX_PLACE)


# LIST_CONTAINS macro
# - This macro checks if a list contains a given value
#
# The variable given as first argument will be set to either TRUE or FALSE.
#
# Usage:
#  LIST_CONTAINS(value ${list})
# value will be set to either TRUE or FALSE

MACRO  (LIST_CONTAINS var value)
  SET (${var})
  FOREACH (value2 ${ARGN})
    IF (${value} STREQUAL ${value2})
      SET (${var} TRUE)
    ENDIF (${value} STREQUAL ${value2})
  ENDFOREACH (value2)
ENDMACRO (LIST_CONTAINS)


# FIRST macro
# - This macro returns sets the first argument given equal to the first element in a list.
# The list should be the second argument
#
# Usage:
#  FIRST(variable ${list})

MACRO (FIRST var)
  SET (${var} ${ARGV1})
ENDMACRO (FIRST)


# REMOVE_FIRST macro
# - Removes the first element of the list given as argument
#
# Usage:
#  REMOVE_FIRST(${list})

MACRO (REMOVE_FIRST var junk)
  SET (${var} ${ARGN})
ENDMACRO (REMOVE_FIRST)


# PARSE_ARGUMENTS macro
# - Macro to parse arguments given to macros.
#
# This is copied from http://www.cmake.org/Wiki/CMake_User_Contributed_Macros.
#
# Usage examples:
#   PARSE_ARGUMENTS( ${prefix} ${arg_names} ${option_names})
#   PARSE_ARGUMENTS(  _PREFIX "A;B" "C" ${ARGN})
#
# It will parse ${ARGN} and define the variables _PREFIX_A and _PREFIX_B.
# It will set _PREFIX_C to either TRUE or FALSE.

MACRO(PARSE_ARGUMENTS prefix arg_names option_names)
  SET(DEFAULT_ARGS)
  FOREACH(arg_name ${arg_names})
    SET(${prefix}_${arg_name})
  ENDFOREACH(arg_name)
  FOREACH(option ${option_names})
    SET(${prefix}_${option} FALSE)
  ENDFOREACH(option)

  # Note: macro behavior changes for CMake version > 2.4.7.
  COMPARE_VERSION_STRINGS (
    "${CMAKE_MAJOR_VERSION}.${CMAKE_MINOR_VERSION}.${CMAKE_PATCH_VERSION}"
    "2.4.7"
    _version_GT_CMake247)

  SET(current_arg_name DEFAULT_ARGS)
  SET(current_arg_list)
  FOREACH(arg ${ARGN})
    # Version > 2.4.7
    IF (_version_GT_CMake247 EQUAL 1)
      SET(larg_names ${arg_names})
      LIST(FIND larg_names "${arg}" is_arg_name)
      IF (is_arg_name GREATER -1)
        SET(${prefix}_${current_arg_name} ${current_arg_list})
        SET(current_arg_name ${arg})
        SET(current_arg_list)
      ELSE (is_arg_name GREATER -1)
        SET(loption_names ${option_names})
        LIST(FIND loption_names "${arg}" is_option)
        IF (is_option GREATER -1)
         SET(${prefix}_${arg} TRUE)
        ELSE (is_option GREATER -1)
         SET(current_arg_list ${current_arg_list} ${arg})
        ENDIF (is_option GREATER -1)
      ENDIF (is_arg_name GREATER -1)
    # Version <= 2.4.7
    ELSE (_version_GT_CMake247 EQUAL 1)
      LIST_CONTAINS (is_arg_name ${arg} ${arg_names})
      IF (is_arg_name)
        SET (${prefix}_${current_arg_name} ${current_arg_list})
        SET (current_arg_name ${arg})
        SET (current_arg_list)
      ELSE (is_arg_name)
        LIST_CONTAINS (is_option ${arg} ${option_names})
        IF (is_option)
          SET (${prefix}_${arg} TRUE)
        ELSE (is_option)
          SET (current_arg_list ${current_arg_list} ${arg})
        ENDIF (is_option)
      ENDIF (is_arg_name)
    ENDIF (_version_GT_CMake247 EQUAL 1)
  ENDFOREACH(arg)
  SET(${prefix}_${current_arg_name} ${current_arg_list})
ENDMACRO(PARSE_ARGUMENTS)
