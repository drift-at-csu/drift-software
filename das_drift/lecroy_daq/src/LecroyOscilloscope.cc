#include "LecroyOscilloscope.h"
using namespace Lecroy;

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <sstream>
#include <fstream>
using namespace std;

//-------------------------------------------------------------------//
Oscilloscope::Oscilloscope():
		fCH1trace(NULL), fCH2trace(NULL), fCH3trace(NULL), fCH4trace(NULL),
		fNumberOfPoints(0), fVGain(0.0), fHinterval(0.0){
}
//-------------------------------------------------------------------//
Oscilloscope::~Oscilloscope(){
  CleanupMemory();
}
//-------------------------------------------------------------------//
int Oscilloscope::LoadASCII(unsigned char aChannelNumber, std::string aFilename){
  string line, str1, str2;
  string search_pattern;
  int search_offset = 0;

  ifstream file(aFilename.c_str(), ifstream::in);
  if ( !file.good()) {
	cout << "...Can't open file " << aFilename << endl;
	return -1;
  }
  // Let's read a header

  unsigned long nof_points = 0;
  search_pattern ="SegmentSize,";
  search_offset = 0;
  for (int ii = 0; ii < 5; ++ii) {
  	getline(file, line);
	if (!file.good()) {
		cout << "...Can't read header in " << aFilename << endl;
		file.close();
		return -2;
	}
	if ((search_offset = line.find(search_pattern, 0)) != string::npos) {
		str1 = line.substr(search_offset + search_pattern.size());
		nof_points = atoi(str1.c_str());
		// cout << " Number of points: " << nof_points << endl;
	}
  }
  // Let's allocate the arrays first
  if (nof_points != fNumberOfPoints) {
  	// cout << "...Cleaning up memory" << endl;
	CleanupMemory();
	AllocateMemoryFor(nof_points);
  }

  double x, y;
  unsigned long nof_lines = 0;
  search_pattern =",";
  fVGain = 1.0E-6;
  fHinterval = 20.0E-9;
  short *trace_pointer;

  while (1) {

  	getline(file, line);
	if (!file.good()) 
		break;

	// let's clear line from back carrier...
	// line.erase(line.end()-1, line.end() );
	// cout << "...We read line: '" << line << "' size=" << line.size() <<  endl;
	// Else let's read points
	if ((search_offset = line.find(search_pattern, 0)) != string::npos) {
		str1=line.substr(0, search_offset);
		str2=line.substr(search_offset+search_pattern.size(), line.size());
		// cout << "...In strings:" << str1 << "|" <<  str2 << endl;

		x=atof(str1.c_str());
		y=atof(str2.c_str());
		// cout << "...X=" << x << "|Y=" << y << endl; 

		trace_pointer = GetTracePointer(aChannelNumber);
		if (trace_pointer != NULL) {
			if (aChannelNumber == 1)
				trace_pointer[nof_lines] = (short) ((double) y/fVGain/20.0);
			else
				trace_pointer[nof_lines] = (short) ((double) y/fVGain*1.0);
			nof_lines++;
		}
	}
  }
  cout << "...Header says:" << nof_points << ", and we read:" << nof_lines << endl;
  file.close();

  return 1;
}
//-------------------------------------------------------------------//
int Oscilloscope::Save(string aFilename){
  unsigned long num_of_channels = 4;
  cout << "...Saving Waveforms with " << fNumberOfPoints 
  	<<  "points to " << aFilename << endl;

  ofstream file;
  file.open(aFilename.c_str(), ios::out | ios::trunc | ios::binary);
  file.write(reinterpret_cast<const char*> (&fNumberOfPoints), sizeof(unsigned long));
  file.write(reinterpret_cast<const char*> (&num_of_channels), sizeof(unsigned long));
  file.write(reinterpret_cast<const char*> (&fVGain), sizeof(double));
  file.write(reinterpret_cast<const char*> (&fHinterval), sizeof(double));
  file.write(reinterpret_cast<const char*> (fCH1trace), sizeof(short) * fNumberOfPoints);
  file.write(reinterpret_cast<const char*> (fCH2trace), sizeof(short) * fNumberOfPoints);
  file.write(reinterpret_cast<const char*> (fCH3trace), sizeof(short) * fNumberOfPoints);
  file.write(reinterpret_cast<const char*> (fCH4trace), sizeof(short) * fNumberOfPoints);
  file.close();
  
  return 1;
}
//-------------------------------------------------------------------//
void Oscilloscope::CleanupMemory(){
  if (fCH1trace != NULL){
	delete [] fCH1trace;
	fCH1trace = NULL;
  }
  if (fCH2trace != NULL){
	delete [] fCH2trace;
	fCH2trace = NULL;
  }
  if (fCH3trace != NULL){
	delete [] fCH3trace;
	fCH3trace = NULL;
  }
  if (fCH4trace != NULL){
	delete [] fCH4trace;
	fCH4trace = NULL;
  }
  fNumberOfPoints = 0;
}
//-------------------------------------------------------------------//
int Oscilloscope::AllocateMemoryFor(unsigned long aNumberOfPoints){
  if ((fCH1trace !=NULL)||(fCH2trace !=NULL)||(fCH3trace !=NULL)||(fCH4trace !=NULL)) {
	cout << "...Cleanup memory first before allocation" << endl;
	return 0;
  }
  fNumberOfPoints = 0;
  fCH1trace = new (nothrow) short [aNumberOfPoints];
  if (fCH1trace == NULL) {
	cout << "...Can't allocate memory for CH1" << endl;
	return 0;
  }
  fCH2trace = new (nothrow) short [aNumberOfPoints];
  if (fCH1trace == NULL) {
	delete [] fCH1trace; fCH1trace = NULL;
	cout << "...Can't allocate memory for CH2" << endl;
	return 0;
  }
  fCH3trace = new (nothrow) short [aNumberOfPoints];
  if (fCH3trace == NULL) {
	delete [] fCH1trace; fCH1trace = NULL;
	delete [] fCH2trace; fCH2trace = NULL;
	cout << "...Can't allocate memory for CH3" << endl;
	return 0;
  }
  fCH4trace = new (nothrow) short [aNumberOfPoints];
  if (fCH4trace == NULL) {
	delete [] fCH1trace; fCH1trace = NULL;
	delete [] fCH2trace; fCH2trace = NULL;
	delete [] fCH3trace; fCH3trace = NULL;
	cout << "...Can't allocate memory for CH4" << endl;
	return 0;
  }
  fNumberOfPoints = aNumberOfPoints;
  return 1;
}
//-------------------------------------------------------------------//
int Oscilloscope::Load(std::string aFilename){
  unsigned long num_of_channels = 0;
  unsigned long num_of_points = 0;
  ifstream file(aFilename.c_str(), ios::binary);

  // Let's read the header
  file.read(reinterpret_cast< char*> (&num_of_points), sizeof(unsigned long));
  if (num_of_points != fNumberOfPoints) {
  	CleanupMemory();
	AllocateMemoryFor(num_of_points);
  }
  file.read(reinterpret_cast< char*> (&num_of_channels), sizeof(unsigned long));
  if (num_of_channels != 4) {
  	file.close();
	cout << "...Can't load since number of channels != 4" << endl;
	return 0;
  }
  file.read(reinterpret_cast< char*> (&fVGain), sizeof(double));
  file.read(reinterpret_cast< char*> (&fHinterval), sizeof(double));

  file.read(reinterpret_cast< char*> (fCH1trace), sizeof(short) * fNumberOfPoints);
  file.read(reinterpret_cast< char*> (fCH2trace), sizeof(short) * fNumberOfPoints);
  file.read(reinterpret_cast< char*> (fCH3trace), sizeof(short) * fNumberOfPoints);
  file.read(reinterpret_cast< char*> (fCH4trace), sizeof(short) * fNumberOfPoints);

  file.close();

  return 1;;

}
//-------------------------------------------------------------------//
short *Oscilloscope::GetTracePointer(unsigned char aChannelNumber){
  if (aChannelNumber == 1) {
  	return fCH1trace;
  } else if (aChannelNumber == 2) {
  	return fCH2trace;
  } else if (aChannelNumber == 3) {
  	return fCH3trace;
  } else if (aChannelNumber == 4) {
  	return fCH4trace;
  } else {
  	return NULL;
  }
}
//-------------------------------------------------------------------//
unsigned long Oscilloscope::GetNofTimebins(unsigned char aChannelNumber){
  if ((aChannelNumber <= 4)&&(aChannelNumber >= 1)) {
	return fNumberOfPoints;
  } else {
  	return 0;
  }
}
//-------------------------------------------------------------------//
unsigned char Oscilloscope::GetNofBytesPerTimebin(){
  return sizeof(short);
}
//-------------------------------------------------------------------//
double Oscilloscope::GetHorInterval(){
  return fHinterval;
}
//-------------------------------------------------------------------//
double Oscilloscope::GetVertGain(){
  return fVGain;
}
//-------------------------------------------------------------------//
