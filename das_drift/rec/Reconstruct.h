#ifndef _DRIFT_rec_Reconstruct_h_
#define _DRIFT_rec_Reconstruct_h_

#include <list>
#include <string>

namespace drift {
namespace evt{
  class Channel;
  class Event;
}
namespace det{
  class Detector;
}
namespace rec {
class Reconstruct {
  public:
  	typedef std::list<evt::Channel*> ChannelContainer;
	typedef ChannelContainer::iterator ChannelIterator;
	typedef ChannelContainer::const_iterator ConstChannelIterator;

  public:
  	Reconstruct();
	~Reconstruct();

	bool RecFirstAC(const unsigned int channelId);
	bool RecSecondAC(const unsigned int channelId, 
		bool correct_one_over_f);
	bool RecCurrent(const unsigned int channelId);
	bool RecCharge(const unsigned int channelId);

	void SetDetector(det::Detector*  detector);
	void SetEvent(evt::Event* event);

	unsigned int GetNofChanFirstAC() const;
	unsigned int GetNofChanSecondAC() const;
	unsigned int GetNofChanCurrent() const;
	unsigned int GetNofChanCharge() const;

	void MakeFirstACChannel(const unsigned int channelId);
	bool HasFirstACChannel(const unsigned int channelId);
	evt::Channel *GetFirstACChannel(const unsigned int channelId);
	void ClearFirstAC();
	void ClearFirstAC(const unsigned int channelId);

	void MakeSecondACChannel(const unsigned int channelId);
	bool HasSecondACChannel(const unsigned int channelId);
	evt::Channel *GetSecondACChannel(const unsigned int channelId);
	void ClearSecondAC();
	void ClearSecondAC(const unsigned int channelId);

	void MakeCurrentChannel(const unsigned int channelId);
	bool HasCurrentChannel(const unsigned int channelId);
	evt::Channel *GetCurrentChannel(const unsigned int channelId);
	void ClearCurrent();
	void ClearCurrent(const unsigned int channelId);

	void MakeChargeChannel(const unsigned int channelId);
	bool HasChargeChannel(const unsigned int channelId);
	evt::Channel *GetChargeChannel(const unsigned int channelId);
	void ClearCharge();
	void ClearCharge(const unsigned int channelId);

  	ChannelIterator ChanFirstACBegin();
	ChannelIterator ChanFirstACEnd();
	ConstChannelIterator ChanFirstACBegin() const;
	ConstChannelIterator ChanFirstACEnd() const;

	ChannelIterator ChanSecondACBegin();
	ChannelIterator ChanSecondACEnd();
	ConstChannelIterator ChanSecondACBegin() const;
	ConstChannelIterator ChanSecondACEnd() const;

	ChannelIterator ChanCurrentBegin();
	ChannelIterator ChanCurrentEnd();
	ConstChannelIterator ChanCurrentBegin() const;
	ConstChannelIterator ChanCurrentEnd() const;

	ChannelIterator ChanChargeBegin();
	ChannelIterator ChanChargeEnd();
	ConstChannelIterator ChanChargeBegin() const;
	ConstChannelIterator ChanChargeEnd() const;

  private:
	evt::Event*	fEvent;
	det::Detector*	fDetector;

	ChannelContainer fFirstAC;
	ChannelContainer fSecondAC;
	ChannelContainer fCurrent;
	ChannelContainer fCharge;
}; /* class Reconstruct */
} /* namespace rec */
} /* namespace drift */
#endif /* _DRIFT_rec_Reconstruct_h_ */
