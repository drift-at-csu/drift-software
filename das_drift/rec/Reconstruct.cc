#include <evt/Event.h>
#include <evt/Channel.h>
#include <det/Detector.h>
#include <det/Channel.h>
#include <utl/Trace.h>
#include <rec/Reconstruct.h>
using namespace drift;


#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string.h>
using namespace std;

//-------------------------------------------------------------------//
rec::Reconstruct::Reconstruct():
	fEvent(NULL), fDetector(NULL){
}
//-------------------------------------------------------------------//
rec::Reconstruct::~Reconstruct(){
  ClearFirstAC();
  ClearSecondAC();
  ClearCurrent();
  ClearCharge();
}
//-------------------------------------------------------------------//
bool rec::Reconstruct::RecFirstAC(const unsigned int channelId){
  if (fEvent == NULL)
	return false;
  if (fDetector == NULL)
	return false;
  evt::Channel *event_channel = fEvent->GetChannel(channelId);
  if (event_channel == NULL)
  	return false;
  det::Channel *det_channel = fDetector->GetChannel(channelId);
  if (det_channel == NULL)
  	return false;
  cout << "...rec::Reconstruct::RecFirstAC for Channel " << channelId << endl;
  //After this  channel description of the detector as well as 
  //the channel in the event do exist.
  
  // Let's Make the target channel
  MakeFirstACChannel(channelId);
  // And Get a pointer to it
  evt::Channel *target_channel = GetFirstACChannel(channelId);
  // We need to copy the source trace to the target trace
  for(unsigned int ii = 0; ii < event_channel->GetNofTraces(); ++ii){
	target_channel->MakeTrace(ii);
	(*target_channel->GetTrace(ii)) = (*event_channel->GetTrace(ii));
  }

  //Let's put a range for pedestals here if it doesn't exist
  double chan_gain = det_channel->GetFirstACGain();
  if (chan_gain != 0.0) 
  	chan_gain = 1.0/chan_gain;
  target_channel->GetTrace(0)->MultiplyBy(chan_gain);

  // We want to subtract the pedestal
  target_channel->GetTrace(0)->ShiftPedestalToZero(0.0, 1000.0);

  // And deconvolve the trace
  target_channel->GetTrace(0)->DeconvolveACCoupling( det_channel->GetFirstACTau() );

  return true;
}
//-------------------------------------------------------------------//
bool rec::Reconstruct::RecSecondAC(const unsigned int channelId,
	bool correct_one_over_f){
  if (fDetector == NULL)
	return false;
  det::Channel *det_channel = fDetector->GetChannel(channelId);
  if (det_channel == NULL)
	return false;

  if (!HasFirstACChannel(channelId)) {
	cout << "...rec::Reconstruct::RecSecondAC: Channel " << channelId
		<< " doesn't have FirstAC Rec" << endl;
	return false;
  }
  cout << "...rec::Reconstruct::RecSecondAC for Channel " << channelId << endl;
  // Let's Make the target channel
  MakeSecondACChannel(channelId);
  // And Get a pointer to it
  evt::Channel *target_channel = GetSecondACChannel(channelId);

  // We need to copy the source trace to the target trace
  for(unsigned int ii = 0; ii < GetFirstACChannel(channelId)->GetNofTraces(); ++ii){
	target_channel->MakeTrace(ii);
	(*target_channel->GetTrace(ii)) = (*GetFirstACChannel(channelId)->GetTrace(ii));
  }

  // Let's correct 1/f noise
  
  if (correct_one_over_f) {
	target_channel->GetTrace(0)->CorrectOneOverF();
  } else {
  	target_channel->GetTrace(0)->ShiftPedestalToZero(0.0, 1000.0);
  }
  

  double chan_gain = det_channel->GetSecondACGain();
  if (chan_gain != 0.0)
	chan_gain = 1.0/chan_gain;
  target_channel->GetTrace(0)->MultiplyBy(chan_gain);

  // And deconvolve the trace
  target_channel->GetTrace(0)->DeconvolveACCoupling( det_channel->GetSecondACTau());

  // If we were told to correct for 1/f noise coupling?
  if (correct_one_over_f) {
  	target_channel->GetTrace(0)->CorrectOneOverF();
  } else {
	target_channel->GetTrace(0)->ShiftPedestalToZero(0.0, 1000.0);
  }
  
  return true;
}
//-------------------------------------------------------------------//
bool rec::Reconstruct::RecCurrent(const unsigned int channelId){
  if (fDetector == NULL)
	return false;
  det::Channel *det_channel = fDetector->GetChannel(channelId);
  if (det_channel == NULL)
	return false;
  if (!HasSecondACChannel(channelId)) {
	cout << "...rec::Reconstruct::RecCurrent: Channel " << channelId
		<< " doesn't have SecondAC Rec" << endl;
	return false;
  }
  cout << "...rec::Reconstruct::RecCurrentAC for Channel " << channelId << endl;
  // Let's Make the target channel
  MakeCurrentChannel(channelId);
  // And Get a pointer to it
  evt::Channel *target_channel = GetCurrentChannel(channelId);

  // We need to copy the source trace to the target trace
  for(unsigned int ii = 0; ii < GetSecondACChannel(channelId)->GetNofTraces(); ++ii){
	target_channel->MakeTrace(ii);
	(*target_channel->GetTrace(ii)) = (*GetSecondACChannel(channelId)->GetTrace(ii));
  }

  // target_channel->GetTrace(0)->CorrectOneOverF(500.0, 500.0);

  double chan_gain = det_channel->GetTransimpGain();
  if (chan_gain != 0.0)
	chan_gain = 1.0/chan_gain;
  target_channel->GetTrace(0)->MultiplyBy(chan_gain);

  // We want to subtract the pedestal
  // target_channel->GetTrace(0)->ShiftPedestalToZero(0.0, 1000.0);

  // And deconvolve the trace
  target_channel->GetTrace(0)->DeconvolveCurrent(det_channel->GetTransimpTau());

  return true;
}
//-------------------------------------------------------------------//
bool rec::Reconstruct::RecCharge(const unsigned int channelId){
  if (fDetector == NULL)
	return false;
  det::Channel *det_channel = fDetector->GetChannel(channelId);
  if (det_channel == NULL)
	return false;
  if (!HasCurrentChannel(channelId)) {
	cout << "...rec::Reconstruct::RecCharge: Channel " << channelId
		<< " doesn't have Current Rec" << endl;
	return false;
  }
  cout << "...rec::Reconstruct::RecCharge for Channel " << channelId << endl;
  // Let's Make the target channel
  MakeChargeChannel(channelId);
  // And Get a pointer to it
  evt::Channel *target_channel = GetChargeChannel(channelId);
  // We need to copy the source trace to the target trace
  for(unsigned int ii = 0; ii < GetCurrentChannel(channelId)->GetNofTraces(); ++ii){
	target_channel->MakeTrace(ii);
	(*target_channel->GetTrace(ii)) = (*GetCurrentChannel(channelId)->GetTrace(ii));
  }

  // target_channel->GetTrace(0)->CorrectOneOverF(500.0, 500.0);
  // And integrate it
  target_channel->GetTrace(0)->Integrate(6241.51); // 1fC = 6241.51 electrons

  return true;
}
//-------------------------------------------------------------------//
void rec::Reconstruct::SetDetector(det::Detector*  detector){
  fDetector = detector;
}
//-------------------------------------------------------------------//
void rec::Reconstruct::SetEvent(evt::Event* event){
  fEvent = event;
}
//-------------------------------------------------------------------//
unsigned int rec::Reconstruct::GetNofChanFirstAC() const{
  return fFirstAC.size();
}
//-------------------------------------------------------------------//
unsigned int rec::Reconstruct::GetNofChanSecondAC() const{
  return fSecondAC.size();
}
//-------------------------------------------------------------------//
unsigned int rec::Reconstruct::GetNofChanCurrent() const{
  return fCurrent.size();
}
//-------------------------------------------------------------------//
unsigned int rec::Reconstruct::GetNofChanCharge() const{
  return fCharge.size();
}
//-------------------------------------------------------------------//
void rec::Reconstruct::MakeFirstACChannel(const unsigned int channelId){
  if (HasFirstACChannel(channelId)) 
	return;
   evt::Channel *ChannelPnt = new evt::Channel();
   ChannelPnt->SetId(channelId);
   fFirstAC.push_back(ChannelPnt);
}
//-------------------------------------------------------------------//
bool rec::Reconstruct::HasFirstACChannel(const unsigned int channelId){
  for (ConstChannelIterator iter = ChanFirstACBegin(); iter != ChanFirstACEnd(); ++iter) {
	if ((*iter)->GetId() == channelId) {
		return true;
	}
  }
  return false;
}
//-------------------------------------------------------------------//
evt::Channel *rec::Reconstruct::GetFirstACChannel(const unsigned int channelId){
  for (ConstChannelIterator iter = ChanFirstACBegin(); iter != ChanFirstACEnd(); ++iter) {
  	if ((*iter)->GetId() == channelId) {
		return (*iter);
	}
  }
  return NULL;
}
//-------------------------------------------------------------------//
void rec::Reconstruct::ClearFirstAC(){
  for (ChannelIterator iter = ChanFirstACBegin(); iter != ChanFirstACEnd(); ++iter) {
	if ((*iter)!=NULL) {
		delete (*iter);
		(*iter) = NULL;
	}
  }
  fFirstAC.clear();
}
//-------------------------------------------------------------------//
void rec::Reconstruct::ClearFirstAC(const unsigned int channelId){
  for (ChannelIterator iter = ChanFirstACBegin(); iter != ChanFirstACEnd(); ++iter) {
	if ((*iter)->GetId() == channelId) {
		delete (*iter);
		fFirstAC.erase(iter);
		break;
	}
  }
}
//-------------------------------------------------------------------//
void rec::Reconstruct::MakeSecondACChannel(const unsigned int channelId){
  if (HasSecondACChannel(channelId))
	return;
  evt::Channel* ChannelPnt = new evt::Channel();
  ChannelPnt->SetId(channelId);
  fSecondAC.push_back(ChannelPnt);
}
//-------------------------------------------------------------------//
bool rec::Reconstruct::HasSecondACChannel(const unsigned int channelId){
  for (ConstChannelIterator iter = ChanSecondACBegin(); iter != ChanSecondACEnd(); ++iter) {
	if ((*iter)->GetId() == channelId) {
		return true;
	}
  }
  return false;
}
//-------------------------------------------------------------------//
evt::Channel *rec::Reconstruct::GetSecondACChannel(const unsigned int channelId){
  for (ConstChannelIterator iter = ChanSecondACBegin(); iter != ChanSecondACEnd(); ++iter) {
	if ((*iter)->GetId() == channelId) {
		return (*iter);
	}
  }
  return NULL;
}
//-------------------------------------------------------------------//
void rec::Reconstruct::ClearSecondAC(){
  for (ChannelIterator iter = ChanSecondACBegin(); iter != ChanSecondACEnd(); ++iter) {
	if ((*iter)!=NULL) {
		delete (*iter);
		(*iter) = NULL;
	}
  }
  fSecondAC.clear();
}
//-------------------------------------------------------------------//
void rec::Reconstruct::ClearSecondAC(const unsigned int channelId){
  for (ChannelIterator iter = ChanSecondACBegin(); iter != ChanSecondACEnd(); ++iter) {
	if ((*iter)->GetId() == channelId) {
		delete (*iter);
		fSecondAC.erase(iter);
		break;
	}
  }
}
//-------------------------------------------------------------------//
void rec::Reconstruct::MakeCurrentChannel(const unsigned int channelId){
  if (HasCurrentChannel(channelId))
	return;
  evt::Channel* ChannelPnt = new evt::Channel();
  ChannelPnt->SetId(channelId);
  fCurrent.push_back(ChannelPnt);
}
//-------------------------------------------------------------------//
bool rec::Reconstruct::HasCurrentChannel(const unsigned int channelId){
  for (ConstChannelIterator iter = ChanCurrentBegin(); iter != ChanCurrentEnd(); ++iter) {
	if ((*iter)->GetId() == channelId) {
		return true;
	}
  }
  return false;
}
//-------------------------------------------------------------------//
evt::Channel *rec::Reconstruct::GetCurrentChannel(const unsigned int channelId){
  for (ConstChannelIterator iter = ChanCurrentBegin(); iter != ChanCurrentEnd(); ++iter) {
	if ((*iter)->GetId() == channelId) {
		return (*iter);
	}
  }
  return NULL;
}
//-------------------------------------------------------------------//
void rec::Reconstruct::ClearCurrent(){
  for (ChannelIterator iter = ChanCurrentBegin(); iter != ChanCurrentEnd(); ++iter) {
	if ((*iter)!=NULL) {
		delete (*iter);
		(*iter) = NULL;
	}
  }
  fCurrent.clear();
}
//-------------------------------------------------------------------//
void rec::Reconstruct::ClearCurrent(const unsigned int channelId){
  for (ChannelIterator iter = ChanCurrentBegin(); iter != ChanCurrentEnd(); ++iter) {
	if ((*iter)->GetId() == channelId) {
		delete (*iter);
		fCurrent.erase(iter);
		break;                                    
	}
  }
}
//-------------------------------------------------------------------//
void rec::Reconstruct::MakeChargeChannel(const unsigned int channelId){
  if (HasChargeChannel(channelId))
	return;
  evt::Channel* ChannelPnt = new evt::Channel();
  ChannelPnt->SetId(channelId);
  fCharge.push_back(ChannelPnt);
}
//-------------------------------------------------------------------//
bool rec::Reconstruct::HasChargeChannel(const unsigned int channelId){
  for (ConstChannelIterator iter = ChanChargeBegin(); iter != ChanChargeEnd(); ++iter) {
	if ((*iter)->GetId() == channelId) {
		return true;
	}
  }
  return false;
}
//-------------------------------------------------------------------//
evt::Channel *rec::Reconstruct::GetChargeChannel(const unsigned int channelId){
  for (ConstChannelIterator iter = ChanChargeBegin(); iter != ChanChargeEnd(); ++iter) {
	if ((*iter)->GetId() == channelId) {
		return (*iter);
	}
  }
  return NULL;
}
//-------------------------------------------------------------------//
void rec::Reconstruct::ClearCharge(){
  for (ChannelIterator iter = ChanChargeBegin(); iter != ChanChargeEnd(); ++iter) {
	if ((*iter)!=NULL) {
		delete (*iter);
		(*iter) = NULL;
	}
  }
  fCharge.clear();
}
//-------------------------------------------------------------------//
void rec::Reconstruct::ClearCharge(const unsigned int channelId){
  for (ChannelIterator iter = ChanChargeBegin(); iter != ChanChargeEnd(); ++iter) {
	if ((*iter)->GetId() == channelId) {
		delete (*iter);
		fCharge.erase(iter);
		break;
	}
  }
}                                                                   
//-------------------------------------------------------------------//
rec::Reconstruct::ChannelIterator rec::Reconstruct::ChanFirstACBegin(){
  return fFirstAC.begin();
}
//-------------------------------------------------------------------//
rec::Reconstruct::ChannelIterator rec::Reconstruct::ChanFirstACEnd(){
  return fFirstAC.end();
}
//-------------------------------------------------------------------//
rec::Reconstruct::ConstChannelIterator rec::Reconstruct::ChanFirstACBegin() const{
  return fFirstAC.begin();
}
//-------------------------------------------------------------------//
rec::Reconstruct::ConstChannelIterator rec::Reconstruct::ChanFirstACEnd() const{
  return fFirstAC.end();
}
//-------------------------------------------------------------------//
rec::Reconstruct::ChannelIterator rec::Reconstruct::ChanSecondACBegin(){
  return fSecondAC.begin();
}
//-------------------------------------------------------------------//
rec::Reconstruct::ChannelIterator rec::Reconstruct::ChanSecondACEnd(){
  return fSecondAC.end();
}
//-------------------------------------------------------------------//
rec::Reconstruct::ConstChannelIterator rec::Reconstruct::ChanSecondACBegin() const{
  return fSecondAC.begin();
}
//-------------------------------------------------------------------//
rec::Reconstruct::ConstChannelIterator rec::Reconstruct::ChanSecondACEnd() const{
  return fSecondAC.end();
}
//-------------------------------------------------------------------//
rec::Reconstruct::ChannelIterator rec::Reconstruct::ChanCurrentBegin(){
  return fCurrent.begin();
}
//-------------------------------------------------------------------//
rec::Reconstruct::ChannelIterator rec::Reconstruct::ChanCurrentEnd(){
  return fCurrent.end();
}
//-------------------------------------------------------------------//
rec::Reconstruct::ConstChannelIterator rec::Reconstruct::ChanCurrentBegin() const{
  return fCurrent.begin();
}
//-------------------------------------------------------------------//
rec::Reconstruct::ConstChannelIterator rec::Reconstruct::ChanCurrentEnd() const{
  return fCurrent.end();
}
//-------------------------------------------------------------------//
rec::Reconstruct::ChannelIterator rec::Reconstruct::ChanChargeBegin(){
  return fCharge.begin();
}
//-------------------------------------------------------------------//
rec::Reconstruct::ChannelIterator rec::Reconstruct::ChanChargeEnd(){
  return fCharge.end();
}
//-------------------------------------------------------------------//
rec::Reconstruct::ConstChannelIterator rec::Reconstruct::ChanChargeBegin() const{
  return fCharge.begin();
}
//-------------------------------------------------------------------//
rec::Reconstruct::ConstChannelIterator rec::Reconstruct::ChanChargeEnd() const{
  return fCharge.end();
}       
//-------------------------------------------------------------------//
