#include <stdlib.h>
using namespace std;

#include <TGButton.h>
#include <TGFrame.h>
#include <TGProgressBar.h>
#include <TGLayout.h>
#include <TGDimension.h>
#include <TColor.h>
#include <TROOT.h>

#include "TReadProgress.h"

//-------------------------------------------------------------------//
TReadProgress::TReadProgress(
	const TGWindow *p, const UInt_t w, UInt_t h, const char * windowName):
		TGMainFrame(p, w, h) {

  fLayF = new TGLayoutHints(
  	kLHintsTop | kLHintsLeft | kLHintsExpandX, 0, 0, 0, 0);
  fLayS = new TGLayoutHints(
  	kLHintsTop | kLHintsCenterX, 10, 10, 10, 10);
  fLayB = new TGLayoutHints(
  	kLHintsTop | kLHintsLeft | kLHintsExpandX, 10, 10,  20, 10);

  fFrame = new TGVerticalFrame(this, 0, 0, 0);
 
  fBar = new TGHProgressBar(fFrame, TGProgressBar::kFancy, 300);
  fBar->SetBarColor(gROOT->GetColor(38)->GetPixel());
  fBar->ShowPosition(kTRUE, kFALSE, "%.0f events");

  fStop = new TGTextButton(fFrame,"  Stop  ", 1);
  fStop->Associate(this);

  fFrame->AddFrame(fBar, fLayB);
  fFrame->AddFrame(fStop, fLayS);

  fFrame->Resize(w, h);
  this->AddFrame(fFrame, fLayF);
 
  TGDimension size = GetDefaultSize();
  Resize(size);

  if ( windowName == NULL )
    SetWindowName("Opening File..."); 
  else
    SetWindowName(windowName); 
    
  MapSubwindows();
  MapWindow();

  fS=kFALSE;
}
//-------------------------------------------------------------------//
TReadProgress::~TReadProgress() {
  delete fStop;
  delete fBar;
  delete fFrame;

  delete fLayF;
  delete fLayS;
  delete fLayB;
}
//-------------------------------------------------------------------//
void TReadProgress::CloseWindow() {
  TGMainFrame::CloseWindow();
  delete this;
}
//-------------------------------------------------------------------//
Bool_t TReadProgress::ProcessMessage(Long_t msg, Long_t parm1, Long_t) {

  switch (GET_MSG(msg)) {
  case kC_COMMAND:
	switch (GET_SUBMSG(msg)) {
	case kCM_BUTTON:
		switch (parm1) {
		case 1:
			fS=kTRUE;
		break;
		default:
		break;
		} break;
    		default:
		break;  
	} break;
	default:
	break;
  }
  return kTRUE;
}
//-------------------------------------------------------------------//
