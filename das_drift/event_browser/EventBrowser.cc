#include "EventBrowser.h"
#include "EventBrowserMenu.h"
#include "EventBrowserSignals.h"
#include "VPlots.h"
#include "MWPCPlots.h"
#include "StatusBar.h"
#include "TReadProgress.h"

#include <TROOT.h>
#include <TStyle.h>
#include <TSystem.h>
#include <TCanvas.h>
#include <TChain.h>
#include <TApplication.h>
#include <TLatex.h>
#include <TString.h>
#include <KeySymbols.h>

#include <TGFrame.h>
#include <TGTab.h>
#include <TGFileDialog.h>
#include <TGComboBox.h>
#include <TGNumberEntry.h>
#include <TGMsgBox.h>

#include <iostream>
#include <vector>
#include <sstream>
#include <cstdlib>
#include <algorithm>
using namespace std;


#include <evt/Event.h>
#include <det/Detector.h>
#include <rec/Reconstruct.h>
using namespace drift;

ClassImp (EventBrowser);
//-------------------------------------------------------------------//
string EventBrowser::GetVersion() {
  return string("trunk");
}
//-------------------------------------------------------------------//
string EventBrowser::GetRevision() {
  return string("0.1");
}
//-------------------------------------------------------------------//
string EventBrowser::GetDate() {
  return string("2013-10");
}
//-------------------------------------------------------------------//
void EventBrowser::Welcome() {
cout << "__/\\\\\\\\\\\\\\\\\\\\\\\\______/\\\\\\\\\\\\\\\\\\____/\\\\\\\\\\";
cout << "\\\\\\\\\\\\_/\\\\\\\\\\\\\\\\\\\\\\\\\\/\\\\\\\\\\\\\\\\\\\\\\\\\\\\_______" << endl;
cout << "__\\/\\\\\\////////\\\\\\__/\\\\\\///////\\\\\\_\\/////\\\\\\///_\\/";
cout << "\\\\\\/////////\\//////\\\\\\/////_______" << endl;
cout << "___\\/\\\\\\______\\//\\\\\\\\/\\\\\\_____\\/\\\\\\_____\\/\\\\\\____";
cout << "\\/\\\\\\______________\\/\\\\\\___________" << endl;
cout << "____\\/\\\\\\_______\\/\\\\\\\\/\\\\\\\\\\\\\\\\\\\\\\/______\\/\\\\\\____\\/";
cout << "\\\\\\\\\\\\\\\\\\\\\\______\\/\\\\\\__________"<< endl;
cout << "_____\\/\\\\\\_______\\/\\\\\\\\/\\\\\\//////\\\\\\______\\/\\\\\\____\\/";
cout << "\\\\\\///////_______\\/\\\\\\_________" << endl;
cout << "______\\/\\\\\\_______\\/\\\\\\\\/\\\\\\____\\//\\\\\\_____";
cout << "\\/\\\\\\____\\/\\\\\\______________\\/\\\\\\________" << endl;
cout << "_______\\/\\\\\\_______/\\\\\\_\\/\\\\\\_____\\//\\\\\\____";
cout << "\\/\\\\\\____\\/\\\\\\______________\\/\\\\\\_______" << endl;
cout << "________\\/\\\\\\\\\\\\\\\\\\\\\\\\/__\\/\\\\\\______\\//\\\\\\/";
cout << "\\\\\\\\\\\\\\\\\\\\\\\\/\\\\\\______________\\/\\\\\\______" << endl;
cout << "_________\\////////////____\\///________\\///\\";
cout << "///////////_\\///_______________\\///______" << endl;
cout << "__________                  _____  ________" ;
cout << "   Version: " << GetVersion() << " rev" << GetRevision() << endl;
cout << "___  ____/__   _______________  /_ ___  __ )";
cout << "______________      ____________________" << endl;
cout << "__  __/  __ | / /  _ \\_  __ \\  __/ __  __  ";
cout << "|_  __/  __ \\_ | /| / /_  ___/  _ \\_  __/" << endl;
cout << "_  /___  __ |/ //  __/  / / / /_   _  /_/ /";
cout << "_  /  / /_/ /_ |/ |/ /_(__  )/  __/  /" << endl;
cout << "/_____/  _____/ \\___//_/ /_/\\__/   /_____/";
cout << " /_/   \\____/____/|__/ /____/ \\___//_/" << endl;
}
//-------------------------------------------------------------------//
EventBrowser::EventBrowser(const TGWindow * p,
                           UInt_t width, UInt_t height,
                           const vector<string> & fileNames,
			   det::Detector* det_descr, 
			   rec::Reconstruct *reconstructor) :
		TGMainFrame(p, width, height),
		fEvent(NULL), fTotEvent(0), fCurrentEventNumber(0),
		fCurrentEventFileName(""),
		fDetector(det_descr),
		fReconstr(reconstructor),
		fMenu(NULL),
		fStatusBar(NULL),
		fMWPCPlots(NULL), fMWPCFrame(NULL), fMWPCTabIndex(0),
		fTabs(NULL){
  Welcome();

  // menu, auger tab, status bar
  const UInt_t menuHeight = 25;
  const UInt_t statusBarHeight = 25;
  const int tmpHeight = (int)height - (int)menuHeight - (int)statusBarHeight;
  // const UInt_t tabHeight = tmpHeight > 1000 ? tmpHeight : 1000;

  fMenu = new EventBrowserMenu(this);
  fTabs = new TGTab(this, width, tmpHeight);

  TGLayoutHints* tabLayout =
    new TGLayoutHints(kLHintsExpandX|kLHintsExpandY|kLHintsCenterX|kLHintsCenterY, 3, 3, 3, 3);
  AddFrame(fTabs, tabLayout);

  fMWPCTabIndex = fTabs->GetNumberOfTabs();
  fMWPCFrame  = fTabs->AddTab("MWPC Plane");
  fMWPCPlots  = new MWPCPlots(fMWPCFrame, fDetector, fReconstr);
  fPlotVector.push_back(fMWPCPlots);


  fStatusBar = new StatusBar(this, width, statusBarHeight);

  MapSubwindows();
  Resize (GetDefaultSize());
  MapWindow();

  // lets go ...
  SetWindowName("EventBrowser");
  InitKeyStrokes();
}
//-------------------------------------------------------------------//
EventBrowser::~EventBrowser() {
  delete fTabs;
  delete fMenu;
//  delete fStatusBar;
}
//-------------------------------------------------------------------//
bool EventBrowser::HandleKey(Event_t *event) {
  unsigned int keysym;
  char str[2];

  gVirtualX->LookupString(event, str, sizeof(str), keysym);

  if (event->fType == kGKeyPress) {
    EKeySym theKeyPressed = (EKeySym) keysym;
    if (theKeyPressed == kKey_q || theKeyPressed == kKey_Q)
      CloseWindow();
    else if (theKeyPressed == kKey_Escape)
      CloseWindow();
    else if (theKeyPressed == kKey_Down || theKeyPressed == kKey_PageDown)
      ChangeEvent(eNextEvent);
    else if (theKeyPressed == kKey_Up || theKeyPressed == kKey_PageUp)
      ChangeEvent(ePreviousEvent);
    else if (theKeyPressed == kKey_Home)
      ChangeEvent(eFirstEvent);
    else if (theKeyPressed == kKey_End)
      ChangeEvent(eLastEvent);
    else if (theKeyPressed == kKey_Space || theKeyPressed == kKey_Backtab)
      ChangeTab(ePreviousTab);
    else if (theKeyPressed == kKey_Tab)
      ChangeTab(eNextTab);
  }
  return kTRUE;
}
//-------------------------------------------------------------------//
Bool_t EventBrowser::ProcessMessage(Long_t msg, Long_t parm1, Long_t parm2) {
  switch (GET_MSG (msg)) {
  case kC_COMMAND:
	switch (GET_SUBMSG (msg)) {
	case kCM_COMBOBOX: {
		if (parm1 == eStatusBarEventList) {
			// fEventFile->ReadEvent(parm2);
			UpdateBrowser();
		}
	} break;
	case kCM_TAB: {
		// if (!fEventInfo.empty()) UpdateTab(parm1);
	} break;
	case kCM_BUTTON: {
		if (parm1 == eNextEvent)
			ChangeEvent(eNextEvent);
		else if (parm1 == ePreviousEvent)
			ChangeEvent(ePreviousEvent);
		else if (parm1 == eFirstEvent)
			ChangeEvent(eFirstEvent);
		else if (parm1 == eLastEvent)
			ChangeEvent(eLastEvent);
	} break;
	case kCM_MENU:
	switch (parm1) {
		// ------------------- THE FILE MENU ----------------
		case eFileOpen: {
			TString directory(".");
			TGFileInfo fi;
			fi.fIniDir  = StrDup(directory.Data());
			const char * rootFiles[] = { "DRIFT Raw Traces files",   "*.drt" ,
				"All files",    "*",
				0,              0 };
			fi.fFileTypes   = rootFiles;
			new TGFileDialog(gClient->GetRoot(), this, kFDOpen, &fi);
			if(fi.fFilename){
				fCurrentEventFileName = string(fi.fFilename);
				fCurrentEventNumber = 1;
				OpenDRTFile();
			}
		} break;
		case eFileQuit:
			CloseWindow();
		break;
		// ------------------- THE HELP MENU ----------------
		case eAbout: {
			EMsgBoxIcon icontype = kMBIconAsterisk;
			EMsgBoxButton buttonType = kMBOk;
			Int_t retval;

			ostringstream info;
			info << "DRIFT EventBrowser " << endl
				<< "Version: rev " << GetRevision() << " from "
				<< GetDate() << endl
				<< "Compiled with ROOT v" << gROOT->GetVersion() << endl
				<< "Authors: Alexei Dorofeev" << endl;
			
			new TGMsgBox(gClient->GetRoot(), gClient->GetRoot(),
				"About",info.str().c_str(),
				icontype, buttonType, &retval);
		} break;
		} // switch
	}
  }
  return kTRUE;
}
//-------------------------------------------------------------------//
void EventBrowser::CloseWindow() {

  // return from TApplication::Run()
  gSystem->ExitLoop();

}
//-------------------------------------------------------------------//
void EventBrowser::UpdateBrowser() {
  const int currentTab = fTabs->GetCurrent();

  // reset everything
  for (unsigned int i=0; i<fPlotVector.size(); ++i) {
    // fPlotVector[i]->Clear();
    // fPlotVector[i]->SetUpToDate(false);
  }


  // fTabs->SetEnabled(fSDTabIndex, bool(fEvent->GetSDEvent().GetEventId()));

  // always stay on selection tab ...
  // if (currentTab == int(fCutTabIndex))
  //  return;

  // ... stay on current tab if active ...
  if (fTabs->IsEnabled(currentTab)) {
    UpdateTab(currentTab);
    return;
  }

}
//-------------------------------------------------------------------//
void EventBrowser::UpdateTab(Long_t iTab) {
  if (iTab < (Long_t) fPlotVector.size()) {
	if (!fPlotVector[iTab]->IsUpToDate()) {
		// fPlotVector[iTab]->Update();
		// fPlotVector[iTab]->SetUpToDate(true);
	}
  }
}
//-------------------------------------------------------------------//
void EventBrowser::UpdateTabsOnOpenFile() {

  /*
  if(fTotEvent < 1)
    return;
   */

  // FileInfo thisFileInfo;
  // fEventFile->ReadFileInfo (thisFileInfo);
  // fCuts->SetEventInfoClassVersion(thisFileInfo.GetEventInfoClassVersion());
  // fCuts->SelectEvents(fEventInfo,fStatusBar->GetEventList());
  // fSDPlots->SetRecStationClassVersion(thisFileInfo.GetSdRecStationClassVersion());
  // fAugerPlots->SetRecStationClassVersion(thisFileInfo.GetSdRecStationClassVersion());

  // if (fIsMC)
  //  fMenu->EnableMC();

  // for (unsigned int i=0; i<fPlotVector.size(); ++i)
  //  fPlotVector[i]->SetShowMC(fIsMC);


  // for (unsigned int i=0;i<fFDPlots.size(); ++i) {
  //  if(thisFileInfo.HasFdTraces())
  //    fFDPlots[i]->EnablePixelTraceTab();
  //  fFDPlots[i]->SetRecStationClassVersion(thisFileInfo.GetSdRecStationClassVersion());
  // }
  ChangeEvent(eCurrentEvent);
}
//-------------------------------------------------------------------//
void EventBrowser::ReadEventList() {
  // get rid of pending events
  gSystem->ProcessEvents();

  // read in event info
  TReadProgress* theProgress =
    new TReadProgress( gClient->GetRoot(), 300, 100);
  theProgress->SetNEvents(fTotEvent);

  // ProcessEvents() is slow, so update only nUpdate times
  const int nUpdate =  100;
  int nEventsForUpdate = fTotEvent / nUpdate;
  if (nEventsForUpdate == 0)
    nEventsForUpdate = 1;

  // fEventInfo.resize(fTotEvent);
  /*
  int i=0;
  while ( fEventFile->GetEventInfos(i,nEventsForUpdate,fEventInfo)) {
    if (theProgress->Event(i))
      break;
    gSystem->ProcessEvents();
    i += nEventsForUpdate;
  }
  */

  delete theProgress;
  gSystem->ProcessEvents();
}
//-------------------------------------------------------------------//
void EventBrowser::OpenDRTFile(){
  cout << "...EventBrowser: Opening DRT file \"" << fCurrentEventFileName  << "\"" << endl;
  // Delete the old Event
  if (fEvent != NULL) delete fEvent; 
  fEvent = NULL;
  fEvent = new evt::Event();
  if ( !fEvent->LoadFromDRTFile(fCurrentEventFileName, fCurrentEventNumber) ) {
  	// This means we couldn't load the file
	// fCurrentEventNumber = 0;
	delete fEvent; fEvent = NULL;
	return;
  }
  cout << "...Loaded event # " << fCurrentEventNumber << " from file" << endl;
  UpdateTabsOnOpenFile();
}
//-------------------------------------------------------------------//
void EventBrowser::ChangeTab(Long_t where) {

  const int nTabs = fTabs->GetNumberOfTabs();
  const int currTab = fTabs->GetCurrent();

  if (where == eNextTab) {
    for (int i = currTab+1; i<nTabs; ++i)
      if (fTabs->IsEnabled(i)) {
        fTabs->SetTab(i);
        return;
      }

    for (int i = 0; i<currTab ; ++i)
      if (fTabs->IsEnabled(i)) {
        fTabs->SetTab(i);
        return;
      }

  }
  else if (where == ePreviousTab) {
    for (int i = currTab-1; i>-1; i--)
      if (fTabs->IsEnabled(i)) {
        fTabs->SetTab(i);
        return;
      }

    for (int i = nTabs-1; i>currTab ; i--)
      if (fTabs->IsEnabled(i)) {
        fTabs->SetTab(i);
        return;
      }

  }
}
//-------------------------------------------------------------------//
void EventBrowser::InitKeyStrokes() {
  BindKey((const TGWindow*)this, gVirtualX->KeysymToKeycode(kKey_Q), kAnyModifier);
  BindKey((const TGWindow*)this, gVirtualX->KeysymToKeycode(kKey_Escape), kAnyModifier);
  BindKey((const TGWindow*)this, gVirtualX->KeysymToKeycode(kKey_Down),kAnyModifier);
  BindKey((const TGWindow*)this, gVirtualX->KeysymToKeycode(kKey_Up), kAnyModifier);
  BindKey((const TGWindow*)this, gVirtualX->KeysymToKeycode(kKey_PageDown),kAnyModifier);
  BindKey((const TGWindow*)this, gVirtualX->KeysymToKeycode(kKey_PageUp), kAnyModifier);
  BindKey((const TGWindow*)this, gVirtualX->KeysymToKeycode(kKey_Home),kAnyModifier);
  BindKey((const TGWindow*)this, gVirtualX->KeysymToKeycode(kKey_End), kAnyModifier);
  BindKey((const TGWindow*)this, gVirtualX->KeysymToKeycode(kKey_Space),kAnyModifier);
  BindKey((const TGWindow*)this, gVirtualX->KeysymToKeycode(kKey_Tab),kAnyModifier);
  BindKey((const TGWindow*)this, gVirtualX->KeysymToKeycode(kKey_Backtab),kAnyModifier);
  BindKey((const TGWindow*)this, gVirtualX->KeysymToKeycode(kKey_P),kAnyModifier);
  BindKey((const TGWindow*)this, gVirtualX->KeysymToKeycode(kKey_S),kAnyModifier);
  BindKey((const TGWindow*)this, gVirtualX->KeysymToKeycode(kKey_A),kAnyModifier);
}
//-------------------------------------------------------------------//
bool EventBrowser::ChangeEvent(Long_t what) {

  cout << "...EventBrowser::ChangeEvent " << endl;

  if (what == eNextEvent ) {
  	fCurrentEventNumber += 1;
	OpenDRTFile();
  } else if (what == ePreviousEvent ) {
  	fCurrentEventNumber -= 1;
	OpenDRTFile();
  }

  for (unsigned long ii = 0; ii < fPlotVector.size();++ii){
  	if (fPlotVector[ii] != NULL) {
  		fPlotVector[ii]->ChangeEvent(fEvent);
	}
  }

  return true;
}
//-------------------------------------------------------------------//
void printHelp() {
  cout << "\n  Usage: EventBrowser [-s <scale factor>] [-k] [-b] <RecEvent files> \n"
       << "     (use -k to keep temporary files, i.e. of the spot animation ...) \n"
       << "     (use -b to produce plots in batch mode)"
       << endl;
}
//-------------------------------------------------------------------//
int main(int argc, char **argv) {

  // Draw a title
  if (gROOT->IsBatch()) {
    cerr << argv[0] << ": cannot run in batch mode";
    return 1;
  }

  // get command line options

  int j = 1;
  double scaleFactor = 1.;
  int c;

  while ((c = getopt (argc, argv, "s:hv")) != -1) {
    switch (c) {
    case 's':
      scaleFactor = atof(optarg);
      j += 2;
      break;
    case 'v':
      EventBrowser::Welcome();
      return 0;
      break;
    default:
      printHelp();
      return 0;
    }
  }

  // read list of filenames from command line

  vector<string> fileNames;
  while ( argv[j] != NULL) {
    fileNames.push_back(string(argv[j]));
    j++;
  }

  // open a root application
  int nArg = 1;
  TApplication theApp("App", &nArg, argv);

  // Set global variables
  gROOT->Reset();
  gStyle->SetOptStat(kFALSE);
  gStyle->SetFrameBorderMode(0);
  gROOT->ForceStyle();

  // Get the screen dimensions
  Int_t  x, y;
  UInt_t w, h;
  gVirtualX->GetWindowSize(gClient->GetRoot()->GetId(),x,y,w,h);

  // The window is slightly smaller than the screen
  UInt_t height = UInt_t(h*0.9*scaleFactor);
  UInt_t width = UInt_t(height*4.5/3.*scaleFactor);

  det::Detector* det_descr = new det::Detector();
  for (unsigned long ii = 0; ii < 48; ++ii) { 
	det_descr->MakeChannel(ii);
  }
  cout << "...Initialized Detector with " << det_descr->GetNofChannels() 
  	<< " Channels " << endl;

  rec::Reconstruct *reconstructor = new rec::Reconstruct();
  reconstructor->SetDetector(det_descr);

  cout << "...Starting Main Window " << width << "x" << height << endl;
  EventBrowser* mainWindow =
    new EventBrowser(gClient->GetRoot(), 
    	width, height, fileNames, det_descr, reconstructor);
  cout << "...Main Window launched " << endl;

  /*
  ostringstream iconName;
  iconName << ADST_CONFIG_DIR << "/eventBrowser.ico";

  mainWindow->SetIconPixmap(iconName.str().c_str());
  */

  if (!mainWindow) {
    cerr << " Could not start up!" << endl;
    exit (1);
  }

  theApp.Run();

  delete det_descr; det_descr=NULL;
  delete reconstructor; reconstructor = NULL;

  return 0;
}
