#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ class EventBrowser+;
#pragma link C++ class VPlots+;
#pragma link C++ class MWPCPlots+;
#pragma link C++ class TGTextMargin+;
#pragma link C++ class StatusBar+;

#endif
