#include "StatusBar.h"
#include "EventBrowserSignals.h"

#include <TGFrame.h>
#include <TGTextBuffer.h>
#include <TGTextEntry.h>
#include <TGButton.h>
#include <TGString.h>
#include <TGLabel.h>
#include <TGComboBox.h>
#include <iostream>
using namespace std;

ClassImp (StatusBar);

//-------------------------------------------------------------------//
StatusBar::StatusBar(TGCompositeFrame *main, UInt_t width, UInt_t height) :
		fMain (main) {
    
  // Horizontal frame
  TGHorizontalFrame *Horizontal = new TGHorizontalFrame (fMain, width, height);
  fMain->AddFrame (Horizontal, new TGLayoutHints (kLHintsExpandX, 3, 3, 3, 3));

  // combo box for the events
  fEventList = new TGComboBox(Horizontal, eStatusBarEventList);
  Horizontal->AddFrame(fEventList, new TGLayoutHints(kLHintsTop|kLHintsLeft,1, 1, 1, 1));
  fEventList->Resize((int) (width*0.5), 20);
  fEventList->Associate(main);

  // navigation buttons 
  TGPictureButton* first = new TGPictureButton(Horizontal, 
	gClient->GetPicture("first_t.xpm"), eFirstEvent);
  TGPictureButton* next = new TGPictureButton(Horizontal, 
	gClient->GetPicture("next_t.xpm"), eNextEvent);
  TGPictureButton* previous = new TGPictureButton(Horizontal, 
	gClient->GetPicture("previous_t.xpm"), ePreviousEvent);
  TGPictureButton* last = new TGPictureButton(Horizontal, 
	gClient->GetPicture("last_t.xpm"), eLastEvent);

  last->SetHeight(22);
  first->SetHeight(22);
  previous->SetHeight(22);
  next->SetHeight(22);

  first->Associate(main);
  previous->Associate(main);
  next->Associate(main);
  last->Associate(main);

  first->SetToolTipText("First event ");
  previous->SetToolTipText("Previous event ");
  next->SetToolTipText("Next event ");
  last->SetToolTipText("Last event ");

  Horizontal->AddFrame(first, new TGLayoutHints(
  	kLHintsTop|kLHintsLeft|kLHintsExpandX,1, 1, 1, 1));
  Horizontal->AddFrame(previous, new TGLayoutHints(
  	kLHintsTop|kLHintsLeft|kLHintsExpandX,1, 1, 1, 1));
  Horizontal->AddFrame(next, new TGLayoutHints(
  	kLHintsTop|kLHintsLeft|kLHintsExpandX,1, 1, 1, 1));
  Horizontal->AddFrame(last, new TGLayoutHints(
  	kLHintsTop|kLHintsLeft|kLHintsExpandX,1, 1, 1, 1));
}
//-------------------------------------------------------------------//
StatusBar::~StatusBar() {
  fMain->Cleanup();
}
//-------------------------------------------------------------------//
