#ifndef _DRIFT_EB_StatusBar_h_
#define _DRIFT_EB_StatusBar_h_

#include <TQObject.h>

class TGCompositeFrame;
class TGComboBox;

class StatusBar : public TQObject {
  public:
	StatusBar(TGCompositeFrame *main, UInt_t width, UInt_t height);
	~StatusBar();

	TGComboBox* GetEventList() {return fEventList;}
  private:
	TGCompositeFrame* fMain;
	TGComboBox* fEventList;

  ClassDef (StatusBar, 1);
}; /* class StatusBar */
#endif /* _DRIFT_EB_StatusBar_h_ */
