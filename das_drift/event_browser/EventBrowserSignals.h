#ifndef _DRIFT_EB_EventBrowserSignals_h_
#define _DRIFT_EB_EventBrowserSignals_h_

// list of signals
enum EButtonSignals {
	// file menu
	eFileOpen,
	eFileQuit,

	// signals for tab navigation control
	eNextTab,
	ePreviousTab,

	// signals for event navigation control
	eFirstEvent,
	eCurrentEvent,
	eNextEvent,
	ePreviousEvent,
	eLastEvent,

	// help menu
	eAbout,

	eStatusBarEventList
}; /* enum EButtonSignals */
#endif /* _DRIFT_EB_EventBrowserSignals_h_ */
