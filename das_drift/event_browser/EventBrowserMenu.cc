#include "EventBrowserMenu.h"
#include "EventBrowserSignals.h"
#include "EventBrowser.h"

#include <TGClient.h>
#include <TGFrame.h>
#include <TGWindow.h>
#include <TGMenu.h>
#include <TGLayout.h>

#include <iostream>
using namespace std;

//-------------------------------------------------------------------//
EventBrowserMenu::EventBrowserMenu(TGCompositeFrame* main) {
  // menu layouts
  fMenuBarLayout = new TGLayoutHints(kLHintsTop | kLHintsLeft | kLHintsExpandX,
				     0, 0, 1, 1);
  fMenuBarItemLayout = new TGLayoutHints(kLHintsTop | kLHintsLeft, 0, 4, 0, 0);
  fMenuBarHelpLayout = new TGLayoutHints(kLHintsTop | kLHintsRight);

  // for the Menu
  fMenuFile = new TGPopupMenu(gClient->GetRoot());
  fMenuFile->AddEntry("Open file ...", eFileOpen);
  fMenuFile->AddSeparator();
  fMenuFile->AddEntry("Quit", eFileQuit);

  // for some help
  fMenuHelp = new TGPopupMenu(gClient->GetRoot());
  fMenuHelp->AddEntry("About", eAbout);

  fMenuFile->Associate(main);
  fMenuHelp->Associate(main);


  fMenuBar = new TGMenuBar(main);
  fMenuBar->AddPopup("File", fMenuFile, fMenuBarItemLayout);
  fMenuBar->AddPopup("Help", fMenuHelp, fMenuBarHelpLayout);
  fMenuBar->DrawBorder();

  main->AddFrame(fMenuBar, fMenuBarLayout);

}
//-------------------------------------------------------------------//
EventBrowserMenu::~EventBrowserMenu() {
  delete fMenuBar;
}
//-------------------------------------------------------------------//
