#ifndef _DRIFT_EB_MWPCPlots_h_
#define _DRIFT_EB_MWPCPlots_h_

#include <TGFrame.h>
#include <TString.h>
#include <TMarker.h>
#include <TGButtonGroup.h>
#include <TGButton.h>
// #include <TH1D.h>

#include "VPlots.h"
#include "EventBrowserSignals.h"

#include <list>

class TCanvas;
class TGWindow;
class TGTab;
class TGHSlider;
class TGCompositeFrame;
class TObjArray;
class TGListBox;
class TRootEmbeddedCanvas;
class TH1D;
class TGTextMargin;

namespace drift{
namespace evt{
  class Event;
}
namespace det{
  class Detector;
}
namespace rec{
  class Reconstruct;
}
}
class MWPCPlots : public TGFrame, public VPlots {
  public:
  	typedef std::pair <unsigned short, TH1D *> HistoPair;

  	typedef std::list< HistoPair > HistoContainer;
	typedef HistoContainer::iterator HistoIterator;
	typedef HistoContainer::const_iterator ConstHistoIterator;
  private:
	MWPCPlots();
  public:
  	MWPCPlots(TGCompositeFrame* main, 
		drift::det::Detector *det_descr,
		drift::rec::Reconstruct* reconstr);	
	~MWPCPlots();

	void SelectChannel();

	/// Upper level interfaces
	bool ChangeEvent(drift::evt::Event* an_event);
  private:
	void ReconUpdateFirstAC();
	void ReconUpdateSecondAC();
	void ReconUpdateCurrent();
	void ReconUpdateCharge();

	void TracesFillFromRawEvent();
	void TracesFillFromFirstAC();
	void TracesFillFromSecondAC();
	void TracesFillFromCurrent();
	void TracesFillFromCharge();
	void TracesUpdateOnCanvas(double vertical_offset);



  	void DrawEventInfo();
	// Takes a numbers for a given channel from
	// detector description and fills electronics values with these numbers
	void LoadElectronicsValues();

	// Takes time ranges for a given Event channel.
	void LoadChanRecValues();

	// SYSTEM ENTRY for signal handling
	Bool_t ProcessMessage (Long_t msg, Long_t parm1, Long_t parm2);
  public:
  	void DoRecLevel(Int_t rec_level);
	void DoVerticalOffset();
	void DoTransimpRf();
	void DoTransimpCf();
	void DoFirstACRf();
	void DoFirstACRg();
	void DoFirstACCg();
	void DoSecondACRf();
	void DoSecondACRg();
	void DoSecondACCg();

	// void FillDefaultTimeRange();
	void DoTimeRangeBeg();
	void DoTimeRangeEnd();

  private: //======== Variables =========//
	drift::det::Detector* fDetector;
	drift::rec::Reconstruct* fReconstr;
	drift::evt::Event* fEvent;


	TGTab* fVariablesTab;
	TGListBox* fChannelsListBox;
	TRootEmbeddedCanvas* fCanvTraces;

	//----- Values For "Options tab"
	TGTextMargin *fOptVertOffsetNumber;	
	//----- Values For "Channel Electronics tab"
	TGTextMargin *fElecTransRfNumber;
	TGTextMargin *fElecTransCfNumber;

	TGTextMargin *fElecAC1RfNumber;
	TGTextMargin *fElecAC1RgNumber;
	TGTextMargin *fElecAC1CgNumber;

	TGTextMargin *fElecAC2RfNumber;
	TGTextMargin *fElecAC2RgNumber;
	TGTextMargin *fElecAC2CgNumber;
	//----- Values For "Channel Rec tab"
	TGTextMargin *fRecSignalBegTimeNumber;
	TGTextMargin *fRecSignalEndTimeNumber;

	//----- Values For Histograms
	HistoContainer fRawHistoVector;
	double fChannelsDrawingOffset;
	unsigned long fCurrentSelectedChannel;
	unsigned short fReconstructionLevel;

  ClassDef (MWPCPlots, 1);
}; /* class MWPCPlots */
#endif /* _DRIFT_EB_MWPCPlots_h_ */
