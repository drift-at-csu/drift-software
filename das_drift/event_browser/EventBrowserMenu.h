#ifndef _DRIFT_EB_EventBrowserMenu_h_
#define _DRIFT_EB_EventBrowserMenu_h_

class TGWindow;
class TGCompositeFrame;
class TGMenuBar;
class TGLayoutHints;
class TGPopupMenu;

class  EventBrowserMenu {
  public:
	EventBrowserMenu  (TGCompositeFrame* main);
	~EventBrowserMenu();

//	bool ToggleShowMCTraces();
  private:
	TGMenuBar* fMenuBar;

	TGLayoutHints* fMenuBarLayout;
	TGLayoutHints* fMenuBarItemLayout;
	TGLayoutHints* fMenuBarHelpLayout;

	TGPopupMenu* fMenuFile;
	TGPopupMenu* fMenuHelp;
}; /* class  EventBrowserMenu */

#endif /* _DRIFT_EB_EventBrowserMenu_h_ */
