#ifndef _DRIFT_EB_EventBrowserUtilities_h_
#define _DRIFT_EB_EventBrowserUtilities_h_

#include <sstream>
#include <string>
#include <iomanip>

//-------------------------------------------------------------------//
template <typename T> std::string stringFromNumber(T value, int precision=0)  {
  std::ostringstream tmp;
  if ( precision != 0 )
    tmp << std::showpoint;
  tmp << std::setiosflags(std::ios::fixed) << std::setprecision(precision)
      << value;
  return tmp.str();
}
//-------------------------------------------------------------------//
#endif /* _DRIFT_EB_EventBrowserUtilities_h_ */
