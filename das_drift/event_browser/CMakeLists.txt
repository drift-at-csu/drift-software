cmake_minimum_required(VERSION 2.6)

set (DRIFTEventBrowser_SOURCES)

# add_definitions("-DDPA")
# add_definitions("-DTLT_MSTC")

include_directories (${DAS_DRIFT_SOURCE_DIR}/event_browser)
include_directories (${DAS_DRIFT_SOURCE_DIR})
include_directories (${Root_INCLUDE_DIRS})
# include_directories (${MySQL_INCLUDE_DIR}/mysql)
# include_directories (${Boost_INCLUDE_DIR})

list ( APPEND DRIFTEventBrowser_SOURCES
	EventBrowserMenu.cc
	StatusBar.cc
	TReadProgress.cc
	MWPCPlots.cc
	TGTextMargin.cc
)

Root_GENERATE_DICTIONARY ( EventBrowserDict
	LINKDEF EventBrowserLinkDef.h
	INFILES EventBrowser.h
		StatusBar.h
		EventBrowserConst.h
		EventBrowserSignals.h
		EventBrowserUtilities.h
		MWPCPlots.h
		VPlots.h
		TGTextMargin.h
	INCLUDE_DIRS ${DAS_DRIFT_SOURCE_DIR}/event_browser
		${DAS_DRIFT_SOURCE_DIR}
)
list (APPEND DRIFTEventBrowser_SOURCES ${EventBrowserDict_DICT_SOURCE})

add_library (DRIFT_EBrowser  STATIC ${DRIFTEventBrowser_SOURCES})

# add_dependencies (TektDAQ	vxi11)
# link_directories (${Root_LIBDIR})

add_executable (drift_event_browser EventBrowser.cc)
target_link_libraries (drift_event_browser
	DRIFT_EBrowser
	DRIFT_Event
	DRIFT_Util
	DRIFT_Detector
	DRIFT_Reconstruct
	${Root_LIBRARIES}
#	${Boost_LIBRARIES}
#	${MySQL_LIBRARIES}
)
