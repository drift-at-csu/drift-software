#ifndef _DRIFT_EB_TReadProgress_h_
#define _DRIFT_EB_TReadProgress_h_

#ifndef ROOT_TGFrame
#include "TGFrame.h"
#endif

#include "TGProgressBar.h"

class TGTextButton;
class TGVerticalFrame;
class TGLayoutHints;

class TReadProgress : public TGMainFrame {
  private:
	TGTextButton      *fStop;
	TGHProgressBar    *fBar;
	TGVerticalFrame   *fFrame;
	TGLayoutHints     *fLayF, *fLayB, *fLayS;
	int                fNEvents;
	Bool_t             fS;
  public:  
	TReadProgress(const TGWindow *p, UInt_t w, UInt_t h, 
		const char * windowName = NULL);
	virtual ~TReadProgress();

	virtual Bool_t ProcessMessage(Long_t msg, Long_t parm1, Long_t parm2);
	virtual void CloseWindow();
  
	void SetNEvents(int evs) { fBar->SetRange(0,evs);}
	Bool_t Event(int i) {  fBar->SetPosition(i); fBar->Layout(); return fS;}
}; /* class TReadProgress */
#endif /* _DRIFT_EB_TReadProgress_h_ */




