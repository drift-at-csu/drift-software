#include "MWPCPlots.h"
#include "EventBrowserSignals.h"
#include "EventBrowserConst.h"
#include "EventBrowserUtilities.h"

#include <TGFrame.h>
#include <TGSlider.h>
#include <TRootEmbeddedCanvas.h>
#include <TCanvas.h>
#include <TGTab.h>
#include <TGraphErrors.h>
#include <TGListBox.h>
#include <TGNumberEntry.h>
#include <TLegend.h>
#include <TLatex.h>
#include <TGLabel.h>
#include <TGaxis.h>
#include <TSpline.h>
#include <TH2F.h>
#include <TSystem.h>

#include <TGTextMargin.h>

#include <cmath>
#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdlib>
#include <algorithm>
#include <iomanip>
using namespace std;

using namespace view::Consts;

using namespace drift;
#include <evt/Event.h>
#include <evt/Channel.h>
#include <det/Detector.h>
#include <det/Channel.h>
#include <utl/Trace.h>
#include <rec/Reconstruct.h>

ClassImp (MWPCPlots);
//-------------------------------------------------------------------//
MWPCPlots::MWPCPlots(TGCompositeFrame* main, 
		det::Detector *det_descr,
		rec::Reconstruct* reconstr):
	fDetector(det_descr),
	fReconstr(reconstr),
	fEvent(NULL),
	fVariablesTab(NULL), fChannelsListBox(NULL), fCanvTraces(NULL), 
	fOptVertOffsetNumber(NULL), 
	fChannelsDrawingOffset(20.0), fCurrentSelectedChannel(0),
	fReconstructionLevel(1){

  const double width  = main->GetWidth();
  const double height = main->GetHeight();

  cout << "...Creating MWPC Plots in a Tab:" << width << "x" << height << endl;

  //------ Hints -------//
  TGLayoutHints* mainLayout = new TGLayoutHints(
  	kLHintsExpandX, 1,1,1,1);
  // TGLayoutHints* centeredLayout= new TGLayoutHints(
  //	kLHintsCenterX|kLHintsCenterY, 1,1,1,1);
  // TGLayoutHints* centeredXLayout= new TGLayoutHints(
  //	kLHintsCenterX,1,1,1,1);
  // TGLayoutHints* centeredExpandXYLayout = new TGLayoutHints(
  // 	kLHintsExpandX|kLHintsExpandY|kLHintsCenterX|kLHintsCenterY,1,1,1,1);
  TGLayoutHints* expandXYLayout = new TGLayoutHints(kLHintsExpandX|
	kLHintsExpandY,1,1,1,1);
  TGLayoutHints* expandYLayout = new TGLayoutHints(
	kLHintsExpandY,1,1,1,1);
  //------ End of layout hints --------//
  //-------- Frames -------------------//
  TGHorizontalFrame* mainHorTopFrame = new TGHorizontalFrame(main,
	UInt_t(width), UInt_t(0.78*height));
  TGHorizontalFrame* mainHorBotFrame = new TGHorizontalFrame(main,
  	UInt_t(width), UInt_t(0.22*height));
  main->AddFrame(mainHorTopFrame, mainLayout);
  main->AddFrame(mainHorBotFrame, mainLayout);

  cout << "...MWPC: Created 2 Frames:" << mainHorTopFrame->GetWidth() << "x"
  	<< mainHorTopFrame->GetHeight()  << " and " 
	<< mainHorBotFrame->GetWidth() << "x" << mainHorBotFrame->GetHeight() 
	<< endl;


  //--------- Canva in which traces are drawn --------------//
  TGVerticalFrame* tracesFrame = new TGVerticalFrame(mainHorTopFrame,
	UInt_t(0.7*mainHorTopFrame->GetWidth()), mainHorTopFrame->GetHeight());
  mainHorTopFrame->AddFrame(tracesFrame, expandYLayout);
  fCanvTraces = new TRootEmbeddedCanvas(
  		"tracesCanvas", tracesFrame,
		tracesFrame->GetWidth(), tracesFrame->GetHeight());
  tracesFrame->AddFrame(fCanvTraces, expandXYLayout);

  //---------- This is a list of channels in the event -------//
  TGVerticalFrame* channelsListFrame = new TGVerticalFrame(mainHorTopFrame,
	UInt_t(0.3*mainHorTopFrame->GetWidth()), mainHorTopFrame->GetHeight());
  mainHorTopFrame->AddFrame(channelsListFrame, expandYLayout);
  TGTab* fChannelsListTab = new TGTab(channelsListFrame,
  	channelsListFrame->GetWidth(), channelsListFrame->GetHeight());
  channelsListFrame->AddFrame(fChannelsListTab, expandYLayout);

  TGCompositeFrame *channelsInfoFrame = fChannelsListTab->AddTab("Channels");

  fChannelsListBox = new TGListBox(channelsInfoFrame, 49);
  fChannelsListBox->Connect("Selected(Int_t)", "MWPCPlots",
  	this, "SelectChannel()");
  channelsInfoFrame->AddFrame(fChannelsListBox, expandXYLayout);

  for (det::Detector::ConstChannelIterator iter = fDetector->ChannelsBegin();
  	iter != fDetector->ChannelsEnd(); ++iter) {

	TString chan_name = "Chan "; 
	chan_name += (*iter)->GetId();
  	fChannelsListBox->InsertEntry( chan_name, (*iter)->GetId(), (*iter)->GetId() );
  }
//  fChannelsListBox->MapSubwindows();

  //------------ Variables Tabs  ----------//
  fVariablesTab = new TGTab(mainHorBotFrame,
	mainHorBotFrame->GetWidth(), mainHorBotFrame->GetHeight());
  mainHorBotFrame->AddFrame(fVariablesTab, 
  	new TGLayoutHints(kLHintsTop|kLHintsExpandX,1,1,1,1));

  TGCompositeFrame *optFrame = fVariablesTab->AddTab("Options");
  optFrame->ChangeOptions(optFrame->GetOptions() | kHorizontalFrame);
  TGCompositeFrame *elecFrame = fVariablesTab->AddTab("Channel Electronics");
  elecFrame->ChangeOptions(elecFrame->GetOptions() | kHorizontalFrame);
  TGCompositeFrame *recFrame = fVariablesTab->AddTab("Channel Reconstruction");
  recFrame->ChangeOptions(recFrame->GetOptions() | kHorizontalFrame);

  //-------- Reconstruction Level Buttons -----//
  TGVButtonGroup *recLevelButtons = new TGVButtonGroup(optFrame,"Rec Level");
  optFrame->AddFrame(recLevelButtons, 
  	new TGLayoutHints(kLHintsTop,1,1,1,1));
  TGRadioButton *fRecLevelButton1 = new TGRadioButton(recLevelButtons, "Raw", 1);
  // TGRadioButton *fRecLevelButton2 = new TGRadioButton(recLevelButtons, "First AC", 2);
  // TGRadioButton *fRecLevelButton3 = new TGRadioButton(recLevelButtons, "Second AC", 3);
  // TGRadioButton *fRecLevelButton4 = new TGRadioButton(recLevelButtons, "Current", 4);
  // TGRadioButton *fRecLevelButton5 = new TGRadioButton(recLevelButtons, "Charge", 5);
  recLevelButtons->SetRadioButtonExclusive();
  fRecLevelButton1->SetState(kButtonDown);
  recLevelButtons->Connect("Pressed(Int_t)", "MWPCPlots", 
  	this, "DoRecLevel(Int_t)");

  fOptVertOffsetNumber = new TGTextMargin(optFrame, "Vertical Offset");
  optFrame->AddFrame(fOptVertOffsetNumber,
	new TGLayoutHints(kLHintsTop|kLHintsExpandX, 3, 2, 2, 2));
  fOptVertOffsetNumber->GetEntry()->Connect("TextChanged(char*)", "MWPCPlots",
	this, "DoVerticalOffset()");
  fOptVertOffsetNumber->GetEntry()->SetNumber(fChannelsDrawingOffset);

  //-------- Electronics Parameters: TransImpedance -----//
  TGGroupFrame *elecParamsTransimpFrame = new TGGroupFrame(elecFrame, "Transimpedance");
  elecFrame->AddFrame(elecParamsTransimpFrame, 
  	new TGLayoutHints(kLHintsTop|kLHintsExpandY,0,0,2,2));

  fElecTransRfNumber = new TGTextMargin(elecParamsTransimpFrame, "Rf [MOhms]");
  elecParamsTransimpFrame->AddFrame(fElecTransRfNumber, 
  	new TGLayoutHints(kLHintsTop|kLHintsExpandX, 3, 2, 2, 2));
  fElecTransRfNumber->GetEntry()->SetFormat(TGNumberFormat::kNESRealOne);
  fElecTransRfNumber->GetEntry()->Connect("TextChanged(char*)", "MWPCPlots",
	this, "DoTransimpRf()");

  fElecTransCfNumber = new TGTextMargin(elecParamsTransimpFrame, "Cf [pF]");
  elecParamsTransimpFrame->AddFrame(fElecTransCfNumber, 
  	new TGLayoutHints(kLHintsTop|kLHintsExpandX, 3, 2, 2, 2));
  fElecTransCfNumber->GetEntry()->SetFormat(TGNumberFormat::kNESRealOne);
  fElecTransCfNumber->GetEntry()->Connect("TextChanged(char*)", "MWPCPlots",
  	this, "DoTransimpCf()");

  //-------- Electronics Parameters: AC First -------//
  TGGroupFrame *elecParamsAC1Frame = new TGGroupFrame(elecFrame, "First AC Coupling");
  elecFrame->AddFrame(elecParamsAC1Frame,
  	new TGLayoutHints(kLHintsTop|kLHintsExpandY,0,0,2,2));
  
  fElecAC1RfNumber = new TGTextMargin(elecParamsAC1Frame, "Rf [Ohms]");
  elecParamsAC1Frame->AddFrame(fElecAC1RfNumber, new TGLayoutHints(kLHintsExpandX, 0, 0, 2, 2));
  fElecAC1RfNumber->GetEntry()->SetFormat(TGNumberFormat::kNESRealOne);
  fElecAC1RfNumber->GetEntry()->Connect("TextChanged(char*)", "MWPCPlots",
	this, "DoFirstACRf()");

  fElecAC1RgNumber = new TGTextMargin(elecParamsAC1Frame, "Rg [Ohms]");
  elecParamsAC1Frame->AddFrame(fElecAC1RgNumber, new TGLayoutHints(kLHintsExpandX, 0, 0, 2, 2));
  fElecAC1RgNumber->GetEntry()->SetFormat(TGNumberFormat::kNESRealOne);
  fElecAC1RgNumber->GetEntry()->Connect("TextChanged(char*)", "MWPCPlots",
	this, "DoFirstACRg()");

  fElecAC1CgNumber = new TGTextMargin(elecParamsAC1Frame, "Cg [uF]");
  elecParamsAC1Frame->AddFrame(fElecAC1CgNumber, new TGLayoutHints(kLHintsExpandX, 0, 0, 2, 2));
  fElecAC1CgNumber->GetEntry()->SetFormat(TGNumberFormat::kNESRealOne);
  fElecAC1CgNumber->GetEntry()->Connect("TextChanged(char*)", "MWPCPlots",
	this, "DoFirstACCg()");

 
  //-------- Electronics Parameters: AC Second -------//
  TGGroupFrame *elecParamsAC2Frame = new TGGroupFrame(elecFrame, "Second AC Coupling");
  elecFrame->AddFrame(elecParamsAC2Frame,
  	new TGLayoutHints(kLHintsTop|kLHintsExpandY,0,0,2,2));

  fElecAC2RfNumber = new TGTextMargin(elecParamsAC2Frame, "Rf [Ohms]");
  elecParamsAC2Frame->AddFrame(fElecAC2RfNumber, new TGLayoutHints(kLHintsExpandX, 0, 0, 2, 2));
  fElecAC2RfNumber->GetEntry()->SetFormat(TGNumberFormat::kNESRealOne);
  fElecAC2RfNumber->GetEntry()->Connect("TextChanged(char*)", "MWPCPlots",
  	this, "DoSecondACRf()");

  fElecAC2RgNumber = new TGTextMargin(elecParamsAC2Frame, "Rg [Ohms]");
  elecParamsAC2Frame->AddFrame(fElecAC2RgNumber, new TGLayoutHints(kLHintsExpandX, 0, 0, 2, 2));
  fElecAC2RgNumber->GetEntry()->SetFormat(TGNumberFormat::kNESRealOne);
  fElecAC2RgNumber->GetEntry()->Connect("TextChanged(char*)", "MWPCPlots",
	this, "DoSecondACRg()");

  fElecAC2CgNumber = new TGTextMargin(elecParamsAC2Frame, "Cg [uF]");
  elecParamsAC2Frame->AddFrame(fElecAC2CgNumber, new TGLayoutHints(kLHintsExpandX, 0, 0, 2, 2));
  fElecAC2CgNumber->GetEntry()->SetFormat(TGNumberFormat::kNESRealOne);
  fElecAC2CgNumber->GetEntry()->Connect("TextChanged(char*)", "MWPCPlots",
	this, "DoSecondACCg()");


  //-------- Rec Parameters: Time Interval -----//
  TGGroupFrame *recTimeIntervalFrame = new TGGroupFrame(recFrame, "TimeInterval");
  recFrame->AddFrame(recTimeIntervalFrame,
	new TGLayoutHints(kLHintsTop|kLHintsExpandY,0,0,2,2));

  fRecSignalBegTimeNumber = new TGTextMargin(recTimeIntervalFrame, "Signal Begins [usec]");
  recTimeIntervalFrame->AddFrame(fRecSignalBegTimeNumber, 
  	new TGLayoutHints(kLHintsExpandX, 0, 0, 2, 2));
  fRecSignalBegTimeNumber->GetEntry()->SetFormat(TGNumberFormat::kNESReal);
  fRecSignalBegTimeNumber->GetEntry()->Connect("TextChanged(char*)", "MWPCPlots",
	this, "DoTimeRangeBeg()");

  fRecSignalEndTimeNumber = new TGTextMargin(recTimeIntervalFrame, "Signal Ends [usec]");
  recTimeIntervalFrame->AddFrame(fRecSignalEndTimeNumber, 
	new TGLayoutHints(kLHintsExpandX, 0, 0, 2, 2));
  fRecSignalEndTimeNumber->GetEntry()->SetFormat(TGNumberFormat::kNESReal);
  fRecSignalEndTimeNumber->GetEntry()->Connect("TextChanged(char*)", "MWPCPlots",
	this, "DoTimeRangeEnd()");

  fVariablesTab->SetEnabled(1, kFALSE); // 1 is channel electronics
  fVariablesTab->SetEnabled(2, kFALSE); // 2 is channel rec parameters
}
//-------------------------------------------------------------------//
MWPCPlots::~MWPCPlots() {
  for (HistoIterator iter = fRawHistoVector.begin(); iter != fRawHistoVector.end(); ++iter) {
	if (iter->second !=NULL ) {
		delete (iter->second);
		iter->second = NULL;
	}
  }
}
//-------------------------------------------------------------------//
bool MWPCPlots::ChangeEvent(drift::evt::Event* an_event){
  cout << "...MWPCPlots::ChangeEvent: Changing Event" << endl;

  fEvent=an_event;
  //Let's clear all reconstruction levels
  fReconstr->ClearFirstAC();
  fReconstr->ClearSecondAC();
  fReconstr->ClearCurrent();
  fReconstr->ClearCharge();
  // And assign Event to reconstruction chain
  fReconstr->SetEvent(an_event);
  
  if (fReconstructionLevel == 1) {
	TracesFillFromRawEvent();
  } else if (fReconstructionLevel == 2) {
  	ReconUpdateFirstAC();
	TracesFillFromFirstAC();
  } else if (fReconstructionLevel == 3) {
  	ReconUpdateSecondAC();
	TracesFillFromSecondAC();
  } else if (fReconstructionLevel == 4) {
  	ReconUpdateCurrent();
	TracesFillFromCurrent();
  } else if (fReconstructionLevel == 5) {
	ReconUpdateCharge();
	TracesFillFromCharge();
  } else {
  	TracesFillFromRawEvent();
  }

  LoadChanRecValues();
  TracesUpdateOnCanvas(fChannelsDrawingOffset);

  return true;
}
//-------------------------------------------------------------------//
void  MWPCPlots::ReconUpdateFirstAC(){
  cout << "...MWPCPlots::ReconUpdateFirstAC " << endl;
  if (fReconstr == NULL) {
  	cout << "...MWPCPlots::ReconUpdateFirstAC: Reconstruction is NULL " << endl;
	return;
  }
  if (fEvent == NULL) {
  	cout << "...MWPCPlots::ReconUpdateFirstAC: Event is NULL " << endl;
	return;
  }
  // Let's go and reconstruct if we need each trace in event
  for (evt::Event::ConstChannelIterator iter = fEvent->ChannelsBegin();
                  iter != fEvent->ChannelsEnd(); ++iter) {
	if (! fReconstr->HasFirstACChannel((*iter)->GetId()) ){
		// We don't have a first reconstruction for this channel
		fReconstr->RecFirstAC((*iter)->GetId());
	}
  }
}
//-------------------------------------------------------------------//
void  MWPCPlots::ReconUpdateSecondAC(){
  cout << "...MWPCPlots::ReconUpdateSecondAC " << endl;
  if (fReconstr == NULL) {
	cout << "...MWPCPlots::ReconUpdateSecondAC: Reconstruction is NULL " << endl;
	return;
  }
  if (fEvent == NULL) {
	cout << "...MWPCPlots::ReconUpdateSecondAC: Event is NULL " << endl;
	return;
  }
  // Let's go and reconstruct if we need each trace in event
  for (evt::Event::ConstChannelIterator iter = fEvent->ChannelsBegin();
		iter != fEvent->ChannelsEnd(); ++iter) {
	// We need a first AC reconstruction for the channel if
	// we don't have one
	if (! fReconstr->HasFirstACChannel((*iter)->GetId()) ){
		fReconstr->RecFirstAC((*iter)->GetId());
	}
	// And then we reconstruct the second AC if we don't have it
	if (!fReconstr->HasSecondACChannel((*iter)->GetId()) ){
		fReconstr->RecSecondAC((*iter)->GetId(), true);
	}
  }
}
//-------------------------------------------------------------------//
void MWPCPlots::ReconUpdateCurrent(){
  cout << "...MWPCPlots::ReconUpdateCurrent " << endl;
  if (fReconstr == NULL) {
	cout << "...MWPCPlots::ReconUpdateCurrent: Reconstruction is NULL " << endl;
	return;
  }
  if (fEvent == NULL) {
	cout << "...MWPCPlots::ReconUpdateCurrent: Event is NULL " << endl;
	return;
  }
  // Let's go and reconstruct if we need each trace in event
  for (evt::Event::ConstChannelIterator iter = fEvent->ChannelsBegin();
		iter != fEvent->ChannelsEnd(); ++iter) {
	// We need a first AC reconstruction for the channel if
	// we don't have one
	if (! fReconstr->HasFirstACChannel((*iter)->GetId()) ){
		fReconstr->RecFirstAC((*iter)->GetId());
	}
	// And then we reconstruct the second AC if we don't have it
	if (!fReconstr->HasSecondACChannel((*iter)->GetId()) ){
		fReconstr->RecSecondAC((*iter)->GetId(), true);
	}
	// And then we reconstruct Current if we don't have it
	if (! fReconstr->HasCurrentChannel((*iter)->GetId()) ){
		fReconstr->RecCurrent((*iter)->GetId());
	}
  }	
}
//-------------------------------------------------------------------//
void MWPCPlots::ReconUpdateCharge(){
  cout << "...MWPCPlots::ReconUpdateCharge " << endl;
  if (fReconstr == NULL) {
	cout << "...MWPCPlots::ReconUpdateCharge: Reconstruction is NULL " << endl;
	return;
  }
  if (fEvent == NULL) {
	cout << "...MWPCPlots::ReconUpdateCharge: Event is NULL " << endl;
	return;
  }
  // Let's go and reconstruct if we need each trace in event
  for (evt::Event::ConstChannelIterator iter = fEvent->ChannelsBegin();
		iter != fEvent->ChannelsEnd(); ++iter) {
	// We need a first AC reconstruction for the channel if
	// we don't have one
	if (! fReconstr->HasFirstACChannel((*iter)->GetId()) ){
		fReconstr->RecFirstAC((*iter)->GetId());
	}
	// And then we reconstruct the second AC if we don't have it
	if (!fReconstr->HasSecondACChannel((*iter)->GetId()) ){
		fReconstr->RecSecondAC((*iter)->GetId(), true);
	}
	// And then we reconstruct Current if we don't have it
	if (! fReconstr->HasCurrentChannel((*iter)->GetId()) ){
		fReconstr->RecCurrent((*iter)->GetId());
	}
	// Let's reconstruct charge
	if (! fReconstr->HasChargeChannel((*iter)->GetId()) ){
		fReconstr->RecCharge((*iter)->GetId());
	}
  }
}
//-------------------------------------------------------------------//
void MWPCPlots::TracesFillFromRawEvent(){
  // Let's Clean the prev histos
  for (HistoIterator iter = fRawHistoVector.begin(); iter != fRawHistoVector.end(); ++iter) {
	if (iter->second !=NULL ) {
		delete (iter->second);
		iter->second = NULL;
	}
  }
  fRawHistoVector.clear();
  if (fEvent == NULL) {
	cout << "...TracesFillFromRawEvent: Event is NULL" << endl;
	return;
  }

  for (evt::Event::ConstChannelIterator iter = fEvent->ChannelsBegin();
		iter != fEvent->ChannelsEnd(); ++iter) {
	for (evt::Channel::ConstTraceIterator trc_iter = (*iter)->TracesBegin();
			trc_iter != (*iter)->TracesEnd(); ++trc_iter) {

		cout << "...Filling CHan " << (*iter)->GetId()
			<< ", Trace" << (*trc_iter)->GetId() << endl;

		TString histo_name = "chan_";
		histo_name += (Int_t) (*iter)->GetId();
		histo_name += "_";
		histo_name += (Int_t) (*trc_iter)->GetId();
		histo_name += "_raw_histo";
		TString histo_title = " Channel "; histo_title += " Raw Trace";

		TH1D * current_histo = new TH1D(
				histo_name, histo_title,
				(*trc_iter)->GetNofTimeBins(),
				(*trc_iter)->GetTimeOffset(),
				((*trc_iter)->GetBinDuration()*
					(*trc_iter)->GetNofTimeBins()) +
					(*trc_iter)->GetTimeOffset());

		fRawHistoVector.push_back( std::make_pair( (*iter)->GetId(),
			current_histo) );
		for (unsigned long ii = 0; ii < (*trc_iter)->GetNofTimeBins(); ++ii){
			current_histo->SetBinContent( ii+1, (*trc_iter)->GetTimeBin(ii) );
		}
	}
  }
}
//-------------------------------------------------------------------//
void MWPCPlots::TracesFillFromFirstAC(){
  // Let's Clean the prev histos
  for (HistoIterator iter = fRawHistoVector.begin(); iter != fRawHistoVector.end(); ++iter) {
	if (iter->second !=NULL ) {
		delete (iter->second);
		iter->second = NULL;
	}
  }
  fRawHistoVector.clear();
  if (fReconstr == NULL) {
	cout << "...TracesFillFromFirstAC: Reconstr is NULL" << endl;
	return;
  }
  /*
  for (rec::Reconstruct::ConstChannelIterator iter = fReconstr->ChanFirstACBegin();
		iter != fReconstr->ChanFirstACEnd(); ++iter) {
	TString histo_name = "chan_";
	histo_name += (Int_t) (*iter)->GetId(); histo_name += "_ac1_histo";
	TString histo_title = " Channel "; histo_title += " First AC Trace";
	fRawHistoVector.insert(std::pair<unsigned long,TH1D*>((*iter)->GetId(),
		new TH1D( histo_name, histo_title,
			(*iter)->GetTrace().GetNofTimeBins(),
			0.0, (*iter)->GetTrace().GetBinDuration()*
			(*iter)->GetTrace().GetNofTimeBins()
		)
	));
	for (unsigned long ii = 0; ii < (*iter)->GetTrace().GetNofTimeBins(); ++ii){
		fRawHistoVector[(*iter)->GetId()]->SetBinContent( ii+1,
			(*iter)->GetTrace().GetTimeBin(ii) );
	}
  }
  */
}
//-------------------------------------------------------------------//
void MWPCPlots::TracesFillFromSecondAC(){
  // Let's Clean the prev histos
  for (HistoIterator iter = fRawHistoVector.begin(); iter != fRawHistoVector.end(); ++iter) {
	if (iter->second !=NULL ) {
	        delete (iter->second);
		iter->second = NULL;
	}
  }
  fRawHistoVector.clear();

  if (fReconstr == NULL) {
	cout << "...TracesFillFromSecondAC: Reconstr is NULL" << endl;
	return;
  }
  /*
  for (rec::Reconstruct::ConstChannelIterator iter = fReconstr->ChanSecondACBegin();
		iter != fReconstr->ChanSecondACEnd(); ++iter) {
	TString histo_name = "chan_";
	histo_name += (Int_t) (*iter)->GetId(); histo_name += "_ac2_histo";
	TString histo_title = " Channel "; histo_title += " Second AC Trace";
	fRawHistoVector.insert(std::pair<unsigned long,TH1D*>((*iter)->GetId(),
		new TH1D( histo_name, histo_title,
			(*iter)->GetTrace().GetNofTimeBins(), 0.0, 
			(*iter)->GetTrace().GetBinDuration()* 
			(*iter)->GetTrace().GetNofTimeBins()
		)
	));
  	for (unsigned long ii = 0; ii < (*iter)->GetTrace().GetNofTimeBins(); ++ii){
		fRawHistoVector[(*iter)->GetId()]->SetBinContent( ii+1,
			(*iter)->GetTrace().GetTimeBin(ii) );
	}
  }
  */
}
//-------------------------------------------------------------------//
void MWPCPlots::TracesFillFromCurrent(){
  // Let's Clean the prev histos
  for (HistoIterator iter = fRawHistoVector.begin(); iter != fRawHistoVector.end(); ++iter) {
	if (iter->second !=NULL ) {
	        delete (iter->second);
		iter->second = NULL;
	}
  }
  fRawHistoVector.clear();
  if (fReconstr == NULL) {
	cout << "...TracesFillFromCurrent: Reconstr is NULL" << endl;
	return;
  }
  /*
  for (rec::Reconstruct::ConstChannelIterator iter = fReconstr->ChanCurrentBegin();
		iter != fReconstr->ChanCurrentEnd(); ++iter) {
	TString histo_name = "chan_";
	histo_name += (Int_t) (*iter)->GetId(); histo_name += "_current_histo";
	TString histo_title = " Channel "; histo_title += " Current Trace";
	fRawHistoVector.insert(std::pair<unsigned long,TH1D*>((*iter)->GetId(),
		new TH1D( histo_name, histo_title,
			(*iter)->GetTrace().GetNofTimeBins(), 0.0,
			(*iter)->GetTrace().GetBinDuration()*
			(*iter)->GetTrace().GetNofTimeBins()
		)
	));
	for (unsigned long ii = 0; ii < (*iter)->GetTrace().GetNofTimeBins(); ++ii){
		fRawHistoVector[(*iter)->GetId()]->SetBinContent( ii+1,
			(*iter)->GetTrace().GetTimeBin(ii) );
	}
  }
  */
}
//-------------------------------------------------------------------//
void MWPCPlots::TracesFillFromCharge(){
  // Let's Clean the prev histos
  for (HistoIterator iter = fRawHistoVector.begin(); iter != fRawHistoVector.end(); ++iter) {
	if (iter->second !=NULL ) {
		delete (iter->second);
		iter->second = NULL;
	}
  }
  fRawHistoVector.clear();
  if (fReconstr == NULL) {
	cout << "...TracesFillFromCharge: Reconstr is NULL" << endl;
	return;
  }
  /*
  for (rec::Reconstruct::ConstChannelIterator iter = fReconstr->ChanChargeBegin();
		iter != fReconstr->ChanChargeEnd(); ++iter) {
	TString histo_name = "chan_";
	histo_name += (Int_t) (*iter)->GetId(); histo_name += "_charge_histo";
	TString histo_title = " Channel "; histo_title += " Charge Trace";
	fRawHistoVector.insert(std::pair<unsigned long,TH1D*>((*iter)->GetId(),
		new TH1D( histo_name, histo_title,
			(*iter)->GetTrace().GetNofTimeBins(), 0.0,
			(*iter)->GetTrace().GetBinDuration()*
			(*iter)->GetTrace().GetNofTimeBins()
		)
	));
	for (unsigned long ii = 0; ii < (*iter)->GetTrace().GetNofTimeBins(); ++ii){
		fRawHistoVector[(*iter)->GetId()]->SetBinContent( ii+1,
			(*iter)->GetTrace().GetTimeBin(ii) );
	}
  }
  */
}
//-------------------------------------------------------------------//
void MWPCPlots::TracesUpdateOnCanvas(double vertical_offset){
  fCanvTraces->GetCanvas()->cd();
  fCanvTraces->GetCanvas()->Clear();

  double vert_max_value = 0.0;
  double vert_min_value = 0.0;

  double horiz_max_value = 0.0;
  double horiz_min_value = 0.0;

  // TAxis *vertical_axis = NULL;
  // TAxis *horisontal_axis = NULL;

  unsigned short first_bin_number = 0;
  double current_trace_offset = 0.0;

  // Calculating the horizontal axis first
  for (HistoIterator iter = fRawHistoVector.begin(); iter != fRawHistoVector.end(); ++iter) {
	if (first_bin_number == 0) {
		first_bin_number = 1;
		horiz_max_value = iter->second->GetXaxis()->GetXmax();
		horiz_min_value = iter->second->GetXaxis()->GetXmin();
	} else{
		if ( horiz_max_value < iter->second->GetXaxis()->GetXmax() )
			horiz_max_value = iter->second->GetXaxis()->GetXmax();
		if ( horiz_min_value > iter->second->GetXaxis()->GetXmin() )
			horiz_min_value = iter->second->GetXaxis()->GetXmin();
	}
	
  }
  // draw a frame to define the range
  TH1F *hr = fCanvTraces->GetCanvas()->DrawFrame(
  	horiz_min_value,0.0,
  	horiz_max_value,2000.0);
  hr->SetXTitle("Time, usec");
  hr->SetYTitle("Voltage, mV");
  // pad->GetFrame()->SetFillColor(21);
  // pad->GetFrame()->SetBorderSize(12);
  cout << "...MWPCPlots::TracesUpdateOnCanvas Horizontal Range: " 
  	<< horiz_min_value << "us to " << horiz_max_value << "us" << endl;

  first_bin_number = 0;
  for (HistoIterator iter = fRawHistoVector.begin(); iter != fRawHistoVector.end(); ++iter) {
	// Doing Offset first
	if (vertical_offset != 0) {
		current_trace_offset = vertical_offset*iter->first;
		for (unsigned long ii = 0; ii < iter->second->GetNbinsX(); ++ii){
			iter->second->SetBinContent( ii+1,
				iter->second->GetBinContent(ii+1) + current_trace_offset );
		}
	}

	if (first_bin_number == 0) {
		first_bin_number = 1;
		iter->second->Draw("SAME][");
		vert_max_value = iter->second->GetBinContent(iter->second->GetMaximumBin());
		vert_min_value  = iter->second->GetBinContent(iter->second->GetMinimumBin());
	} else {
		iter->second->Draw("SAME][");
		if (vert_max_value < iter->second->GetBinContent(iter->second->GetMaximumBin()))
			vert_max_value = iter->second->GetBinContent(iter->second->GetMaximumBin());
		if (vert_min_value > iter->second->GetBinContent(iter->second->GetMinimumBin()))
			vert_min_value = iter->second->GetBinContent(iter->second->GetMinimumBin());

	}
	/*
	if ( fCurrentSelectedChannel == iter->first ) {
		iter->second->SetLineWidth(3.0);
	} else {
		iter->second->SetLineWidth(1.0);
	}
	*/
  }
  // Let's update vertical scale
  hr->GetYaxis()->SetRangeUser(vert_min_value, vert_max_value);
  // At this point all the raw histograms should be filled with traces
  fCanvTraces->GetCanvas()->Update();
}
//-------------------------------------------------------------------//
Bool_t MWPCPlots::ProcessMessage (Long_t msg, Long_t parm1, Long_t /*parm2*/) {

  switch (GET_MSG (msg)) {
  case kC_COMMAND:
	switch (GET_SUBMSG (msg)) {
	case kCM_TAB: {
		// if ( parm1 == 0 )
			// fCanvasInfo->Update();
	}
	default:
	break;
  	}
  default:
  break;
  }
  return kTRUE;
}
//-------------------------------------------------------------------//
void MWPCPlots::SelectChannel(){
  int channel_number= fChannelsListBox->GetSelected();
  cout << "...Channel number " << channel_number << " selected" << endl;

  fCurrentSelectedChannel = channel_number;

  //---- We want to Read out the Electronics values from detector description
  //---- for a given channel
  LoadElectronicsValues();

  //---- We also want to Read out Defined signal range from Event description
  LoadChanRecValues();

  TracesUpdateOnCanvas(0.0);

  //---- Let's Enable Electronics Tab
  TGString *tab_name = new TGString(); 
  (*tab_name)+="Channel ";(*tab_name)+=channel_number;(*tab_name)+=" Electronics";
  fVariablesTab->GetTabTab(1)->SetText(tab_name);
  fVariablesTab->SetEnabled(1, kTRUE); // 1 is channel electronics

  tab_name = new TGString();
  (*tab_name)+="Channel ";(*tab_name)+=channel_number; (*tab_name)+=" Reconstruction";
  fVariablesTab->GetTabTab(2)->SetText(tab_name);
  fVariablesTab->SetEnabled(2, kTRUE); // 2 is channel reconstruction
  // UpdateTracesPlots(channel_number);
}
//-------------------------------------------------------------------//
void MWPCPlots::LoadElectronicsValues() {
  if (fDetector->HasChannel(fCurrentSelectedChannel)) {
	// we do have channel selected
	fElecTransRfNumber->GetEntry()->SetNumber(
		fDetector->GetChannel(fCurrentSelectedChannel)->GetTransimpRf());
	fElecTransCfNumber->GetEntry()->SetNumber(
		fDetector->GetChannel(fCurrentSelectedChannel)->GetTransimpCf());

	fElecAC1RfNumber->GetEntry()->SetNumber(
		fDetector->GetChannel(fCurrentSelectedChannel)->GetFirstACRf());
	fElecAC1RgNumber->GetEntry()->SetNumber(
		fDetector->GetChannel(fCurrentSelectedChannel)->GetFirstACRg());
	fElecAC1CgNumber->GetEntry()->SetNumber(
		fDetector->GetChannel(fCurrentSelectedChannel)->GetFirstACCg());

	fElecAC2RfNumber->GetEntry()->SetNumber(
		fDetector->GetChannel(fCurrentSelectedChannel)->GetSecondACRf());
	fElecAC2RgNumber->GetEntry()->SetNumber(
		fDetector->GetChannel(fCurrentSelectedChannel)->GetSecondACRg());
	fElecAC2CgNumber->GetEntry()->SetNumber(
		fDetector->GetChannel(fCurrentSelectedChannel)->GetSecondACCg());
  }
}
//-------------------------------------------------------------------//
void MWPCPlots::LoadChanRecValues(){
  if (fEvent == NULL){
	cout << "...MWPCPlots::LoadChanRecValues: Event is NULL" << endl;
	return;
  }
  /*
  if (fEvent->HasChannel(fCurrentSelectedChannel)) {
	utl::Trace *event_trace =
        	&(fEvent->GetChannel(fCurrentSelectedChannel)->GetTrace());

	if ( event_trace->GetNofPedestals() != 4 ){
		cout << "...MWPCPlots::LoadChanRecValues: There were " << 
			event_trace->GetNofPedestals() << " pedestals" << endl;

		double total_trace_length =
			((double)(event_trace->GetNofTimeBins()))*
			event_trace->GetBinDuration();

		
		fEvent->SetSignalRange(fCurrentSelectedChannel,
			1000.0, total_trace_length-1000.0);
		
		
		fRecSignalBegTimeNumber->GetEntry()->SetNumber(1000.0);
		fRecSignalEndTimeNumber->GetEntry()->SetNumber(total_trace_length-1000.0);
	} else {
		// We do have 4 Channels
		cout << "...MWPCPlots::LoadChanRecValues: Found " 
			<< event_trace->GetNofPedestals() 
			<< " pedestals, updating numbers" << endl;

		fRecSignalBegTimeNumber->GetEntry()->SetNumber(
			event_trace->GetPedestal(1).GetEnd());
		fRecSignalEndTimeNumber->GetEntry()->SetNumber(
			event_trace->GetPedestal(2).GetBegin());
	}
  }
  */
}
//-------------------------------------------------------------------//
void MWPCPlots::DoRecLevel(Int_t rec_level){
  cout << "...MWPCPlots: Rec Level chosen " << rec_level << endl;
  if (rec_level == 1) {
	fReconstructionLevel = 1;
	TracesFillFromRawEvent();
	TracesUpdateOnCanvas(fChannelsDrawingOffset);
  } else if (rec_level == 2) {
	fReconstructionLevel = 2;
	ReconUpdateFirstAC();
	TracesFillFromFirstAC();
	TracesUpdateOnCanvas(fChannelsDrawingOffset);
  } else if (rec_level == 3) {
  	fReconstructionLevel = 3;
	ReconUpdateSecondAC();
	TracesFillFromSecondAC();
	TracesUpdateOnCanvas(fChannelsDrawingOffset);
  } else if (rec_level == 4) {
  	fReconstructionLevel = 4;
	ReconUpdateCurrent();
	TracesFillFromCurrent();
	TracesUpdateOnCanvas(fChannelsDrawingOffset);
  } else if (rec_level == 5) {
  	fReconstructionLevel = 5;
	ReconUpdateCharge();
	TracesFillFromCharge();
	TracesUpdateOnCanvas(fChannelsDrawingOffset);
  }
}
//-------------------------------------------------------------------//
void MWPCPlots::DoVerticalOffset(){
  TracesUpdateOnCanvas(fOptVertOffsetNumber->GetEntry()->GetNumber()-fChannelsDrawingOffset);
  fChannelsDrawingOffset = fOptVertOffsetNumber->GetEntry()->GetNumber();
  // cout << "...Changed Vertical Offset to \"" << fChannelsDrawingOffset << "\"" << endl;
}
//-------------------------------------------------------------------//
void MWPCPlots::DoTransimpRf(){
  if (fDetector->HasChannel(fCurrentSelectedChannel)) {
	fDetector->GetChannel(fCurrentSelectedChannel)->SetTransimpRf(
		fElecTransRfNumber->GetEntry()->GetNumber() );
  }
}
//-------------------------------------------------------------------//
void MWPCPlots::DoTransimpCf(){
  if (fDetector->HasChannel(fCurrentSelectedChannel)) {
	fDetector->GetChannel(fCurrentSelectedChannel)->SetTransimpCf(
		fElecTransCfNumber->GetEntry()->GetNumber() );
  }
}
//-------------------------------------------------------------------//
void MWPCPlots::DoFirstACRf(){
  if (fDetector->HasChannel(fCurrentSelectedChannel)) {
	fDetector->GetChannel(fCurrentSelectedChannel)->SetFirstACRf(
		fElecAC1RfNumber->GetEntry()->GetNumber() );
  }
}
//-------------------------------------------------------------------//
void MWPCPlots::DoFirstACRg(){
  if (fDetector->HasChannel(fCurrentSelectedChannel)) {
	fDetector->GetChannel(fCurrentSelectedChannel)->SetFirstACRg(
		fElecAC1RgNumber->GetEntry()->GetNumber() );
  }
}
//-------------------------------------------------------------------//
void MWPCPlots::DoFirstACCg(){
  if (fDetector->HasChannel(fCurrentSelectedChannel)) {
	fDetector->GetChannel(fCurrentSelectedChannel)->SetFirstACCg(
		fElecAC1CgNumber->GetEntry()->GetNumber() );
  }
}
//-------------------------------------------------------------------//
void MWPCPlots::DoSecondACRf(){
  if (fDetector->HasChannel(fCurrentSelectedChannel)) {
	fDetector->GetChannel(fCurrentSelectedChannel)->SetSecondACRf(
		fElecAC2RfNumber->GetEntry()->GetNumber() );
  }
}
//-------------------------------------------------------------------//
void MWPCPlots::DoSecondACRg(){
  if (fDetector->HasChannel(fCurrentSelectedChannel)) {
	fDetector->GetChannel(fCurrentSelectedChannel)->SetSecondACRg(
		fElecAC2RgNumber->GetEntry()->GetNumber() );
  }
}
//-------------------------------------------------------------------//
void MWPCPlots::DoSecondACCg(){
  if (fDetector->HasChannel(fCurrentSelectedChannel)) {
	fDetector->GetChannel(fCurrentSelectedChannel)->SetSecondACCg(
		fElecAC2CgNumber->GetEntry()->GetNumber() );
  }
}
//-------------------------------------------------------------------//
void MWPCPlots::DoTimeRangeBeg(){
  if (fEvent == NULL) {return;}
  if ( !fEvent->HasChannel(fCurrentSelectedChannel) ){return;}

  utl::Trace *event_trace =
  	fEvent->GetChannel(fCurrentSelectedChannel)->GetTrace(0);
  
  cout << "...MWPCPlots::DoTimeRangeBeg: Event has "
  	<< event_trace->GetNofPedestals() << " Pedestals" << endl;
 

  if (event_trace->GetNofPedestals() != 4) {
	// LoadChanRecValues();
	cout << "...MWPCPlots::DoTimeRangeBeg nof peds is not 4, shouldn't happen" << endl;
	// Let's load Default Values
	double total_trace_length =
		((double)(event_trace->GetNofTimeBins()))*
		event_trace->GetBinDuration();
	fEvent->SetSignalRange(fCurrentSelectedChannel,
		1000.0, total_trace_length-1000.0);
	return;
  }
  // let's calculate the stored value:
  double old_begining = event_trace->GetPedestal(1).GetEnd();
  double old_ending = event_trace->GetPedestal(2).GetBegin();
  double new_begining = fRecSignalBegTimeNumber->GetEntry()->GetNumber();

  if (old_begining == new_begining) {
	// Do nothing
	cout << "...MWPCPlots::DoTimeRangeBeg new value is an old value" << endl;
	return;
  }
  // If not then reset the begining
  fEvent->SetSignalRange(fCurrentSelectedChannel, 
  	new_begining, old_ending);

  // We want to erase all the prev recs for this channel:
  if (fReconstr == NULL) {return;}

  if (fReconstr->HasFirstACChannel(fCurrentSelectedChannel))
  	fReconstr->ClearFirstAC(fCurrentSelectedChannel);
  if (fReconstr->HasSecondACChannel(fCurrentSelectedChannel))
  	fReconstr->ClearSecondAC(fCurrentSelectedChannel);
  if (fReconstr->HasCurrentChannel(fCurrentSelectedChannel))
	fReconstr->ClearCurrent(fCurrentSelectedChannel);
  if (fReconstr->HasChargeChannel(fCurrentSelectedChannel))
	fReconstr->ClearCharge(fCurrentSelectedChannel);

  // And update the reconstruction to the current value
  DoRecLevel(fReconstructionLevel);
}
//-------------------------------------------------------------------//
void MWPCPlots::DoTimeRangeEnd(){
  if (fEvent == NULL) {return;}
  if ( !fEvent->HasChannel(fCurrentSelectedChannel) ){return;}

  utl::Trace *event_trace =
	fEvent->GetChannel(fCurrentSelectedChannel)->GetTrace(0);

  cout << "...MWPCPlots::DoTimeRangeEnd: Event has "
	<< event_trace->GetNofPedestals() << " Pedestals" << endl;

  if (event_trace->GetNofPedestals() != 4) {
	// LoadChanRecValues();
	cout << "...MWPCPlots::DoTimeRangeEnd nof peds is not 4, shouldn't happen" << endl;
	// Let's load Default Values
	double total_trace_length =
		((double)(event_trace->GetNofTimeBins()))*
	  		event_trace->GetBinDuration();
	fEvent->SetSignalRange(fCurrentSelectedChannel,
		1000.0, total_trace_length-1000.0);
	return;
  }
  // let's calculate the stored value:
  double old_begining = event_trace->GetPedestal(1).GetEnd();
  double old_ending = event_trace->GetPedestal(2).GetBegin();
  double new_ending = fRecSignalEndTimeNumber->GetEntry()->GetNumber();

  if (old_ending == new_ending) {
	// Do nothing
	cout << "...MWPCPlots::DoTimeRangeEnd new value is an old value" << endl;
	return;
  }
  
  // If not then reset the begining
  fEvent->SetSignalRange(fCurrentSelectedChannel,
	old_begining, new_ending);


  // We want to erase all the prev recs for this channel:
  if (fReconstr == NULL) {return;}
  if (fReconstr->HasFirstACChannel(fCurrentSelectedChannel))
	fReconstr->ClearFirstAC(fCurrentSelectedChannel);
  if (fReconstr->HasSecondACChannel(fCurrentSelectedChannel))
	fReconstr->ClearSecondAC(fCurrentSelectedChannel);
  if (fReconstr->HasCurrentChannel(fCurrentSelectedChannel))
	fReconstr->ClearCurrent(fCurrentSelectedChannel);
  if (fReconstr->HasChargeChannel(fCurrentSelectedChannel))
	fReconstr->ClearCharge(fCurrentSelectedChannel);

  // And update the reconstruction up to current level
  DoRecLevel(fReconstructionLevel);
}
//-------------------------------------------------------------------//
