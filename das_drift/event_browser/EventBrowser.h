#include <TGFrame.h>

class TGWindow;
class TGTab;
class TGCompositeFrame;

class EventBrowserMenu;
class StatusBar;

class MWPCPlots;
class VPlots;

#include <string>
#include <vector>


namespace drift{
namespace evt{
  class Event;
}
namespace det{
  class Detector;
}
namespace rec{
  class Reconstruct;
}
}
using namespace drift;

class EventBrowser : public TGMainFrame {

  private:
  	EventBrowser();
  public:
	EventBrowser(const TGWindow * p, UInt_t w, UInt_t h,
		const std::vector<std::string> & fileNames ,
		det::Detector* det_descr, rec::Reconstruct *reconstructor);
	virtual ~EventBrowser();

	// SYSTEM ENTRY for signal handling
	Bool_t ProcessMessage(Long_t msg, Long_t parm1, Long_t parm2); // +
	Bool_t HandleKey(Event_t *event); // +
	void CloseWindow(); // + do the cleaning up
  private:
	void UpdateBrowser(); // +
	void UpdateTab(long int); // +
	void ReadEventList();
	void InitKeyStrokes(); // +
  public:
	bool ChangeEvent(Long_t);
  private:
  	void OpenDRTFile();
	void ChangeTab(Long_t);
	void UpdateTabsOnOpenFile();
  private:
  	// Event Stuff
	evt::Event*		fEvent;
	int 			fTotEvent; // total no of events in file
	int			fCurrentEventNumber;
	std::string		fCurrentEventFileName;
	// Detector Stuff
	det::Detector*		fDetector;
	// Reconstruction Stuff
	rec::Reconstruct*	fReconstr;
    
	// GUI stuff
	EventBrowserMenu*	fMenu;
	StatusBar*		fStatusBar;

	// --- the plots inside the tabs
	MWPCPlots*		fMWPCPlots;
	TGCompositeFrame*	fMWPCFrame;
	unsigned int 		fMWPCTabIndex;
	

	// Tabs
	std::vector<VPlots*> 	fPlotVector;
	TGTab*			fTabs;
  public:
	static std::string GetVersion();
	static std::string GetRevision();
	static std::string GetDate();
	static void Welcome();

  ClassDef (EventBrowser,1);
};
