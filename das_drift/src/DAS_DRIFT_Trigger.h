#ifndef _DAS_DRIFT_Trigger_h_
#define _DAS_DRIFT_Trigger_h_

#include <stdint.h>
#include <string>
#include <vector>

#include <DAS_DRIFT_TimeStamp.h>

namespace  das_drift {

/* This class is essentially an information that Accumulator
 * Board send to DAS every time it's getting a trigger on any channel */
class Trigger {
  public:
	Trigger();
	~Trigger();

//  private:
	uint16_t fChannelNumber;
	TimeStamp fTriggerTime;

	// Trigger Type :
	// 	bit 0; 1->Threshold
	// 	bit 1; 1->Positive 0->Negative
	uint16_t fTriggerType;
	/* Calculated as positive signal in [mV*usec] through
	 * the total duration of the corresponding trace which is 1sigma
	 * above the RMS of the noise */
	uint64_t  fPositiveSignal;
	/* The same for negative signal in [mV*sec] through the
	 * total duration of corresponding trace which is 1sigma below
	 * the RMS of the noise */
	uint64_t  fNegativeSignal;

	uint32_t fTotalTimeBinsAbove;
	uint32_t fTotalTimeBinsBelow;
	// This is a full signall span from first triggered bin to last triggered bin
	uint32_t fTotalTimeBins;
	// Number of times that the signal is coming from neg to positive and vise versa
	uint32_t fNumberOfZeroCrossing;


}; /* class Trigger */
} /* namespace das_drift */
#endif /* _DAS_DRIFT_Trigger_h_ */
