#include <DAS_DRIFT_TimeInterval.h>
using namespace das_drift;

//-------------------------------------------------------------------//
TimeInterval::TimeInterval() :
	fSeconds(0), fNanoSeconds(0) {
}
//-------------------------------------------------------------------//
TimeInterval::TimeInterval(const long aSeconds, const long aNanoSec) :
	fSeconds(aSeconds), fNanoSeconds(aNanoSec){

  while (fNanoSeconds < -1000000000) {
  	fNanoSeconds += 1000000000;
	fSeconds -= 1;
  }
  while (fNanoSeconds > 1000000000) {
  	fNanoSeconds -= 1000000000;
	fSeconds += 1;
  }
}
//-------------------------------------------------------------------//
TimeInterval::TimeInterval(const TimeInterval& aTimeInterval) :
	fSeconds(aTimeInterval.fSeconds), 
	fNanoSeconds(aTimeInterval.fNanoSeconds) {
}
//-------------------------------------------------------------------//
TimeInterval::~TimeInterval() {
}
//-------------------------------------------------------------------//
long TimeInterval::GetSecond() const{
  return fSeconds;
}
//-------------------------------------------------------------------//
long TimeInterval::GetNanoSecond() const{
  return fNanoSeconds;
}
//-------------------------------------------------------------------//
