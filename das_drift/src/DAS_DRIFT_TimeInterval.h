#ifndef _DAS_DRIFT_TimeInterval_h_
#define _DAS_DRIFT_TimeInterval_h_

namespace  das_drift {
class TimeInterval {
  public:
	TimeInterval();
	TimeInterval(const long aSeconds, const long aNanoSec = 0);
	TimeInterval(const TimeInterval& aTimeInterval);
	~TimeInterval();

	long GetSecond() const;
	long GetNanoSecond() const;

  private:
  	long fSeconds; // The interval in seconds
	long fNanoSeconds; // nanoseconds part

}; /* class TimeInterval */
} /* namespace das_drift */
#endif /* _DAS_DRIFT_TimeInterval_h_ */
