#include <DAS_DRIFT_Trace.h>
using namespace das_drift;

//-------------------------------------------------------------------//
Trace::Trace():
	fTraceSource(0),fWidthOfTimeBin(0){
}
//-------------------------------------------------------------------//
Trace::~Trace(){
  ClearTrace();
}
//-------------------------------------------------------------------//
Trace::Iterator Trace::Begin(){
  return fTrace.begin();
}
//-------------------------------------------------------------------//
Trace::Iterator Trace::End(){
  return fTrace.end();
}
//-------------------------------------------------------------------//
Trace::ConstIterator Trace::Begin() const{
  return fTrace.begin();
}
//-------------------------------------------------------------------//
Trace::ConstIterator Trace::End() const{
  return fTrace.end();
}
//-------------------------------------------------------------------//
void Trace::PushBack(const short &aValue){
  fTrace.push_back(aValue);
}
//-------------------------------------------------------------------//
unsigned long Trace::GetBinning() const{
  return fWidthOfTimeBin;
}
//-------------------------------------------------------------------//
void Trace::SetBinning(const unsigned long aBinning){
  fWidthOfTimeBin = aBinning;
}
//-------------------------------------------------------------------//
TimeStamp Trace::GetStart() const{
  return fStartOfTraceTime;
}
//-------------------------------------------------------------------//
void Trace::SetStart(const TimeStamp aStart){
  fStartOfTraceTime = aStart;
}
//-------------------------------------------------------------------//
Trace::SizeType Trace::GetSize() const{
  return fTrace.size();
}
//-------------------------------------------------------------------//
void Trace::ClearTrace(){
  fTrace.clear();
}
//-------------------------------------------------------------------//
Trace& Trace::operator=(const Trace& aTrace){
  fTraceSource = aTrace.fTraceSource;
  fStartOfTraceTime = aTrace.fStartOfTraceTime;
  fWidthOfTimeBin = aTrace.fWidthOfTimeBin;
  fTrace = aTrace.fTrace;

  return *this;
}
//-------------------------------------------------------------------//
bool Trace::operator==(const Trace& aTrace) const{
  return
  	fTraceSource == aTrace.fTraceSource &&
	fStartOfTraceTime == aTrace.fStartOfTraceTime &&
	fWidthOfTimeBin == aTrace.fWidthOfTimeBin &&
	fTrace == aTrace.fTrace;
}
//-------------------------------------------------------------------//
short& Trace::operator[](const SizeType i){
  return fTrace[i];
}
//-------------------------------------------------------------------//
const short& Trace::operator[](const SizeType i) const{
  return fTrace[i];
}
//-------------------------------------------------------------------//
