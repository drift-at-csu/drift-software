#include <DAS_DRIFT_Event.h>
using namespace das_drift;

#include <iostream> // for cout
#include <algorithm> // for swap
//-------------------------------------------------------------------//
Event::Event():fTotalSignal(0), fTotalTimeBins(0){
}
//-------------------------------------------------------------------//
Event::~Event(){
  fTriggers.clear();
  fChannels.clear();
}
//-------------------------------------------------------------------//
bool  Event::isTimeClose(das_drift::TimeStamp a_timestamp,
			das_drift::TimeInterval a_timeinterval){
  if ((fEventStopTime >= a_timestamp)&&(a_timestamp >= fEventStartTime)) {
  	// The event is in between start and stop
  	return true;
  }
  if (fEventStopTime < a_timestamp ) {
  	// The event is happening after the stop
	if ((fEventStopTime + a_timeinterval) >= a_timestamp) {
		/*
		std::cout << "Event:idClose After: Stop[" 
			<< fEventStopTime.GetGPSSecond() << ":"
			<< fEventStopTime.GetGPSNanoSecond() << "] Evnt["
			<< a_timestamp.GetGPSSecond() << ":"
			<< a_timestamp.GetGPSNanoSecond() << "] Int["
			<< a_timeinterval.GetSecond() << ":"
			<< a_timeinterval.GetNanoSecond() << "]" << std::endl;
		*/
		return true;
	}
  }
  if (a_timestamp < fEventStartTime ) {
  	// The event is happening before the stop
	if ((a_timestamp + a_timeinterval) >= fEventStartTime)
		return true;
  }

  return false;
}
//-------------------------------------------------------------------//
void Event::AddTrigger(das_drift::Trigger &a_trigger){
  if (GetNofTriggers() == 0) {
	// This is a situation in which the event time is wrong
	fEventStartTime = a_trigger.fTriggerTime;
	fEventStopTime = a_trigger.fTriggerTime;
	fTriggers.push_back(a_trigger);
	fChannels.push_back(a_trigger.fChannelNumber);
	fTotalSignal +=a_trigger.fPositiveSignal + a_trigger.fNegativeSignal;
	fTotalTimeBins += ( a_trigger.fTotalTimeBinsAbove + a_trigger.fTotalTimeBinsBelow );
	return;
  }

  if (a_trigger.fTriggerTime > fEventStopTime)
  	fEventStopTime = a_trigger.fTriggerTime;
  if (a_trigger.fTriggerTime < fEventStartTime)
  	fEventStartTime = a_trigger.fTriggerTime;

  fTriggers.push_back(a_trigger);
  fTotalSignal +=a_trigger.fPositiveSignal + a_trigger.fNegativeSignal;
  fTotalTimeBins += ( a_trigger.fTotalTimeBinsAbove + a_trigger.fTotalTimeBinsBelow );

  int channel_is_there = 0;
  for (unsigned long ii = 0; ii < GetNofChannels(); ++ii) {
	if (fChannels[ii] == a_trigger.fChannelNumber) {
		channel_is_there = 1;
		break;
	}
  }
  if (channel_is_there == 0) {
	fChannels.push_back(a_trigger.fChannelNumber);
  }
}
//-------------------------------------------------------------------//
long Event::GetNofChannels(){
  return fChannels.size();
}
//-------------------------------------------------------------------//
long Event::GetNofTriggers(){
  return fTriggers.size();
}
//-------------------------------------------------------------------//
bool Event::isChannelThere(unsigned long chan_id){
  for (unsigned long ii = 0; ii < GetNofChannels(); ++ii) {
  	if (fChannels[ii] == chan_id)
		return true;
  }
  return false;
}
//-------------------------------------------------------------------//
void Event::SortTriggersByTime(){
  // The sorting is a buble algorythm

  bool all_vectors_reordered = false;
  while (!all_vectors_reordered) {
	all_vectors_reordered = true;
	for (long ii = 0; ii < fTriggers.size() - 1; ++ii){
		if (fTriggers[ii].fTriggerTime > fTriggers[ii+1].fTriggerTime ) {
			// std::cout << "...Swapping " << ii << " and " << ii+1 << std::endl;
			// std::iter_swap(fTriggers.begin()+ii, fTriggers.begin()+ii+1);
			std::swap(fTriggers[ii], fTriggers[ii+1]);
			all_vectors_reordered = false;
			// break;
		}
	}
  }
}
//-------------------------------------------------------------------//
