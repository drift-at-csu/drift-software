#ifndef _DAS_DRIFT_TimeStamp_h_
#define _DAS_DRIFT_TimeStamp_h_

#include <string>
#include <vector>

#include <DAS_DRIFT_TimeInterval.h>

namespace  das_drift {
class TimeStamp {
  public:
	TimeStamp();
	TimeStamp(const long aSec, const long aNsec = 0);
	~TimeStamp();

	void SetGPSTime(const long aSec, const long aNsec = 0);

	long GetGPSSecond() const;
	long GetGPSNanoSecond() const;

	bool operator==(const TimeStamp& ts) const;
	bool operator!=(const TimeStamp& ts) const;
	bool operator>(const TimeStamp& ts) const;
	bool operator>=(const TimeStamp& ts) const;
	bool operator<(const TimeStamp& ts) const;
	bool operator<=(const TimeStamp& ts) const;

	TimeStamp operator=(const TimeStamp& ts);
	
	TimeStamp operator+(const TimeInterval& ti);
	TimeStamp& operator+=(const TimeInterval& ti);
	TimeInterval operator-(const TimeStamp& ts);
	TimeStamp operator-(const TimeInterval& ti);
	TimeStamp& operator-=(const TimeInterval& ti);

  private:
	// Take care of positive nanosecond and GPS epoch
	void SetNormalized(long sec, long nsec);

  	long fGPSSecond;
	long fGPSNanoSecond;

}; /* class TimeStamp */
} /* namespace das_drift */
#endif /* _DAS_DRIFT_TimeStamp_h_ */
