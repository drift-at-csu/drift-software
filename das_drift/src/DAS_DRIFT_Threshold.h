#ifndef _DAS_DRIFT_Threshold_h_
#define _DAS_DRIFT_Threshold_h_

#include <string>
#include <vector>

#include <DAS_DRIFT_TimeInterval.h>

namespace  das_drift {
class Threshold {
  public:
	Threshold();
	~Threshold();

//  protected:
  	unsigned long fChannelID;
	// Number of RAW bins over which to integrate the signal
	// in order to get a trigger trace
	unsigned short fTriggerIntegTime;
	// The accumulated Threshold is the ADCs values below and above
	unsigned long fAboveThreshold;
	unsigned long fBelowThreshold;
	// Number of RAW bins to keep before the trigger in the trace
	unsigned long fPreTrigger;
	// Number of RAW bins to keep after the trigger in the trace
	unsigned long fAfterTrigger;
	// Number of RAW bins over which to integrate in order to calculate
	// a pedestal
	unsigned long fPedestalIntegTime;
	

}; /* class Threshold */
} /* namespace das_drift */
#endif /* _DAS_DRIFT_Threshold_h_ */
