#include <DAS_DRIFT_Threshold.h>
using namespace das_drift;

//-------------------------------------------------------------------//
Threshold::Threshold():
	fChannelID(0), fTriggerIntegTime(0),
	fAboveThreshold(0), fBelowThreshold(0),
	fPreTrigger(0), fAfterTrigger(0){
}
//-------------------------------------------------------------------//
Threshold::~Threshold(){
}
//-------------------------------------------------------------------//
