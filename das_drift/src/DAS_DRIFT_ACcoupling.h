#ifndef _DAS_DRIFT_ACcoupling_h_
#define _DAS_DRIFT_ACcoupling_h_

#include <string>
#include <vector>

namespace  das_drift {

class ACcoupling {
  public:
	ACcoupling();
	~ACcoupling();

	void GenerateTemplate(double anACcoupl, double aBinWidth, unsigned long aNofBins);
	double GetTemplate(unsigned long aBinNumber);


  private:
  	std::vector <double> fTemplate;
}; /* class ACcoupling */
} /* namespace das_drift */
#endif /* _DAS_DRIFT_ACcoupling_h_ */
