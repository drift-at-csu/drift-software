#ifndef _DAS_DRIFT_Trace_h_
#define _DAS_DRIFT_Trace_h_

#include <string>
#include <vector>

#include <DAS_DRIFT_TimeStamp.h>
#include <DAS_DRIFT_TimeInterval.h>

namespace  das_drift {

class Trace {
  public:
	typedef typename std::vector<short>::size_type SizeType;
	typedef typename std::vector<short>::iterator Iterator;
	typedef typename std::vector<short>::const_iterator ConstIterator;

	Trace();
	~Trace();

	Iterator Begin();
	Iterator End();
	ConstIterator Begin() const;
	ConstIterator End() const;

	void PushBack(const short &value);

	unsigned long GetBinning() const;
	void SetBinning(const unsigned long aBinning);

	TimeStamp GetStart() const;
	void SetStart(const TimeStamp aStart);

	SizeType GetSize() const;
	void ClearTrace();

	Trace& operator=(const Trace& aTrace);
	bool operator==(const Trace& aTrace) const;

	short& operator[](const SizeType i);
	const short& operator[](const SizeType i) const;

  private:
  	/* The following two are uniquely define the trace */
	unsigned short fTraceSource; // Channel Id of the trace
	TimeStamp fStartOfTraceTime; // This is a timestamp of the begining of the trace

	unsigned long fWidthOfTimeBin; // this is a width of 1 time bin in ns
	std::vector <short> fTrace; // trace itself

}; /* class Trace */
} /* namespace das_drift */
#endif /* _DAS_DRIFT_Trace_h_ */
