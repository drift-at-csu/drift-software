#ifndef _DAS_DRIFT_Event_h_
#define _DAS_DRIFT_Event_h_

#include <stdint.h> 
#include <string>
#include <vector>

#include <DAS_DRIFT_TimeInterval.h>
#include <DAS_DRIFT_TimeStamp.h>
#include <DAS_DRIFT_Trigger.h>
/*
 * The Purpose of this class is to hold a mixture
 * of Triggers and traces for a single event
 * which is close in time set of wire hits
 */

namespace  das_drift {
class Event {
  public:
	Event();
	~Event();

	// Returns true if the time is close to an event time
	bool isTimeClose(das_drift::TimeStamp a_timestamp,
		das_drift::TimeInterval a_timeinterval);

	/// Adds A trigger to the triggers list
	void AddTrigger(das_drift::Trigger &a_trigger);

	/// Returns Number of Channels in the event
	long GetNofChannels();
	/// Returns Number of Triggers in the event
	long GetNofTriggers();

	bool isChannelThere(unsigned long chan_id);

	void SortTriggersByTime();

//  private:
  	das_drift::TimeStamp fEventStartTime;
	das_drift::TimeStamp fEventStopTime;
	std::vector <das_drift::Trigger> fTriggers;
	std::vector <long> fChannels;

	int64_t fTotalSignal;
	unsigned long fTotalTimeBins;

}; /* class Event */
} /* namespace das_drift */
#endif /* _DAS_DRIFT_Event_h_ */
