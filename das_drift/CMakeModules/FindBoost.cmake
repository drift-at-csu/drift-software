################################################################################
# Module to find Boost                                                         #
#                                                                              #
# This sets the following variables:                                           #
#   - Boost_FOUND                                                              #
#   - Boost_LIBRARIES                                                          #
#   - Boost_INCLUDE_DIR                                                        #
#   - Boost_CPPFLAGS                                                           #
#   - Boost_LDFLAGS                                                            #
################################################################################

# Search directories for Boost
SET (_DIRECTORIES
  $ENV{BOOST_PATH}
  /usr/local
  /opt/local
  /usr)

find_path (Boost_HOME include/boost/version.hpp
	PATHS ${_DIRECTORIES}
	NO_DEFAULT_PATH)

set(_DIRECTORIES)

set (Boost_INCLUDE_DIR
	${Boost_HOME}/include)
set (Boost_LIB_DIR
	${Boost_HOME}/lib)

set (Boost_HOME)


FIND_LIBRARY (Boost_SYSTEM_LIBRARY 
	libboost_system.so 
	${Boost_LIB_DIR} 
	NO_DEFAULT_PATH)
FIND_LIBRARY (Boost_FILESYSTEM_LIBRARY 
	libboost_filesystem.so 
	${Boost_LIB_DIR} 
	NO_DEFAULT_PATH)

IF (Boost_SYSTEM_LIBRARY AND Boost_FILESYSTEM_LIBRARY)
  SET (Boost_FOUND TRUE)
  SET (HAVE_Boost 1)
  SET (Boost_LIBRARIES 
	${Boost_SYSTEM_LIBRARY} 
	 ${Boost_FILESYSTEM_LIBRARY})
  SET (Boost_CPPFLAGS "-I${Boost_INCLUDE_DIR}")
ENDIF (Boost_SYSTEM_LIBRARY AND Boost_FILESYSTEM_LIBRARY)


IF (Boost_FOUND)
  IF (NOT Boost_FIND_QUIETLY)
    MESSAGE (STATUS "Boost library: ${Boost_LIBRARIES}")
    MESSAGE (STATUS "Boost include: ${Boost_INCLUDE_DIR}")
  ENDIF (NOT Boost_FIND_QUIETLY)
ELSE (Boost_FOUND)
  IF (Boost_FIND_REQUIRED)
    MESSAGE (FATAL_ERROR "Could not find Boost")
  else (Boost_FIND_REQUIRED)
    if (Boost_FIND_QUIETLY)
      MESSAGE (STATUS "Could not find Boost")
    endif (Boost_FIND_QUIETLY)
  ENDIF (Boost_FIND_REQUIRED)
ENDIF (Boost_FOUND)
