#include <evt/Event.h>
#include <evt/Channel.h>
using namespace drift;

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string.h>
using namespace std;

//-------------------------------------------------------------------//
evt::Event::Event(){
}
//-------------------------------------------------------------------//
evt::Event::~Event(){
  for (ChannelIterator iter = ChannelsBegin(); iter != ChannelsEnd(); ++iter) {
	if ((*iter)!=NULL) {
		delete (*iter);
		(*iter) = NULL;
	}
  }
}
//-------------------------------------------------------------------//
bool evt::Event::LoadFromDRTFile(std::string file_name, 
		unsigned long event_number) {
  ifstream drt_file(file_name.c_str(), ios::binary);
  if (!drt_file.is_open() ) {
	cout << "...evt::Event: Can't open \"" << file_name << "\"" << endl;
	return false;
  }
  unsigned long current_event_number = 1;
  while (1) {
	char tmp_char[7];
	drt_file.read(reinterpret_cast< char*> (tmp_char), 6);
	// cout << "...evt::Event: Magic string is \"" << tmp_char << "\"" << endl;
	if ( strcmp(tmp_char,"DRIFTE")!=0 ) {
		cout << "...evt::Event: Magic string is incorrect" << endl;
		drt_file.close();
		return false;
	}
	unsigned short file_version = 0;
	drt_file.read(reinterpret_cast< char*> (&file_version),sizeof(unsigned short));
	cout << "...evt::Event: Trying to read event v." << file_version << endl;
	unsigned long evnt_gps_sec; 
	unsigned long evnt_gps_nsec;
	drt_file.read(reinterpret_cast< char*> (&evnt_gps_sec),sizeof(unsigned long));
	drt_file.read(reinterpret_cast< char*> (&evnt_gps_nsec),sizeof(unsigned long));
	GetHeader().GetTime().SetGPSTime(evnt_gps_sec, evnt_gps_nsec);
	cout << "...evt::Event: Event time: " << GetHeader().GetTime().GetGPSSecond() <<
		"[" << GetHeader().GetTime().GetGPSNanoSecond() << "]" << endl;
	unsigned long evnt_nof_trcs = 0;
	drt_file.read(reinterpret_cast< char*> (&evnt_nof_trcs),sizeof(unsigned long));
	cout << "...evt::Event: There are " << evnt_nof_trcs << " traces in this event" << endl;

	// Let's clear the previous traces if any
	fChannels.clear();

	for (unsigned long ii=0; ii < evnt_nof_trcs; ++ii){
		unsigned long trace_size;
		drt_file.read(reinterpret_cast< char*> (&trace_size),sizeof(unsigned long));
		cout << "...evt::Event: Trace " << ii+1 << " size=" << trace_size << " bytes" << endl;
		if (current_event_number != event_number) {
			// This means we just want to skip to next event
			drt_file.seekg(trace_size, ios_base::cur);
		} else {
			// We're reading this event
			unsigned long trace_chan_id;
			drt_file.read(reinterpret_cast< char*> (&trace_chan_id),sizeof(unsigned long));
			cout << "...evt::Event: Trace " << ii+1 << " Making Channel " 
				<< trace_chan_id << endl;

			MakeChannel(trace_chan_id);

			long trace_offset;
			drt_file.read(reinterpret_cast< char*> (&trace_offset),sizeof(long));
			// We want to make a new Trace for this channel
			GetChannel(trace_chan_id)->MakeTrace(
					GetChannel(trace_chan_id)->GetNofTraces());

			utl::Trace* trace_pnt = GetChannel(trace_chan_id)->GetTrace(
					GetChannel(trace_chan_id)->GetNofTraces() - 1);

			trace_pnt->SetTimeOffset(
				(double) trace_offset/1000.0); // since our offset is in nsecs
			double  trace_vert_scale;
			drt_file.read(reinterpret_cast< char*> (&trace_vert_scale),sizeof(double));
			double  trace_horiz_scale;
			drt_file.read(reinterpret_cast< char*> (&trace_horiz_scale),sizeof(double));
			unsigned long trace_number_of_bins;
			drt_file.read(reinterpret_cast< char*> (&trace_number_of_bins),
				sizeof(unsigned long));
			 cout << "...evt::Event: VScale=" << trace_vert_scale
			 	<< ",HScale=" <<  trace_horiz_scale
				<< ",Off=" 	<< trace_offset
				<< ",NofBins="	<< trace_number_of_bins
				<< endl;


			uint32_t trace_current_bin_value;
			trace_pnt->SetBinDuration(trace_horiz_scale/1.0E+3); 
				// we expect usecs here

			for (unsigned long jj=0; jj < trace_number_of_bins; ++jj){
				drt_file.read (reinterpret_cast< char*> (&trace_current_bin_value), 
					sizeof(uint32_t));
				trace_pnt->AddTimeBin(
					(double) trace_current_bin_value*trace_vert_scale);
			}
		}
	}
	if (current_event_number == event_number) {
		// This means that we read out the right event
		break;
	}
	current_event_number ++;
  } // while(1) loop on the events
  drt_file.close();
  return true;
}
//-------------------------------------------------------------------//
void evt::Event::SetSignalRange(const unsigned int channelId,
		double left_ped, double right_ped){
  if (!HasChannel(channelId)) {
  	return;
  }
  evt::Channel *target_channel = GetChannel(channelId);
  // Let's clear the pedestals just in case
  for (evt::Channel::ConstTraceIterator iter = target_channel->TracesBegin(); 
  		iter != target_channel->TracesEnd(); ++iter) {
	(*iter)->ClearPedestals();
  }
  
  /*
  double total_trace_length = target_channel->GetTrace().GetBinDuration()*
  	((double)(target_channel->GetTrace().GetNofTimeBins()));

  // Check if we're within the range
  if (right_ped > total_trace_length)
  	right_ped = total_trace_length;
  if (left_ped < 0.0)
  	left_ped = 0;

  cout << "...evt::Event: Set Signal Range ["
  	<< left_ped << "," << right_ped << "]" << endl;

  // Add 2 ranges for left channel
  target_channel->GetTrace().AddPedestal(
	0.0,  left_ped/2.0);
  target_channel->GetTrace().AddPedestal(
	left_ped/2.0, left_ped);
  // And to the right channel
  target_channel->GetTrace().AddPedestal(
	right_ped, right_ped + (total_trace_length-right_ped)/2.0);
  target_channel->GetTrace().AddPedestal(
  	right_ped + (total_trace_length-right_ped)/2.0, total_trace_length);
  */

  // And this is it
}
//-------------------------------------------------------------------//
evt::Header& evt::Event::GetHeader(){
  return fHeader;
}
//-------------------------------------------------------------------//
const evt::Header& evt::Event::GetHeader() const{
  return fHeader;
}
//-------------------------------------------------------------------//
void evt::Event::SetHeader(const evt::Header& header){
  fHeader = header;
}
//-------------------------------------------------------------------//
unsigned int evt::Event::GetNofChannels() const{
  return fChannels.size();
}
//-------------------------------------------------------------------//
void evt::Event::MakeChannel(const unsigned int channelId){
  if (HasChannel(channelId)) 
	return;
   Channel *ChannelPnt = new Channel();
   ChannelPnt->SetId(channelId);
   fChannels.push_back(ChannelPnt);
}
//-------------------------------------------------------------------//
bool evt::Event::HasChannel(const unsigned int channelId){
  for (ConstChannelIterator iter = ChannelsBegin(); iter != ChannelsEnd(); ++iter) {
	if ((*iter)->GetId() == channelId) {
		return true;
	}
  }
  return false;
}
//-------------------------------------------------------------------//
evt::Channel *evt::Event::GetChannel(const unsigned int channelId){
  for (ConstChannelIterator iter = ChannelsBegin(); iter != ChannelsEnd(); ++iter) {
  	if ((*iter)->GetId() == channelId) {
		return (*iter);
	}
  }
  return NULL;
}
//-------------------------------------------------------------------//
evt::Event::ChannelIterator evt::Event::ChannelsBegin(){
  return fChannels.begin();
}
//-------------------------------------------------------------------//
evt::Event::ChannelIterator evt::Event::ChannelsEnd(){
  return fChannels.end();
}
//-------------------------------------------------------------------//
evt::Event::ConstChannelIterator evt::Event::ChannelsBegin() const{
  return fChannels.begin();
}
//-------------------------------------------------------------------//
evt::Event::ConstChannelIterator evt::Event::ChannelsEnd() const{
  return fChannels.end();
}
//-------------------------------------------------------------------//
