// #include <evt/Event.h>
#include <evt/Channel.h>
using namespace drift;

//-------------------------------------------------------------------//
evt::Channel::Channel():
		fId(0){
}
//-------------------------------------------------------------------//
evt::Channel::~Channel(){
  for (TraceIterator iter = TracesBegin(); iter != TracesEnd(); ++iter) {
	if ((*iter)!=NULL) {
		delete (*iter);
		(*iter) = NULL;
	}
  }
}
//-------------------------------------------------------------------//
unsigned int evt::Channel::GetId() const{
  return fId;
}
//-------------------------------------------------------------------//
void evt::Channel::SetId(unsigned int id){
  fId = id;
}
//-------------------------------------------------------------------//
unsigned int evt::Channel::GetNofTraces() const{
  return fTraces.size();
}
//-------------------------------------------------------------------//
void evt::Channel::MakeTrace(const unsigned int traceId){
  if (HasTrace(traceId))
	return;
  utl::Trace *TracePnt = new utl::Trace();
  TracePnt->SetId(traceId);
  fTraces.push_back(TracePnt);
}
//-------------------------------------------------------------------//
bool evt::Channel::HasTrace(const unsigned int traceId){
  for (ConstTraceIterator iter = TracesBegin(); iter != TracesEnd(); ++iter) {
	if ((*iter)->GetId() == traceId) {
		return true;
	}
  }
  return false;
}
//-------------------------------------------------------------------//
utl::Trace *evt::Channel::GetTrace(const unsigned int traceId){
  for (ConstTraceIterator iter = TracesBegin(); iter != TracesEnd(); ++iter) {
	if ((*iter)->GetId() == traceId) {
		return (*iter);
	}
  }
  return NULL;
}
//-------------------------------------------------------------------//
evt::Channel::TraceIterator evt::Channel::TracesBegin(){
  return fTraces.begin();
}
//-------------------------------------------------------------------//
evt::Channel::TraceIterator evt::Channel::TracesEnd(){
  return fTraces.end();
}
//-------------------------------------------------------------------//
evt::Channel::ConstTraceIterator evt::Channel::TracesBegin() const{
  return fTraces.begin();
}
//-------------------------------------------------------------------//
evt::Channel::ConstTraceIterator evt::Channel::TracesEnd() const{
  return fTraces.end();
}
//-------------------------------------------------------------------//
