#ifndef _DRIFT_evt_Channel_h_
#define _DRIFT_evt_Channel_h_

#include <utl/Trace.h>

#include <list>
namespace drift {
namespace evt {

class Channel {
  public:
  	typedef std::list<utl::Trace*> TraceContainer;
	typedef TraceContainer::iterator TraceIterator;
	typedef TraceContainer::const_iterator ConstTraceIterator;
  public:
  	Channel();
	~Channel();

	unsigned int GetId() const;
	void SetId(unsigned int id);

	unsigned int GetNofTraces() const;

	void MakeTrace(const unsigned int traceId);
	bool HasTrace(const unsigned int traceId);
	utl::Trace *GetTrace(const unsigned int traceId);

	TraceIterator TracesBegin();
	TraceIterator TracesEnd();
	ConstTraceIterator TracesBegin() const;
	ConstTraceIterator TracesEnd()   const;

  private:
	unsigned int fId;
	TraceContainer fTraces;

}; /* class Channel */
} /* namespace evt */
} /* namespace drift */
#endif /* _DRIFT_evt_Channel_h_ */
