#ifndef _DRIFT_evt_Event_h_
#define _DRIFT_evt_Event_h_

#include <list>
#include <string>

#include <evt/Header.h>
// #include <evt/Channel.h>

namespace drift {
namespace evt {
  class Channel;

class Event {
  public:
  	typedef std::list<Channel*> ChannelContainer;
	typedef ChannelContainer::iterator ChannelIterator;
	typedef ChannelContainer::const_iterator ConstChannelIterator;

  public:
  	Event();
	~Event();

	/// Loading a given event from file
	bool LoadFromDRTFile(std::string file_name, unsigned long event_number = 1);

	/// Sets the signal range for a channel channelId. Left_ped
	/// is the time (usecs) from the begining of the trace. right_ped
	/// is the time from the begining of the trace where right
	/// pedestal begins
	void SetSignalRange(const unsigned int channelId,
		double left_ped, double right_ped);

	drift::evt::Header& GetHeader();
	const drift::evt::Header& GetHeader() const;
	void SetHeader(const Header& header);

	unsigned int GetNofChannels() const;

	void MakeChannel(const unsigned int channelId);
	bool HasChannel(const unsigned int channelId);
	Channel *GetChannel(const unsigned int channelId);

  	ChannelIterator ChannelsBegin();
	ChannelIterator ChannelsEnd();
	ConstChannelIterator ChannelsBegin() const;
	ConstChannelIterator ChannelsEnd() const;

  private:
	Header fHeader;
	ChannelContainer fChannels;
}; /* class Event */
} /* namespace evt */
} /* namespace drift */
#endif /* _DRIFT_evt_Event_h_ */
