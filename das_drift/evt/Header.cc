#include <evt/Header.h>
using namespace drift;

//-------------------------------------------------------------------//
evt::Header::Header(){
}
//-------------------------------------------------------------------//
evt::Header::~Header(){
}
//-------------------------------------------------------------------//
const std::string& evt::Header::GetId() const{
  return fId;
}
//-------------------------------------------------------------------//
void evt::Header::SetId(const std::string& id){
  fId = id;
}
//-------------------------------------------------------------------//
const utl::TimeStamp& evt::Header::GetTime() const{
  return fTime;
}
//-------------------------------------------------------------------//
utl::TimeStamp& evt::Header::GetTime() {
  return fTime;
}
//-------------------------------------------------------------------//
void evt::Header::SetTime(const utl::TimeStamp& t){
  fTime = t;
}
//-------------------------------------------------------------------//
