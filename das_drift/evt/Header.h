#ifndef _DRIFT_evt_Header_h_
#define _DRIFT_evt_Header_h_

#include <utl/TimeStamp.h>

namespace drift {
namespace evt {

class Header {
  public:
  	Header();
	~Header();

  	/// Get the event identifier
	const std::string& GetId() const;
	/// Set the Id of the event
	void SetId(const std::string& id);

	/// Get the time stamp of the event
	const utl::TimeStamp& GetTime() const;
	utl::TimeStamp &GetTime();
	void SetTime(const utl::TimeStamp& t);

  private:
	utl::TimeStamp fTime;
	std::string fId;
}; // class Header
} // namespace evt
} // namespace drift
#endif /* _das_drift_evt_Header_h_ */
