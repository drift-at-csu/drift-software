#include <utl/Trace.h>
using namespace drift;

#include <iostream>
#include <math.h>
#include <algorithm>
using namespace std;

#include <TMinuit.h>
#include <TSpline.h>

namespace drift{
namespace utl{
	unsigned long FitNofPoints;
	double *FitYarray;
	double *FitXarray;
}
}
//-------------------------------------------------------------------//
utl::Trace::Trace():
	fId(0),
	fTimeBinDuration(0.0){
}
//-------------------------------------------------------------------//
utl::Trace::~Trace(){
  Clear();
  ClearPedestals();
}
//-------------------------------------------------------------------//
unsigned int utl::Trace::GetId() const{
  return fId;
}
//-------------------------------------------------------------------//
void utl::Trace::SetId(unsigned int id){
  fId = id;
}
//-------------------------------------------------------------------//
void utl::Trace::MultiplyBy(double value){
  for (TimeBinsIterator iter=TimeBinsBegin(); iter != TimeBinsEnd();
  		++iter){
	(*iter) *= value;
  }
}
//-------------------------------------------------------------------//
void utl::Trace::ShiftPedestalToZero(
		double ped_time_begin, double ped_time_end){

  long begin_time_bin = (ped_time_begin/fTimeBinDuration);
  long end_time_bin = (ped_time_end/fTimeBinDuration);

  if (begin_time_bin < 0) begin_time_bin = 0;
  if (begin_time_bin > end_time_bin) end_time_bin = begin_time_bin;

  // Let's find a pedestal
  double pedestal = 0.0;
  for (long ii = begin_time_bin; ii <= end_time_bin; ++ii){
	pedestal += fTimeBins[ii];
  }
  pedestal = pedestal/((double)(end_time_bin - begin_time_bin + 1));
  // cout << "___utl::Trace::ShiftPedestalToZero: pedestal = " << pedestal << endl;
  for (TimeBinsIterator iter = TimeBinsBegin(); iter != TimeBinsEnd(); ++iter) {
	(*iter) -= pedestal;
  }
	
}
//-------------------------------------------------------------------//
void utl::Trace::DeconvolveACCoupling(double time_const){
  // Let's create an AC coupling template first:
  std::vector <double> ac_template;

  double current_bin = 0.0;
  double result = 0.0;
  double an_integral = 0.0;
  for (unsigned long ii = 0; ii < GetNofTimeBins(); ++ii){
  	if (ii == 0 ) {
		result = (1 - exp(-current_bin/time_const));
	} else {
		result = exp((GetBinDuration()-current_bin)/time_const)
			- exp(-current_bin/time_const);
	}
	ac_template.push_back(result);
	current_bin += GetBinDuration();
	an_integral += result;
  }
  /*
  cout << "...utl::Trace::DeconvolveACCoupling: Integr=" 
  	<< an_integral << " (should be ~1)" << endl;
  */

  for (unsigned long ii = 0; ii < GetNofTimeBins(); ++ii){
	result = fTimeBins[ii];
	for (unsigned long jj = ii; jj < GetNofTimeBins(); ++jj) {
		fTimeBins[jj] += result*ac_template[jj-ii];
	}
  }
}
//-------------------------------------------------------------------//
void utl::Trace::CorrectOneOverF(){
  if (GetNofPedestals() < 4){
	cout << "...Trace::CorrectOneOverF: For a Correct spline there should be >= 4"
		<< " points" << endl;
	return;
  }

  double point_value[GetNofPedestals()];
  double point_times[GetNofPedestals()];
  unsigned long point_nofbins[GetNofPedestals()];
  for (unsigned int ii = 0; ii < GetNofPedestals(); ++ii){
	point_value[ii]=0.0;
	point_times[ii]=0.0;
	point_nofbins[ii]=0;
  }

  for (unsigned long ii = 0; ii < GetNofTimeBins(); ++ii){
  	for (unsigned long jj=0; jj < GetNofPedestals(); ++jj) {
		if ( fPedestals[jj].isWithin(((double)(ii))*fTimeBinDuration ) ){
			point_value[jj] += fTimeBins[ii];
			point_times[jj] += ((double)(ii))*fTimeBinDuration;
			point_nofbins[jj]++; 
		}
	}
  }

  for (unsigned int ii = 0; ii < GetNofPedestals(); ++ii){
	point_value[ii]=point_value[ii]/((double)(point_nofbins[ii]));
	point_times[ii]=point_times[ii]/((double)(point_nofbins[ii]));
  }

  TSpline5 *spline = new TSpline5("Pedest Spline", 
  	point_times, point_value, GetNofPedestals());

  for (unsigned long ii = 0; ii < GetNofTimeBins(); ++ii){
  	fTimeBins[ii] -= spline->Eval(((double)(ii))*fTimeBinDuration );
  }

  delete spline; spline = NULL;

}
//-------------------------------------------------------------------//
void utl::Trace::CorrectOneOverFMinuit(double ped_before, double ped_after){
  long begin_time_bin = (ped_before/fTimeBinDuration);
  long end_time_bin = (ped_after/fTimeBinDuration);

  if (begin_time_bin <= 0)
  	begin_time_bin = 1;
  if (end_time_bin <=0 )
  	end_time_bin = 1;
  end_time_bin = GetNofTimeBins() - end_time_bin;

  // Let's calculate how many points do we have to fit
  long nof_points_before = 0;
  long nof_points_after = 0;
  for (unsigned long ii = 0; ii < GetNofTimeBins(); ++ii){
	if (ii < (unsigned long) begin_time_bin) {
		nof_points_before ++;
	} else if ( ii >= (unsigned long) end_time_bin ) {
		nof_points_after ++;
	} else {
	}
  }
  FitNofPoints = nof_points_before+nof_points_after;
  // Now we do have a number of points, let's create  arrays
  FitXarray = new double[FitNofPoints];
  FitYarray = new double[FitNofPoints];

  // Fill the arrays with our pedestal data
  unsigned long current_array_bin = 0;
  for (unsigned long ii = 0; ii < GetNofTimeBins(); ++ii){
	if ((ii < (unsigned long) begin_time_bin)||(ii >= (unsigned long) end_time_bin)) {
		FitXarray[current_array_bin] = ((double)ii)*fTimeBinDuration;
		FitYarray[current_array_bin] = fTimeBins[ii];
		current_array_bin ++;
	}
  }

  TMinuit *a_minuit = new TMinuit(4);
  Double_t arglist[10];
  Int_t ierflg = 0;

  // Turning off the output
  arglist[0]=-1;
  a_minuit->mnexcm("SET PRINT",arglist,1,ierflg);

  arglist[0] = 0;
  a_minuit->mnexcm("SET NOW",  arglist,0,ierflg);

  // Setting a function
  a_minuit->SetFCN( FitParabolaChi2 );

  // Set all the errors to 1
  arglist[0] = 1;
  a_minuit->mnexcm("SET ERR", arglist , 1, ierflg);

  // Set starting values and step sizes for parameters
  static Double_t vstart[4] = { 
  	((double)GetNofTimeBins())/2.0*fTimeBinDuration, 
	0.0 , 0.0 , 0.0};
  static Double_t step[4] = {1.0 , 1.0E-8 , 1.0E-6 , 1.0E-6};
  a_minuit->mnparm(0, "a1", vstart[0], step[0], 0,0,ierflg);
  a_minuit->mnparm(1, "a2", vstart[1], step[1], 0,0,ierflg);
  a_minuit->mnparm(2, "a3", vstart[2], step[2], 0,0,ierflg);
  a_minuit->mnparm(3, "a4", vstart[3], step[3], 0,0,ierflg);

  // Now ready for minimization step
  arglist[0] = 500;
  arglist[1] = 1.;
  a_minuit->mnexcm("MIGRAD", arglist ,2,ierflg);

  // And the parameters are:
  double parameters[4];
  double param_errors[4];

  a_minuit->GetParameter(0,parameters[0],param_errors[0]);
  a_minuit->GetParameter(1,parameters[1],param_errors[1]);
  a_minuit->GetParameter(2,parameters[2],param_errors[2]);
  a_minuit->GetParameter(3,parameters[3],param_errors[3]);

  cout << "...Print results from Minuit:" << endl;
  cout << "   P1 = " << parameters[0] << ", P2 = " << parameters[1] <<
	", P3 = " << parameters[2] << ", P4 = " << parameters[3] << endl;

  for (unsigned long ii = 0; ii < GetNofTimeBins(); ++ii){
	fTimeBins[ii] -= FitParabolaFunc( (((double)ii)*fTimeBinDuration),
		parameters);
  }

  // let's not forget to delete arrays
  delete a_minuit; a_minuit = NULL;
  delete FitYarray; FitYarray = NULL;
  delete FitXarray; FitXarray = NULL;
}
//-------------------------------------------------------------------//
void utl::Trace::DeconvolveCurrent(double time_const){
  for (unsigned long ii = 0; ii < GetNofTimeBins()-1; ++ii){
	fTimeBins[ii] += time_const/GetBinDuration()*
		(fTimeBins[ii+1] - fTimeBins[ii]);
  }
}
//-------------------------------------------------------------------//
void utl::Trace::Integrate(double mult_factor){
  double result = 0.0;
  for (unsigned long ii = 0; ii < GetNofTimeBins(); ++ii){
  	result += fTimeBins[ii]*GetBinDuration();
	fTimeBins[ii] = result*mult_factor;
  }
}
//-------------------------------------------------------------------//
void utl::Trace::SetBinDuration(double bin_duration){
  fTimeBinDuration = bin_duration;
}
//-------------------------------------------------------------------//
double utl::Trace::GetBinDuration() const {
  return fTimeBinDuration;
}
//-------------------------------------------------------------------//
void utl::Trace::SetTimeOffset(double time_offset){
  fTraceTimeOffset = time_offset;
}
//-------------------------------------------------------------------//
double utl::Trace::GetTimeOffset() const{
  return fTraceTimeOffset;
}
//-------------------------------------------------------------------//
unsigned int utl::Trace::GetNofTimeBins() const{
  return fTimeBins.size();
}
//-------------------------------------------------------------------//
double& utl::Trace::GetTimeBin(const unsigned int time_bin){
  return fTimeBins[time_bin];
}
//-------------------------------------------------------------------//
const double& utl::Trace::GetTimeBin(const unsigned int time_bin) const{
  return fTimeBins[time_bin];
}
//-------------------------------------------------------------------//
utl::Trace::TimeBinsIterator utl::Trace::TimeBinsBegin(){
  return fTimeBins.begin();
}
//-------------------------------------------------------------------//
utl::Trace::TimeBinsIterator utl::Trace::TimeBinsEnd(){
  return fTimeBins.end();
}
//-------------------------------------------------------------------//
utl::Trace::ConstTimeBinsIterator utl::Trace::TimeBinsBegin() const{
  return fTimeBins.begin();
}
//-------------------------------------------------------------------//
utl::Trace::ConstTimeBinsIterator utl::Trace::TimeBinsEnd() const{
  return fTimeBins.end();
}
//-------------------------------------------------------------------//
utl::Trace utl::Trace::operator=(const utl::Trace& trc){
  Clear();
  ClearPedestals();

  fTimeBins = trc.fTimeBins;
  fTimeBinDuration = trc.fTimeBinDuration;
  fPedestals = trc.fPedestals;

  return *this;
}
//-------------------------------------------------------------------//
void utl::Trace::AddTimeBin(double time_bin_value){
  // cout << "...Adding Value: " << time_bin_value << endl;
  fTimeBins.push_back(time_bin_value);
}
//-------------------------------------------------------------------//
void utl::Trace::Clear(){
  fTimeBins.clear();
}
//-------------------------------------------------------------------//
unsigned int utl::Trace::GetNofPedestals() const{
  return fPedestals.size();
}
//-------------------------------------------------------------------//
utl::TimeRange& utl::Trace::GetPedestal(const unsigned int ped_number){
  return fPedestals[ped_number];
}
//-------------------------------------------------------------------//
const utl::TimeRange& utl::Trace::GetPedestal(const unsigned int ped_number) const{
  return fPedestals[ped_number];
}
//-------------------------------------------------------------------//
utl::Trace::PedestalsIterator utl::Trace::PedestalsBegin(){
  return fPedestals.begin();
}
//-------------------------------------------------------------------//
utl::Trace::PedestalsIterator utl::Trace::PedestalsEnd(){
  return fPedestals.end();
}
//-------------------------------------------------------------------//
utl::Trace::ConstPedestalsIterator utl::Trace::PedestalsBegin() const{
  return fPedestals.begin();
}
//-------------------------------------------------------------------//
utl::Trace::ConstPedestalsIterator utl::Trace::PedestalsEnd() const{
  return fPedestals.end();
}
//-------------------------------------------------------------------//
void utl::Trace::AddPedestal(double time_begin, double time_end){
  // before adding the pedestal let's check if

  fPedestals.push_back(TimeRange(time_begin, time_end));
  // std::sort (fPedestals.begin(), fPedestals.end());

  // Let's also Make sure that ranges of the Pedestals
}
//-------------------------------------------------------------------//
void utl::Trace::ClearPedestals(){
  fPedestals.clear();
}
//===================================================================//
Double_t utl::FitParabolaFunc(double x, Double_t *par){
  Double_t value=(par[1]*(x-par[0])*(x-par[0]))+(par[2]*(x-par[0]))+par[3];
  return value;
}
//-------------------------------------------------------------------//
void utl::FitParabolaChi2(
	Int_t &npar, Double_t *gin, Double_t &f, Double_t *par, Int_t iflag){

  //calculate chisquare
  Double_t chisq = 0;
  Double_t delta;
  for (unsigned long ii = 0; ii < FitNofPoints; ii++) {
	delta  = (FitYarray[ii]-FitParabolaFunc(FitXarray[ii], par));
	chisq += delta*delta;
  }
  f = chisq;
}
//-------------------------------------------------------------------//
