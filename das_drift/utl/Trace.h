#ifndef _DRIFT_utl_Trace_h_
#define _DRIFT_utl_Trace_h_

#include <TROOT.h>

#include <string>
#include <vector>

#include <utl/TimeRange.h>

namespace drift {
namespace utl {

class Trace {
  public:
  	typedef std::vector<double> TimeBinsContainer;
	typedef TimeBinsContainer::iterator TimeBinsIterator;
	typedef TimeBinsContainer::const_iterator ConstTimeBinsIterator;

	typedef std::vector<TimeRange> PedestalsContainer;
	typedef PedestalsContainer::iterator PedestalsIterator;
	typedef PedestalsContainer::const_iterator ConstPedestalsIterator;

  public:
	Trace();
	~Trace();
	
	/// Id is used for higher hierarchy only
	unsigned int GetId() const;
	void SetId(unsigned int id);

	///===> Let's List a useful Math one can do with the trace:
	/// Multiplies the whole trace by a value number
	void MultiplyBy(double value);
	/// This subtract the pedestal from the trace, pedestal time
	/// is defined between ped_time_begin and ped_time_end
	void ShiftPedestalToZero(double ped_time_begin, double ped_time_end);
	/// Do a deconvolution of AC coupling with a given time constant
	void DeconvolveACCoupling(double time_const);
	/// Do a correction for 1/f noise, for this we want to know
	/// a signal range. ped_before is how much time to allocate
	/// for a pedestal in the begining of the trace. ped_after
	/// is how much time to allocate at the end of the trace
	void CorrectOneOverF();
	/// Minuit Fit of parabola
	void CorrectOneOverFMinuit(double ped_before, double ped_after);
	/// Do a deconvolution on current: Iout = Iin + tau*(dIin/dt)
	void DeconvolveCurrent(double time_const);
	/// Do an integration of charge from current and multiply
	/// the resalt by value:
	void Integrate(double mult_factor);


	///===> Setters and Gettersafter this
	void SetBinDuration(double bin_duration);
	double GetBinDuration() const;

	void SetTimeOffset(double time_offset);
	double GetTimeOffset() const;

	unsigned int GetNofTimeBins() const;
	double& GetTimeBin(const unsigned int time_bin);
	const double& GetTimeBin(const unsigned int time_bin) const;

	TimeBinsIterator TimeBinsBegin();
	TimeBinsIterator TimeBinsEnd();
	ConstTimeBinsIterator TimeBinsBegin() const;
	ConstTimeBinsIterator TimeBinsEnd() const;

	Trace operator=(const Trace& trc);

	void AddTimeBin(double time_bin_value);
	/// Clears all time bins from Container
	void Clear();

	///===> Setters and getters for Pedestal Time Ranges
	unsigned int GetNofPedestals() const;
	TimeRange& GetPedestal(const unsigned int ped_number);
	const TimeRange& GetPedestal(const unsigned int ped_number) const;

	PedestalsIterator PedestalsBegin();
	PedestalsIterator PedestalsEnd();
	ConstPedestalsIterator PedestalsBegin() const;
	ConstPedestalsIterator PedestalsEnd() const;

	void AddPedestal(double time_begin, double time_end);

	void ClearPedestals();
  private:
	unsigned int fId;

	double fTimeBinDuration; // in usecs
	TimeBinsContainer fTimeBins;
	double fTraceTimeOffset; // in usecs

	/// The next is the ranges of the pedestals in the begining
	/// and at the end of traces for algorythms that are using it
	PedestalsContainer fPedestals;	

}; /* class Trace */

//=============================== Global functions for Minuit
// This is a parabola fit function
// par[0] is an offset over time
// par[1] quadratic term
// par[2] linear term
// par[3] offset
Double_t FitParabolaFunc(double x, Double_t *par);
// This is Chi2 for parabola fit
void FitParabolaChi2(Int_t &npar,
	Double_t *gin, Double_t &f, Double_t *par, Int_t iflag);

} /* namespace utl */
} /* namespace drift */
#endif /* _DRIFT_utl_Trace_h_ */
