#ifndef _DRIFT_utl_TimeStamp_h_
#define _DRIFT_utl_TimeStamp_h_

#include <string>
#include <vector>

#include <utl/TimeInterval.h>

namespace drift {
namespace utl {

class TimeStamp {
  public:
	TimeStamp();
	TimeStamp(const long aSec, const long aNsec = 0);
	~TimeStamp();

	void SetGPSTime(const long aSec, const long aNsec = 0);

	long GetGPSSecond() const;
	long GetGPSNanoSecond() const;

	bool operator==(const TimeStamp& ts) const;
	bool operator!=(const TimeStamp& ts) const;
	bool operator>(const TimeStamp& ts) const;
	bool operator>=(const TimeStamp& ts) const;
	bool operator<(const TimeStamp& ts) const;
	bool operator<=(const TimeStamp& ts) const;

	TimeStamp operator=(const TimeStamp& ts);
	
	TimeStamp operator+(const TimeInterval& ti);
	TimeStamp& operator+=(const TimeInterval& ti);
	TimeInterval operator-(const TimeStamp& ts);
	TimeStamp operator-(const TimeInterval& ti);
	TimeStamp& operator-=(const TimeInterval& ti);

  private:
	// Take care of positive nanosecond and GPS epoch
	void SetNormalized(long sec, long nsec);

  	long fGPSSecond;
	long fGPSNanoSecond;

}; /* class TimeStamp */
} /* namespace utl */
} /* namespace drift */
#endif /* _DRIFT_utl_TimeStamp_h_ */
