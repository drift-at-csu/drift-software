#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <sstream>
#include <getopt.h>
using namespace std;

#include "TektDAQOscilloscope.h"
using namespace TektDAQ;

//-------------------------------------------------------------------//
int main (int argc, char **argv) {
  int c;
  stringstream osc_addr;
  osc_addr << "192.168.1.115";
  unsigned long max_number_of_runs = 100;

  while ((c = getopt(argc, argv, "1:2:3:4:i:n:")) != EOF) {
	switch (c) {
		case '1':
			continue;
		case '2':
			continue;
		case '3':
			continue;
		case '4':
			continue;
		case 'i':
			osc_addr.str("");
			osc_addr << optarg;
			continue;
		case 'n':
			max_number_of_runs = atol(optarg);
			continue;
	};
  }
  if ( (optind + 1) != argc ) { // need to provide a filename
	cerr << "Usage: " << argv[0]
		<< "-1 A1 -2 G2 -3 A3 -4 A4"
		<< "-i <IP.ADD.RES.S> -n <runs_to_perform> "
		<< " <filename> "
		<< endl;
	return -1;
  }

  stringstream filename_string;
  filename_string << argv[optind];

  cout << "TekDAQ is about to write to " << filename_string.str() << endl;

  Oscilloscope oscil;
  cout << "...Connecting to " << osc_addr.str() << endl;
  oscil.fIpaddr = osc_addr.str();
  oscil.Connect();

  cout << "...Initializing Channels" << endl;
  oscil.InitializeChannels();
  cout << "...Initializing Trigger" << endl;
  oscil.InitializeTrigger();
  cout << "...Initializing Aquizition" << endl;
  oscil.InitializeAquisition();
  
  unsigned long cur_run_number = 0;

  while (cur_run_number < max_number_of_runs) {

  	cur_run_number ++;
  	filename_string.str("");
	filename_string << argv[optind] << "_" << cur_run_number << ".raw";
	oscil.Aquire();
	oscil.Save(filename_string.str());

	cout << "...Done with run number " << cur_run_number << endl;
  }

  cout << "...Disconnecting from " << osc_addr.str() << endl;
  oscil.Disconnect();
  return 0;
}

//-------------------------------------------------------------------//
