#include <TROOT.h>
#include <TStyle.h>
#include <TApplication.h>
#include <TCanvas.h>
#include <TH1D.h>
#include <TH1I.h> // One short per channel
#include <TF1.h> // For Fit functiona
#include <TMath.h>
#include <Minuit2/FCNBase.h>
#include <TFitterMinuit.h>

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include <map>
using namespace std;

#include <DAS_DRIFT_ACcoupling.h>
#include <DAS_DRIFT_Savitzky_Golay.h>
using namespace das_drift;


//-------------------------------------------------------------------//
class PeaksChar {
public:
	double fHeight;   // Height of the puls
	double fPosition; // Position of the centroid
	double fWidth;	   // Width of the pulse

};
//-------------------------------------------------------------------//
long FindPeakInTrace(TH1D &the_trace, double start, double stop, PeaksChar &peak){
  long result = 0;
  double maximum = -10.0;
  long maximum_bin = 0;
  double minimum = 10.0;
  long minimum_bin = 0;
  long total_number_of_bins = 0;

  for (long ii = the_trace.FindBin(start); ii < the_trace.FindBin(stop); ++ii){
  	if ( the_trace.GetBinContent(ii) > maximum ){
		maximum_bin = ii;
		maximum = the_trace.GetBinContent(ii);
	}
	if ( the_trace.GetBinContent(ii) < minimum ){
		minimum_bin = ii;
		minimum = the_trace.GetBinContent(ii);
	}
	total_number_of_bins ++;
  }

  if (TMath::Abs(maximum) > TMath::Abs(minimum) ) {
  	// positive pulse
	peak.fPosition = the_trace.GetBinCenter(maximum_bin);
	peak.fHeight = maximum;
	peak.fWidth = 50.0;
  } else {
  	// negative pulse
	peak.fPosition = the_trace.GetBinCenter(minimum_bin);
	peak.fHeight = minimum;
	peak.fWidth = 50.0;
  }

  return result;
}
//-------------------------------------------------------------------//
Double_t TransimpVoltFitFun(Double_t *x, Double_t *par) {
// Fit parameters:
//	par[0]=Gaussian mean
//	par[1]=Gaussian Sigma (W)
//	par[2]=Total area of Gaussian (integral -inf to inf, normalization constant)
//	par[3]=Time Constant of Transimpedance stage (tau) in usec
//	par[4]=Resistance of Transimpedance stage in MOhms
//	par[5]=Offset
//
  double result = 0.0;

  if (par[3] <= 0.0) return 1.e30;
  if (par[1] <= 0.0) return 1.e30;

  double time = x[0] - par[0];
  double max_nof_sigmas = 5.0;

  double time_min = -max_nof_sigmas*TMath::Sqrt(par[1]*par[1]);
  double time_max =  max_nof_sigmas*TMath::Sqrt(par[1]*par[1]+par[3]*par[3]);

  if ((time > time_max) || (time < time_min)){
  	return par[5];
  }
  // long number_of_points = 1000; // 100 points for a whole trace both sides of gaussian
  double current_time = time_min;
  double delta_time = 1.0; // (double) (time_max - time_min)/number_of_points;

  /* 
  cout << "...delta_time is " << delta_time << "usecs" 
  	<< ", min = " << time_min << ", max = " << time_max
  	<< endl;
   */

  double delta_charge = 0.0;

  while (current_time < time ) {
  	if ((current_time < delta_time)&&(current_time > 0.0)) {
		// we're right in the center of gaussian
		delta_charge = 0.5*(
			TMath::Erf(TMath::Abs(current_time)/par[1]/TMath::Sqrt(2.0)) +
			TMath::Erf(TMath::Abs(current_time-delta_time)/par[1]/TMath::Sqrt(2.0)) );
	} else {
		// we're either to the left or to the right of gaussian
		delta_charge = 0.5*(
			TMath::Erf(TMath::Abs(current_time)/par[1]/TMath::Sqrt(2.0)) -
			TMath::Erf(TMath::Abs(current_time-delta_time)/par[1]/TMath::Sqrt(2.0)) );
		if (delta_charge < 0) {
			delta_charge = -delta_charge;
		}
	}
	delta_charge *= par[2]*1.0E-3;
  	result += par[4]/par[3]*delta_charge - result*delta_time/par[3];

	current_time += delta_time;
  }

  return result*10.911 + par[5];

}
//-------------------------------------------------------------------//
//// Fit Function to fit the gaussian
Double_t AvalanFitFun(Double_t *x, Double_t *par) {
// Fit parameters:
//	par[0]=Gaussian mean
//	par[1]=Gaussian Sigma
//	par[2]=Total area (integral -inf to inf, normalization constant)
//	par[3]=Pedestal Offset
//
	return (par[2] * TMath::Gaus(x[0], par[0],par[1], kTRUE) + par[3]);
}
//-------------------------------------------------------------------//
void ACcorrectTrace (ACcoupling *the_coupling, std::vector <double> &the_trace, 
	double ped_1) {

  if (the_coupling == NULL)
  	return;

  Double_t bin_result = 0.0;
  Double_t iter_bin_result = 0.0;
  for (unsigned long ii = 0; ii < (unsigned long) the_trace.size(); ++ii) {
  	the_trace[ii] -= ped_1;
  }
  for (unsigned long ii = 0; ii < (unsigned long) the_trace.size(); ++ii) {
  	bin_result = the_trace[ii];
	for (unsigned long jj = ii; jj < (unsigned long)the_trace.size(); ++jj) {
		iter_bin_result = the_trace[jj];
		iter_bin_result += bin_result * the_coupling->GetTemplate(jj-ii);
		the_trace[jj] = iter_bin_result;
	}
  }
}

//-------------------------------------------------------------------//
class ACareaFCN : public ROOT::Minuit2::FCNBase { 
  public: 
  	//----------------------//
	ACareaFCN():fACcoupling(NULL){
		fTrace.clear();
	}
	//----------------------//
	void SetInputParameters (const std::vector <double> &the_trace, ACcoupling *anACcoupling){
		fTrace.clear();
		fTrace = the_trace;
		fACcoupling = anACcoupling;
	}
	//----------------------//
	double operator() (const std::vector<double> & x) const {
		long nof_points_pedestal = 100;

		// Let's make a new trace
		std::vector <double> tmp_trace;
		tmp_trace = fTrace;
		
		// Correct AC coupling
		ACcorrectTrace (fACcoupling, tmp_trace, x[0]);

		// Let's try to minimize the difference between
		// pedestall at the end
		double pedestal_difference = 0.0;
		for (long ii = (long) tmp_trace.size() - nof_points_pedestal; 
				ii < (long) tmp_trace.size(); ++ii) {
			pedestal_difference += TMath::Abs(tmp_trace[ii] - fTrace[ii]);
		}
		pedestal_difference /= nof_points_pedestal;


		// Total area (should be zero)
		double total_area = 0.0;
		for (long ii = 0; ii < (long) tmp_trace.size(); ++ii) {
			total_area += tmp_trace[ii];
		}
		//

		// Calculate RMSs of the pedestals
		
		/*
		double pedestal = 0.0;
		double pedestal_rms = 0.0;
		for (long ii = 0;  ii < (long) tmp_trace.size(); ++ii) {
			pedestal += tmp_trace[ii];
			pedestal_rms += tmp_trace[ii]*tmp_trace[ii];
		}
		pedestal /= nof_points_pedestal;
		pedestal_rms /=  nof_points_pedestal;
		pedestal_rms = (pedestal_rms - pedestal*pedestal);
		*/


		tmp_trace.clear();
		
		// return  total_area*total_area;
		return pedestal_difference*pedestal_difference;
		
		
	}
	//----------------------//
	double Up() const { 
		return 1.; 
	}
	//----------------------//
  private: 
	std::vector <double> fTrace;
	ACcoupling *fACcoupling;
};
//-------------------------------------------------------------------//
// This function is meant for the first AC coupling. It tries to set
// the pedestal in a such way that the total area after AC coupling
// correction is minimized.
//
void IterativelyCorrectAC(ACcoupling *the_coupling, TH1D &the_trace, 
	TFitterMinuit *the_minuit, ACareaFCN *fit_function) {

  // Let's copy the trace to a temporary location
  std::vector <double> tmp_trace;
  tmp_trace.clear();
  for (long ii = 0; ii < (long) the_trace.GetNbinsX(); ++ii) {
  	tmp_trace.push_back(the_trace.GetBinContent(ii+1));
  }

  // Let's calculate pedestal
  double pedestal_value = 0.0;
  for (long ii = 0; ii < (long) tmp_trace.size(); ++ii) {
	pedestal_value += tmp_trace[ii];
  }
  pedestal_value /= tmp_trace.size();

  // Minimization comes in here
  //============================>
  fit_function->SetInputParameters (tmp_trace, the_coupling);
  the_minuit->SetParameter(0, "Pedestal", pedestal_value,
  		1.000E-4, // error
		-0.04, +0.04); // low, high
  the_minuit->SetPrintLevel(-1);

  // cout << "...Pedestal Before: " << pedestal_value << " ... " << flush;
  int iret = the_minuit->Minimize();
  pedestal_value = the_minuit->GetParameter(0); 
  // cout << "...After: " << the_minuit->GetParameter(0) << endl;
  //============================>
  // Correct the AC coupling
  ACcorrectTrace(the_coupling, tmp_trace, the_minuit->GetParameter(0));

  // Copy the content back
  for (long ii = 0; ii < (long) tmp_trace.size(); ++ii) {
	the_trace.SetBinContent(ii+1, tmp_trace[ii]);
  }

}
//-------------------------------------------------------------------//
void FillCurrentTrace(TH1D &voltage_trace, TH1D &current_trace, 
	double resistance, double time_constant) {

  double bin_result = 0.0;
  double next_bin_result = 0.0;
  double current_value = 0.0;

  for (unsigned long ii = 0; ii < (unsigned long) current_trace.GetNbinsX()-1; ++ii) {
  	bin_result =voltage_trace.GetBinContent(ii+1);
	next_bin_result=voltage_trace.GetBinContent(ii+2);
	current_value = (  0.5*(bin_result + next_bin_result) + 
		time_constant*(next_bin_result - bin_result)/5.0  )/resistance;
	current_trace.SetBinContent(ii+1, 1.0E+3*current_value/10.911);
  }
  current_trace.SetBinContent(current_trace.GetNbinsX(),
  	current_trace.GetBinContent(current_trace.GetNbinsX()));
}
//-------------------------------------------------------------------//
int main (int argc, char **argv) {
  int c;
  string triggers_filename("");

  while ((c = getopt(argc, argv, "t:")) != EOF) {
	switch (c) {
		case 't':
			triggers_filename = optarg;
			continue;
	};
  }
  if ( ((optind + 1) != argc) || (triggers_filename == "") ) { // need to provide a filename
	cerr << "Usage: " << argv[0]
		<< "-t <triggers_filename> "
		<< " <raw_filename> "
		<< endl;
	return -1;
  }

  stringstream filename_string;
  filename_string << argv[optind];

  cout << "Traces are about to be read from " << filename_string.str() << endl;
  cout << "Triggers will be read from " << triggers_filename << endl;

  TApplication theApp ("App", NULL, 0);
  gROOT->Reset();
  gStyle->SetOptStat(kFALSE);
  gStyle->SetFrameBorderMode(0);
  gROOT->ForceStyle();

  TCanvas myc("myc", "Fe55 temporary analyzer", 1200, 800);
  myc.SetFillColor(kWhite);
  myc.cd();
  myc.SetBorderMode(0);
  myc.Draw();


  TPad canv1("canv1", "Voltages",0.01,0.51,0.99,0.99);
  canv1.SetBorderMode(0);
  canv1.Draw();
  canv1.SetFillColor(kWhite);
  canv1.SetFrameFillColor(kWhite);
  canv1.SetGridx();
  canv1.SetGridy();


  TPad canv2("canv2", "Currents",0.01,0.01,0.99,0.49);
  canv2.SetBorderMode(0);
  canv2.Draw();
  canv2.SetFillColor(kWhite);
  canv2.SetFrameFillColor(kWhite);
  canv2.SetGridx();
  canv2.SetGridy();


  double hor_interval = 0.0;
  unsigned long nop_in_file = 0;
  double vert_gain = 0.0;
  unsigned long noch_in_file = 0;
  unsigned long header_length; // in bytes
  header_length = sizeof(unsigned long) +
  	sizeof(unsigned long) +
	sizeof(double) +
	sizeof(double);

  // Input file is coming from the saved oscilloscope file
  ifstream raw_file(filename_string.str().c_str(), ios::binary);
  if ( !raw_file.good()) {
  	cout << "===Can't open RAW file, exiting" << endl;
  	return -1;
  }

  // Triggers are given in ascii file for convinience
  ifstream trigger_file(triggers_filename.c_str(), ifstream::in);
  if ( !trigger_file.good()) {
  	cout << "===Can't open Trigger file, exiting" << endl;
  	raw_file.close();
	return -1;
  }

  // Let's read the header
  raw_file.read(reinterpret_cast< char*> (&nop_in_file), sizeof(unsigned long));
  raw_file.read(reinterpret_cast< char*> (&noch_in_file), sizeof(unsigned long));
  raw_file.read(reinterpret_cast< char*> (&vert_gain), sizeof(double));
  raw_file.read(reinterpret_cast< char*> (&hor_interval), sizeof(double));

  // FIle Header should be read
  cout << "...RAW File Header: " << endl;
  cout << "   Number of Points per channel: " << nop_in_file << endl;
  cout << "   Number of Channels: " << noch_in_file << endl;
  cout << "   Vertical Gain: " << vert_gain << " Volts" << endl;
  cout << "   Horizontal bin width: " << hor_interval*1.0E+6 << " us" << endl;

  // Variables for channels
  double resistor_values [4];
  double time_const_values [4];

  resistor_values[0] = 10.0;
  resistor_values[1] = 50.0;
  resistor_values[2] = 10.0;
  resistor_values[3] = 50.0;

  time_const_values[0] = resistor_values[0] * 0.6 / (1.0E+6*hor_interval);
  time_const_values[1] = resistor_values[1] * 0.6 / (1.0E+6*hor_interval);
  time_const_values[2] = resistor_values[2] * 0.6 / (1.0E+6*hor_interval);
  time_const_values[3] = resistor_values[3] * 0.6 / (1.0E+6*hor_interval);

  cout << "...Time Constants = " << time_const_values[0] << ","
  	<< time_const_values[1] << "," << time_const_values[2] << "," 
	<< time_const_values[3] << " usec" << endl;

  // Variables for histograms
  TH1D voltage_trace[4];
  voltage_trace[0].SetName("voltage_trace_1"); voltage_trace[0].SetTitle("Trace 1");
  voltage_trace[1].SetName("voltage_trace_2"); voltage_trace[1].SetTitle("Trace 2");
  voltage_trace[2].SetName("voltage_trace_3"); voltage_trace[2].SetTitle("Trace 3");
  voltage_trace[3].SetName("voltage_trace_4"); voltage_trace[3].SetTitle("Trace 4");

  voltage_trace[0].SetLineColor(kBlack);
  voltage_trace[1].SetLineColor(kRed);
  voltage_trace[2].SetLineColor(kBlue);
  voltage_trace[3].SetLineColor(kMagenta);

  for (unsigned short ii = 0; ii < 4; ++ii) {
  	voltage_trace[ii].SetLineWidth(1);
	voltage_trace[ii].GetYaxis()->SetRangeUser(-0.1, +0.1);
  }

  canv1.cd();
  voltage_trace[0].Draw();
  for (unsigned short ii = 1; ii < 4; ++ii) {
	voltage_trace[ii].Draw("SAME");
  }
  // Fit Functions for Gaussian charge on the input of transimpedance
  TF1	voltage_timp_fit_1("timp_fit_1", TransimpVoltFitFun, 0.0, 1000.0, 6);
  TF1   voltage_timp_fit_2("timp_fit_2", TransimpVoltFitFun, 0.0, 1000.0, 6);
  TF1   voltage_timp_fit_3("timp_fit_3", TransimpVoltFitFun, 0.0, 1000.0, 6);
  TF1   voltage_timp_fit_4("timp_fit_4", TransimpVoltFitFun, 0.0, 1000.0, 6);

  //	par[0]=Gaussian mean	in usec
  //	par[1]=Gaussian Sigma (W) in usec
  //    par[2]=Total area of Gaussian (integral -inf to inf, normalization constant) in fC
  //    par[3]=Time Constant of Transimpedance stage (tau) in usec
  //    par[4]=Resistance of Transimpedance stage in MOhms

  voltage_timp_fit_1.SetParameters(500.0, 5.0,	-1.00, time_const_values[0], resistor_values[0], 0.02);
  voltage_timp_fit_2.SetParameters(500.0, 10.0,	-1.00, time_const_values[1], resistor_values[1], 0.00);
  voltage_timp_fit_3.SetParameters(500.0, 50.0,  1.00, time_const_values[2], resistor_values[2],-0.02);
  voltage_timp_fit_4.SetParameters(500.0, 50.0,  1.00, time_const_values[3], resistor_values[3],-0.04);

  voltage_timp_fit_1.SetParNames("Center", "Width", "Area", "TimeConst", "Resistance", "Offset");
  voltage_timp_fit_2.SetParNames("Center", "Width", "Area", "TimeConst", "Resistance", "Offset");
  voltage_timp_fit_3.SetParNames("Center", "Width", "Area", "TimeConst", "Resistance", "Offset");
  voltage_timp_fit_4.SetParNames("Center", "Width", "Area", "TimeConst", "Resistance", "Offset");

  voltage_timp_fit_1.SetParLimits(0, 0.0, 1000.0);
  voltage_timp_fit_1.SetParLimits(1, 1.0, 80.0);
  voltage_timp_fit_1.SetParLimits(2, -100.0, 100.0);
  voltage_timp_fit_1.FixParameter(3, time_const_values[0]);
  voltage_timp_fit_1.FixParameter(4, resistor_values[0]);
  voltage_timp_fit_1.FixParameter(5, 0.0);

  voltage_timp_fit_2.SetParLimits(0, 0.0, 1000.0);
  voltage_timp_fit_2.SetParLimits(1, 1.0, 80.0);
  voltage_timp_fit_2.SetParLimits(2, -100.0, 100.0);
  voltage_timp_fit_2.FixParameter(3, time_const_values[1]);
  voltage_timp_fit_2.FixParameter(4, resistor_values[1]);
  voltage_timp_fit_2.FixParameter(5, 0.0);

  voltage_timp_fit_3.SetParLimits(0, 0.0, 1000.0);
  voltage_timp_fit_3.SetParLimits(1, 1.0, 80.0);
  voltage_timp_fit_3.SetParLimits(2, -100.0, 100.0);
  voltage_timp_fit_3.FixParameter(3, time_const_values[2]);
  voltage_timp_fit_3.FixParameter(4, resistor_values[2]);
  voltage_timp_fit_3.FixParameter(5, 0.0);

  voltage_timp_fit_4.SetParLimits(0, 0.0, 1000.0);
  voltage_timp_fit_4.SetParLimits(1, 1.0, 80.0);
  voltage_timp_fit_4.SetParLimits(2, -100.0, 100.0);
  voltage_timp_fit_4.FixParameter(3, time_const_values[3]);
  voltage_timp_fit_4.FixParameter(4, resistor_values[3]);
  voltage_timp_fit_4.FixParameter(5, 0.0);

  voltage_timp_fit_1.SetLineColor(kGray+2);
  voltage_timp_fit_2.SetLineColor(kRed+2);
  voltage_timp_fit_3.SetLineColor(kBlue+2);
  voltage_timp_fit_4.SetLineColor(kMagenta+2);

  voltage_timp_fit_1.Draw("SAME");
  voltage_timp_fit_2.Draw("SAME");
  voltage_timp_fit_3.Draw("SAME");
  voltage_timp_fit_4.Draw("SAME");

	

  // Now currents histograms
  TH1D current_trace[4];
  TH1D summ_current;
  TH1D summ_current_averaged;
  current_trace[0].SetName("current_trace_1"); current_trace[0].SetTitle("Current");
  current_trace[1].SetName("current_trace_2"); current_trace[1].SetTitle("Current");
  current_trace[2].SetName("current_trace_3"); current_trace[2].SetTitle("Current");
  current_trace[3].SetName("current_trace_4"); current_trace[3].SetTitle("Current");
  summ_current.SetName("summ_current"); summ_current.SetTitle("Current");
  summ_current_averaged.SetName("current_aver"); summ_current_averaged.SetTitle("Current");

  current_trace[0].SetLineColor(kBlack);
  current_trace[1].SetLineColor(kRed);
  current_trace[2].SetLineColor(kBlue);
  current_trace[3].SetLineColor(kMagenta);

  summ_current.SetLineColor(kGreen);
  summ_current_averaged.SetLineColor(kBlack);

  for (unsigned short ii = 0; ii < 4; ++ii) {
	current_trace[ii].SetLineWidth(1);
	current_trace[ii].GetYaxis()->SetRangeUser(-1.0, +1.0);
  }
  summ_current.SetLineWidth(1);
  summ_current.GetYaxis()->SetRangeUser(-1.0, +1.0);
  summ_current_averaged.SetLineWidth(2);
  summ_current_averaged.GetYaxis()->SetRangeUser(-1.0, +1.0);

  canv2.cd();
  current_trace[0].Draw();
  for (unsigned short ii = 1; ii < 4; ++ii) {
	current_trace[ii].Draw("SAME");
  }
  summ_current.Draw("SAME");
  summ_current_averaged.Draw("SAME");

  // These are used for fitting the current
  TF1	current_avlan_fit_1("avalan_fit_1", AvalanFitFun, 0.0, 1000.0, 4);
  TF1   current_avlan_fit_2("avalan_fit_2", AvalanFitFun, 0.0, 1000.0, 4);
  TF1   current_avlan_fit_3("avalan_fit_3", AvalanFitFun, 0.0, 1000.0, 4);
  TF1   current_avlan_fit_4("avalan_fit_4", AvalanFitFun, 0.0, 1000.0, 4);

  current_avlan_fit_1.SetParameters(500.0, 50.0, 1.0E-6, 0.0);
  current_avlan_fit_2.SetParameters(500.0, 50.0, 1.0E-6, 0.0);
  current_avlan_fit_3.SetParameters(500.0, 50.0, 1.0E-6, 0.0);
  current_avlan_fit_4.SetParameters(500.0, 50.0, 1.0E-6, 0.0);


  current_avlan_fit_1.SetParNames("Center","Width","Area","Offset");
  current_avlan_fit_2.SetParNames("Center","Width","Area","Offset");
  current_avlan_fit_3.SetParNames("Center","Width","Area","Offset");
  current_avlan_fit_4.SetParNames("Center","Width","Area","Offset");

  // "Center","Width","Area","Offset"
  current_avlan_fit_1.SetParLimits(0, 0.0, 1000.0);
  current_avlan_fit_1.SetParLimits(1, 1.0, 200.0);
  current_avlan_fit_1.SetParLimits(2, -200.0, 200.0);
  current_avlan_fit_1.FixParameter(3, 0.0);

  current_avlan_fit_2.SetParLimits(0, 0.0, 1000.0);
  current_avlan_fit_2.SetParLimits(1, 1.0, 200.0);
  current_avlan_fit_2.SetParLimits(2, -200.0, 200.0);
  current_avlan_fit_2.FixParameter(3, 0.0);

  current_avlan_fit_3.SetParLimits(0, 0.0, 1000.0);
  current_avlan_fit_3.SetParLimits(1, 1.0, 200.0);
  current_avlan_fit_3.SetParLimits(2, -200.0, 200.0);
  current_avlan_fit_3.FixParameter(3, 0.0);

  current_avlan_fit_4.SetParLimits(0, 0.0, 1000.0);
  current_avlan_fit_4.SetParLimits(1, 1.0, 200.0);
  current_avlan_fit_4.SetParLimits(2, -200.0, 200.0);
  current_avlan_fit_4.FixParameter(3, 0.0);

  current_avlan_fit_1.SetLineColor(kGray+2);
  current_avlan_fit_2.SetLineColor(kRed+2);
  current_avlan_fit_3.SetLineColor(kBlue+2);
  current_avlan_fit_4.SetLineColor(kMagenta+2);

  current_avlan_fit_1.Draw("SAME");
  current_avlan_fit_2.Draw("SAME");
  current_avlan_fit_3.Draw("SAME");
  current_avlan_fit_4.Draw("SAME");


  // AC coupling correction, use the same one for all traces seems to work
  ACcoupling ac_corr;
  ac_corr.GenerateTemplate(
  	110.0, // Time constant in usec
	5 * hor_interval*1.0E+6, // binning size in usec
	1000 // How many bins we want generated
  );

  // Finally Minuit fitter
  TFitterMinuit *minuit = new TFitterMinuit();
  ACareaFCN ac_fit_fcn;
  minuit->SetMinuitFCN(&ac_fit_fcn);
  minuit->SetParameter(0, "Pedestal1", 0.0,
		0.1, // error
		-0.04, +0.04); // low, high
  minuit->SetPrintLevel(0);
  minuit->CreateMinimizer();

  
  string trigger_string_line; // for storing single triggering file
  unsigned long prev_start_bin = 0;
  while (1) {
	getline(trigger_file, trigger_string_line);
	if (!trigger_file.good()){
		cout << "...Reached the end of trigger file" << endl;
		break;
	}
	if (trigger_string_line[0] == '#') {
		// This is a comment, skipping this line
		continue;
	}
	unsigned long start_bin, stop_bin;
	short curr_rdout_val; 
	long accum_rdout_val;
	std::stringstream trigger_line_ss(trigger_string_line);

	trigger_line_ss >> start_bin >> stop_bin;
	cout << "... Analyzing bins: \t" << start_bin << "\t" << stop_bin << endl;
	// Let's Create Histograms for storing the traces
	
	// Let's recreate Voltage traces based on start and stop bins
	for (unsigned short ii = 0; ii < 4; ++ii) {
		voltage_trace[ii].SetBins((stop_bin-start_bin)/5,
			-1000.0, (stop_bin-start_bin)-1000.0);
	}

	// Trace number 1
	raw_file.seekg (header_length + ((start_bin - 1)* sizeof(short)) +
		0 * nop_in_file * sizeof(short), ios::beg);
	for (unsigned long ii = 0; ii < (stop_bin - start_bin)/5; ++ii) {
		accum_rdout_val = 0;
		for (unsigned long jj = 0; jj < 5; ++jj){
			raw_file.read(reinterpret_cast< char*> (&curr_rdout_val), sizeof(short));
			accum_rdout_val += curr_rdout_val;
		}
		voltage_trace[0].SetBinContent(ii+1, (double) accum_rdout_val*vert_gain/5);
	}
	// Trace number 2
	raw_file.seekg (header_length + ((start_bin - 1)* sizeof(short)) +
	        1 * nop_in_file * sizeof(short), ios::beg);
	for (unsigned long ii = 0; ii < (stop_bin - start_bin)/5; ++ii) {
		accum_rdout_val = 0;
		for (unsigned long jj = 0; jj < 5; ++jj){
			raw_file.read(reinterpret_cast< char*> (&curr_rdout_val), sizeof(short));
			accum_rdout_val += curr_rdout_val;
		}
		voltage_trace[1].SetBinContent(ii+1, (double) accum_rdout_val*vert_gain/5);
	}
	// Trace number 3
	raw_file.seekg (header_length + ((start_bin - 1)* sizeof(short)) +
		2 * nop_in_file * sizeof(short), ios::beg);
	for (unsigned long ii = 0; ii < (stop_bin - start_bin)/5; ++ii) {
		accum_rdout_val = 0;
		for (unsigned long jj = 0; jj < 5; ++jj){
			raw_file.read(reinterpret_cast< char*> (&curr_rdout_val), sizeof(short));
			accum_rdout_val += curr_rdout_val;
		}
		voltage_trace[2].SetBinContent(ii+1, (double) accum_rdout_val*vert_gain/5);
	}
	// Trace number 4
	raw_file.seekg (header_length + ((start_bin - 1)* sizeof(short)) +
		3 * nop_in_file * sizeof(short), ios::beg);
	for (unsigned long ii = 0; ii < (stop_bin - start_bin)/5; ++ii) {
		accum_rdout_val = 0;
		for (unsigned long jj = 0; jj < 5; ++jj){
			raw_file.read(reinterpret_cast< char*> (&curr_rdout_val), sizeof(short));
			accum_rdout_val += curr_rdout_val;
		}
		voltage_trace[3].SetBinContent(ii+1, (double) accum_rdout_val*vert_gain/5);
	}
//=========== Now The Traces should be in Histograms =====//
	// Let's do AC coupling correction
	for (unsigned short ii = 0; ii < 4; ++ii) {
		// Trying to minimize the pedestal difference
		IterativelyCorrectAC(&ac_corr, voltage_trace[ii], minuit, &ac_fit_fcn);
		// Let's try to ac decouple only the range from 200 to 300
		double pedestal = 0.0;
		
		for (long jj = 140; jj < 240; ++jj) {
			pedestal += voltage_trace[ii].GetBinContent(jj+1);
		}
		pedestal /= 100;

		// pedestal = 0.0;
		
		// The part of the trace : 
		std::vector <double> tmp_trace;
		for (long jj = 200; jj < voltage_trace[ii].GetNbinsX() - 200; ++jj) {
			tmp_trace.push_back(voltage_trace[ii].GetBinContent(jj+1));
		}
		ACcorrectTrace (&ac_corr, tmp_trace, pedestal);

		// now, at this point the trace might diverge, therefore we want to
		// correct it by subtracting a straight line
		// let's calculate the jump
		double jump_value = 0.0;
		for (long jj = voltage_trace[ii].GetNbinsX() - 200 - 10; 
				jj < voltage_trace[ii].GetNbinsX() - 200; ++jj) {
			jump_value += tmp_trace[jj-200] - voltage_trace[ii].GetBinContent(jj+1);
		}
		jump_value /= 10;
		// cout << " Jump value per bin = " << jump_value << endl;
		// let's correct the temporary trace
		jump_value /= tmp_trace.size();
		
		for (long jj = 0; jj < tmp_trace.size(); ++jj) {
			tmp_trace[jj] -= jump_value * jj;
		}
		

		// fill it back to histogram
		for (long jj = 200; jj < voltage_trace[ii].GetNbinsX() - 200; ++jj) {
			voltage_trace[ii].SetBinContent(jj+1, tmp_trace[jj-200]);
		}
	} // end of the cycle for ac_coupling corrections

	//===================> Let's try to fit Our Voltages signals
	PeaksChar peaks[4];
	
	for (short ii = 0; ii < 4; ++ii) {
		FindPeakInTrace(voltage_trace[ii], 0.0, 1000.0, peaks[ii]);
	}

	// Let's set the range
	voltage_timp_fit_1.SetRange(peaks[0].fPosition-3.0*peaks[0].fWidth, 
		peaks[0].fPosition+3.0*peaks[0].fWidth);
	voltage_timp_fit_2.SetRange(peaks[1].fPosition-3.0*peaks[1].fWidth, 
		peaks[1].fPosition+3.0*peaks[1].fWidth);
	voltage_timp_fit_3.SetRange(peaks[2].fPosition-3.0*peaks[2].fWidth, 
		peaks[2].fPosition+3.0*peaks[2].fWidth);
	voltage_timp_fit_4.SetRange(peaks[3].fPosition-3.0*peaks[3].fWidth, 
		peaks[3].fPosition+3.0*peaks[3].fWidth);
	
	//Return Offset to 0
	voltage_timp_fit_1.SetParameter(5,0.0);
	voltage_timp_fit_2.SetParameter(5,0.0);
	voltage_timp_fit_3.SetParameter(5,0.0);
	voltage_timp_fit_4.SetParameter(5,0.0);

	// Return width to calculated one
	voltage_timp_fit_1.SetParameter(1, peaks[0].fWidth*0.5);
	voltage_timp_fit_2.SetParameter(1, peaks[1].fWidth*0.5);
	voltage_timp_fit_3.SetParameter(1, peaks[2].fWidth*0.5);
	voltage_timp_fit_4.SetParameter(1, peaks[3].fWidth*0.5);

	// Return center to 500.0 usec
	voltage_timp_fit_1.SetParameter(0, peaks[0].fPosition);
	voltage_timp_fit_1.SetParLimits(0, peaks[0].fPosition - peaks[0].fWidth,
		peaks[0].fPosition + peaks[0].fWidth);
	voltage_timp_fit_2.SetParameter(0, peaks[1].fPosition);
	voltage_timp_fit_2.SetParLimits(0, peaks[1].fPosition - peaks[1].fWidth,
		peaks[1].fPosition + peaks[1].fWidth);
	voltage_timp_fit_3.SetParameter(0, peaks[2].fPosition);
	voltage_timp_fit_3.SetParLimits(0, peaks[2].fPosition - peaks[2].fWidth,
		peaks[2].fPosition + peaks[2].fWidth);
	voltage_timp_fit_4.SetParameter(0, peaks[3].fPosition);
	voltage_timp_fit_4.SetParLimits(0, peaks[3].fPosition - peaks[3].fWidth,
		peaks[3].fPosition + peaks[3].fWidth);

	// Return area to 10.0 fC
	if (peaks[0].fHeight > 0) {
		voltage_timp_fit_1.SetParameter(2, 10.0);
		voltage_timp_fit_1.SetParLimits(2, 0.0, 100.0);
	} else {
		voltage_timp_fit_1.SetParameter(2, -10.0);
		voltage_timp_fit_1.SetParLimits(2, -100.0, 0.0);
	}
	if (peaks[1].fHeight > 0) {
		voltage_timp_fit_2.SetParameter(2, 10.0);
		voltage_timp_fit_2.SetParLimits(2, 0.0, 100.0);
	} else {
		voltage_timp_fit_2.SetParameter(2, -10.0);
		voltage_timp_fit_2.SetParLimits(2, -100.0, 0.0);
	}
	if (peaks[2].fHeight > 0) {
		voltage_timp_fit_3.SetParameter(2, 10.0);
		voltage_timp_fit_3.SetParLimits(2, 0.0, 100.0);
	} else {
		voltage_timp_fit_3.SetParameter(2, -10.0);
		voltage_timp_fit_3.SetParLimits(2, -100.0, 0.0);
		
	}
	if (peaks[3].fHeight > 0) {
		voltage_timp_fit_4.SetParameter(2, 10.0);
		voltage_timp_fit_4.SetParLimits(2, 0.0, 100.0);
	} else {
		voltage_timp_fit_4.SetParameter(2, -10.0);
		voltage_timp_fit_4.SetParLimits(2, -100.0, 0.0);
	}


	// Do the fit
	voltage_trace[0].Fit("timp_fit_1","RBW0MQ");
	voltage_trace[1].Fit("timp_fit_2","RBW0MQ");
	voltage_trace[2].Fit("timp_fit_3","RBW0MQ");
	voltage_trace[3].Fit("timp_fit_4","RBW0MQ");

//=========================================================//
	// Let's recreate Current traces based on start and stop bins
	for (unsigned short ii = 0; ii < 4; ++ii) {
		// The currents will be 1 bin smaller in size
		current_trace[ii].SetBins(voltage_trace[ii].GetNbinsX(),
			-1000.0, (stop_bin-start_bin)-1000.0); 
		// And fill it with calculated Currents
		FillCurrentTrace(voltage_trace[ii], current_trace[ii],
			resistor_values[ii], time_const_values[ii]);
	}
	summ_current.SetBins(current_trace[0].GetNbinsX(),
			-1000.0, (stop_bin-start_bin)-1000.0);
	summ_current_averaged.SetBins(summ_current.GetNbinsX(),
			-1000.0, (stop_bin-start_bin)-1000.0);
	// Calculate the summ of all currents and average them
	for(long ii = 0; ii < summ_current.GetNbinsX() ; ++ii) {
		summ_current.SetBinContent(ii+1,
			current_trace[0].GetBinContent(ii+1) +
			current_trace[1].GetBinContent(ii+1) +
			current_trace[2].GetBinContent(ii+1) +
			current_trace[3].GetBinContent(ii+1) );
		summ_current_averaged.SetBinContent(ii+1,
			summ_current.GetBinContent(ii+1)/10.0);
	}

	summ_current_averaged.Rebin(10);

	//==========================> Now let's fit all the functions
	// "Center","Width","Area","Offset"
	//Return Offset to 0
	current_avlan_fit_1.SetParameter(3,0.0);
	current_avlan_fit_2.SetParameter(3,0.0);
	current_avlan_fit_3.SetParameter(3,0.0);
	current_avlan_fit_4.SetParameter(3,0.0);

	// Return area
	double area_calcs[4];

	// Vary Areas 50%

	current_avlan_fit_1.SetParameter(2,voltage_timp_fit_1.GetParameter(2));
	current_avlan_fit_2.SetParameter(2,voltage_timp_fit_2.GetParameter(2));
	current_avlan_fit_3.SetParameter(2,voltage_timp_fit_3.GetParameter(2));
	current_avlan_fit_4.SetParameter(2,voltage_timp_fit_4.GetParameter(2));


	if (voltage_timp_fit_1.GetParameter(2) > 0) {
		current_avlan_fit_1.SetParLimits(2, 
			0.5 * voltage_timp_fit_1.GetParameter(2),
			1.5 * voltage_timp_fit_1.GetParameter(2));
	} else {
		current_avlan_fit_1.SetParLimits(2,
			1.5 * voltage_timp_fit_1.GetParameter(2), 
			0.5 * voltage_timp_fit_1.GetParameter(2));
	}

	if (voltage_timp_fit_2.GetParameter(2) > 0) {
		current_avlan_fit_2.SetParLimits(2,
			0.5 * voltage_timp_fit_2.GetParameter(2),
			1.5 * voltage_timp_fit_2.GetParameter(2));
	} else {
		current_avlan_fit_2.SetParLimits(2,
			1.5 * voltage_timp_fit_2.GetParameter(2),
			0.5 * voltage_timp_fit_2.GetParameter(2));
	}

	if (voltage_timp_fit_3.GetParameter(2) > 0) {
		current_avlan_fit_3.SetParLimits(2,
			0.5 * voltage_timp_fit_3.GetParameter(2),
			1.5 * voltage_timp_fit_3.GetParameter(2));
	} else {
		current_avlan_fit_3.SetParLimits(2,
			1.5 * voltage_timp_fit_3.GetParameter(2),
			0.5 * voltage_timp_fit_3.GetParameter(2));
	}

	if (voltage_timp_fit_4.GetParameter(2) > 0) {
		current_avlan_fit_4.SetParLimits(2,
			0.5 * voltage_timp_fit_4.GetParameter(2),
			1.5 * voltage_timp_fit_4.GetParameter(2));
	} else {
		current_avlan_fit_4.SetParLimits(2,
			1.5 * voltage_timp_fit_4.GetParameter(2),
			0.5 * voltage_timp_fit_4.GetParameter(2));
	}

	
	// Vary width 50%
	current_avlan_fit_1.SetParameter(1, voltage_timp_fit_1.GetParameter(1));
	current_avlan_fit_1.SetParLimits(1, 0.1*voltage_timp_fit_1.GetParameter(1), 
		1.5*voltage_timp_fit_1.GetParameter(1));

	current_avlan_fit_2.SetParameter(1, voltage_timp_fit_2.GetParameter(1));
	current_avlan_fit_2.SetParLimits(1, 0.1*voltage_timp_fit_2.GetParameter(1), 
		1.5*voltage_timp_fit_2.GetParameter(1));

	current_avlan_fit_3.SetParameter(1, voltage_timp_fit_3.GetParameter(1));
	current_avlan_fit_3.SetParLimits(1, 0.1*voltage_timp_fit_3.GetParameter(1), 
		1.5*voltage_timp_fit_3.GetParameter(1));

	current_avlan_fit_4.SetParameter(1, voltage_timp_fit_4.GetParameter(1));
	current_avlan_fit_4.SetParLimits(1, 0.1*voltage_timp_fit_4.GetParameter(1),
		1.5*voltage_timp_fit_4.GetParameter(1));

	// Vary center at 100usec level
	current_avlan_fit_1.SetParameter(0, voltage_timp_fit_1.GetParameter(0));
	current_avlan_fit_1.SetParLimits(0, voltage_timp_fit_1.GetParameter(0) - 50.0,
		voltage_timp_fit_1.GetParameter(0) + 50.0);

	current_avlan_fit_2.SetParameter(0, voltage_timp_fit_2.GetParameter(0));
	current_avlan_fit_2.SetParLimits(0, voltage_timp_fit_2.GetParameter(0) - 50.0,
		voltage_timp_fit_2.GetParameter(0) + 50.0);

	current_avlan_fit_3.SetParameter(0, voltage_timp_fit_3.GetParameter(0));
	current_avlan_fit_3.SetParLimits(0, voltage_timp_fit_3.GetParameter(0) - 50.0,
		voltage_timp_fit_3.GetParameter(0) + 50.0);

	current_avlan_fit_4.SetParameter(0, voltage_timp_fit_4.GetParameter(0));
	current_avlan_fit_4.SetParLimits(0, voltage_timp_fit_4.GetParameter(0) - 50.0,
		voltage_timp_fit_4.GetParameter(0) + 50.0);

	// Do the fit
	current_trace[0].Fit("avalan_fit_1","IRBW0MQ");
	current_trace[1].Fit("avalan_fit_2","IRBW0MQ");
	current_trace[2].Fit("avalan_fit_3","IRBW0MQ");
	current_trace[3].Fit("avalan_fit_4","IRBW0MQ");

	// Let's calculate currents one more time...
	area_calcs[0] = current_trace[0].Integral(
		current_trace[0].FindBin( -5.0 +
			current_avlan_fit_1.GetParameter(0)-3.0*current_avlan_fit_1.GetParameter(1)),
		current_trace[0].FindBin( +5.0 +
			current_avlan_fit_1.GetParameter(0)+3.0*current_avlan_fit_1.GetParameter(1)),
		"width");
	
	area_calcs[1] = current_trace[1].Integral(
		current_trace[1].FindBin( -5.0 +
			current_avlan_fit_2.GetParameter(0) - 3.0*current_avlan_fit_2.GetParameter(1)),
		current_trace[1].FindBin( +5.0 +
			current_avlan_fit_2.GetParameter(0) + 3.0*current_avlan_fit_2.GetParameter(1)),
		"width");
	
	area_calcs[2] = current_trace[2].Integral(
		current_trace[2].FindBin( -5.0 +
			current_avlan_fit_3.GetParameter(0) - 3.0*current_avlan_fit_3.GetParameter(1)),
		current_trace[2].FindBin( +5.0 +
			current_avlan_fit_3.GetParameter(0) + 3.0*current_avlan_fit_3.GetParameter(1)),
		"width");
	
	area_calcs[3] = current_trace[3].Integral(
		current_trace[3].FindBin( -5.0 +
			current_avlan_fit_4.GetParameter(0) - 3.0*current_avlan_fit_4.GetParameter(1)),
		current_trace[3].FindBin( +5.0 +
			current_avlan_fit_4.GetParameter(0) + 3.0*current_avlan_fit_4.GetParameter(1)),
		"width");

	cout    << "CH1:\tA=" << current_avlan_fit_1.GetParameter(2) 
			<< "[" << area_calcs[0] << "]"
		<< ",\tW=" << current_avlan_fit_1.GetParameter(1)
		<< ",\tC=" << current_avlan_fit_1.GetParameter(0)
		<< endl
		<< "CH2:\tA=" << current_avlan_fit_2.GetParameter(2)
			<< "[" << area_calcs[1] << "]"
		<< ",\tW=" << current_avlan_fit_2.GetParameter(1)
		<< ",\tC=" << current_avlan_fit_2.GetParameter(0)
		<< endl
		<< "CH3:\tA=" << current_avlan_fit_3.GetParameter(2) 
			<< "[" << area_calcs[2] << "]"
		<< ",\tW=" << current_avlan_fit_3.GetParameter(1)
		<< ",\tC=" << current_avlan_fit_3.GetParameter(0)
		<< endl
		<< "CH4:\tA=" << current_avlan_fit_4.GetParameter(2) 
			<< "[" << area_calcs[3] << "]"
		<< ",\tW=" << current_avlan_fit_4.GetParameter(1) 
		<< ",\tC=" << current_avlan_fit_4.GetParameter(0)
		<< endl;
	

//=========== Visualisation after this line ==============//
	// Before we draw the histograms, let's shift them by a known amount
	for (unsigned short ii = 0; ii < 4; ++ii) {
		for(long jj = 0; jj < voltage_trace[ii].GetNbinsX() ; ++jj) {
			voltage_trace[ii].SetBinContent(jj+1,
				voltage_trace[ii].GetBinContent(jj+1) + 
					0.02 - 0.02*ii);
		}
	}
	// Let's also offset our fit functions
	voltage_timp_fit_1.SetParameter(5,  voltage_timp_fit_1.GetParameter(5) + 0.02 - 0.00);
	voltage_timp_fit_2.SetParameter(5,  voltage_timp_fit_2.GetParameter(5) + 0.00 - 0.00);
	voltage_timp_fit_3.SetParameter(5,  voltage_timp_fit_3.GetParameter(5) - 0.02 - 0.00);
	voltage_timp_fit_4.SetParameter(5,  voltage_timp_fit_4.GetParameter(5) - 0.04 - 0.00);

	canv1.Modified();

	for (unsigned short ii = 0; ii < 4; ++ii) {
		for(long jj = 0; jj < current_trace[ii].GetNbinsX() ; ++jj) {
			current_trace[ii].SetBinContent(jj+1,
				current_trace[ii].GetBinContent(jj+1) +
					0.2 - 0.2*ii);
		}
	}
	for(long ii = 0; ii < summ_current.GetNbinsX() ; ++ii) {
		summ_current.SetBinContent(ii+1,
			summ_current.GetBinContent(ii+1) + 0.6);
	}
	for(long ii = 0; ii < summ_current_averaged.GetNbinsX() ; ++ii) {
		summ_current_averaged.SetBinContent(ii+1,
			summ_current_averaged.GetBinContent(ii+1) + 0.8);
	}

	// Let's also offset our fit functions
	current_avlan_fit_1.SetParameter(3,  current_avlan_fit_1.GetParameter(3) + 0.2 - 0.0);
	current_avlan_fit_2.SetParameter(3,  current_avlan_fit_2.GetParameter(3) + 0.0 - 0.0);
	current_avlan_fit_3.SetParameter(3,  current_avlan_fit_3.GetParameter(3) - 0.2 - 0.0);
	current_avlan_fit_4.SetParameter(3,  current_avlan_fit_4.GetParameter(3) - 0.4 - 0.0);


	canv2.Modified();

	myc.Modified();

	myc.Update();
	cout << "#...Sleeping for " << (start_bin - prev_start_bin)*10 << endl;
	// usleep((start_bin - prev_start_bin)*10);
	prev_start_bin = start_bin;
  } // End of read and analyze cycle

  raw_file.close();
  trigger_file.close();


  theApp.Run();
  return 0;
}

//-------------------------------------------------------------------//
