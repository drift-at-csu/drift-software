#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <sstream>
#include <getopt.h>
using namespace std;

#include "LecroyOscilloscope.h"
using namespace Lecroy;

//-------------------------------------------------------------------//
int main (int argc, char **argv) {
  int c;
  while ((c = getopt(argc, argv, "1:2:3:4:")) != EOF) {
	switch (c) {
		case '1':
			continue;
		case '2':
			continue;
		case '3':
			continue;
		case '4':
			continue;
	};
  }
  if ( (optind + 1) != argc ) { // need to provide a filename
	cerr << "Usage: " << argv[0]
		<< "-1 A1 -2 G2 -3 A3 -4 A4"
		<< " <filename> "
		<< endl;
	return -1;
  }

  stringstream filename_string;
  filename_string << argv[optind];

  cout << "Lecroy converter is about to read from " << filename_string.str() << endl;

  Oscilloscope oscil;

  for (int ii = 0; ii < 4; ii++){
	filename_string.str("");
	filename_string << argv[optind] << ".ch" << ii+1;
	if (!oscil.LoadASCII(ii+1, filename_string.str())) {
		cout << "...Failed to load file" << endl;
		return -2;
	}
	cout << "...File " << filename_string.str() << " loaded" << endl;
  }
  // Let's dump it in raw file format
  filename_string.str("");
  filename_string << argv[optind] << ".raw";

  oscil.Save(filename_string.str());

  return 0;
}

//-------------------------------------------------------------------//
