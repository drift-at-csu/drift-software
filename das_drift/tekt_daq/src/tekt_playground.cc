#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <sstream>
#include <getopt.h>
#include <unistd.h>
using namespace std;

#include "TektDAQOscilloscope.h"
using namespace TektDAQ;

//-------------------------------------------------------------------//
int main (int argc, char **argv) {
  int c;
  stringstream osc_master_addr, osc_slave_addr;
  osc_master_addr << "192.168.1.107";
  osc_slave_addr << "192.168.1.101";
  unsigned long max_number_of_runs = 100;

  while ((c = getopt(argc, argv, "s:m:n:")) != EOF) {
	switch (c) {
		case 's':
			osc_slave_addr.str("");
			osc_slave_addr << optarg;
			continue;
		case 'm':
			osc_master_addr.str("");
			osc_master_addr << optarg;
		case 'n':
			max_number_of_runs = atol(optarg);
			continue;
	};
  }
  if ( (optind + 1) != argc ) { // need to provide a filename
	cerr << "Usage: " << argv[0]
		<< "-s <SLAVE ip_addr> "
		<< "-m <MASTER ip_addr> "
		<< "-n <runs_to_perform> "
		<< " <filename> "
		<< endl;
	return -1;
  }

  stringstream filename_string;
  filename_string << argv[optind];

  cout << "TekDAQ is about to write to " << filename_string.str() << endl;

  Oscilloscope oscil_master, oscil_slave;
  cout << "...Connecting to MASTER at " << osc_master_addr.str() << endl;
  oscil_master.fIpaddr = osc_master_addr.str();
  oscil_master.Connect();

  cout << "...Connecting to SLAVE at " << osc_slave_addr.str() << endl;
  oscil_slave.fIpaddr = osc_slave_addr.str();
  oscil_slave.Connect();

  cout << "...Initializing MASTER Channels" << endl;
  oscil_master.InitializeChannels(0.050, 0.050, 0.050, 0.050);
  cout << "...Initializing SLAVE Channels" << endl;
  oscil_slave.InitializeChannels( 0.050, 0.050, 0.050, 0.050);


  cout << "...Initializing MASTER Trigger" << endl;
  oscil_master.InitializeTrigger();
  cout << "...Initializing SLAVE Trigger" << endl;
  oscil_master.InitializeTrigger();

  cout << "...Initializing MASTER Aquizition" << endl;
  oscil_master.InitializeAquisition(10000000, 10.0); // 1.0 MSPS
  cout << "...Initializing SLAVE Aquizition" << endl;
  oscil_slave.InitializeAquisition(  5000000, 10.0); // 0.5 MSPS

  
  unsigned long cur_run_number = 0;

  stringstream filename_string_slave;
  stringstream filename_string_master;
  while (cur_run_number < max_number_of_runs) {

  	cur_run_number ++;
  	filename_string_slave.str("");
	filename_string_slave << argv[optind] << "_" << cur_run_number << "_slv.raw";
	filename_string_master.str("");
	filename_string_master << argv[optind] << "_" << cur_run_number << "_mst.raw";
	// Slave is first
	oscil_slave.RunTrigger(false); // prepare slave for triggers
	usleep(10000);
	oscil_master.RunTrigger(true); // really run master

	oscil_master.Aquire();
	oscil_master.Save(filename_string_master.str());
	oscil_slave.Aquire();
	oscil_slave.Save(filename_string_slave.str());

	cout << "===Done with run number " << cur_run_number << endl;
  }

  cout << "...Disconnecting from MASTER at " << osc_master_addr.str() << endl;
  oscil_master.Disconnect();
  cout << "...Disconnecting from SLAVE at " << osc_slave_addr.str() << endl;
  oscil_slave.Disconnect();
  return 0;
}

//-------------------------------------------------------------------//
