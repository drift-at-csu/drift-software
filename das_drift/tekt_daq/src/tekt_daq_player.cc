#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <sstream>
#include <getopt.h>
#include <algorithm> // for sort
using namespace std;

#include "TektDAQOscilloscope.h"
#include "TektDAQ_PLD.h"
using namespace TektDAQ;

#include <DAS_DRIFT_Event.h>
#include <DAS_DRIFT_TimeStamp.h>
#include <DAS_DRIFT_TimeInterval.h>

//-------------------------------------------------------------------//
int main (int argc, char **argv) {
  int c;
  string output_filename("");
  while ((c = getopt(argc, argv, "o:")) != EOF) {
	switch (c) {
		case 'o':
			output_filename = optarg;
			continue;
	};
  }
  if ( (optind + 2) != argc ) { // need to provide a filename
	cerr << "Usage: " << argv[0]
		<< "-o <output_filename>"
		<< " <filename_mst.raw> <filename_slv.raw> "
		<< endl;
	return -1;
  }

  stringstream filename_mst_string;
  filename_mst_string << argv[optind];
  stringstream filename_slv_string;
  filename_slv_string << argv[optind+1];

  cout << "TekDAQ Player is about to read from " << filename_mst_string.str() 
  	<<  " and "<< filename_slv_string.str() << endl;
  if (output_filename != "") {
  	cout << "   And write results to " << output_filename << endl;
  }

  Oscilloscope oscil_mst, oscil_slv;

  if (!oscil_mst.Load(filename_mst_string.str())) {
  	cout << "...Failed to load master file" << endl;
	return -2;
  }
  if (!oscil_slv.Load(filename_slv_string.str())) {
  	cout << "...Failed to load slave file" << endl;
	return -2;
  }

  PLD accumul;

  unsigned long ii;
  for (ii = 1; ii <= 4; ++ii){
  	accumul.AddChannel(ii);
	das_drift::Threshold * threshold = accumul.GetThreshold(ii);
	if (ii == 1){
		threshold->fTriggerIntegTime = 40; // 20 usec
		threshold->fAboveThreshold = 1000; // 2400ADCs ~ 11mV
		threshold->fBelowThreshold = 1000; //
	} else if (ii == 2){
		threshold->fTriggerIntegTime = 40; // 20 usec
		threshold->fAboveThreshold = 1000; // 1200
		threshold->fBelowThreshold = 1000; //
	} else if (ii == 3){
		threshold->fTriggerIntegTime = 40; // 20 
		threshold->fAboveThreshold = 1000; // 1100
		threshold->fBelowThreshold = 1000; // 
	} else if (ii == 4){
		threshold->fTriggerIntegTime = 40; // 20
		threshold->fAboveThreshold = 1000; // 1250
		threshold->fBelowThreshold = 1000; //
	}
	threshold->fPreTrigger = 2000; // 2000 usec
	threshold->fAfterTrigger = 4000; // 4000 usec
	threshold->fPedestalIntegTime = 200; // 1000 usec 
	accumul.ProcessRawBuffer(
		oscil_mst.GetTracePointer(ii),
		oscil_mst.GetNofTimebins(ii),
		oscil_mst.GetVertGain(),
		oscil_mst.GetHorInterval(),
		ii);
  }

  for (ii = 5; ii <= 8; ++ii){
	accumul.AddChannel(ii);
	das_drift::Threshold * threshold = accumul.GetThreshold(ii);
	if (ii == 5){
		threshold->fTriggerIntegTime = 20; // 10
		threshold->fAboveThreshold = 500; // 950
		threshold->fBelowThreshold = 500; //
	} else if (ii == 6){
		threshold->fTriggerIntegTime = 20; // 10
		threshold->fAboveThreshold = 500; // 1300
		threshold->fBelowThreshold = 500; //
	} else if (ii == 7){
		threshold->fTriggerIntegTime = 20; // 10
		threshold->fAboveThreshold = 500; // 1350
		threshold->fBelowThreshold = 500; // 
	} else if (ii == 8){
		threshold->fTriggerIntegTime = 20; // 5.0usec
		threshold->fAboveThreshold = 500; // 1100
		threshold->fBelowThreshold = 500; //
	}
	threshold->fPreTrigger = 1000; // 2000.0 usec
	threshold->fAfterTrigger = 2000; // 4000.0usec
	threshold->fPedestalIntegTime = 100; // 1000usec
	accumul.ProcessRawBuffer(
		oscil_slv.GetTracePointer(ii-4),
		oscil_slv.GetNofTimebins(ii-4),
		oscil_slv.GetVertGain(),
		oscil_slv.GetHorInterval(),
		ii);
			
  }

  // At this point we do have everything in triggers list
  // let's create the Event input and add
  std::vector <das_drift::Event> events_list;
  // This is by how much individual triggers should be away
  // in order to be merged in one event in usecs
  das_drift::Trigger * trigger_pnt = NULL;
  das_drift::TimeInterval tmp_interval(0,  2000000); // in nsec

  for (unsigned long ii = 0; ii < accumul.GetNofTriggers(); ++ii) {
  	trigger_pnt = accumul.GetTrigger(ii);
	// We want to correct data from slave here
	if (	(trigger_pnt->fChannelNumber == 5)||
		(trigger_pnt->fChannelNumber == 6)||
		(trigger_pnt->fChannelNumber == 7)||
		(trigger_pnt->fChannelNumber == 8)){

		trigger_pnt->fTriggerTime.SetGPSTime(
			trigger_pnt->fTriggerTime.GetGPSSecond()*2,
			trigger_pnt->fTriggerTime.GetGPSNanoSecond()*2 - 404000);
		// trigger_pnt->fTotalTimeBinsAbove*=2;
		// trigger_pnt->fTotalTimeBinsBelow*=2;
	}

	int trigger_belongs_to_event = 0;
	for (unsigned long jj = 0; jj < events_list.size(); ++jj) {
		if (events_list[jj].isTimeClose(trigger_pnt->fTriggerTime,
			tmp_interval) ){
			// This means this trigger belongs to
			// an existing event
			events_list[jj].AddTrigger(*trigger_pnt);
			trigger_belongs_to_event = 1;
			break;
		}
	}
	// We listed through all the events and didn't find belonging trigger
	if (trigger_belongs_to_event == 0) {
		events_list.push_back(das_drift::Event());
		unsigned long jj = events_list.size() - 1;
		events_list[jj].AddTrigger(*trigger_pnt);
	}
  }

  // Now how many events do we have:
  cout << "... EventStart\tEventEnd\t[ Channels]=Nof Trigs\tTotSignal\tTotTimebins" << endl;


  // let's fill matrix for each chennel
  long chan1_mat[8] = {0, 0, 0, 0, 0, 0, 0, 0};
  long chan2_mat[7] = {0, 0, 0, 0, 0, 0, 0 };
  long chan3_mat[6] = {0, 0, 0, 0, 0, 0 };
  long chan4_mat[5] = {0, 0, 0, 0, 0 };
  long chan5_mat[4] = {0, 0, 0, 0 };
  long chan6_mat[3] = {0, 0, 0 };
  long chan7_mat[2] = {0, 0 };
  long chan8_mat = 0;

  // This is a total number of events based on channel
  long chan_nof_trigs [8];
  for (unsigned long ii = 0; ii < 8; ++ii){
  	chan_nof_trigs[ii] = 0;
  }
  for (unsigned long jj = 0; jj < events_list.size(); ++jj){
  	// if ( events_list[jj].fTotalSignal > 0) {
		events_list[jj].SortTriggersByTime();
		// in a given event the triggers are time ordered now,
		// let's recalculate the real event duration

  	  	cout 	<< events_list[jj].fEventStartTime.GetGPSSecond()*1000000 + 
			events_list[jj].fEventStartTime.GetGPSNanoSecond()/1000 << "\t"

			<< events_list[jj].fEventStopTime.GetGPSSecond()*1000000 +
		   	events_list[jj].fEventStopTime.GetGPSNanoSecond()/1000 << "\t"

			<< "\tTotalSignal=" << events_list[jj].fTotalSignal
		        << "\tDuration=" << events_list[jj].fTotalTimeBins 
			<< endl;


		// Let's Print the summary for each channel
		cout << "\t\tChan\tTime\tLength\tSignal" << endl;
		das_drift::TimeStamp tmp_ts_of_first_trigger;
		for (unsigned long ii = 0; ii < events_list[jj].GetNofTriggers();++ii ) {
			cout << "\t\t" << events_list[jj].fTriggers[ii].fChannelNumber;
			if (events_list[jj].fTriggers[ii].fTriggerType == 0x1)
				cout << " Neg";
			else if (events_list[jj].fTriggers[ii].fTriggerType == 0x3)
				cout << " Pos";

			if (ii == 0 ){
				// This is the first triggered event
				tmp_ts_of_first_trigger = events_list[jj].fTriggers[ii].fTriggerTime;
				cout << "\t0";
				cout << "\t" << events_list[jj].fTriggers[ii].fTotalTimeBins;
				cout << "\t" 
					<< events_list[jj].fTriggers[ii].fPositiveSignal +
					   events_list[jj].fTriggers[ii].fNegativeSignal
					<< endl;
				continue;
			}
			// We want here the time from the begining of the event
			// to the trigger time in usecs
			das_drift::TimeInterval tmp_time_interval;
			tmp_time_interval =
				events_list[jj].fTriggers[ii].fTriggerTime - 
				tmp_ts_of_first_trigger;

			cout << "\t" << tmp_time_interval.GetSecond()*1000000 +
					tmp_time_interval.GetNanoSecond()/1000;

			cout << "\t" << events_list[jj].fTriggers[ii].fTotalTimeBins;

			cout << "\t"  << events_list[jj].fTriggers[ii].fPositiveSignal +
					events_list[jj].fTriggers[ii].fNegativeSignal
				<< endl;
		} // End of list for triggers in the event



		if ( events_list[jj].isChannelThere(1) ) {
		   chan_nof_trigs[0] ++;
		   if (events_list[jj].GetNofChannels() == 1) {
		  	chan1_mat[0] ++;
		   } else {
			if ( events_list[jj].isChannelThere(2) ) chan1_mat[1] ++;
			if ( events_list[jj].isChannelThere(3) ) chan1_mat[2] ++;
			if ( events_list[jj].isChannelThere(4) ) chan1_mat[3] ++;
			if ( events_list[jj].isChannelThere(5) ) chan1_mat[4] ++;
			if ( events_list[jj].isChannelThere(6) ) chan1_mat[5] ++;
			if ( events_list[jj].isChannelThere(7) ) chan1_mat[6] ++;
			if ( events_list[jj].isChannelThere(8) ) chan1_mat[7] ++;
		   }

		}
		if ( events_list[jj].isChannelThere(2) ) {
		   chan_nof_trigs[1] ++;
		   if (events_list[jj].GetNofChannels() == 1) {
		   	chan2_mat[0] ++;
		   } else {
		   	if ( events_list[jj].isChannelThere(3) ) chan2_mat[1] ++;
			if ( events_list[jj].isChannelThere(4) ) chan2_mat[2] ++;
			if ( events_list[jj].isChannelThere(5) ) chan2_mat[3] ++;
			if ( events_list[jj].isChannelThere(6) ) chan2_mat[4] ++;
			if ( events_list[jj].isChannelThere(7) ) chan2_mat[5] ++;
			if ( events_list[jj].isChannelThere(8) ) chan2_mat[6] ++;
		   }
		}
		if ( events_list[jj].isChannelThere(3) ) {
		   chan_nof_trigs[2] ++;
		   if (events_list[jj].GetNofChannels() == 1) {
			chan3_mat[0] ++;
		   } else {
			if ( events_list[jj].isChannelThere(4) ) chan3_mat[1] ++;
			if ( events_list[jj].isChannelThere(5) ) chan3_mat[2] ++;
			if ( events_list[jj].isChannelThere(6) ) chan3_mat[3] ++;
			if ( events_list[jj].isChannelThere(7) ) chan3_mat[4] ++;
			if ( events_list[jj].isChannelThere(8) ) chan3_mat[5] ++;
		   }
		}
		if ( events_list[jj].isChannelThere(4) ) {
		   chan_nof_trigs[3] ++;
		   if (events_list[jj].GetNofChannels() == 1) {
		   	chan4_mat[0] ++;
		   } else {
			if ( events_list[jj].isChannelThere(5) ) chan4_mat[1] ++;
			if ( events_list[jj].isChannelThere(6) ) chan4_mat[2] ++;
			if ( events_list[jj].isChannelThere(7) ) chan4_mat[3] ++;
			if ( events_list[jj].isChannelThere(8) ) chan4_mat[4] ++;
		   }
		}
		if ( events_list[jj].isChannelThere(5) ) {
		   chan_nof_trigs[4] ++;
		   if (events_list[jj].GetNofChannels() == 1) {
			chan5_mat[0] ++;
		   } else {
			if ( events_list[jj].isChannelThere(6) ) chan5_mat[1] ++;
			if ( events_list[jj].isChannelThere(7) ) chan5_mat[2] ++;
			if ( events_list[jj].isChannelThere(8) ) chan5_mat[3] ++;
		   }
		}
		if ( events_list[jj].isChannelThere(6) ) {
		   chan_nof_trigs[5] ++;
		   if (events_list[jj].GetNofChannels() == 1) {
			chan6_mat[0] ++;
		   } else {
			if ( events_list[jj].isChannelThere(7) ) chan6_mat[1] ++;
			if ( events_list[jj].isChannelThere(8) ) chan6_mat[2] ++;
		   }
		}
		if ( events_list[jj].isChannelThere(7) ) {
		   chan_nof_trigs[6] ++;
		   if (events_list[jj].GetNofChannels() == 1) {
			chan7_mat[0] ++;
		   } else {
			if ( events_list[jj].isChannelThere(8) ) chan7_mat[1] ++;
		   }
		}
		if ( events_list[jj].isChannelThere(8) ) {
		   chan_nof_trigs[7] ++;
		   if (events_list[jj].GetNofChannels() == 1) {
		   	chan8_mat ++;
		   }
		}
	// }

  }


  cout << "...Summary: Trigg = " << accumul.GetNofTriggers()
	<< " Traces = " <<  accumul.GetNofTraces()
	<< " Channels = " << accumul.GetNofChannels()
	<< endl;

  cout << "...Events Summary: Nof=" << events_list.size() << endl;

  cout << "...Channels Correlation Table:" << endl;
  cout << " |\t1\t2\t3\t4\t5\t6\t7\t8" << endl;
  cout << "1|\t" << chan1_mat[0] << "\t" << chan1_mat[1] << "\t" << chan1_mat[2] << "\t"
  	<< chan1_mat[3] << "\t" <<  chan1_mat[4] << "\t" << chan1_mat[5] << "\t"
	<< chan1_mat[6] << "\t" << chan1_mat[7] << endl;
  cout << "2|\tx\t" << chan2_mat[0] << "\t" << chan2_mat[1] << "\t" << chan2_mat[2] << "\t"
  	<< chan2_mat[3] << "\t" << chan2_mat[4] << "\t" << chan2_mat[5] << "\t"
	<< chan2_mat[6] << endl;
  cout << "3|\tx\tx\t" << chan3_mat[0] << "\t" << chan3_mat[1] << "\t" << chan3_mat[2] << "\t"
  	<< chan3_mat[3] << "\t" << chan3_mat[4] << "\t" << chan3_mat[5] << endl;
  cout << "4|\tx\tx\tx\t" << chan4_mat[0] << "\t" << chan4_mat[1] << "\t" << chan4_mat[2] << "\t"
  	<< chan4_mat[3] << "\t" << chan4_mat[4] << endl;
  cout << "5|\tx\tx\tx\tx\t" << chan5_mat[0] << "\t" << chan5_mat[1] << "\t" << chan5_mat[2] << "\t"
  	<< chan5_mat[3] << endl;
  cout << "6|\tx\tx\tx\tx\tx\t" << chan6_mat[0] << "\t" << chan6_mat[1] << "\t" << chan6_mat[2] 
  	<< endl;
  cout << "7|\tx\tx\tx\tx\tx\tx\t" << chan7_mat[0] << "\t" << chan7_mat[1] << endl;
  cout << "8|\tx\tx\tx\tx\tx\tx\tx\t" << chan8_mat << endl;

  cout << "...Channel Triggers:" << endl;
  cout  << "1:" << chan_nof_trigs[0] << endl << "2:" << chan_nof_trigs[1] << endl 
  	<< "3:" << chan_nof_trigs[2] << endl << "4:" << chan_nof_trigs[3] << endl 
	<< "5:" << chan_nof_trigs[4] << endl << "6:" << chan_nof_trigs[5] << endl 
	<< "7:" << chan_nof_trigs[6] << endl << "8:" << chan_nof_trigs[7] << endl;

  return 0;
}

//-------------------------------------------------------------------//
