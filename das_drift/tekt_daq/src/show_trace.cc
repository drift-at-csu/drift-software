#include <TROOT.h>
#include <TStyle.h>
#include <TApplication.h>
#include <TCanvas.h>
#include <TH1D.h>
#include <TH1I.h> // One short per channel
#include <TF1.h> // For Fit functiona
#include <TMath.h>

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <sstream>
#include <fstream>
using namespace std;

#include <DAS_DRIFT_ACcoupling.h>
#include <DAS_DRIFT_Savitzky_Golay.h>
using namespace das_drift;

//-------------------------------------------------------------------//
// Fit Function to fit will be Landau convoluted with gauss or langau
Double_t langaufun(Double_t *x, Double_t *par) {

  // Fit parameters:
  // par[0]=Gaussian mean
  // par[1]=Gaussian Sigma
  // par[2]=Total area (integral -inf to inf, normalization constant)
 
  return (par[2] * TMath::Gaus(x[0], par[0],par[1], kTRUE));
}
//-------------------------------------------------------------------//
int main (int argc, char **argv) {
  int c;
  unsigned long master_start_bin = 1;
  unsigned long master_stop_bin = 1000;
  unsigned long slave_start_bin = 1;
  unsigned long slave_stop_bin = 500;
  long master_to_slave_offset = 0;

  unsigned long master_average_over_bin = 2;
  unsigned long slave_average_over_bin = 1;

  unsigned short number_of_accoupling_correct = 0;
  unsigned long integrate_over_bin = 1;
  unsigned long savitz_golay_nof_bins = 0;
  unsigned short after_ac_correct = 0;
  unsigned short number_of_scope_files = 0;

  while ((c = getopt(argc, argv, "S:P:A:C:I:F:EO:")) != EOF) {
	switch (c) {
		case 'S':
			master_start_bin = atol(optarg);
			slave_start_bin = master_start_bin/2;
			continue;
		case 'P':
			master_stop_bin = atol(optarg);
			slave_stop_bin = master_stop_bin/2;
			continue;
		case 'A':
			master_average_over_bin = atol(optarg);
			slave_average_over_bin = master_average_over_bin / 2;
			continue;
		case 'C':
			number_of_accoupling_correct = atol(optarg);
			continue;
		case 'I':
			integrate_over_bin = atol(optarg);
			continue;
		case 'F':
			savitz_golay_nof_bins = atol(optarg);
			continue;
		case 'E':
			after_ac_correct = 1;
			continue;
		case 'O':
			master_to_slave_offset = atol(optarg);
			continue;
	};
  }
  number_of_scope_files = argc - optind;
  cout << "...Number of RAW files provided " << number_of_scope_files << endl;

  if ( number_of_scope_files < 1 ) { // need to provide a filename
	cerr << "Usage: " << argv[0]
		<< "-S <start_bin> -P <stop_bin> "
		<< "-C <nof_corr> -I <nof_integrate_bins> "
		<< "-F <nof bins for SG filtering> -A <nof_average_bins> "
		<< "-E [correct AC coupling] "
		<< "-O <nof_bins_slave_to_master> " 
		<< " <filename_mst.raw> <filename_slv.raw> "
		<< endl;
	return -1;
  }

  TApplication theApp ("App", NULL, 0);
  gROOT->Reset();
  gStyle->SetOptStat(kFALSE);
  gStyle->SetFrameBorderMode(0);
  gROOT->ForceStyle();
  

  stringstream master_filename_string;
  master_filename_string << argv[optind];

  stringstream slave_filename_string;
  if (number_of_scope_files > 1) {
  	slave_filename_string << argv[optind+1];
  }

  cout << "Traces are about to be read from " << master_filename_string.str();
  if (number_of_scope_files > 1) {
  	cout << " and " << slave_filename_string.str();
  }
  cout << endl;

  slave_start_bin += master_to_slave_offset/2;
  slave_stop_bin += master_to_slave_offset/2;

  cout << "...Slave is shifted by " << master_to_slave_offset/2 << " bins" << endl;

  TCanvas myc("myc", "Tektronix Random trigger Viewer", 800, 600);
  myc.SetFillColor(kWhite);
  myc.cd();
  myc.SetBorderMode(0);
  myc.Draw();


  TPad canv1("canv1", "Trace Pad",0.01,0.51,0.99,0.99);
  canv1.SetBorderMode(0);
  canv1.Draw();
  canv1.SetFillColor(kWhite);
  canv1.SetFrameFillColor(kWhite);
  canv1.SetGridx();
  canv1.SetGridy();
  // canv1.cd();

  TPad canv2("canv2", "Current Pad",0.01,0.01,0.99,0.49);
  canv2.SetBorderMode(0);
  canv2.Draw();
  canv2.SetFillColor(kWhite);
  canv2.SetFrameFillColor(kWhite);
  canv2.SetGridx();
  canv2.SetGridy();

  canv1.cd();

  double master_hor_interval = 0.0;
  unsigned long master_nop_in_file = 0;
  double master_vert_gain = 0.0;
  unsigned long master_noch_in_file = 0;

  double slave_hor_interval = 0.0;
  unsigned long slave_nop_in_file = 0;
  double slave_vert_gain = 0.0;
  unsigned long slave_noch_in_file = 0;

  unsigned long header_length; // in bytes
  header_length = sizeof(unsigned long) +
  	sizeof(unsigned long) +
	sizeof(double) +
	sizeof(double);

  ifstream master_file(master_filename_string.str().c_str(), ios::binary);
  // Let's read the header
  master_file.read(reinterpret_cast< char*> (&master_nop_in_file), sizeof(unsigned long));
  master_file.read(reinterpret_cast< char*> (&master_noch_in_file), sizeof(unsigned long));
  master_file.read(reinterpret_cast< char*> (&master_vert_gain), sizeof(double));
  master_file.read(reinterpret_cast< char*> (&master_hor_interval), sizeof(double));


  ifstream slave_file(slave_filename_string.str().c_str(), ios::binary);
  if (number_of_scope_files > 1) {
	slave_file.read(reinterpret_cast< char*> (&slave_nop_in_file), sizeof(unsigned long));
	slave_file.read(reinterpret_cast< char*> (&slave_noch_in_file), sizeof(unsigned long));
	slave_file.read(reinterpret_cast< char*> (&slave_vert_gain), sizeof(double));
	slave_file.read(reinterpret_cast< char*> (&slave_hor_interval), sizeof(double));
  }


  // FIle Header should be read
  cout << "...Header: " << endl;
  cout << "   Number of Points per channel: " << master_nop_in_file << endl;
  cout << "   Number of Channels: " << master_noch_in_file << endl;
  cout << "   Vertical Gain: " << master_vert_gain << " Volts" << endl;
  cout << "   Horizontal bin width: " << master_hor_interval*1.0E+6 << " us" << endl;

  if (number_of_scope_files > 1) {
	cout << "...Header: " << endl;
	cout << "   Number of Points per channel: " << slave_nop_in_file << endl;
	cout << "   Number of Channels: " << slave_noch_in_file << endl;
	cout << "   Vertical Gain: " << slave_vert_gain << " Volts" << endl;
	cout << "   Horizontal bin width: " << slave_hor_interval*1.0E+6 << " us" << endl;
  }

 
  double bins_in_pedestal = 200; // 3600

  // Let's Create TH1S histograms
  TH1D ch1_histo("ch1_histo", "Channel 1", (master_stop_bin - master_start_bin)/
  		master_average_over_bin, 
  	( -bins_in_pedestal*master_average_over_bin)* master_hor_interval * 1.0E+6, 
	(master_stop_bin - master_start_bin - bins_in_pedestal*master_average_over_bin)* 
		master_hor_interval * 1.0E+6);
  TH1D ch2_histo("ch2_histo", "Channel 2", (master_stop_bin - master_start_bin)/
  		master_average_over_bin,
  	( -bins_in_pedestal*master_average_over_bin )* master_hor_interval * 1.0E+6, 
	(master_stop_bin - master_start_bin - bins_in_pedestal*master_average_over_bin)* 
		master_hor_interval * 1.0E+6);
  TH1D ch3_histo("ch3_histo", "Channel 3", (master_stop_bin - master_start_bin)/
  		master_average_over_bin,
  	( -bins_in_pedestal*master_average_over_bin ) * master_hor_interval * 1.0E+6, 
	(master_stop_bin - master_start_bin - bins_in_pedestal*master_average_over_bin) * 
		master_hor_interval * 1.0E+6);
  TH1D ch4_histo("ch4_histo", "AC Corrected Traces", (master_stop_bin - master_start_bin)/
  		master_average_over_bin,
  	( -bins_in_pedestal*master_average_over_bin ) * master_hor_interval * 1.0E+6, 
	(master_stop_bin - master_start_bin - bins_in_pedestal*master_average_over_bin) * 
		master_hor_interval * 1.0E+6);

  TH1D ch5_histo("ch5_histo", "Channel 5", (slave_stop_bin - slave_start_bin)/
  		slave_average_over_bin,
	( -bins_in_pedestal*slave_average_over_bin)* slave_hor_interval * 1.0E+6,
	(slave_stop_bin - slave_start_bin - bins_in_pedestal*slave_average_over_bin)* 
		slave_hor_interval * 1.0E+6);
  TH1D ch6_histo("ch6_histo", "Channel 6", (slave_stop_bin - slave_start_bin)/
  		slave_average_over_bin,
	( -bins_in_pedestal*slave_average_over_bin )* slave_hor_interval * 1.0E+6,
	(slave_stop_bin - slave_start_bin - bins_in_pedestal*slave_average_over_bin)* 
		slave_hor_interval * 1.0E+6);
  TH1D ch7_histo("ch7_histo", "Channel 7", (slave_stop_bin - slave_start_bin)/
  		slave_average_over_bin,
	( -bins_in_pedestal*slave_average_over_bin ) * slave_hor_interval * 1.0E+6,
	(slave_stop_bin - slave_start_bin - bins_in_pedestal*slave_average_over_bin)* 
		slave_hor_interval * 1.0E+6);
  TH1D ch8_histo("ch8_histo", "AC Corrected Traces", (slave_stop_bin - slave_start_bin)/
  		slave_average_over_bin,
	( -bins_in_pedestal*slave_average_over_bin ) * slave_hor_interval * 1.0E+6,
	(slave_stop_bin - slave_start_bin - bins_in_pedestal*slave_average_over_bin)*
		slave_hor_interval * 1.0E+6);

  // Now, we should already be at the begining of CH1, let's jump to the begining of data
  master_file.seekg (header_length + ((master_start_bin - 1)* sizeof(short)), ios::beg);
  short curr_rdout_val;
  long accum_rdout_val;
  for (unsigned long ii = 0; ii < (master_stop_bin - master_start_bin)/master_average_over_bin; ++ii) {
  	accum_rdout_val = 0;
	for (unsigned long jj = 0; jj < master_average_over_bin; ++jj){
		master_file.read(reinterpret_cast< char*> (&curr_rdout_val), sizeof(short));
		accum_rdout_val += curr_rdout_val;
	}
  	ch1_histo.SetBinContent(ii+1, (double) accum_rdout_val*master_vert_gain/master_average_over_bin);
  }
  master_file.seekg (header_length + ((master_start_bin - 1)* sizeof(short)) + 
  	master_nop_in_file * sizeof(short), ios::beg);
  for (unsigned long ii = 0; ii < (master_stop_bin - master_start_bin)/master_average_over_bin; ++ii) {
  	accum_rdout_val = 0;
	for (unsigned long jj = 0; jj < master_average_over_bin; ++jj){
  		master_file.read(reinterpret_cast< char*> (&curr_rdout_val), sizeof(short));
		accum_rdout_val += curr_rdout_val;
	}
	ch2_histo.SetBinContent(ii+1, (double) accum_rdout_val*master_vert_gain/master_average_over_bin);
  }
  master_file.seekg (header_length + ((master_start_bin - 1)* sizeof(short)) + 
         ( master_nop_in_file * sizeof(short) * 2), ios::beg);
  for (unsigned long ii = 0; ii < (master_stop_bin - master_start_bin)/master_average_over_bin; ++ii) {
  	accum_rdout_val = 0;
	for (unsigned long jj = 0; jj < master_average_over_bin; ++jj){
		master_file.read(reinterpret_cast< char*> (&curr_rdout_val), sizeof(short));
		accum_rdout_val += curr_rdout_val;
	}
	ch3_histo.SetBinContent(ii+1, (double) accum_rdout_val*master_vert_gain/master_average_over_bin);
  }
  master_file.seekg (header_length + ((master_start_bin - 1)* sizeof(short)) +
  	( master_nop_in_file * sizeof(short) * 3), ios::beg);
  for (unsigned long ii = 0; ii < (master_stop_bin - master_start_bin)/master_average_over_bin; ++ii) {
  	accum_rdout_val = 0;
	for (unsigned long jj = 0; jj < master_average_over_bin; ++jj){
  		master_file.read(reinterpret_cast< char*> (&curr_rdout_val), sizeof(short));
		accum_rdout_val += curr_rdout_val;
	}
	ch4_histo.SetBinContent(ii+1, (double) accum_rdout_val*master_vert_gain/master_average_over_bin);
  }
  master_file.close();

  if (number_of_scope_files > 1) {
    slave_file.seekg (header_length + ((slave_start_bin - 1)* sizeof(short)), ios::beg);
    for (unsigned long ii = 0; ii < (slave_stop_bin - slave_start_bin)/slave_average_over_bin; ++ii) {
	accum_rdout_val = 0;
	for (unsigned long jj = 0; jj < slave_average_over_bin; ++jj){
		slave_file.read(reinterpret_cast< char*> (&curr_rdout_val), sizeof(short));
		accum_rdout_val += curr_rdout_val;
	}
	ch5_histo.SetBinContent(ii+1, (double) accum_rdout_val*slave_vert_gain/slave_average_over_bin);
    }
    slave_file.seekg (header_length + ((slave_start_bin - 1)* sizeof(short)) +
	slave_nop_in_file * sizeof(short), ios::beg);
    for (unsigned long ii = 0; ii < (slave_stop_bin - slave_start_bin)/slave_average_over_bin; ++ii) {
	accum_rdout_val = 0;
	for (unsigned long jj = 0; jj < slave_average_over_bin; ++jj){
		slave_file.read(reinterpret_cast< char*> (&curr_rdout_val), sizeof(short));
		accum_rdout_val += curr_rdout_val;
	}
	ch6_histo.SetBinContent(ii+1, (double) accum_rdout_val*slave_vert_gain/slave_average_over_bin);
    }
    slave_file.seekg (header_length + ((slave_start_bin - 1)* sizeof(short)) +
	( slave_nop_in_file * sizeof(short) * 2), ios::beg);
    for (unsigned long ii = 0; ii < (slave_stop_bin - slave_start_bin)/slave_average_over_bin; ++ii) {
	accum_rdout_val = 0;
	for (unsigned long jj = 0; jj < slave_average_over_bin; ++jj){
		slave_file.read(reinterpret_cast< char*> (&curr_rdout_val), sizeof(short));
		accum_rdout_val += curr_rdout_val;
	}
	ch7_histo.SetBinContent(ii+1, (double) accum_rdout_val*slave_vert_gain/slave_average_over_bin);
    }
    slave_file.seekg (header_length + ((slave_start_bin - 1)* sizeof(short)) +
	( slave_nop_in_file * sizeof(short) * 3), ios::beg);
    for (unsigned long ii = 0; ii < (slave_stop_bin - slave_start_bin)/slave_average_over_bin; ++ii) {
	accum_rdout_val = 0;
	for (unsigned long jj = 0; jj < slave_average_over_bin; ++jj){
		slave_file.read(reinterpret_cast< char*> (&curr_rdout_val), sizeof(short));
		accum_rdout_val += curr_rdout_val;
	}
	ch8_histo.SetBinContent(ii+1, (double) accum_rdout_val*slave_vert_gain/slave_average_over_bin);
    }
  }
  slave_file.close();


  // Let's AC couple deconvolve
  ACcoupling ac_corr[8];
  double bin_result;
  double pedestal[8];

  for (unsigned short kk = 0; kk < number_of_accoupling_correct; ++kk){
    if (kk == 0){
    	// 2nd stage AC coupling
	ac_corr[0].GenerateTemplate(110.0, master_average_over_bin*master_hor_interval*1.0E+6, 
		400000/master_average_over_bin);
	ac_corr[1].GenerateTemplate(110.0, master_average_over_bin*master_hor_interval*1.0E+6, 
		400000/master_average_over_bin);
        ac_corr[2].GenerateTemplate(110.0, master_average_over_bin*master_hor_interval*1.0E+6, 
		400000/master_average_over_bin);
	ac_corr[3].GenerateTemplate(110.0, master_average_over_bin*master_hor_interval*1.0E+6, 
		400000/master_average_over_bin);
	ac_corr[4].GenerateTemplate(110.0, slave_average_over_bin*slave_hor_interval*1.0E+6,
		400000/slave_average_over_bin);
	ac_corr[5].GenerateTemplate(110.0, slave_average_over_bin*slave_hor_interval*1.0E+6,
		400000/slave_average_over_bin);
	ac_corr[6].GenerateTemplate(110.0, slave_average_over_bin*slave_hor_interval*1.0E+6,
		400000/slave_average_over_bin);
	ac_corr[7].GenerateTemplate(110.0, slave_average_over_bin*slave_hor_interval*1.0E+6,
		400000/slave_average_over_bin);
    } else {
    	// 1st stage AC coupling
	ac_corr[0].GenerateTemplate(110.0, master_average_over_bin*master_hor_interval*1.0E+6, 
		400000/master_average_over_bin);
	ac_corr[1].GenerateTemplate(110.0, master_average_over_bin*master_hor_interval*1.0E+6, 
		400000/master_average_over_bin);
	ac_corr[2].GenerateTemplate(110.0, master_average_over_bin*master_hor_interval*1.0E+6, 
		400000/master_average_over_bin);
	ac_corr[3].GenerateTemplate(110.0, master_average_over_bin*master_hor_interval*1.0E+6, 
		400000/master_average_over_bin);
	ac_corr[4].GenerateTemplate(110.0, slave_average_over_bin*slave_hor_interval*1.0E+6,
		400000/slave_average_over_bin);
	ac_corr[5].GenerateTemplate(110.0, slave_average_over_bin*slave_hor_interval*1.0E+6,
		400000/slave_average_over_bin);
	ac_corr[6].GenerateTemplate(110.0, slave_average_over_bin*slave_hor_interval*1.0E+6,
		400000/slave_average_over_bin);
	ac_corr[7].GenerateTemplate(110.0, slave_average_over_bin*slave_hor_interval*1.0E+6,
		400000/slave_average_over_bin);
    }


    pedestal[0] = 0.0; pedestal[1] = 0.0; pedestal[2] = 0.0; pedestal[3] = 0.0;
    pedestal[4] = 0.0; pedestal[5] = 0.0; pedestal[6] = 0.0; pedestal[7] = 0.0;
    for (unsigned long ii = 0; ii < bins_in_pedestal; ++ii) {

	pedestal[0] += ch1_histo.GetBinContent(ii+1);
	pedestal[1] += ch2_histo.GetBinContent(ii+1);
	pedestal[2] += ch3_histo.GetBinContent(ii+1);
	pedestal[3] += ch4_histo.GetBinContent(ii+1);
	pedestal[4] += ch5_histo.GetBinContent(ii+1);
	pedestal[5] += ch6_histo.GetBinContent(ii+1);
	pedestal[6] += ch7_histo.GetBinContent(ii+1);
	pedestal[7] += ch8_histo.GetBinContent(ii+1);
    }
    pedestal[0] /= bins_in_pedestal; pedestal[1] /= bins_in_pedestal;
    pedestal[2] /= bins_in_pedestal; pedestal[3] /= bins_in_pedestal;
    pedestal[4] /= bins_in_pedestal; pedestal[5] /= bins_in_pedestal;
    pedestal[6] /= bins_in_pedestal; pedestal[7] /= bins_in_pedestal;

    cout << "...Pedestals before: " << pedestal[0] << "," << pedestal[1] << ","
    	<< pedestal[2] << "," << pedestal[3] << ","
	<< pedestal[4] << "," << pedestal[5] << ","
	<< pedestal[6] << "," << pedestal[7]
	<< endl;


    for (unsigned long ii = 0; ii < (master_stop_bin - master_start_bin)/master_average_over_bin; ++ii) {
	bin_result = ch1_histo.GetBinContent(ii+1);
	ch1_histo.SetBinContent(ii+1, bin_result - pedestal[0]);
	bin_result = ch2_histo.GetBinContent(ii+1);
	ch2_histo.SetBinContent(ii+1, bin_result - pedestal[1]);
	bin_result = ch3_histo.GetBinContent(ii+1);
	ch3_histo.SetBinContent(ii+1, bin_result - pedestal[2]);
	bin_result = ch4_histo.GetBinContent(ii+1);
	ch4_histo.SetBinContent(ii+1, bin_result - pedestal[3]);
    }
    for (unsigned long ii = 0; ii < (slave_stop_bin - slave_start_bin)/slave_average_over_bin; ++ii) {
	bin_result = ch5_histo.GetBinContent(ii+1);
	ch5_histo.SetBinContent(ii+1, bin_result - pedestal[4]);
	bin_result = ch6_histo.GetBinContent(ii+1);
	ch6_histo.SetBinContent(ii+1, bin_result - pedestal[5]);
	bin_result = ch7_histo.GetBinContent(ii+1);
	ch7_histo.SetBinContent(ii+1, bin_result - pedestal[6]);
	bin_result = ch8_histo.GetBinContent(ii+1);
	ch8_histo.SetBinContent(ii+1, bin_result - pedestal[7]);
    }
    double iter_bin_result;
    
    for (unsigned long ii = 0; ii < (master_stop_bin - master_start_bin)/master_average_over_bin; ++ii) {
	bin_result = ch1_histo.GetBinContent(ii+1);
	for (unsigned long jj = ii; 
			jj < (master_stop_bin - master_start_bin)/master_average_over_bin; ++jj) {
		iter_bin_result = ch1_histo.GetBinContent(jj+1);
		iter_bin_result += bin_result * ac_corr[0].GetTemplate(jj-ii);
		ch1_histo.SetBinContent(jj+1, iter_bin_result);
		
	}
    }
    
    for (unsigned long ii = 0; ii < (master_stop_bin - master_start_bin)/master_average_over_bin; ++ii) {
    	bin_result = ch2_histo.GetBinContent(ii+1);
	for (unsigned long jj = ii; 
			jj < (master_stop_bin - master_start_bin)/master_average_over_bin; ++jj) {
		iter_bin_result = ch2_histo.GetBinContent(jj+1);
		iter_bin_result += bin_result * ac_corr[1].GetTemplate(jj-ii);
		ch2_histo.SetBinContent(jj+1, iter_bin_result);
	}
    }

    for (unsigned long ii = 0; ii < (master_stop_bin - master_start_bin)/master_average_over_bin; ++ii) {
	bin_result = ch3_histo.GetBinContent(ii+1);
	for (unsigned long jj = ii; 
			jj < (master_stop_bin - master_start_bin)/master_average_over_bin; ++jj) {
		iter_bin_result = ch3_histo.GetBinContent(jj+1);
		iter_bin_result += bin_result * ac_corr[2].GetTemplate(jj-ii);
		ch3_histo.SetBinContent(jj+1, iter_bin_result);
	}
    }
    for (unsigned long ii = 0; ii < (master_stop_bin - master_start_bin)/master_average_over_bin; ++ii) {
	bin_result = ch4_histo.GetBinContent(ii+1);
	for (unsigned long jj = ii; 
			jj < (master_stop_bin - master_start_bin)/master_average_over_bin; ++jj) {
		iter_bin_result = ch4_histo.GetBinContent(jj+1);
		iter_bin_result += bin_result * ac_corr[3].GetTemplate(jj-ii);
		ch4_histo.SetBinContent(jj+1, iter_bin_result);
	}
    }

    for (unsigned long ii = 0; ii < (slave_stop_bin - slave_start_bin)/slave_average_over_bin; ++ii) {
	bin_result = ch5_histo.GetBinContent(ii+1);
	for (unsigned long jj = ii;
			jj < (slave_stop_bin - slave_start_bin)/slave_average_over_bin; ++jj) {
		iter_bin_result = ch5_histo.GetBinContent(jj+1);
		iter_bin_result += bin_result * ac_corr[4].GetTemplate(jj-ii);
		ch5_histo.SetBinContent(jj+1, iter_bin_result);
	}
    }
    for (unsigned long ii = 0; ii < (slave_stop_bin - slave_start_bin)/slave_average_over_bin; ++ii) {
	bin_result = ch6_histo.GetBinContent(ii+1);
	for (unsigned long jj = ii;
			jj < (slave_stop_bin - slave_start_bin)/slave_average_over_bin; ++jj) {
		iter_bin_result = ch6_histo.GetBinContent(jj+1);
		iter_bin_result += bin_result * ac_corr[5].GetTemplate(jj-ii);
		ch6_histo.SetBinContent(jj+1, iter_bin_result);
	}
    }
    for (unsigned long ii = 0; ii < (slave_stop_bin - slave_start_bin)/slave_average_over_bin; ++ii) {
	bin_result = ch7_histo.GetBinContent(ii+1);
	for (unsigned long jj = ii;
		jj < (slave_stop_bin - slave_start_bin)/slave_average_over_bin; ++jj) {
		iter_bin_result = ch7_histo.GetBinContent(jj+1);
		iter_bin_result += bin_result * ac_corr[6].GetTemplate(jj-ii);
		ch7_histo.SetBinContent(jj+1, iter_bin_result);
	}
    }
    for (unsigned long ii = 0; ii < (slave_stop_bin - slave_start_bin)/slave_average_over_bin; ++ii) {
	bin_result = ch8_histo.GetBinContent(ii+1);
	for (unsigned long jj = ii;
			jj < (slave_stop_bin - slave_start_bin)/slave_average_over_bin; ++jj) {
		iter_bin_result = ch8_histo.GetBinContent(jj+1);
		iter_bin_result += bin_result * ac_corr[7].GetTemplate(jj-ii);
		ch8_histo.SetBinContent(jj+1, iter_bin_result);
	}
    }

    // If one wants to correct for 1/f frequency noise
    if ((after_ac_correct) && ((kk ==0)||(kk ==1))){
    	pedestal[0] = 0.0; pedestal[1] = 0.0; pedestal[2] = 0.0; pedestal[3] = 0.0;
	pedestal[4] = 0.0; pedestal[5] = 0.0; pedestal[6] = 0.0; pedestal[7] = 0.0;
	for (unsigned long ii = (master_stop_bin - master_start_bin)/master_average_over_bin - bins_in_pedestal; 
			ii < (master_stop_bin - master_start_bin)/master_average_over_bin; ++ii) {
		pedestal[0] += ch1_histo.GetBinContent(ii+1);
		pedestal[1] += ch2_histo.GetBinContent(ii+1);
		pedestal[2] += ch3_histo.GetBinContent(ii+1);
		pedestal[3] += ch4_histo.GetBinContent(ii+1);
	}
	for (unsigned long ii = (slave_stop_bin - slave_start_bin)/slave_average_over_bin - bins_in_pedestal;
			ii < (slave_stop_bin - slave_start_bin)/slave_average_over_bin; ++ii) {
		pedestal[4] += ch5_histo.GetBinContent(ii+1);
		pedestal[5] += ch6_histo.GetBinContent(ii+1);
		pedestal[6] += ch7_histo.GetBinContent(ii+1);
		pedestal[7] += ch8_histo.GetBinContent(ii+1);
	}
	pedestal[0] /= bins_in_pedestal; pedestal[1] /= bins_in_pedestal;
	pedestal[2] /= bins_in_pedestal; pedestal[3] /= bins_in_pedestal;
	pedestal[4] /= bins_in_pedestal; pedestal[5] /= bins_in_pedestal;
	pedestal[6] /= bins_in_pedestal; pedestal[7] /= bins_in_pedestal;
	
	cout << "...Pedestals after: " << pedestal[0] << "," << pedestal[1] << ","
		<< pedestal[2] << "," << pedestal[3] << ","
		<< pedestal[4] << "," << pedestal[5] << ","
		<< pedestal[6] << "," << pedestal[7] << endl;

	long bins_to_correct_ac = (master_stop_bin - master_start_bin)/
		master_average_over_bin - 2.0*bins_in_pedestal;
	if (bins_to_correct_ac <= 0) {
		cout << "...Not enough bins to correct" << endl;
		break;
	}
	// Let's correct the pedestal itself first
	for (unsigned long ii = (master_stop_bin - master_start_bin)/
		master_average_over_bin - bins_in_pedestal;
			ii < (master_stop_bin - master_start_bin)/master_average_over_bin; ++ii) {
		bin_result = ch1_histo.GetBinContent(ii+1);
	        ch1_histo.SetBinContent(ii+1, bin_result - pedestal[0]);
		bin_result = ch2_histo.GetBinContent(ii+1);
		ch2_histo.SetBinContent(ii+1, bin_result - pedestal[1]);
		bin_result = ch3_histo.GetBinContent(ii+1);
		ch3_histo.SetBinContent(ii+1, bin_result - pedestal[2]);
		bin_result = ch4_histo.GetBinContent(ii+1);
		ch4_histo.SetBinContent(ii+1, bin_result - pedestal[3]);
	}

	for (unsigned long ii = (slave_stop_bin - slave_start_bin)/
		slave_average_over_bin - bins_in_pedestal;
			ii < (slave_stop_bin - slave_start_bin)/slave_average_over_bin; ++ii) {
		bin_result = ch5_histo.GetBinContent(ii+1);
		ch5_histo.SetBinContent(ii+1, bin_result - pedestal[4]);
		bin_result = ch6_histo.GetBinContent(ii+1);
		ch6_histo.SetBinContent(ii+1, bin_result - pedestal[5]);
		bin_result = ch7_histo.GetBinContent(ii+1);
		ch7_histo.SetBinContent(ii+1, bin_result - pedestal[6]);
		bin_result = ch8_histo.GetBinContent(ii+1);
		ch8_histo.SetBinContent(ii+1, bin_result - pedestal[7]);
	}


	double one_over_f_correction[8];
	one_over_f_correction [0] = (double) pedestal[0]/bins_to_correct_ac;
	one_over_f_correction [1] = (double) pedestal[1]/bins_to_correct_ac;
	one_over_f_correction [2] = (double) pedestal[2]/bins_to_correct_ac;
	one_over_f_correction [3] = (double) pedestal[3]/bins_to_correct_ac;
	for (unsigned long ii = 0; ii < (unsigned long) bins_to_correct_ac; ++ii) {
		bin_result = ch1_histo.GetBinContent(ii+1+bins_in_pedestal);
		ch1_histo.SetBinContent(ii+1+bins_in_pedestal, 
			bin_result - (double)one_over_f_correction[0]*ii);
		bin_result = ch2_histo.GetBinContent(ii+1+bins_in_pedestal);
		ch2_histo.SetBinContent(ii+1+bins_in_pedestal,
			bin_result - (double)one_over_f_correction[1]*ii);
		bin_result = ch3_histo.GetBinContent(ii+1+bins_in_pedestal);
		ch3_histo.SetBinContent(ii+1+bins_in_pedestal, 
			bin_result - (double)one_over_f_correction[2]*ii);
		bin_result = ch4_histo.GetBinContent(ii+1+bins_in_pedestal);
		ch4_histo.SetBinContent(ii+1+bins_in_pedestal,
			bin_result - (double)one_over_f_correction[3]*ii);
	}
	one_over_f_correction [4] = (double) pedestal[4]/bins_to_correct_ac;
	one_over_f_correction [5] = (double) pedestal[5]/bins_to_correct_ac;
	one_over_f_correction [6] = (double) pedestal[6]/bins_to_correct_ac;
	one_over_f_correction [7] = (double) pedestal[7]/bins_to_correct_ac;
	for (unsigned long ii = 0; ii < (unsigned long) bins_to_correct_ac; ++ii) {
		bin_result = ch5_histo.GetBinContent(ii+1+bins_in_pedestal);
		ch5_histo.SetBinContent(ii+1+bins_in_pedestal,
			bin_result - (double)one_over_f_correction[4]*ii);
		bin_result = ch6_histo.GetBinContent(ii+1+bins_in_pedestal);
		ch6_histo.SetBinContent(ii+1+bins_in_pedestal,
			bin_result - (double)one_over_f_correction[5]*ii);
		bin_result = ch7_histo.GetBinContent(ii+1+bins_in_pedestal);
		ch7_histo.SetBinContent(ii+1+bins_in_pedestal,
			bin_result - (double)one_over_f_correction[6]*ii);
		bin_result = ch8_histo.GetBinContent(ii+1+bins_in_pedestal);
		ch8_histo.SetBinContent(ii+1+bins_in_pedestal,
			bin_result - (double)one_over_f_correction[7]*ii);
	}
    } // If one wants to correct tail of AC coupling
  } // AC coupling correction

  double channels_offsets_to_show[8];
  channels_offsets_to_show [0] =  0.02;
  channels_offsets_to_show [1] =  0.00;
  channels_offsets_to_show [2] = -0.02;
  channels_offsets_to_show [3] = -0.04;
  channels_offsets_to_show [4] = -0.06;
  channels_offsets_to_show [5] =  -0.08;
  channels_offsets_to_show [6] = -0.10;
  channels_offsets_to_show [7] = -0.12;

  for (unsigned long ii = 0; ii < (master_stop_bin - master_start_bin)/master_average_over_bin; ++ii) {
	bin_result = ch1_histo.GetBinContent(ii+1);
	ch1_histo.SetBinContent(ii+1, (bin_result + channels_offsets_to_show[0])/integrate_over_bin);
	bin_result = ch2_histo.GetBinContent(ii+1);
	ch2_histo.SetBinContent(ii+1, (bin_result + channels_offsets_to_show[1])/integrate_over_bin);
	bin_result = ch3_histo.GetBinContent(ii+1);
	ch3_histo.SetBinContent(ii+1, (bin_result + channels_offsets_to_show[2])/integrate_over_bin);
	bin_result = ch4_histo.GetBinContent(ii+1);
	ch4_histo.SetBinContent(ii+1, (bin_result + channels_offsets_to_show[3])/integrate_over_bin);
  }

  for (unsigned long ii = 0; ii < (slave_stop_bin - slave_start_bin)/slave_average_over_bin; ++ii) {
  	bin_result = ch5_histo.GetBinContent(ii+1);
	ch5_histo.SetBinContent(ii+1, (bin_result + channels_offsets_to_show[4])/integrate_over_bin);
	bin_result = ch6_histo.GetBinContent(ii+1);
	ch6_histo.SetBinContent(ii+1, (bin_result + channels_offsets_to_show[5])/integrate_over_bin);
	bin_result = ch7_histo.GetBinContent(ii+1);
	ch7_histo.SetBinContent(ii+1, (bin_result + channels_offsets_to_show[6])/integrate_over_bin);
	bin_result = ch8_histo.GetBinContent(ii+1);
	ch8_histo.SetBinContent(ii+1, (bin_result + channels_offsets_to_show[7])/integrate_over_bin);
  }

  ch1_histo.SetLineColor(kBlack);
  ch1_histo.GetYaxis()->SetRangeUser(-0.15, +0.15);
  ch1_histo.SetLineWidth(1);

  ch2_histo.SetLineColor(kRed);
  ch2_histo.GetYaxis()->SetRangeUser(-0.15, +0.15);
  ch2_histo.SetLineWidth(1);

  ch3_histo.SetLineColor(kBlue);
  ch3_histo.GetYaxis()->SetRangeUser(-0.15, +0.15);
  ch3_histo.SetLineWidth(1);

  ch4_histo.SetLineColor(kMagenta);
  ch4_histo.GetYaxis()->SetRangeUser(-0.15, +0.15);
  ch4_histo.GetYaxis()->SetTitle("DFE Output, Volts");
  ch4_histo.GetXaxis()->SetTitle("Time, usec");
  ch4_histo.SetLineWidth(1);

  ch5_histo.SetLineColor(kBlack);
  ch5_histo.GetYaxis()->SetRangeUser(-0.15, +0.15);
  ch5_histo.SetLineWidth(1);

  ch6_histo.SetLineColor(kRed);
  ch6_histo.GetYaxis()->SetRangeUser(-0.15, +0.15);
  ch6_histo.SetLineWidth(1);

  ch7_histo.SetLineColor(kBlue);
  ch7_histo.GetYaxis()->SetRangeUser(-0.15, +0.15);
  ch7_histo.SetLineWidth(1);

  ch8_histo.SetLineColor(kMagenta);
  ch8_histo.GetYaxis()->SetRangeUser(-0.15, +0.15);
  ch8_histo.SetLineWidth(1);


  ch4_histo.Draw();
  ch1_histo.Draw("SAME");
  ch3_histo.Draw("SAME");
  ch2_histo.Draw("SAME");
  ch5_histo.Draw("SAME");
  ch6_histo.Draw("SAME");
  ch7_histo.Draw("SAME");
  ch8_histo.Draw("SAME");

  // Let's calculate the currents
  canv2.cd();
  TH1D ch1_current("ch1_current", "Current of Channel 1", 
  	(master_stop_bin - master_start_bin)/master_average_over_bin,
	( -bins_in_pedestal*master_average_over_bin)* master_hor_interval * 1.0E+6,
	(master_stop_bin - master_start_bin - bins_in_pedestal*master_average_over_bin)* 
		master_hor_interval * 1.0E+6);
  ch1_current.SetLineColor(kBlack);
  ch1_current.SetLineWidth(1);

  TH1D ch2_current("ch2_current", "Current of Channel 2", 
  	(master_stop_bin - master_start_bin)/master_average_over_bin,
  	( -bins_in_pedestal*master_average_over_bin)* master_hor_interval * 1.0E+6,
	(master_stop_bin - master_start_bin - bins_in_pedestal*master_average_over_bin)* 
		master_hor_interval * 1.0E+6);
  ch2_current.SetLineColor(kRed);
  ch2_current.SetLineWidth(1);

  TH1D ch3_current("ch3_current", "Current of Channel 3", 
  	(master_stop_bin - master_start_bin)/master_average_over_bin,
	( -bins_in_pedestal*master_average_over_bin)* master_hor_interval * 1.0E+6,
	(master_stop_bin - master_start_bin - bins_in_pedestal*master_average_over_bin)* 
		master_hor_interval * 1.0E+6);
  ch3_current.SetLineColor(kBlue);
  ch3_current.SetLineWidth(1);

  TH1D ch4_current("ch4_current", "Current of Channel 4", 
  	(master_stop_bin - master_start_bin)/master_average_over_bin,
	( -bins_in_pedestal*master_average_over_bin)* master_hor_interval * 1.0E+6,
	(master_stop_bin - master_start_bin - bins_in_pedestal*master_average_over_bin)* 
		master_hor_interval * 1.0E+6);
  ch4_current.SetLineColor(kMagenta);
  ch4_current.SetLineWidth(1);

  TH1D ch5_current("ch5_current", "Current of Channel 5",
	(slave_stop_bin - slave_start_bin)/slave_average_over_bin,
	( -bins_in_pedestal*slave_average_over_bin)* slave_hor_interval * 1.0E+6,
	(slave_stop_bin - slave_start_bin - bins_in_pedestal*slave_average_over_bin)*
		slave_hor_interval * 1.0E+6);
  ch5_current.SetLineColor(kBlack);
  ch5_current.SetLineWidth(1);
  TH1D ch6_current("ch6_current", "Current of Channel 6",
	(slave_stop_bin - slave_start_bin)/slave_average_over_bin,
	( -bins_in_pedestal*slave_average_over_bin)* slave_hor_interval * 1.0E+6,
	(slave_stop_bin - slave_start_bin - bins_in_pedestal*slave_average_over_bin)*
		slave_hor_interval * 1.0E+6);
  ch6_current.SetLineColor(kRed);
  ch6_current.SetLineWidth(1);
  TH1D ch7_current("ch7_current", "Current of Channel 7",
	(slave_stop_bin - slave_start_bin)/slave_average_over_bin,
	( -bins_in_pedestal*slave_average_over_bin)* slave_hor_interval * 1.0E+6,
	(slave_stop_bin - slave_start_bin - bins_in_pedestal*slave_average_over_bin)*
		slave_hor_interval * 1.0E+6);
  ch7_current.SetLineColor(kBlue);
  ch7_current.SetLineWidth(1);
  TH1D ch8_current("ch8_current", "Current of Channel 8",
	(slave_stop_bin - slave_start_bin)/slave_average_over_bin,
	( -bins_in_pedestal*slave_average_over_bin)* slave_hor_interval * 1.0E+6,
	(slave_stop_bin - slave_start_bin - bins_in_pedestal*slave_average_over_bin)*
		slave_hor_interval * 1.0E+6);
  ch8_current.SetLineColor(kMagenta);
  ch8_current.SetLineWidth(1);

  TH1D all_current("all_current", "Current Induced on Wire", 
  	(master_stop_bin - master_start_bin)/master_average_over_bin,
  	( -bins_in_pedestal*master_average_over_bin)* master_hor_interval * 1.0E+6,
	(master_stop_bin - master_start_bin - bins_in_pedestal*master_average_over_bin)* 
		master_hor_interval * 1.0E+6);
  all_current.SetLineColor(kGreen);
  all_current.SetLineWidth(1);
	
  double resistor_values [8];
  double time_const_values [8];

  resistor_values[0] = 50.0;
  resistor_values[1] = 50.0;
  resistor_values[2] = 50.0;
  resistor_values[3] = 50.0;
  resistor_values[4] = 50.0;
  resistor_values[5] = 50.0;
  resistor_values[6] = 50.0;
  resistor_values[7] = 50.0;

  time_const_values[0] = resistor_values[0] * 0.6 / (1.0E+6*master_hor_interval * master_average_over_bin);
  time_const_values[1] = resistor_values[1] * 0.6 / (1.0E+6*master_hor_interval * master_average_over_bin);
  time_const_values[2] = resistor_values[2] * 0.6 / (1.0E+6*master_hor_interval * master_average_over_bin);
  time_const_values[3] = resistor_values[3] * 0.6 / (1.0E+6*master_hor_interval * master_average_over_bin);
  time_const_values[4] = resistor_values[4] * 0.6 / (1.0E+6*slave_hor_interval * slave_average_over_bin);
  time_const_values[5] = resistor_values[5] * 0.6 / (1.0E+6*slave_hor_interval * slave_average_over_bin);
  time_const_values[6] = resistor_values[6] * 0.6 / (1.0E+6*slave_hor_interval * slave_average_over_bin);
  time_const_values[7] = resistor_values[7] * 0.6 / (1.0E+6*slave_hor_interval * slave_average_over_bin);


  double next_bin_result = 0.0;
  double current_value = 0.0;
  double total_current = 0.0;
  for (unsigned long ii = 0; 
  		ii < (master_stop_bin - master_start_bin)/master_average_over_bin - 1; ++ii) {
	total_current = 0.0;

	bin_result =(ch1_histo.GetBinContent(ii+1) - channels_offsets_to_show[0])/integrate_over_bin;
	next_bin_result=(ch1_histo.GetBinContent(ii+2) - channels_offsets_to_show[0])/integrate_over_bin;
	current_value = (0.5*(bin_result + next_bin_result) + time_const_values[0] *
		(next_bin_result - bin_result))/resistor_values[0];
	ch1_current.SetBinContent(ii+1, 1.0E+3*current_value/10.911 -  0.8 );
	total_current += TMath::Abs(current_value);
	// 10.933 actually, not 10.911 is the whole amplification of opamps.

	bin_result =(ch2_histo.GetBinContent(ii+1) - channels_offsets_to_show[1])/integrate_over_bin;
	next_bin_result=(ch2_histo.GetBinContent(ii+2) - channels_offsets_to_show[1])/integrate_over_bin;
	current_value = (0.5*(bin_result + next_bin_result) + time_const_values[1] *
		(next_bin_result - bin_result))/resistor_values[1];
	ch2_current.SetBinContent(ii+1, 1.0E+3*current_value/10.911 -  1.0);
	total_current += TMath::Abs(current_value);
	
	bin_result =(ch3_histo.GetBinContent(ii+1) - channels_offsets_to_show[2])/integrate_over_bin;
	next_bin_result=(ch3_histo.GetBinContent(ii+2) - channels_offsets_to_show[2])/integrate_over_bin;
	current_value = (0.5*(bin_result + next_bin_result) + time_const_values[2] *
		(next_bin_result - bin_result))/resistor_values[2];
	ch3_current.SetBinContent(ii+1, 1.0E+3*current_value/10.911 -  1.2);
	total_current += TMath::Abs(current_value);

	bin_result =(ch4_histo.GetBinContent(ii+1) - channels_offsets_to_show[3])/integrate_over_bin;
	next_bin_result=(ch4_histo.GetBinContent(ii+2) - channels_offsets_to_show[3])/integrate_over_bin;
	current_value = (0.5*(bin_result + next_bin_result) + time_const_values[3] *
		(next_bin_result - bin_result))/resistor_values[3];
	ch4_current.SetBinContent(ii+1, 1.0E+3*current_value/10.911 - 1.4);
	total_current += TMath::Abs(current_value);
	//
	bin_result =(ch5_histo.GetBinContent(ii+1) - channels_offsets_to_show[4])/integrate_over_bin;
	next_bin_result=(ch5_histo.GetBinContent(ii+2) - channels_offsets_to_show[4])/integrate_over_bin;
	current_value = (0.5*(bin_result + next_bin_result) + time_const_values[4] *
		(next_bin_result - bin_result))/resistor_values[4];
	ch5_current.SetBinContent(ii+1, 1.0E+3*current_value/10.911 - 1.6);
	total_current += TMath::Abs(current_value);
	
	bin_result =(ch6_histo.GetBinContent(ii+1) - channels_offsets_to_show[5])/integrate_over_bin;
	next_bin_result=(ch6_histo.GetBinContent(ii+2) - channels_offsets_to_show[5])/integrate_over_bin;
	current_value = (0.5*(bin_result + next_bin_result) + time_const_values[5] *
		(next_bin_result - bin_result))/resistor_values[5];
	ch6_current.SetBinContent(ii+1, 1.0E+3*current_value/10.911 - 1.8);
	total_current += TMath::Abs(current_value);
	
	bin_result =(ch7_histo.GetBinContent(ii+1) - channels_offsets_to_show[6])/integrate_over_bin;
	next_bin_result=(ch7_histo.GetBinContent(ii+2) - channels_offsets_to_show[6])/integrate_over_bin;
	current_value = (0.5*(bin_result + next_bin_result) + time_const_values[6] *
		(next_bin_result - bin_result))/resistor_values[6];
	ch7_current.SetBinContent(ii+1, 1.0E+3*current_value/10.911 - 2.0);
	total_current += TMath::Abs(current_value);
	
	bin_result =(ch8_histo.GetBinContent(ii+1) - channels_offsets_to_show[7])/integrate_over_bin;
	next_bin_result=(ch8_histo.GetBinContent(ii+2) - channels_offsets_to_show[7])/integrate_over_bin;
	current_value = (0.5*(bin_result + next_bin_result) + time_const_values[7] *
		(next_bin_result - bin_result))/resistor_values[7];
	ch8_current.SetBinContent(ii+1, 1.0E+3*current_value/10.911 - 2.2);
	total_current += TMath::Abs(current_value);


	all_current.SetBinContent(ii+1, 1.0E+3*total_current/10.911);

  }
 
  ch1_current.GetYaxis()->SetRangeUser(-1.5, +1.5);
  ch2_current.GetYaxis()->SetRangeUser(-1.5, +1.5);
  ch3_current.GetYaxis()->SetRangeUser(-1.5, +1.5);
  ch4_current.GetYaxis()->SetRangeUser(-1.5, +1.5);
  ch5_current.GetYaxis()->SetRangeUser(-1.5, +1.5);
  ch6_current.GetYaxis()->SetRangeUser(-1.5, +1.5);
  ch7_current.GetYaxis()->SetRangeUser(-1.5, +1.5);
  ch8_current.GetYaxis()->SetRangeUser(-1.5, +1.5);

  all_current.GetYaxis()->SetRangeUser(-2.0, +1.5);
  all_current.GetYaxis()->SetTitle("Induced Current, nA");
  all_current.GetXaxis()->SetTitle("Time, usec");

  all_current.Draw("");

  ch4_current.Draw("SAME");
  ch3_current.Draw("SAME");
  ch2_current.Draw("SAME");
  ch1_current.Draw("SAME");
  ch5_current.Draw("SAME");
  ch6_current.Draw("SAME");
  ch7_current.Draw("SAME");
  ch8_current.Draw("SAME");

  theApp.Run();
  return 0;
}

//-------------------------------------------------------------------//
