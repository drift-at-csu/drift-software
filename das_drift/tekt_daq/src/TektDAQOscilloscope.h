#ifndef _TektDAQOscilloscope_h_
#define _TektDAQOscilloscope_h_

#include <string>
#include <vector>
// using namespace std;

#include "vxi11_user.h"


namespace TektDAQ {
class Oscilloscope {
  public:
  	Oscilloscope();
	~Oscilloscope();

	int Connect();
	int Disconnect();
	int isConnected();

	int InitializeChannels(
		double chan_one_range = 0.050, 
		double chan_two_range = 0.050,
	        double chan_three_range = 0.050, 
		double chan_four_range = 0.050);
	int InitializeTrigger();
	int InitializeAquisition(
		unsigned long nof_samples = 1000000, 
		double full_hor_range = 1.0);

	int RunTrigger(bool is_master);
	int Aquire();

	int Save(std::string aFilename);
	int Load(std::string aFilename);

	short *GetTracePointer(unsigned char aChannelNumber);
	unsigned long GetNofTimebins(unsigned char aChannelNumber);
	unsigned char GetNofBytesPerTimebin();
	double GetHorInterval();
	double GetVertGain();

	std::string fIpaddr;

  private:
  	void CleanupMemory();
	int AllocateMemoryFor(unsigned long aNumberOfPoints);


  	CLINK *fLink;
	short *fCH1trace;
	short *fCH2trace;
	short *fCH3trace;
	short *fCH4trace;
	unsigned long fNumberOfPoints; // Number of points in each trace
	double fVGain; // Vertical gain in Volts
	double fHinterval; // Width of 1 bin in sec

};
} /* namespace TektDAQ */
#endif /* _TektDAQOscilloscope_h_ */
