#ifndef _TektDAQ_PLD_h_
#define _TektDAQ_PLD_h_

#include <string>
#include <vector>
#include <map>

#include <DAS_DRIFT_Trigger.h>
#include <DAS_DRIFT_TimeStamp.h>
#include <DAS_DRIFT_Trace.h>
#include <DAS_DRIFT_Threshold.h>

namespace TektDAQ {
class PLD {
  public:
  	PLD();
	~PLD();

	void ProcessRawBuffer(short *buffer, 
		unsigned long nof_timebins,
		double aVertGain,   // in mV
		double aHorizScale, // in nsec
		unsigned long aChannelNumber);
	

	void ClearTriggers();
	void ClearTraces();
	void ClearThresholds();

	unsigned long GetNofTriggers();
	unsigned long GetNofTraces();
	unsigned long GetNofChannels();

	int isThereChannel(unsigned long aChannelNumber);
	int AddChannel(unsigned long aChannelNumber);

	das_drift::Trigger *GetTrigger(unsigned long aTrigNumber);
	das_drift::Trace *GetTrace(unsigned long aTraceNumber);
	das_drift::Threshold *GetThreshold(unsigned long aChannelNumber);

	void SortTriggersByTime();

  private:
	std::vector <das_drift::Trigger> fTriggers;
	std::vector <das_drift::Trace> fTraces;
	std::vector <das_drift::Threshold> fThresholds;	
};
} /* namespace TektDAQ */
#endif /* _TektDAQ_PLD_h_ */
