#include "TektDAQ_PLD.h"
using namespace TektDAQ;

#include <stdio.h>
#include <iostream>
#include <sstream>
#include <fstream>
using namespace std;

//-------------------------------------------------------------------//
PLD::PLD(){
}
//-------------------------------------------------------------------//
PLD::~PLD(){
  fTriggers.clear();
  fTraces.clear();
  fThresholds.clear();

}
//-------------------------------------------------------------------//
void PLD::ProcessRawBuffer(short *buffer,
		unsigned long nof_timebins,
		double aVertGain,   // in mV
		double aHorizScale, // in nsec
		unsigned long aChannelNumber){
  cout << "...PLD::ProcessRawBuffer: Processing channel " << aChannelNumber 
  	<< "... with horiz gain" << aHorizScale << "and vert gain " 
	<< aVertGain <<  endl;

  das_drift::Threshold * threshold_pnt = GetThreshold(aChannelNumber);
  if (threshold_pnt == NULL) {
  	cout << "   No such channel: " << aChannelNumber << endl;
	return;
  }

  int64_t cur_pedestal = 0; // The current value of the pedestal
  int64_t mid_pedestal = 0;
  int64_t prev_pedestal = 0; // The previous value of the pedestal
  unsigned long ped_bins_counter = 0;

  int64_t cur_trigger = 0; // The value of the current integrated value
  int64_t prev_trigger = 0; // The value of the prev integrated value

  int trigger_happened = 0; // This is an indicator that trigger is happening
  short pos_trigger_happened = 0; 
  short neg_trigger_happened = 0;
  short first_trigger_happened = 0; // This is an indicator that first trigger happened
  int stop_baseline_calc = 0; // This is indicator to stop baseline calculation
  int64_t after_trigger_counter = 0; // This counts how many bins were there after trigger
  int64_t nof_bins_after_first_trigger = 0;
  short last_was_neg_or_pos = 0; // If this is 1 than last trig was pos if 2 then neg

  // Let's create a temporary trigger structure
  das_drift::Trigger tmp_trig;
  tmp_trig.fChannelNumber = aChannelNumber;
  // tmp_trig.fTriggerTime.SetGPSTime
  //	(aStartBin/1000000, 1000 * (aStartBin%1000000));
  tmp_trig.fTriggerTime.SetGPSTime (0, 0);
  tmp_trig.fTriggerType = 0x0;
  tmp_trig.fPositiveSignal = 0;
  tmp_trig.fNegativeSignal = 0;
  tmp_trig.fTotalTimeBinsAbove = 0;
  tmp_trig.fTotalTimeBinsBelow = 0;
  tmp_trig.fTotalTimeBins = 0;
  tmp_trig.fNumberOfZeroCrossing = 0;

  for (unsigned long ii = 0; ii < nof_timebins; ii++) {
	// Let's reserve first fPedestalIntegTime for pedestal and pre-triggering
	// calculations...
	if ( (ii % threshold_pnt->fTriggerIntegTime)== 0 ) {
		prev_trigger = cur_trigger;
		cur_trigger = buffer[ii];
	} else {
		cur_trigger += buffer[ii];
	}

	// Pedestal Integration
	if (ii < threshold_pnt->fPedestalIntegTime) {
		// we just want to start pedestal calculation
		// after integration number of bins
		cur_pedestal += buffer[ii];
		mid_pedestal = cur_pedestal;
		continue;
	}
	if ( (ii % threshold_pnt->fPedestalIntegTime)== 0 ) {
		if (stop_baseline_calc == 1) {
			// We had a trigger on prev bin, so we
			// do not assign a prev_pedestal value
			stop_baseline_calc = 0;
			// cout << "...Stoping Baseline calc for bin " << ii << endl;
		} else {
			/*
			cout << "...Pedestal Updated : " 
				<< prev_pedestal << "," 
				<< mid_pedestal << ","
				<< cur_pedestal << endl;
			*/
			prev_pedestal = mid_pedestal;
			if (ped_bins_counter == threshold_pnt->fPedestalIntegTime - 1)
				mid_pedestal = cur_pedestal;
		}
		cur_pedestal = 0;
		ped_bins_counter = 0;

	} else {
		if (trigger_happened == 1) {
			/*
			cout << "...Trigger Happened, Peds: "
				<< prev_pedestal << "," 
				<< mid_pedestal << "," 
				<< cur_pedestal << endl;
			*/
			stop_baseline_calc = 1;
			mid_pedestal = prev_pedestal;
			// Let's revert to prev trig only once
		} else {
			ped_bins_counter++;
			cur_pedestal += buffer[ii];
		}
	}



	// We have a trigger happening, so we want to acknowledge it after
	// some time
	if (trigger_happened == 1) {
		if (after_trigger_counter < threshold_pnt->fAfterTrigger) {
			// We want to kill an infinite trigger:

			after_trigger_counter ++;

			if ((ii - nof_bins_after_first_trigger ) > threshold_pnt->fAfterTrigger) {
				trigger_happened = 0;
				after_trigger_counter = 0;
				nof_bins_after_first_trigger = ii;
			}
		} else {
			trigger_happened = 0;
			after_trigger_counter = 0;
		}
	}

	// We execute the following at each integration time
	if ( (ii % threshold_pnt->fTriggerIntegTime)== 0 ) {
		int64_t raw_trigger = prev_trigger*((int64_t)threshold_pnt->fPedestalIntegTime);
		int64_t raw_pos_thresh = ( prev_pedestal + 
			((int64_t)threshold_pnt->fAboveThreshold)*
			((int64_t)threshold_pnt->fPedestalIntegTime) )*
			((int64_t)threshold_pnt->fTriggerIntegTime);
		int64_t raw_neg_thresh = ( prev_pedestal -
			((int64_t)threshold_pnt->fBelowThreshold)*
			((int64_t)threshold_pnt->fPedestalIntegTime) )*
			((int64_t)threshold_pnt->fTriggerIntegTime);

		if ( raw_trigger >= raw_pos_thresh ) {
			if (trigger_happened == 0) {
				// There were no trigger
				first_trigger_happened = 1;
				tmp_trig.fTriggerType = 0x3;
				nof_bins_after_first_trigger = ii - 1;
				// cout << "===> " << ii << endl;
			}
			/*
			cout << "POS Trigger: " 
				<< " Trig=" <<  prev_trigger
				<< " Peds=" <<  prev_pedestal
				<< " TrigRaw=" << raw_trigger
				<< " ThrsRaw=" << raw_pos_thresh
				<< endl;
			*/
			pos_trigger_happened = 1;
			after_trigger_counter = 0;
			trigger_happened = 1;
			// tmp_trig.fPositiveSignal += prev_trigger;
			tmp_trig.fTotalTimeBinsAbove ++;
			tmp_trig.fPositiveSignal +=
				(raw_trigger - raw_pos_thresh + 
				((int64_t)threshold_pnt->fAboveThreshold)*
				((int64_t)threshold_pnt->fPedestalIntegTime)*
				((int64_t)threshold_pnt->fTriggerIntegTime))/
				((int64_t)threshold_pnt->fPedestalIntegTime)/
				((int64_t)threshold_pnt->fTriggerIntegTime);

			tmp_trig.fTotalTimeBins = ii - nof_bins_after_first_trigger;
			if (last_was_neg_or_pos == 2) {
				// last trigger was negative
				tmp_trig.fNumberOfZeroCrossing ++;
				last_was_neg_or_pos = 1;
			}

		} else if ( raw_trigger <= raw_neg_thresh ) {
			if (trigger_happened == 0) {
				// There were no trigger
				first_trigger_happened = 1;
				tmp_trig.fTriggerType = 0x1;
				nof_bins_after_first_trigger = ii-1;
				// cout << "===> " << ii << endl;
			}
			/*
			cout << "NEG Trigger: "
				<< " Trig=" <<  prev_trigger
				<< " Peds=" <<  prev_pedestal
				<< " TrigRaw=" << raw_trigger
				<< " ThrsRaw=" << raw_neg_thresh
				<< endl;
			*/
			neg_trigger_happened = 1;
			trigger_happened = 1;
			after_trigger_counter = 0;
			tmp_trig.fTotalTimeBinsBelow ++;
			tmp_trig.fNegativeSignal +=
				(raw_neg_thresh - raw_trigger +
				((int64_t)threshold_pnt->fBelowThreshold)*
				((int64_t)threshold_pnt->fPedestalIntegTime)*
				((int64_t)threshold_pnt->fTriggerIntegTime))/
				((int64_t)threshold_pnt->fPedestalIntegTime)/
				((int64_t)threshold_pnt->fTriggerIntegTime);

			tmp_trig.fTotalTimeBins = ii - nof_bins_after_first_trigger;

			if (last_was_neg_or_pos == 1) {
				// last trigger was positive
				tmp_trig.fNumberOfZeroCrossing ++;
				last_was_neg_or_pos = 2;
			}
		}

		// Managing trigger happening
		if (first_trigger_happened == 1) {
			// cout << "---This was first Trigger " << endl;
			// nof_bins_after_first_trigger = ii - 1;
			first_trigger_happened = 0;
			tmp_trig.fTriggerTime.SetGPSTime(ii/1000000, 1000 * (ii%1000000));

		}
		if ((trigger_happened == 0)&&(
			(pos_trigger_happened == 1)||(neg_trigger_happened == 1))) {
			// We're at the end of the event
			// cout << "===> End At " << ii << endl;
			pos_trigger_happened = 0;
			neg_trigger_happened = 0;


			fTriggers.push_back(tmp_trig);
			// Zeroing the TMP trigger again
			tmp_trig.fTriggerTime.SetGPSTime (0, 0);
			tmp_trig.fTriggerType = 0x0;
			tmp_trig.fPositiveSignal = 0;
			tmp_trig.fNegativeSignal = 0;
			tmp_trig.fTotalTimeBinsAbove = 0;
			tmp_trig.fTotalTimeBinsBelow = 0;
			tmp_trig.fTotalTimeBins = 0;
			tmp_trig.fNumberOfZeroCrossing = 0;

			nof_bins_after_first_trigger = 0;
			last_was_neg_or_pos = 0;
		}

	} // At trigger integration time	
  }
}
//-------------------------------------------------------------------//
void PLD::ClearTriggers(){
  fTriggers.clear();
}
//-------------------------------------------------------------------//
void PLD::ClearTraces(){
  fTraces.clear();
}
//-------------------------------------------------------------------//
void PLD::ClearThresholds(){
  fThresholds.clear();
}
//-------------------------------------------------------------------//
unsigned long PLD::GetNofTriggers(){
  return fTriggers.size();
}
//-------------------------------------------------------------------//
unsigned long PLD::GetNofTraces(){
  return fTraces.size();
}
//-------------------------------------------------------------------//
unsigned long PLD::GetNofChannels(){
  return fThresholds.size();
}
//-------------------------------------------------------------------//
int PLD::isThereChannel(unsigned long aChannelNumber){
  int result = 0;
  for (unsigned long ii = 0; ii < GetNofChannels(); ++ii) {
	if (aChannelNumber == fThresholds[ii].fChannelID) {
		result = 1;
		break;
	}
  }
  return result;
}
//-------------------------------------------------------------------//
int PLD::AddChannel(unsigned long aChannelNumber){
  // Check if the channel with this ID is already there
  if (isThereChannel(aChannelNumber)){
	return 0;
  }
  fThresholds.push_back(das_drift::Threshold());
  fThresholds[fThresholds.size() - 1].fChannelID = aChannelNumber;
  return 1;
}
//-------------------------------------------------------------------//
das_drift::Trigger *PLD::GetTrigger(unsigned long aTrigNumber){
  if (aTrigNumber >= fTriggers.size())  
  	return NULL;
  
  return &(fTriggers[aTrigNumber]);
}
//-------------------------------------------------------------------//
das_drift::Trace *PLD::GetTrace(unsigned long aTraceNumber){
  if (aTraceNumber >= fTraces.size())
  	return NULL;

  return &(fTraces[aTraceNumber]);
}
//-------------------------------------------------------------------//
das_drift::Threshold *PLD::GetThreshold(unsigned long aChannelNumber){
  das_drift::Threshold *result = NULL;
  for (unsigned long ii = 0; ii < GetNofChannels(); ++ii) {
	if (aChannelNumber == fThresholds[ii].fChannelID) {
		result = &fThresholds[ii];
		break;
  	}
  }
  return result;
}
//-------------------------------------------------------------------//
