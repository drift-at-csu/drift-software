#include "TektDAQOscilloscope.h"
using namespace TektDAQ;

#include <stdio.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <unistd.h>
using namespace std;

//-------------------------------------------------------------------//
Oscilloscope::Oscilloscope():
		fIpaddr(""),
		fLink(NULL),
		fCH1trace(NULL), fCH2trace(NULL), fCH3trace(NULL), fCH4trace(NULL),
		fNumberOfPoints(0), fVGain(0.0), fHinterval(0.0){
}
//-------------------------------------------------------------------//
Oscilloscope::~Oscilloscope(){
  Disconnect();
  CleanupMemory();
}
//-------------------------------------------------------------------//
int Oscilloscope::Connect(){
  if (isConnected()) {
  	cout << "...Oscilloscope::Connect: Already connected" << endl;
	return 0;
  }
  fLink = new CLINK;
  vxi11_open_device(fIpaddr.c_str(), fLink);

  return 1;
}
//-------------------------------------------------------------------//
int Oscilloscope::Disconnect(){
  if (isConnected()) {
	vxi11_close_device(fIpaddr.c_str(), fLink);
	delete fLink; 
	fLink = NULL;
	cout << "...Oscilloscope::Disconnect: disconnected from " << fIpaddr << endl;
  	return 1;
  } else {
	cout << "...Oscilloscope::Disconnect: already disconnected" << endl;
	return 1;
  }	
}
//-------------------------------------------------------------------//
int Oscilloscope::isConnected(){
  if (fLink != NULL) {
  	return 1;
  } else {
  	return 0;
  }
}
//-------------------------------------------------------------------//
int Oscilloscope::InitializeChannels(double chan_one_range, double chan_two_range, 
	double chan_three_range, double chan_four_range){
  if (!isConnected()){
	cout << "...Need to be connected to Initialize channels" << endl;
	return 0;
  }
  stringstream tmp_str_stream;
  /* Let's set bandwdth to full */
  vxi11_send(fLink, "CH1:BAN FUL"); vxi11_send(fLink, "CH2:BAN FUL");
  vxi11_send(fLink, "CH3:BAN FUL"); vxi11_send(fLink, "CH4:BAN FUL");
  /* Setting coupling to DC */
  // vxi11_send(fLink, "CH1:COUP DC"); vxi11_send(fLink, "CH2:COUP DC");
  // vxi11_send(fLink, "CH3:COUP DC"); vxi11_send(fLink, "CH4:COUP DC");
  /* Setting offset to 1.000 Volts */
  // vxi11_send(fLink, "CH1:OFFS 1.000"); vxi11_send(fLink, "CH2:OFFS 1.000");
  // vxi11_send(fLink, "CH3:OFFS 1.000"); vxi11_send(fLink, "CH4:OFFS 1.000");
  /* Setting position to 0.0, hopefully means that all the traces will be in the middle */
  vxi11_send(fLink, "CH1:POS 0.0"); vxi11_send(fLink, "CH2:POS 0.0");
  vxi11_send(fLink, "CH3:POS 0.0"); vxi11_send(fLink, "CH4:POS 0.0");
  /* Setting the scale to 50mV will give us realystic dynamic range for HiGain */
  // vxi11_send(fLink, "CH1:SCA 0.050");
  tmp_str_stream.str("");
  tmp_str_stream << "CH1:SCA " << chan_one_range;
  vxi11_send(fLink, tmp_str_stream.str().c_str());
  tmp_str_stream.str("");
  tmp_str_stream << "CH2:SCA " << chan_two_range;
  vxi11_send(fLink, tmp_str_stream.str().c_str());
  tmp_str_stream.str("");
  tmp_str_stream << "CH3:SCA " << chan_three_range;
  vxi11_send(fLink, tmp_str_stream.str().c_str());
  tmp_str_stream.str("");
  tmp_str_stream << "CH4:SCA " << chan_four_range;
  vxi11_send(fLink, tmp_str_stream.str().c_str());
  
  return 1;
}
//-------------------------------------------------------------------//
int Oscilloscope::InitializeTrigger(){
  if (!isConnected()){
	cout << "...Need to be connected to Initialize trigger" << endl;
	return 0;
  }
  /* Setting trigger mode to normal (aquire only at trigger) */
  vxi11_send(fLink, "TRIG:A:MOD NORM");
  /* Let's set trigger for edge now */
  vxi11_send(fLink, "TRIG:A:TYP EDG");
  /* Let's specify simple threshold trigger */
  vxi11_send(fLink, "TRIG:A:EDGE:COUP DC");
  /* Let's trigger on the rising edge */
  vxi11_send(fLink, "TRIG:A:EDGE:SLO FALL");
  /* Let's trigger on Channel 1 */
  vxi11_send(fLink, "TRIG:A:EDGE:SOU EXT");

  /* Let's set Triggers for all channels low enough, so that they will not trigger */
  vxi11_send(fLink, "TRIG:A:LOW:CH1 0.00"); vxi11_send(fLink, "TRIG:A:LOW:CH2 0.00");
  vxi11_send(fLink, "TRIG:A:LOW:CH4 0.00"); vxi11_send(fLink, "TRIG:A:LOW:CH4 0.00");

  return 1;
}
//-------------------------------------------------------------------//
int Oscilloscope::InitializeAquisition(unsigned long nof_samples, double full_hor_range){
  stringstream tmp_str_stream;

  if (!isConnected()){
	cout << "...Need to be connected to Initialize aquisition" << endl;
	return 0; 
  }
  /* We will use hires mode here */
  vxi11_send(fLink, "ACQ:MOD HIR");
  /* We want to stop after each aquisition in order to completely download the waveforms */
  vxi11_send(fLink, "ACQ:STOPA SEQ");
  /* We don't want any headers */
  vxi11_send(fLink, "HEAD OFF");
  /* Let's grub 20x10^6 points */
  // vxi11_send(fLink, "HOR:RECO 20.0E+6");
  tmp_str_stream.str("");
  tmp_str_stream << "HOR:RECO " << nof_samples;
  vxi11_send(fLink, tmp_str_stream.str().c_str());
  /* Let's set the full time scale (per division) in sec */
  tmp_str_stream.str("");
  tmp_str_stream << "HOR:SCA " << full_hor_range/10.0;
  vxi11_send(fLink, tmp_str_stream.str().c_str());
  /* with roughly 20 events per second from the source, we're expecting
   * 4 of them to be in our sample range */
  /* 2 bytes per data point (16 bit) */
  vxi11_send(fLink, "DAT:WID 2");
  /* little endian, signed */
  vxi11_send(fLink, "DAT:ENC SRI");

  return 1;
}
//-------------------------------------------------------------------//
int Oscilloscope::RunTrigger(bool is_master) {
  if (!isConnected()){
	cout << "...Need to be connected to Run Trigger" << endl;
	return 0;
  }
  long  opc_value = 0;

  vxi11_send(fLink, "ACQ:STOPA SEQ");
  vxi11_send(fLink, "ACQ:STATE 1");

  if (is_master) {
	sleep(5);
	vxi11_send(fLink, "TRIG FORCE");
	sleep(5); // this is in order to get after the trigger filled
	opc_value = vxi11_obtain_long_value(fLink, "*OPC?", 10000);
	if (opc_value != 1) {
		cout << "...Oscilloscope::Aquire: Didn't make it" << endl;
		return 0;
	}
  }
  return 1;
}
//-------------------------------------------------------------------//
int Oscilloscope::Aquire(){
  if (!isConnected()){
	cout << "...Need to be connected to Aquire" << endl;
	return 0;
  }
  // double yoff, yzero, voffset, xzero;
 
  // yoff = vxi11_obtain_double_value(fLink, "WFMO:YOF?");
  // yzero = vxi11_obtain_double_value(fLink, "WFMO:YZE?");
  fVGain = vxi11_obtain_double_value(fLink, "WFMO:YMU?");
  // voffset = -(yoff * vgain) + yzero;
  fHinterval = vxi11_obtain_double_value(fLink, "WFMO:XIN?");
  // xzero = vxi11_obtain_double_value(fLink, "WFMO:XZE?");

  cout << "   Vertical gain: " << fVGain << endl;
  // cout << "   Vertical offset: " << -voffset << endl;
  cout << "   Horizontal interval : " << fHinterval << endl;

  unsigned long nof_points = 0;
  nof_points = vxi11_obtain_long_value(fLink, "HOR:RECO?", 10000);

  if (fNumberOfPoints != nof_points) {
  	cout << "...Reallocating memory" << endl;
	CleanupMemory();
	if ( !AllocateMemoryFor(nof_points)) {
		cout << "...Allocating memory failed, Break fro Aquire" << endl;
		return 0;
	}
  }

  unsigned long nof_time_bins_in_buf = 100000;
  short buf[nof_time_bins_in_buf];
  unsigned long buf_len = nof_time_bins_in_buf*2;
  stringstream buffer_start_string;
  stringstream buffer_stop_string;
  unsigned long bytes_returned = 0;
  unsigned long ii;


  // Let's grub our 10^7 points here
  cout << "...Grabbing " << nof_points << " x 4 datapoints";
  for (ii = 0; ii < nof_points/nof_time_bins_in_buf; ii++){
  	buffer_start_string.str("");
	buffer_start_string << "DAT:STAR " << (ii * nof_time_bins_in_buf) + 1;
	buffer_stop_string.str("");
	buffer_stop_string << "DAT:STOP " << ((ii+1) * nof_time_bins_in_buf);
	vxi11_send(fLink, buffer_start_string.str().c_str());
	vxi11_send(fLink, buffer_stop_string.str().c_str());

	// For Channel 1
	vxi11_send(fLink, "DAT:SOU CH1");
	vxi11_send(fLink, "CURVE?");
	bytes_returned +=vxi11_receive_data_block(fLink, (char *)buf, buf_len, 10000);
	std::copy (buf, buf+nof_time_bins_in_buf, &fCH1trace[ii*nof_time_bins_in_buf] );
	// For Channel 2
	vxi11_send(fLink, "DAT:SOU CH2");
	vxi11_send(fLink, "CURVE?");
	bytes_returned +=vxi11_receive_data_block(fLink, (char *)buf, buf_len, 10000);
	std::copy (buf, buf+nof_time_bins_in_buf, &fCH2trace[ii*nof_time_bins_in_buf] );
	// For Channel 3
	vxi11_send(fLink, "DAT:SOU CH3");
	vxi11_send(fLink, "CURVE?");
	bytes_returned +=vxi11_receive_data_block(fLink, (char *)buf, buf_len, 10000);
	std::copy (buf, buf+nof_time_bins_in_buf, &fCH3trace[ii*nof_time_bins_in_buf] );
	// For Channel 4
	vxi11_send(fLink, "DAT:SOU CH4");
	vxi11_send(fLink, "CURVE?");
	bytes_returned +=vxi11_receive_data_block(fLink, (char *)buf, buf_len, 10000);
	std::copy (buf, buf+nof_time_bins_in_buf, &fCH4trace[ii*nof_time_bins_in_buf] );
	cout << "." << flush;
  }
  cout << endl << "   Returned " << bytes_returned << " bytes" << endl;

  return 1;
}
//-------------------------------------------------------------------//
int Oscilloscope::Save(string aFilename){
  unsigned long num_of_channels = 4;
  cout << "...Saving Waveforms with " << fNumberOfPoints 
  	<<  "points to " << aFilename << endl;

  ofstream file;
  file.open(aFilename.c_str(), ios::out | ios::trunc | ios::binary);
  file.write(reinterpret_cast<const char*> (&fNumberOfPoints), sizeof(unsigned long));
  file.write(reinterpret_cast<const char*> (&num_of_channels), sizeof(unsigned long));
  file.write(reinterpret_cast<const char*> (&fVGain), sizeof(double));
  file.write(reinterpret_cast<const char*> (&fHinterval), sizeof(double));
  file.write(reinterpret_cast<const char*> (fCH1trace), sizeof(short) * fNumberOfPoints);
  file.write(reinterpret_cast<const char*> (fCH2trace), sizeof(short) * fNumberOfPoints);
  file.write(reinterpret_cast<const char*> (fCH3trace), sizeof(short) * fNumberOfPoints);
  file.write(reinterpret_cast<const char*> (fCH4trace), sizeof(short) * fNumberOfPoints);
  file.close();
  
  return 1;
}
//-------------------------------------------------------------------//
void Oscilloscope::CleanupMemory(){
  if (fCH1trace != NULL){
	delete [] fCH1trace;
	fCH1trace = NULL;
  }
  if (fCH2trace != NULL){
	delete [] fCH2trace;
	fCH2trace = NULL;
  }
  if (fCH3trace != NULL){
	delete [] fCH3trace;
	fCH3trace = NULL;
  }
  if (fCH4trace != NULL){
	delete [] fCH4trace;
	fCH4trace = NULL;
  }
  fNumberOfPoints = 0;
}
//-------------------------------------------------------------------//
int Oscilloscope::AllocateMemoryFor(unsigned long aNumberOfPoints){
  if ((fCH1trace !=NULL)||(fCH2trace !=NULL)||(fCH3trace !=NULL)||(fCH4trace !=NULL)) {
	cout << "...Cleanup memory first before allocation" << endl;
	return 0;
  }
  fNumberOfPoints = 0;
  fCH1trace = new (nothrow) short [aNumberOfPoints];
  if (fCH1trace == NULL) {
	cout << "...Can't allocate memory for CH1" << endl;
	return 0;
  }
  fCH2trace = new (nothrow) short [aNumberOfPoints];
  if (fCH1trace == NULL) {
	delete [] fCH1trace; fCH1trace = NULL;
	cout << "...Can't allocate memory for CH2" << endl;
	return 0;
  }
  fCH3trace = new (nothrow) short [aNumberOfPoints];
  if (fCH3trace == NULL) {
	delete [] fCH1trace; fCH1trace = NULL;
	delete [] fCH2trace; fCH2trace = NULL;
	cout << "...Can't allocate memory for CH3" << endl;
	return 0;
  }
  fCH4trace = new (nothrow) short [aNumberOfPoints];
  if (fCH4trace == NULL) {
	delete [] fCH1trace; fCH1trace = NULL;
	delete [] fCH2trace; fCH2trace = NULL;
	delete [] fCH3trace; fCH3trace = NULL;
	cout << "...Can't allocate memory for CH4" << endl;
	return 0;
  }
  fNumberOfPoints = aNumberOfPoints;
  return 1;
}
//-------------------------------------------------------------------//
int Oscilloscope::Load(std::string aFilename){
  unsigned long num_of_channels = 0;
  unsigned long num_of_points = 0;
  ifstream file(aFilename.c_str(), ios::binary);

  // Let's read the header
  file.read(reinterpret_cast< char*> (&num_of_points), sizeof(unsigned long));
  if (num_of_points != fNumberOfPoints) {
  	CleanupMemory();
	AllocateMemoryFor(num_of_points);
  }
  file.read(reinterpret_cast< char*> (&num_of_channels), sizeof(unsigned long));
  if (num_of_channels != 4) {
  	file.close();
	cout << "...Can't load since number of channels != 4" << endl;
	return 0;
  }
  file.read(reinterpret_cast< char*> (&fVGain), sizeof(double));
  file.read(reinterpret_cast< char*> (&fHinterval), sizeof(double));

  file.read(reinterpret_cast< char*> (fCH1trace), sizeof(short) * fNumberOfPoints);
  file.read(reinterpret_cast< char*> (fCH2trace), sizeof(short) * fNumberOfPoints);
  file.read(reinterpret_cast< char*> (fCH3trace), sizeof(short) * fNumberOfPoints);
  file.read(reinterpret_cast< char*> (fCH4trace), sizeof(short) * fNumberOfPoints);

  file.close();

  return 1;;

}
//-------------------------------------------------------------------//
short *Oscilloscope::GetTracePointer(unsigned char aChannelNumber){
  if (aChannelNumber == 1) {
  	return fCH1trace;
  } else if (aChannelNumber == 2) {
  	return fCH2trace;
  } else if (aChannelNumber == 3) {
  	return fCH3trace;
  } else if (aChannelNumber == 4) {
  	return fCH4trace;
  } else {
  	return NULL;
  }
}
//-------------------------------------------------------------------//
unsigned long Oscilloscope::GetNofTimebins(unsigned char aChannelNumber){
  if ((aChannelNumber <= 4)&&(aChannelNumber >= 1)) {
	return fNumberOfPoints;
  } else {
  	return 0;
  }
}
//-------------------------------------------------------------------//
unsigned char Oscilloscope::GetNofBytesPerTimebin(){
  return sizeof(short);
}
//-------------------------------------------------------------------//
double Oscilloscope::GetHorInterval(){
  return fHinterval;
}
//-------------------------------------------------------------------//
double Oscilloscope::GetVertGain(){
  return fVGain;
}
//-------------------------------------------------------------------//
