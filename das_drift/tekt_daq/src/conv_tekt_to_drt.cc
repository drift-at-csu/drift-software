#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <getopt.h>
using namespace std;

// #include <DAS_DRIFT_ACcoupling.h>
// #include <DAS_DRIFT_Savitzky_Golay.h>
// using namespace das_drift;

//-------------------------------------------------------------------//
int main (int argc, char **argv) {
  int c;
  unsigned long master_start_bin = 1;
  unsigned long master_stop_bin = 1000;
  unsigned long slave_start_bin = 1;
  unsigned long slave_stop_bin = 500;
  long master_to_slave_offset = 0;
  stringstream out_file_name;
  unsigned short number_of_scope_files = 0;
  short out_file_name_provided = 0;

  while ((c = getopt(argc, argv, "S:P:O:o:")) != EOF) {
	switch (c) {
		case 'S':
			master_start_bin = atol(optarg);
			slave_start_bin = master_start_bin/2;
			continue;
		case 'P':
			master_stop_bin = atol(optarg);
			slave_stop_bin = master_stop_bin/2;
			continue;
		case 'O':
			master_to_slave_offset = atol(optarg);
			continue;
		case 'o':
			out_file_name_provided = 1;
			out_file_name << optarg;
			continue;
			
	};
  }
  number_of_scope_files = argc - optind;
  cout << "...Number of RAW files provided " << number_of_scope_files << endl;

  if (( number_of_scope_files < 1 )||(out_file_name_provided == 0)) { // need to provide a filename
	cerr << "Usage: " << argv[0]
		<< "-S <start_bin> -P <stop_bin> "
		<< "-O <nof_bins_slave_to_master> "
		<< "-o <Output_file_name.drt>"
		<< " <filename_mst.raw> <filename_slv.raw> "
		<< endl;
	return -1;
  }
  cout << "...Adding event to file \"" << out_file_name.str() << "\"" << endl;

  stringstream master_filename_string;
  master_filename_string << argv[optind];

  stringstream slave_filename_string;
  if (number_of_scope_files > 1) {
  	slave_filename_string << argv[optind+1];
  }

  cout << "Traces are about to be read from \"" << master_filename_string.str();
  if (number_of_scope_files > 1) {
  	cout << "\" and \"" << slave_filename_string.str();
  }
  cout << "\"" << endl;

  slave_start_bin += master_to_slave_offset/2;
  slave_stop_bin += master_to_slave_offset/2;

  cout << "...Slave is shifted by " << master_to_slave_offset/2 << " bins" << endl;


  double master_hor_interval = 0.0; // in seconds?
  unsigned long master_nop_in_file = 0;
  double master_vert_gain = 0.0; // in volts ?
  unsigned long master_noch_in_file = 0;

  double slave_hor_interval = 0.0; // in seconds ?
  unsigned long slave_nop_in_file = 0;
  double slave_vert_gain = 0.0; // in volts ?
  unsigned long slave_noch_in_file = 0;

  unsigned long header_length; // in bytes
  header_length = sizeof(unsigned long) +
  	sizeof(unsigned long) +
	sizeof(double) +
	sizeof(double);

  ifstream master_file(master_filename_string.str().c_str(), ios::binary);
  // Let's read the header
  master_file.read(reinterpret_cast< char*> (&master_nop_in_file), sizeof(unsigned long));
  master_file.read(reinterpret_cast< char*> (&master_noch_in_file), sizeof(unsigned long));
  master_file.read(reinterpret_cast< char*> (&master_vert_gain), sizeof(double));
  master_file.read(reinterpret_cast< char*> (&master_hor_interval), sizeof(double));


  ifstream slave_file(slave_filename_string.str().c_str(), ios::binary);
  if (number_of_scope_files > 1) {
	slave_file.read(reinterpret_cast< char*> (&slave_nop_in_file), sizeof(unsigned long));
	slave_file.read(reinterpret_cast< char*> (&slave_noch_in_file), sizeof(unsigned long));
	slave_file.read(reinterpret_cast< char*> (&slave_vert_gain), sizeof(double));
	slave_file.read(reinterpret_cast< char*> (&slave_hor_interval), sizeof(double));
  }


  // FIle Header should be read
  cout << "...Master Header: " << endl;
  cout << "   Number of Points per channel: " << master_nop_in_file << endl;
  cout << "   Number of Channels: " << master_noch_in_file << endl;
  cout << "   Vertical Gain: " << master_vert_gain << " Volts" << endl;
  cout << "   Horizontal bin width: " << master_hor_interval*1.0E+6 << " us" << endl;

  if (number_of_scope_files > 1) {
	cout << "...Slave Header: " << endl;
	cout << "   Number of Points per channel: " << slave_nop_in_file << endl;
	cout << "   Number of Channels: " << slave_noch_in_file << endl;
	cout << "   Vertical Gain: " << slave_vert_gain << " Volts" << endl;
	cout << "   Horizontal bin width: " << slave_hor_interval*1.0E+6 << " us" << endl;
  }

  // Let's append our file and write to it
  ofstream output_file( out_file_name.str().c_str(), ios::binary | ios::out | ios::app );
  output_file << "DRIFTE"; // 6 bytes is a magic number

  // Event format version (to be good from the very beginning)
  unsigned short evnt_version = 0x0001;
  output_file.write(reinterpret_cast< char*> (&evnt_version), sizeof(unsigned short));

  // Then follows gps second (1 unsigned long) and gps nanosecond (1 unsigned long) of the event
  unsigned long evnt_gps_sec  = 0xAA;	
  output_file.write(reinterpret_cast< char*> (&evnt_gps_sec), sizeof(unsigned long));
  unsigned long evnt_gps_nsec = 0xBADC;
  output_file.write(reinterpret_cast< char*> (&evnt_gps_nsec), sizeof(unsigned long));

  // Then How many traces there are in the event (1 ulong)
  unsigned long evnt_nof_trcs = 4;
  if (number_of_scope_files > 1) { evnt_nof_trcs = 8; }
  output_file.write(reinterpret_cast< char*> (&evnt_nof_trcs), sizeof(unsigned long));

  //=====> For Each trace
	// Length in bytes of this particular trace, including header
  unsigned long trace_size;
	// Cannel ID ( 1 ulong )
  unsigned long trace_chan_id;
  	// What is the timestamp of the begining of the event with respect to the event timestamp
	// 	in time bins
  long trace_offset;
	// Vertical scale (of 1 ADC count) in mVolts
  double  trace_vert_scale;
	// Horizontal scale in usec
  double  trace_horiz_scale;
  	// Number of Time bins in trace
  unsigned long trace_number_of_bins;
  
  short curr_rdout_val;
  // Now, we should already be at the begining of CH1, let's jump to the begining of data
  
  trace_size = sizeof(unsigned long) + sizeof(long) + 
  	sizeof(double) + sizeof(double) + sizeof(unsigned long) +
  	sizeof(short)*(master_stop_bin - master_start_bin);
  output_file.write(reinterpret_cast< char*> (&trace_size), sizeof(unsigned long));
  trace_chan_id = 1;
  output_file.write(reinterpret_cast< char*> (&trace_chan_id), sizeof(unsigned long));
  trace_offset = 0;
  output_file.write(reinterpret_cast< char*> (&trace_offset), sizeof( long));
  trace_vert_scale = master_vert_gain*1.0E+3;
  output_file.write(reinterpret_cast< char*> (&trace_vert_scale), sizeof(double));
  trace_horiz_scale = master_hor_interval*1.0E+6;
  output_file.write(reinterpret_cast< char*> (&trace_horiz_scale), sizeof(double));
  trace_number_of_bins = master_stop_bin - master_start_bin;
  output_file.write(reinterpret_cast< char*> (&trace_number_of_bins), sizeof(unsigned long));

  master_file.seekg (header_length + ((master_start_bin - 1)* sizeof(short)), ios::beg);
  for (unsigned long ii = 0; ii < ( master_stop_bin - master_start_bin ); ++ii) {
	master_file.read(reinterpret_cast< char*> (&curr_rdout_val), sizeof(short));
	// Chan 1 data is in curr_rdout_val;
	output_file.write(reinterpret_cast< char*> (&curr_rdout_val), sizeof(short));
  }
//================================//
  trace_chan_id = 2;
  output_file.write(reinterpret_cast< char*> (&trace_size), sizeof(unsigned long));
  output_file.write(reinterpret_cast< char*> (&trace_chan_id), sizeof(unsigned long));
  output_file.write(reinterpret_cast< char*> (&trace_offset), sizeof( long));
  output_file.write(reinterpret_cast< char*> (&trace_vert_scale), sizeof(double));
  output_file.write(reinterpret_cast< char*> (&trace_horiz_scale), sizeof(double));
  output_file.write(reinterpret_cast< char*> (&trace_number_of_bins), sizeof(unsigned long));

  master_file.seekg (header_length + ((master_start_bin - 1)* sizeof(short)) + 
  	master_nop_in_file * sizeof(short), ios::beg);
  for (unsigned long ii = 0; ii < (master_stop_bin - master_start_bin ); ++ii) {
  	master_file.read(reinterpret_cast< char*> (&curr_rdout_val), sizeof(short));
	// Chan 2 data is in curr_rdout_val;
	output_file.write(reinterpret_cast< char*> (&curr_rdout_val), sizeof(short));
  }
//================================//
  trace_chan_id = 3;
  output_file.write(reinterpret_cast< char*> (&trace_size), sizeof(unsigned long));
  output_file.write(reinterpret_cast< char*> (&trace_chan_id), sizeof(unsigned long));
  output_file.write(reinterpret_cast< char*> (&trace_offset), sizeof( long));
  output_file.write(reinterpret_cast< char*> (&trace_vert_scale), sizeof(double));
  output_file.write(reinterpret_cast< char*> (&trace_horiz_scale), sizeof(double));
  output_file.write(reinterpret_cast< char*> (&trace_number_of_bins), sizeof(unsigned long));

  master_file.seekg (header_length + ((master_start_bin - 1)* sizeof(short)) + 
         ( master_nop_in_file * sizeof(short) * 2), ios::beg);
  for (unsigned long ii = 0; ii < (master_stop_bin - master_start_bin); ++ii) {
	master_file.read(reinterpret_cast< char*> (&curr_rdout_val), sizeof(short));
	//Chan 3 data is in curr_rdout_val;
	output_file.write(reinterpret_cast< char*> (&curr_rdout_val), sizeof(short));
  }
//================================//
  trace_chan_id = 4;
  output_file.write(reinterpret_cast< char*> (&trace_size), sizeof(unsigned long));
  output_file.write(reinterpret_cast< char*> (&trace_chan_id), sizeof(unsigned long));
  output_file.write(reinterpret_cast< char*> (&trace_offset), sizeof( long));
  output_file.write(reinterpret_cast< char*> (&trace_vert_scale), sizeof(double));
  output_file.write(reinterpret_cast< char*> (&trace_horiz_scale), sizeof(double));
  output_file.write(reinterpret_cast< char*> (&trace_number_of_bins), sizeof(unsigned long));

  master_file.seekg (header_length + ((master_start_bin - 1)* sizeof(short)) +
  	( master_nop_in_file * sizeof(short) * 3), ios::beg);
  for (unsigned long ii = 0; ii < (master_stop_bin - master_start_bin); ++ii) {
  	master_file.read(reinterpret_cast< char*> (&curr_rdout_val), sizeof(short));
	//Chan 4 data is in curr_rdout_val;
	output_file.write(reinterpret_cast< char*> (&curr_rdout_val), sizeof(short));
  }
  master_file.close();

  if (number_of_scope_files > 1) {
    trace_vert_scale = slave_vert_gain*1.0E+3;
    trace_horiz_scale = slave_hor_interval*1.0E+6;
    trace_number_of_bins = slave_stop_bin - slave_start_bin;
    trace_size = sizeof(unsigned long) + sizeof(long) +
	sizeof(double) + sizeof(double) + sizeof(unsigned long) +
	sizeof(short)*(slave_stop_bin - slave_start_bin);

    trace_chan_id = 5;
    output_file.write(reinterpret_cast< char*> (&trace_size), sizeof(unsigned long));
    output_file.write(reinterpret_cast< char*> (&trace_chan_id), sizeof(unsigned long));
    output_file.write(reinterpret_cast< char*> (&trace_offset), sizeof( long));
    output_file.write(reinterpret_cast< char*> (&trace_vert_scale), sizeof(double));
    output_file.write(reinterpret_cast< char*> (&trace_horiz_scale), sizeof(double));
    output_file.write(reinterpret_cast< char*> (&trace_number_of_bins), sizeof(unsigned long));

    slave_file.seekg (header_length + ((slave_start_bin - 1)* sizeof(short)), ios::beg);
    for (unsigned long ii = 0; ii < (slave_stop_bin - slave_start_bin); ++ii) {
	slave_file.read(reinterpret_cast< char*> (&curr_rdout_val), sizeof(short));
	//Chan 5 data is in curr_rdout_val;
	output_file.write(reinterpret_cast< char*> (&curr_rdout_val), sizeof(short));
    }

    trace_chan_id = 6;
    output_file.write(reinterpret_cast< char*> (&trace_size), sizeof(unsigned long));
    output_file.write(reinterpret_cast< char*> (&trace_chan_id), sizeof(unsigned long));
    output_file.write(reinterpret_cast< char*> (&trace_offset), sizeof( long));
    output_file.write(reinterpret_cast< char*> (&trace_vert_scale), sizeof(double));
    output_file.write(reinterpret_cast< char*> (&trace_horiz_scale), sizeof(double));
    output_file.write(reinterpret_cast< char*> (&trace_number_of_bins), sizeof(unsigned long));

    slave_file.seekg (header_length + ((slave_start_bin - 1)* sizeof(short)) +
	slave_nop_in_file * sizeof(short), ios::beg);
    for (unsigned long ii = 0; ii < (slave_stop_bin - slave_start_bin); ++ii) {
	slave_file.read(reinterpret_cast< char*> (&curr_rdout_val), sizeof(short));
	//Chan 6 data is in curr_rdout_val;
	output_file.write(reinterpret_cast< char*> (&curr_rdout_val), sizeof(short));
    }

    trace_chan_id = 7;
    output_file.write(reinterpret_cast< char*> (&trace_size), sizeof(unsigned long));
    output_file.write(reinterpret_cast< char*> (&trace_chan_id), sizeof(unsigned long));
    output_file.write(reinterpret_cast< char*> (&trace_offset), sizeof( long));
    output_file.write(reinterpret_cast< char*> (&trace_vert_scale), sizeof(double));
    output_file.write(reinterpret_cast< char*> (&trace_horiz_scale), sizeof(double));
    output_file.write(reinterpret_cast< char*> (&trace_number_of_bins), sizeof(unsigned long));

    slave_file.seekg (header_length + ((slave_start_bin - 1)* sizeof(short)) +
	( slave_nop_in_file * sizeof(short) * 2), ios::beg);
    for (unsigned long ii = 0; ii < (slave_stop_bin - slave_start_bin); ++ii) {
	slave_file.read(reinterpret_cast< char*> (&curr_rdout_val), sizeof(short));
	//Chan 7 data is in curr_rdout_val;
	output_file.write(reinterpret_cast< char*> (&curr_rdout_val), sizeof(short));
    }
   
    trace_chan_id = 8;
    output_file.write(reinterpret_cast< char*> (&trace_size), sizeof(unsigned long));
    output_file.write(reinterpret_cast< char*> (&trace_chan_id), sizeof(unsigned long));
    output_file.write(reinterpret_cast< char*> (&trace_offset), sizeof( long));
    output_file.write(reinterpret_cast< char*> (&trace_vert_scale), sizeof(double));
    output_file.write(reinterpret_cast< char*> (&trace_horiz_scale), sizeof(double));
    output_file.write(reinterpret_cast< char*> (&trace_number_of_bins), sizeof(unsigned long));

    slave_file.seekg (header_length + ((slave_start_bin - 1)* sizeof(short)) +
	( slave_nop_in_file * sizeof(short) * 3), ios::beg);
    for (unsigned long ii = 0; ii < (slave_stop_bin - slave_start_bin); ++ii) {
	slave_file.read(reinterpret_cast< char*> (&curr_rdout_val), sizeof(short));
	//Chan 8 data is in curr_rdout_val;
	output_file.write(reinterpret_cast< char*> (&curr_rdout_val), sizeof(short));
    }
  }
  slave_file.close();


  output_file.close();
  return 0;
}

//-------------------------------------------------------------------//
