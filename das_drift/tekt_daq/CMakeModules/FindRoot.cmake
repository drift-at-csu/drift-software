################################################################################
# Module to find Root                                                          #
#                                                                              #
# This sets the following variables:                                           #
#	- Root_FOUND                                                               #
#	- Root_LIBRARIES                                                           #
#	- Root_INCLUDE_DIRS                                                     #
#	- Root_BIN_DIR                            
#	- Root_CINT
################################################################################

# If root was found already, don't find it again.
# This works because Root_FOUND is not cached
if (NOT Root_FOUND)
# include (CMakeCompareVersionStrings)
include (CMakeUtilityMacros)

# Minimum ROOT requirements.
set (Root_MIN_REQUIRED_VERSION "5.16.00")

# the following disables all default paths (either from cmake, from environment)
find_path (Root_BIN_DIR root-config
  PATHS $ENV{ROOTSYS}/bin
  $ENV{AUGER_BASE}/External/ROOT/pro/bin
  NO_DEFAULT_PATH
)

# now defaults are allowed but if nothing is found it is not overwritten
# (because it is cached)
find_path (Root_BIN_DIR root-config)

if (Root_BIN_DIR)
  # ----------------------------------------------------------------------------
  # Get ROOT version, re-express in form XX.YY.ZZ, compare to requirements.
  # ----------------------------------------------------------------------------
  execute_process (COMMAND ${Root_BIN_DIR}/root-config --version
    OUTPUT_VARIABLE Root_VERSION
  )
  string (REGEX REPLACE "[ \t\r\n]+" "" Root_VERSION ${Root_VERSION})
  string (REGEX REPLACE "/" "." Root_VERSION ${Root_VERSION})

  # Note: letters sometimes appended to ROOT patches (e.g., 5.14/00e) will be
  # ignored in the version comparison.
  compare_version_strings (
    "${Root_VERSION}"
    ${Root_MIN_REQUIRED_VERSION}
    _have_MIN_Root_VER
  )

  if (${_have_MIN_Root_VER} GREATER 0 OR ${_have_MIN_Root_VER} EQUAL 0)
    # --------------------------------------------------------------------------
    # Set ROOT compilation flags.
    # --------------------------------------------------------------------------
    SET (Root_FOUND TRUE)

    execute_process (COMMAND ${Root_BIN_DIR}/root-config --libs --glibs
      OUTPUT_VARIABLE Root_LIBS
    )
    string (REGEX REPLACE "[ \t\r\n]+" " " Root_LIBS ${Root_LIBS})

    execute_process (COMMAND ${Root_BIN_DIR}/root-config --libdir
	OUTPUT_VARIABLE _Root_LIBDIR
    )
    string (REGEX REPLACE "[ \t\r\n]+" "" Root_LIBDIR ${_Root_LIBDIR})

    execute_process (COMMAND ${Root_BIN_DIR}/root-config --incdir
      OUTPUT_VARIABLE Root_INCLUDE_DIRS
    )
    string (REGEX REPLACE "[ \t\r\n]+" "" Root_INCLUDE_DIRS ${Root_INCLUDE_DIRS})

    #---- Setting libraries ----#
    string (REGEX REPLACE "-l" " " CRAP ${Root_LIBS})
    string (REGEX REPLACE "-" " " CRAP ${CRAP})
    separate_arguments (CRAP)
    foreach (_file ${CRAP})
    	find_library (_LIBY${_file} ${_file} ${Root_LIBDIR} NO_DEFAULT_PATH)
	if (_LIBY${_file})
		list (APPEND Root_LIBRARIES ${_LIBY${_file}})
	endif (_LIBY${_file})
	set (_LIBY${_file} ${_LIBY${_file}} CACHE INTERNAL "" FORCE)
    endforeach (_file)
    list (APPEND Root_LIBRARIES ${Root_LIBDIR}/libMinuit2.so)
    list (APPEND Root_LIBRARIES -ldl)

  else (${_have_MIN_Root_VER} GREATER 0 OR ${_have_MIN_Root_VER} EQUAL 0)

    set (Root_FOUND FALSE)

    if (NOT Root_FIND_QUIETLY)
      message (STATUS
        "Compatible version of ROOT not found. "
        "Minimum required version: ${Root_MIN_REQUIRED_VERSION}")
    endif (NOT Root_FIND_QUIETLY)

  endif (${_have_MIN_Root_VER} GREATER 0 OR ${_have_MIN_Root_VER} EQUAL 0)

ENDIF (Root_BIN_DIR)

###########################################
#
#       Macros for building ROOT dictionary
#
###########################################

# The following macro creates rules for a dictionary
# If the LinkDef file is called SomeLinkDef.h
# it will create the files SomeDictionary.cc and SomeDictionary.h
# it defines the variables $Some_DICT_DOURCE and $Some_DICT_HEADER
# $Some_INFILES $Some_INCLUDE_DIRS $Some_VERBOSE

# local variables: _LINKDEF _INCLUDE_DIRS _NAME _current_FILE

macro (Root_GENERATE_DICTIONARY _NAME)
  parse_arguments (${_NAME}
    "LINKDEF;INFILES;INCLUDE_DIRS;DEFINE_FLAGS"
    "VERBOSE"
    ${ARGN}
  )
  set (_INCLUDE_DIRS)
  set (${_NAME}_DICT_SOURCE ${CMAKE_CURRENT_BINARY_DIR}/${_NAME}Dictionary.cc)
  set (${_NAME}_DICT_HEADER ${CMAKE_CURRENT_BINARY_DIR}/${_NAME}Dictionary.h)

  # Set up the list of includes
  foreach (_current_FILE ${${_NAME}_INCLUDE_DIRS} ${_DIR_INCLUDES})
    set (_INCLUDE_DIRS ${_INCLUDE_DIRS} -I${_current_FILE})
  endforeach (_current_FILE ${${_NAME}_INCLUDE_DIRS})

  # Set up the list of compiler definitions
  foreach (_current_DEF ${${_NAME}_DEFINE_FLAGS} ${_DEF_FLAGS})
    set (_DEFINE_FLAGS ${_DEFINE_FLAGS} -D${_current_DEF})
  endforeach (_current_DEF ${${_NAME}_DEFINE_FLAGS})

  # Set up the call to rootcint
  if (${_NAME}_VERBOSE)
    message (STATUS "Root dictionary:\n   ${_NAME}_INFILES: ${${_NAME}_INFILES}")
    message ("   OutFILEs: ${${_NAME}_DICT_SOURCE} ${${_NAME}_DICT_HEADER}")
    message ("   LINKDEF: ${${_NAME}_LINKDEF}")
  endif (${_NAME}_VERBOSE)

  add_custom_command (OUTPUT ${${_NAME}_DICT_SOURCE} ${${_NAME}_DICT_HEADER}
    COMMAND ${Root_BIN_DIR}/rootcint -f ${${_NAME}_DICT_SOURCE} -c ${_INCLUDE_DIRS} ${_DEFINE_FLAGS} ${${_NAME}_INFILES} ${${_NAME}_LINKDEF}
    DEPENDS ${${_NAME}_INFILES}
  )

endmacro (Root_GENERATE_DICTIONARY)
endif (NOT Root_FOUND)
