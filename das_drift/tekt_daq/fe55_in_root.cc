#include <TApplication.h>
#include <TROOT.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TColor.h>
#include <TH1D.h>

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <sstream>
#include <fstream>
using namespace std;

//-------------------------------------------------------------------//
int main (int argc, char **argv) {
  int c;
  long start_bin = 0;
  while ((c = getopt(argc, argv, "S:")) != EOF) {
	switch (c) {
		case 'S':
			start_bin = atol(optarg);
			continue;
  	};
  }
  if ( (optind + 1) != argc ) { // need to provide a filename
	cerr << "Usage: " << argv[0]
		<< "-S <start_bin> "
		<< " <filename> "
		<< endl;
	return -1;
  }

  stringstream filename_string;
  filename_string << argv[optind];

  cout << "Traces are about to be read from " << filename_string.str() << endl;

  TApplication theApp ("App", NULL, 0);
  gROOT->Reset();
  gStyle->SetOptStat(kFALSE);
  gStyle->SetFrameBorderMode(0);
  gROOT->ForceStyle();

  TCanvas myc("myc", "Fe55 Spectrum", 800, 600);
  myc.SetFillColor(kWhite);
  myc.cd();
  myc.SetBorderMode(0);
  myc.Draw();

  TPad canv1("canv1", "Trace Pad",0.01,0.51,0.99,0.99);
  canv1.SetBorderMode(0);
  canv1.Draw();
  canv1.SetFillColor(kWhite); 
  canv1.SetFrameFillColor(kWhite);
  canv1.SetGridx();
  canv1.SetGridy();
  // canv1.cd();
  ifstream file(filename_string.str().c_str(), ios::binary);

  if ( !file.is_open() ) {
	cout << "The file could not be opened..." << endl;
	exit (0);
  }

  string line;
  long start_time, stop_time;
  double signal;
  TH1D fe55_signal_histo("ch1_histo", "Channel 1", 200, -0.02, +0.02);

  while (1) {
  	getline(file, line);
	if (!file.good()) break;
	if (line.c_str()[0] == '#') continue;

	std::stringstream ss(line);
	ss >> start_time; ss >> stop_time;  ss >> signal;
	fe55_signal_histo.Fill(signal, 1.0);

	// cout << "...At time " << start_time << " signal " << signal << endl;
	
  }

  file.close();
  fe55_signal_histo.Draw();


  theApp.Run();
  return 0;
}
