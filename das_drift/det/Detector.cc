#include <det/Channel.h>
#include <det/Detector.h>
using namespace drift;

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string.h>
using namespace std;

//-------------------------------------------------------------------//
det::Detector::Detector(){
}
//-------------------------------------------------------------------//
det::Detector::~Detector(){
  for (ChannelIterator iter = ChannelsBegin(); iter != ChannelsEnd(); ++iter) {
	if ((*iter)!=NULL) {
		delete (*iter);
		(*iter) = NULL;
	}
  }
}
//-------------------------------------------------------------------//
unsigned int det::Detector::GetNofChannels() const{
  return fChannels.size();
}
//-------------------------------------------------------------------//
void det::Detector::MakeChannel(const unsigned int channelId){
  if (HasChannel(channelId)) 
	return;
   Channel *ChannelPnt = new Channel();
   ChannelPnt->SetId(channelId);
   fChannels.push_back(ChannelPnt);
}
//-------------------------------------------------------------------//
bool det::Detector::HasChannel(const unsigned int channelId){
  for (ConstChannelIterator iter = ChannelsBegin(); iter != ChannelsEnd(); ++iter) {
	if ((*iter)->GetId() == channelId) {
		return true;
	}
  }
  return false;
}
//-------------------------------------------------------------------//
det::Channel *det::Detector::GetChannel(const unsigned int channelId){
  for (ConstChannelIterator iter = ChannelsBegin(); iter != ChannelsEnd(); ++iter) {
  	if ((*iter)->GetId() == channelId) {
		return (*iter);
	}
  }
  return NULL;
}
//-------------------------------------------------------------------//
det::Detector::ChannelIterator det::Detector::ChannelsBegin(){
  return fChannels.begin();
}
//-------------------------------------------------------------------//
det::Detector::ChannelIterator det::Detector::ChannelsEnd(){
  return fChannels.end();
}
//-------------------------------------------------------------------//
det::Detector::ConstChannelIterator det::Detector::ChannelsBegin() const{
  return fChannels.begin();
}
//-------------------------------------------------------------------//
det::Detector::ConstChannelIterator det::Detector::ChannelsEnd() const{
  return fChannels.end();
}
//-------------------------------------------------------------------//
