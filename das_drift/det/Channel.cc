#include <det/Channel.h>
using namespace drift;

//-------------------------------------------------------------------//
det::Channel::Channel():
		fId(0),
		fTransimpRf(50.0), fTransimpCf(0.6), fTransimpAtten(1.0),
		fSecondACRf(330.0),fSecondACRg(50.0),fSecondACCg(2.2),fSecondACAtten(0.5),
		fFirstACRf(330.0), fFirstACRg(50.0), fFirstACCg(2.2), fFirstACAtten(0.5){
  // This initialized with standard CSU DFE values
  // Attenuation gains: fSecondACAtten is because we have
  // Rout=49.9 Ohms and Rterm=100 Ohms. For the fFirstACAtten it's chosen to be 0.5
  // because we're looking for only positive output of FE wrt ground on
  // oscilloscope
}
//-------------------------------------------------------------------//
det::Channel::~Channel(){
}
//-------------------------------------------------------------------//
unsigned int det::Channel::GetId() const{
  return fId;
}
//-------------------------------------------------------------------//
void det::Channel::SetId(unsigned int id){
  fId = id;
}
//-------------------------------------------------------------------//
double det::Channel::GetTransimpTau() const{
  return fTransimpRf*fTransimpCf;
}
//-------------------------------------------------------------------//
double det::Channel::GetTransimpGain() const{
  return fTransimpAtten*fTransimpRf; //V[mV] = I[na]*Rf[MOhms]
}
//-------------------------------------------------------------------//
double det::Channel::GetSecondACTau() const{
  return fSecondACRg*fSecondACCg;
}
//-------------------------------------------------------------------//
double det::Channel::GetSecondACGain() const{
  if (fSecondACRg == 0.0)
  	return 1.0E+20;
  return fSecondACRf/fSecondACRg*fSecondACAtten;
}
//-------------------------------------------------------------------//
double det::Channel::GetFirstACTau() const{
  return fFirstACRg*fFirstACCg;
}
//-------------------------------------------------------------------//
double det::Channel::GetFirstACGain() const{
  if (fFirstACRg == 0.0)
	return 1.0E+20;
  return fFirstACRf/fFirstACRg*fFirstACAtten;
}
//-------------------------------------------------------------------//
double det::Channel::GetTransimpRf() const{
  return fTransimpRf;
}
//-------------------------------------------------------------------//
void det::Channel::SetTransimpRf(double value){
  fTransimpRf = value;
}
//-------------------------------------------------------------------//
double det::Channel::GetTransimpCf() const{
  return fTransimpCf;  
}
//-------------------------------------------------------------------//
void det::Channel::SetTransimpCf(double value){
  fTransimpCf = value;
}
//-------------------------------------------------------------------//
double det::Channel::GetTransimpAtten() const{
  return fTransimpAtten;
}
//-------------------------------------------------------------------//
void det::Channel::SetTransimpAtten(double value){
  fTransimpAtten = value;
}
//-------------------------------------------------------------------//
double det::Channel::GetSecondACRf() const{
  return fSecondACRf;
}
//-------------------------------------------------------------------//
void det::Channel::SetSecondACRf(double value){
  fSecondACRf = value;
}
//-------------------------------------------------------------------//
double det::Channel::GetSecondACRg() const{
  return fSecondACRg;
}
//-------------------------------------------------------------------//
void det::Channel::SetSecondACRg(double value){
  fSecondACRg = value;
}
//-------------------------------------------------------------------//
double det::Channel::GetSecondACCg() const{
  return fSecondACCg;
}
//-------------------------------------------------------------------//
void det::Channel::SetSecondACCg(double value){
  fSecondACCg = value;
}
//-------------------------------------------------------------------//
double det::Channel::GetSecondACAtten() const{
  return fSecondACAtten;
}
//-------------------------------------------------------------------//
void det::Channel::SetSecondACAtten(double value){
  fSecondACAtten = value;
}
//-------------------------------------------------------------------//
double det::Channel::GetFirstACRf() const{
  return fFirstACRf;
}
//-------------------------------------------------------------------//
void det::Channel::SetFirstACRf(double value){
  fFirstACRf = value;
}
//-------------------------------------------------------------------//
double det::Channel::GetFirstACRg() const{
  return fFirstACRg;
}
//-------------------------------------------------------------------//
void det::Channel::SetFirstACRg(double value){
  fFirstACRg = value;
}
//-------------------------------------------------------------------//
double det::Channel::GetFirstACCg() const{
  return fFirstACCg;
}
//-------------------------------------------------------------------//
void det::Channel::SetFirstACCg(double value){
  fFirstACCg = value;
}
//-------------------------------------------------------------------//
double det::Channel::GetFirstACAtten() const{
  return fFirstACAtten;
}
//-------------------------------------------------------------------//
void det::Channel::SetFirstACAtten(double value){
  fFirstACAtten = value;
}
//-------------------------------------------------------------------//
