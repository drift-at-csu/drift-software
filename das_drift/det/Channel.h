#ifndef _DRIFT_det_Channel_h_
#define _DRIFT_det_Channel_h_


#include <list>

namespace drift {
namespace det {
  class Detector;

class Channel {
  public:
  	Channel();
	~Channel();

	unsigned int GetId() const;
	void SetId(unsigned int id);

	//---- These numbers are recalculated ----//
	double GetTransimpTau() const; // in usec
	double GetTransimpGain() const; // Vout = Gain*Iin [mVolts/nA]
	double GetSecondACTau() const; // in usec
	double GetSecondACGain() const; // Vout = Gain*Vin
	double GetFirstACTau() const;  // in usec
	double GetFirstACGain() const; // Vout = Gain*Vin

	//---- Getters and setters (raw numbers)
	// You don't want to use these directly
	double GetTransimpRf() const;
	void SetTransimpRf(double value);
	double GetTransimpCf() const;
	void SetTransimpCf(double value);
	double GetTransimpAtten() const;
	void SetTransimpAtten(double value);

	double GetSecondACRf() const;
	void SetSecondACRf(double value);
	double GetSecondACRg() const;
	void SetSecondACRg(double value);
	double GetSecondACCg() const;
	void SetSecondACCg(double value);
	double GetSecondACAtten() const;
	void SetSecondACAtten(double value);

	double GetFirstACRf() const;
	void SetFirstACRf(double value);
	double GetFirstACRg() const;
	void SetFirstACRg(double value);
	double GetFirstACCg() const;
	void SetFirstACCg(double value);
	double GetFirstACAtten() const;
	void SetFirstACAtten(double value);

  private:
	unsigned int fId;
	
	/// Attenuation coefficients 
	/// combine the output resistor and termination resistor
	/// they show by how much signal decreases after the stage
	//----- These are transimpedance stage values
	double fTransimpRf; // MOhms
	double fTransimpCf; // pFarads
	double fTransimpAtten; 
	//----- These are second AC correction
	double fSecondACRf; // Ohms
	double fSecondACRg; // Ohms
	double fSecondACCg; // uFarads
	double fSecondACAtten;
	//----- These are first AC correction
	double fFirstACRf;  // Ohms
	double fFirstACRg;  // Ohms
	double fFirstACCg;  // uFarads
	double fFirstACAtten; 

}; /* class Channel */
} /* namespace det */
} /* namespace drift */
#endif /* _DRIFT_det_Channel_h_ */
