#ifndef _das_drift_evt_Event_h_
#define _das_drift_evt_Event_h_

#include <evt/Header.h>

namespace das_drift {
namespace evt {

namespace mwpc{
  class Event;
}

class Event {
  public:
  	Event();
	~Event();

	mwpc::Event& GetMWPCEvent();
	const mwpc::Event& GetMWPCEvent() const;
	bool HasMWPCEvent();
	void MakeMWPCEvent();

	das_drift::evt::Header& GetHeader();
	const das_drift::evt::Header& GetHeader() const;

  private:
	Header fHeader;
	mwpc::Event fMWPCEvent;
}; // class Event

} // namespace evt
} // namespace das_drift
#endif /* _das_drift_evt_Event_h_ */
