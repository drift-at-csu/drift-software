#ifndef _DRIFT_det_Detector_h_
#define _DRIFT_det_Detector_h_

#include <list>
#include <string>

// #include <det/Channel.h>

namespace drift {
namespace det {
  class Channel;
class Detector {
  public:
  	typedef std::list<Channel*> ChannelContainer;
	typedef ChannelContainer::iterator ChannelIterator;
	typedef ChannelContainer::const_iterator ConstChannelIterator;

  public:
  	Detector();
	~Detector();

	unsigned int GetNofChannels() const;
	void MakeChannel(const unsigned int channelId);
	bool HasChannel(const unsigned int channelId);
	Channel *GetChannel(const unsigned int channelId);

  	ChannelIterator ChannelsBegin();
	ChannelIterator ChannelsEnd();
	ConstChannelIterator ChannelsBegin() const;
	ConstChannelIterator ChannelsEnd() const;

  private:
	ChannelContainer fChannels;
}; /* class Detector */
} /* namespace det */
} /* namespace drift */
#endif /* _DRIFT_det_Detector_h_ */
