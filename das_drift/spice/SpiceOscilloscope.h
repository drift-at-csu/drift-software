#ifndef _SpiceOscilloscope_h_
#define _SpiceOscilloscope_h_

#include <string>
#include <vector>
// using namespace std;

namespace Spice {
class Oscilloscope {
  public:
  	Oscilloscope();
	~Oscilloscope();

	int LoadASCII(unsigned char aChannelNumber, std::string aFilename);

	int Save(std::string aFilename);
	int Load(std::string aFilename);

	short *GetTracePointer(unsigned char aChannelNumber);
	unsigned long GetNofTimebins(unsigned char aChannelNumber);
	unsigned char GetNofBytesPerTimebin();
	double GetHorInterval();
	double GetVertGain();

  private:
  	void CleanupMemory();
	int AllocateMemoryFor(unsigned long aNumberOfPoints);

	short *fCH1trace;
	short *fCH2trace;
	short *fCH3trace;
	short *fCH4trace;
	unsigned long fNumberOfPoints; // Number of points in each trace
	double fVGain; // Vertical gain in Volts
	double fHinterval; // Width of 1 bin in sec
};
} /* namespace Spice */
#endif /* _SpiceOscilloscope_h_ */
