#ifndef DASDRIFT_BASE_COPYABLE_H
#define DASDRIFT_BASE_COPYABLE_H

namespace dasdrift {
namespace base {
/// A tag class emphasises the objects are copyable.
/// The empty base class optimization applies.
/// Any derived class of copyable should be a value type.
class Copyable {
};
} // namespace base
} // namespace dasdrift
#endif /* DASDRIFT_BASE_COPYABLE_H */
