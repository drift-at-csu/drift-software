#ifndef DASDRIFT_BASE_TIMESTSAMP_H
#define DASDRIFT_BASE_TIMESTAMP_H

#include <dasdrift/base/Copyable.h>
#include <dasdrift/base/Types.h>

#include <boost/operators.hpp>

namespace dasdrift {
namespace base {

/// Time stamp in GPS, in nanosec resolution.
///
/// This class is immutable.
/// It's recommended to pass it by value, since it's passed in register on x64.

class TimeStamp :
	public dasdrift::base::Copyable,
	public boost::less_than_comparable<TimeStamp>{
  public:
  	/// Constucts a zero timestamp.
	TimeStamp();
	/// Constructs a valid timestamp
	explicit TimeStamp(int32_t gps_sec, int32_t gps_nsec);
	/// Swap this and other timestamps.
	void Swap(TimeStamp& other);
	/// Put timestamp in ASCII format just GPS format
	string ToGPSString() const;
	/// The same thing but formatted.
	string ToUTCString() const;
	/// Returns true if this timestamp is valid
	bool isValid() const;

	static TimeStamp Now();
	static TimeStamp Invalid();

  private:
  	int32_t fSecs;
	int32_t fNanosecs;
}; // class TimeStamp
  // The Operators are living outside of the class

  bool operator<(TimeStamp lhs, TimeStamp rhs);
  bool operator==(TimeStamp lhs, TimeStamp rhs);

  /// Gets time difference of two timestamps, result in seconds.
  double TimeDifference(TimeStamp high, TimeStamp low);

  /// Increase timestamp by a given number of seconds
  Timestamp addTime(Timestamp timestamp, double seconds);

} // namespace base
} // namespace dasdrift
#endif /* DASDRIFT_BASE_TIMESTAMP_H */
