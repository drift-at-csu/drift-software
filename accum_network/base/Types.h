#ifndef DASDRIFT_BASE_TYPES_H
#define DASDRIFT_BASE_TYPES_H

namespace dasdrift {
namespace base {
  using std::string;

  template<typename To, typename From>
	inline To implicit_cast(From const &f) {
	return f;
  }

  template<typename To, typename From>
  	inline To down_cast(From* f) {
	if (false) {
		implicit_cast<From*, To>(0);
	}
	return static_cast<To>(f);
  }
} // namespace base
} // namespace dasdrift
#endif /* DASDRIFT_BASE_COPYABLE_H */
