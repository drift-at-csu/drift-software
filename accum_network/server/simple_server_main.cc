#include "ServerSocket.h"
#include "SocketException.h"

#include <string>
#include <iostream> // for cout
#include <queue>
using namespace std;

#include <boost/thread.hpp>


// Forward Declaration
bool threadHandleRequest(int tid);
//===================================================================//
const size_t MAX_THREADS = 4;

queue<ServerSocket> socketBuffer; // Holds new accepted sockets
boost::mutex queueGuard; // Guards the socketBuffer queue
boost::condition_variable queueIndicator; // Signals a new connection to the worker threads
bool ctrlc_pressed = false; // This is signaling quit to a client

//-------------------------------------------------------------------//
int main ( int argc, int argv[] ) {

  cout << "Starting Server" << endl;
  boost::thread_group threads;
  for(int ii = 0; ii < (int) MAX_THREADS; ii++) {
	threads.create_thread(boost::bind(&threadHandleRequest, ii+1));
  }
  
  ServerSocket s_server (30000); // Port to listen to

  while(!ctrlc_pressed) {
	// wait for incoming connections and pass them to the worker threads
	ServerSocket s_connection;
	if( s_server.Accept( s_connection) ) {
		boost::unique_lock<boost::mutex> lock(queueGuard);
		socketBuffer.push(s_connection);
		queueIndicator.notify_one();
	}
  } // whil Ctrlc_pressed
  threads.interrupt_all(); // interrupt the threads (at queueGuard.wait())
  threads.join_all(); // wait for all threads to finish
  
  // s_server.close();
  return 0;
}
//-------------------------------------------------------------------//
bool threadHandleRequest(int tid) {
  while(true) {
	// wait for a semaphore counter > 0 and automatically decrease the counter
	boost::unique_lock<boost::mutex> lock(queueGuard);
	try {
		queueIndicator.wait(lock);
	} catch (boost::thread_interrupted) {
		return false;
	}
	assert(!socketBuffer.empty());

	ServerSocket s_client = socketBuffer.front();
	socketBuffer.pop();

	lock.unlock();

	// Do whatever you need to do with the socket here
	std::string data;
	s_client >> data;
	cout << "...Recieved \"" << data << "\"" << endl;
  } // while true
}
//-------------------------------------------------------------------//
