#ifndef ServerSocket_h_
#define ServerSocket_h_

#include "Socket.h"

class ServerSocket : private Socket {
 public:
	ServerSocket ( int port );
	ServerSocket ();
	virtual ~ServerSocket();

  	const ServerSocket& operator << ( const std::string& ) const;
	const ServerSocket& operator >> ( std::string& ) const;

  	int Accept ( ServerSocket& );
};
#endif
