// Implementation of the Socket class.


#include "Socket.h"

#include <iostream>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>

//-------------------------------------------------------------------//
Socket::Socket() : fSock ( -1 ) {
  memset ( &fAddr,0, sizeof ( fAddr ) );
}
//-------------------------------------------------------------------//
Socket::~Socket() {
  if ( isValid() )
	::close ( fSock );
}
//-------------------------------------------------------------------//
int Socket::Create() {
  fSock = socket ( AF_INET, SOCK_STREAM, 0 );
  if ( ! isValid() ) 
	return 0;

  // TIME_WAIT - argh
  int on = 1;
  if ( setsockopt ( fSock, SOL_SOCKET, SO_REUSEADDR, ( const char* ) &on, sizeof ( on ) ) == -1 )
	return 0;
  return 1;
}
//-------------------------------------------------------------------//
int Socket::Bind ( const int port ) {
  if ( ! isValid() )
      return 0;

  fAddr.sin_family = AF_INET;
  fAddr.sin_addr.s_addr = INADDR_ANY;
  fAddr.sin_port = htons ( port );

  int bind_return = ::bind ( fSock,
	( struct sockaddr * ) &fAddr, sizeof ( fAddr ) );

  if ( bind_return == -1 )
      return 0;

  return 1;
}
//-------------------------------------------------------------------//
int Socket::Listen() const {
  if ( ! isValid() )
      return 0;

  int listen_return = ::listen ( fSock, MAXCONNECTIONS );
  if ( listen_return == -1 )
	return 0;

  return 1;
}
//-------------------------------------------------------------------//
int Socket::Accept ( Socket& new_socket ) const {
  int addr_length = sizeof ( fAddr );
  new_socket.fSock = ::accept ( fSock, ( sockaddr * ) &fAddr, ( socklen_t * ) &addr_length );
  if ( new_socket.fSock <= 0 )
	return 0;

  return 1;
}
//-------------------------------------------------------------------//
int Socket::Send ( const std::string s ) const {
  int status = ::send ( fSock, s.c_str(), s.size(), MSG_NOSIGNAL );
  if ( status == -1 )
	return 0;
  return 1;
}
//-------------------------------------------------------------------//
int Socket::Recv ( std::string& s ) const {
  char buf [ MAXRECV + 1 ];
  s = "";
  memset ( buf, 0, MAXRECV + 1 );
  int status = ::recv ( fSock, buf, MAXRECV, 0 );
  if ( status == -1 ) {
	std::cout << "status == -1   errno == " << errno << "  in Socket::recv\n";
	return 0;
  } else if ( status == 0 ) {
	return 0;
  } else {
	s = buf;
	return status;
  }
}
//-------------------------------------------------------------------//
int Socket::Connect ( const std::string host, const int port ) {
  if ( ! isValid() ) 
	return 0;

  fAddr.sin_family = AF_INET;
  fAddr.sin_port = htons ( port );

  int status = inet_pton ( AF_INET, host.c_str(), &fAddr.sin_addr );
  if ( errno == EAFNOSUPPORT ) 
	return 0;

  status = ::connect ( fSock, ( sockaddr * ) &fAddr, sizeof ( fAddr ) );

  if ( status == 0 )
	return 1;

  return 0;
}
//-------------------------------------------------------------------//
void Socket::SetNonBlocking ( const int b ) {
  int opts;

  opts = fcntl ( fSock, F_GETFL );

  if ( opts < 0 ) return;

  if ( b )
    opts = ( opts | O_NONBLOCK );
  else
    opts = ( opts & ~O_NONBLOCK );

  fcntl ( fSock, F_SETFL,opts );
}
//-------------------------------------------------------------------//
int Socket::isValid() const { 
  return fSock != -1; 
}
//-------------------------------------------------------------------//
