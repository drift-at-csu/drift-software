#ifndef Socket_h_
#define Socket_h_


#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>
#include <string>
#include <arpa/inet.h>


const int MAXHOSTNAME = 200;
const int MAXCONNECTIONS = 5;
const int MAXRECV = 500;

class Socket {
  public:
	Socket();
	virtual ~Socket();

  	// Server initialization
	int Create();
	int Bind ( const int port );
	int Listen() const;
	int Accept ( Socket& ) const;

	// Client initialization
	int Connect ( const std::string host, const int port );

	// Data Transimission
	int Send ( const std::string ) const;
	int Recv ( std::string& ) const;


  	void SetNonBlocking ( const int );

	int isValid() const;

  private:

	int fSock;
	sockaddr_in fAddr;
};
#endif /* Socket_h_ */
