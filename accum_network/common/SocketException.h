#ifndef SocketException_h_
#define SocketException_h_

#include <string>

class SocketException {
  public:
	SocketException ( std::string s ) : fExcStr ( s ) {};
	~SocketException (){};

	std::string Description() { return fExcStr; }
  private:
	std::string fExcStr;

};

#endif /* SocketException_h_ */
