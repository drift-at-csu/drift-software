#include "ClientSocket.h"
#include "SocketException.h"

//-------------------------------------------------------------------//
ClientSocket::ClientSocket ( std::string host, int port ) {
  if ( ! Socket::Create() )
	throw SocketException ( "Could not create client socket." );


  if ( ! Socket::Connect ( host, port ) )
	throw SocketException ( "Could not bind to port." );
}
//-------------------------------------------------------------------//
const ClientSocket& ClientSocket::operator << ( const std::string& s ) const {
  if ( ! Socket::Send ( s ) )
	throw SocketException ( "Could not write to socket." );
  return *this;
}
//-------------------------------------------------------------------//
const ClientSocket& ClientSocket::operator >> ( std::string& s ) const {
  if ( ! Socket::Recv ( s ) )
	throw SocketException ( "Could not read from socket." );
  return *this;
}
//-------------------------------------------------------------------//
