#include "ClientSocket.h"
#include "SocketException.h"

#include <iostream>
#include <string>
#include <sstream>
using namespace std;

//-------------------------------------------------------------------//
int main ( int argc, int argv[] ) {
    try {
	ClientSocket client_socket ( "localhost", 30000 );
	// string reply;
	unsigned long trig_number = 0;
	stringstream  string_to_send;

	while (true) {
	  try {
		sleep(1);
		string_to_send.str("");

		string_to_send << "Time: " << trig_number;
		client_socket << string_to_send.str();
		trig_number ++;

	  	// client_socket >> reply;
	  } catch ( SocketException& ) {
	  }
	  // std::cout << "Client: Received:\n\"" << reply << "\"\n";
	}
    } catch ( SocketException& e ) {
	cout << "Exception was caught:" << e.Description() << endl;
    }
  return 0;
}
//-------------------------------------------------------------------//
