// Definition of the ClientSocket class

#ifndef ClientSocket_h_
#define ClientSocket_h_

#include "Socket.h"


class ClientSocket : private Socket {
  public:
	ClientSocket ( std::string host, int port );
	virtual ~ClientSocket(){};

	const ClientSocket& operator << ( const std::string& ) const;
	const ClientSocket& operator >> ( std::string& ) const;
};

#endif /* ClientSocket_h_ */
