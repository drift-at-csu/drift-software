#include <stdio.h>
#include <stdint.h>

#define OUT_REG_STRING "wb_dat_o"
#define ASSIGN_STRING "<= "
#define SIZE_STRING "32'h"
#define BEFORE_STRING "%d : "OUT_REG_STRING" "ASSIGN_STRING" "SIZE_STRING

int main(void) {

  int c, word_counter=0;
  uint32_t word;
  uint8_t *wordarray = (uint8_t*) &word;

  while((c = getchar()) != EOF) {
	/* Endianess might change things here */
	wordarray[3] = c;
	wordarray[2] = getchar();
	wordarray[1] = getchar();
	wordarray[0] = getchar();
	printf(BEFORE_STRING, word_counter);
	printf("%.8x;\n", word);
	word_counter++;
  }
  return(0);
}
