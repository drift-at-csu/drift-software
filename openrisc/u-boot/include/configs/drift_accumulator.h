#ifndef __CONFIG_H
#define __CONFIG_H

/*
 * BOARD/CPU
 */
#define CONFIG_SYS_CLK_FREQ	50000000
/* Hard-reset address	*/
#define CONFIG_SYS_RESET_ADDR	0xF0000100
/* #define CONFIG_SYS_RELOCATE_VECTORS */

#define CONFIG_SYS_SDRAM_BASE	0x00000000
#define CONFIG_SYS_SDRAM_SIZE	0x00800000

#define CONFIG_SYS_CACHELINE_SIZE	16

#define CONFIG_SYS_UART_BASE	0x90000000
#define CONFIG_SYS_UART_FREQ	CONFIG_SYS_CLK_FREQ
#define CONFIG_SYS_UART_BAUD	115200

#define CONFIG_BOARD_NAME	"DRIFT Accumulator (OR1200)"

#define CONFIG_SYS_NO_FLASH
#define CONFIG_SYS_MAX_FLASH_SECT	0

/*
 * SERIAL
 */
# define CONFIG_SYS_NS16550
# define CONFIG_SYS_NS16550_SERIAL
# define CONFIG_SYS_NS16550_REG_SIZE	1
# define CONFIG_CONS_INDEX		1
# define CONFIG_SYS_NS16550_COM1	(0x90000000)
# define CONFIG_SYS_NS16550_CLK		CONFIG_SYS_CLK_FREQ

#define CONFIG_BAUDRATE			CONFIG_SYS_UART_BAUD
#define CONFIG_SYS_BAUDRATE_TABLE	{CONFIG_BAUDRATE}
/* Suppress console info */
/* #define CONFIG_SYS_CONSOLE_INFO_QUIET */
#define CONSOLE_ARG			"console=console=ttyS0,115200\0"
/*
 * Ethernet
 */
#define CONFIG_ETHOC
#define CONFIG_SYS_ETHOC_BASE	0x92000000
#define CONFIG_ETHADDR          de:ad:be:ef:da:00
#define CONFIG_IPADDR           192.168.1.200
#define CONFIG_NETMASK          255.255.255.0
#define CONFIG_GATEWAYIP        192.168.1.1
#define CONFIG_SERVERIP         192.168.1.1
#define CONFIG_HOSTNAME         accumulator00

#define CONFIG_MII
#define CONFIG_PHY_ADDR		0x03 
#define CONFIG_PHYLIB
#define CONFIG_PHY_MICREL
#define CONFIG_PHY_MICREL_KSZ9021
/*
 * ADCs
 */
#define CONFIG_CMD_ADCLVDS

/*
 * TIMER
 */
#define CONFIG_SYS_HZ			1000
#define CONFIG_SYS_OPENRISC_TMR_HZ	100

/*
 * SPI
 */
#define CONFIG_SPI
#define CONFIG_OC_SIMPLE_SPI
#define CONFIG_OC_SIMPLE_SPI_BUILTIN_SS
#define CONFIG_SYS_SIMPLE_SPI_BASE	0xb0000000
#define CONFIG_SYS_SIMPLE_SPI_LIST 	{{ \
		.freq = CONFIG_SYS_CLK_FREQ, \
		.base = CONFIG_SYS_SIMPLE_SPI_BASE,}}

#define CONFIG_SPI_FLASH
#define CONFIG_SPI_FLASH_STMICRO
#define CONFIG_SF_DEFAULT_MODE	SPI_MODE_0
#define CONFIG_SF_DEFAULT_SPEED CONFIG_SYS_CLK_FREQ
#define CONFIG_SF_DEFAULT_CS	0
/* This defines how much bootrom should download from SPI 
 * It should be equal to the length of bootloader section
 * in the MTD partition Default is 720,896 bytes (0x000b0000)
 * But you can decrease this number. All it needs to be is not
 * smaller than the size of U-Boot binary*/
#define CONFIG_SYS_SPI_UBOOT_SIZE	0x000b0000

/* This is where the Bootloader jumps after downloading U-Boot image */
#define CONFIG_SYS_SPI_UBOOT_RESET	0x00000100

#define CONFIG_ENV_SPI_CS	0
#define CONFIG_ENV_SPI_BUS	0
#define CONFIG_ENV_SPI_MAX_HZ	CONFIG_SYS_CLK_FREQ
#define CONFIG_ENV_SPI_MODE	SPI_MODE_0

#define CONFIG_ENV_OFFSET	(0xf0000)
#define CONFIG_ENV_SECT_SIZE	(64 * 1024)
#define CONFIG_ENV_SIZE		( 4 * 1024)

#define CONFIG_ENV_OVERWRITE    1       /* enable changing MAC at runtime */

#define CONFIG_ENV_IS_IN_SPI_FLASH
/*#define CONFIG_ENV_IS_NOWHERE */
/*
 * Memory organisation:
 *
 * RAM start ---------------------------
 *           | ...                     |
 *           ---------------------------
 *           | Stack                   |
 *           ---------------------------
 *           | Global data             |
 *           ---------------------------
 *           | Environment             |
 *           ---------------------------
 *           | Monitor                 |
 * RAM end   ---------------------------
 */
/* We're running in RAM */
#define CONFIG_MONITOR_IS_IN_RAM
#define CONFIG_SYS_MONITOR_LEN	0x40000	/* Reserve 256k */
#define CONFIG_SYS_MONITOR_BASE	(CONFIG_SYS_SDRAM_BASE + \
				CONFIG_SYS_SDRAM_SIZE - \
				CONFIG_SYS_MONITOR_LEN)

/* Where the U-boot vectors are */
#define CONFIG_SYS_VECTORS_BASE		0x00000000
#define CONFIG_SYS_VECTORS_LEN 		0x00002000
/*
 * Global data object and stack pointer
 */
#define CONFIG_SYS_GBL_DATA_OFFSET	(CONFIG_SYS_MONITOR_BASE - GENERATED_GBL_DATA_SIZE)
#define CONFIG_SYS_GBL_DATA_ADDR	CONFIG_SYS_GBL_DATA_OFFSET
#define CONFIG_SYS_INIT_SP_ADDR		CONFIG_SYS_GBL_DATA_OFFSET
#define CONFIG_SYS_INIT_SP_OFFSET	CONFIG_SYS_GBL_DATA_OFFSET
#define CONFIG_SYS_STACK_LENGTH		0x4000 /* Default: 64KB (0x10000)*/

#define CONFIG_SYS_MALLOC_LEN		0x100000 /* Default: 4MB (0x400000)*/
#define CONFIG_SYS_MALLOC_BASE		(CONFIG_SYS_INIT_SP_OFFSET \
					- CONFIG_SYS_STACK_LENGTH \
					- CONFIG_SYS_MALLOC_LEN)
/*
 * BOOTROM
 */
/* If we want to generate a Bootloader file from *.bin */
#define CONFIG_OR1KBOOTLOADER
/* Where to put binary U-Boot file in memory */
#define CONFIG_BOOTROM_LOAD_ADDR	CONFIG_SYS_VECTORS_BASE
/*
 * MISC
 */
/* Provide extended help */
#define CONFIG_SYS_LONGHELP
/* The prompt of U-Boot */
#define CONFIG_SYS_PROMPT	"accumulator ==> "
/* Console I/O buf size in bytes */
#define CONFIG_SYS_CBSIZE	256
/* Max command args     */
#define CONFIG_SYS_MAXARGS	16
/* Bootarg buf size */
#define CONFIG_SYS_BARGSIZE	CONFIG_SYS_CBSIZE
/* Print buf size */
#define CONFIG_SYS_PBSIZE	(CONFIG_SYS_CBSIZE + sizeof(CONFIG_SYS_PROMPT) + 16)
/* This is ADDRESS in memry where the linux boots */
#define CONFIG_SYS_LOAD_ADDR		CONFIG_SYS_SDRAM_BASE
/* This is the Start address for memtest */
#define CONFIG_SYS_MEMTEST_START	(CONFIG_SYS_LOAD_ADDR + 0x2000)
#define CONFIG_SYS_MEMTEST_END		(CONFIG_SYS_INIT_SP_ADDR - 0x20000)


#define CONFIG_CMDLINE_EDITING
#define CONFIG_CMDLINE_TAG
/*
 * Command line configuration.
 */


/* 
 * Image Formats
 */
/* #define CONFIG_OF_LIBFDT */
/* #define CONFIG_FIT */
/* #define CONFIG_LMB */

#include <config_cmd_default.h>

/* */
/* #undef CONFIG_CMD_NET */
/* #undef CONFIG_CMD_NFS */

#define CONFIG_CMD_IRQ
#define CONFIG_CMD_ELF
#define CONFIG_CMD_BSP

#define CONFIG_CMD_SF
#define CONFIG_CMD_SPI
#define CONFIG_CMD_MTDPARTS
/* #define CONFIG_LZO */

/*
 * MTD
 */
#define CONFIG_MTD_DEVICE
#define CONFIG_MTD_PARTITIONS
/* #define CONFIG_LZO */
/* #define CONFIG_BZIP2 */
#undef CONFIG_GZIP

/*
 * Ethernet Options
 */
#define CONFIG_CMD_NET
#define CONFIG_CMD_MII
#define CONFIG_CMD_MDIO
#define CONFIG_CMD_DHCP
#define CONFIG_CMD_PING

/*
 * BOOTP options
 */
#define CONFIG_BOOTP_BOOTFILESIZE
#define CONFIG_BOOTP_BOOTPATH
#define CONFIG_BOOTP_GATEWAY
#define CONFIG_BOOTP_HOSTNAME

/*
 * Default environment variables
 */
#define CONFIG_LOADADDR		0x2000
#define CONFIG_BOOTARGS		"uart,mmio,0x90000000,115200"
/* */
#define CONFIG_BOOTCOMMAND	"sf probe; "    \
	"sf read $(loadaddr) 0x00100000 $(uimage_size);"  \
	"bootm $(loadaddr);"
#define CONFIG_CMDLINE_TAG
#define CONFIG_BOOTDELAY 5

#define CONFIG_EXTRA_ENV_SETTINGS \
	"uimage_size=0x3190e4\0" \
	"ubootfile=u-boot_accum.bin\0" \
	"ubootupdate=tftp $(loadaddr) $(ubootfile);" \
		"sf probe; " \
		"sf erase 0x0 +0x$(filesize); " \
		"sf write $(loadaddr) 0x0 0x$(filesize);\0" \
	"kernelfile=uImage-accumulator\0" \
	"kernelupdate=tftp $(loadaddr) $(kernelfile); " \
		"sf probe; " \
		"sf erase 0x100000 +0x$(filesize); " \
		"sf write $(loadaddr) 0x100000 0x$(filesize); " \
		"set uimage_size 0x$(filesize);" \
		"env delete fileaddr; env delete filesize; " \
		"saveenv;\0" \
	"rootfsfile=rootfs-accumulator.jffs2\0" \
	"rootfsupdate=tftp $(loadaddr) $(rootfsfile); " \
		"sf probe; " \
		"sf erase 0x600000 +0x$(filesize); " \
		"sf write $(loadaddr) 0x600000 0x$(filesize);\0" \

#endif /* __CONFIG_H */
