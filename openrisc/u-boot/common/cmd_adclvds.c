#include <common.h>
#include <config.h>
#include <command.h>

#if defined(CONFIG_CMD_ADCLVDS)
//-------------------------------------------------------------------//
static int do_adclvds(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[]) {
  int adc_number;
  int chan_number;

  if (argc < 2)
	return CMD_RET_USAGE;
  // We have at least one argument
  if (strncmp(argv[1], "ini", 3) == 0) {
  	if (argc < 3) return CMD_RET_USAGE;
	// We have the second argument
	adc_number = (int)simple_strtol(argv[2], NULL, 10);
	adclvds_init_adc(adc_number);
  } else if (strncmp(argv[1], "shu", 3) == 0) {
  	if (argc < 3) return CMD_RET_USAGE;
	adc_number = (int)simple_strtol(argv[2], NULL, 10);
	adclvds_shutdown_adc(adc_number);
  } else if (strncmp(argv[1], "meminit", 7) == 0) {
  	adclvds_mem_init();
  } else if (strncmp(argv[1], "powon", 5) == 0) {
  	if (argc < 4) return CMD_RET_USAGE;
	adc_number = (int)simple_strtol(argv[2], NULL, 10);
	chan_number= (int)simple_strtol(argv[3], NULL, 10);
	adclvds_chan_poweron (adc_number, chan_number);
  } else {
	return CMD_RET_USAGE;
  }

  return 0;
}
//-------------------------------------------------------------------//
U_BOOT_CMD(
	adclvds, 5, 1, do_adclvds,
	"automate commands for ADCs management",
	"<command> \n\tinit <adc_num>\n\tshutdown <adc_num>\n\tmeminit\n\tpowon <adc_num> <chan_num>\n"
);
//-------------------------------------------------------------------//
#endif
