#include <common.h>
#include <netdev.h>
//-------------------------------------------------------------------//
int board_early_init_f(void) {
  int rc = 0;
  return rc;
}
//-------------------------------------------------------------------//
int checkboard(void) {
  printf("BOARD: %s\n", CONFIG_BOARD_NAME);
  return 0;
}
//-------------------------------------------------------------------//
phys_size_t initdram(int board_type) {
  return 0;
}
//-------------------------------------------------------------------//
#ifdef CONFIG_CMD_NET
int board_eth_init(bd_t *bis) {
  int rc = 0;
#ifdef CONFIG_ETHOC
  printf("ETHOC Init\n");
  rc += ethoc_initialize(0, CONFIG_SYS_ETHOC_BASE);
#endif /* CONFIG_ETHOC */

  return rc;
}
#endif /* CONFIG_CMD_NET */
//-------------------------------------------------------------------//
void board_reset (void) {
  asm ("l.movhi r1, 0xF000");
  asm ("l.ori r1,r1, 0x0100");
  asm ("l.jr r1");
  asm ("l.nop");
}
//-------------------------------------------------------------------//
