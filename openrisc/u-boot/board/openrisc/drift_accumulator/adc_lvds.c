#include <common.h>
#include <netdev.h>
#include <asm/io.h>

#define ADCLVDS_MEM_START	0x01000000
#define ADCLVDS_MEM_CHAN_OFFSET		0x0000	// In case we need some header
#define ADCLVDS_MEM_CHAN_SIZE		0x1000	// 0x0810

#define	ADCLVDS_CBUS_BASE 0x7A000000
#define ADCLVDS_CBUS_INCR 0x01000000

#define ADCLVDS_REG_CONTROL 	0x000
#define ADCLVDS_REG_CONTROL_POWADC	0x01000000
#define ADCLVDS_REG_CONTROL_POWGR1	0x02000000
#define ADCLVDS_REG_CONTROL_POWGR2      0x04000000
#define ADCLVDS_REG_CONTROL_POWGR3      0x08000000
#define ADCLVDS_REG_CONTROL_POWGR4      0x10000000

#define ADCLVDS_REG_PHYSTAT	0x004
#define ADCLVDS_REG_PHYSTAT_RESET	0x20000000
#define ADCLVDS_REG_PHYSTAT_BITSLIP	0x10000000
#define ADCLVDS_REG_PHYSTAT_DELAYINC	0x00001000

#define ADCLVDS_REG_ADCS_CHANBASE	0x010
#define ADCLVDS_REG_ADCS_CHANSIZE	0x020


#define ADCLVDS_REG_SPI		0x1F0
#define ADCLVDS_REG_SPI_READY		0x20000000

#define ADCLVDS_REG_TST_SEC	0x1F4
#define ADCLVDS_REG_TST_QPS     0x1F8


//-------------------------------------------------------------------//
void adclvds_print_range(void){
	printf("Only ADCs 1-6 are valid\n");
}
//-------------------------------------------------------------------//
int adclvds_write_spi (u32 base_addr, u16 addr, u16 data){
  u32 wb_data;
  wb_data = 0x00000000 | ((addr & 0x7FFF) << 16 ) | data;
  writel(wb_data, base_addr + ADCLVDS_REG_SPI );
  do {
	wb_data = readl(base_addr + ADCLVDS_REG_SPI);
  } while (!(wb_data & ADCLVDS_REG_SPI_READY));

  return 0;
}
//-------------------------------------------------------------------//
u16 adclvds_read_spi (u32 base_addr, u16 addr ) {
  u32 wb_data;
  u16 data = 0x0000;
  wb_data = 0x80000000 | ((addr & 0x7FFF) << 16 ) | data;
  writel(wb_data, base_addr + ADCLVDS_REG_SPI );
  do {
	wb_data = readl(base_addr + ADCLVDS_REG_SPI);
  } while (!(wb_data & ADCLVDS_REG_SPI_READY));
  data = wb_data & 0xFFFF;
  return data;
}
//-------------------------------------------------------------------//
// The function returns the pattern for frame clock
u16 adclvds_phy_issuebitslip (u32 base_addr) {
  u32 wb_data;
  wb_data = ADCLVDS_REG_PHYSTAT_BITSLIP;
  writel(wb_data, base_addr + ADCLVDS_REG_PHYSTAT );
  wb_data = readl(base_addr + ADCLVDS_REG_PHYSTAT );

  return ((wb_data & 0x0FFF0000) >> 16);
}
//-------------------------------------------------------------------//
// The function returns the pattern for frame clock
u16 adclvds_phy_issuedelayinc (u32 base_addr) {
  u32 wb_data;
  wb_data = ADCLVDS_REG_PHYSTAT_DELAYINC;
  writel(wb_data, base_addr + ADCLVDS_REG_PHYSTAT );
  wb_data = readl(base_addr + ADCLVDS_REG_PHYSTAT );
  return ((wb_data & 0x0FFF0000) >> 16);
}
//-------------------------------------------------------------------//
void adclvds_init_adc (int adc_number){
  if ((adc_number < 1)||(adc_number > 6)){
  	adclvds_print_range();
	return ;
  }
  u32 base_addr, data;
  int ii;
  u16 fco_pattern;

  base_addr = ADCLVDS_CBUS_BASE + ((adc_number-1)*ADCLVDS_CBUS_INCR);
  printf("...Initializing ADC %d with Addr = 0x%x\n", adc_number, base_addr);
  do {
  	// disable power
	data = 0x00000000; 
	writel(data, base_addr+ADCLVDS_REG_CONTROL);
	udelay (100);
	// Turn On the ADC
	data = ADCLVDS_REG_CONTROL_POWADC; 
	writel(data, base_addr+ADCLVDS_REG_CONTROL);
	udelay (500);
	// Read ADC ID From SPI
	data = adclvds_read_spi( base_addr, 0x0002);
	printf("...Powering ADC:  ID is 0x%x\n", (data & 0xFFFF)); 
	// Put ADC in binary mode
	adclvds_write_spi( base_addr, 0x0014, 0x0000);
	// Issue a reset for PHY
	data = ADCLVDS_REG_PHYSTAT_RESET; 
	writel( data, base_addr+ADCLVDS_REG_PHYSTAT );
	// Put a 0x800 pattern on adc
	adclvds_write_spi( base_addr,0x000D, 0x0100);

	// Issue a bitslip
	for (ii = 0; ii < 6; ++ii) {
		fco_pattern = adclvds_phy_issuebitslip (base_addr);
		// printf("... ADC phy: FCO pattern = 0x%x\n", fco_pattern);
		if ( (fco_pattern&0xFFF) == 0xFC0 )
			break;
	}
  	printf("\t ADC PHY: FCO pattern = 0x%x\n", fco_pattern);
  } while ( (fco_pattern&0xFFF) != 0xFC0 );
  // After This there is a hope of getting the right pattern
  // Let's toggle delay, to make sure we're right in the eye for dco
  // There are 32 taps, which are circularly rotating, let's map
  // the regions, where our fc0 pattern holds
  int delay_tap_start = 33;
  int delay_tap_end = 33;
  int right_pattern_observed = 1;
  for (ii = 0; ii < 32; ++ii) {
  	fco_pattern = adclvds_phy_issuedelayinc (base_addr);
	if (fco_pattern == 0xFC0) {
		// At this tap the righ pattern observed
		if (right_pattern_observed == 0) {
			// We're switching from wrong pattern to right pattern
			delay_tap_start = ii;
		}
		right_pattern_observed = 1;
	} else {
		// At this tap the wrong pattern observed
		if (right_pattern_observed == 1) {
			// We're switching from right pattern to wrong pattern
			delay_tap_end = ii;
		}
		right_pattern_observed = 0;
	}
	// printf("\t ADC PHY: tap = %d FCO pattern = 0x%x\n", ii, fco_pattern);
  }
  if (delay_tap_start > delay_tap_end) {
	// The eye is on the outside borders
	right_pattern_observed = (delay_tap_start + (delay_tap_end + 32))/2;
	right_pattern_observed = right_pattern_observed % 32;
  } else {
  	// The eye is inside
	right_pattern_observed = (delay_tap_start + delay_tap_end)/2;
  }
  printf("\t ADC PHY Delay: The Eye is [%d, %d] = %d\n", 
  	delay_tap_start, delay_tap_end, right_pattern_observed);
  for (ii = 0; ii < right_pattern_observed; ++ii) {
          fco_pattern = adclvds_phy_issuedelayinc (base_addr);
  }
  // Switch to normal operation
  adclvds_write_spi( base_addr,0x000D, 0x0000);
}
//-------------------------------------------------------------------//
void adclvds_shutdown_adc (int adc_number){
  if ((adc_number < 1)||(adc_number > 6)){
  	adclvds_print_range();
	return ;
  }
  u32 base_addr, data;
  printf("...Shutdown ADC %d\n", adc_number);
  base_addr = ADCLVDS_CBUS_BASE + ((adc_number-1)*ADCLVDS_CBUS_INCR);
  // disable power
  data = 0x00000000;
  writel(data, base_addr+ADCLVDS_REG_CONTROL);
}
//-------------------------------------------------------------------//
// Initializes ADC Memory (QDRII, Bank 1)
void adclvds_mem_init(){
  int ii, jj;
  u32 mem_addr = ADCLVDS_MEM_START;
  u32 base_addr = ADCLVDS_CBUS_BASE;
  printf("...Init ADC Memory\n");
  for (ii = 0; ii < 6; ++ii) {
	for (jj = 0; jj < 8; ++jj) {
		writel( mem_addr, base_addr + ADCLVDS_REG_ADCS_CHANBASE + 0x8 +
			(ADCLVDS_REG_ADCS_CHANSIZE*jj) );
		/*
		printf ("...ADC%d, Chan%d, Reg=0x%x, Mem=0x%x\n",
			ii + 1, jj + 1, base_addr + ADCLVDS_REG_ADCS_CHANBASE + 0x8 +
			(ADCLVDS_REG_ADCS_CHANSIZE*jj), mem_addr );
		*/
		mem_addr += ADCLVDS_MEM_CHAN_SIZE;
	}
	base_addr += ADCLVDS_CBUS_INCR;
  }
  // Synchronize the Clocks here too
  base_addr = ADCLVDS_CBUS_BASE;
  for (ii = 0; ii < 6; ++ii) {
  	writel( 0x80000000, base_addr + ADCLVDS_REG_TST_SEC);
	writel( 0x00000000, base_addr + ADCLVDS_REG_TST_SEC);

	base_addr += ADCLVDS_CBUS_INCR;
  }
}
//-------------------------------------------------------------------//
void adclvds_chan_poweron (int adc_number, int chan_number) {
  if ((adc_number < 1)||(adc_number > 6)){
	adclvds_print_range();
	return ;
  }
  if ((chan_number < 1)||(chan_number > 8) ) {
  	adclvds_print_range();
	return;
  }
  u32 base_addr, data;
  int ii; 
  int group_number = ((chan_number-1)/2) + 1;

  if ((group_number < 1)||(group_number > 4) ) {
  	adclvds_print_range();
	return;
  }

  base_addr = ADCLVDS_CBUS_BASE + ((adc_number-1)*ADCLVDS_CBUS_INCR);
  // Let's Read the control register
  data = readl (base_addr+ADCLVDS_REG_CONTROL);

  if ((group_number == 1)&&(data & ADCLVDS_REG_CONTROL_POWGR1)) {
  	// Already turned on
  	return;
  } else if ((group_number == 2)&&(data & ADCLVDS_REG_CONTROL_POWGR2)) {
  	// Already turned on
	return;
  } else if ((group_number == 3)&&(data & ADCLVDS_REG_CONTROL_POWGR3)) {
  	// Already turned on
	return;
  } else if ((group_number == 4)&&(data & ADCLVDS_REG_CONTROL_POWGR4)) {
  	// Already turned on
	return;
  }
  // We should turn on analog sections only when ADC is on
  if ( !(data & ADCLVDS_REG_CONTROL_POWADC)) {
	printf("...Init ADC %d first\n", adc_number);
	return;
  }
  // If the channel number is of group 3 or 4, then let's turn them on
  if (group_number == 3) {
  	data |= ADCLVDS_REG_CONTROL_POWGR3; 
  } else {
  	// The Group Number 3 should be turned on before the rest of channels
  	if ( !(data & ADCLVDS_REG_CONTROL_POWGR3) ) {
  		printf("...Channel group 3 should be turned before group %d\n",
			group_number);
		return;
	}
	if (group_number == 1) {
		data |= ADCLVDS_REG_CONTROL_POWGR1;
	} else if (group_number == 2) {
		data |= ADCLVDS_REG_CONTROL_POWGR2;
	} else if (group_number == 4) {
		data |= ADCLVDS_REG_CONTROL_POWGR4;
	}
  }
  printf ("... Writing 0x%x to 0x%x\n", data, base_addr + ADCLVDS_REG_CONTROL);
  writel( data, base_addr + ADCLVDS_REG_CONTROL);
}
//-------------------------------------------------------------------//
