#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include <string.h> 		// memset
#include <sys/socket.h>		// for socket operations
#include <arpa/inet.h>		// For inet_addr
#include <netinet/ip.h>		//Provides declarations for ip header
#include <netinet/udp.h>	//Provides declarations for udp header
#include <sys/mman.h>		// mmap and munmap

// The following 2 has x4 transfer in bytes
#define FADC_BUFFER_HEADER_SIZE	( 5 )
#define FADC_BUFFER_SIZE	( 512 )
//-------------------------------------------------------------------//
int main (int argc, char **argv) {
  int res,fd;
  unsigned long buffer_data [2]; 	// Data as it's arriving from buffer (4096 bytes)
  unsigned long data_nof_words = FADC_BUFFER_HEADER_SIZE + FADC_BUFFER_SIZE; // (2068 bytes)

  unsigned long *data;
  unsigned long data_size = data_nof_words * sizeof(unsigned long);
  unsigned long *mmaped_data;

  char source_ip[32];
  int sck;
  int sent_bytes;

  printf("DRIFT Accumulator Client\n");
  // printf("PageSize=%d", getpagesize());
  if ((sck=socket(PF_INET, SOCK_RAW, IPPROTO_UDP)) < 0){
	perror("socket");
	return 1;
  }
  // IP header
  struct iphdr *ip_header;
  // UDP header
  struct udphdr *udp_header;
  struct sockaddr_in sin;

  printf("...sizes: [ip=%d][udp=%d]\n",  sizeof(struct ip), sizeof(struct udphdr));
  // Some address resolution
  strcpy(source_ip , "192.168.1.221");

  sin.sin_family = AF_INET;
  sin.sin_port = htons(9930); // port
  sin.sin_addr.s_addr = inet_addr ("192.168.2.1"); // Destination address
  // sin.sin_addr.s_addr = inet_addr ("192.168.1.109");

  //IP_HDRINCL to tell the kernel that headers are included in the packet
  int one = 1;
  const int *val = &one;
  if (setsockopt (sck, IPPROTO_IP, IP_HDRINCL, val, sizeof (one)) < 0){
	perror("setting IP_HDRINCL");
  	return 1;
  }

//---> Open FADC file
  printf("...Enter Readout Loop\n");
  /*
  fd = open("/dev/fadc",O_RDWR);
  if (fd < 0) {
	perror("open /dev/fadc");
	return 1;
  }
  */

  while (1) {
  	fd = open("/dev/fadc",O_RDWR);
	if (fd < 0) {
		perror("open /dev/fadc");
		return 1;
	}
	res = read(fd, buffer_data, 2*sizeof(unsigned long) ); // We're trying to read 2 values
	if (!(res > 0) ) {
		usleep(100000);
		continue;
	}
	// If we're here, then we have something, let's remap our buffer
	// void *mmap(void *addr, size_t length, int prot, int flags,
	//                   int fd, off_t offset);
	// int munmap(void *addr, size_t length);
	mmaped_data = mmap(NULL, 
		sizeof (struct ip) + sizeof (struct udphdr) + data_size, 
		PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
	if (mmaped_data == MAP_FAILED) {
		printf("mmap failed:, length=%lu ",
			sizeof (struct ip) + sizeof (struct udphdr) + data_size);
		return 1;
	}
	//------> Fill in the IP Header
	ip_header =  (struct iphdr *) mmaped_data;
	ip_header->ihl = 5;
	ip_header->version = 4;
	ip_header->tos = 0;
	ip_header->tot_len = sizeof (struct iphdr) + sizeof (struct udphdr) + data_size;
	ip_header->id = htonl (54321);                //Id of this packet
	ip_header->frag_off = 0;
	ip_header->ttl = 255;

	ip_header->protocol = IPPROTO_UDP;
	ip_header->check = 0;                         //Set to 0 before calculating checksum
	ip_header->saddr = inet_addr ( source_ip );   //Spoof the source ip address
	ip_header->daddr = sin.sin_addr.s_addr;
	// IP Checksumm ?
	// iph->check = csum ((unsigned short *) datagram, iph->tot_len);
	//-------> Fill in UDP Header
	udp_header = (struct udphdr *) (mmaped_data + sizeof(struct iphdr)/sizeof(unsigned long));
	udp_header->source = htons(9930);     	// Optional
	udp_header->dest   = htons(9930);
	udp_header->len    = htons(sizeof(struct udphdr) + data_size );
	udp_header->check  = 0;			// Optional

	data = (unsigned long *)(mmaped_data + 
		(sizeof (struct iphdr) + sizeof (struct udphdr))/sizeof(unsigned long) );
	
	// printf("...IP at %p , UDP at %p data at %p\n", ip_header, udp_header, data );

	// Let's print out the header:
	/*
	printf("CH %02lu TS: %05lusec %06luusec ", 
		data[0], data[1],
		(unsigned long)  (data[3])/(data[2]/1000000));
	*/
	// And try to send it: we need an opened socket with sck
	// the address where we're sending it
	// size of the packet and the packet itself
	sent_bytes = sendto(sck, mmaped_data, ip_header->tot_len, 0, 
		(struct sockaddr *)&sin, sizeof(sin) );

	/*
	if ( sent_bytes < 0 ) {
		printf( "failed%d\n", sent_bytes );
	} else {
		printf(" sent  %d\n", sent_bytes);
	}
	*/
	// Unmap the file
	munmap(mmaped_data, ip_header->tot_len);

	// Release the buffer:
	write(fd, data, sizeof(unsigned long));
	close(fd);
  }
  // close(fd);
  return 0;  
}
