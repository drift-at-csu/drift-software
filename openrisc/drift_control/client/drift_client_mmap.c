#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>		// EXIT_FAILURE
#include <string.h>		// memset

#include <sys/socket.h>		// for socket operations
#include <arpa/inet.h>
#include <net/if.h>		// ifreq
#include <sys/ioctl.h>		// ioctl

#include <linux/if_ether.h>
#include <linux/if_packet.h>
#include <poll.h>
#include <pthread.h>

// The following 2 has x4 transfer in bytes
#define FADC_BUFFER_HEADER_SIZE	( 5 )
// #define FADC_BUFFER_SIZE	( 512 )
#define FADC_BUFFER_SIZE     ( 300 )
//-------------------------------------------------------------------//
volatile int fd_socket;
struct tpacket_req s_packet_req;

static int c_mtu	 = 0;
static int c_buffer_sz   = 4096;
static int c_buffer_nb   = 10;	// this results in 40KBytes block size?
static int mode_loss     = 0;
//-------------------------------------------------------------------//
int main (int argc, char **argv) {
  int res,fd;
  char buffer_data [4096]; 	// Data as it's arriving from buffer (4096 bytes)
  unsigned long data_nof_words = FADC_BUFFER_HEADER_SIZE + FADC_BUFFER_SIZE; // (2068 bytes)

  unsigned long *data = (unsigned long *) buffer_data;
  unsigned long data_size = data_nof_words * sizeof(unsigned long);

  // Socket parameters
  int mode_socket;		// socket mode
  struct sockaddr_ll my_addr; 	// This (client) address
  struct ifreq s_ifr; 		// points to one interface returned from ioctl
  int ec;			// handler for ioctl
  int i_ifindex;		// interface index
  uint32_t size;
  int tmp;


  printf("DRIFT Accumulator Client\n");

  printf("...PageSize=%d\n",getpagesize());
  mode_socket = SOCK_RAW;
  // fd_socket = socket(PF_PACKET, mode_socket, htons(ETH_P_ALL));
  fd_socket = socket(AF_PACKET, mode_socket, htons(ETH_P_ALL));
  if(fd_socket == -1) {
	perror("socket");
	return EXIT_FAILURE;
  }
  /* start socket config: device and mtu */
  /* clear structure */
  memset(&my_addr, 0, sizeof(struct sockaddr_ll));
  my_addr.sll_family = PF_PACKET;
  my_addr.sll_protocol = htons(ETH_P_ALL);

  /* initialize interface struct */
  strncpy (s_ifr.ifr_name, "eth0", sizeof(s_ifr.ifr_name));
  
  /* Get the broadcast address */
  ec = ioctl(fd_socket, SIOCGIFINDEX, &s_ifr);
  if(ec == -1) {
	perror("iotcl");
	return EXIT_FAILURE;
  }
  /* update with interface index */
  i_ifindex = s_ifr.ifr_ifindex;

  /* new mtu value */
  if(c_mtu) {
	s_ifr.ifr_mtu = c_mtu;
	/* update the mtu through ioctl */
	ec = ioctl(fd_socket, SIOCSIFMTU, &s_ifr);
	if(ec == -1) {
		perror("iotcl");
		return EXIT_FAILURE;
	}
  }
  /* set sockaddr info */
  memset(&my_addr, 0, sizeof(struct sockaddr_ll));
  my_addr.sll_family = AF_PACKET;
  my_addr.sll_protocol = ETH_P_ALL;
  my_addr.sll_ifindex = i_ifindex;

  /* bind port */
  if (bind(fd_socket, (struct sockaddr *)&my_addr, sizeof(struct sockaddr_ll)) == -1) {
	perror("bind");
	return EXIT_FAILURE;
  }

  /* prepare Tx ring request */
  s_packet_req.tp_block_size = c_buffer_sz;
  s_packet_req.tp_frame_size = c_buffer_sz;
  s_packet_req.tp_block_nr =   c_buffer_nb;
  s_packet_req.tp_frame_nr =   c_buffer_nb;

  /* calculate memory to mmap in the kernel */
  size = s_packet_req.tp_block_size * s_packet_req.tp_block_nr;

  /* set packet loss option */
  tmp = mode_loss;
  if (setsockopt(fd_socket,
		SOL_PACKET,
		PACKET_LOSS,
		(char *)&tmp,
		sizeof(tmp))<0){
	perror("setsockopt: PACKET_LOSS");
	return EXIT_FAILURE;
  }
  /* send TX ring request */
  if (setsockopt(fd_socket,
		SOL_PACKET,
		PACKET_TX_RING,
		(char *)&s_packet_req,
		sizeof(s_packet_req))<0) {
	perror("setsockopt: PACKET_TX_RING");
	return EXIT_FAILURE;
  }

//---> Open FADC file
  printf("...Enter Readout Loop\n");      
  fd = open("/dev/fadc",O_RDONLY);
  if (fd < 0) {
	perror("open /dev/fadc");
	return 1;
  }

  while (1) {
	res = read(fd, data, data_size );
	if (res != data_size ) {
		usleep(100000);
		continue;
	}
	// Let's print out the header:
	printf("Chan %02lu Time: %05lusec %06luusec [0x%08x 0x%08x...0x%08x 0x%08x] ",
		data[0], data[1], 
		(unsigned long)  (data[3])/(data[2]/1000000),
		(unsigned int) data[5], 
		(unsigned int) data[6],
		(unsigned int) data[data_nof_words-2],
		(unsigned int) data[data_nof_words-1]);
	// And try to send it: we need an opened socket with sck
	// the address where we're sending it
	// size of the packet and the packet itself
	/*
	sent_bytes = sendto(sck, buffer_data, ip_header->tot_len, 0, 
		(struct sockaddr *)&sin, sizeof(sin) );
	if ( sent_bytes < 0 ) {
		printf( "failed%d\n", sent_bytes );
	} else {
		printf(" sent  %d\n", sent_bytes);
	}
	*/
	printf("\n");
  }
  close(fd);
  return 0;  
}
