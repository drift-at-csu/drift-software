#!/bin/bash
SYSFS_CHAN_PATH=/sys/class/fadc_channels
SYSFS_CNTR_PATH=/sys/class/fadc_controller/fadc
# turning off 00 01 09 24 25 44 45
SYSFS_CHANNELS="02 03 04 05 06 07 08 10 11 12 13 14 15 16 17 18 19 20 21 22 23 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 46 47"

for chan in $SYSFS_CHANNELS; do
	echo 1 > $SYSFS_CHAN_PATH/chan$chan/enable
done
echo 1 > $SYSFS_CNTR_PATH/timesync
