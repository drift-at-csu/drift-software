#ifndef _DRIFT_SERVER_EventCollection_h_
#define _DRIFT_SERVER_EventCollection_h_

#include <list>
#include <string>

#include "TimeInterval.h"

namespace drift_server {
  class Event;
  class Channel;

class EventCollection {
  public:
  	typedef std::list<Event*> EventContainer;
	typedef EventContainer::iterator EventIterator;
	typedef EventContainer::const_iterator ConstEventIterator;

  public:
  	EventCollection();
	~EventCollection();

	unsigned int GetNofEvents() const;

	void MakeEvent(const unsigned int eventId);
	bool HasEvent(const unsigned int eventId);
	Event *GetEvent(const unsigned int eventId);
	bool RemoveEvent(const unsigned int eventId);

	unsigned int GetHighestEventId() const;

	ConstEventIterator EventsBegin() const;
	ConstEventIterator EventsEnd()   const;
  	EventIterator 	   EventsBegin();
	EventIterator 	   EventsEnd();

	// Procedure for incorporating trace into the events list
	// returns true if the incorporation was successfull
	bool IncorporateChannel(Channel *channel);

	// Procedure for gathering scattered events
	void MergeScatteredEvents(void);

  private:
	EventContainer fEvents;
	unsigned int fHighestEventId;
	TimeInterval fGroupTimeInterval;
}; /* class EventCollection */
} /* namespace drift_server */
#endif /* _DRIFT_SERVER_EventCollection_h_ */
