#include "Event.h"
#include "Channel.h"
using namespace drift_server;

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string.h>
using namespace std;

typedef std::chrono::duration<double, std::ratio<1, 1000>> miliseconds;
//-------------------------------------------------------------------//
Event::Event(){
  fModTime = chrono::system_clock::now();
}
//-------------------------------------------------------------------//
Event::~Event(){
  for (ChannelIterator iter = ChannelsBegin(); iter != ChannelsEnd(); ++iter) {
	if ((*iter)!=NULL) {
		delete (*iter);
		(*iter) = NULL;
	}
  }
}
//-------------------------------------------------------------------//
bool Event::LoadFromDRTFile(std::string file_name, 
		unsigned long event_number) {
  ifstream drt_file(file_name.c_str(), ios::binary);
  if (!drt_file.is_open() ) {
	cout << "...evt::Event: Can't open \"" << file_name << "\"" << endl;
	return false;
  }
  unsigned long current_event_number = 1;
  while (1) {
	char tmp_char[7];
	drt_file.read(reinterpret_cast< char*> (tmp_char), 6);
	// cout << "...evt::Event: Magic string is \"" << tmp_char << "\"" << endl;
	if ( strcmp(tmp_char,"DRIFTE")!=0 ) {
		cout << "...Event: Magic string is incorrect" << endl;
		drt_file.close();
		return false;
	}
	unsigned short file_version = 0;
	drt_file.read(reinterpret_cast< char*> (&file_version),sizeof(unsigned short));
	cout << "...Event: Trying to read event v." << file_version << endl;
	unsigned long evnt_gps_sec; 
	unsigned long evnt_gps_nsec;
	drt_file.read(reinterpret_cast< char*> (&evnt_gps_sec),sizeof(unsigned long));
	drt_file.read(reinterpret_cast< char*> (&evnt_gps_nsec),sizeof(unsigned long));
	GetTimeStart().SetGPSTime(evnt_gps_sec, evnt_gps_nsec);
	unsigned long evnt_nof_trcs = 0;
	drt_file.read(reinterpret_cast< char*> (&evnt_nof_trcs),sizeof(unsigned long));
	// cout << "...evt::Event: There are " << evnt_nof_trcs << " traces in this event" << endl;

	// Let's clear the previous traces if any
	fChannels.clear();

	for (unsigned long ii=0; ii < evnt_nof_trcs; ++ii){
		unsigned long trace_size;
		drt_file.read(reinterpret_cast< char*> (&trace_size),sizeof(unsigned long));
		// cout << "...evt::Event: Trace " << ii+1 << " size=" << trace_size << " bytes" << endl;
		if (current_event_number != event_number) {
			// This means we just want to skip to next event
			drt_file.seekg(trace_size, ios_base::cur);
		} else {
			// We're reading this event
			unsigned long trace_chan_id;
			drt_file.read(reinterpret_cast< char*> (&trace_chan_id),sizeof(unsigned long));
			// cout << "...evt::Event: Trace " << ii+1 << " Making Channel " 
			//	<< trace_chan_id << endl;
			MakeChannel(trace_chan_id);

			long trace_offset;
			drt_file.read(reinterpret_cast< char*> (&trace_offset),sizeof(long));
			double  trace_vert_scale;
			drt_file.read(reinterpret_cast< char*> (&trace_vert_scale),sizeof(double));
			double  trace_horiz_scale;
			drt_file.read(reinterpret_cast< char*> (&trace_horiz_scale),sizeof(double));
			unsigned long trace_number_of_bins;
			drt_file.read(reinterpret_cast< char*> (&trace_number_of_bins),
				sizeof(unsigned long));
			short trace_current_bin_value;

			GetChannel(trace_chan_id)->GetTrace().SetBinDuration(trace_horiz_scale);
			for (unsigned long jj=0; jj < trace_number_of_bins; ++jj){
				drt_file.read (reinterpret_cast< char*> (&trace_current_bin_value), 
					sizeof(short));

				GetChannel(trace_chan_id)->GetTrace().AddTimeBin(trace_current_bin_value);
			}
		}
	}
	if (current_event_number == event_number) {
		// This means that we read out the right event
		
		break;
	}
	current_event_number ++;
  } // while(1) loop on the events
  drt_file.close();
  return true;
}
//-------------------------------------------------------------------//
bool Event::SaveToDRTFile( std::string file_name ){
  ofstream drt_file(file_name.c_str(), ios::binary | ios::out | ios::app);
  if (!drt_file.is_open() ) {
	cout << "...Event: Can't open \"" << file_name << "\" for writing" << endl;
	return false;
  }
  char tmp_char[7] = "DRIFTE";
  drt_file.write(reinterpret_cast< char*> (tmp_char), 6);
  unsigned short file_version = 6;
  drt_file.write(reinterpret_cast< char*> (&file_version),sizeof(unsigned short));

  unsigned long evnt_gps_sec  = GetTimeStart().GetGPSSecond();
  unsigned long evnt_gps_nsec = GetTimeStart().GetGPSNanoSecond();
  drt_file.write(reinterpret_cast< char*> (&evnt_gps_sec),sizeof(unsigned long));
  drt_file.write(reinterpret_cast< char*> (&evnt_gps_nsec),sizeof(unsigned long));

  unsigned long evnt_nof_trcs = GetNofChannels();
  drt_file.write(reinterpret_cast< char*> (&evnt_nof_trcs),sizeof(unsigned long));

  for (ConstChannelIterator iter = ChannelsBegin(); iter != ChannelsEnd(); ++iter){
  	// Here we're calculating the size of the following section in bytes
	unsigned long trace_size = sizeof(unsigned long) // trace_chan_id
		+ sizeof(long) 		// trace_offset
		+ sizeof(double) 	// trace_vert_scale
		+ sizeof(double) 	// trace_horiz_scale
		+ sizeof(unsigned long)	// trace_number_of_bins
		+ sizeof(uint32_t)*(*iter)->GetTrace().GetNofTimeBins();
	drt_file.write(reinterpret_cast< char*> (&trace_size),sizeof(unsigned long));

	unsigned long trace_chan_id = (*iter)->GetId();
	drt_file.write(reinterpret_cast< char*> (&trace_chan_id),sizeof(unsigned long));

	TimeInterval this_trace_offset = (*iter)->GetTimeStamp() - GetTimeStart();
	long trace_offset = ( this_trace_offset.GetSecond()*1E+9 + 
		this_trace_offset.GetNanoSecond() );
	// in Nanosecs
	drt_file.write(reinterpret_cast< char*> (&trace_offset),sizeof(long));

	double  trace_vert_scale = (*iter)->GetTrace().GetVerticalScale();
	drt_file.write(reinterpret_cast< char*> (&trace_vert_scale),sizeof(double));

	double  trace_horiz_scale = (*iter)->GetTrace().GetBinDuration();
	drt_file.write(reinterpret_cast< char*> (&trace_horiz_scale),sizeof(double));

	unsigned long trace_number_of_bins = (*iter)->GetTrace().GetNofTimeBins();
	drt_file.write(reinterpret_cast< char*> (&trace_number_of_bins),
							sizeof(unsigned long));
	uint32_t trace_current_bin_value;
	for (unsigned long ii = 0; ii < (*iter)->GetTrace().GetNofTimeBins(); ++ii){
		trace_current_bin_value = (*iter)->GetTrace().GetTimeBin(ii);
		drt_file.write (reinterpret_cast< char*> (&trace_current_bin_value),
						sizeof(uint32_t));
	}
  }

  drt_file.close();
  return true;
}
//-------------------------------------------------------------------//
unsigned long Event::GetId(){
  return fId;
}
//-------------------------------------------------------------------//
void Event::SetId(unsigned long id){
  fId = id;
}
//-------------------------------------------------------------------//
TimeStamp &Event::GetTimeStart(){
  return fTimeRange.GetBegin();
}
//-------------------------------------------------------------------//
TimeStamp &Event::GetTimeEnd(){
  return fTimeRange.GetEnd();
}
//-------------------------------------------------------------------//
TimeRange &Event::GetTimeRange(){
  return fTimeRange;
}
//-------------------------------------------------------------------//
TimeInterval Event::GetTimeDuration(){
  return GetTimeEnd() - GetTimeStart();
}
//-------------------------------------------------------------------//
unsigned int Event::GetNofChannels() const{
  return fChannels.size();
}
//-------------------------------------------------------------------//
void Event::MakeChannel(const unsigned int channelId){
  if (HasChannel(channelId)) 
	return;
   Channel *ChannelPnt = new Channel();
   ChannelPnt->SetId(channelId);
   fChannels.push_back(ChannelPnt);
   fModTime = chrono::system_clock::now();
}
//-------------------------------------------------------------------//
bool Event::HasChannel(const unsigned int channelId){
  for (ConstChannelIterator iter = ChannelsBegin(); iter != ChannelsEnd(); ++iter) {
	if ((*iter)->GetId() == channelId) {
		return true;
	}
  }
  return false;
}
//-------------------------------------------------------------------//
Channel *Event::GetChannel(const unsigned int channelId){
  for (ConstChannelIterator iter = ChannelsBegin(); iter != ChannelsEnd(); ++iter) {
  	if ((*iter)->GetId() == channelId) {
		return (*iter);
	}
  }
  return NULL;
}
//-------------------------------------------------------------------//
bool Event::IncorporateChannel(Channel *channel) {
  // let's Update the time interval:
  if (GetNofChannels() > 0) {
  	// We do have some channels already in this event
	if (channel->GetTimeStamp() > GetTimeEnd())
		GetTimeEnd() = channel->GetTimeStamp();
	if (channel->GetTimeStamp() < GetTimeStart())
		GetTimeStart() = channel->GetTimeStamp();
  } else {
  	// This is the first channel to add
	GetTimeStart() = channel->GetTimeStamp();
	GetTimeEnd() = channel->GetTimeStamp();
  }
  fChannels.push_back(channel);
  fModTime = chrono::system_clock::now();
  return true;
}
//-------------------------------------------------------------------//
Event::ChannelIterator Event::ChannelsBegin(){
  return fChannels.begin();
}
//-------------------------------------------------------------------//
Event::ChannelIterator Event::ChannelsEnd(){
  return fChannels.end();
}
//-------------------------------------------------------------------//
Event::ConstChannelIterator Event::ChannelsBegin() const{
  return fChannels.begin();
}
//-------------------------------------------------------------------//
Event::ConstChannelIterator Event::ChannelsEnd() const{
  return fChannels.end();
}
//-------------------------------------------------------------------//
std::chrono::system_clock::time_point &Event::GetModTime(){
  return fModTime;
}
//-------------------------------------------------------------------//
double Event::GetSecondsSinceMod(){
  return 0.001*miliseconds( chrono::system_clock::now()-GetModTime() ).count();
}
//-------------------------------------------------------------------//
void Event::ClearChanPointers(void){
  fChannels.clear();
}
//-------------------------------------------------------------------//
