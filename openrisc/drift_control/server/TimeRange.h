#ifndef _DRIFT_SERVER_TimeRange_h_
#define _DRIFT_SERVER_TimeRange_h_

#include "TimeStamp.h"

namespace drift_server {
class TimeRange {
  public:
	TimeRange();
	TimeRange(TimeStamp &begin, TimeStamp &end);
	~TimeRange();

	void SetInterval(TimeStamp &begin, TimeStamp &end);
	TimeStamp &GetBegin();
	TimeStamp &GetEnd();

	/// Returns true if the timestamp value is within this range
	bool isWithin(TimeStamp &value);
	/// Returns true if the time stamp is in proximity of this range
	bool isNear(TimeStamp &value, const TimeInterval& delta);

	TimeRange operator=(const TimeRange& tmr);

  private:
  	TimeStamp fBegin;
	TimeStamp fEnd;

}; /* class TimeRange */
} /* namespace drift_server */
#endif /* _DRIFT_SERVER_TimeRange_h_ */
