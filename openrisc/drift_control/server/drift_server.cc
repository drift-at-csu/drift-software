// C server code to manage data collected by accumulator board
// a config file needs to be added
// more logic may need to be added to check client and server status 
// service packets will also need to be added
// AVD: Moving from TCP/IP to UDP packets


#include <stdio.h> /* perror,fprintf */
#include <sys/types.h> // u32_t like types
#include <sys/socket.h> /* recv,send,socket,bind,listen */
#include <arpa/inet.h> /* */
#include <stdlib.h> /* exit */
#include <string.h> /* memset */
#include <unistd.h> /* close */
#include <netinet/in.h> /*sockaddr_in */
#include <netinet/ip.h>         //Provides declarations for ip header
#include <netinet/udp.h>        //Provides declarations for udp header


#include <fcntl.h>
#include <ctype.h>	// for isspace

#include <iostream>
#include <chrono>
#include <thread>
#include <mutex>
using namespace std;

#include "EventCollection.h"
#include "Event.h"
#include "Channel.h"
using namespace drift_server;

// Let's also create mutexes
std::mutex g_event_collection_mutex;

#define MAXPENDING 50    /* Max connection requests */
#define LINE_SIZE 48
#define BUFF_SIZE 4096

//length of time window in seconds to group data packets as an event
//for actual use with accumulator board this value will be much smaller
#define TIME_WINDOW 0.01 
//-------------------------------------------------------------------//
// Forward declaration functions
void kill_server(char *mess);
void handle_client_packets(EventCollection *events, unsigned short port_number);
void handle_events_storage(EventCollection *events);
//-------------------------------------------------------------------//
// Entry point for the event collection
int main(int argc, char *argv[]) {
	unsigned short port_number;
	if (argc != 2) {
		cerr << "USAGE: ./drift_server <port=9930>" << endl;
		return 1;
	}
	port_number = atoi(argv[1]);
	EventCollection events_collection;

	std::thread t1(handle_client_packets, &events_collection, port_number);
	std::thread t2(handle_events_storage, &events_collection);

	t1.join();
	t2.join();

	return 1;
}
//------------------------------------------------------------------//
void kill_server(char *mess) { 
	perror(mess); 
	exit(1); 
}
//------------------------------------------------------------------//
void handle_events_storage(EventCollection *events){
  if (!events) {
  	cerr << "...EventCollection is NULL" << endl;
	return;
  }

  std::list<unsigned int> event_ids_to_delete;
  double sec_to_wait_for_update = 20.0;
  while (1){	
	g_event_collection_mutex.lock();
	//Let's try to merge events first
	events->MergeScatteredEvents();

	cout << "...Number Of Events:" << events->GetNofEvents() << " [ ";
	for (EventCollection::ConstEventIterator iter = events->EventsBegin(); 
			iter != events->EventsEnd(); ++iter) {
		if ( (*iter)->GetSecondsSinceMod() > sec_to_wait_for_update ) {
			cout << (*iter)->GetSecondsSinceMod() << "\t";
			event_ids_to_delete.push_back((*iter)->GetId());
		}
	}
	cout << " ] sec since mod" << endl;

	for (std::list<unsigned int>::const_iterator iter = event_ids_to_delete.begin();
		iter != event_ids_to_delete.end(); ++iter){
		// Let's save the given event first
		Event *event = events->GetEvent((*iter));
		if ( !event ) {
			cerr << "...WTF? Can't get Event " << (*iter) << endl;
			continue;
		}

		if ( event->SaveToDRTFile("drift_raw_traces.drt") ){
			// We want to remove it only when saved
			events->RemoveEvent((*iter));
		}
	}
	event_ids_to_delete.clear();

	g_event_collection_mutex.unlock();

	// Sleep
	std::this_thread::sleep_for(std::chrono::seconds(2));
  }
}
//------------------------------------------------------------------//
int check_client_packet(unsigned char *packet, unsigned long size){
  struct iphdr  *ip_header =  (struct iphdr *) packet;
  // struct udphdr *udp_header = (struct udphdr *)(packet + sizeof(struct ip));
  if (htons(ip_header->id) != 54321 ){
  	printf("...packet with wrong id %u\n", htons(ip_header->id));
	return 0;
  }
  return 1; // No error
}
//------------------------------------------------------------------//
void create_trace_from_data(uint32_t *data, unsigned long size, 
	EventCollection *events ){

  uint32_t channel_id =   htonl(data[0]);
  uint32_t time_sec =     htonl(data[1]);
  uint32_t time_qps =     htonl(data[2]);
  uint32_t time_qnt =     htonl(data[3]);
  uint32_t aver_reg =	  htonl(data[4]);
  unsigned short aver_waveform = 1 << ((aver_reg & 0x00F)>>0);

  Channel *channel = new Channel();
  channel->SetId(channel_id);
  // In nsec (40MSPS means 25nsecs)
  channel->GetTrace().SetBinDuration(25.0*aver_waveform);
  // The full range of our 12bit ADC is 2000.0mV = 4095
  channel->GetTrace().SetVerticalScale(2000.0/4095/aver_waveform);
  // Fill in the channel trace
  for (unsigned short ii = 5; ii < size; ++ii){
  	channel->GetTrace().AddTimeBin(htonl(data[ii]));
  }
  // Channel Timestamp (the end of the trace)
  channel->GetTimeStamp().SetGPSTime( time_sec, 1.0E+9*time_qnt/time_qps );

  g_event_collection_mutex.lock();
  channel->PrintInfo();
  if (events->IncorporateChannel(channel)){
  	// The incorporation was successfull
  } else {
  	// The incorporation didn't happen
	// channel->PrintInfo();
	delete channel;
  }
  g_event_collection_mutex.unlock();
}
//------------------------------------------------------------------//
void handle_client_packets(EventCollection *events, unsigned short port_number){
  if (!events){
  	cerr << "...EventCollection should not be 0" << endl;
  }

  int sock;
  struct sockaddr_in server_addr;
  unsigned int server_addr_length;

  /* Create the UDP socket */
  if ((sock = socket(AF_INET, SOCK_RAW, IPPROTO_UDP)) < 0) {
  	cerr << "ERROR: Failed to create socket" << endl;
  	return;
  }
  /* Construct the server sockaddr_in structure */
  memset(&server_addr, 0, sizeof(server_addr));           /* Clear struct */
  server_addr.sin_family =        AF_INET;                /* Internet/IP */
  server_addr.sin_addr.s_addr =   htonl(INADDR_ANY);      /* Incoming addr */
  server_addr.sin_port =          htons(port_number);	/* server port */
  server_addr_length = sizeof(server_addr);

  unsigned char packet[BUFF_SIZE];
  uint32_t *data;
  int  received=1;
  
  while( 1 ){
  	if ( (received = recvfrom(sock, (char *)&packet, sizeof(packet), 0,
			(struct sockaddr *) &server_addr, &server_addr_length)) < 0 ){
		perror("packet receive: ");
	}
	if(received > 0){
		// Check the packet
		if (!check_client_packet (packet, received)){
			continue;
		}
		// DATA packet logic
		data = (uint32_t *)packet + (sizeof(struct ip) + sizeof (struct udphdr))/
				sizeof(uint32_t);

		create_trace_from_data(data, 
			(received - sizeof(struct ip) - sizeof (struct udphdr))/sizeof(uint32_t),
			events);

	}
	usleep(1000);
  }
}
//-------------------------------------------------------------------//
