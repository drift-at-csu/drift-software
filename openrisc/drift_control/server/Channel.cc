#include "Channel.h"
using namespace drift_server;

#include<iostream>
using namespace std;

//-------------------------------------------------------------------//
Channel::Channel():
		fId(0){
}
//-------------------------------------------------------------------//
Channel::~Channel(){
}
//-------------------------------------------------------------------//
unsigned int Channel::GetId() const{
  return fId;
}
//-------------------------------------------------------------------//
void Channel::SetId(unsigned int id){
  fId = id;
}
//-------------------------------------------------------------------//
Trace& Channel::GetTrace() {
  return fRawTrace;
}
//-------------------------------------------------------------------//
TimeStamp& Channel::GetTimeStamp(){
  return fTimeStamp;
}
//-------------------------------------------------------------------//
void Channel::PrintInfo(){
  cout << "Chan " << GetId() << " @ " << GetTimeStamp().GetGPSSecond()
  	<< "sec " << GetTimeStamp().GetGPSNanoSecond() << "nsec "
	<< ",NofBins = " << GetTrace().GetNofTimeBins() 
	<< ",VertScale=" << GetTrace().GetVerticalScale() << "mV/ADC "
	<< ",HorizScale=" << GetTrace().GetBinDuration() << "nsec "
	<< endl;

  // GetTrace().PrintInfo();

}
//-------------------------------------------------------------------//
