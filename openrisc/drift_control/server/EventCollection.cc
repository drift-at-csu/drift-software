#include "EventCollection.h"
#include "Event.h"
#include "Channel.h"
using namespace drift_server;

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string.h>
using namespace std;

//-------------------------------------------------------------------//
EventCollection::EventCollection():
	fHighestEventId(0),
	fGroupTimeInterval(0,10000000){ // 4msec
	// 4000000	 // 4msec
	// 10000000ns = grouping over 10 msec
}
//-------------------------------------------------------------------//
EventCollection::~EventCollection(){
  for (EventIterator iter = EventsBegin(); iter != EventsEnd(); ++iter) {
	if ((*iter)!=NULL) {
		delete (*iter);
		(*iter) = NULL;
	}
  }
}
//-------------------------------------------------------------------//
unsigned int EventCollection::GetNofEvents() const{
  return fEvents.size();
}
//-------------------------------------------------------------------//
void EventCollection::MakeEvent(const unsigned int eventId){
  if (HasEvent(eventId)) 
	return;
   Event *EventPnt = new Event();
   EventPnt->SetId(eventId);
   fEvents.push_back(EventPnt);
   if (eventId > fHighestEventId)
   	fHighestEventId = eventId;
}
//-------------------------------------------------------------------//
bool EventCollection::HasEvent(const unsigned int eventId){
  for (ConstEventIterator iter = EventsBegin(); iter != EventsEnd(); ++iter) {
  	
	if ((*iter)->GetId() == eventId) {
		return true;
	}
  }
  return false;
}
//-------------------------------------------------------------------//
Event *EventCollection::GetEvent(const unsigned int eventId){
  for (ConstEventIterator iter = EventsBegin(); iter != EventsEnd(); ++iter) {
  	if ((*iter)->GetId() == eventId) {
		return (*iter);
	}
  }
  return NULL;
}
//-------------------------------------------------------------------//
bool EventCollection::RemoveEvent(const unsigned int eventId){
  for (EventIterator iter = EventsBegin(); iter != EventsEnd(); ++iter) {
	if ((*iter)->GetId() == eventId) {
		delete((*iter));
		fEvents.erase(iter);
		return true;
	}
  }
  return false;
}
//-------------------------------------------------------------------//
unsigned int EventCollection::GetHighestEventId() const{
  return fHighestEventId;
}
//-------------------------------------------------------------------//
EventCollection::EventIterator EventCollection::EventsBegin(){
  return fEvents.begin();
}
//-------------------------------------------------------------------//
EventCollection::EventIterator EventCollection::EventsEnd(){
  return fEvents.end();
}
//-------------------------------------------------------------------//
EventCollection::ConstEventIterator EventCollection::EventsBegin() const{
  return fEvents.begin();
}
//-------------------------------------------------------------------//
EventCollection::ConstEventIterator EventCollection::EventsEnd() const{
  return fEvents.end();
}
//-------------------------------------------------------------------//
bool EventCollection::IncorporateChannel(Channel *channel){
  // Let's scroll through the events and see if the Channel timestamp
  // is near the existing events
  if (!channel)
  	return false;
  Event *event = NULL;
  for (ConstEventIterator iter = EventsBegin(); iter != EventsEnd(); ++iter) {
	if ( (*iter)->GetTimeRange().isNear(channel->GetTimeStamp(), fGroupTimeInterval) ){
		event = (*iter);
		break;
	}
  }
  if (!event){
  	// Let's Create a new event for this trace, since it's the first one
  	unsigned int new_event_id = GetHighestEventId() + 1;
	MakeEvent(new_event_id);
	event = GetEvent(new_event_id);
  }
  event->IncorporateChannel(channel);
  return true; // means we did incorporated the event
}
//-------------------------------------------------------------------//
void EventCollection::MergeScatteredEvents(void) {
  // cout << "...Checking for scattered events" << endl;
  for (EventIterator event_iter1 = EventsBegin(); event_iter1 !=  EventsEnd(); 
  			++event_iter1) {
	for (EventIterator event_iter2 = EventsBegin(); event_iter2 != EventsEnd();
			++event_iter2) {
		if (		(event_iter1 == event_iter2)||
				((*event_iter1) == NULL) || 
				((*event_iter2) == NULL) )
			continue;
		/* cout << "   comparing  " << (*event_iter1)->GetTimeStart().GetGPSSecond()
		 * 	<< " [" << (*event_iter1)->GetTimeStart().GetGPSNanoSecond()
		 *      << "] and " << (*event_iter2)->GetTimeStart().GetGPSSecond()
		 *	<< " [" << (*event_iter2)->GetTimeStart().GetGPSNanoSecond()
		 *	<< "] within " << fGroupTimeInterval.GetSecond() 
		 *	<< " [" << fGroupTimeInterval.GetNanoSecond() 
		 *	<< "]" << endl;
		 *
		 */
		if ( 	(*event_iter1)->GetTimeRange().isNear( 
				(*event_iter2)->GetTimeStart(), fGroupTimeInterval) ||
			(*event_iter1)->GetTimeRange().isNear( 
				(*event_iter2)->GetTimeEnd(),   fGroupTimeInterval) ){
			cout << "    Events " << (*event_iter1)->GetId() << " and "
				<< (*event_iter2)->GetId() << " should be merged" << endl;
			// Merging job is done here...
			for (Event::ConstChannelIterator ch_iter = (*event_iter2)->ChannelsBegin();
				ch_iter != (*event_iter2)->ChannelsEnd(); ++ch_iter){
				cout << "...Incorporating Channel " << (*ch_iter)->GetId();
				(*event_iter1)->IncorporateChannel((*ch_iter));
				cout << "...done" << endl;
			}
			/// clear only pointers of event2
			(*event_iter2)->ClearChanPointers();
			/// And finally delete the event itself
			delete ((*event_iter2)); 
			(*event_iter2) = NULL;
			/// And Erase of this event will happen later
		}
	}
  }

  for (EventIterator iter =  EventsBegin(); iter !=  EventsEnd(); ++iter) {
	if ((*iter) == NULL) {
		cout << "...Erasing NULL event" << endl;
  		iter = fEvents.erase(iter);
	}
  }
}
//-------------------------------------------------------------------//
