#include "TimeRange.h"
using namespace drift_server;

//-------------------------------------------------------------------//
TimeRange::TimeRange():
		fBegin(0,0),
		fEnd(0,0){
}
//-------------------------------------------------------------------//
TimeRange::TimeRange(TimeStamp &begin, TimeStamp &end){
  SetInterval(begin, end);
}
//-------------------------------------------------------------------//
TimeRange::~TimeRange() {
}
//-------------------------------------------------------------------//
void TimeRange::SetInterval(TimeStamp &begin, TimeStamp &end){
  fBegin = begin;
  fEnd = end;
}
//-------------------------------------------------------------------//
TimeStamp & TimeRange::GetBegin(){
  return fBegin;
}
//-------------------------------------------------------------------//
TimeStamp & TimeRange::GetEnd(){
  return fEnd;
}
//-------------------------------------------------------------------//
bool TimeRange::isWithin(TimeStamp &value){
  return ((value >= fBegin)&&(value <= fEnd));
}
//-------------------------------------------------------------------//
bool TimeRange::isNear(TimeStamp &value, const TimeInterval& delta){
  return ((value >= ( fBegin - delta))&&(value <=(fEnd + delta)));
}
//-------------------------------------------------------------------//
TimeRange TimeRange::operator=(const TimeRange& tmr){
  fBegin = tmr.fBegin;
  fEnd = tmr.fEnd;
  return *this;
}
//-------------------------------------------------------------------//
