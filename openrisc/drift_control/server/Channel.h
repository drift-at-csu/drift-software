#ifndef _DRIFT_Channel_h_
#define _DRIFT_Channel_h_

#include "Trace.h"
#include "TimeStamp.h"

#include <list>

namespace drift_server {
//class Event;

class Channel {
  public:
  	Channel();
	~Channel();

	unsigned int GetId() const;
	void SetId(unsigned int id);

	Trace& GetTrace();

	TimeStamp& GetTimeStamp();

	void PrintInfo();

  private:
	unsigned int fId;
	Trace fRawTrace;
	TimeStamp fTimeStamp;

}; /* class Channel */
} /* namespace drift_server */
#endif /* _DRIFT_Channel_h_ */
