#ifndef _DRIFT_SERVER_Trace_h_
#define _DRIFT_SERVER_Trace_h_

// #include <sys/types.h>
#include <stdint.h>	// for uint32_t

#include <string>
#include <vector>

namespace drift_server {

class Trace {
  public:
  	typedef std::vector<uint32_t> TimeBinsContainer;
	typedef TimeBinsContainer::iterator TimeBinsIterator;
	typedef TimeBinsContainer::const_iterator ConstTimeBinsIterator;

  public:
	Trace();
	~Trace();
	/// This subtract the pedestal from the trace, pedestal time
	/// is defined between ped_time_begin and ped_time_end
	void ShiftPedestalToZero(double ped_time_begin, double ped_time_end);

	///===> Setters and Gettersafter this
	void SetBinDuration(double bin_duration);
	double GetBinDuration() const;
	void SetVerticalScale(double vert_scale);
	double GetVerticalScale() const;

	unsigned int GetNofTimeBins() const;
	uint32_t& GetTimeBin(const unsigned int time_bin);
	const uint32_t& GetTimeBin(const unsigned int time_bin) const;

	TimeBinsIterator TimeBinsBegin();
	TimeBinsIterator TimeBinsEnd();
	ConstTimeBinsIterator TimeBinsBegin() const;
	ConstTimeBinsIterator TimeBinsEnd() const;

	Trace operator=(const Trace& trc);

	void AddTimeBin(uint32_t time_bin_value);
	/// Clears all time bins from Container
	void Clear();

	void PrintInfo();

  private:
	double fTimeBinDuration;	// in nsec
	double fVertScale;		// in mV/ADCs
	TimeBinsContainer fTimeBins;	
}; /* class Trace */

} /* namespace drift_server */
#endif /* _DRIFT_SERVER_Trace_h_ */
