#include "TimeStamp.h"
using namespace drift_server;

// #include <stdio.h>
// #include <stdlib.h>
#include <iostream>
using namespace std;

//-------------------------------------------------------------------//
TimeStamp::TimeStamp():
	fGPSSecond(0), fGPSNanoSecond(0){
}
//-------------------------------------------------------------------//
TimeStamp::TimeStamp(const long aSec, const long aNsec){
   SetNormalized(aSec, aNsec);
}
//-------------------------------------------------------------------//
TimeStamp::~TimeStamp(){
}
//-------------------------------------------------------------------//
void TimeStamp::SetGPSTime(const long aSec, const long aNsec){
  SetNormalized(aSec, aNsec);
}
//-------------------------------------------------------------------//
long TimeStamp::GetGPSSecond() const{
  return fGPSSecond;
}
//-------------------------------------------------------------------//
long TimeStamp::GetGPSNanoSecond() const{
  return fGPSNanoSecond;
}
//-------------------------------------------------------------------//
bool TimeStamp::operator==(const TimeStamp& ts) const{
  return (fGPSSecond == ts.fGPSSecond && fGPSNanoSecond == ts.fGPSNanoSecond);
}
//-------------------------------------------------------------------//
bool TimeStamp::operator!=(const TimeStamp& ts) const{
  return !operator==(ts);
}
//-------------------------------------------------------------------//
bool TimeStamp::operator>(const TimeStamp& ts) const{
  return (fGPSSecond > ts.fGPSSecond ||
	(fGPSSecond == ts.fGPSSecond && fGPSNanoSecond > ts.fGPSNanoSecond));
}
//-------------------------------------------------------------------//
bool TimeStamp::operator>=(const TimeStamp& ts) const{
  return !operator<(ts);
}
//-------------------------------------------------------------------//
bool TimeStamp::operator<(const TimeStamp& ts) const{
  return (fGPSSecond < ts.fGPSSecond ||
	(fGPSSecond == ts.fGPSSecond && fGPSNanoSecond < ts.fGPSNanoSecond));
}
//-------------------------------------------------------------------//
bool TimeStamp::operator<=(const TimeStamp& ts) const{
  return !operator>(ts);
}
//-------------------------------------------------------------------//
TimeStamp TimeStamp::operator=(const TimeStamp& ts){
  SetNormalized( ts.fGPSSecond, ts.fGPSNanoSecond);
  return *this;
}
//-------------------------------------------------------------------//
TimeStamp TimeStamp::operator+(const TimeInterval& ti){
  return TimeStamp(fGPSSecond + ti.GetSecond(),
  	fGPSNanoSecond + ti.GetNanoSecond());
}
//-------------------------------------------------------------------//
TimeStamp& TimeStamp::operator+=(const TimeInterval& ti){
  SetNormalized(fGPSSecond + ti.GetSecond(),
	fGPSNanoSecond + ti.GetNanoSecond());
  return *this;
}
//-------------------------------------------------------------------//
TimeInterval TimeStamp::operator-(const TimeStamp& ts){
  return TimeInterval( fGPSSecond - ts.fGPSSecond,
	fGPSNanoSecond - ts.fGPSNanoSecond);
}
//-------------------------------------------------------------------//
TimeStamp TimeStamp::operator-(const TimeInterval& ti){
  return TimeStamp(fGPSSecond - ti.GetSecond(),
	fGPSNanoSecond - ti.GetNanoSecond());
}
//-------------------------------------------------------------------//
TimeStamp& TimeStamp::operator-=(const TimeInterval& ti){
  SetNormalized(fGPSSecond - ti.GetSecond(),
  	fGPSNanoSecond - ti.GetNanoSecond());
  return *this;
}
//-------------------------------------------------------------------//
void TimeStamp::SetNormalized(long sec, long nsec){
  // normalize
  const long sperns = 1000000000;
  while (nsec < 0){
  	--sec;
	nsec += sperns;
  }

  while (nsec >= sperns){
  	++sec;
	nsec -= sperns;
  }

  if (sec >= 0) {
	fGPSSecond = sec;
        fGPSNanoSecond = nsec;
  /*
  } else {
  	cout << "...TimeStamp Error: " << sec << "," << nsec << endl;
	fGPSSecond = fGPSNanoSecond = 0;
  */
  }
  
}
//-------------------------------------------------------------------//
