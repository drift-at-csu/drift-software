#ifndef _DRIFT_Event_h_
#define _DRIFT_Event_h_

#include <list>
#include <string>
#include <chrono>

#include "TimeStamp.h"
#include "TimeRange.h"

namespace drift_server {
  class Channel;

class Event {
  public:
  	typedef std::list<Channel*> ChannelContainer;
	typedef ChannelContainer::iterator ChannelIterator;
	typedef ChannelContainer::const_iterator ConstChannelIterator;

  public:
  	Event();
	~Event();

	/// Loading a given event from file
	bool LoadFromDRTFile(std::string file_name, unsigned long event_number = 1);
	/// Saving a given event to the file
	bool SaveToDRTFile( std::string file_name );

	unsigned long GetId();
	void SetId(unsigned long id);

	TimeStamp 	&GetTimeStart();
	TimeStamp 	&GetTimeEnd();
	TimeRange 	&GetTimeRange();
	TimeInterval	GetTimeDuration();

	unsigned int GetNofChannels() const;
	void MakeChannel(const unsigned int channelId);
	bool HasChannel(const unsigned int channelId);
	Channel *GetChannel(const unsigned int channelId);
	// This incorporates event that is already created,
	// instead of manually creating it
	bool IncorporateChannel(Channel *channel);

  	ChannelIterator ChannelsBegin();
	ChannelIterator ChannelsEnd();
	ConstChannelIterator ChannelsBegin() const;
	ConstChannelIterator ChannelsEnd() const;

	/// This Function returns Modification time
	std::chrono::system_clock::time_point &GetModTime();
	/// This Function returns the difference in seconds
	/// between now, and when the event was modified
	double GetSecondsSinceMod();

	/// This is special function, which destroys channel pointer
	/// without deleting the channel itself
	/// Use it only if you know, what you are doing
	void ClearChanPointers(void);

  private:
	unsigned long	 fId;
	ChannelContainer fChannels;
	TimeRange	 fTimeRange;
	std::chrono::system_clock::time_point fModTime;
}; /* class Event */
} /* namespace drift */
#endif /* _DRIFT_Event_h_ */
