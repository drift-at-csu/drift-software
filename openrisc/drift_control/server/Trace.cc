#include "Trace.h"
using namespace drift_server;

#include <iostream>
#include <math.h>
#include <algorithm>
using namespace std;

//-------------------------------------------------------------------//
Trace::Trace():
	fTimeBinDuration(0.0),
	fVertScale(0.0) {
}
//-------------------------------------------------------------------//
Trace::~Trace(){
  Clear();
}
//-------------------------------------------------------------------//
void Trace::SetBinDuration(double bin_duration){
  fTimeBinDuration = bin_duration;
}
//-------------------------------------------------------------------//
double Trace::GetBinDuration() const {
  return fTimeBinDuration;
}
//-------------------------------------------------------------------//
void Trace::SetVerticalScale(double vert_scale){
  fVertScale = vert_scale;
}
//-------------------------------------------------------------------//
double Trace::GetVerticalScale() const {
  return fVertScale;
}
//-------------------------------------------------------------------//
unsigned int Trace::GetNofTimeBins() const{
  return fTimeBins.size();
}
//-------------------------------------------------------------------//
uint32_t& Trace::GetTimeBin(const unsigned int time_bin){
  return fTimeBins[time_bin];
}
//-------------------------------------------------------------------//
const uint32_t& Trace::GetTimeBin(const unsigned int time_bin) const{
  return fTimeBins[time_bin];
}
//-------------------------------------------------------------------//
Trace::TimeBinsIterator Trace::TimeBinsBegin(){
  return fTimeBins.begin();
}
//-------------------------------------------------------------------//
Trace::TimeBinsIterator Trace::TimeBinsEnd(){
  return fTimeBins.end();
}
//-------------------------------------------------------------------//
Trace::ConstTimeBinsIterator Trace::TimeBinsBegin() const{
  return fTimeBins.begin();
}
//-------------------------------------------------------------------//
Trace::ConstTimeBinsIterator Trace::TimeBinsEnd() const{
  return fTimeBins.end();
}
//-------------------------------------------------------------------//
Trace Trace::operator=(const Trace& trc){
  Clear();
  fTimeBins = trc.fTimeBins;
  fTimeBinDuration = trc.fTimeBinDuration;
  return *this;
}
//-------------------------------------------------------------------//
void Trace::AddTimeBin(uint32_t time_bin_value){
  fTimeBins.push_back(time_bin_value);
}
//-------------------------------------------------------------------//
void Trace::Clear(){
  fTimeBins.clear();
}
//-------------------------------------------------------------------//
void Trace::PrintInfo(){
  cout << "[ ";
  for (unsigned long ii = 0; ii < GetNofTimeBins(); ++ii){
  	cout << fTimeBins[ii] << " ";
  }
  cout << "]" << endl;
}
//-------------------------------------------------------------------//
