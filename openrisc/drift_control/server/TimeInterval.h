#ifndef _DRIFT_SERVER_TimeInterval_h_
#define _DRIFT_SERVER_TimeInterval_h_

namespace drift_server {
class TimeInterval {
  public:
	TimeInterval();
	TimeInterval(const long aSeconds, const long aNanoSec = 0);
	TimeInterval(const TimeInterval& aTimeInterval);
	~TimeInterval();

	long GetSecond() const;
	long GetNanoSecond() const;

  private:
  	long fSeconds; // The interval in seconds
	long fNanoSeconds; // nanoseconds part

}; /* class TimeInterval */
} /* namespace drift_server */
#endif /* _DRIFT_SERVER_TimeInterval_h_ */
