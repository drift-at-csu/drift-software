/*
 * A simple sysfs interface for the generic FADC framework
 */
#include <linux/device.h>
#include <linux/err.h>
#include <linux/slab.h>
#include <linux/kdev_t.h>

#include "fadc.h"

//-------------------------------------------------------------------//
static ssize_t fadc_controller_trigger_show(
	struct device *parent,
	struct device_attribute *attr,
	char *buf) {
  // const struct fadc_channel *channel = dev_get_drvdata(parent);
  return sprintf(buf, "Controller trigger show is not implemented\n");
}
static ssize_t fadc_controller_trigger_store(
	struct device *parent,
	struct device_attribute *attr,
	const char *buf, size_t size) {
  // const struct fadc_channel *channel = dev_get_drvdata(parent);
  unsigned int val;
  int ret;
  unsigned long nof_channels;
  ret = kstrtouint(buf, 0, &val);
  if (ret)
	return ret;
  nof_channels = fadc_channels_trigger_all();
  printk("Controller triggered %lu channels\n",
  	nof_channels );
  return ret ? : size;
}
static DEVICE_ATTR(trigger, 0644,
	fadc_controller_trigger_show, fadc_controller_trigger_store);
//-------------------------------------------------------------------//
static ssize_t fadc_controller_status_show(
        struct device *parent,
        struct device_attribute *attr,
        char *buf) {
  // const struct fadc_channel *channel = dev_get_drvdata(parent);
 
  return sprintf(buf, "NofCHANs=%d\n", fadc_channel_get_total());
}
static ssize_t fadc_controller_status_store(
        struct device *parent,
        struct device_attribute *attr,
        const char *buf, size_t size) {
  // const struct fadc_channel *channel = dev_get_drvdata(parent);
  unsigned int val;
  int ret;
  ret = kstrtouint(buf, 0, &val);
  if (ret)
	return ret;
  printk("Controller status store is not implemented\n");
  return ret ? : size;
}
static DEVICE_ATTR(status, S_IRUGO,
	fadc_controller_status_show, fadc_controller_status_store);
//-------------------------------------------------------------------//
static ssize_t fadc_controller_timesync_show(
	struct device *parent,
	struct device_attribute *attr,
	char *buf) {
  return sprintf(buf, "Show timesync is not implemented\n");
}
static ssize_t fadc_controller_timesync_store(
	struct device *parent,
	struct device_attribute *attr,
	const char *buf, size_t size) {
  unsigned int val;
  int ret;
  unsigned long nof_chips;
  ret = kstrtouint(buf, 0, &val);
  if (ret)
	return ret;
  nof_chips = fadc_chips_timesync(val);  
  printk("Controller timesync %lu chips with %u timestamp\n",
  	nof_chips, val);
  return ret ? : size;
}
static DEVICE_ATTR(timesync, 0644,
        fadc_controller_timesync_show, fadc_controller_timesync_store);
//-------------------------------------------------------------------//
static struct attribute *fadc_controller_attrs[] = {
	&dev_attr_status.attr,
	&dev_attr_trigger.attr,
	&dev_attr_timesync.attr,
	//---> Add More DEVICE_ATTR here
	NULL
};
ATTRIBUTE_GROUPS(fadc_controller);
//-------------------------------------------------------------------//
static struct class fadc_controller_class = {
	.name   = "fadc_controller",
	.owner  = THIS_MODULE,
	.dev_groups     = fadc_controller_groups,
};
//-------------------------------------------------------------------//
struct class *fadc_controller_get_class(void){
  return &fadc_controller_class;
}
EXPORT_SYMBOL_GPL(fadc_controller_get_class);
//-------------------------------------------------------------------//
static int __init fadc_controller_sysfs_init(void) {
  return class_register(&fadc_controller_class);
}
subsys_initcall(fadc_controller_sysfs_init);
//-------------------------------------------------------------------//
