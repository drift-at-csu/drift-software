#include <linux/init.h>
#include <linux/module.h>
#include <linux/device.h>	// for device_create
#include <linux/slab.h>		// for kzalloc and kfree
#include <linux/fs.h>
#include <asm/uaccess.h>	/* for put_user */
#include <linux/cdev.h>		// For Character device operations

#include <linux/mm.h>		// for file virtual memory operations 
#include <linux/poll.h>		// for poll operations
#include <linux/io.h>		// for memcpy_fromio

#include "fadc.h"

// Information about the file status
enum {
	FADC_DEVICE_FILE_OPENED		= 1 << 0,
	FADC_DEVICE_FILE_NONBLOCK	= 1 << 1,
};
struct fadc_device{
	unsigned long 	file_status;
	struct fadc_buffer *current_buffer;
	dev_t		devt;	// Major and minor number given to us
	struct cdev 	cdev;  // for char device
};
static struct fadc_device *fadc_device_info;
#define FADC_DEVICE_MINOR	123	// This is to minimize the probability
					// of unintended operations

//===================================================================//
// File Operations
static ssize_t fadc_device_read(struct file *filp, char __user *buf, 
			size_t count, loff_t *pos) {
  struct fadc_device *dev = filp->private_data;
  // struct fadc_buffer *buffer;
  ssize_t bytes;

  if ( dev->current_buffer ) {
  	// There is a current buffer. Check if we 
	// have it triggered.
	if (test_bit(FADC_BUFFER_TRIGGERED, &dev->current_buffer->flags)){
		// The buffer is still triggered
		// buffer = dev->current_buffer;
	} else {
		// The buffer is not triggered (released), let's try to find another one
		dev->current_buffer = fadc_buffer_find_next_triggered(dev->current_buffer);
		(*pos) = 0;
	}
  } else {
  	// Current Buffer is NULL, let's try to find it
	dev->current_buffer = fadc_buffer_find_next_triggered(dev->current_buffer);
	(*pos) = 0;
  }

  if ( !dev->current_buffer ){
  	// If the Buffer is NULL (No triggered Buffers), let's return 0 here
	(*pos) = 0;
  	return 0;
  }

  // printk("...dev-current_buffer=%p\n", dev->current_buffer);
  // If it's not 0 then fadc_device_info->current buffer is already marked as triggered
  bytes = count < (dev->current_buffer->size - (*pos)) ? 
  	count : (dev->current_buffer->size - (*pos));
  if (bytes == 0) {
  	return 0;
  }
  if ( !access_ok(VERIFY_WRITE, buf, bytes)){
	return -EINVAL;
  }

  memcpy_fromio(buf, 
  	(u32 *) (dev->current_buffer->iosaddr + 28 + (*pos)), bytes);
  // retval = copy_to_user(buf, (u32 *) (buffer->iosaddr + (*pos)), bytes);
  (*pos) += bytes;
  // We read the buffer completely, let's release it
  // clear_bit(FADC_BUFFER_TRIGGERED, &dev->current_buffer->flags);
  // (*pos) = 0;
  
  return bytes;
}
//-------------------------------------------------------------------//
static ssize_t fadc_device_write(struct file *filp, const char __user *buf, 
			size_t count, loff_t *pos) {
  struct fadc_device *dev = filp->private_data;
  struct fadc_buffer *buffer = dev->current_buffer;

  if (!buffer) {
  	return -EINVAL;
  }
  clear_bit(FADC_BUFFER_TRIGGERED, &buffer->flags);
  // (*pos) = 0;
  return count;
}
//-------------------------------------------------------------------//
static int fadc_device_open(struct inode *inode, struct file *filp) {
  // struct fadc_buffer *buffer=NULL;
  if (fadc_device_info->file_status & FADC_DEVICE_FILE_OPENED) {
  	/* Some process has already opened this file. */
	return -EBUSY;
  }
  filp->private_data = fadc_device_info;
  fadc_device_info->file_status |= FADC_DEVICE_FILE_OPENED;

  fadc_buffer_mutex_lock();
  // We need to go through the list and assign the initial buffer
  // fadc_device_info->current_buffer = fadc_buffer_get_first_in_list();
  fadc_device_info->current_buffer = NULL;

  // Let's also renumber the buffers:
  fadc_buffer_renumber_listed(0);
  // Unlock is happening when the file is closed.

  return nonseekable_open(inode, filp);
}
//-------------------------------------------------------------------//
static int fadc_device_release(struct inode *inode, struct file *filp) {
  struct fadc_device *dev = filp->private_data;

  fadc_buffer_mutex_unlock();
  dev->file_status &= ~FADC_DEVICE_FILE_OPENED;
  return 0;
}
//-------------------------------------------------------------------//
void fadc_device_mmap_open(struct vm_area_struct *vma) {
  // struct fadc_device *dev = (struct fadc_device *)vma->vm_private_data;
  // dev->reference++;
}
//-------------------------------------------------------------------//
void fadc_device_mmap_close(struct vm_area_struct *vma) {
  // struct fadc_device *dev = (struct fadc_device *)vma->vm_private_data;
  // dev->reference--;
}
//-------------------------------------------------------------------//
static int fadc_device_mmap_fault(struct vm_area_struct *vma, 
			struct vm_fault *vmf) {
  struct page *page;
  struct fadc_device *dev = (struct fadc_device *)vma->vm_private_data;

  /* the data is in vma->vm_private_data */
  dev = (struct fadc_device *)vma->vm_private_data;
  if (!dev->current_buffer) {
	printk("no data\n");
	return 0;	
  }
  /* get the page */
  page = virt_to_page(dev->current_buffer->iosaddr);
  /* increment the reference count of this page */
  get_page(page);
  vmf->page = page;
  printk("mmap_fault @ 0x%p\n", dev->current_buffer->iosaddr);
  return 0;
}
//-------------------------------------------------------------------//
struct vm_operations_struct fadc_device_mmap_vm_ops = {
	.open =		fadc_device_mmap_open,
	.close =	fadc_device_mmap_close,
	.fault =	fadc_device_mmap_fault,
};
//-------------------------------------------------------------------//
static int fadc_device_mmap(struct file *filp, 
				struct vm_area_struct *vma) { 
  struct fadc_device *dev = filp->private_data;
  unsigned long off, physical, vsize, psize;
  struct fadc_buffer *buffer = dev->current_buffer;

  if (!buffer){
  	printk("no buffer\n");
	return -EINVAL;
  }

  off = vma->vm_pgoff << PAGE_SHIFT;
  // We want to subtract header
  physical = buffer->phyaddr + off - 32;
  vsize = vma->vm_end - vma->vm_start;
  psize = buffer->size - off;

  if (vsize > psize) {
  	printk("error: off=0x%08lx,phys=0x%08lx, vsize=0x%08lx, psize=0x%08lx\n",
		off, physical, vsize, psize);
	return -EINVAL; /*  spans too high */
  }

  /*
  printk("fadc_device_mmap 0x%08lx to 0x%08lx \n",
			vma->vm_start,
			physical );
  */
  vma->vm_page_prot = pgprot_noncached(vma->vm_page_prot);
  vma->vm_flags |= VM_DONTEXPAND | VM_DONTDUMP | VM_IO;
  vma->vm_private_data = filp->private_data;
  fadc_device_mmap_open(vma);
  /*
  if ( io_remap_pfn_range(vma, 
  		vma->vm_start, physical >> PAGE_SHIFT,
		vsize, vma->vm_page_prot) )

	return -EAGAIN;
  */
 
  return io_remap_pfn_range(vma, 
  		vma->vm_start, physical >> PAGE_SHIFT, 
		vsize, vma->vm_page_prot);
}
//-------------------------------------------------------------------//
static const struct file_operations fadc_device_fops = {
	.owner = THIS_MODULE,

	.open = 	fadc_device_open,
	.release = 	fadc_device_release,
	.read = 	fadc_device_read,
	.write = 	fadc_device_write,

	.mmap = 	fadc_device_mmap, // This is new
};
//--------------------------------------------------------------------//
static int fadc_device_init(void) {
  struct class *fadc_controller_class_p = fadc_controller_get_class();

  fadc_device_info = kzalloc(sizeof(*fadc_device_info), GFP_KERNEL);
  if (!fadc_device_info) {
  	printk("Can't allocate memory for device info\n");
  }
  // Allocate char device (should show up in /proc/devices
  if (alloc_chrdev_region( &fadc_device_info->devt, 
  		FADC_DEVICE_MINOR, 1, "fadc_dev") < 0){
	printk("Can't allocate Major Number\n");
	kfree(fadc_device_info); fadc_device_info = NULL;
	return -1;
  }
  // Create the device (should show up in /dev
  if (device_create(fadc_controller_class_p, NULL, 
  		fadc_device_info->devt, NULL, "fadc") == NULL){ //in /dev
	printk("Can't create device\n");
	unregister_chrdev_region(fadc_device_info->devt, 1);
	kfree(fadc_device_info); fadc_device_info = NULL;
	return -1;
  }

  cdev_init(&fadc_device_info->cdev, &fadc_device_fops);
  if (cdev_add(&fadc_device_info->cdev, fadc_device_info->devt, 1) == -1) {
  	printk("Can't add char device\n");
	device_destroy(fadc_controller_class_p, fadc_device_info->devt);
	unregister_chrdev_region(fadc_device_info->devt, 1);
	kfree(fadc_device_info); fadc_device_info = NULL;
	return -1;
  }
  printk("fadc_device_init: Complete\n");
  return 0;
}
//-------------------------------------------------------------------//
static void fadc_device_exit(void) {
  printk("fadc_device_exit\n");
  unregister_chrdev_region(fadc_device_info->devt, 1);
  kfree(fadc_device_info); fadc_device_info = NULL;
}
//-------------------------------------------------------------------//
module_init(fadc_device_init);
module_exit(fadc_device_exit);

MODULE_AUTHOR("Alexei Dorofeev");
MODULE_DESCRIPTION("Flash ADC Buffers Management");
MODULE_LICENSE("GPL");
