#include <linux/io.h>
#include <linux/platform_device.h>
#include <linux/of.h>
#include <linux/module.h>

#include <linux/interrupt.h>
#include <linux/delay.h>
#include <linux/slab.h>

#include "fadc.h"

//-----------> HARDWARE REGISTERS Definition
#define ADCLVDS_REG_CONTROL	0x000
#define ADCLVDS_REG_CONTROL_POWADC      0x01000000
#define ADCLVDS_REG_CONTROL_POWGR1      0x02000000
#define ADCLVDS_REG_CONTROL_POWGR2      0x04000000
#define ADCLVDS_REG_CONTROL_POWGR3      0x08000000
#define ADCLVDS_REG_CONTROL_POWGR4      0x10000000

#define ADCLVDS_REG_PHYSTAT	0x004
#define ADCLVDS_REG_PHYSTAT_RESET	0x20000000
#define ADCLVDS_REG_PHYSTAT_BITSLIP	0x10000000
#define ADCLVDS_REG_PHYSTAT_DELAYINC	0x00001000

#define ADCLVDS_REG_TRGR	0x008
#define ADCLVDS_REG_TRGR_CHAN1		0x00000001
#define ADCLVDS_REG_TRGR_CHAN2		0x00000002
#define ADCLVDS_REG_TRGR_CHAN3		0x00000004
#define ADCLVDS_REG_TRGR_CHAN4		0x00000008
#define ADCLVDS_REG_TRGR_CHAN5		0x00000010
#define ADCLVDS_REG_TRGR_CHAN6		0x00000020
#define ADCLVDS_REG_TRGR_CHAN7		0x00000040
#define ADCLVDS_REG_TRGR_CHAN8		0x00000080

#define ADCLVDS_REG_SPI		0x1F0
#define ADCLVDS_REG_SPI_READY		0x20000000

#define ADCLVDS_REG_SECR        0x1F4   /* Seconds Register (32 bit) */
#define ADCLVDS_REG_SECR_RESET		0x80000000

#define ADCLVDS_REG_QNTR        0x1F8   /* Quanta Register (32 bit) RO */

#define ADCLVDS_REG_IRQR	0x1FC	/* Interrupt Register */
#define ADCLVDS_REG_IRQR_ENA_CHAN1	0x00000001
#define ADCLVDS_REG_IRQR_ENA_CHAN2	0x00000002
#define ADCLVDS_REG_IRQR_ENA_CHAN3	0x00000004
#define ADCLVDS_REG_IRQR_ENA_CHAN4	0x00000008
#define ADCLVDS_REG_IRQR_ENA_CHAN5	0x00000010
#define ADCLVDS_REG_IRQR_ENA_CHAN6	0x00000020
#define ADCLVDS_REG_IRQR_ENA_CHAN7	0x00000040
#define ADCLVDS_REG_IRQR_ENA_CHAN8	0x00000080

#define ADCLVDS_REG_IRQR_PEN_CHAN1	0x00000100
#define ADCLVDS_REG_IRQR_PEN_CHAN2	0x00000200
#define ADCLVDS_REG_IRQR_PEN_CHAN3	0x00000400
#define ADCLVDS_REG_IRQR_PEN_CHAN4	0x00000800
#define ADCLVDS_REG_IRQR_PEN_CHAN5	0x00001000
#define ADCLVDS_REG_IRQR_PEN_CHAN6	0x00002000
#define ADCLVDS_REG_IRQR_PEN_CHAN7	0x00004000
#define ADCLVDS_REG_IRQR_PEN_CHAN8	0x00008000

#define ADCLVDS_ADDR_CH1	0x010	/* Base Addresses for all channels */
#define ADCLVDS_ADDR_CH2	0x030
#define ADCLVDS_ADDR_CH3	0x050
#define ADCLVDS_ADDR_CH4	0x070
#define ADCLVDS_ADDR_CH5	0x090
#define ADCLVDS_ADDR_CH6	0x0B0
#define ADCLVDS_ADDR_CH7	0x0D0
#define ADCLVDS_ADDR_CH8	0x0F0

#define ADCLVDS_REG_CH_TLOWR    0x00     /* Ch1 Triger Low   */
#define ADCLVDS_REG_CH_THIGR    0x04     /* Ch1 Trigger High */
#define ADCLVDS_REG_CH_ADDRR    0x08     /* Ch1 Address Register */
#define ADCLVDS_REG_CH_AVRGR    0x0C     /* Ch1 Average Control Register */
#define ADCLVDS_REG_CH_PEDSR    0x10     /* Ch1 Pedestal Register */

// Forward Declarations
// static struct platform_driver adclvds_driver;

/**
 * struct adclvds - driver-private device structure
 * @iobase:     	pointer to I/O memory region ( Registers )
 * @io_region_size:	The size of Register (512 Bytes)
 * @membase:    pointer to buffer memory region
 */
struct adclvds_device {
	void __iomem *io_base;
	resource_size_t io_size;

	void __iomem *buf_base;
	resource_size_t buf_size;

	int	irq;
};
//===================================================================//
// Hardware Functions
static inline u32 adclvds_io_read(struct adclvds_device *dev, loff_t offset) {
#ifdef CONFIG_WISHBONE_BUS_BIG_ENDIAN
	return ioread32be(dev->io_base + offset);
#else
	return ioread32(dev->io_base + offset);
#endif
}
//-------------------------------------------------------------------//
static inline void adclvds_io_write(struct adclvds_device *dev, loff_t offset, u32 data) {
#ifdef CONFIG_WISHBONE_BUS_BIG_ENDIAN
	iowrite32be(data, dev->io_base + offset);
#else
	iowrite32(data, dev->io_base + offset);
#endif
}
//-------------------------------------------------------------------//
static inline u32 adclvds_buf_read(void * addr) {
#ifdef CONFIG_WISHBONE_BUS_BIG_ENDIAN
	return ioread32be((void __iomem *) addr);
#else
	return ioread32( (void __iomem *) addr);
#endif
}
//-------------------------------------------------------------------//
static inline void adclvds_buf_write(void *addr, u32 data) {
#ifdef CONFIG_WISHBONE_BUS_BIG_ENDIAN
	iowrite32be(data, (void __iomem *) addr);
#else
	iowrite32(data, (void __iomem *)addr);
#endif
}
//-------------------------------------------------------------------//
static int adclvds_spi_write (struct adclvds_device *dev, u32 addr, u32 data){
  u32 wb_data = ((addr & 0x7FFF) << 16 ) | (data & 0xFFFF);
  int timeout = 100;
  adclvds_io_write(dev, ADCLVDS_REG_SPI, wb_data);
  do {
  	udelay(1);
	wb_data = adclvds_io_read(dev, ADCLVDS_REG_SPI);
	timeout = timeout - 1;
  } while ((!(wb_data & ADCLVDS_REG_SPI_READY))&&(timeout > 0));

  return timeout ? 0 : -1;
}
//-------------------------------------------------------------------//
static inline u32 adclvds_spi_read( struct adclvds_device *dev, u32 addr){
  u32 wb_data = 0x80000000 | ((addr & 0x7FFF) << 16 );
  int timeout = 100;
  adclvds_io_write(dev, ADCLVDS_REG_SPI, wb_data);
  do {
  	udelay(1);
	wb_data = adclvds_io_read(dev, ADCLVDS_REG_SPI);
	timeout = timeout - 1;
  } while ((!(wb_data & ADCLVDS_REG_SPI_READY))&&(timeout > 0));
  wb_data &= 0xFFFF;
  return wb_data;
}
//-------------------------------------------------------------------//
static u32 adclvds_phy_issue_bitslip(struct adclvds_device *dev) {
  u32 wb_data = ADCLVDS_REG_PHYSTAT_BITSLIP;
  adclvds_io_write(dev, ADCLVDS_REG_PHYSTAT, wb_data);
  wb_data = adclvds_io_read(dev, ADCLVDS_REG_PHYSTAT);
  return ((wb_data & 0x0FFF0000) >> 16);
}
//-------------------------------------------------------------------//
static u32 adclvds_phy_issue_delayinc(struct adclvds_device *dev){
  u32 wb_data = ADCLVDS_REG_PHYSTAT_DELAYINC;
  adclvds_io_write(dev, ADCLVDS_REG_PHYSTAT, wb_data);
  wb_data = adclvds_io_read(dev, ADCLVDS_REG_PHYSTAT);
  return ((wb_data & 0x0FFF0000) >> 16);
}
//-------------------------------------------------------------------//
static void adclvds_phy_move_fco_eye(struct adclvds_device *dev){
  int delay_tap_start = 33;
  int delay_tap_end = 33;
  int right_pattern_observed = 1;
  int ii;
  u32 fco_pattern;
  for (ii = 0; ii < 32; ++ii) {
	fco_pattern = adclvds_phy_issue_delayinc(dev);
	if (fco_pattern == 0xFC0) {
		// At this tap the righ pattern observed
		if (right_pattern_observed == 0) {
			delay_tap_start = ii;
		}
		right_pattern_observed = 1;
	} else {
		// At this tap the wrong pattern observed
		if (right_pattern_observed == 1) {
			// We're switching from right pattern to wrong pattern
			delay_tap_end = ii;
		}
		right_pattern_observed = 0;
	}
  }
  if (delay_tap_start > delay_tap_end) {
	// The eye is on the outside borders
	right_pattern_observed = (delay_tap_start + (delay_tap_end + 32))/2;
	right_pattern_observed = right_pattern_observed % 32;
  } else {
	// The eye is inside
	right_pattern_observed = (delay_tap_start + delay_tap_end)/2;
  }
  printk("   ADC PHY Delay: The Eye is [%d, %d] = %d\n",
	delay_tap_start, delay_tap_end, right_pattern_observed);
  for (ii = 0; ii < right_pattern_observed; ++ii) {
	fco_pattern = adclvds_phy_issue_delayinc(dev);
  }
}
//-------------------------------------------------------------------//
static void adclvds_hw_chip_enable( struct adclvds_device *dev ) {
  u32 control_reg = adclvds_io_read(dev, ADCLVDS_REG_CONTROL);
  // Let's Make sure that all the channels are Off:
  control_reg &= ~(     ADCLVDS_REG_CONTROL_POWGR1 | ADCLVDS_REG_CONTROL_POWGR2 |
  			ADCLVDS_REG_CONTROL_POWGR3 | ADCLVDS_REG_CONTROL_POWGR4 );
  control_reg |= ADCLVDS_REG_CONTROL_POWADC;
  adclvds_io_write(dev, ADCLVDS_REG_CONTROL, control_reg);
}
//-------------------------------------------------------------------//
static void adclvds_hw_chip_disable( struct adclvds_device *dev ) {
  u32 control_reg = adclvds_io_read(dev, ADCLVDS_REG_CONTROL);
  // Make sure we would also power-off channels ( just in case )
  control_reg &= ~(	ADCLVDS_REG_CONTROL_POWGR1 | ADCLVDS_REG_CONTROL_POWGR2 |
  			ADCLVDS_REG_CONTROL_POWGR3 | ADCLVDS_REG_CONTROL_POWGR4 |
			ADCLVDS_REG_CONTROL_POWADC );
  adclvds_io_write(dev, ADCLVDS_REG_CONTROL, control_reg);
}
//-------------------------------------------------------------------//
static void adclvds_hw_chip_calibrate( struct adclvds_device *dev ) {
  u32 control_reg;
  u32 data;
  int ii;
  u32 fco_pattern;
  int timeout = 100;

  control_reg = adclvds_io_read(dev, ADCLVDS_REG_CONTROL);
  // Let's make sure that all the channels are off
  control_reg &= ~(	ADCLVDS_REG_CONTROL_POWGR1 | ADCLVDS_REG_CONTROL_POWGR2 |
			ADCLVDS_REG_CONTROL_POWGR3 | ADCLVDS_REG_CONTROL_POWGR4 |
			ADCLVDS_REG_CONTROL_POWADC );
  timeout = 100;
  do {
	// Turn Off the ADC (if it was Off, nothing will happen
	control_reg &= ~ADCLVDS_REG_CONTROL_POWADC;
	adclvds_io_write(dev, ADCLVDS_REG_CONTROL, control_reg);
	// we want to wait for ADC to really go down
	msleep_interruptible(1); // sleep for 1 milisec
	// Turn On the ADC
	control_reg |= ADCLVDS_REG_CONTROL_POWADC;
	adclvds_io_write(dev, ADCLVDS_REG_CONTROL, control_reg);
	msleep_interruptible(1); // sleep for 1 milisec
	// Read ADC ID From SPI
	data = adclvds_spi_read(dev, 0x0002);
	printk("...SPI ID of the ADC is %04x\n", data);
	// Put ADC in binary mode
	adclvds_spi_write(dev, 0x0014, 0x0000);
	// Issue a reset for PHY On the PLD
	adclvds_io_write(dev, ADCLVDS_REG_PHYSTAT, ADCLVDS_REG_PHYSTAT_RESET);
	// Put a 0x800 pattern on adc
	adclvds_spi_write(dev, 0x000D, 0x0100);
	// Issue a bitslip
	for (ii = 0; ii < 6; ++ii) {
		fco_pattern = adclvds_phy_issue_bitslip(dev);
		if ( (fco_pattern&0xFFF) == 0xFC0 )
			break;
	}
	timeout = timeout - 1;
	printk("...PHY: FCO pattern = 0x%x\n", fco_pattern&0xFFF);
  } while ( ((fco_pattern&0xFFF) != 0xFC0 )&&(timeout > 0) );
  // let's move the data into the eye:
  adclvds_phy_move_fco_eye(dev);
  // Switch to normal operation of the ADC
  adclvds_spi_write(dev, 0x000D, 0x0000);
}
//-------------------------------------------------------------------//
static void adclvds_hw_chip_timesync( struct adclvds_device *dev,
					unsigned long timesec){
  // Timesec is not used for current PLD code version
  adclvds_io_write(dev, ADCLVDS_REG_SECR, 0);
  adclvds_io_write(dev, ADCLVDS_REG_SECR, ADCLVDS_REG_SECR_RESET);
  // Reading resets the time reset
  adclvds_io_read(dev, ADCLVDS_REG_SECR);
}
//-------------------------------------------------------------------//
// Turns On the Power Group, corresponding to a certain channel
static void adclvds_hw_channel_enable( 	struct adclvds_device *dev, 
					unsigned long hwid ) {
  u32 control_reg = adclvds_io_read(dev, ADCLVDS_REG_CONTROL);
  if 		((hwid == 0) || (hwid == 1)) {
	control_reg |= ADCLVDS_REG_CONTROL_POWGR1;
  } else if 	((hwid == 2) || (hwid == 3)) {
	control_reg |= ADCLVDS_REG_CONTROL_POWGR2;
  } else if 	((hwid == 4) || (hwid == 5)) {
	control_reg |= ADCLVDS_REG_CONTROL_POWGR3;
  } else if     ((hwid == 6) || (hwid == 7)) {
	control_reg |= ADCLVDS_REG_CONTROL_POWGR4;
  } 
  adclvds_io_write(dev, ADCLVDS_REG_CONTROL, control_reg);
}
//-------------------------------------------------------------------//
static void adclvds_hw_channel_disable(	struct adclvds_device *dev,
					unsigned long hwid ) {
  u32 control_reg = adclvds_io_read(dev, ADCLVDS_REG_CONTROL);
  if            ((hwid == 0) || (hwid == 1)) {
	control_reg &= ~ADCLVDS_REG_CONTROL_POWGR1;
  } else if     ((hwid == 2) || (hwid == 3)) {
	control_reg &= ~ADCLVDS_REG_CONTROL_POWGR2;
  } else if     ((hwid == 4) || (hwid == 5)) {
	control_reg &= ~ADCLVDS_REG_CONTROL_POWGR3;
  } else if     ((hwid == 6) || (hwid == 7)) {
	control_reg &= ~ADCLVDS_REG_CONTROL_POWGR4;
  }
  adclvds_io_write(dev, ADCLVDS_REG_CONTROL, control_reg);
}
//-------------------------------------------------------------------//
static void adclvds_hw_channel_trigger( struct adclvds_device *dev,
					unsigned long hwid) {
  u32 trigger_reg = adclvds_io_read(dev, ADCLVDS_REG_TRGR);
  u32 bit_to_toggle = 0;
  if (hwid == 0){
	bit_to_toggle = ADCLVDS_REG_TRGR_CHAN1; 
  } else if (hwid == 1){
  	bit_to_toggle = ADCLVDS_REG_TRGR_CHAN2;
  } else if (hwid == 2){
	bit_to_toggle = ADCLVDS_REG_TRGR_CHAN3;
  } else if (hwid == 3){
	bit_to_toggle = ADCLVDS_REG_TRGR_CHAN4;
  } else if (hwid == 4){
	bit_to_toggle = ADCLVDS_REG_TRGR_CHAN5;
  } else if (hwid == 5){
	bit_to_toggle = ADCLVDS_REG_TRGR_CHAN6;
  } else if (hwid == 6){
	bit_to_toggle = ADCLVDS_REG_TRGR_CHAN7;
  } else if (hwid == 7){
	bit_to_toggle = ADCLVDS_REG_TRGR_CHAN8;
  }
  // Now, let's toggle this bit
  trigger_reg &= ~bit_to_toggle; // clear it (just to be sure)
  adclvds_io_write(dev, ADCLVDS_REG_TRGR, trigger_reg);
  trigger_reg |= bit_to_toggle; // set it
  adclvds_io_write(dev, ADCLVDS_REG_TRGR, trigger_reg);
  trigger_reg &= ~bit_to_toggle; // clear it again
  adclvds_io_write(dev, ADCLVDS_REG_TRGR, trigger_reg);
}
//-------------------------------------------------------------------//
static void adclvds_hw_channel_write_low_threshold_reg(
                                        struct adclvds_device *dev,
                                        unsigned long hwid,
                                        u32 value ){
  if (hwid == 0){
	adclvds_io_write(dev, ADCLVDS_ADDR_CH1 + ADCLVDS_REG_CH_TLOWR, value);
  } else if (hwid == 1){
	adclvds_io_write(dev, ADCLVDS_ADDR_CH2 + ADCLVDS_REG_CH_TLOWR, value);
  } else if (hwid == 2){
	adclvds_io_write(dev, ADCLVDS_ADDR_CH3 + ADCLVDS_REG_CH_TLOWR, value);
  } else if (hwid == 3){
	adclvds_io_write(dev, ADCLVDS_ADDR_CH4 + ADCLVDS_REG_CH_TLOWR, value);
  } else if (hwid == 4){
	adclvds_io_write(dev, ADCLVDS_ADDR_CH5 + ADCLVDS_REG_CH_TLOWR, value);
  } else if (hwid == 5){
	adclvds_io_write(dev, ADCLVDS_ADDR_CH6 + ADCLVDS_REG_CH_TLOWR, value);
  } else if (hwid == 6){
	adclvds_io_write(dev, ADCLVDS_ADDR_CH7 + ADCLVDS_REG_CH_TLOWR, value);
  } else if (hwid == 7){
	adclvds_io_write(dev, ADCLVDS_ADDR_CH8 + ADCLVDS_REG_CH_TLOWR, value);
  }
}
//-------------------------------------------------------------------//
static u32 adclvds_hw_channel_read_low_threshold_reg(
                                        struct adclvds_device *dev,
                                        unsigned long hwid){
  if (hwid == 0){
	return adclvds_io_read(dev, ADCLVDS_ADDR_CH1 + ADCLVDS_REG_CH_TLOWR);
  } else if (hwid == 1){
	return adclvds_io_read(dev, ADCLVDS_ADDR_CH2 + ADCLVDS_REG_CH_TLOWR);
  } else if (hwid == 2){
	return adclvds_io_read(dev, ADCLVDS_ADDR_CH3 + ADCLVDS_REG_CH_TLOWR);
  } else if (hwid == 3){
	return adclvds_io_read(dev, ADCLVDS_ADDR_CH4 + ADCLVDS_REG_CH_TLOWR);
  } else if (hwid == 4){
	return adclvds_io_read(dev, ADCLVDS_ADDR_CH5 + ADCLVDS_REG_CH_TLOWR);
  } else if (hwid == 5){
	return adclvds_io_read(dev, ADCLVDS_ADDR_CH6 + ADCLVDS_REG_CH_TLOWR);
  } else if (hwid == 6){
	return adclvds_io_read(dev, ADCLVDS_ADDR_CH7 + ADCLVDS_REG_CH_TLOWR);
  } else if (hwid == 7){
	return adclvds_io_read(dev, ADCLVDS_ADDR_CH8 + ADCLVDS_REG_CH_TLOWR);
  }
  return 0;
}
//-------------------------------------------------------------------//
static void adclvds_hw_channel_write_high_threshold_reg(
					struct adclvds_device *dev,
					unsigned long hwid,
					u32 value ){
  if (hwid == 0){
	adclvds_io_write(dev, ADCLVDS_ADDR_CH1 + ADCLVDS_REG_CH_THIGR, value);
  } else if (hwid == 1){
	adclvds_io_write(dev, ADCLVDS_ADDR_CH2 + ADCLVDS_REG_CH_THIGR, value);
  } else if (hwid == 2){
	adclvds_io_write(dev, ADCLVDS_ADDR_CH3 + ADCLVDS_REG_CH_THIGR, value);
  } else if (hwid == 3){
	adclvds_io_write(dev, ADCLVDS_ADDR_CH4 + ADCLVDS_REG_CH_THIGR, value);
  } else if (hwid == 4){
	adclvds_io_write(dev, ADCLVDS_ADDR_CH5 + ADCLVDS_REG_CH_THIGR, value);
  } else if (hwid == 5){
	adclvds_io_write(dev, ADCLVDS_ADDR_CH6 + ADCLVDS_REG_CH_THIGR, value);
  } else if (hwid == 6){
	adclvds_io_write(dev, ADCLVDS_ADDR_CH7 + ADCLVDS_REG_CH_THIGR, value);
  } else if (hwid == 7){
	adclvds_io_write(dev, ADCLVDS_ADDR_CH8 + ADCLVDS_REG_CH_THIGR, value);
  }
}
//-------------------------------------------------------------------//
static u32 adclvds_hw_channel_read_high_threshold_reg(
					struct adclvds_device *dev,
					unsigned long hwid){
  if (hwid == 0){
	return adclvds_io_read(dev, ADCLVDS_ADDR_CH1 + ADCLVDS_REG_CH_THIGR);
  } else if (hwid == 1){
	return adclvds_io_read(dev, ADCLVDS_ADDR_CH2 + ADCLVDS_REG_CH_THIGR);
  } else if (hwid == 2){
	return adclvds_io_read(dev, ADCLVDS_ADDR_CH3 + ADCLVDS_REG_CH_THIGR);
  } else if (hwid == 3){
	return adclvds_io_read(dev, ADCLVDS_ADDR_CH4 + ADCLVDS_REG_CH_THIGR);
  } else if (hwid == 4){
	return adclvds_io_read(dev, ADCLVDS_ADDR_CH5 + ADCLVDS_REG_CH_THIGR);
  } else if (hwid == 5){
	return adclvds_io_read(dev, ADCLVDS_ADDR_CH6 + ADCLVDS_REG_CH_THIGR);
  } else if (hwid == 6){
	return adclvds_io_read(dev, ADCLVDS_ADDR_CH7 + ADCLVDS_REG_CH_THIGR);
  } else if (hwid == 7){
	return adclvds_io_read(dev, ADCLVDS_ADDR_CH8 + ADCLVDS_REG_CH_THIGR);
  }
  return 0;
}
//-------------------------------------------------------------------//
static void adclvds_hw_channel_write_average_reg(
					struct adclvds_device *dev,
					unsigned long hwid,
					u32 value){
  if (hwid == 0){
	adclvds_io_write(dev, ADCLVDS_ADDR_CH1 + ADCLVDS_REG_CH_AVRGR, value);
  } else if (hwid == 1){
	adclvds_io_write(dev, ADCLVDS_ADDR_CH2 + ADCLVDS_REG_CH_AVRGR, value);
  } else if (hwid == 2){
	adclvds_io_write(dev, ADCLVDS_ADDR_CH3 + ADCLVDS_REG_CH_AVRGR, value);
  } else if (hwid == 3){
	adclvds_io_write(dev, ADCLVDS_ADDR_CH4 + ADCLVDS_REG_CH_AVRGR, value);
  } else if (hwid == 4){
	adclvds_io_write(dev, ADCLVDS_ADDR_CH5 + ADCLVDS_REG_CH_AVRGR, value);
  } else if (hwid == 5){
	adclvds_io_write(dev, ADCLVDS_ADDR_CH6 + ADCLVDS_REG_CH_AVRGR, value);
  } else if (hwid == 6){
	adclvds_io_write(dev, ADCLVDS_ADDR_CH7 + ADCLVDS_REG_CH_AVRGR, value);
  } else if (hwid == 7){
	adclvds_io_write(dev, ADCLVDS_ADDR_CH8 + ADCLVDS_REG_CH_AVRGR, value);
  }
}
//-------------------------------------------------------------------//
static u32 adclvds_hw_channel_read_average_reg(
					struct adclvds_device *dev,
					unsigned long hwid ) {
  if (hwid == 0){
	return adclvds_io_read(dev, ADCLVDS_ADDR_CH1 + ADCLVDS_REG_CH_AVRGR);
  } else if (hwid == 1){
	return adclvds_io_read(dev, ADCLVDS_ADDR_CH2 + ADCLVDS_REG_CH_AVRGR);
  } else if (hwid == 2){
	return adclvds_io_read(dev, ADCLVDS_ADDR_CH3 + ADCLVDS_REG_CH_AVRGR);
  } else if (hwid == 3){
	return adclvds_io_read(dev, ADCLVDS_ADDR_CH4 + ADCLVDS_REG_CH_AVRGR);
  } else if (hwid == 4){
	return adclvds_io_read(dev, ADCLVDS_ADDR_CH5 + ADCLVDS_REG_CH_AVRGR);
  } else if (hwid == 5){
	return adclvds_io_read(dev, ADCLVDS_ADDR_CH6 + ADCLVDS_REG_CH_AVRGR);
  } else if (hwid == 6){
	return adclvds_io_read(dev, ADCLVDS_ADDR_CH7 + ADCLVDS_REG_CH_AVRGR);
  } else if (hwid == 7){
	return adclvds_io_read(dev, ADCLVDS_ADDR_CH7 + ADCLVDS_REG_CH_AVRGR);
  }
  return 0;
}
//-------------------------------------------------------------------//
static void adclvds_hw_channel_write_addr(struct adclvds_device *dev,
                                        unsigned long hwid,
					u32 value){
  if (hwid == 0){
	adclvds_io_write(dev, ADCLVDS_ADDR_CH1 + ADCLVDS_REG_CH_ADDRR, value);
  } else if (hwid == 1){
	adclvds_io_write(dev, ADCLVDS_ADDR_CH2 + ADCLVDS_REG_CH_ADDRR, value);
  } else if (hwid == 2){
	adclvds_io_write(dev, ADCLVDS_ADDR_CH3 + ADCLVDS_REG_CH_ADDRR, value);
  } else if (hwid == 3){
	adclvds_io_write(dev, ADCLVDS_ADDR_CH4 + ADCLVDS_REG_CH_ADDRR, value);
  } else if (hwid == 4){
	adclvds_io_write(dev, ADCLVDS_ADDR_CH5 + ADCLVDS_REG_CH_ADDRR, value);
  } else if (hwid == 5){
	adclvds_io_write(dev, ADCLVDS_ADDR_CH6 + ADCLVDS_REG_CH_ADDRR, value);
  } else if (hwid == 6){
	adclvds_io_write(dev, ADCLVDS_ADDR_CH7 + ADCLVDS_REG_CH_ADDRR, value);
  } else if (hwid == 7){
	adclvds_io_write(dev, ADCLVDS_ADDR_CH8 + ADCLVDS_REG_CH_ADDRR, value);
  }
}
//-------------------------------------------------------------------//
static void adclvds_hw_channel_interrupt_enable(
					struct adclvds_device *dev,
					unsigned long hwid ) {
  u32 interrupt_reg = adclvds_io_read(dev, ADCLVDS_REG_IRQR);
  // Since we do share interrupt control lines, let's not acknowledge
  // pending interrupts by mistake.
  interrupt_reg &= 0xFF;
  if (hwid == 0){
	interrupt_reg |= ADCLVDS_REG_IRQR_ENA_CHAN1;
  } else if (hwid == 1){
	interrupt_reg |= ADCLVDS_REG_IRQR_ENA_CHAN2;
  } else if (hwid == 2){
	interrupt_reg |= ADCLVDS_REG_IRQR_ENA_CHAN3;
  } else if (hwid == 3){
	interrupt_reg |= ADCLVDS_REG_IRQR_ENA_CHAN4;
  } else if (hwid == 4){
	interrupt_reg |= ADCLVDS_REG_IRQR_ENA_CHAN5;
  } else if (hwid == 5){
	interrupt_reg |= ADCLVDS_REG_IRQR_ENA_CHAN6;
  } else if (hwid == 6){
	interrupt_reg |= ADCLVDS_REG_IRQR_ENA_CHAN7;
  } else if (hwid == 7){
	interrupt_reg |= ADCLVDS_REG_IRQR_ENA_CHAN8;
  }
  adclvds_io_write(dev, ADCLVDS_REG_IRQR, interrupt_reg);
}
//-------------------------------------------------------------------//
static void adclvds_hw_channel_interrupt_disable(
					struct adclvds_device *dev,
					unsigned long hwid ) {
  u32 interrupt_reg = adclvds_io_read(dev, ADCLVDS_REG_IRQR);
  // Since we do share interrupt control lines, let's not acknowledge
  // pending interrupts by mistake.
  interrupt_reg &= 0xFF;
  if (hwid == 0){
	interrupt_reg &= ~ADCLVDS_REG_IRQR_ENA_CHAN1;
  } else if (hwid == 1){
	interrupt_reg &= ~ADCLVDS_REG_IRQR_ENA_CHAN2;
  } else if (hwid == 2){
	interrupt_reg &= ~ADCLVDS_REG_IRQR_ENA_CHAN3;
  } else if (hwid == 3){
	interrupt_reg &= ~ADCLVDS_REG_IRQR_ENA_CHAN4;
  } else if (hwid == 4){
	interrupt_reg &= ~ADCLVDS_REG_IRQR_ENA_CHAN5;
  } else if (hwid == 5){
	interrupt_reg &= ~ADCLVDS_REG_IRQR_ENA_CHAN6;
  } else if (hwid == 6){
	interrupt_reg &= ~ADCLVDS_REG_IRQR_ENA_CHAN7;
  } else if (hwid == 7){
	interrupt_reg &= ~ADCLVDS_REG_IRQR_ENA_CHAN8;
  }
  adclvds_io_write(dev, ADCLVDS_REG_IRQR, interrupt_reg);
}
//===================================================================//
// Helper functions for interrupt processing
void adclvds_process_irq_on_channel(struct fadc_channel *channel){
  struct fadc_chip *chip = channel->chip;
  struct adclvds_device *dev = fadc_get_chip_data(chip);
  struct fadc_buffer *buffer;
  int ii;

  // channel->circ_buf is giving us the currently circulating buffer number
  // We should mark it as cleared
  buffer = &channel->buffers[channel->circ_buf];
  if (!test_bit(FADC_BUFFER_CIRCULATING, &buffer->flags)) {
	// The buffer wasn't circulating,
	// so let's do nothing here, most probably it? 
	return;
  }
  // If we're here, then the buffer was circulating one
  // let's mark it as triggered.
  set_bit( FADC_BUFFER_TRIGGERED, &buffer->flags );
  // we shouldn't remove the circulating buffer ?
  clear_bit(FADC_BUFFER_CIRCULATING, &buffer->flags);
 
  // let's scroll through the buffers and try to assign the buffer,
  // that is not triggered
  for (ii = 0; ii < channel->nbuffers; ++ii){
  	buffer = &channel->buffers[ii];
	if (!test_bit(FADC_BUFFER_TRIGGERED, &buffer->flags)) {
		channel->circ_buf = ii;
		set_bit( FADC_BUFFER_CIRCULATING, &buffer->flags );
		clear_bit(FADC_BUFFER_TRIGGERED, &buffer->flags);
		break;
	}
  }
  // Even if all buffers are full the channel->circ_buf will point to the triggered one
  buffer = &channel->buffers[channel->circ_buf];
  clear_bit(FADC_BUFFER_TRIGGERED, &buffer->flags);
  set_bit(FADC_BUFFER_CIRCULATING, &buffer->flags);
  // And we want to update the address, which will clear the interrupt
  // We want to translate virtual address used in buffer->start
  adclvds_hw_channel_write_addr(dev, channel->hwid, buffer->phyaddr);
}
//-------------------------------------------------------------------//
// Handle the Interrupt
static irqreturn_t adclvds_irq(int irq, void *pw){
  struct fadc_chip *chip = pw;
  struct adclvds_device *dev = fadc_get_chip_data(chip);
  u32 interrupt_reg;
  int processed_channels;

  do {
  	interrupt_reg = adclvds_io_read(dev, ADCLVDS_REG_IRQR);
	adclvds_io_write(dev, ADCLVDS_REG_IRQR, interrupt_reg);
  	processed_channels = 0;

	if (interrupt_reg & ADCLVDS_REG_IRQR_PEN_CHAN1) {
		adclvds_process_irq_on_channel(&chip->channels[0]);
		processed_channels += 1;
	}
	if (interrupt_reg & ADCLVDS_REG_IRQR_PEN_CHAN2) {
		adclvds_process_irq_on_channel(&chip->channels[1]);
		processed_channels += 1;
	}
	if (interrupt_reg & ADCLVDS_REG_IRQR_PEN_CHAN3) {
		adclvds_process_irq_on_channel(&chip->channels[2]);
		processed_channels += 1;
	}
	if (interrupt_reg & ADCLVDS_REG_IRQR_PEN_CHAN4) {
		adclvds_process_irq_on_channel(&chip->channels[3]);
		processed_channels += 1;
	}
	if (interrupt_reg & ADCLVDS_REG_IRQR_PEN_CHAN5) {
		adclvds_process_irq_on_channel(&chip->channels[4]);
		processed_channels += 1;
	}
	if (interrupt_reg & ADCLVDS_REG_IRQR_PEN_CHAN6) {
		adclvds_process_irq_on_channel(&chip->channels[5]);
		processed_channels += 1;
	}
	if (interrupt_reg & ADCLVDS_REG_IRQR_PEN_CHAN7) {
		adclvds_process_irq_on_channel(&chip->channels[6]);
		processed_channels += 1;
	}
	if (interrupt_reg & ADCLVDS_REG_IRQR_PEN_CHAN8) {
		adclvds_process_irq_on_channel(&chip->channels[7]);
		processed_channels += 1;
	}
  } while( processed_channels != 0);


  return IRQ_HANDLED;
}
//===================================================================//
// Device Operations
//-------------------------------------------------------------------//
// Init Structure for holding private data
static int adclvds_private_request( struct fadc_chip *chip ) {
  struct adclvds_device *priv;
  // printk("adclvds_private_request\n");
  priv = kzalloc(sizeof(*priv), GFP_KERNEL);
  if (!priv)
	return -ENOMEM;
  fadc_set_chip_data(chip, priv);

  return 0;
}
//-------------------------------------------------------------------//
// Free Private data structure
static void adclvds_private_free( struct fadc_chip *chip ) {
  struct adclvds_device *priv = fadc_get_chip_data(chip);
  // printk("adclvds_private_free\n");
  if (priv) {
	// peripheral_free(priv->pin);
	kfree(priv);
	priv = NULL; // AVD: Do we need this?
  }
}
//-------------------------------------------------------------------//
static void adclvds_chip_timesync(struct fadc_chip *chip, 
                        unsigned long timesec) {
  struct adclvds_device *dev = fadc_get_chip_data(chip);

  adclvds_hw_chip_timesync(dev, timesec);
}
//-------------------------------------------------------------------//
static int adclvds_channel_enable(struct fadc_channel *channel ) {
  struct fadc_chip *chip = channel->chip;
  struct adclvds_device *dev = fadc_get_chip_data(chip);

  if (test_bit(FADC_CHAN_ENABLED, &channel->flags)) {
  	// If the channel was enabled, do nothing
	return 0;
  }
  dev_info(chip->dev, "Enabling channel %d\n", channel->id);
  // Make Sure that the chip is enabled
  if (!test_bit(FADC_CHIP_ENABLED, &chip->flags)) {
	// If chip was not enabled, enable it
	adclvds_hw_chip_enable(dev);
	set_bit(FADC_CHIP_ENABLED, &chip->flags);
  }
  // Make Sure that the chip is calibrated
  if (!test_bit(FADC_CHIP_CALIBRATED, &chip->flags)) {
  	adclvds_hw_chip_calibrate(dev);
	set_bit(FADC_CHIP_CALIBRATED, &chip->flags);
  }
  //There is a non standart logic in turning on the channels. power group 3 
  // (hw channels 5 and 6) is also powering the reference voltage buffers
  // from ADC chip to the other channels, so channels 5 and 6 should be turned
  // on before the rest of channels

  // Since we need to power channel 5 and 6 anyway, let's do it here
  if (	(!test_bit(FADC_CHIP_ENABLED, &(&chip->channels[4])->flags))||
  	(!test_bit(FADC_CHIP_ENABLED, &(&chip->channels[5])->flags)) ){
	adclvds_hw_channel_enable(dev, 4);
	set_bit(FADC_CHAN_ENABLED, &(&chip->channels[4])->flags );
	set_bit(FADC_CHAN_ENABLED, &(&chip->channels[5])->flags );
	// And enable interrupts
	adclvds_hw_channel_interrupt_enable(dev, 4);
	adclvds_hw_channel_interrupt_enable(dev, 5);
  }
  // We want to enable the other channel in power group
  if 		((channel->hwid == 0)||(channel->hwid == 1)) {
  	// Powering one channel in hardware automatically powers
	// the other, so let's do it only once
	adclvds_hw_channel_enable(dev, 0);
	set_bit(FADC_CHAN_ENABLED, &(&chip->channels[0])->flags );
	set_bit(FADC_CHAN_ENABLED, &(&chip->channels[1])->flags );
	adclvds_hw_channel_interrupt_enable(dev, 0);
	adclvds_hw_channel_interrupt_enable(dev, 1);
  } else if 	((channel->hwid == 2)||(channel->hwid == 3)) {
	adclvds_hw_channel_enable(dev, 2);
	set_bit(FADC_CHAN_ENABLED, &(&chip->channels[2])->flags );
	set_bit(FADC_CHAN_ENABLED, &(&chip->channels[3])->flags );
	adclvds_hw_channel_interrupt_enable(dev, 2);
	adclvds_hw_channel_interrupt_enable(dev, 3);
  } else if 	((channel->hwid == 6)||(channel->hwid == 7)) {
	adclvds_hw_channel_enable(dev, 6);
	set_bit(FADC_CHAN_ENABLED, &(&chip->channels[6])->flags );
	set_bit(FADC_CHAN_ENABLED, &(&chip->channels[7])->flags );
	adclvds_hw_channel_interrupt_enable(dev, 6);
	adclvds_hw_channel_interrupt_enable(dev, 7);
  }

  return 0;
}
//-------------------------------------------------------------------//
static int adclvds_channel_disable(struct fadc_channel *channel ) {
  struct fadc_chip *chip = channel->chip;
  struct adclvds_device *dev = fadc_get_chip_data(chip);
  unsigned long ii;

  if (!test_bit(FADC_CHAN_ENABLED, &channel->flags)) {
	// If the channel was disabled, do nothing
	return 0;
  }
  dev_info(chip->dev, "Disabling channel %d\n", channel->id);
  // If we're disabling channels 5 or 6, then we guaranteed to
  // power-down the whole ADC, including the ADC itself
  if 		((channel->hwid == 4)||(channel->hwid == 5)) {
	for (ii = 0; ii < chip->nchannels; ++ii) {
		adclvds_hw_channel_interrupt_disable(dev, ii);
		clear_bit(FADC_CHAN_ENABLED, &(&chip->channels[ii])->flags );
	}
	adclvds_hw_chip_disable(dev);
	clear_bit(FADC_CHIP_ENABLED, &chip->flags);
	clear_bit(FADC_CHIP_CALIBRATED, &chip->flags);
  } else if 	((channel->hwid == 0)||(channel->hwid == 1)) {
	adclvds_hw_channel_interrupt_disable(dev, 0);
	adclvds_hw_channel_interrupt_disable(dev, 1);
	clear_bit(FADC_CHAN_ENABLED, &(&chip->channels[0])->flags );
	clear_bit(FADC_CHAN_ENABLED, &(&chip->channels[1])->flags );
	adclvds_hw_channel_disable(dev, 0);
  } else if 	((channel->hwid == 2)||(channel->hwid == 3)) {
  	adclvds_hw_channel_interrupt_disable(dev, 2);
	adclvds_hw_channel_interrupt_disable(dev, 3);
	clear_bit(FADC_CHAN_ENABLED, &(&chip->channels[2])->flags );
	clear_bit(FADC_CHAN_ENABLED, &(&chip->channels[3])->flags );
	adclvds_hw_channel_disable(dev, 2);
  } else if 	((channel->hwid == 6)||(channel->hwid == 7)) {
	adclvds_hw_channel_interrupt_disable(dev, 6);
	adclvds_hw_channel_interrupt_disable(dev, 7);
	clear_bit(FADC_CHAN_ENABLED, &(&chip->channels[6])->flags );
	clear_bit(FADC_CHAN_ENABLED, &(&chip->channels[7])->flags );
	adclvds_hw_channel_disable(dev, 6);
  }
  return 0;
}
//-------------------------------------------------------------------//
static int adclvds_channel_trigger( struct fadc_channel *channel ){
  struct fadc_chip *chip = channel->chip;
  struct adclvds_device *dev = fadc_get_chip_data(chip);
  if (channel->hwid >= chip->nchannels )
  	return -1;
  adclvds_hw_channel_trigger(dev, channel->hwid);
  return 0;
}
//-------------------------------------------------------------------//
static int adclvds_channel_set_threshold( struct fadc_channel *channel, 
				int type, unsigned long value ){
  struct fadc_chip *chip = channel->chip;
  struct adclvds_device *dev = fadc_get_chip_data(chip);
  if (channel->hwid >= chip->nchannels )
	return -1;
  // type=0 is high threshold and type=1 is low threshold
  if (type == 0){
	adclvds_hw_channel_write_high_threshold_reg(dev,channel->hwid,value);
	return 0;
  } else if (type == 1) {
	adclvds_hw_channel_write_low_threshold_reg(dev,channel->hwid,value);
	return 0;
  }
  return -1;
}
//-------------------------------------------------------------------//
static int adclvds_channel_get_threshold( struct fadc_channel *channel, 
				int type, unsigned long *value ){
  struct fadc_chip *chip = channel->chip;
  struct adclvds_device *dev = fadc_get_chip_data(chip);
  if (channel->hwid >= chip->nchannels )
	return -1;
  if (type == 0){
	*value = adclvds_hw_channel_read_high_threshold_reg(dev, channel->hwid);
	return 0;
  } else if (type == 1) {
	*value = adclvds_hw_channel_read_low_threshold_reg(dev, channel->hwid);
	return 0;
  }
  return -1;
}
//-------------------------------------------------------------------//
static int adclvds_channel_set_average( struct fadc_channel *channel, 
				int type, unsigned long value ){
  struct fadc_chip *chip = channel->chip;
  struct adclvds_device *dev = fadc_get_chip_data(chip);
  u32 average_reg;

  if (channel->hwid >= chip->nchannels )
	return -1;
  average_reg = adclvds_hw_channel_read_average_reg(dev, channel->hwid);
  if (type == 0){
	average_reg &=~ 0x00F;
	average_reg |= (value << 0)&0x00F;
  } else if (type == 1){
	average_reg &=~ 0x0F0;
	average_reg |= (value << 4)&0x0F0;
  } else if (type == 2){
	average_reg &=~ 0xF00;
	average_reg |= (value << 8)&0xF00;
  } else {
	return -1;
  }
  adclvds_hw_channel_write_average_reg(dev, channel->hwid, average_reg);
  return 0;
}
//-------------------------------------------------------------------//
static int adclvds_channel_get_average( struct fadc_channel *channel,
				int type, unsigned long *value ){
  struct fadc_chip *chip = channel->chip;
  struct adclvds_device *dev = fadc_get_chip_data(chip);
  if (channel->hwid >= chip->nchannels )
	return -1;
  if (type == 0){
	*value = (adclvds_hw_channel_read_average_reg(dev, channel->hwid)>>0)&0xF;
	return 0;
  } else if (type == 1){
	*value = (adclvds_hw_channel_read_average_reg(dev, channel->hwid)>>4)&0xF;
	return 0;
  } else if (type == 2){
	*value = (adclvds_hw_channel_read_average_reg(dev, channel->hwid)>>8)&0xF;;
	return 0;
  }
  return -1;
}
//-------------------------------------------------------------------//
// A simple routine to fill the internals of the buffers
static void adclvds_chip_init_buffers(struct fadc_chip *chip, 
				u32 phystart, void* iostart, 
				resource_size_t each_buf_size){
  struct fadc_channel *channel;
  struct adclvds_device *dev = fadc_get_chip_data(chip);
  struct fadc_buffer *buffer;
  u32 current_phyaddr = phystart;
  void * current_ioaddr = iostart;
  int ii;
  int jj;
  
  for(ii = 0; ii < chip->nchannels; ++ii){
	channel = &chip->channels[ii];
	channel->circ_buf = 0;
	for (jj=0; jj < channel->nbuffers; ++jj){
		buffer = &channel->buffers[jj];
		// Ther reserved: 20 Bytes for IP and 8 Bytes for UDP header + 4 Bytes
		// for Channel ID.
		buffer->phyaddr = current_phyaddr + 32;
		buffer->iosaddr = current_ioaddr;
		buffer->size = each_buf_size;
		// for each buffer we also want to include
		// 4 bytes header, indicating the channel number:
		adclvds_buf_write(buffer->iosaddr + 28, channel->id);

		current_phyaddr += each_buf_size;
		current_ioaddr  += each_buf_size;
		/*
		printk("...CHAN:%02d BUF:%02d Buffer virt_addr=0x%p phys_addr=0x%08x\n",
			ii, jj, buffer->iosaddr, buffer->phyaddr );
		*/
		clear_bit(FADC_BUFFER_TRIGGERED,   	&buffer->flags);
		clear_bit(FADC_BUFFER_CIRCULATING,   	&buffer->flags);
		// We want to set the buffer 0 as circulating
		if (jj == 0 ) {
			adclvds_hw_channel_write_addr(dev, channel->hwid, buffer->phyaddr);
			set_bit(FADC_BUFFER_CIRCULATING, &buffer->flags);
		}
	}
  }
}
//-------------------------------------------------------------------//
static struct fadc_chip_ops adclvds_chip_ops = {
	.request = 	adclvds_private_request,
	.free = 	adclvds_private_free,

	.timesync = 	adclvds_chip_timesync,

	.enable = 	adclvds_channel_enable,
	.disable = 	adclvds_channel_disable,

	.trigger =	adclvds_channel_trigger,

	.set_threshold =	adclvds_channel_set_threshold,
	.get_threshold = 	adclvds_channel_get_threshold,

	.set_average =	adclvds_channel_set_average,
	.get_average =  adclvds_channel_get_average,

	.owner = THIS_MODULE,
};
//===================================================================//
static int adclvds_probe(struct platform_device *pdev) {
  struct fadc_chip *fadc;
  struct adclvds_device *priv; 
  struct resource *res = NULL;
  struct resource *mmio = NULL;
  void __iomem *mmio_base = NULL;
  struct resource *mbuf = NULL;
  void __iomem *mbuf_base = NULL;
  unsigned long each_buf_size = 0x2000; 
  	// This is the default 0x810 value (512samples+4header)=2064
  int ret;

  fadc = devm_kzalloc(&pdev->dev, sizeof(*fadc), GFP_KERNEL);
  if (!fadc) {
	dev_err(&pdev->dev, "failed to allocate memory\n");
	return -ENOMEM;
  }
  platform_set_drvdata(pdev, fadc);

  fadc->dev = &pdev->dev;
  fadc->ops = &adclvds_chip_ops;
  fadc->base_id = -1; // Automatic
  fadc->nchannels = 8;

  // Before adding the chip, let's make sure we can allocate
  // IO and Buffer memories

  // IO Memory Space:
  // 	1. get platform description
  // 	2. Request the memory
  // 	3. Remap the memory
  //====>
  res = platform_get_resource(pdev, IORESOURCE_MEM, 0);
  if (!res) {
	dev_err(&pdev->dev, "cannot obtain I/O memory space\n");
	return -ENOMEM;
  }
  mmio = devm_request_mem_region(&pdev->dev, res->start,
		resource_size(res), res->name);
  if (!mmio) {
	dev_err(&pdev->dev, "cannot request I/O memory space\n");
	return -ENOMEM;
  }
  mmio_base = devm_ioremap_nocache(&pdev->dev, mmio->start, resource_size(mmio));
  if (!mmio_base) {
	dev_err(&pdev->dev, "cannot remap I/O memory space\n");
	return -ENOMEM;
  }
  // Buffers Memory Space:
  // 	1. Get platform description
  // 	2. Request the memory
  // 	3. Remap the memory
  res = platform_get_resource(pdev, IORESOURCE_MEM, 1);
  if (!res) {
	dev_err(&pdev->dev, "cannot obtain buffer memory space\n");
	return -ENOMEM;
  }
  mbuf = devm_request_mem_region(&pdev->dev, res->start,
		resource_size(res), res->name);
  if (!mbuf) {
	dev_err(&pdev->dev, "cannot request buffer memory space\n");
	return -ENOMEM;
  }
  mbuf_base = devm_ioremap_nocache(&pdev->dev, mbuf->start, resource_size(mbuf) );
  if (!mbuf_base) {
	dev_err(&pdev->dev, "cannot remap buffer memory space\n");
	return -ENOMEM;
  }
  // One more thing todo is to calculate how many buffers we do have per channel
  // I'm suggesting to quit with error
  fadc->nbuf_per_channel = resource_size(mbuf)/
  	(fadc->nchannels*each_buf_size); // For Now, let's create two

  //====> After we do have IO and buffer Memory, let's add the chip
  ret = fadc_chip_add(fadc);
  if (ret < 0) {
	dev_err(&pdev->dev, "fadc_chip_add() failed: %d\n", ret);
	return -ENODEV;
  }
  //===>Let's fill the private data...
  priv = fadc_get_chip_data(fadc);

  priv->io_size = resource_size(mmio);
  priv->io_base = mmio_base;
  dev_info(&pdev->dev, "IOs MEM @ 0x%p,size=0x%08x\n",
			priv->io_base, priv->io_size );

  priv->buf_size = resource_size(mbuf);
  priv->buf_base = mbuf_base;
  dev_info(&pdev->dev, "BUF MEM @ 0x%p,size=0x%08x\n",
			priv->buf_base, priv->buf_size );

  // The buffers should be created now, but the internal values
  // Still need to be initialized for this chip
  // mbuf->start is physical address , mbuf_base is ioremaped address
  adclvds_chip_init_buffers(fadc, mbuf->start, mbuf_base, each_buf_size);

  /* obtain device IRQ number */
  res  = platform_get_resource(pdev, IORESOURCE_IRQ, 0);
  if (!res) {
  	dev_err(&pdev->dev, "failed to get adc irq\n");
	return -ENODEV;
  }
  priv->irq = res->start;
  ret = devm_request_irq(&pdev->dev, priv->irq, adclvds_irq, 
  			0, dev_name(&pdev->dev), fadc);
  if (ret) {
	dev_err(&pdev->dev, "failed to attach adc irq\n");
	return -ENODEV;
  }
  dev_info(&pdev->dev, "IRQ %d requested\n", priv->irq);

  return 0;
}
//-------------------------------------------------------------------//
static int adclvds_remove(struct platform_device *pdev) {
  struct fadc_chip *fadc = platform_get_drvdata(pdev);

  return fadc_chip_remove(fadc);
}
//-------------------------------------------------------------------//
#ifdef CONFIG_PM
static int adclvds_suspend(struct platform_device *pdev, pm_message_t state) {
  return -ENOSYS;
}
static int adclvds_resume(struct platform_device *pdev) {
  return -ENOSYS;
}
#else  /* CONFIG_PM */
  #define adclvds_suspend NULL
  #define adclvds_resume  NULL
#endif /* CONFIG_PM */
//-------------------------------------------------------------------//
static struct of_device_id adclvds_match[] = {
	{ .compatible = "driftcores,adclvds-v1" },
	{},
};
MODULE_DEVICE_TABLE(of, adclvds_match);

static struct platform_driver adclvds_driver = {
	.probe   = adclvds_probe,
	.remove  = adclvds_remove,
	.suspend = adclvds_suspend,
	.resume  = adclvds_resume,
	.driver  = {
		.name = "adclvds",
		.owner = THIS_MODULE,
		.of_match_table = adclvds_match,
	},
};
module_platform_driver(adclvds_driver);

MODULE_AUTHOR("Alexei Dorofeev");
MODULE_DESCRIPTION("LVDS ADC (8chan) Flash ADC Driver");
MODULE_LICENSE("GPL");
