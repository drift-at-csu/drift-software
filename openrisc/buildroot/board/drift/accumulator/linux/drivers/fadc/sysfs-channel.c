/*
 * A simple sysfs interface for the generic FADC framework
 */
#include <linux/device.h>
#include <linux/err.h>
#include <linux/slab.h>
#include <linux/kdev_t.h>

#include "fadc.h"

//-------------------------------------------------------------------//
static ssize_t fadc_channel_enable_show(
				struct device *parent,
				struct device_attribute *attr,
			        char *buf) {
  const struct fadc_channel *channel = dev_get_drvdata(parent);

  return sprintf(buf, "%d\n", test_bit(FADC_CHAN_ENABLED, &channel->flags));
}
static ssize_t fadc_channel_enable_store(
				struct device *parent,
				struct device_attribute *attr,
				const char *buf, size_t size) {
  /* AVD: Do we need a "const" here? */
  struct fadc_channel *channel = dev_get_drvdata(parent);
  unsigned int val;
  int ret;
  if (!channel->chip)
  	return 0;
  if (!channel->chip->ops)
  	return 0;

  ret = kstrtouint(buf, 0, &val);
  if (ret)
	return ret;

  if (val){
  	channel->chip->ops->enable(channel);
	// set_bit(FADC_CHAN_ENABLED, &channel->flags);
  } else {
  	channel->chip->ops->disable(channel);
	// clear_bit(FADC_CHAN_ENABLED, &channel->flags);
  }
  return ret ? : size;
}
static DEVICE_ATTR(enable, S_IRUGO | S_IWUSR, 
		fadc_channel_enable_show, fadc_channel_enable_store);
//-------------------------------------------------------------------//
static ssize_t fadc_channel_status_show(
					struct device *parent,
				        struct device_attribute *attr,
					char *buf) {
  const struct fadc_channel *channel = dev_get_drvdata(parent);
  const struct fadc_chip *chip = channel->chip;

  return sprintf(buf, "REQ:%s CHAN_EN:%s ADC_EN:%s ADC_CAL:%s\n",
  		test_bit(FADC_CHAN_REQUESTED, 	&channel->flags) 	? "Yes":"No ",
		test_bit(FADC_CHAN_ENABLED, 	&channel->flags)	? "Yes":"No ",
		test_bit(FADC_CHIP_ENABLED, 	&chip->flags) 		? "Yes":"No ",
		test_bit(FADC_CHIP_CALIBRATED, 	&chip->flags)		? "Yes":"No ");
}
static DEVICE_ATTR(status, S_IRUGO, fadc_channel_status_show, NULL);
//-------------------------------------------------------------------//
static ssize_t fadc_channel_trigger_show(
					struct device *parent,
					struct device_attribute *attr,
					char *buf) {
  const struct fadc_channel *channel = dev_get_drvdata(parent);
  return sprintf(buf, "Trigger show is not implemented for Chan%02u ( [HWchan%01u])\n",
		channel->id, channel->hwid);
}
static ssize_t fadc_channel_trigger_store(
					struct device *parent,
					struct device_attribute *attr,
					const char *buf, size_t size) {
  struct fadc_channel *channel = dev_get_drvdata(parent);
  unsigned int val;
  int ret;
 
  ret = kstrtouint(buf, 0, &val);
  if (ret)
	return ret;
  // printk("Triggering Chan=%02u (HW=%01u)= %u\n", channel->id, channel->hwid, val);
  channel->chip->ops->trigger(channel);
  return ret ? : size;
}
static DEVICE_ATTR(trigger, 0644, fadc_channel_trigger_show, fadc_channel_trigger_store);
//-------------------------------------------------------------------//
static ssize_t fadc_channel_average_show(
                                        struct device *parent,
                                        struct device_attribute *attr,
                                        char *buf) {
  struct fadc_channel *channel = dev_get_drvdata(parent);
  int ii = 0;
  unsigned long value;
  unsigned long tot_nof_bytes = 0;

  // tot_nof_bytes += sprintf(buf+tot_nof_bytes,"AVG:"); 
  while (1){
	if ( channel->chip->ops->get_average(channel, ii, &value) < 0)
		break;
	tot_nof_bytes += sprintf(buf+tot_nof_bytes,"0x%08x ", (u32)value);
	ii += 1;
  }
  tot_nof_bytes += sprintf(buf+tot_nof_bytes,"\n");
  return tot_nof_bytes;
}
static ssize_t fadc_channel_average_store(
                                        struct device *parent,
                                        struct device_attribute *attr,
                                        const char *buf, size_t size) {
  struct fadc_channel *channel = dev_get_drvdata(parent);
  unsigned int value;
  int ret = 0;
  char *token;
  char *running;
  const char delimiters[] = " \t";
  int ii;
  
  ii = 0;
  running = (char *)buf;
  while(1) {
	token = strsep(&running, delimiters);
	if (!token) // We reached the end of the buffer
		break;
	if (token[0] == '\0') // We just ignore 
		continue;
	ret = kstrtouint(token, 0, &value);
	if (ret) // Error in string conversion, let's try to continue
		break;
	if ( channel->chip->ops->set_average(channel, ii,  value) < 0)
		// There is no such parameter
		break;
	printk("AVG%d=%08x ", ii, value);
	ii += 1;
  }
  printk("\n");
  return ret ? : size;
}
static DEVICE_ATTR(average, 0644, fadc_channel_average_show, fadc_channel_average_store);
//-------------------------------------------------------------------//
static ssize_t fadc_channel_threshold_show(
					struct device *parent,
					struct device_attribute *attr,
					char *buf) {
  struct fadc_channel *channel = dev_get_drvdata(parent);
  int ii = 0;
  unsigned long value;
  unsigned long tot_nof_bytes = 0;

  // tot_nof_bytes += sprintf(buf+tot_nof_bytes,"AVG:"); 
  while (1){
	if ( channel->chip->ops->get_threshold(channel, ii, &value) < 0)
		break;
	tot_nof_bytes += sprintf(buf+tot_nof_bytes,"0x%08x ", (u32)value);
	ii += 1;
  }
  tot_nof_bytes += sprintf(buf+tot_nof_bytes,"\n");
  return tot_nof_bytes;
}
static ssize_t fadc_channel_threshold_store(
					struct device *parent,
					struct device_attribute *attr,
					const char *buf, size_t size) {
  struct fadc_channel *channel = dev_get_drvdata(parent);
  unsigned int value;
  int ret = 0;
  char *token;
  char *running;
  const char delimiters[] = " \t";
  int ii;

  ii = 0;
  running = (char *)buf;
  while(1) {
	token = strsep(&running, delimiters);
	if (!token) // We reached the end of the buffer
		break;
	if (token[0] == '\0') // We just ignore 
		continue;
	ret = kstrtouint(token, 0, &value);
	if (ret) // Error in string conversion, let's try to continue
		break;
	if ( channel->chip->ops->set_threshold(channel, ii,  value) < 0)
		// There is no such parameter
		break;
	printk("THR%d=%08x ", ii, value);
	ii += 1;
  }
  printk("\n");
  return ret ? : size;
}
static DEVICE_ATTR(threshold, 0644, fadc_channel_threshold_show, fadc_channel_threshold_store);
//-------------------------------------------------------------------//
static struct attribute *fadc_channel_attrs[] = {
	&dev_attr_enable.attr,
	&dev_attr_status.attr,
	&dev_attr_trigger.attr,
	&dev_attr_average.attr,
	&dev_attr_threshold.attr,
	//---> Add More DEVICE_ATTR here
	NULL
};
ATTRIBUTE_GROUPS(fadc_channel);
//-------------------------------------------------------------------//
static struct class fadc_channel_class = {
	.name	= "fadc_channels",
	.owner  = THIS_MODULE,
	.dev_groups     = fadc_channel_groups,
};
//-------------------------------------------------------------------//
static int fadcchip_sysfs_match(
				struct device *parent, 
				const void *data) {
  return dev_get_drvdata(parent) == data;
}
//-------------------------------------------------------------------//
void fadc_channel_sysfs_export( struct fadc_channel *channel) {
  struct device *parent;
  /*
   * If device_create() fails the fadc_chip is still usable by
   * the kernel its just not exported.
   */
  // Let's create separate device for each channel
  parent = device_create(
	&fadc_channel_class, 	// class - pointer to the struct class 
				// that this device should be registered to 
	channel->chip->dev, 	// parent - pointer to the parent struct device of this 
				// new device, if any 
	MKDEV(0, 0),	// devt - the dev_t for the char device to be added
	channel, 		// drvdata - the data to be added to the device for callbacks
	"chan%02d", 	// fmt - string for the device's name
		channel->id );	// argument(s) for fmt
  if (IS_ERR(parent)) {
	dev_warn(channel->chip->dev, "device_create failed for fadc_channel sysfs export\n");
  }
}
//-------------------------------------------------------------------//
void fadc_channel_sysfs_unexport(struct fadc_channel *channel) {
  struct device *parent;
  parent = class_find_device(&fadc_channel_class, NULL, channel->chip, fadcchip_sysfs_match);
  if (parent) {
	/* for class_find_device() */
	put_device(parent);
	device_unregister(parent);
  }
}
//-------------------------------------------------------------------//
static int __init fadc_channel_sysfs_init(void) {
  return class_register(&fadc_channel_class);
}
subsys_initcall(fadc_channel_sysfs_init);
//-------------------------------------------------------------------//
