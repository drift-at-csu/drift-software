// Generic fadclib implementation

#include <linux/module.h>
#include <linux/radix-tree.h>
#include <linux/mutex.h>
#include <linux/err.h>
#include <linux/slab.h>
#include <linux/device.h>

#include "fadc.h"

#define MAX_FADC_CHANNELS 99

//static DEFINE_MUTEX(fadc_lookup_lock);
//static LIST_HEAD(fadc_lookup_list);

static DEFINE_MUTEX(fadc_chips_lock);
static LIST_HEAD(fadc_chips);

static DECLARE_BITMAP(allocated_fadc_channels, MAX_FADC_CHANNELS);
static RADIX_TREE(fadc_channels_tree, GFP_KERNEL);
static DEFINE_MUTEX(fadc_channels_lock);
static LIST_HEAD(fadc_channels);

static DEFINE_MUTEX(fadc_buffers_lock);
static LIST_HEAD(fadc_buffers);

//-------------------------------------------------------------------//
static int alloc_fadc_channels(int channel, unsigned int count) {
  unsigned int from = 0;
  unsigned int start;

  if (channel >= MAX_FADC_CHANNELS)
	return -EINVAL;

  if (channel >= 0)
	from = channel;

  start = bitmap_find_next_zero_area(
  	allocated_fadc_channels, MAX_FADC_CHANNELS, from, count, 0);

  if (channel >= 0 && start != channel)
	return -EEXIST;

  if (start + count > MAX_FADC_CHANNELS)
	return -ENOSPC;

  return start;
}
//-------------------------------------------------------------------//
static void free_fadc_channels(struct fadc_chip *chip) {
  unsigned int ii;
  // unsigned int jj;

  for (ii = 0; ii < chip->nchannels; ++ii) {
	struct fadc_channel *channel = &chip->channels[ii];
	/*
	for (jj = 0; jj < channel->nbuffers; ++jj) {
		struct fadc_buffer *buffer = &channel->buffers[ii];
	}
	*/
	channel->nbuffers = 0;
	kfree(channel->buffers);
	channel->buffers = NULL;
	radix_tree_delete(&fadc_channels_tree, channel->id);
  }
  bitmap_clear(allocated_fadc_channels, chip->base_id, chip->nchannels);
  kfree(chip->channels);
  chip->channels = NULL;
}
//===================================================================//
int fadc_channel_get_total(void){
  struct fadc_channel *channel;
  int counter = 0;

  mutex_lock(&fadc_channels_lock);
  list_for_each_entry(channel, &fadc_channels, list) {
	counter += 1;
  }
  mutex_unlock(&fadc_channels_lock);
  return counter;
}
//-------------------------------------------------------------------//
int fadc_channels_trigger_all(void){
  struct fadc_channel *channel;
  int counter = 0;
  mutex_lock(&fadc_channels_lock);
  list_for_each_entry(channel, &fadc_channels, list) {
  	channel->chip->ops->trigger(channel);
  	counter += 1;
  }
  mutex_unlock(&fadc_channels_lock);
  return counter;
}
EXPORT_SYMBOL_GPL(fadc_channels_trigger_all);
//-------------------------------------------------------------------//
int fadc_chips_timesync(unsigned long timesec){
  struct fadc_chip *chip;
  int counter = 0;

  mutex_lock (&fadc_chips_lock);
  list_for_each_entry(chip, &fadc_chips, list) {
  	chip->ops->timesync(chip, timesec);
	counter += 1;
  }
  mutex_unlock(&fadc_chips_lock);
  return counter;
}
EXPORT_SYMBOL_GPL(fadc_chips_timesync);
//===================================================================//
void fadc_buffer_mutex_lock(void){
  mutex_lock (&fadc_buffers_lock);
}
//-------------------------------------------------------------------//
void fadc_buffer_mutex_unlock(void){
  mutex_unlock (&fadc_buffers_lock);
}
//-------------------------------------------------------------------//
struct fadc_buffer *fadc_buffer_find_next_triggered(struct fadc_buffer *first_buffer ){
  unsigned short ii;
  // We can supply a NULL pointer to this function. In this case the list
  // will get the first buffer in the list if it exists
  if (!first_buffer){
  	first_buffer = fadc_buffer_get_first_in_list();
	// If we're still NULL here, then the list is empty
	if (!first_buffer)
		return NULL;
  }
  /*
   * list_for_each_entry_from - iterate over list of given type from the current point
   * @pos:        the type * to use as a loop cursor. 
   * @head:       the head for your list.
   * @member:     the name of the list_struct within the struct.
   *
   * Iterate over list of given type, continuing from current position.
   * list_for_each_entry_from(pos, head, member) 
   *
   *  for (; prefetch(pos->member.next), &pos->member != (head);
   *  	pos = list_entry(pos->member.next, typeof(*pos), member))
   *
   */
  // Here we want to check if we're at the end of the list, and move the pointer
  // to the top of the list if this is the case. list_first_entry expects
  // the list to be non-empty
  /*
  if ( list_is_last(&first_buffer->list, &fadc_buffers ) ){
	first_buffer = fadc_buffer_get_first_in_list();
  }
  */

  // In case we started from the middle of the list, lets try to go to a next buffer
  for (ii = 0; ii < 2; ++ii){
  	list_for_each_entry_from(first_buffer, &fadc_buffers, list) {
  		if (test_bit(FADC_BUFFER_TRIGGERED, &first_buffer->flags) ) {
			// buffer is triggered
			return first_buffer;
		}		
  	}
	// If we're here, we went all the way down and didn't met a triggered buffer,
	// therefore we want to repeat from the begining...
	first_buffer = fadc_buffer_get_first_in_list();
  }

  // And I guess we tried what we can, with no triggered buffers,
  // If the buffers will appear at the top of the list, they should
  // be rewinded with call to a next read function.
  // return NULL here
  return NULL;
}
//-------------------------------------------------------------------//
struct fadc_buffer *fadc_buffer_get_first_in_list(void){
  struct fadc_buffer *buffer;

  if (fadc_buffers.next == &fadc_buffers)
  	buffer = NULL;
  else
	buffer = list_entry(fadc_buffers.next, struct fadc_buffer, list);
  return buffer;
}
//-------------------------------------------------------------------//
void fadc_buffer_renumber_listed (unsigned long base){
  struct fadc_buffer *buffer;
  list_for_each_entry(buffer, &fadc_buffers, list) {
  	buffer->id = base;
	base += 1;
  }
}
EXPORT_SYMBOL_GPL(fadc_buffer_renumber_listed);
//===================================================================//
int fadc_set_chip_data(struct fadc_chip *chip, void *data) {
  if (!chip)
	return -EINVAL;
  chip->chip_data = data;
  return 0;
}
EXPORT_SYMBOL_GPL(fadc_set_chip_data);
//-------------------------------------------------------------------//
void *fadc_get_chip_data(struct fadc_chip *chip) {
  return chip ? chip->chip_data : NULL;
}
EXPORT_SYMBOL_GPL(fadc_get_chip_data);
//-------------------------------------------------------------------//
int fadc_chip_add(struct fadc_chip *chip) {
  struct fadc_channel *channel;
  struct fadc_buffer *buffer;
  unsigned int i;
  unsigned int jj;
  int ret;

  if (!chip || !chip->dev || !chip->ops) {
  	printk("fadc_chip_add: Invalid Values\n");
	return -EINVAL;
  }
  mutex_lock(&fadc_chips_lock);

  // let's create a private adc data
  ret = chip->ops->request(chip);
  if (ret) {
	module_put(chip->ops->owner);
	goto out;
  } 

  ret = alloc_fadc_channels( chip->base_id, chip->nchannels);
  if (ret < 0) {
  	printk("fadc_chip_add: Channels Allocation Failed\n");
	goto out;
  }

  chip->channels = kzalloc(chip->nchannels * sizeof(*channel), GFP_KERNEL);
  if (!chip->channels) {
  	printk("fadc_chip_add: Channels Memory Allocation Failed\n");
	ret = -ENOMEM;
	goto out;
  }
  chip->base_id = ret;

  for (i = 0; i < chip->nchannels; ++i) {
	channel = &chip->channels[i];
	channel->chip = chip;
	channel->id = chip->base_id + i;
	channel->hwid = i;
	// printk("...Inserting for Base=%d ADC %u\n", chip->base, channel->hwid);
	radix_tree_insert(&fadc_channels_tree, channel->id, channel);

	// We also want to allocate the buffers here
	channel->buffers = kzalloc(chip->nbuf_per_channel * sizeof(*buffer), GFP_KERNEL);

	if (channel->buffers) {
		for (jj = 0; jj < chip->nbuf_per_channel; ++jj){
			buffer = &channel->buffers[jj];

			// Should we set flags here ?
			buffer->channel = channel;
			fadc_buffer_mutex_lock();
			INIT_LIST_HEAD(&buffer->list);
			list_add(&buffer->list, &fadc_buffers);
			fadc_buffer_mutex_unlock();
		}
		channel->nbuffers = chip->nbuf_per_channel;
		/*
		printk("Allocated %u buffers for channel %u\n", 
			channel->nbuffers, channel->id);
		*/
	} else {
		printk("Can't allocate buffers for channel %u\n", channel->id);
		channel->nbuffers = 0;
	}	
	mutex_lock(&fadc_channels_lock);
	INIT_LIST_HEAD(&channel->list);
	list_add(&channel->list, &fadc_channels);
	mutex_unlock(&fadc_channels_lock);
  }
  bitmap_set(allocated_fadc_channels, chip->base_id, chip->nchannels);
  INIT_LIST_HEAD(&chip->list);
  list_add(&chip->list, &fadc_chips);

  ret = 0;
  /*
  if (IS_ENABLED(CONFIG_OF))
	of_fadc_chip_add(chip);
  */
  for (i = 0; i < chip->nchannels; ++i) {
  	channel = &chip->channels[i];
	fadc_channel_sysfs_export(channel);
  }

out:
  mutex_unlock(&fadc_chips_lock);
  return ret;
}
EXPORT_SYMBOL_GPL(fadc_chip_add);
//-------------------------------------------------------------------//
int fadc_chip_remove(struct fadc_chip *chip) {
  unsigned int i;
  int ret = 0;

  mutex_lock(&fadc_chips_lock);

  if ( chip->ops ) {
  	chip->ops->free(chip);
  }

  for (i = 0; i < chip->nchannels; i++) {
	struct fadc_channel *channel = &chip->channels[i];
	if (test_bit(FADC_CHAN_REQUESTED, &channel->flags)) {
		ret = -EBUSY;
		goto out;
	}
  }

  for (i = 0; i < chip->nchannels; i++) {
  	struct fadc_channel *channel = &chip->channels[i];
	fadc_channel_sysfs_unexport(channel);
  }

  list_del_init(&chip->list);
  /*
  if (IS_ENABLED(CONFIG_OF))
	of_fadc_chip_remove(chip);
  */

  free_fadc_channels(chip);

out:
  mutex_unlock(&fadc_chips_lock);
  return ret;
}
EXPORT_SYMBOL_GPL(fadc_chip_remove);
//-------------------------------------------------------------------//
