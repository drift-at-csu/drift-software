/* Autoconf and/or portability configuration */
#include "config.h"
#include "port.h"
/* System includes */
#include <stdlib.h>
#include <stdio.h>
/* Reading File headers */
#include <unistd.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
/* Package includes */
#include "sim-config.h"
#include "arch.h"
#include "abstract.h"
#include "toplevel-support.h"
#include "sim-cmd.h"

/* Definitions */
#define ADCLVDS_ADDR_SPACE		512	/* ADCLVDS addr space size in bytes */
/* Addresses of the registers */
#define	ADCLVDS_REG_GCLR	0x000	/* Global Control Register (32bit)*/

#define ADCLVDS_REG_PHYR	0x004	/* PHY interface Register (32bit) */
#define ADCLVDS_BIT_PHYR_RESET		0x20000000
#define ADCLVDS_BIT_PHYR_BITSLIP	0x10000000
#define ADCLVDS_BIT_PHYR_DELAYINC	0x00001000

#define ADCLVDS_REG_TRGR	0x008	/* Trigger Register (32bit) */

#define ADCLVDS_REG_SPIR	0x1F0	/* SPI Register (32 bit) */
#define ADCLVDS_BIT_SPIR_READY	0x20000000

#define ADCLVDS_REG_SECR	0x1F4	/* Seconds Register (32 bit) */
#define ADCLVDS_REG_QNTR	0x1F8	/* Quanta Register (32 bit) RO */

#define ADCLVDS_ADDR_CHLENGTH	0x020	/* Length of each reserved section 8 words */

#define ADCLVDS_ADDR_CH1	0x010
#define ADCLVDS_ADDR_CH2	0x030
#define ADCLVDS_ADDR_CH3	0x050
#define ADCLVDS_ADDR_CH4	0x070
#define ADCLVDS_ADDR_CH5        0x090
#define ADCLVDS_ADDR_CH6        0x0B0
#define ADCLVDS_ADDR_CH7        0x0D0
#define ADCLVDS_ADDR_CH8        0x0F0


#define ADCLVDS_REG_CH_TLOWR	0x0	/* Ch1 Triger Low   */
#define ADCLVDS_REG_CH_THIGR	0x1	/* Ch1 Trigger High */
#define ADCLVDS_REG_CH_ADDRR	0x2	/* Ch1 Address Register */
#define ADCLVDS_REG_CH_AVRGR	0x3	/* Ch1 Average Control Register */
#define ADCLVDS_REG_CH_PEDSR	0x4	/* Ch1 Pedestal Register */

struct dev_adclvds {
  /* Registers Structure */
  struct{
	uint32_t	gclr;
	uint32_t	phyr;
	uint32_t	trgr;
	uint32_t	spir;

	uint32_t	ch_tlowr[8];
	uint32_t	ch_thigr[8];
	uint32_t	ch_addrr[8];
	uint32_t	ch_avrgr[8];
	uint32_t	ch_pedsr[8];
  } regs;

  /* Internal Counters */
  int		eye_position;


  /* Configuration */
  int		enabled;
  oraddr_t	baseaddr;

};
/* Forward Declarations */

/* --------------------------------------------------------------------------*/
/*!Read a byte from an external device

   To model Wishbone accurately, we always do this as a 4-byte access, with a
   mask for the bytes we don't want.

   Since this is only a byte, the endianess of the result is irrelevant.

   @note We are passed the device address, but we must convert it to a full
         address for external use, to allow the single upcall handler to
         decode multiple generic devices.

   @param[in] addr  The device address to read from (host endian).
   @param[in] dat   The device data structure

   @return  The byte read.                                                   */
/* --------------------------------------------------------------------------*/
static uint32_t adclvds_read_word (oraddr_t  addr, void *dat) {
  struct dev_adclvds *dev = (struct dev_adclvds *) dat;
  uint32_t value = 0;
  int ii = -1;
  int reg_number = -1;

  switch (addr) {
  case ADCLVDS_REG_GCLR:
  	value = dev->regs.gclr;
	fprintf(stderr, "adclvds: GCLR read 0x%"PRIx32 "\n",value);
	break;
  case ADCLVDS_REG_PHYR:
  	value = dev->regs.phyr;
	fprintf(stderr, "adclvds: PHYR read 0x%"PRIx32 "\n",value);
	break;
  case ADCLVDS_REG_TRGR:
	value = dev->regs.trgr;
	fprintf(stderr, "adclvds: TRGR read 0x%"PRIx32 "\n",value);
	break;
  case ADCLVDS_REG_SPIR:
  	// Reading From SPI Register
  	value = dev->regs.spir | ADCLVDS_BIT_SPIR_READY; // We're always ready
  	fprintf(stderr, "adclvds: SPIR read 0x%"PRIx32 "\n",value);
	break;
  case ADCLVDS_REG_SECR:
  	value = 0x0;
	fprintf(stderr, "adclvds: SECR read 0x%"PRIx32 "\n",value);
	break;
  case ADCLVDS_REG_QNTR:
  	value = 0x0;
	fprintf(stderr, "adclvds: QNTR read 0x%"PRIx32 "\n",value);
	break;
  default:
  	ii = -1;
  	if 	  (addr >= ADCLVDS_ADDR_CH1 && addr < (ADCLVDS_ADDR_CH1+ADCLVDS_ADDR_CHLENGTH) ) {
		ii = 0;
		reg_number = (addr - ADCLVDS_ADDR_CH1)/4;
	} else if (addr >= ADCLVDS_ADDR_CH2 && addr < (ADCLVDS_ADDR_CH2+ADCLVDS_ADDR_CHLENGTH) ) {
		ii = 1;
		reg_number = (addr - ADCLVDS_ADDR_CH2)/4;
	} else if (addr >= ADCLVDS_ADDR_CH3 && addr < (ADCLVDS_ADDR_CH3+ADCLVDS_ADDR_CHLENGTH) ) {
		ii = 2;
		reg_number = (addr - ADCLVDS_ADDR_CH3)/4;
	} else if (addr >= ADCLVDS_ADDR_CH4 && addr < (ADCLVDS_ADDR_CH4+ADCLVDS_ADDR_CHLENGTH) ) {
		ii = 3;
		reg_number = (addr - ADCLVDS_ADDR_CH4)/4;
	} else if (addr >= ADCLVDS_ADDR_CH5 && addr < (ADCLVDS_ADDR_CH5+ADCLVDS_ADDR_CHLENGTH) ) {
		ii = 4;
		reg_number = (addr - ADCLVDS_ADDR_CH5)/4;
	} else if (addr >= ADCLVDS_ADDR_CH6 && addr < (ADCLVDS_ADDR_CH6+ADCLVDS_ADDR_CHLENGTH) ) {
		ii = 5;
		reg_number = (addr - ADCLVDS_ADDR_CH6)/4;
	} else if (addr >= ADCLVDS_ADDR_CH7 && addr < (ADCLVDS_ADDR_CH7+ADCLVDS_ADDR_CHLENGTH) ) {
		ii = 6;
		reg_number = (addr - ADCLVDS_ADDR_CH7)/4;
	} else if (addr >= ADCLVDS_ADDR_CH8 && addr < (ADCLVDS_ADDR_CH8+ADCLVDS_ADDR_CHLENGTH) ) {
		ii = 7;
		reg_number = (addr - ADCLVDS_ADDR_CH8)/4;
	}
	if (ii < 0 || ii > 7) {
		fprintf(stderr, "adclvds: ADC %d is outside the range\n", ii);
		break;
	}
	if (reg_number < 0 || reg_number > 2) {
		fprintf(stderr, "adclvds: ADC %d , Reg %d doesn't exist\n", ii, reg_number);
		break;
	}
	// After this we should have a valid register combination
	switch (reg_number){
		case ADCLVDS_REG_CH_TLOWR:
			value = dev->regs.ch_tlowr[ii];
			break;
		case ADCLVDS_REG_CH_THIGR:
			value = dev->regs.ch_thigr[ii];
			break;
		case ADCLVDS_REG_CH_ADDRR:
			value = dev->regs.ch_addrr[ii];
			break;
		case ADCLVDS_REG_CH_AVRGR:
			value = dev->regs.ch_avrgr[ii];
			break;
		case ADCLVDS_REG_CH_PEDSR:
			value = dev->regs.ch_pedsr[ii];
			break;
		default:
			break;
	}
	break;
	// default ends here
  }
  return value;  
}
/* --------------------------------------------------------------------------*/
/*!Write a byte to an external device

   @note We are passed the device address, but we must convert it to a full
         address for external use, to allow the single upcall handler to
         decode multiple generic devices.

   @param[in] addr  The device address to write to (host endian)
   @param[in] value The byte value to write
   @param[in] dat   The device data structure                                */
/* --------------------------------------------------------------------------*/
static void adclvds_write_word (oraddr_t addr, uint32_t value, void  *dat){
  struct dev_adclvds *dev = (struct dev_adclvds *) dat;
  int ii = -1;
  int reg_number = -1;

  if (addr >= ADCLVDS_ADDR_SPACE) {
	fprintf (stderr, "Byte written out of range for ADCLVDS "
		"(addr 0x%" PRIxADDR")\n", addr);
	return;
  }
  
  switch (addr) {
  case ADCLVDS_REG_GCLR:
	fprintf(stderr, "adclvds: GCLR write 0x%"PRIx32 "\n",value);
	break;
  case ADCLVDS_REG_PHYR:
	fprintf(stderr, "adclvds: PHYR write 0x%"PRIx32 "\n",value);
	break;
  case ADCLVDS_REG_TRGR:
	fprintf(stderr, "adclvds: TRGR write 0x%"PRIx32 "\n",value);
	break;
  case ADCLVDS_REG_SPIR:
	// Writing to SPI Register
	fprintf(stderr, "adclvds: SPIR write 0x%"PRIx32 "\n",value);
	break;
  case ADCLVDS_REG_SECR:
	fprintf(stderr, "adclvds: SECR write 0x%"PRIx32 "\n",value);
	break;
  case ADCLVDS_REG_QNTR:
	fprintf(stderr, "adclvds: QNTR write 0x%"PRIx32 "\n",value);
	break;
  default:
	ii = -1;
	if	  (addr >= ADCLVDS_ADDR_CH1 && addr < (ADCLVDS_ADDR_CH1+ADCLVDS_ADDR_CHLENGTH) ) {
		ii = 0;
		reg_number = (addr - ADCLVDS_ADDR_CH1)/4;
	} else if (addr >= ADCLVDS_ADDR_CH2 && addr < (ADCLVDS_ADDR_CH2+ADCLVDS_ADDR_CHLENGTH) ) {
		ii = 1;
		reg_number = (addr - ADCLVDS_ADDR_CH2)/4;
	} else if (addr >= ADCLVDS_ADDR_CH3 && addr < (ADCLVDS_ADDR_CH3+ADCLVDS_ADDR_CHLENGTH) ) {
		ii = 2;
		reg_number = (addr - ADCLVDS_ADDR_CH3)/4;
	} else if (addr >= ADCLVDS_ADDR_CH4 && addr < (ADCLVDS_ADDR_CH4+ADCLVDS_ADDR_CHLENGTH) ) {
		ii = 3;
		reg_number = (addr - ADCLVDS_ADDR_CH4)/4;
	} else if (addr >= ADCLVDS_ADDR_CH5 && addr < (ADCLVDS_ADDR_CH5+ADCLVDS_ADDR_CHLENGTH) ) {
		ii = 4;
		reg_number = (addr - ADCLVDS_ADDR_CH5)/4;
	} else if (addr >= ADCLVDS_ADDR_CH6 && addr < (ADCLVDS_ADDR_CH6+ADCLVDS_ADDR_CHLENGTH) ) {
		ii = 5;
		reg_number = (addr - ADCLVDS_ADDR_CH6)/4;
  	} else if (addr >= ADCLVDS_ADDR_CH7 && addr < (ADCLVDS_ADDR_CH7+ADCLVDS_ADDR_CHLENGTH) ) {
		ii = 6;
		reg_number = (addr - ADCLVDS_ADDR_CH7)/4;
	} else if (addr >= ADCLVDS_ADDR_CH8 && addr < (ADCLVDS_ADDR_CH8+ADCLVDS_ADDR_CHLENGTH) ) {
		ii = 7;
		reg_number = (addr - ADCLVDS_ADDR_CH8)/4;
	}
	if (ii < 0 || ii > 7) {
		fprintf(stderr, "adclvds: ADC %d is outside the range\n", ii);
		break;
	}
	if (reg_number < 0 || reg_number > 2) {
		fprintf(stderr, "adclvds: ADC %d , Reg %d doesn't exist\n", ii, reg_number);
		break;
	}
	switch (reg_number){
	case ADCLVDS_REG_CH_TLOWR:
		dev->regs.ch_tlowr[ii] = value;
		break;
	case ADCLVDS_REG_CH_THIGR:
		dev->regs.ch_thigr[ii] = value;
		break;
	case ADCLVDS_REG_CH_ADDRR:
		dev->regs.ch_addrr[ii] = value;
		break;
	case ADCLVDS_REG_CH_AVRGR:
		// value = dev->regs.ch_avrgr[ii];
		break;
	case ADCLVDS_REG_CH_PEDSR:
		// value = dev->regs.ch_pedsr[ii];
		break;
	default:
		break;
	}
	break;
  }
}
/* --------------------------------------------------------------------------*/
/* Reset is a null operation */
static void adclvds_reset (void *dat) {
  struct dev_adclvds *dev = (struct dev_adclvds *) dat; 

/*
  if (dev->file1_buffer != NULL) {
  	free(dev->file1_buffer);
	dev->file1_buffer = NULL;
  }
  dev->file1_buffer = (uint8_t *)spiflash_open_file_if (dev->file1_name, &(dev->file1_size));
*/
}
//-------------------------------------------------------------------//
static void adclvds_enabled (union param_val val, void *dat) {
  ((struct dev_adclvds *) dat)->enabled = val.int_val;

}
//-------------------------------------------------------------------//
static void adclvds_baseaddr (union param_val val, void *dat) {
  ((struct dev_adclvds *) dat)->baseaddr = val.addr_val;

}
//-------------------------------------------------------------------//
/* Status report can only advise of configuration. */
static void adclvds_status (void *dat) {
  struct dev_adclvds *dev = (struct dev_adclvds *) dat;

  PRINTF ("\nADCLVDS device at 0x%" PRIxADDR ":\n", dev->baseaddr);
  PRINTF ("\n");
}

//-------------------------------------------------------------------//
//-------------------------------------------------------------------//
/* Start of new generic section */
static void *adclvds_sec_start (){

  struct dev_adclvds *new = (struct dev_adclvds *) 
  	malloc (sizeof (struct dev_adclvds));
  if (0 == new) {
	fprintf (stderr, "SPIFLASH==> Run out of memory\n");
	exit (-1);
  }
  /* Default names */
  new->enabled = 1;
  new->baseaddr = 0;

  /* Set Default settings here, like this:
  new->regs.spcr = 0x00;
  new->regs.spsr = 0x00;
  new->regs.spdr = 0x00;
  new->regs.sper = 0x00;
  */

  return new;
}
//-------------------------------------------------------------------//
/* End of new SPI section */
static void adclvds_sec_end (void *dat) {
  struct dev_adclvds *dev = (struct dev_adclvds *) dat;
  struct mem_ops ops;

  /* Give up if not enabled */
  if (!dev->enabled) {
	free (dat);
	return;
  }

  /* Zero all the ops, then set the ones we care about. */
  memset (&ops, 0, sizeof (struct mem_ops));

  ops.readfunc32 = adclvds_read_word;
  ops.writefunc32 = adclvds_write_word;
  ops.read_dat32 = dat;
  ops.write_dat32 = dat;

  /* Register everything */
  reg_mem_area (dev->baseaddr, ADCLVDS_ADDR_SPACE, 0, &ops);
  reg_sim_reset (adclvds_reset, dat);
  reg_sim_stat (adclvds_status, dat);
}
//-------------------------------------------------------------------//
/* Register a generic section. */
void reg_adclvds_sec (void) {
  struct config_section *sec = reg_config_sec ("adclvds",
				adclvds_sec_start,
				adclvds_sec_end);

  reg_config_param (sec, "enabled",		PARAMT_INT,	adclvds_enabled);
  reg_config_param (sec, "baseaddr",		PARAMT_ADDR,	adclvds_baseaddr);
}
//-------------------------------------------------------------------//
