/* Autoconf and/or portability configuration */
#include "config.h"
#include "port.h"
/* System includes */
#include <stdlib.h>
#include <stdio.h>
/* Reading File headers */
#include <unistd.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
/* Package includes */
#include "sim-config.h"
#include "arch.h"
#include "sim-config.h"
#include "abstract.h"
#include "toplevel-support.h"
#include "sim-cmd.h"

/* Definitions */
#define SPI_ADDR_SPACE		5	/* SPIFLASH addr space size in bytes */
/* Addresses of the registers */
#define	SPI_REG_SPCR	0x00	/* Serial Perepherial Control Register (8bit)*/
#define SPI_REG_SPSR	0x01	/* Serial Perepherial Status Register (8bit) */
#define SPI_REG_SPDR	0x02	/* Serial Perepherial Data Register (8bit) */
#define SPI_REG_SPER	0x03	/* Serial Perepherial Extensions Register (8bit) */

/* SPCR register */
#define SPI_SPCR_SPIE	0x80	/* Interrupt Enable */
#define SPI_SPCR_SPE	0x40	/* Core Enable */	
#define SPI_SPCR_MSTR	0x10	/* Master Mode Select */
#define SPI_SPCR_CPOL	0x08	/* Clock Polarity */
#define SPI_SPCR_CPHA	0x04	/* Clock Phase */
#define SPI_SPCR_SPR1	0x02	/* Clock Rate (2bits)*/
#define SPI_SPCR_SPR0	0x01	/* */

/* SPSR register */
#define SPI_SPSR_SPIF		0x80	/* Serial Perepherial interrupt flag */
#define SPI_SPSR_WCOL		0x40	/* Write Collision */
#define SPI_SPSR_WFFULL		0x08	/* Write FIFO Full */
#define SPI_SPSR_WFEMPTY	0x04	/* Write FIFO Empty */
#define SPI_SPSR_RFFULL		0x02	/* Read FIFO Full */
#define SPI_SPSR_RFEMPTY	0x01	/* Read FIFO Empty */

/* SPER register */
#define SPI_SPER_ICNT1	0x80	/* Interrupt Count */
#define SPI_SPER_ICNT0	0x40	/* conts after how many transfers to set int */
#define SPI_SPER_ESPR1	0x02	/* SPI Clock Rates (2bits) */
#define SPI_SPER_ESPR0	0x01	/* */


struct dev_spiflash {
  /* Simple SPI registers */
  struct{
	uint8_t		spcr;
	uint8_t		spsr;
	uint8_t		spdr;
	uint8_t		sper;
  } regs;
  /* InternalSimple SPI Counters */
  uint8_t  	rx_fifo [4];
  int		nofrx_fifo;	/* This is a number of pending reads in RX FIFO */
  /* Flash internal registers */
  struct{
  	enum {	BYTE3_ADDR_MODE,
		BYTE4_ADDR_MODE
	} current_mode;
	enum {
		SPIFLASH_READID,
		SPIFLASH_READINSTRUCTION,
		SPIFLASH_READMEMORY,
		SPIFLASH_IDLE
	} current_state; /* State Machine of the SPI */
	uint32_t	current_addr; /* This is the current addres of transfered byte */
	int		addr_counter; /* If we are recieving address, this counter increments */
  } flash;


  uint8_t	* file1_buffer;
  ssize_t	file1_size;

  uint8_t	* file2_buffer;
  ssize_t	file2_size;

  uint8_t	* file3_buffer;
  ssize_t	file3_size;
  /* Configuration */
  int		enabled;
  oraddr_t	baseaddr;
  uint32_t	flash_id;

  char  	*file1_name;
  uint32_t	file1_offset;

  char		*file2_name;
  uint32_t	file2_offset;

  char		*file3_name;
  uint32_t	file3_offset;
};
/* Forward Declarations */
uint8_t *spiflash_open_file_if (char *file_name, ssize_t *file_size);
uint8_t spiflash_read_byte_from_file (struct dev_spiflash *dev);

/* --------------------------------------------------------------------------*/
/*!Read a byte from an external device

   To model Wishbone accurately, we always do this as a 4-byte access, with a
   mask for the bytes we don't want.

   Since this is only a byte, the endianess of the result is irrelevant.

   @note We are passed the device address, but we must convert it to a full
         address for external use, to allow the single upcall handler to
         decode multiple generic devices.

   @param[in] addr  The device address to read from (host endian).
   @param[in] dat   The device data structure

   @return  The byte read.                                                   */
/* --------------------------------------------------------------------------*/
static uint8_t spiflash_read_byte (oraddr_t  addr, void *dat) {
  struct dev_spiflash *dev = (struct dev_spiflash *) dat;
  uint8_t value = 0;
  int ii;

  switch (addr) {
  case SPI_REG_SPCR:
  	value = dev->regs.spcr;
	// fprintf(stderr, "spiflash:SPCR read 0x%"PRIx32 "\n",value);
	break;
  case SPI_REG_SPSR:
	value = 0x00;
	if (dev->nofrx_fifo > 3)
		value |= SPI_SPSR_RFFULL;
	if (dev->nofrx_fifo == 0)
		value |= SPI_SPSR_RFEMPTY;
	// Always empty Write FIFO
	// TODO: Should we change this behavior ?
		value |= SPI_SPSR_WFEMPTY;
	
	// SPI_SPSR_WCOL always 0
	// SPI_SPSR_SPIF always 0
	
	// fprintf(stderr, "spiflash:SPSR read 0x%"PRIx8 "\n",value);
	break;
  case SPI_REG_SPDR:
	// Now let's assign value
	value = dev->rx_fifo[0];
	// And Shift it out of fifo
	for (ii = 0; ii < dev->nofrx_fifo; ++ii){
		dev->rx_fifo[ii]=dev->rx_fifo[ii+1];
	}
	dev->nofrx_fifo--;
	// fprintf(stderr, "spiflash:SPDR read 0x%"PRIx32 "\n",value);
	break;
  case SPI_REG_SPER:
  	value = dev->regs.sper;
	// fprintf(stderr, "spiflash:SPER read 0x%"PRIx32 "\n",value);
	break;
  }
  return value;  
}
/* --------------------------------------------------------------------------*/
/*!Write a byte to an external device

   @note We are passed the device address, but we must convert it to a full
         address for external use, to allow the single upcall handler to
         decode multiple generic devices.

   @param[in] addr  The device address to write to (host endian)
   @param[in] value The byte value to write
   @param[in] dat   The device data structure                                */
/* --------------------------------------------------------------------------*/
static void spiflash_write_byte (oraddr_t addr, uint8_t value, void  *dat){
  struct dev_spiflash *dev = (struct dev_spiflash *) dat;
  /*
  if (addr >= dev->size) {
	fprintf (stderr, "Byte written out of range for SPIFLASH %s "
		"(addr 0x%" PRIxADDR")\n", dev->name, addr);
	return;
  }
  */
  switch (addr) {
  case SPI_REG_SPCR:
  	// fprintf(stderr, "spiflash:SPCR write 0x%"PRIx32 "\n",value);
	if ((value & SPI_SPCR_SPIE)&&(!(dev->regs.spcr & SPI_SPCR_SPIE)) ){
		fprintf(stderr, "SPI==> Enabling Interrupt\n");
	}
	if ((!(value & SPI_SPCR_SPIE))&&(dev->regs.spcr & SPI_SPCR_SPIE) ){
		fprintf(stderr, "SPI==> Disabling Interrupt\n");
	}
	if ((value & SPI_SPCR_SPE)&&(!(dev->regs.spcr & SPI_SPCR_SPE)) ){
		fprintf(stderr, "SPI==> Enabling Core\n");
		dev->flash.current_addr = 0;
		dev->flash.current_state = SPIFLASH_IDLE;
	}
	if ((!(value & SPI_SPCR_SPE))&&(dev->regs.spcr & SPI_SPCR_SPE) ){
		fprintf(stderr, "SPI==> Disabling Core\n");
		dev->flash.current_addr = 0;
		dev->flash.current_state = SPIFLASH_IDLE;
	}
	if ((value & SPI_SPCR_MSTR)&&(!(dev->regs.spcr & SPI_SPCR_MSTR)) ){
		fprintf(stderr, "SPI==> Switching to Master Mode\n");
	}
	if ((!(value & SPI_SPCR_MSTR))&&(dev->regs.spcr & SPI_SPCR_MSTR) ){
		fprintf(stderr, "SPI==> Switching to Slave Mode\n");
	}
	if ((value & SPI_SPCR_CPOL)&&(!(dev->regs.spcr & SPI_SPCR_CPOL)) ){
		fprintf(stderr, "SPI==> Switching to Positive Clock Polarity\n");
	}
	if ((!(value & SPI_SPCR_CPOL))&&(dev->regs.spcr & SPI_SPCR_CPOL) ){
		fprintf(stderr, "SPI==> Switching to Negative Clock Polarity\n");
	}
	if ((value & SPI_SPCR_CPHA)&&(!(dev->regs.spcr & SPI_SPCR_CPHA)) ){
		fprintf(stderr, "SPI==> Switching to Positive Clock Phase\n");
	}
	if ((!(value & SPI_SPCR_CPHA))&&(dev->regs.spcr & SPI_SPCR_CPHA) ){
		fprintf(stderr, "SPI==> Switching to Negative Clock Phase\n");
	}
	if ( (value & (SPI_SPCR_SPR1|SPI_SPCR_SPR1 )) !=  
		(dev->regs.spcr & (SPI_SPCR_SPR1|SPI_SPCR_SPR1 )) ){
		fprintf(stderr, "SPI==> Changing Clock Rate\n");
	}
	dev->regs.spcr = value;
  	break;
  case SPI_REG_SPSR:
  	fprintf(stderr, "SPI==> SPSR write 0x%"PRIx32 "\n",value);
	dev->regs.spsr = value;
	break;
  case SPI_REG_SPDR:
  	// fprintf(stderr, "spiflash:SPDR write 0x%"PRIx32 "\n",value);
	if (dev->nofrx_fifo > 3) {
		fprintf(stderr, "SPI==> ERROR: RX FIFO Overrun!!!\n");
		dev->nofrx_fifo = 0;
	}
	/* Depends on what state we're in */
	dev->regs.spdr = value;
	if (dev->flash.current_state == SPIFLASH_IDLE) {
		// We are in IDLE mode, ready for instructions
		// Add different possibilities here
		if ((dev->regs.spdr == 0x9E)||(dev->regs.spdr == 0x9F)) {
			dev->flash.current_state = SPIFLASH_READID;
			dev->flash.current_addr = 0;
			// We want to transmit something to RX FIFO:
			fprintf(stderr, "FLASH==> READ ID Instruction\n");
		} else if (dev->regs.spdr == 0x06) {
			fprintf(stderr, "FLASH==> WRITE ENABLE  Instruction\n");
		} else if (dev->regs.spdr == 0x04) {
			fprintf(stderr, "FLASH==> WRITE DISABLE Instruction\n");
		} else if (dev->regs.spdr == 0xB7) {
			fprintf(stderr, "FLASH==> ENTER 4B ADDR MODE Instruction\n");
			dev->flash.current_mode = BYTE4_ADDR_MODE;
		} else if (dev->regs.spdr == 0x05) {
			// fprintf(stderr, "FLASH==> READ STATUS REGISTER Instruction\n");
			dev->flash.current_state = SPIFLASH_READINSTRUCTION;
			dev->flash.current_addr = dev->regs.spdr;
		} else if ((dev->regs.spdr == 0x0B)||(dev->regs.spdr == 0x03)) {
			// fprintf(stderr, "FLASH==> FAST READ Instruction\n");
			dev->flash.current_state = SPIFLASH_READMEMORY;
			dev->flash.current_addr = 0x00000000;
			dev->flash.addr_counter = 0;
		} else {
			fprintf(stderr, "===> spiflash: Unknown Instruction 0x%"
				PRIx8 "\n",dev->regs.spdr);
		}
		dev->rx_fifo[dev->nofrx_fifo] = 0xFF;
	} else if (dev->flash.current_state == SPIFLASH_READID) {
		// We are in READ ID mode
		dev->rx_fifo[dev->nofrx_fifo] =
			(dev->flash_id >> (dev->flash.current_addr*8))&0xFF;
		dev->flash.current_addr += 1;
		if (dev->flash.current_addr > 4) {
			// We transmitted The ID, go back to idle
			dev->flash.current_addr = 0;
			dev->flash.current_state = SPIFLASH_IDLE;
		}
	} else if (dev->flash.current_state == SPIFLASH_READINSTRUCTION){
		// We are in READ INSTRUCTION mode, the instruction code was
		// stored in dev->flash.current_addr
		if (dev->flash.current_addr == 0x05) {
			// Flash Status register:
			// bit 0 -> Write in progress 0=ready
			// bit 1 -> Write enable latch 0=cleared
			// bit 6,4,3,2 -> Block Protect
			// bit 5 -> Top/bottom 0=Top
			// bit 7 -> Status Register write Enable 0=enabled
			dev->rx_fifo[dev->nofrx_fifo] = 0x00;
			dev->flash.current_state = SPIFLASH_IDLE;
		} else {
			fprintf(stderr, "FLASH==> Unknown Instruction\n");
			dev->rx_fifo[dev->nofrx_fifo] = 0xFF;
			dev->flash.current_state = SPIFLASH_IDLE;
		}
	} else if ( dev->flash.current_state == SPIFLASH_READMEMORY){
		// In this mode we expect the address first (4 or 3 bytes)
		// And then after 1 dummy cycle we're supplying data
		unsigned short addr_mode = 3;
		if ( dev->flash.current_mode == BYTE4_ADDR_MODE) {
			addr_mode = 4;
		}

		if (dev->flash.addr_counter < addr_mode) {
			dev->flash.current_addr |= 
				( dev->regs.spdr << ((addr_mode - 1 - dev->flash.addr_counter)*8)  );
			dev->flash.addr_counter += 1;
			dev->rx_fifo[dev->nofrx_fifo] = 0x00;
		/*
		} else if ((dev->flash.addr_counter < addr_mode + 1)&&
				(addr_mode == 4)) {
			dev->rx_fifo[dev->nofrx_fifo] = 0x00;
			dev->flash.addr_counter += 1;
			fprintf( stderr, "FR[0x%"PRIxADDR"-", dev->flash.current_addr);
		*/
		} else {
			if ((addr_mode == 3)&&(dev->flash.addr_counter == 3)) {
				fprintf( stderr, "R3[0x%"PRIxADDR"-", dev->flash.current_addr);

				dev->flash.addr_counter +=1;
			} 
			if ((addr_mode == 4)&&(dev->flash.addr_counter == 4)) {
				fprintf( stderr, "R4[0x%"PRIxADDR"-", dev->flash.current_addr);
				dev->flash.addr_counter +=1;
			}
			// We're supplying data
			dev->rx_fifo[dev->nofrx_fifo] =
				spiflash_read_byte_from_file(dev);

			/*	
			fprintf( stderr, "FLASH==> Providing 0x%"PRIx8 " at 0x%"
				PRIxADDR"\n",
				dev->rx_fifo[dev->nofrx_fifo], 
				dev->flash.current_addr );
			*/
			
			 
			
			
			// Incrementing address for next read
			dev->flash.current_addr += 1;

			if (dev->regs.spdr != 0x00) {
				fprintf( stderr, "0x%"PRIxADDR"] ", dev->flash.current_addr);
				/*
				fprintf( stderr, "===>FLASH FAST Read Ends at ADDR 0x%"
					PRIxADDR"\n", dev->flash.current_addr);
				*/
				// Let's switch to this instruction then

				dev->flash.current_state = SPIFLASH_READINSTRUCTION;
				dev->flash.current_addr = dev->regs.spdr;
			}
		}
	} else {
		// Writing to this register is not valid otherwise
		// fprintf(stderr, "spiflash:Error SPDR write 0x%"PRIx8" in non-idle-state\n",
		//	value);
		dev->rx_fifo[dev->nofrx_fifo] = 0xEE;
		fprintf(stderr, "SPI==> ERROR: In Unknown Mode 0x%"
			PRIx8 "\n",dev->regs.spdr);
	}
	dev->nofrx_fifo++;
	break;
  case SPI_REG_SPER:
  	// fprintf(stderr, "spiflash:SPER write 0x%"PRIx8 "\n",value);
	if ( (value & (SPI_SPER_ICNT1|SPI_SPER_ICNT0 )) !=
		(dev->regs.sper & (SPI_SPER_ICNT1|SPI_SPER_ICNT0 )) ){
		fprintf(stderr, "SPI==> Changing Interrupt Count\n");
	}
	if ( (value & (SPI_SPER_ESPR1|SPI_SPER_ESPR0 )) !=
		(dev->regs.sper & (SPI_SPER_ESPR1|SPI_SPER_ESPR0 )) ){
		fprintf(stderr, "SPI==> Changing Extended Clock Rate\n");
	}
	dev->regs.sper = value;
	break;
  }
    
}
/* --------------------------------------------------------------------------*/
/* Reset is a null operation */
static void spiflash_reset (void *dat) {
  struct dev_spiflash *dev = (struct dev_spiflash *) dat; 

  if (dev->file1_buffer != NULL) {
  	free(dev->file1_buffer);
	dev->file1_buffer = NULL;
  }
  dev->file1_buffer = (uint8_t *)spiflash_open_file_if (dev->file1_name, &(dev->file1_size));

  if (dev->file2_buffer != NULL) {
	free(dev->file2_buffer);
	dev->file2_buffer = NULL;
  }
  dev->file2_buffer = (uint8_t *)spiflash_open_file_if (dev->file2_name, &(dev->file2_size));

  if (dev->file3_buffer != NULL) {
	free(dev->file3_buffer);
	dev->file3_buffer = NULL;
  }
  dev->file3_buffer = (uint8_t *)spiflash_open_file_if (dev->file3_name, &(dev->file3_size));
}
//-------------------------------------------------------------------//
uint8_t spiflash_read_byte_from_file (struct dev_spiflash *dev){
  // The Address is supposed to be written to dev->flash.current_addr
  // We need to calculate difference with the files offsets

  uint8_t value = 0xFF;
  int offset = dev->flash.current_addr - dev->file1_offset;
  if ((offset < dev->file1_size)&&(offset >= 0)) {
  	value = dev->file1_buffer[offset];
	return value;
  }
  // fprintf( stderr, "FLASH==> Not in first, offset = %d \n", offset);  
  offset = dev->flash.current_addr - dev->file2_offset;
  if ((offset < dev->file2_size)&&(offset >= 0)) {
  	value = dev->file2_buffer[offset];
	return value;
  }
  // fprintf( stderr, "FLASH==> Not in second, offset = %d \n", offset);
  offset = dev->flash.current_addr - dev->file3_offset;
  if ((offset < dev->file3_size)&&(offset >= 0)) {
	value = dev->file3_buffer[offset];
        return value;
  }
  // fprintf( stderr, "FLASH==> Not in third , offset = %d \n", offset);

  return 0xFF;
}
//-------------------------------------------------------------------//
static void spiflash_enabled (union param_val val, void *dat) {
  ((struct dev_spiflash *) dat)->enabled = val.int_val;

}
//-------------------------------------------------------------------//
static void spiflash_baseaddr (union param_val val, void *dat) {
  ((struct dev_spiflash *) dat)->baseaddr = val.addr_val;

}
//-------------------------------------------------------------------//
static void  spiflash_flash_id (union param_val val, void *dat) {
  ((struct dev_spiflash *) dat)->flash_id = val.int_val;
}
//-------------------------------------------------------------------//
/* Status report can only advise of configuration. */
static void spiflash_status (void *dat) {
  struct dev_spiflash *dev = (struct dev_spiflash *) dat;

  PRINTF ("\nSPIFLASH device at 0x%" PRIxADDR ":\n", dev->baseaddr);
  PRINTF ("\n");
}

//-------------------------------------------------------------------//
uint8_t *spiflash_open_file_if (char *file_name, ssize_t *file_size){
  // Let's Dump this file to the memory
  uint8_t *result = NULL;

  int file_id = -1;
  file_id = open (file_name, O_RDONLY);
  if (file_id < 0) {
	fprintf (stderr, "Warning: Cannot open SPIFLASH file \"%s\": %s\n",
		file_name, strerror(errno));
	return result;
  }
  (*file_size) = 0;
  // Let's figure out the length
  off_t offset = 0;
  uint8_t value;
  while (read(file_id, &value, sizeof(value)) > 0){
	(*file_size) += 1;
  }
  fprintf (stderr, "==>SPIFLASH: Allocating %d bytes in Memory\n",
  	(uint32_t)(*file_size));

  result = malloc((*file_size));
  if (result==NULL){
  	fprintf (stderr, "==>SPIFLASH: Out of Memory\n");
	exit (1);
  }
  // fprintf (stderr, "==>SPIFLASH: Memory allocated.\n");
  if (pread (file_id, result, (*file_size), offset) < 0) {
  	fprintf (stderr, "==>SPIFLASH: Can't Read SPIFLASH file second time\n");
	free (result);
	exit(1);
  }
  fprintf (stderr, "==>SPIFLASH: File \"%s\" loaded\n", file_name);
  close(file_id);

  return result;
}
//-------------------------------------------------------------------//
static void spiflash_flash_file1_name(union param_val val, void *dat) {
  struct dev_spiflash *dev = (struct dev_spiflash *) dat;
  if (NULL != dev->file1_name) {
	free (dev->file1_name);
	dev->file1_name = NULL;
  }
  if (!(dev->file1_name = strdup (val.str_val))) {
	fprintf (stderr, "===>SPIFLASH: Run out of memory\n");
	exit (-1);
  }
}
//-------------------------------------------------------------------//
static void spiflash_flash_file1_offset(union param_val val, void *dat) {
  ((struct dev_spiflash *) dat)->file1_offset = val.addr_val;
}
//-------------------------------------------------------------------//
static void spiflash_flash_file2_name(union param_val val, void *dat) {
  struct dev_spiflash *dev = (struct dev_spiflash *) dat;
  if (NULL != dev->file2_name) {
	free (dev->file2_name);
	dev->file2_name = NULL;
  }
  if (!(dev->file2_name = strdup (val.str_val))) {
	fprintf (stderr, "===>SPIFLASH: Run out of memory\n");
	exit (-1);
  }
}
//-------------------------------------------------------------------//
static void spiflash_flash_file2_offset(union param_val val, void *dat) {
  ((struct dev_spiflash *) dat)->file2_offset = val.addr_val;
}
//-------------------------------------------------------------------//
static void spiflash_flash_file3_name(union param_val val, void *dat) {
  struct dev_spiflash *dev = (struct dev_spiflash *) dat;
  if (NULL != dev->file3_name) {
	free (dev->file3_name);
	dev->file3_name = NULL;
  }
  if (!(dev->file3_name = strdup (val.str_val))) {
	fprintf (stderr, "===>SPIFLASH: Run out of memory\n");
	exit (-1);
  }
}
//-------------------------------------------------------------------//
static void spiflash_flash_file3_offset(union param_val val, void *dat) {
  ((struct dev_spiflash *) dat)->file3_offset = val.addr_val;
}
//-------------------------------------------------------------------//
/* Start of new generic section */
static void *spiflash_sec_start (){

  struct dev_spiflash *new = (struct dev_spiflash *) 
  	malloc (sizeof (struct dev_spiflash));
  if (0 == new) {
	fprintf (stderr, "SPIFLASH==> Run out of memory\n");
	exit (-1);
  }
  /* Default names */
  new->enabled = 1;
  new->baseaddr = 0;
  new->flash_id = 0xFFFFFFFF;

  new->file1_name = NULL;
  new->file1_offset = 0x00000000;
  new->file2_name = NULL;
  new->file2_offset = 0x00000000;
  new->file3_name = NULL;
  new->file3_offset = 0x00000000;

  new->file1_buffer = NULL;
  new->file1_size = 0;
  new->file2_buffer = NULL;
  new->file2_size = 0;
  new->file3_buffer = NULL;
  new->file3_size = 0;

  new->regs.spcr = 0x00;
  new->regs.spsr = 0x00;
  new->regs.spdr = 0x00;
  new->regs.sper = 0x00;

  new->nofrx_fifo = 0;

  new->flash.current_mode = BYTE3_ADDR_MODE;
  new->flash.current_state = SPIFLASH_IDLE;
  new->flash.current_addr = 0x00000000;

  return new;
}
//-------------------------------------------------------------------//
/* End of new SPI section */
static void spiflash_sec_end (void *dat) {
  struct dev_spiflash *dev = (struct dev_spiflash *) dat;
  struct mem_ops ops;

  /* Give up if not enabled */
  if (!dev->enabled) {
	free (dat);
	return;
  }

  /* Zero all the ops, then set the ones we care about. */
  memset (&ops, 0, sizeof (struct mem_ops));

  ops.readfunc8 = spiflash_read_byte;
  ops.writefunc8 = spiflash_write_byte;
  ops.read_dat8 = dat;
  ops.write_dat8 = dat;

  /* Register everything */
  reg_mem_area (dev->baseaddr, SPI_ADDR_SPACE, 0, &ops);
  reg_sim_reset (spiflash_reset, dat);
  reg_sim_stat (spiflash_status, dat);
}
//-------------------------------------------------------------------//
/* Register a generic section. */
void reg_spiflash_sec (void) {
  struct config_section *sec = reg_config_sec ("spiflash",
				spiflash_sec_start,
				spiflash_sec_end);

  reg_config_param (sec, "enabled",		PARAMT_INT,	spiflash_enabled);
  reg_config_param (sec, "baseaddr",		PARAMT_ADDR,	spiflash_baseaddr);
  reg_config_param (sec, "flash_id",		PARAMT_ADDR,	spiflash_flash_id);

  reg_config_param (sec, "flash_file1_name",	PARAMT_STR,	spiflash_flash_file1_name);
  reg_config_param (sec, "flash_file1_offset",	PARAMT_ADDR,	spiflash_flash_file1_offset);
  reg_config_param (sec, "flash_file2_name",    PARAMT_STR,     spiflash_flash_file2_name);
  reg_config_param (sec, "flash_file2_offset",  PARAMT_ADDR,    spiflash_flash_file2_offset);
  reg_config_param (sec, "flash_file3_name",    PARAMT_STR,     spiflash_flash_file3_name);
  reg_config_param (sec, "flash_file3_offset",  PARAMT_ADDR,    spiflash_flash_file3_offset);
}
//-------------------------------------------------------------------//
