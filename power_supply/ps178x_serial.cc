#include "ps178x_serial.h"

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fcntl.h>
#include <unistd.h>
#include <termios.h>
#include <sys/ioctl.h>
using namespace std;

//-------------------------------------------------------------------//
ps178x_serial::ps178x_serial():
		fFileID(-1) {
  fOutBuf[0] = 0xAA; // It Should always be 0xAA
}
//-------------------------------------------------------------------//
ps178x_serial::~ps178x_serial(){
  if (!(fFileID < 0)) close(fFileID);
}
//-------------------------------------------------------------------//
int ps178x_serial::Init( std::string aSerialName ) {
  // Just in case we had it open
  if (!(fFileID < 0)) close(fFileID);

  fFileID = open(aSerialName.c_str(), O_RDWR | O_NOCTTY /*| O_NDELAY*/);
 
  if (fFileID < 0) {
  	cerr << "...Can't open device \"" << aSerialName << "\"" << endl;
	return -1;
  }
  struct termios options;
  fcntl(fFileID, F_SETFL, FNDELAY);
  tcgetattr(fFileID, &options);
  cfsetospeed(&options, B9600);	// Set BaudRate
  options.c_cflag |= (CLOCAL | CREAD);
  options.c_cflag &= ~(PARENB | PARODD); // no parity
  options.c_cflag &= ~CSTOPB; //1 stop bit
  options.c_cflag &= ~CSIZE; // Mask the character size bits 
  options.c_cflag |= CS8; //8 bit words
  options.c_cflag &= ~CRTSCTS;  //no hardware handshake

  /* Set Raw Input */
  options.c_lflag     &= ~(ICANON | ECHO | ECHOE | ISIG);
  options.c_oflag     &= ~OPOST;

  options.c_cc[VMIN]  = 0; 	// VMIN specifies the minimum number of characters to read. 
  				// If it is set to 0, then the VTIME value specifies the 
				// time to wait for every character read.
  options.c_cc[VTIME] = 1; 	// VTIME specifies the amount of time to wait for incoming 
  				// characters in tenths of seconds.
				// If VTIME is set to 0 (the default), calls to read will 
				// block (wait) indefinitely unless the NDELAY option 
				// is set on the port with open or fcntl.
  // Write our changes to the port configuration
  tcsetattr(fFileID, TCSANOW, &options);
  // ioctl( fFileID, TCFLSH, TCIOFLUSH );
  return 0;
}
//-------------------------------------------------------------------//
int ps178x_serial::SubmitCommand() {
  if (fFileID < 0){ // Means Serial device file is not opened
	cerr << "...Open Serial device first " << endl;
	return -1;
  }
  // before sending the command one would need to calculate CRC
  fOutBuf[25] = 0;
  for (int ii = 0; ii < 25; ++ii) {
  	fOutBuf[25] += fOutBuf[ii];
  }
  // Send packet and flush serial port
  int bytes;
  bytes = write( fFileID, fOutBuf, 26 ); // ioctl(fFileID, TCFLSH, 2);
  // ioctl( fFileID, TCFLSH, TCOFLUSH );
  tcflush(fFileID, TCOFLUSH);
  // cerr << "...Write results in " << bytes << " bytes" << endl;
  // fsync(fFileID);

  // Let's read the packet from serial port
  bytes = 0;
  int noftry = 100;
  while (noftry > 0) {
	usleep(1000);
	// ioctl( fFileID, TCFLSH, TCIFLUSH );
	ioctl(fFileID, FIONREAD, &bytes);
	// cerr << "...Bytes to read " << bytes << endl;
	/*
	if (bytes == 0) 
		ioctl( fFileID, TCFLSH, TCIFLUSH );
	*/
	if ( bytes < 26 )
		noftry --;
	else
		break;
  }

  if (noftry == 0) {
  	cerr << "...Response timeout" << endl;
  	return -2;
  }

  bytes = read(fFileID, fInpBuf, 26);
  // cerr << "...Read results in " << bytes << " bytes" << endl;
  // let's check the crc???

  return 0;
}
//-------------------------------------------------------------------//
void ps178x_serial::SetAddress (unsigned char address){
  fOutBuf[1] = address;
}
//-------------------------------------------------------------------//
void ps178x_serial::SetCommand (unsigned char command){
  fOutBuf[2] = command;
}
//-------------------------------------------------------------------//
void ps178x_serial::SetDataByte(int byte_number, unsigned char byte_value){
  if ((byte_number < 0)||(byte_number > 21)) {
  	cerr << "...Byte Out of range [0:21] = " << byte_number << endl;
	return ;
  }
  fOutBuf[byte_number+3] = byte_value; 
}
//-------------------------------------------------------------------//
unsigned char ps178x_serial::GetAddress(){
  return fInpBuf[1];
}
//-------------------------------------------------------------------//
unsigned char ps178x_serial::GetCommand(){
  return fInpBuf[2];
}
//-------------------------------------------------------------------//
unsigned char ps178x_serial::GetDataByte(int byte_number){
  if ((byte_number < 0)||(byte_number > 21)) {
  	cerr << "...Byte Out of range [0:21] = " << byte_number << endl;
	return 0x00;
  }
  return fInpBuf[byte_number+3];
}
//-------------------------------------------------------------------//
