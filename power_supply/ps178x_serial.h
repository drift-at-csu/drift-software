#ifndef PS178X_SERIAL_H
#define PS178X_SERIAL_H

#include <string>

class ps178x_serial {
  public:
	ps178x_serial();
	~ps178x_serial();

	int Init( std::string aSerialName );

	//---> This submits The command and reads back the response
	int SubmitCommand();

	//---> Setters for Output Buffer
	void SetAddress (unsigned char address);
	void SetCommand (unsigned char command);
	void SetDataByte(int byte_number, unsigned char byte_value);

	//---> Getters for Input Buffers
	unsigned char GetAddress();
	unsigned char GetCommand();
	unsigned char GetDataByte(int byte_number);
  private:
  	// ID of opened serial device file
	int fFileID;
	// There is always 26 bytes
	unsigned char fInpBuf[26];
	unsigned char fOutBuf[26];
};
#endif /* PS178X_SERIAL_H */
