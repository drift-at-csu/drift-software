#include "ps178x_serial.h"

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <getopt.h>
#include <sstream>
using namespace std;

//-------------------------------------------------------------------//
void ShowUsage(char *progname){
  cerr << "Usage: " << progname
  	<< "-c </dev/ttyUSB0> "
	<< "-h <help> "
	<< "0=OFF/1=ON "
	<< endl;
}
//-------------------------------------------------------------------//
int main (int argc, char **argv) {
  stringstream serial_device_string("/dev/ttyUSB1");
  int c;
  unsigned short onoff_request = 0;
  while ((c = getopt(argc, argv, "hc:")) != EOF) {
	switch (c) {
		case 'c':
			serial_device_string.str("");
			serial_device_string << optarg;
			continue;
		case 'h':
			ShowUsage(argv[0]);
			exit(0);
	};
  }
  if ((argc - optind) != 1) {
  	ShowUsage(argv[0]);
	exit(0);
  }
  
  if (argv[optind][0] == '0') {
  	onoff_request = 0;
	cerr << "...Turning PS Off" << endl;
  } else if (argv[optind][0] == '1') {
  	onoff_request = 1;
	cerr << "...Turning PS On" << endl;
  } else {
  	ShowUsage(argv[0]);
	exit(0);
  }

  // cout << "...Trying to connect to \"" << serial_device_string.str() << "\"" << endl;
  ps178x_serial pow_supp;
  pow_supp.Init(serial_device_string.str());
  pow_supp.SetAddress(0x00);

  pow_supp.SetCommand(0x20); // Remote Control Mode
  pow_supp.SetDataByte(0, 0x01); // 1->remote 0->FrontPanel
  pow_supp.SubmitCommand();
  pow_supp.SubmitCommand();

  // cout << 	"...Feedback: Addr=0x" << hex << (int)pow_supp.GetAddress() << 
  //		", Command=0x" << (int)pow_supp.GetCommand() << dec << endl;

  pow_supp.SetCommand(0x26); // Reading Voltage, current etc
  pow_supp.SubmitCommand();
  pow_supp.SubmitCommand();

  double current_value;
  double voltage_value;

  //First 2 bytes is Current (LSB first) , Next 2 bytes is Voltage
  current_value = pow_supp.GetDataByte(0) + pow_supp.GetDataByte(1)*256.0;
  voltage_value = pow_supp.GetDataByte(2) + pow_supp.GetDataByte(3)*256.0;

  cout 	<< "===> Voltage=" << voltage_value/1000.0
  	<< "[V], Current=" << current_value << "[mA]"<< endl;

  // Now Let's Turn The PS Off
  pow_supp.SetCommand(0x21); // Setting the output ON/OFF
  if ( onoff_request == 1 )
	pow_supp.SetDataByte(0, 0x01);
  else
 	pow_supp.SetDataByte(0, 0x00);
  pow_supp.SubmitCommand();

  pow_supp.SetCommand(0x26); // Reading Voltage, current etc
  pow_supp.SubmitCommand();

  current_value = pow_supp.GetDataByte(0) + pow_supp.GetDataByte(1)*256.0;
  voltage_value = pow_supp.GetDataByte(2) + pow_supp.GetDataByte(3)*256.0;

  cout  << "===> Voltage=" << voltage_value/1000.0
  	<< "[V], Current=" << current_value << "[mA]"<< endl;

  return 0;
}
//-------------------------------------------------------------------//
